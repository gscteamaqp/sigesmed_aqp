//creamos nuestro modulo llamado app
var app = angular.module("app", ['ngRoute','treeControl', 'ngTable', 'ngAnimate','ui.bootstrap','chart.js','ngMaterial']);

app.constant('urls', {
    BASE: '/TIGER/rest',
    BASELOGIN: '/TIGER/',
    BASECHAT: "/TIGER/chat_sigesmed"
});

app.config(['$httpProvider','$routeProvider',function($httpProvider,$routeProvider) {

    $httpProvider.interceptors.push(['$q','urls',function($q,urls){
        return{
            'request': function(config){
                config.headers = config.headers || {};
                if(localStorage.getItem('jwt')){
                    config.headers.autorizacion = localStorage.getItem('jwt');
                }
                return config;
            },
            'responseError': function(response){
                if(response.status === 401 || response.status === 403 || response.status === 400){                   
                   localStorage.clear();
                   location.replace(urls.BASELOGIN );
                   return;
                }
                return $q.reject(response);
            }
        };
    }]);
    //hacemos el ruteo de las interfaces de nuestra aplicación
    $routeProvider.when("/menuInicio", {
        templateUrl : "menuInicio.html"
    })
    .when("/subMenuInicio", {
        templateUrl : "subMenuInicio.html"
    })
    .when("/menuPersonalizado", {
        templateUrl : "menuPersonalizado.html"
    })
    //funciones que todos los usuarios tienen
    .when("/bandejaMensajes", {
        templateUrl : "pedagogico/web/bandejaMensajes.html",
        controller : "bandejaMensajesCtrl"
    })
    .when("/calendario", {
        templateUrl : "pedagogico/web/calendario.html",
        controller : "calendarioCtrl"
    })
    .when("/perfilUsuario", {
        templateUrl : "agregados/configuracion_inicial/perfilUsuario.html",
        controller : "perfilUsuarioCtrl"
    })
    //Fin funciones que todos los usuarios tienen
    .otherwise({ redirectTo : "/"});
    
    var modulos = localStorage.getItem('modulos');
    if(modulos){
        //cargamos el ruteo        
        cargaRuteoApp($routeProvider,JSON.parse(modulos));
    }

}]);

//metodo run se inicia una sola vez
app.run(['$rootScope','$location','urls','crud',function($rootScope,$location, urls,crud) {
     
    if( window.innerWidth < 479 ){
        $rootScope.movil = true;
    }
    
    $rootScope.showLoadingIcon = false;    
        
    $rootScope.usuMaster = {usuario:"",rol:"",organizacion:""};
    $rootScope.menuPrincipal = [];
        
    $rootScope.ROLES = {
        DIR_IE: 7,
        ADM_DRE: 2,
        ADM_UGEL: 3
    };
    
    $rootScope.noticias = [{hora:"0:0",titulo:"noticia nueva",descripcion:"descripcion"},{hora:"0:0",titulo:"noticia nueva",descripcion:"descripcion"}];
    $rootScope.notificaciones = [];//[{tipo:"alert-info",nombre:"notificacino de alerta"},{tipo:"alert-danger",nombre:"notificacino de peligro"},{tipo:"alert-warning",nombre:"notificacino de exito"}];
    $rootScope.mensajes = [];//[{nombre:"profesor",hora:"10:00",contenido:"hola como estas"},{nombre:"profesor",hora:"10:00",contenido:"hola como estas"}];
    
    $rootScope.menuParent = [];
    $rootScope.subModuloActual = [];
    $rootScope.vistaActual = [];
    $rootScope.menu = [];
    $rootScope.modNom = "";
    $rootScope.subModNom = "";
    $rootScope.visNom = ""; 
    
    //configuracion inicial "no tocar"
    if(!localStorage.getItem('jwt') || !localStorage.getItem('modulos')){
        localStorage.clear();
        location.replace(urls.BASELOGIN );
    }
    else{
        crud.verificarToken();
        $rootScope.usuMaster.usuario = JSON.parse( window.atob( localStorage.getItem('usuario')) );
                
        $rootScope.usuMaster.rol = JSON.parse( window.atob(localStorage.getItem('rol')) );
        $rootScope.usuMaster.organizacion = JSON.parse( window.atob(localStorage.getItem('organizacion')) );
        if(localStorage.getItem('area'))
            $rootScope.usuMaster.area = JSON.parse( window.atob(localStorage.getItem('area')) );
        
        $rootScope.menuPrincipal = JSON.parse( localStorage.getItem('modulos') );
        $rootScope.funciones = JSON.parse( localStorage.getItem('funciones') );
        
        if( localStorage.getItem('menu') ){
            $rootScope.menu = JSON.parse( localStorage.getItem('menu'));
            $rootScope.modNom = localStorage.getItem('modNom');
            if( localStorage.getItem('visNom') ){
                $rootScope.subModNom = localStorage.getItem('subModNom');
                $rootScope.visNom = JSON.parse( localStorage.getItem('visNom'));
            }
            else
                $location.path("menuInicio");
        }
        else
            $location.path("menuInicio");
        
        if( localStorage.getItem('personalizacion') ){
            $rootScope.personalizacion = JSON.parse( window.atob( localStorage.getItem('personalizacion')));
            if($rootScope.personalizacion.ladoIzq=='N') {
                //$rootScope.ocultarMenu();
                $('#sidebar').toggleClass('hide-left-bar');
                $('#main-content').toggleClass('merge-left');
            }
                
            if ($rootScope.personalizacion.ladoDer=='S') {
                //$rootScope.mostrarMenuDerecha();
                $('.right-sidebar').toggleClass('hide-right-bar');
                $('#main-content').toggleClass('merge-right');
            }           
        }
    }
    
    $rootScope.visNom = "";    
    
    $rootScope.current = new Date();
    $rootScope.options = {
        customClass: getDayClass,
        showWeeks: false,
        startingDay: 0
    };
  
    $rootScope.events = JSON.parse(localStorage.getItem('events'));
    
    $rootScope.cerrarSession = function (){
        localStorage.clear();
        location.replace(urls.BASELOGIN );
    };    
    $rootScope.inicio = function(){
        $rootScope.menuParent = [];
        $rootScope.subModuloActual = [];
        $rootScope.vistaActual = [];
        $rootScope.menu = [];
        $rootScope.modNom = "";
        $rootScope.subModNom = "";
        $rootScope.visNom = "";
        $location.path("menuInicio");
    };        
    
    $rootScope.showLoading = function() {
        $rootScope.showLoadingIcon = true;
    }
    
    $rootScope.hideLoading = function() {
        $rootScope.showLoadingIcon = false;
        $("#loadingDiv").removeClass("ng-hide");
    }
    
    $rootScope.elegirMenu = function(menu){    
       
        $rootScope.menuParent = menu;
        $rootScope.menu = menu.subModulos;
        $rootScope.modNom = $rootScope.movil? menu.codigo: menu.nombre;
        localStorage.setItem('menu', JSON.stringify($rootScope.menu));
        localStorage.setItem('modNom', $rootScope.modNom);
        $rootScope.subModNom = "";
        $rootScope.visNom = "";
        $rootScope.color = menu.color;
        $location.path("subMenuInicio");
        
    };
    
    $rootScope.menuToggle = function(e,subModulo){
              
        $rootScope.subModuloActual = subModulo;
        
        if(subModulo.funciones){
            if($rootScope.subModNom == ($rootScope.movil? subModulo.codigo: subModulo.nombre)){
                //$rootScope.subModNom = ""; //Desactivamos la funcion toggle
                localStorage.setItem('subModNom', "");
                $location.path("subMenuInicio");
            }
            else{                
                $rootScope.subModNom = $rootScope.movil? subModulo.codigo: subModulo.nombre;
                localStorage.setItem('subModNom', $rootScope.subModNom);
            }
            $rootScope.visNom = "";            
        }
    };
    
    $rootScope.elegirVista = function(vista,subModulo){
        
        $rootScope.vistaActual = vista;
        $rootScope.visNom = vista.nombre;
        localStorage.setItem('visNom', JSON.stringify($rootScope.visNom));
        if(subModulo){
            $rootScope.subModNom = $rootScope.movil? subModulo.codigo: subModulo.nombre;;
            localStorage.setItem('subModNom', $rootScope.subModNom);
        }
        else{
            //ocultar siempre que elijamos una opcion, solo para moviles
            if(window.innerWidth < 981){
                $('#sidebar').toggleClass('hide-left-bar');
                $('#main-content').toggleClass('merge-left');
            }
        }
        $location.path(vista.clave);
    };
    $rootScope.ocultarMenu = function(){
        $('#sidebar').toggleClass('hide-left-bar');
        $('#main-content').toggleClass('merge-left');
    };
    $rootScope.mostrarMenuDerecha = function(){
        $('.right-sidebar').toggleClass('hide-right-bar');
        $('#main-content').toggleClass('merge-right');
    };
    $rootScope.verAyuda = function(){
        
        var o = buscarObjeto($rootScope.menu,'nombre',$rootScope.subModNom);
        if(o){
            $rootScope.imgAyuda = o.codigo.trim()+'.png';
            $('#modalAyuda').modal('show');
        }
    };
    $rootScope.listarNotificaciones = function(){
        //preparamos un objeto request
        var request = crud.crearRequest('bandejaAlerta',1,'listarBandejaAlertaPendientes');
        crud.listar("/web",request,function(res){
            
            $rootScope.notificaciones = res.data;
        },function(data){
            console.info(data);
        });
    };
    $rootScope.listarMensajes = function(){
        //preparamos un objeto request
        var request = crud.crearRequest('mensaje',1,'listarMensajesNuevos');
        crud.listar("/web",request,function(res){
            
            $rootScope.mensajes = res.data;
        },function(data){
            console.info(data);
        });
    };
    $rootScope.verAlerta = function(alerta){
        var request = crud.crearRequest('bandejaAlerta',1,'marcarComoVisto');
        request.setData({bandejaAlertaID:alerta.bandejaAlertaID});
        crud.actualizar("/web",request,function(res){
            eliminarElemento($rootScope.notificaciones,alerta.i);
            $location.path(alerta.accion);
        },function(data){
            console.info(data);
        });
        
    };
    $rootScope.verMensaje = function(mensaje){
        eliminarElemento($rootScope.mensajes,mensaje.i);
        $location.path("bandejaMensajes");        
    };
    $rootScope.listarNotificaciones();
    $rootScope.listarMensajes();    
    
    function getDayClass(data) {
        var date = data.date, mode = data.mode;
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0,0,0,0);
            
            for (var i = 0; i < $rootScope.events.length; i++) {
                var currentDay = new Date($rootScope.events[i].date).setHours(0,0,0,0);
                
                if (dayToCheck === currentDay)
                    return $rootScope.events[i].status;
            }
        }
        
        return '';
    }
}]);

//cargando los archivos javascript
var modulos = localStorage.getItem('modulos');
var color=0;
if(localStorage.getItem('personalizacion')){
    color=JSON.parse( window.atob( localStorage.getItem('personalizacion'))).color;    
}
if(modulos){
    cargaArchivosJavascript( JSON.parse(modulos) );    
    cargaEstilos(color);    
} 

function cargaEstilos(color){
    switch(color){
        case 0: //Default
            break;
        case 1:// AZUL
            document.write('<link href="../recursos/css/style_azul.css" rel="stylesheet">');
            break;
        case 2: //VERDE
            break;
        case 3: //ROJO
            document.write('<link href="../recursos/css/style_rojo.css" rel="stylesheet">');
            break;
        default:
            break;
    }
   
};

function cargaArchivosJavascript(modulos){
    
    var subModulos;
    var funciones;

    for(var i=0;i<modulos.length;i++ ){
        subModulos = modulos[i].subModulos;
        for(var j=0;subModulos && j<subModulos.length;j++ ){
            funciones = subModulos[j].funciones;
            for(var k=0;funciones && k<funciones.length;k++ ){
                document.write('<script src="'+funciones[k].url+funciones[k].controlador+'.js"></script>');
            }
        }  
    }
    //añadiendo funciones genericas
    document.write('<script src="pedagogico/web/bandejaMensajesCtrl.js"></script>');
    document.write('<script src="pedagogico/web/calendarioCtrl.js"></script>');
    document.write('<script src="agregados/configuracion_inicial/perfilUsuarioCtrl.js"></script>');    
};
function cargaRuteoApp(router,modulos){
    
    var subModulos;
    var funciones;

    for(var i=0;i<modulos.length;i++ ){
        subModulos = modulos[i].subModulos;
        for(var j=0;subModulos && j<subModulos.length;j++ ){
            funciones = subModulos[j].funciones;
            for(var k=0;funciones && k<funciones.length;k++ ){
                router.when("/"+funciones[k].clave, {
                    templateUrl : funciones[k].url+funciones[k].interfaz,
                    controller : funciones[k].controlador,
                    controllerAs : 'ctrl' + funciones[k].controlador
                });
            }
        }
    }  
};

