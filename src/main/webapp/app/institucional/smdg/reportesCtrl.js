app.controller("reportesCtrl",["$rootScope","$scope","$sce","NgTableParams","crud","modal", function ($rootScope,$scope,$sce,NgTableParams,crud,modal){        
        
//#############################    TAB de lista de Fichas general      #############################//    
    var paramsItems = {count: 10};
    var settingItems = {counts: []};
    $scope.tablaFichas = new NgTableParams(paramsItems, settingItems);
    $scope.plaId = undefined;
    
    $scope.documentos = [];
    $scope.selectedDocumento = "";
    
    $scope.ini1 = function(){
        $scope.listarDocumentos();
    };
    
    $scope.dataReporte1 = [];
    
    $scope.listarFichas = function(d){   
        var request = crud.crearRequest('ficha_evaluacion',1,'filtrarFichas');
//        request.setData({iteide:d.iteide});   
        request.setData({iteide:d});   
        crud.listar("/smdg",request,function(data){
            settingItems.dataset = data.data;
            iniciarPosiciones(settingItems.dataset);            
            $scope.tablaFichas.settings(settingItems);            
            $scope.generarGraficoFichasFIle(data.data);
            $scope.dataReporte1 = data.data;
        },function(data){
            console.info(data);
        });        
    };    
    
    $scope.listarDocumentos = function(){        

        var request = crud.crearRequest('item_file',1,'listarItemsxOrganizacion');
        
        request.setData({orgId:$rootScope.usuMaster.organizacion.organizacionID});
        crud.listar("/smdg",request,function(data){
            $scope.documentos = data.data;
            
        },function(data){
            console.info(data);
        });
    };
    
    $scope.generarGraficoFichasFIle= function(d){        
                
        var data = [];
        for(var k in d){
            data.push({nombre:d[k].planom, data:d[k].fictot});
        }        

        Chart.defaults.global.legend.display = true;
        Chart.defaults.global.tooltips.enabled = true;
        crearGraficoBarras2("graFichasFileBarra","Puntaje por fichas",data);
        crearGraficoPie("graFichasFilePie",data);
        
    };
    
    $scope.imprimirReporte1 = function(){
        var grafico1 = new MyFile("GRAFICO 1");
        grafico1.parseDataURL( document.getElementById("graFichasFileBarra").toDataURL("image/png") );
        grafico1.size = 50;
        
        var grafico2 = new MyFile("GRAFICO 2");
        grafico2.parseDataURL( document.getElementById("graFichasFilePie").toDataURL("image/png") );
        grafico2.size = 50;
        
        var request = crud.crearRequest('reporte',1,'reporteFichasEvaluacion');
//        request.setData(grafico);
//        request.setData(grafico2);
        request.setData({tabla:$scope.dataReporte1, grafico1:grafico1, grafico2:grafico2});
        
        crud.insertar("/smdg",request,function(response){
            if(response.responseSta){                
                verDocumento( response.data.reporte );
            }            
        },function(data){
            console.info(data);
        });       
    };
    
    
//#############################    TAB de lista de Fichas individual       #############################//
    
    $scope.selectedFicha = "";
    $scope.fichas = [];
    
    $scope.plantilla={
        codigo:"",
        nombre:"",
        tipo:0, 
        tipoNom:""        
    };
    
    $scope.grupos = [];
    
    $scope.ini2 = function(){
        $scope.listarFichas2();
    };
    
    $scope.listarFichas2 = function(){                
        
        var request = crud.crearRequest('ficha_evaluacion',1,'listarFichasxOrganizacion');               
        crud.listar("/smdg",request,function(data){
            $scope.fichas = data.data;            
        },function(data){
            console.info(data);
        });        
    };
    
    $scope.mostrarFicha = function(ficide, plaid){
        $scope.ficide = ficide;
        //preparamos un objeto request           
        var request = crud.crearRequest('ficha_evaluacion',1,'listarFicha');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        request.setData({ficide:ficide, plaid:plaid});
        crud.listar("/smdg",request,function(data){ 
            $scope.plantilla = data.data.pop();
            $scope.grupos = data.data;
            $scope.fun1($scope.grupos);
        },function(data){
            console.info(data);
        });
    };
    
    $scope.fun1 = function(grupos){
        var numInd = 0;
        for(var i in grupos){
            numInd += grupos[i].indicadores.length;
        }
        $scope.plantilla.maxPunt = numInd * 3;
    };
    
    $scope.fun2 = function(val){
        if(val === undefined) return "0.00";
        return ((val*100)/$scope.plantilla.maxPunt).toFixed(2);
    };
    
    $scope.sumarPuntosGrupo = function(indicador, grupo){

        $scope.plantilla.total = ($scope.plantilla.total === undefined?0:$scope.plantilla.total);

        indicador.tempPunt = (indicador.tempPunt === undefined?0:indicador.tempPunt);       

        var punto = parseInt(indicador.punto);
        grupo.total = (grupo.total === undefined?0:grupo.total);       
        grupo.total += (punto - indicador.tempPunt);
        
        $scope.plantilla.total += (punto - indicador.tempPunt);
        
        indicador.tempPunt = punto; 
        
    };
    
    $scope.datosEvaluador = {};
    
    $scope.listarFichaEspecifica = function(f){                
        $scope.datosEvaluador.cargo = f.evacar;
        $scope.datosEvaluador.nombres = f.ficeva;
        $scope.datosEvaluador.fecha = f.ficfec;
        $scope.mostrarFicha(f.ficide, f.plaid);
    };
    
    $scope.imprimirReporte2 = function(){
        
        var request = crud.crearRequest('reporte',1,'reporteFichaIndividual');
        request.setData({evaluador:$scope.datosEvaluador, plantilla:$scope.plantilla, grupos:$scope.grupos});
        
        crud.insertar("/smdg",request,function(response){
            if(response.responseSta){
                verDocumento( response.data.reporte );
            }            
        },function(data){
            console.info(data);
        });       
    };
    
//#############################    TAB de avance de objetivos    #############################//
    
    $scope.selectedDocumento3 = "";
    $scope.dataReporte3 = [];
    
    $scope.documento = {};
    var paramsObj = {count: 5};
    var settingObj = {counts: []};
    $scope.tablaObjetivos = new NgTableParams(paramsObj, settingObj);
    
    $scope.prepararObjetivos = function(d){            
            $scope.listarObjetivos(d.iteide);
    };
    
    $scope.listarObjetivos = function(iteide){
        var request = crud.crearRequest('item_file',1,'listarObjetivos');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        request.setData({iteide:iteide, tipo: 'O'});
        crud.listar("/smdg",request,function(data){
            
            for(var i = 0; i < data.data.length ;++i){
                data.data[i].objusu = fun11(data.data[i].objusu);
                data.data[i].objava = data.data[i].objava.toFixed(2);
            }
            
            settingObj.dataset = data.data;
            iniciarPosiciones(settingObj.dataset);
            $scope.tablaObjetivos.settings(settingObj);
            $scope.generarGraficoObjetivos(data.data);
            $scope.dataReporte3 = data.data;
            
        },function(data){
            console.info(data);
        });
    };    
    
    $scope.generarGraficoObjetivos = function(d){        
                
        var data = [];
        for(var k in d){
            data.push({nombre:d[k].objdes.slice(0,7), data:d[k].objava});
        }        

        Chart.defaults.global.legend.display = true;
        Chart.defaults.global.tooltips.enabled = true;
        crearGraficoBarras2("graObjBarra","% de Avance por Objetivos",data);
        crearGraficoPie("graObjPie",data);
        
    };
    
    
    $scope.imprimirReporte3 = function(){
        var grafico1 = new MyFile("GRAFICO 3");
        grafico1.parseDataURL( document.getElementById("graObjBarra").toDataURL("image/png") );
        grafico1.size = 50;
        
        var grafico2 = new MyFile("GRAFICO 2");
        grafico2.parseDataURL( document.getElementById("graObjPie").toDataURL("image/png") );
        grafico2.size = 50;
        
        var request = crud.crearRequest('reporte',1,'reporteObjetivos');
        
        request.setData({tabla:$scope.dataReporte3, grafico1:grafico1, grafico2:grafico2});
        
        crud.insertar("/smdg",request,function(response){
            if(response.responseSta){                
                verDocumento( response.data.reporte );
            }            
        },function(data){
            console.info(data);
        });       
    };

//#############################             fin         #############################//
  
//#############################    TAB de avance de actividades       #############################//
    
    $scope.selectedDocumento2 = "";
    $scope.dataReporte4 = [];
    
//    $scope.documento = {};
    var paramsAct = {count: 5};
    var settingAct = {counts: []};
    $scope.tablaActividades = new NgTableParams(paramsAct, settingAct);
    
    $scope.prepararActividades = function(d){
            $scope.listarActividades(d);
    };
    
    $scope.listarActividades = function(iteide){
        var request = crud.crearRequest('item_file',1,'listarObjetivos');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        request.setData({iteide:iteide, tipo: 'A'});
        crud.listar("/smdg",request,function(data){
            for(var i = 0; i < data.data.length ;++i){
                data.data[i].objusu = fun11(data.data[i].objusu);
                data.data[i].objava = data.data[i].objava.toFixed(2);
            }
            
            settingAct.dataset = data.data;
            iniciarPosiciones(settingAct.dataset);
            $scope.tablaActividades.settings(settingAct);
            $scope.tablaActividades.data = [];
            $scope.generarGraficoActividades(data.data);
            $scope.dataReporte4 = data.data;
        },function(data){
            console.info(data);
        });
    };

    fun11 = function (ideres){               
        return $scope.trabajadores2[ideres];
    };

    
    $scope.trabajadores2 = {};
    
    $scope.listarTrabajadores = function() {
        //preparamos un objeto request        
        var request = crud.crearRequest('trabajador',1,'listarTrabajadoresPorOrganizacionyTipo');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        request.setData({orgId:$rootScope.usuMaster.organizacion.organizacionID, traTip:""});
        crud.listar("/directorio",request,function(data){            
            $scope.trabajadores = data.data;
            fun22();
            $scope.trabajadores2;
        },function(data){
            console.info(data);
        });
    };
    
    $scope.listarTrabajadores();
    
    fun22 = function (){        
        for(var i = 0; i<$scope.trabajadores.length; ++i){
            $scope.trabajadores2[$scope.trabajadores[i].traId] = $scope.trabajadores[i];
        }        
    };
    
    $scope.generarGraficoActividades = function(d){        
                
        var data = [];
        for(var k in d){
            data.push({nombre:d[k].objdes.slice(0,7), data:d[k].objava});
        }        

        Chart.defaults.global.legend.display = true;
        Chart.defaults.global.tooltips.enabled = true;
        crearGraficoBarras2("graActBarra","% de Avance por Actividades",data);
        crearGraficoPie("graActPie",data);
        
    };
    
    $scope.imprimirReporte4 = function(){
        var grafico1 = new MyFile("GRAFICO 4");
        grafico1.parseDataURL( document.getElementById("graActBarra").toDataURL("image/png") );
        grafico1.size = 50;
        
        var grafico2 = new MyFile("GRAFICO 4");
        grafico2.parseDataURL( document.getElementById("graActPie").toDataURL("image/png") );
        grafico2.size = 50;
        
        var request = crud.crearRequest('reporte',1,'reporteObjetivos');

        request.setData({tabla:$scope.dataReporte4, grafico1:grafico1, grafico2:grafico2});
        
        crud.insertar("/smdg",request,function(response){
            if(response.responseSta){                
                verDocumento( response.data.reporte );
            }            
        },function(data){
            console.info(data);
        });       
    };
        
}]);

