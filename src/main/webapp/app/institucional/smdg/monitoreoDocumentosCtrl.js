app.controller("monitoreoDocumentosCtrl",["$rootScope","$scope","$sce","NgTableParams","crud","modal", function ($rootScope,$scope,$sce,NgTableParams,crud,modal){        
    $scope.content = "";
    $scope.dataBase64 = "";
    
    var paramsItems = {count: 10};
    var settingItems = {counts: []};
    $scope.tablaItems = new NgTableParams(paramsItems, settingItems);
    
    $scope.listarItems = function(){        
        //preparamos un objeto request        
        var request = crud.crearRequest('item_file',1,'listarItemsxOrganizacion');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
//        orgId:$rootScope.usuMaster.organizacion.organizacionID = 4;// ide el aorganizacion UGEL
        request.setData({orgId:$rootScope.usuMaster.organizacion.organizacionID});
        crud.listar("/smdg",request,function(data){
            settingItems.dataset = data.data;
            iniciarPosiciones(settingItems.dataset);
            $scope.tablaItems.settings(settingItems);
        },function(data){
            console.info(data);
        });
    };
//    $scope.listarItems();

//    OBJETIVOS

    $scope.tipoObj = '';
    $scope.titulo = '';

    $scope.objetivoTipo = [
        {key:'q',value:'cualitativo'},
        {key:'c',value:'cuantitativo'}
    ];
    
    $scope.documento = {};
    var paramsObj = {count: 5};
    var settingObj = {counts: []};
    $scope.tablaObjetivos = new NgTableParams(paramsObj, settingObj);
    
    $scope.objetivosEliminados = [];
    
    $scope.prepararObjetivos = function(d, tipo){
        
        $scope.tipoObj = tipo;
        $scope.titulo = (tipo === 'O'?['Objetivo', 'Objetivos']:['Actividad', 'Actividades']);        
        $scope.documento = d;
        $scope.listarObjetivos(d.iteide);
                
    };
    
    $scope.listarObjetivos = function(iteide, tipo){
        var request = crud.crearRequest('item_file',1,'listarObjetivos');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        request.setData({iteide:iteide, tipo: $scope.tipoObj});
        crud.listar("/smdg",request,function(data){  
            
            for(var i = 0; i < data.data.length ;++i){
                data.data[i].objfec = new Date(data.data[i].objfec);
                data.data[i].objusu = fun1(data.data[i].objusu);
                data.data[i].objava = data.data[i].objava.toFixed(2);
            }
            
            settingObj.dataset = data.data;
            iniciarPosiciones(settingObj.dataset);
            $scope.tablaObjetivos.settings(settingObj);
            $scope.tablaObjetivos.data = [];
            $('#modalObjetivos').modal('show'); 
            
        },function(data){
            console.info(data);
        });
    };
    
    $scope.prepararObjetivo = function() {
        
        $scope.nuevoObjetivo = {};
        
        $scope.cuantitativoShow(false);
        $scope.nuevoObjetivo.objfec = new Date();
        $scope.nuevoObjetivo.objtip = 'q';
        
        $('#modalNuevoObjetivo').modal('show');
        
    };
    
    $scope.nuevoObjetivo = {};
    $scope.agregarObjetivo = function() {
        
        if($scope.nuevoObjetivo.objide !== undefined){            
            settingObj.dataset[$scope.nuevoObjetivo.i] = $scope.nuevoObjetivo;
            $scope.tablaObjetivos.reload();
            $scope.nuevoObjetivo = {};
            $('#modalNuevoObjetivo').modal('hide');
        }
        else{
            objeto = {                
                objdes:$scope.nuevoObjetivo.objdes,
                objtip:$scope.nuevoObjetivo.objtip === undefined ? 'q':$scope.nuevoObjetivo.objtip,                                                        
                objusu:$scope.nuevoObjetivo.objusu,
                objfec:$scope.nuevoObjetivo.objfec,
                objava:0,
                indicador:$scope.nuevoObjetivo.indicador,
                meta:$scope.nuevoObjetivo.meta,
                odeava:0
            };
            insertarElemento(settingObj.dataset, objeto);
            iniciarPosiciones(settingObj.dataset);//<----------
            $scope.tablaObjetivos.reload();
            $scope.nuevoObjetivo = {};
            $('#modalNuevoObjetivo').modal('hide');
        }
        
    };
    
    $scope.eliminarObjetivo = function(i,r){        
        //si estamso cancelando la edicion
        if(r.edi){
            r.edi = false;
            delete r.copia;
        }
        //si queremos eliminar el elemento
        else{                 
            if(r.objide !== undefined){
                
                $scope.objetivosEliminados.push({objide:r.objide});
                
                eliminarElemento(settingObj.dataset,i);
                $scope.tablaObjetivos.reload();
            }
            else{
                eliminarElemento(settingObj.dataset,i);
                $scope.tablaObjetivos.reload();
            }            
        }                
        //        
    };
    
    $scope.guardarObjetivos = function() {
        
        var request = crud.crearRequest('item_file',1,"guardarObjetivos");
        var objetivos = $scope.tablaObjetivos.data;

        for(var k=0; k < objetivos.length; ++k){
            objetivos[k].objfec = [objetivos[k].objfec.getDate(), objetivos[k].objfec.getMonth()+1, objetivos[k].objfec.getFullYear()].join('/');// "23/23/2014"
        }
        
        request.setData({iteide: $scope.documento.iteide, objetivos: objetivos, eliminados: $scope.objetivosEliminados, tipo:$scope.tipoObj});
        
        crud.actualizar("/smdg",request,function(response){              
            modal.mensaje("CONFIRMACION",response.responseMsg);                
                $scope.listarItems();
    //            REINICIAR VARIABLES
                $scope.nuevoObjetivo = {};
                settingObj.dataset = [];
                iniciarPosiciones(settingObj.dataset);
                $scope.tablaObjetivos.settings(settingObj);
                
                $scope.objetivosEliminados = [];
                $scope.tipoObj = '';
                
        },function(data){
            console.info(data);
        });
    };
    
    $scope.tempObj = {};
    $scope.editarObjetivo = function(r, f) {
        //si estamso editando
                    
        $scope.tempObj = r;
        $scope.nuevoObjetivo = JSON.parse(JSON.stringify(r));
        $scope.nuevoObjetivo.objfec = r.objfec;

        if($scope.nuevoObjetivo.objtip === 'c'){
            
            $scope.cuantitativo = true;
            $('#modalNuevoObjetivo').modal('show');
            
        }
        else{            
            $scope.cuantitativo = false;
            $('#modalNuevoObjetivo').modal('show');
        }
        
        
    };        
    
    $scope.listarTrabajadores = function() {
        //preparamos un objeto request        
        var request = crud.crearRequest('trabajador',1,'listarTrabajadoresPorOrganizacionyTipo');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        request.setData({orgId:$rootScope.usuMaster.organizacion.organizacionID, traTip:""});
        crud.listar("/directorio",request,function(data){            
            $scope.trabajadores = data.data;
            fun2();
            $scope.trabajadores2;            
        },function(data){
            console.info(data);
        });
    };        
    $scope.listarTrabajadores();
    $scope.trabajadores2 = {};
    
    fun1 = function (ideres){               
        return $scope.trabajadores2[ideres];
    };
    
    fun2 = function (){        
        for(var i = 0; i<$scope.trabajadores.length; ++i){
            $scope.trabajadores2[$scope.trabajadores[i].traId] = $scope.trabajadores[i];
        }        
    };
    
    $scope.objetivo = {};
    $scope.objetivoDetalle = {};
    
    $scope.avanceObjetivos = function(o){        
       
        $scope.objetivo = {
            i:o.i,
            odeide:o.odeide,
            objava:o.objava,
            objdes:o.objdes,
            odeava:o.objava,
            indicador:o.indicador,
            meta:o.meta            
        };

        if(o.objtip === 'c'){
                $('#modalAvanceObjetivo').modal('show');              
        }
        else{
            $('#modalAvanceObjetivo2').modal('show');
        }
        
    };
    
    $scope.editarAvance = function(r) {
        //si estamso editando
        if(r.edi){            
            r.odeava = r.copia.odeava;            
            delete r.copia;            
            r.edi = false; 
            r.objava = (r.odeava*100/r.meta).toFixed(2);
        }
        //si queremos editar
        else{
            r.copia = JSON.parse(JSON.stringify({odeava:r.odeava}));            
            r.edi = true;
        }
    };
    
    $scope.editarAvance2 = function(r) {
        //si estamso editando
        if(r.edi){            
            r.odeava = r.copia.odeava;            
            delete r.copia;            
            r.edi = false; 
            r.objava = r.odeava;
        }
        //si queremos editar
        else{
            r.copia = JSON.parse(JSON.stringify({odeava:r.odeava}));
            r.copia.odeava = parseFloat(r.copia.odeava);
            r.edi = true;
        }
    };
    
    $scope.cuantitativo = false;
    $scope.cuantitativoShow = function(t){
        $scope.cuantitativo = t;
    };
    
    $scope.verificar = function (b){
        if($scope.nuevoObjetivo.objtip === 'c')
            return b.indi.$invalid || b.meta.$invalid  || b.name.$invalid || b.nameres.$invalid;
        else
            return b.name.$invalid || b.nameres.$invalid;
    };
    
    $scope.actualizarAvance = function (r){

        settingObj.dataset[r.i].objava = r.objava;
        settingObj.dataset[r.i].odeide = r.odeide;
        settingObj.dataset[r.i].odeava = r.odeava;
        settingObj.dataset[r.i].indicador = r.indicador;
        settingObj.dataset[r.i].meta = r.meta;
        $scope.tablaObjetivos.reload();
        $('#modalAvanceObjetivo').modal('hide');

    };
    
    $scope.actualizarAvance2 = function (r){

        settingObj.dataset[r.i].objava = r.objava;
        settingObj.dataset[r.i].odeide = r.odeide;
        settingObj.dataset[r.i].odeava = r.odeava;
        $scope.tablaObjetivos.reload();
        $('#modalAvanceObjetivo2').modal('hide');

    };
    
    
//    FIN OBJETIVOS

//    FICHAS
    
    var paramsItems = {count: 20};
    var settingItems = {counts: []};
    $scope.tablaFichas = new NgTableParams(paramsItems, settingItems);
    $scope.plaId = undefined;
    
    $scope.listarFichas = function(d){        
        
        $scope.documento = d;
        
        var request = crud.crearRequest('ficha_evaluacion',1,'filtrarFichas');
        request.setData({iteide:d.iteide});
        crud.listar("/smdg",request,function(data){
            settingItems.dataset = data.data;
            iniciarPosiciones(settingItems.dataset);            
            $scope.tablaFichas.settings(settingItems);
            $('#modalFichas').modal('show');
            
        },function(data){
            console.info(data);
        });        
    };
    
    $scope.evaluador = {};
    
    $scope.visualizarFicha = function(f, plaid){
                
        $scope.evaluador.cargo = f.evacar;
        $scope.evaluador.nombres = f.ficeva;
        $scope.evaluador.fecha = f.ficfec;
        
        //preparamos un objeto request           
        var request = crud.crearRequest('ficha_evaluacion',1,'imprimirFicha');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        request.setData({ficide:f.ficide, plaid:plaid, evaluador:$scope.evaluador});
        crud.insertar("/smdg",request,function(response){
            if(response.responseSta){                
                verDocumento( response.data.reporte );
            }            
        },function(data){
            console.info(data);
        });        
        
    };
//    FIN 

}]);

