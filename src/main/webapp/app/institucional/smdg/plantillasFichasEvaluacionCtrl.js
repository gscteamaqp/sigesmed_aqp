app.controller("plantillasFichasEvaluacionCtrl",["$rootScope","$scope","$sce","NgTableParams","crud","modal", function ($rootScope,$scope,$sce,NgTableParams,crud,modal){        

    $scope.main = true;
    var paramsItems = {count: 20};
    var settingItems = {counts: []};
    $scope.tablaPlantillas = new NgTableParams(paramsItems, settingItems);
    $scope.plaId = undefined;
    
    $scope.listarPlantillas = function(){        
        //preparamos un objeto request        
        var request = crud.crearRequest('plantilla_ficha',1,'listarPlantillas');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error        
        crud.listar("/smdg",request,function(data){              
            settingItems.dataset = data.data;            
            iniciarPosiciones(settingItems.dataset);            
            $scope.tablaPlantillas.settings(settingItems);
        },function(data){
            console.info(data);
        });        
    };
    
    function listarTipoGrupo(){
        //preparamos un objeto request
        var request = crud.crearRequest('plantilla_grupo',1,'listarGrupos');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/smdg",request,function(data){
            $scope.tipoGrupos = data.data;
            $scope.nuevoGrupo.Tipo = data.data[0].tpoId;
        },function(data){
            console.info(data);
        });
    };
    listarTipoGrupo();
    function listarTipoPlantilla(){
        //preparamos un objeto request
        var request = crud.crearRequest('plantilla_ficha',1,'listarTipoPlantillas');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/smdg",request,function(data){
            $scope.tipoPlantillas = data.data;
            $scope.plantilla.tipo = data.data[0].id;
        },function(data){
            console.info(data);
        });
    };
    listarTipoPlantilla();
    
    $scope.crearPlantilla = function(flag, flag2){
        if(flag2){
            $scope.plaId = undefined;
            $scope.nuevaPlantilla = {};
            $scope.nuevoGrupo = {};
            $scope.nuevoGrupo.Tipo = 1;
            $scope.nuevoIndicador = {};
            $scope.nuevoItem = {};
            $scope.grupos = [];            
        }
        
        
        $scope.main=flag;
        $scope.listarPlantillas();
//        $('#modalCrearPlantilla').modal('show'); 
    };

    //JSON para una plantilla
    $scope.nuevaPlantilla = {};
    $scope.nuevoGrupo = {};
    $scope.nuevoIndicador = {};
    $scope.nuevoItem = {};
    
    $scope.grupos = [];
    
    //Grupos    
    $scope.agregarGrupo = function() {
        $scope.nuevoGrupo.indicadores = [];
        $scope.grupos.push($scope.nuevoGrupo);
        $scope.nuevoGrupo = {};
        $scope.nuevoGrupo.Tipo = $scope.tipoGrupos[0].tpoId;
    };

    $scope.eliminarGrupo = function(index) {        
        $scope.grupos.splice(index, 1);      
    };
    
    $scope.editarGrupo = function(r) {
        if(r.edi){                        
            r.gruNom = r.copia.gruNom;
            r.Tipo = r.copia.Tipo;
            delete r.copia;
            r.edi = false; 
        }
        else{
            r.copia = JSON.parse(JSON.stringify(r));            
            r.edi = true;
        }
    }
    //fin grupos
  
    //Indicadores
    $scope.agregarIndicador = function(grupo) {        

        grupo.indicadores.push(grupo.nuevoIndicador);
        grupo.nuevoIndicador = {};
    };

    $scope.eliminarIndicador = function(index, grupo) {
        grupo.indicadores.splice(index, 1);
    };
    $scope.editarIndicador = function(grupo, r) {
        if(r.edi){            
            r.indNom = r.copia.indNom;
            delete r.copia;
            r.edi = false; 
        }
        else{
            r.copia = JSON.parse(JSON.stringify(r));            
            r.edi = true;
        }
    };
    
    //fin indicadores
    
    //Items
//    $scope.agregarItem = function(indicador) {
//      indicador.items.push(indicador.nuevoItem);
//      indicador.nuevoItem = {};
//    };
//
//    $scope.eliminarItem = function(index, indicador) {      
//      indicador.items.splice(index, 1);
//    };
//    $scope.editarItem = function(r) {
//        if(r.edi){
//            r.iteNom = r.copia.iteNom;
//            r.itePun = r.copia.itePun;
//            delete r.copia;
//            r.edi = false; 
//        }
//        else{
//            r.copia = JSON.parse(JSON.stringify(r));            
//            r.edi = true;
//        }
//    }
    //fin items
    //datos necesarios para guardar una plantilla
    $scope.plantilla={
        codigo:"",
        nombre:"",
        tipo:0
    };
    
    //operacion para guardar una plantilla
    $scope.guardarPlantilla = function(){
        var request = crud.crearRequest('plantilla_ficha',1,'registrarPlantilla');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error        
        request.setData({grupos:$scope.grupos, nombre:$scope.plantilla.nombre, tipo:$scope.plantilla.tipo});
        crud.insertar("/smdg",request,function(response){              
            modal.mensaje("CONFIRMACION",response.responseMsg);
                $scope.crearPlantilla(true, true);
                $scope.listarPlantillas();
//                reiniciarvariables();
        },function(data){
            console.info(data);
        });
    };
    //Fin
    
//    Editar una plantilla
    $scope.editarPlantilla = function(plaide){
        //preparamos un objeto request   
        $scope.plaId = plaide;
        var request = crud.crearRequest('plantilla_ficha',1,'listarPlantilla');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        request.setData({plaId:$scope.plaId});
        crud.listar("/smdg",request,function(data){                          
            $scope.plantilla = data.data.pop();
            $scope.grupos = data.data;            
            $scope.crearPlantilla(false, false);            
        },function(data){
            console.info(data);
        });
    };
    
    $scope.actualizarCabecera = function(){
        modal.mensajeConfirmacion($scope,"seguro que desea cambiar los datos de esta plantilla",function(){            
            var request = crud.crearRequest('plantilla_ficha',1,'actualizarPlantilla');                    
            request.setData({plaId:$scope.plantilla.plaide, 
                            plaNom:$scope.plantilla.nombre,
                            plaTip:$scope.plantilla.tipo                            
            });

            crud.actualizar("/smdg",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                
            },function(data){
                console.info(data);                        
            });            
        }); 
    };
    
    $scope.reportePlantilla = function(){
        
        var objeto = {
            
            nombre:$scope.plantilla.nombre,
            tipo:$scope.plantilla.tipoNom
        };
        
        var request = crud.crearRequest('plantilla_ficha',1,'imprimirPlantilla');
        request.setData({grupos:$scope.grupos, objeto:objeto});
        crud.listar("/smdg",request,function(data){            
            $scope.dataBase64 = data.data[0].datareporte;
//            window.open($scope.dataBase64);
            verDocumentoPestana($scope.dataBase64);
        },function(data){
            console.info(data);
        });
        
    };
    $scope.visualizarPlantilla = function(plaide){
        //preparamos un objeto request   
        $scope.plaId = plaide;
        var request = crud.crearRequest('plantilla_ficha',1,'listarPlantilla');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        request.setData({plaId:$scope.plaId});
        crud.listar("/smdg",request,function(data){                          
            $scope.plantilla = data.data.pop();
            $scope.grupos = data.data;            
            $scope.reportePlantilla();
        },function(data){
            console.info(data);
        });
        
    };
    
    $scope.advertenciaGrupo = false;
    $scope.verificarPlantilla = function(flag){
        $scope.advertenciaGrupo = ($scope.grupos.length > 0 ? false:true);
        return $scope.grupos.length > 0 ? (flag && true):true;
    };
    
}]);

