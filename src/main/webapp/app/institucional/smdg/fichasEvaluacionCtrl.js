app.controller("fichasEvaluacionCtrl",["$rootScope","$scope","$sce","NgTableParams","crud","modal", function ($rootScope,$scope,$sce,NgTableParams,crud,modal){        

    $scope.ventanas = [true,false,false];
    $scope.ventanaActual = 0;
     
    $scope.selectedPlantilla = "";
    $scope.selectedDocumento = "";
    $scope.fecha = new Date();
    
    $scope.ficide = ""; //variable necesaria para actualizar o insertar
    
    $scope.datosEvaluador = {};
    
    $scope.obtenerDatosUsuarioActual = function(){
        var request = crud.crearRequest('trabajador',1,'obtenerDatosPersonales');
        request.setData({orgId:$rootScope.usuMaster.organizacion.organizacionID, perId:$rootScope.usuMaster.usuario.usuarioID});
        crud.listar("/directorio",request,function(data){
            $scope.datosEvaluador = data.data;            
        },function(data){
            console.info(data);
        });
    };
    
    $scope.obtenerDatosUsuarioActual();
    
    $scope.adelante = function(num){
        $scope.ventanas[$scope.ventanaActual + num] = $scope.ventanas[$scope.ventanaActual];
        $scope.ventanas[$scope.ventanaActual] = !$scope.ventanas[$scope.ventanaActual];
        $scope.ventanaActual += num;
    };
    
    $scope.atras = function(num){
        
        $scope.ventanas[$scope.ventanaActual - num] = $scope.ventanas[$scope.ventanaActual];
        $scope.ventanas[$scope.ventanaActual] = !$scope.ventanas[$scope.ventanaActual];
        $scope.ventanaActual -= num;
                
    };
        
//#############################    lista de Fichas      #############################//    
    var paramsItems = {count: 20};
    var settingItems = {counts: []};
    $scope.tablaFichas = new NgTableParams(paramsItems, settingItems);
    $scope.plaId = undefined;
    
    $scope.listarFichas = function(){        
        
        var request = crud.crearRequest('ficha_evaluacion',1,'listarFichasxOrganizacion');
               
        crud.listar("/smdg",request,function(data){
            settingItems.dataset = data.data;
            iniciarPosiciones(settingItems.dataset);            
            $scope.tablaFichas.settings(settingItems);
            
        },function(data){
            console.info(data);
        });
    };
    
    
    
//#############################    lista de plantillas   #############################//
    $scope.plantillas = [];
    
    $scope.listarPlantillas = function(){        
        
        var request = crud.crearRequest('plantilla_ficha',1,'listarPlantillas');             
        crud.listar("/smdg",request,function(data){
            $scope.plantillas = data.data;

        },function(data){
            console.info(data);
        });        
    };
    $scope.listarPlantillas();
    
//#############################           Listar documentos        #############################//
    $scope.documentos = [];
    $scope.listarDocumentos = function(){        

        var request = crud.crearRequest('item_file',1,'listarItemsxOrganizacion');
        
        request.setData({orgId:$rootScope.usuMaster.organizacion.organizacionID});
        crud.listar("/smdg",request,function(data){
            $scope.documentos = data.data;

        },function(data){
            console.info(data);
        });
    };
    $scope.listarDocumentos();

//#############################    Funciones para el componente de fecha    #############################//
    $scope.today = function() {
        $scope.dt = new Date();
        
    };
    $scope.today();
    
    $scope.clear = function() {
        $scope.dt = null;
    };
    
    $scope.openDate = function() {
        $scope.popup.opened = true;
    };
    
    $scope.popup = {
        opened: false
    };
    
    $scope.placement = {
        options: [
          'Sanchez Cerro',
          'ILO',
          'Mariscal Nieto'
        ],
        selected: 'Mariscal Nieto'
    };
        
//    operaciones sobre una ficha
    $scope.plantilla={
        codigo:"",
        nombre:"",
        tipo:0, 
        tipoNom:""        
    };
    
    $scope.aplicarPlantilla = function(plaide){ 
        
        $scope.adelante(1);        
//        $scope.plaId = $scope.selectedPlantilla.plaide;
        $scope.plaId = $scope.selectedPlantilla.codpla;
        var request = crud.crearRequest('plantilla_ficha',1,'listarPlantilla');
        request.setData({plaId:$scope.plaId});
        crud.listar("/smdg",request,function(data){                          

            $scope.plantilla = data.data.pop();
            $scope.grupos = data.data;
            $scope.fun1($scope.grupos);
            
        },function(data){
            console.info(data);
        });
    };
    
    $scope.tempPunt = 0;
    $scope.tempIndi = 0;    
    
    $scope.sumarPuntosGrupo = function(indicador, grupo){

        $scope.plantilla.total = ($scope.plantilla.total === undefined?0:$scope.plantilla.total);

        indicador.tempPunt = (indicador.tempPunt === undefined?0:indicador.tempPunt);       

        var punto = parseInt(indicador.punto);
        grupo.total = (grupo.total === undefined?0:grupo.total);       
        grupo.total += (punto - indicador.tempPunt);
        
        $scope.plantilla.total += (punto - indicador.tempPunt);
        
        indicador.tempPunt = punto; 
        
    };
    
    $scope.fun1 = function(grupos){
        var numInd = 0;
        for(var i in grupos){
            numInd += grupos[i].indicadores.length;
        }
        $scope.plantilla.maxPunt = numInd * 3;
    };
            
    $scope.fun2 = function(val){
        if(val === undefined) return "0.00";
        return ((val*100)/$scope.plantilla.maxPunt).toFixed(2);
    };
    
//#############################             fin         #############################//
    
//    Operaciones sobre la ficha de evaluacion   
    //JSON para una plantilla
    $scope.nuevaPlantilla = {};
    $scope.nuevoGrupo = {};
    $scope.nuevoIndicador = {};
    $scope.nuevoItem = {};
    
    $scope.grupos = [];
    
    $scope.guardaroactualizarFicha = function(ficide){
        if($scope.ficide == null){
            modal.mensajeConfirmacion($scope,"seguro que desea actualizar esta ficha",function(){
                var request = crud.crearRequest('ficha_evaluacion',1,"actualizarFicha");
                
                request.setData({grupos:$scope.grupos, total:$scope.plantilla.total, ficide:$scope.ficide});//(cambiar el 0 por el ide del usuarioactual), codigo:$scope.plantilla.codigo, nombre:$scope.plantilla.nombre, tipo:$scope.plantilla.tipo});
                crud.actualizar("/smdg",request,function(response){              
                    modal.mensaje("CONFIRMACION",response.responseMsg);                
                        $scope.listarDocumentos();
                        $scope.atras(2);                        
                        $scope.listarFichas();
        //                REINICIAR VARIABLES X COMPLETAR: reiniciarvariables();
                        $scope.ficide = "";
                        $scope.selectedDocumento = "";
                        $scope.fecha = new Date();
                        $scope.selectedPlantilla = "";
                        
                },function(data){
                    console.info(data);
                });
            });                      
            return;
        } 
        var request = crud.crearRequest('ficha_evaluacion',1,"insertarFicha");
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error  
        var fecha =  [$scope.fecha.getDate(), $scope.fecha.getMonth()+1, $scope.fecha.getFullYear()].join('/');
        request.setData({grupos:$scope.grupos, total:$scope.plantilla.total ,iteide:$scope.selectedDocumento ,fevfec:fecha ,feveva:$scope.iteusuide ,fevesp: $scope.iteusuide, plaide:$scope.plantilla.plaide, ficide:$scope.ficide});//identificar quien es feveva y quien fevesp
        crud.insertar("/smdg",request,function(response){              
            modal.mensaje("CONFIRMACION",response.responseMsg);                
                $scope.listarDocumentos();
                $scope.atras(2);                
                $scope.listarFichas();
//                REINICIAR VARIABLES X COMPLETAR: reiniciarvariables();                
                $scope.ficide = "";  
                $scope.selectedDocumento = "";
                $scope.fecha = new Date();
                $scope.selectedPlantilla = "";
                
        },function(data){
            console.info(data);
        });
    };
    
    $scope.editando = false;
//    Editar una plantilla
    $scope.editarFicha = function(ficide, plaid){
        $scope.ficide = ficide;
        //preparamos un objeto request           
        var request = crud.crearRequest('ficha_evaluacion',1,'listarFicha');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        request.setData({ficide:ficide, plaid:plaid});
        crud.listar("/smdg",request,function(data){                          
            $scope.plantilla = data.data.pop();
            $scope.grupos = data.data;
            $scope.fun1($scope.grupos);
            $scope.adelante(2);
            
            $scope.editando = true;
            
        },function(data){
            console.info(data);
        });
    };
    
    $scope.regresar = function(){
        if($scope.editando) $scope.atras(2);
        else $scope.atras(1);            
    };
    
    $scope.avanzar = function(){
        $scope.editando = false;
        $scope.adelante(1);        
    };
    
    $scope.evaluador = {};
    
    $scope.visualizarFicha = function(f, plaid){
                
        $scope.evaluador.cargo = f.evacar;
        $scope.evaluador.nombres = f.ficeva;
        $scope.evaluador.fecha = f.ficfec;
        
        //preparamos un objeto request           
        var request = crud.crearRequest('ficha_evaluacion',1,'imprimirFicha');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        request.setData({ficide:f.ficide, plaid:plaid, evaluador:$scope.evaluador});
        crud.insertar("/smdg",request,function(response){
            if(response.responseSta){                
                verDocumentoPestana( response.data.reporte );
            }            
        },function(data){
            console.info(data);
        });        
        
    };  
    
    $scope.eliminarFicha = function(i, ficid){
        modal.mensajeConfirmacion($scope,"seguro que desea eliminar este registro",function(){
            
            var request = crud.crearRequest('ficha_evaluacion',1,'eliminarFicha');
            request.setData({ficid:ficid});

            crud.actualizar("/smdg",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    eliminarElemento(settingItems.dataset,i);
                    $scope.tablaFichas.reload();
                }

            },function(data){
                console.info(data);
            });
            
        });
    };
        
}]);

