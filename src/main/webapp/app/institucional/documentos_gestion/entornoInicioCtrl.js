app.controller("entornoInicioCtrl", ["$scope", "NgTableParams","crud", "modal", function infoController($scope, NgTableParams, crud, modal) {
        //Variables usadas por todo el controlador
        $scope.documentos = [];
        $scope.rolelist = [];
        $scope.contDirAct = [];
        $scope.contDirActNav = [];
        
        $scope.pathURL="";
        $scope.fecAnio = new Date().getFullYear();
        $scope.fecMes = new Date().getMonth();
        $scope.fecDia = new Date().getDay();
        $scope.fecCompl = "" + $scope.fecAnio + "-" + $scope.fecMes + "-" + $scope.fecDia;
        $scope.documento = {coddoc: "-1", desdoc: "", tamanio: "", fecCre: new Date(), urldoc: "", archivo: "Ninguno"};
        $scope.directorio = {codDir: "-1", verDir: "1", nomDir: "", tamDir: "", fecCre: new Date(), fecIniP: "", fecFinP: "", urlDir: ""};
        $scope.iteFilSelected = {};
        $scope.mensajePlazoEditar="";
        
        $scope.filtroNombreDoc = "";
        $scope.filtroNombreDir = "";
        $scope.urlArch = "#";
        $scope.nomDirAnt = "";
        $scope.fecIniPlaAnt={};
        $scope.fecFinPlaAnt={};
        $scope.verEditarFechaDir=false;
        $scope.verBorrarFechaDir=false;
             
        $scope.regionTmp = "";
        $scope.tipesps = [];
        $scope.entidades = [];
        
        //Ubicacion de Entidades
        $scope.selUbiEnt = {Reg: "0", Uge: "0", Ins: "0", TipDoc:0};
        $scope.regiones = [];
        $scope.ugeles = [];
        $scope.insts = [];
        $scope.institucionDefecto = "";
        
        $scope.dirPar = {roleId: "-1", roleUrl: "rutaDefecto"};
        
        //Variables para el control de plazo
        $scope.labelCndPlazo = "Sin Control Plazo";
        $scope.flagControlPlazo = false;
        $scope.flagEliminarPlazoEditar=false;
        $scope.flagDesproteger=false;
        $scope.fechasPlazo = {
            fecini: new Date(),
            fecfin: new Date()
        };
        
        
        $scope.directSelected=[];
        $scope.directSelectedNav = [];

        //Documento Seleccionado
        $scope.docSelect = {index:"-1"};
//        $scope.tamUrl = 0;
        
        $scope.selectedRow = null;  // initialize our variable to null
        
        //Datos de Proteccion File
        $scope.dataProt = {idUsuario: -1, codProt: "", ideFil:0};
        $scope.dataAutElim = {idUsuario: -1, dniUsuario: "", codProt:"",coddoc:0};
        
        //Check de entrada para notificacion
        $scope.setcheckb = {};
        $scope.setcheckb.solPerm = false;
        
        //Variables para el control de la ng-table
        var paramsRepositorio = {count: 10};
        var settingRepositorio = {counts: []};
        $scope.tablaRepositorio = new NgTableParams(paramsRepositorio, settingRepositorio);

        //Variables para el control de usuarios de archivo
        var paramsUsersArch = {count: 5};
        var settingUsersArch = {counts: []};
        $scope.tablaUsuariosArch = new NgTableParams(paramsUsersArch, settingUsersArch);
        
        var paramsHistorial={count: 10};
        var settingHistorial ={counts:[]};
        $scope.tablaHistorial=new NgTableParams(paramsHistorial,settingHistorial);
        
        //Variable que almacena la lista de usuarios de un archivo
        $scope.listUsuariosArch = [];
        $scope.codFilTmpInCompart = 0; //codigo de item antiguamente seleccionado para el compartir
        
        //Seleccionadores de la lista de Usuarios para Compartir
        $scope.selItemPerm = {posFil:0,idPer:1};
        $scope.selPermisos = [$scope.selItemPerm];
        $scope.listPerms = []; //Lista de permisos
        $scope.listCrgs = []; //lista de Cargos
        $scope.flagSelPerm = false;
        $scope.idePerAnt = -1;
        $scope.userComp = {ide:0, nom:"",correo:"",idePer:0,ideCrg:0};
        $scope.listPers = [];
        $scope.primerPers = "";
        
        //usuario seleccionado
        $scope.usuSelect = {index:"-1"};
        
        //Variables para el reporte
        $scope.labels2 = [];
        $scope.data2 = [];
        
        
        //Variable de Notificacion
        $scope.varsSolPerm = {};
        $scope.varsSolPerm.tipDoc = "";
        $scope.varsSolPerm.nomDoc = "";
        $scope.varsSolPerm.dniUsu = "";
        
        $scope.docSelectedByMenu = {};
        $scope.filtroGen = false;
        $scope.vistaAdmin=false;
        $scope.vistaUgel=false;
        
        //Activador de busqueda personalizada
        $scope.activarFiltroEsp=function(){
            if($scope.filtroGen === true)
                $scope.filtroGen = false;
            else
                $scope.filtroGen = true;
        };
        
        $scope.historialNombre="";
        $scope.historialTipo="";
        $scope.oldProteccion="";
        
        $scope.flagProteccion=false;
        $scope.flagProteccionEditar=false;
        $scope.flagControlPlazoEditar=false;
        
        $scope.organizaciones=[];
        $scope.organizacionSel;
        
        $scope.showLoadBar = false;
        
       $scope.options = {legend: {display: true},
       title: {
                display: true,
                text: 'Documentos de Gestion'
        },
        tooltips: {
            enabled: true,	//no tooltips on mose hover
            cornerRadius: 0, //no border-radius
        }        
        };


       

        /*-------------------------------------------------------------------------
         *   Funcion encargada de levantar todo el arbol jerarquico de files
         -------------------------------------------------------------------------*/
        $scope.listarDocs = function () {
            if ($scope.rolelist.length === 0) {
                
                var request = crud.crearRequest('entornoInicio', 1, 'listarDocumentosGestion');                
                
                crud.listar("/documentosGestion", request, function (data) {
                    //colocar la data de la base a la variable del ambito

                    
                    settingRepositorio.dataset=data.data;
                    iniciarPosiciones(settingRepositorio.dataset);
                    $scope.tablaRepositorio.settings(settingRepositorio);
                   $scope.rolelist = data.data;

                }, function (data) {
                    //console.info(data);
                });
            }
        };
        /*-------------------------------------------------------------------------
         *         Inserta un nuevo documento o archivo al repositorio
         -------------------------------------------------------------------------*/
        $scope.agregarDocGes = function () {
            
            $scope.showLoadBar = true;
            var request = crud.crearRequest('entornoInicio', 1, 'insertarDocumentosGestion');

                    $scope.documento.selTipDoc = $scope.tipesps[$scope.selUbiEnt.TipDoc -1].id;;         //Tipo de Documento
                    
                    $scope.documento.idResponsable = $scope.usuMaster.usuario.usuarioID; //Responsable

                    $scope.documento.org=$scope.usuMaster.organizacion.organizacionID;
                    $scope.documento.tamanio=$scope.documento.archivo.size;
                    $scope.documento.flagProteccion=$scope.flagProteccion;
            //                    $scope.documento.tamanio=$scope.documento;
                    
                    request.setData($scope.documento);
                    console.log($scope.documento);
                    console.log(request);
                    
//                    return;
                    if ($scope.documento.desdoc !== "" && $scope.documento.coddoc!=="-1") {
                        if($scope.flagProteccion==true && $scope.documento.proteccion.length!==5 ){                    
                            modal.mensaje("MENSAJE","El código de protección debe tener 5 dígitos");
                        }else{
                            crud.insertar("/documentosGestion", request, function (response) {
                                $scope.showLoadBar = false;
                                modal.mensaje("CONFIRMACION HECHA", response.responseMsg);
                                console.info("Inserción Exitosa de Archivo");
                                if (response.responseSta) {
                                    //$scope.listarDocs();
                                    $scope.documento.coddoc=response.data.coddoc;
                                    insertarElemento(settingRepositorio.dataset,$scope.documento);
                                
                                    $scope.tablaRepositorio.reload();
//                                  $scope.showSelected($scope.temp_node_selected);
                                
                                    //reiniciamos las variables
                                    $scope.documento = {coddoc: "", verdoc: 1, desdoc: "", tipo: "", fecCre: new Date(), urldoc: "", archivo: "Ninguno"};                                                                                                                   
                                    $scope.flagProteccion=false;
                                    $scope.showSelected($scope.temp_node_selected);
                                    $('#modalNuevoArch').modal('hide');
                                }
                            }, function (data) {
                            });
                        }                           
                    } else {
                        $scope.showLoadBar = false;
                        if($scope.documento.coddoc === "-1"){
                            modal.mensaje("CONFIRMACION", "Por favor seleccione un directorio al almacenar");
                        }else{
                            modal.mensaje("CONFIRMACION", "Por favor rellene todos los campos");
                        }
                    }
        };
        
        //Inicia los indices relacionados a las entidades para mostrarlos al inicio de la edicion
        $scope.actualizarIndicesOrganizacion = function(list){
            $scope.selUbiEnt.TipDoc = $scope.docSel.tesFil;
        };
        
        //Prepara los parametros de un documento y mostrarlos al inicio de la edicion
        $scope.prepararEditarDoc = function(i,doc){
            //Documento Seleccionado de la Tabla
            $scope.docSel = JSON.parse(JSON.stringify(doc));
            $scope.docSel.i = i;
            //Debemos Buscar La Ugel, Region y su Institucion
            $scope.actualizarIndicesOrganizacion();
            $('#modalEditarDoc').modal('show'); 
        };
        $scope.prepararHistorial=function(docges,tipo){
            var request = crud.crearRequest('entornoInicio', 1, 'listarHistorialPorDocumento');                
                $scope.historialNombre=docges.nomFil;                
                $scope.historialTipo=tipo;
                request.setData({docID: docges.ideFil});
                crud.listar("/documentosGestion", request, function (data) {
                    //colocar la data de la base a la variable del ambito

                    
                    settingHistorial.dataset=data.data;
                    $scope.tablaHistorial.settings(settingHistorial);
                    $('#modalHistorial').modal('show');

                    
                    
                    //console.info($scope.rolelist[0]);
                }, function (data) {
                    modal.mensaje("MENSAJE","Error al momento de listar el Historial");
                });
        
            
        }
        
        /*-------------------------------------------------------------------------
         *                  Actualizar un documento o archivo
         -------------------------------------------------------------------------*/
        $scope.actualizarDoc = function () {
            $scope.documento.selTipDoc = $scope.selUbiEnt.TipDoc;         //Tipo de Documento
            $scope.documento.idResponsable = $scope.usuMaster.usuario.ID; //Responsable
            
             //Cambiando datos
            $scope.documento.codpad = $scope.docSel.idePadFil; //Padre que lo contiene
            $scope.documento.desdoc = $scope.docSel.nomFil;            
            $scope.documento.fecCre = $scope.docSel.fecCre; //Fecha de Creacion
            $scope.documento.fecMod = new Date(); //Fecha de Modificacion
            $scope.documento.tifIde = $scope.docSel.tifFil;
            $scope.documento.coddoc = $scope.docSel.ideFil; //codigo del documento
            $scope.documento.org=$scope.usuMaster.organizacion.organizacionID;
            
            var request = crud.crearRequest('entornoInicio', 1, 'actualizarDocumento');
            request.setData($scope.documento);

            crud.actualizar("/documentosGestion", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    //actualizando la lista general de documentos solo con la posicion
                    $scope.contDirAct[$scope.docSel.i] = $scope.docSel;
                    $('#modalEditarDoc').modal('hide');
                }
            }, function (data) {
                console.info(data);
            });                     
        };
        //Eliminar Documento del Repositorio
        $scope.eliminarDoc = function(i,docges){
            if(docges.iteProt==="0"){
               //Pasar todos los parametros de docges a docSel
                $scope.docSel = JSON.parse(JSON.stringify(docges));
                //Eliminacion de Documento pero solo desactivar
                 modal.mensajeConfirmacion($scope, "Esta seguro de eliminar el documento", function () {
                    var request = crud.crearRequest('entornoInicio', 1, 'eliminarDocumento');
                    request.setData({idDoc: docges.ideFil});
                    crud.eliminar("/documentosGestion", request, function (response) {
                        modal.mensaje("CONFIRMACION", response.responseMsg);
                        if (response.responseSta) {
                            eliminarElemento(settingRepositorio.dataset,i);
                            $scope.tablaRepositorio.reload();
                            console.info("Se envio el archivo a papelera exitosamente");
                        }
                    }, function (data) {

                    });
                }); 
            }else{
                modal.mensaje("NOTIFICACION", "El archivo esta protegido");
                //Debe activarse un formulario de solicitud de codigo
                $scope.setcheckb.solPerm = false;
                $scope.docTemporal=docges;
                $scope.tempi=i;
                $('#modalSolPermDoc').modal('show');              
            }                     
        };

        /*-------------------------------------------------------------------------
         *               Direccion Base donde se inicia el Repositorio
         -------------------------------------------------------------------------*/
        //esta funcion permite conocer el tamaÃ±o de la url que se hara una sola vez para todos los archivos
        $scope.construirUrlGen = function () {
            console.log($scope.usuMaster);                     
            var request = crud.crearRequest('organizacion',1,'buscarOrganizacion');
            request.setData({organizacionID:$scope.usuMaster.organizacion.organizacionID});
            crud.listar("/configuracionInicial",request,function(data){
                if(data.data.tipoOrganizacion=="Unidad de Gestion Educativa Local"){//SI ES UGEL
                    $scope.vistaUgel=true;
                     $scope.vistaAdmin=true; 
                }else{
                    if($scope.usuMaster.rol.rolID==11){//ROL DE DIRECTOR DE IE.
                        $scope.vistaAdmin=true;                
                    }else{
                        $scope.vistaAdmin=false;
                    }
                }
            },function(data){
                console.info(data);
            });

            request = crud.crearRequest('entornoInicio', 1, 'construirUrlArch');
                crud.listar("/documentosGestion", request, function (data) {

                    $scope.pathURL=data.data.ruta;
                }, function (data) {
                    //console.info(data);
                });            
        };
        
        //Visualizar de archivos en formato PDF
        $scope.visualizarDoc = function () {
            var page=$scope.tablaRepositorio.page();
            $scope.iteFilSeleccionado = settingRepositorio.dataset[(parseInt($scope.docSelect.index,10))+10*(page-1)];
            $scope.nameFull="archivos/"+$scope.iteFilSeleccionado.urlFil+"/"+$scope.iteFilSeleccionado.nomFil+".pdf";
        };
        
        
        
        $scope.listarUsuarioByArch = function (coddocumento) {
            var request = crud.crearRequest('entornoInicio', 1, 'listarUsersPorArch');
            request.setData({coddoc: coddocumento});
            //Iniciamos la variable que guarda la historia del codigo del documento
            $scope.codFilTmpInCompart = coddocumento;
            console.log("$scope.codFilTmpInCompart");
            console.log($scope.codFilTmpInCompart);
            crud.listar("/documentosGestion", request, function (data) {
                //colocar la data de la base a la variable del ambito
                console.log("data.data");
                console.log(data.data);
                $scope.listUsuariosArch = data.data;
                settingUsersArch.dataset = $scope.listUsuariosArch;
                $scope.tablaUsuariosArch.settings(settingUsersArch);                
            }, function (data) {
                //console.info(data);
            });

        };
        
        
        //Funcion que lista los tipos de documentos especializados (PAT,PEI,POI,PEI,RI,PCC,etc)
        $scope.listarPermisos = function () {
            var request = crud.crearRequest('entornoInicio', 1, 'listarPermisos');
            crud.listar("/documentosGestion", request, function (data) {
                //colocar la data de la base a la variable del ambito
                $scope.listPerms = data.data;
            }, function (data) {
               console.info(data);
            });
        };
        
        //Funcion para activar el select actual por la pos de fila
        $scope.eliminarComparticion = function(pos,dataUsu){
            //Pasar tan solo el codigo de itemfiledetalle
            //Eliminacion de Documento pero solo desactivar

                 var request = crud.crearRequest('entornoInicio', 1, 'eliminarComparticion');
                
                request.setData({ideIfd: dataUsu.usuario.usuarioID, ideDoc: $scope.iteFilSeleccionado.ideFil});
                crud.eliminar("/documentosGestion", request, function (response) {
                    if (response.responseSta) {
                        //eliminar de la lista
                        settingUsersArch.dataset.splice(pos,1);
                        $scope.tablaUsuariosArch.reload();
                        modal.mensaje("MENSAJE","Se elimino la comparticion");
                    }
                }, function (data) {

                });
//             modal.mensajeConfirmacion($scope, "Esta seguro de eliminar la actual comparticion", function () {
//               
//            });
        };        
        //Compartir Archivos en PDF
        $scope.compartirArch = function(docges){
            //Levantar datos para la organizacion
            var request = crud.crearRequest('organizacion',1,'listarOrganizaciones');
            crud.listar("/configuracionInicial",request,function(data){
                $scope.organizaciones = data.data;                
            },function(data){
                console.info(data);
            });
                    
            //$scope.iteFilSeleccionado = settingRepositorio.dataset[parseInt($scope.docSelect.index, 10)];
             //Limpiar variables
            $scope.userComp = {ideUsu:0, rol:"",nomUsu:"",idePer:0,org:""};
            $scope.organizacionSel=0;
            $scope.iteFilSeleccionado = docges;
            console.log($scope.iteFilSeleccionado.ideFil);
            console.log($scope.codFilTmpInCompart);
            //if($scope.iteFilSeleccionado.ideFil !== $scope.codFilTmpInCompart)
                $scope.listarUsuarioByArch($scope.iteFilSeleccionado.ideFil);
            $('#modalCompartArch').modal('show');
            //$scope.usuSelected = settingUsersArch.dataset[parseInt($scope.usuSelect.index,10)];
           
        };
              
        
        $scope.saveOldIdPer = function(idePer){
            $scope.idePerAnt = idePer;
        };
        
        $scope.buscarPersonaByNombre = function(){
            //Buscar a la persona por el nombre
            
              var request=crud.crearRequest('entornoInicio',1,'buscarPersonaByNombreYOrganizacion');
                request.setData({nomUser: $scope.userComp.nom, organizacionID:$scope.organizacionSel})
                crud.listar("/documentosGestion",request,function(data){
                    $scope.listPers = data.data;
                    //$scope.userComp.ide = $scope.listPers[0].usuarioID;
                    //$scope.$broadcast('angucomplete-alt:changeInput', 'listaPersonas', $scope.listPers[0]);
                },function(data){
                    console.info(data);
                });                                
        };
        
        //Funcion cuando se seleccion una persona de los resultados
        $scope.setPersona = function (selected) {
            if (selected) {
                console.info(selected.originalObject);
                //Activar Correo
                $scope.userComp.correo = selected.originalObject.email;
                $scope.userComp.ide = selected.originalObject.perId;
                $scope.userComp.nom = selected.originalObject.nom;
            } else {
                console.info(":(");
            }
        };
        
        //Funcion de Busqeuda para en el autocompletado de Persona
        $scope.localBusquedaPers = function (str, listPers) {
            var matches = [];
            console.info(listPers);
            listPers.forEach(function (pers) {
                if (pers.nom.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0) {
                    matches.push(pers);
                }
            });
            return matches;
        };
        
        //Registra usuario en la comparticion de archivos
        $scope.registrarComparticion = function(){
            //Funcion que registra un nuevo usuario en la comparticion del archivo
            $scope.organizacionSel;
            var request = crud.crearRequest('entornoInicio', 1, 'registrarComparticion');
            //insertando resultando del contenido del archivo
            var datos = {};
            datos.ideUsu = $scope.userComp.usuarioID;
            datos.ideUsuSes = $scope.userComp.usuarioSesionID;
//            datos.idePerm = $scope.userComp.idePer;-->NO
            datos.ideDoc = $scope.iteFilSeleccionado.ideFil;

            //datos.ideCrg = $scope.userComp.ideCrg;
            request.setData(datos);
            
            if ($scope.datos !== null) {
                crud.insertar("/documentosGestion", request, function (response) {
                    modal.mensaje("CONFIRMACION HECHA", response.responseMsg);
                    console.info("Insercion Hecha de Comparticion");
                    if (response.responseSta) {
                        //Actualizar la lista de usuarios compartidos
                        var nuevoUser = {};
                        //nuevoUser.idePer = $scope.userComp.idePer;
                        nuevoUser.usuario = {};
                        nuevoUser.usuario.usuarioID = $scope.userComp.usuarioID;
  
                        nuevoUser.usuario.rol = $scope.userComp.rol;
                        
                        //nuevoUser.usuario.nomUsu = $scope.listPers[0].nombreCompleot;
                        nuevoUser.usuario.nombreCompleto=$scope.userComp.nombreCompleto;
                        nuevoUser.usuario.organizacion=$scope.userComp.organizacion;
                        
                        settingUsersArch.dataset.push(nuevoUser);
                        $scope.tablaUsuariosArch.reload();
                        
                        
                        //Limpiar variables
                        $scope.userComp = {ideUsu:0, rol:"",nomUsu:"",idePer:0,org:""};
                        //Ocultar la ventana
                        //$('#modalCompartArch').modal('hide');
                    }
                }, function (data) {

                });
            } else {
                modal.mensaje("CONFIRMACIÃ“N", "Porfavor Rellene todos los campos");
            }

        };
        
        
        //Busca un elemento sobre el arbol de files (Recursivo)
        $scope.buscarItemArbolById = function(listaRaiz,id){
            //CasoBase
            if (listaRaiz["roleId"] === id)
                return listaRaiz;
            for (var i = 0; i < listaRaiz.children.length; i++) {
                //Recorriendo los hijos
                var hijo = listaRaiz.children[i];
                //Caso Recursivo
                var obj = $scope.buscarItemArbolById(hijo,id);
                if(typeof obj !== "undefined"){
                    return obj;
                }
            }
        };
        
        //Inserta un nuevo file por el padre que lo almacenara
        $scope.insertItemArbolDirByIdPad = function(idFile, name, ver, cod,url,tam){

            var item = {roleId: 0, roleCat: "", roleFec: new Date(), roleName: "", rolePad: 0, roleTam: 0, roleUrl: "", roleVer: 0, children: []};
            item.roleId = cod;
            item.roleCat = "D";
            item.roleFec = (new Date()).getTime();
            item.roleName = name;
            item.roleVer = ver;
            item.roleTam = 0;
            item.roleUrl = url;
            item.rolePad = idFile; //si no tiene padre le pondra cero como valor

            //Verificamos si es el primer item a insertarse
            //es decir no tiene padre alguno
            if (idFile !== 0) {
                //buscamos al padre de manera normal si es un segundo item
                $scope.itemArbol = $scope.buscarItemArbolById($scope.rolelist[0], idFile);
                $scope.itemArbol.children.push(item);
            }else{
//                $scope.rolelist[0] = item;
                //$scope.itemArbol.push=item;
                $scope.rolelist.push=item;
            }
                
            console.info($scope.rolelist[0]);
        };


        /*-------------------------------------------------------------------------
         *                      Insertar un nuevo Directorio
         -------------------------------------------------------------------------*/
        $scope.agregarDirGes = function () {
            //Guardado de Directorio
            var request = crud.crearRequest('entornoInicio', 1, 'insertarDirectorioGestion');
            //insertando resultando del contenido del archivo
            $scope.directorio.fecIniP = $scope.fechasPlazo.fecini;
            $scope.directorio.fecFinP = $scope.fechasPlazo.fecfin;
            $scope.directorio.flagPlazo = $scope.flagControlPlazo;
            $scope.directorio.flagProteccion=$scope.flagProteccion;
            $scope.directorio.fecCre = new Date();
            $scope.directorio.usuarioID=$scope.usuMaster.usuario.usuarioID;
            request.setData($scope.directorio);
            

            if ($scope.directorio.nomDir !== ""/* && $scope.directorio.verDir !== ""*/) {
                if($scope.flagProteccion==true && $scope.directorio.proteccion.length!==5 ){                    
                    modal.mensaje("MENSAJE","El cÃ³digo de proteccioÃ³n debe tener 5 dÃ­gitos");
                }else{
                    crud.insertar("/documentosGestion", request, function (response) {
                    modal.mensaje("CONFIRMACION HECHA", response.responseMsg);
                    if (response.responseSta) {
                         
                        $scope.directorio.codDir=response.data.ideFil;
                        insertarElemento(settingRepositorio.dataset,$scope.directorio);
                        
                        if($scope.temp_node_selected){
                            $scope.tablaRepositorio.reload();            
                            $scope.showSelected($scope.temp_node_selected);
                        }
                        
                        
                        
                        //Actualizar el arbol de la estructura mostrada el ROLELIST
                        $scope.insertItemArbolDirByIdPad(response.data.padFil, $scope.directorio.nomDir, $scope.directorio.verDir, response.data.ideFil, response.data.urlFil, $scope.directorio.tamDir);
                        //reiniciamos las variables
                        $scope.directorio = {codDir: "", verDir: 1, nomDir: "", tamDir: 0, fecListDir: "", urlDir: $scope.directorio.urlDir, fecInsertDir: new Date(), codPad:$scope.directorio.codPad};
                        $scope.fecIniPlaAnt={};
                        $scope.fecFinPlaAnt={};
                        
                        //$scope.showSelected($scope.temp_node_selected);
                        $scope.listarDocs();
                        $('#modalNuevoDirect').modal('hide');
                        $scope.flagControlPlazoEditar=false;
                            $scope.flagEliminarPlazoEditar=false
                            $scope.flagProteccion=false;
                     }
                    }, function (data) {
                        modal.mensaje("MENSAJE", "Falla al insertar el directorio");
                    });
                }                
            } else {
                modal.mensaje("CONFIRMACIÃ“N", "Porfavor Rellene todos los campos");
            }

        };
        
        
        //Busca Item con posicion en SubArbol de File por el ID (No Recursivo)
        $scope.buscarItemByIdFilInSubArbolDir = function(listaHijos, idSelected){
            //Debemos buscar justo el id de hijo que debemos eliminar
            var itemEncontrado = {objItem:"",posItem:0};
            for(var i = 0; i < listaHijos.length; i++){
                if(listaHijos[i].roleId === idSelected){
                    itemEncontrado.objItem = listaHijos[i];
                    itemEncontrado.posItem = i;
                    return itemEncontrado; // En caso de que encontro el hijo
                }
            }
            return itemEncontrado; // En caso de que no exista el hijo buscado
        };
        
        
        //Actualiza el Arbol de File por el padre con nuevas propiedades y nuevo padre
        $scope.actualizarItemArbolDirByIdPad = function(idPadreDir,idDirActual,codNuevoPadreDir,newProperties){
            //visualizarDoc//Haremos 2 busquedas una para el padre del directorio y otra
            //para buscar el directorio donde reubicar
            var itemPadre = $scope.buscarItemArbolById($scope.rolelist[0],idPadreDir);
            //En este padre debemos encontrar tanto el id como el propio objeto
            //del directorio seleccionado por el id tomaremos como ejemplo
            var dirActualTmp = $scope.buscarItemByIdFilInSubArbolDir (itemPadre.children,idDirActual);
            var dirActual = dirActualTmp.objItem;
            var posItem = dirActualTmp.posItem;
            //Eliminando el objeto antiguo y colocando el nuevo objeto en su posicion
            itemPadre.children.splice(posItem,1,newProperties);
            
            //Solo cuando existe reubicacion
            if (codNuevoPadreDir !== idDirActual) {
                //Quitar el directorio ya actualizado
                itemPadre.children.splice(posItem, 1);
                //Ahora debemos buscar el directorio a reubicar
                //ToDo: Al ser cualquier ruta de ubicacion esta puede encontrase en cualquier ruta
                //por lo cual necesariamente debe buscarse desde la raiz
                var nuevoPadre = $scope.buscarItemArbolById($scope.rolelist[0], codNuevoPadreDir);
                //El directorio ahora puede ser seteado con lo nuevos datos
                nuevoPadre.children.push(newProperties);
            }
        };
        
        /*-------------------------------------------------------------------------
         *                  Actualizar Directorio del Repositorio
         -------------------------------------------------------------------------*/
        $scope.updateDirGes = function () {
            if ($scope.iteFilSelected.roleCat === "D") {
//                if ($scope.directSelected.ideFil !== $scope.directSelectedNav.ideFil || $scope.nomDirAnt !== $scope.directSelected.nomFil) {
                    //Actualizando el directorio con los nuevos datos
                    var request = crud.crearRequest('entornoInicio', 1, 'actualizarDirGes');
                    //Con los nuevos datos del directorio (nombre y ruta podremos actualizarlo)

                    //Uniendo la informacion
                    $scope.dirSelNav = {ideFil: 0, nomFil: "", idePadFil: 0, nomPad: "", urlPropia: "", fecMod: new Date(), urlAlm: ""};

                    //Los ides tanto del directorio movido como del directorio a donde se movera
                    $scope.dirSelNav.ideFil = $scope.directSelected.ideFil;
                    $scope.dirSelNav.idePadFil = $scope.directSelectedNav.ideFil;

                    //El nombre del padre que lo contendra tambien ayuda en la construccion de la url
                    $scope.dirSelNav.nomPad = $scope.directSelectedNav.nomFil;

                    //El nombre del directorio actual
                    $scope.dirSelNav.nomFil = $scope.directSelected.nomFil;

                    //La url padre propia del directorio a mover ayudara a formar la nueva url
                    $scope.dirSelNav.urlPropia = $scope.directSelected.urlFil;

                    //La url a almacenar padre
                    $scope.dirSelNav.urlAlm = $scope.directSelectedNav.urlFil;

                    $scope.dirSelNav.flagControlPlazo=$scope.flagControlPlazoEditar;
                    $scope.dirSelNav.flagEliminarPlazoEditar=$scope.flagEliminarPlazoEditar;
                    $scope.dirSelNav.plazoIni=$scope.dateDirIni;
                    $scope.dirSelNav.plazoFin=$scope.dateDirFin;
                    $scope.dirSelNav.usuarioID=$scope.usuMaster.usuario.usuarioID;
                    $scope.dirSelNav.flagProteccionEditar=$scope.flagProteccionEditar;
                    $scope.dirSelNav.proteccion=$scope.directSelected.iteProt;
                    //Nombre del directorio
                    request.setData($scope.dirSelNav);
                    console.log(request);
                    crud.actualizar("/documentosGestion", request, function (response) {
                        modal.mensaje("CONFIRMACION", response.responseMsg);
                        if (response.responseSta) {
                            //actualizando la lista general de documentos solo con la posicion
                            //$scope.tiposDocs[$scope.tipoDocSel.i] = $scope.tipoDocSel;
                            
                            //Actualizando el item existente con sus nuevos valores
                            var newProperties = {roleId: 0,roleCat: "", roleFec: new Date(), roleName: "", rolePad: 0, roleTam: 0, roleUrl: "", roleVer:0,children:[]};
                            newProperties.roleName = $scope.dirSelNav.nomFil;
                            newProperties.roleId = $scope.dirSelNav.ideFil; //el mismo que ya cuenta id no cambia
                            newProperties.roleTam = 0; //Siempre es cero
                            newProperties.roleCat = "D"; //Los actualizados son directorios
                            newProperties.roleVer = $scope.iteFilSeleccionado.roleVer + 1;
                            newProperties.roleFec = $scope.iteFilSeleccionado.roleFec;
                            newProperties.rolePad = $scope.iteFilSeleccionado.rolePad;
                            if($scope.dirSelNav.ideFil === $scope.dirSelNav.idePadFil){
                                newProperties.roleUrl = $scope.dirSelNav.urlPropia;
                            }else{
                                newProperties.roleUrl = $scope.dirSelNav.urlAlm + "/" + $scope.dirSelNav.nomPad;
                                newProperties.rolePad = $scope.dirSelNav.idePadFil;
                            }
                            newProperties.children = $scope.iteFilSeleccionado.children;
                            
                            //Actualizando el Arbol de Files
                            $scope.actualizarItemArbolDirByIdPad($scope.directorio.codPad,$scope.dirSelNav.ideFil,$scope.dirSelNav.idePadFil,newProperties);
                            
                            $('#modalEditDirect').modal('hide');
                            $scope.fecIniPlaAnt={};
                            $scope.fecFinPlaAnt={};
                            $scope.flagControlPlazoEditar=false;
                            $scope.flagEliminarPlazoEditar=false;
                            $scope.flagProteccionEditar=false;        
                        }
                    }, function (data) {
                        console.info(data);
                    });
//                } else {
//                    modal.mensaje("CONFIRMACIÃ“N", "Es el mismo Directorio");
//                }
            } else {
                modal.mensaje("NOTIFICACION", "Seleccione una carpeta");
            }
              $scope.flagControlPlazoEditar=false;
                            $scope.flagEliminarPlazoEditar=false;
            $scope.flagProteccionEditar=false;
        };
        
        
        //Retornar la posicion del item por el id del archivo en el Arbol de Files
        $scope.buscarPosByIdFil = function(listaHijos, idSelected){
            //Debemos buscar justo el id de hijo que debemos eliminar
            for(var i = 0; i < listaHijos.length; i++){
                if(listaHijos[i].roleId === idSelected){
                    return i;
                }
            }
            return -1;
        };
        
        
        //Quita del Arbol de Files un item por el ide del Padre
        $scope.eliminarItemArbolDirByIdPad = function(idPadre,idSelected){
            $scope.itemArbol = $scope.buscarItemArbolById($scope.rolelist[0],idPadre);
            var posHijo = $scope.buscarPosByIdFil($scope.itemArbol.children, idSelected);
            //Removera 1 item en la posicion de idSelected
            if(posHijo !== -1){
                $scope.itemArbol.children.splice(posHijo, 1);
            }else{
                console.info("No se pudo quitar directorio del Arbol de Directorio");
            }
        };
            
        /*-------------------------------------------------------------------------
         *                          Eliminar Directorio
         -------------------------------------------------------------------------*/
//        $scope.solicitarPermisoDirec=function(){
//            
//            
//        };
        
        $scope.eliminarDirect = function () {            
            if($scope.iteFilSeleccionado.roleId != 1){
                if ($scope.iteFilSeleccionado.rolePro === "0") {
                    if ($scope.iteFilSelected.roleCat === "D") {
                        //Eliminacion de Directorio y todo su contenido pero solo desactivar
                        modal.mensajeConfirmacion($scope, "Esta seguro de eliminar el directorio", function () {
                            var request = crud.crearRequest('entornoInicio', 1, 'eliminarDirectorio');
                            request.setData({idDir: $scope.documento.coddoc});
                            crud.eliminar("/documentosGestion", request, function (response) {
                                modal.mensaje("CONFIRMACION", response.responseMsg);
                                if (response.responseSta) {                                  
                                    $scope.tablaRepositorio.reload();
                                    $scope.eliminarItemArbolDirByIdPad($scope.directorio.codPad, $scope.documento.coddoc);
                                }
                            }, function (data) {

                            });
                        });
                    } else {
                        modal.mensaje("NOTIFICACION", "La seleccion actual es un archivo porfavor seleccione un directorio");
                    }
                } else {
                    modal.mensaje("NOTIFICACION", "El directorio esta protegido");
                    //Debe activarse un formulario de solicitud de codigo
                    $scope.setcheckb.solPerm = false;
                    $('#modalSolPermDir').modal('show');
                }                
            }else{
                modal.mensaje("MENSAJE","La carpeta seleccionada no puede eliminarse");
            }
        };
        $scope.verificarCodigoProteccion=function(codProt){                        
            if($scope.directSelected.iteProt == codProt){
                var request = crud.crearRequest('entornoInicio', 1, 'eliminarDirectorio');
                request.setData({idDir: $scope.documento.coddoc});
                crud.eliminar("/documentosGestion", request, function (response) {
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                    if (response.responseSta) {                                  
                        $scope.tablaRepositorio.reload();
                        $scope.eliminarItemArbolDirByIdPad($scope.directorio.codPad, $scope.documento.coddoc);
                    }                    
                }, function (data) {

                });
                
            }else{
                modal.mensaje("ERROR","Codigo incorrecto");
            }
            $('#modalSolPermDir').modal('hide');
            $scope.dataAutElim.codProt="";
        }
        $scope.verificarCodigoProteccionArchivo=function(codProt){  
            
            if($scope.docTemporal.iteProt == codProt){
               //Pasar todos los parametros de docges a docSel
                $scope.docSel = JSON.parse(JSON.stringify($scope.docTemporal));
                //Eliminacion de Documento pero solo desactivar
                $('#modalSolPermDoc').modal('hide');
                 modal.mensajeConfirmacion($scope, "Esta seguro de eliminar el documento", function () {
                    var request = crud.crearRequest('entornoInicio', 1, 'eliminarDocumento');
                    request.setData({idDoc: $scope.docTemporal.ideFil});
                    crud.eliminar("/documentosGestion", request, function (response) {
                        modal.mensaje("CONFIRMACION", response.responseMsg);
                        if (response.responseSta) {
                            eliminarElemento(settingRepositorio.dataset,$scope.tempi);
                            $scope.tablaRepositorio.reload();
                            console.info("Se elimino el archivo exitoxamente");
                        }
                    }, function (data) {

                    });
                });
            }else{
                modal.mensaje("ERROR","Codigo incorrecto");
                $('#modalSolPermDoc').modal('hide');
            }
            
            $scope.dataAutElim.codProt="";
        }
        
        
        //Setea los parametros de navegacion de directorios para la edicion de directorios
        $scope.prepararEdicionDir = function(){
            if($scope.iteFilSeleccionado.roleId != 1){
                $scope.contDirActNav = $scope.contDirAct;
                console.log($scope.directSelected);
                if($scope.directSelected.plazoIni){
                    $scope.dateDirIni=new Date($scope.directSelected.plazoIni);
                    $scope.dateDirFin=new Date($scope.directSelected.plazoFin);
                    $scope.fecIniPlaAnt=$scope.dateDirIni;
                    $scope.fecFinPlaAnt=$scope.dateDirFin;
                    $scope.mensajePlazoEditar="Editar Plazo";
                }else{
                    $scope.dateDirIni=new Date();
                    $scope.dateDirFin=new Date();                
                    $scope.mensajePlazoEditar="Agregar Plazo";
                }
                $scope.directSelectedNav = $scope.directSelected;
                $scope.nomDirAnt = $scope.directSelected.nomFil;

                if ($scope.iteFilSelected.roleCat === "D") {
                    $('#modalEditDirect').modal('show'); 
                }else{
                    modal.mensaje("NOTIFICACION", "La seleccion actual es un archivo porfavor seleccione un directorio");
                }
            
            }else{
                modal.mensaje("MENSAJE", "La carpeta seleccionada no puede ser editada");
            }            
        };
        
        
        /*-------------------------------------------------------------------------
         *                     Mostrar Contenido de Directorio
         -------------------------------------------------------------------------*/
        $scope.mostrarContenidoDir = function (codPadre, flagNaveg, tipoFile) {
            if (tipoFile==="D") {
                if (codPadre !== 0) {                    
                    var request = crud.crearRequest('entornoInicio',1,'mostrarContentDir');
                    request.setData({roleId: codPadre,usuarioID:$scope.usuMaster.usuario.usuarioID, organizacionID:$scope.usuMaster.organizacion.organizacionID, flag:$scope.vistaUgel});
                    crud.listar("/documentosGestion", request, function (data) {
                        //colocar la data de la base a la variable del ambito
                        //los hijos del directorio actual               
                        if (!flagNaveg) {
                            $scope.contDirAct = data.data.datosHijos;
                            $scope.directSelected = data.data.datosPadre;
                        } else {
                            $scope.contDirActNav = data.data.datosHijos;
                            $scope.directSelectedNav = data.data.datosPadre;
                        }
                        if (data.data.datosHijos.length !== 0) 
                            settingRepositorio.dataset = data.data.datosHijos;
                        else {
                            settingRepositorio.dataset = [];
                            settingRepositorio.dataset[0]=data.data.datosPadre;
                        }
                        $scope.tablaRepositorio.settings(settingRepositorio);
                        $scope.docSelect = {index:"-1"};
              
                    }, function () {
                        //console.info(data);
                    });
                }
            }
        };

        //Preparar las variables de region, ugel e institucion 
        $scope.rellenarCamposEntidades = function () {
            //Primera Capa
            for (var i = 0; i < $scope.entidades.length; i++) {
                var objRegion = {'idRegion': "", 'nomRegion': ""};
                objRegion.idRegion = $scope.entidades[i].roleId;
                objRegion.nomRegion = $scope.entidades[i].roleName;
                $scope.regiones.push(objRegion);
            }

            var listUgeles = $scope.entidades[0].children;
            var tamUgeles = $scope.entidades[0].children.length;
            
            //Segunda Capa            
            for (var i = 0; i < tamUgeles; i++) {
                var objUgel = {'idUgel': "", 'nomUgel': ""};
                objUgel.idUgel = listUgeles[i].roleId;
                objUgel.nomUgel = listUgeles[i].roleName;
                $scope.ugeles.push(objUgel);
            }
        };
        
        //Funcion que lista los tipos de documentos especializados (PAT,PEI,POI,PEI,RI,PCC,etc)
        $scope.levantarTiposEsp = function () {
            var request = crud.crearRequest('entornoInicio', 1, 'listarTiposEsp');            
            crud.listar("/documentosGestion", request, function (data) {
                //colocar la data de la base a la variable del ambito
                console.info(data.data);
                $scope.tipesps = data.data;
            }, function (data) {
                //console.info(data);
            });
        };
        
        //Funcion que construye la estructura de entidades desde region, ugel hasta la institucion
        $scope.levantarEntidades = function () {
            var request = crud.crearRequest('entornoInicio', 1, 'listarEntidades');
            crud.listar("/documentosGestion", request, function (data) {
                //colocar la data de la base a la variable del ambito
                console.info(data.data);
                $scope.entidades = data.data;
                //Con las entidades ya dispuestas pasamos a rellenar los campos
                $scope.rellenarCamposEntidades();
            }, function (data) {
                //console.info(data);
            });
        };
        
        //Apertura un nuevo Directorio 
        $scope.prepararNuevoDir = function(){
            /*if ($scope.iteFilSelected.roleCat === "D") {*/
                $('#modalNuevoDirect').modal('show'); 
            /*} else {
                modal.mensaje("NOTIFICACION", "El Directorio Padre no puede ser un archivo porfavor seleeccione un directorio donde almacenar");
            }*/
        };
        
        //Levanta los datos necesarios para el formulario de nuevo Archivo
        $scope.levantarDatosModalNuevoFile = function () {
            //Funcion para levantar los tipos de documentos especializados
            if ($scope.tipesps.length === 0 && $scope.regiones.length === 0 && $scope.ugeles.length === 0) {
                $scope.levantarTiposEsp();
                $scope.levantarEntidades();
            }
        };
        
        //Limpia los campos relacionados a las entidades
        $scope.limpiarData = function(){
            //Limpiando las variables de seleccion
            $scope.selUbiEnt.Ins = 0;
            $scope.institucionDefecto = "";
            $scope.selUbiEnt.Uge = 0;
            $scope.selUbiEnt.Reg = 0;
            $scope.selUbiEnt.TipDoc=0;
            $scope.documento.desdoc="";
            $scope.documento.archivo="Ninguno";
        };
        
        //Evento que apertura las instituciones cuando se selecciona una Ugel
        $scope.seleccionUgel = function (idUgel) {
            //Primero limpiamos
            $scope.insts = [];
            //Buscando en Segunda Capa
            var ugelElegida;
            var listUgeles = $scope.entidades[0].children;
            var tamUgeles = $scope.entidades[0].children.length;
            for (var i = 0; i < tamUgeles; i++) {
                var num = listUgeles[i].roleId + "";
                //Tratar de mantener un solo formato al num
                if (num === idUgel || listUgeles[i].roleId === idUgel) {
                    ugelElegida = listUgeles[i];
                    break;
                }
            }
            
            //Tercera Capa
            var listInsts = ugelElegida.children;
            var numInsts = ugelElegida.children.length;

            for (var i = 0; i < numInsts; i++) {
                var objInst = {'idInst': "", 'nomInst': ""};
                objInst.idInst = listInsts[i].roleId;
                objInst.nomInst = listInsts[i].roleName;
                $scope.insts.push(objInst);
            }
            console.info($scope.insts);
        };

        //Funcion de comparacion para la busqueda de instituciones educativas
        $scope.localBusquedaInst = function (str, insts) {
            var matches = [];
            console.info(insts);
            insts.forEach(function (insti) {
                if (insti.nomInst.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0) {
                    matches.push(insti);
                }
            });
            return matches;
        };
        
        //Setea la institucion seleccionada por el usuario
        $scope.setInstitucion = function (selected) {
            if (selected) {
                console.info(selected.originalObject);
                $scope.selUbiEnt.Ins = selected.originalObject.idInst;
            } else {
                console.info(":(");
            }
        };
        
        //Activa y Desactiva el control de plazos durante la creacion de directorios
        $scope.cargarValsPlazos = function () {
            if ($scope.flagControlPlazo) {
                $scope.labelCndPlazo = "Con Control Plazo";
            } else {
                $scope.labelCndPlazo = "Sin Control Plazo";
            }
        };

        //Activa la seleccion en el Arbol de Files
        $scope.showSelected = function (sel) {
            $scope.temp_node_selected=sel;
            $scope.selected = sel.roleId;
            $scope.iteFilSeleccionado = sel;
            //Compartiendo variable entre directorio y documento
            //ambos se rescatan la misma data
            $scope.iteFilSelected.roleId = sel.roleId;
            $scope.iteFilSelected.roleName = sel.roleName;
            $scope.iteFilSelected.roleUrl = sel.roleUrl;
            $scope.iteFilSelected.roleCat = sel.roleCat;
            $scope.iteFilSelected.roleVer = sel.roleVer;
            //podemos conseguir mas datos aqui...

            //Archivos
            $scope.documento.urldoc = sel.roleUrl;
            $scope.documento.coddoc = sel.roleId;

            //Directorio
            $scope.directorio.codDir = sel.roleId;
            if(sel.roleUrl == sel.roleName)
                $scope.directorio.urlDir = sel.roleUrl
            else
                $scope.directorio.urlDir = sel.roleUrl + "/"+sel.roleName;
            $scope.directorio.codPad = sel.rolePad;
            $scope.directorio.nombrePadre=sel.roleName;
            
            if ($scope.dirPar.roleId !== -1) {
                $scope.mostrarContenidoDir(sel.roleId,false,sel.roleCat);
            }
        };

        //Variables para los filtros
        $scope.reverse = true;
        $scope.orderby = 'roleName';
        $scope.byLabel = function () {
            $scope.orderby = 'roleName';
        };
        $scope.predicate = "";
        $scope.comparator = false;
        
        //Busca Item con posicion en SubArbol de File por el ID (No Recursivo)
        $scope.buscarItemByIdFilInSubArbolDir = function(listaHijos, idSelected){
            //Debemos buscar justo el id de hijo que debemos eliminar
            var itemEncontrado = {objItem:"",posItem:0};
            for(var i = 0; i < listaHijos.length; i++){
                if(listaHijos[i].roleId === idSelected){
                    itemEncontrado.objItem = listaHijos[i];
                    itemEncontrado.posItem = i;
                    return itemEncontrado; // En caso de que encontro el hijo
                }
            }
            return itemEncontrado; // En caso de que no exista el hijo buscado
        };
        
        
        //Actualiza el Arbol de File por el padre con nuevas propiedades y nuevo padre
        $scope.actualizarItemCodProt = function(idPadreDir,idDirActual,codProt){
            //Haremos 2 busquedas una para el padre del directorio y otra
            //para buscar el directorio donde reubicar
            var itemPadre = $scope.buscarItemArbolById($scope.rolelist[0],idPadreDir);
            //En este padre debemos encontrar tanto el id como el propio objeto
            //del directorio seleccionado por el id tomaremos como ejemplo
            var dirActualTmp = $scope.buscarItemByIdFilInSubArbolDir (itemPadre.children,idDirActual);
            var dirActual = dirActualTmp.objItem;
            dirActual.rolePro = codProt;
            var posItem = dirActualTmp.posItem;
            //Eliminando el objeto antiguo y colocando el nuevo objeto en su posicion
            //itemPadre.children.splice(posItem,1,dirActual);
        };
        /*-------------------------------------------------------------------------
         *                 Proteccion de Directorios con codigo
         -------------------------------------------------------------------------*/
        $scope.editarProteccion = function(){
            //Guardado de Directorio
            var request = crud.crearRequest('entornoInicio', 1, 'protegerDocumento');
            //insertando resultando del contenido del archivo
           
            $scope.dataProt.idUsuario = $scope.usuMaster.usuario.ID;
            $scope.dataProt.codProt=$scope.oldProteccion;
            request.setData($scope.dataProt);
            //caso de recibir un codigo de usuario
            if ($scope.dataProt.codProt.length!=5) {
               modal.mensaje("CONFIRMACION", "El codigo de proteccion debe tener 5 digitos");
            } else {                
                crud.insertar("/documentosGestion", request, function (response) {
                modal.mensaje("CONFIRMACION HECHA", response.responseMsg);
                console.info("Proteccion Realizada");
                if (response.responseSta) {
                    $scope.dataProt = {idUsuario: -1, codProt: ""};
                    $('#modalProtegerFile').modal('hide');
                    $scope.showSelected($scope.temp_node_selected);
                    console.info(response.data.codProt);
                    }
                }, function (data) {
                    //Codigo de Proteccion Fallida
                });
            }
            $('#modalProtegerFile').modal('hide');
        };
        $scope.quitarProteccion=function(){
            var request = crud.crearRequest('entornoInicio', 1, 'protegerDocumento');
            //insertando resultando del contenido del archivo

            $scope.dataProt.idUsuario = $scope.usuMaster.usuario.ID;
            $scope.dataProt.codProt="0";
            request.setData($scope.dataProt);
            crud.insertar("/documentosGestion", request, function (response) {
                modal.mensaje("CONFIRMACION HECHA", response.responseMsg);
                if (response.responseSta) {
                    $scope.dataProt = {idUsuario: -1, codProt: ""};
                    $scope.showSelected($scope.temp_node_selected);
                    }
                }, function (data) {
                    //Codigo de Proteccion Fallida
                });
                 $('#modalProtegerFile').modal('hide');
        };
        $scope.peticionCodigo = function(){
//            if($scope.setcheckb.solPerm === false){
//                //ocultar el formulario principal
//                $('#modalSolPerm').modal('hide');
//                //preparar datos
//                $scope.varsSolPerm.nomDoc = $scope.iteFilSeleccionado.roleName;
//                if($scope.iteFilSeleccionado.roleCat === "D"){
//                    $scope.varsSolPerm.tipDoc = "Directorio";
//                }else{
//                    $scope.varsSolPerm.tipDoc = "Archivo";
//                }
//                
//                //mostrar nuevo formulario
//                $('#modalPetCodAut').modal('show');
//            }
          
        };
        
        //Solicitar Permisos de Autorizacion
        $scope.solicitarPermisosEliminacion = function () {
//            $scope.dataAutElim.ideUsuario = $scope.usuMaster.usuario.ID;
//            $scope.dataAutElim.coddoc = $scope.iteFilSeleccionado.roleId;
//            if ($scope.dataAutElim.dniUsuario !== "" && $scope.dataAutElim.codProt !== "") {
//                var request = crud.crearRequest('entornoInicio', 1, 'verificarCodeEliminacion');
//                request.setData($scope.dataAutElim);
//                crud.listar("/documentosGestion", request, function (data) {
//                    //colocar la data de la base a la variable del ambito
//                    if(data.data.codProtRes === $scope.dataAutElim.codProt){
//                        modal.mensaje("CONFIRMACIÃ“N", "El archivo ha sido desbloqueado y eliminado correctamente");
//                        //Actualizamos el Arbol al eliminar el directorio con codigo coddoc
//                        
//                        $scope.eliminarItemArbolDirByIdPad($scope.directorio.codPad, $scope.documento.coddoc);
//                        $('#modalSolPerm').modal('hide');
//                    }else{
//                        modal.mensaje("CONFIRMACIÃ“N", "No se pudo desbloquear el archivo");
//                    }
//                }, function (data) {
//                    //console.info(data);
//                });
//            }
        };

        $scope.autorizarEliminacion = function(){
            if($scope.varsSolPerm.dniUsu!==""){
                modal.mensaje("CONFIRMACION", "Su peticion ha sido Enviada");
                $('#modalPetCodAut').modal('hide');
            }
            else
                modal.mensaje("Notificacion", "Ingrese su DNI porfavor");
        };
        
        //Menu de opciones para el menu contextual
        $scope.menuOptions = [
            ['Proteger Archivo', function ($itemScope) {
                    if($itemScope.docges.autoria=='Propio'){
                        if($itemScope.docges.iteProt=="0"){                        
                          $scope.flagDesproteger=false;
                        }else{//Ya esta protegido
                              $scope.flagDesproteger=true;   
                        }
                        //Implementar que es lo que hara la proteccion de archivos
                          $scope.dataProt.ideFil  = $itemScope.docges.ideFil;
      //                    $scope.docSelectedByMenu = $itemScope.docges;
                          $scope.oldProteccion=$itemScope.docges.iteProt;
                          $('#modalProtegerFile').modal('show');
                    }else{
                            modal.mensaje("MENSAJE", "El archivo seleccionado no es de su propiedad")
                    }
                }]
//            null,
//            ['Mas...', [
//                    ['Agregar Archivo', function ($itemScope) {
//                            $scope.docGesInst.docAct=$itemScope.docges.nomFil;
//                             $('#modalNuevoDocGestionInstitucional').modal('show'); 
//                        }],
//                    ['Eliminar Archivo', function ($itemScope) {
//                            //alert($itemScope.docges.nomFil);
//                            $scope.eliminarDoc($itemScope.index,$itemScope.docges);
//                            
//                        }],
//                    ['Editar Archivo', function ($itemScope) {
//                            //alert($itemScope.docges.nomFil);
//                            $scope.prepararEditarDoc($itemScope.index,$itemScope.docges)
//                        }]
//                ]]
        ];
        
        $scope.setDatosToReporte = function(listEtiVal){
            for(var i = 0; i<listEtiVal.length;i++){
                $scope.labels2.push(listEtiVal[i].etiqueta);
                $scope.data2.push(listEtiVal[i].valor);
            }
        };
        
        $scope.preparaReporteGeneral = function () {
            //limpiamos anterior.
                    //Variables para el reporte
        $scope.labels2 = [];
        $scope.data2 = [];
            
            //Prepara datos de los archivos en en tipo de archivo y su arbol
            var request = crud.crearRequest('entornoInicio', 1, 'construirReporteGeneral');

            
            request.setData({usuCod:$scope.usuMaster.usuario.usuarioID, organizacionID:$scope.usuMaster.organizacion.organizacionID , flagUgel:$scope.vistaUgel});
            crud.listar("/documentosGestion", request, function (data) {
                console.log(data.data);                
                if(data.data.length !=0 ){
                    for(var i=0;i<data.data.length;i++){
                        $scope.labels2.push(data.data[i].cabecera);
                        $scope.data2.push(data.data[i].valor);
                    }
                    $('#modalRepDocs').modal('show');
                }else{
                    modal.mensaje("MENSAJE","No existe algÃºn reporte");
                }                
            }, function (data) {
                //console.info(data);
            });
        };
        
        
        //MI PARTE
        $scope.imprimir = function(){
            var grafico= new MyFile("Reporte Cantidad de Documentos de GestiÃ³n");
            grafico.parseDataURL(document.getElementById("reporteRDG").toDataURL("image/png") );
            grafico.size=0;
            var request=crud.crearRequest('entornoInicio',1,'reporte');
//            request.setData(grafico);
            request.setData({grafico: grafico, orgID:$scope.usuMaster.organizacion.organizacionID, data: $scope.data2, etiquetas:$scope.labels2 , usuID: $scope.usuMaster.usuario.usuarioID });
            crud.insertar("/documentosGestion", request,function(data){
                if(data.responseSta){
                    verDocumentoPestana(data.data.reporte);
                }
            },function(data){
                console.info(data);
            });
        };
        $scope.cambiarNombreNuevoDocumento=function(){
            $scope.selUbiEnt.TipDoc;
            $scope.documento.desdoc=$scope.tipesps[$scope.selUbiEnt.TipDoc - 1].abr + "-" +
                    new Date().getFullYear() + "-" +
                    $scope.usuMaster.organizacion.nombre;
        }
        $scope.cambiarNombreEditarDocumento=function(){
            $scope.docSel.nomFil=$scope.tipesps[$scope.selUbiEnt.TipDoc ].abr + "-" +
                    new Date().getFullYear() + "-" +
                    $scope.usuMaster.organizacion.nombre;
        }
        $scope.prepararAdjuntarDocumentoGestionInstitucional=function(i,doc){
           $scope.docSel = JSON.parse(JSON.stringify(doc));
           $scope.docSel.i = i;
            
            $('#modalNuevoDocGestionInstitucional').modal('show'); 
            
        };
        $scope.borrarPlazoEditar=function(){
            $scope.verEditarFechaDir=true;            
        };
        $scope.habilitarPlazoEditar=function(){
            $scope.verEditarFechaDir=false;
        };
        $scope.open1 = function() {
        $scope.popup1.opened = true;
        };
      $scope.open2 = function() {
        $scope.popup2.opened = true;
        };
    $scope.popup1 = {
        opened: false
    };
        $scope.popup2 = {
        opened: false
    };
$scope.clear = function() {
    $scope.dateDirIni = null;
    $scope.dateDirFin=null;
  };

  $scope.dateOptions = {
    dateDisabled: disabled,
    formatYear: 'yy',
    minDate: new Date(),
    startingDay: 1
  };

  

    // Disable weekend selection
  function disabled(data) {
    var date = data.date,
      mode = data.mode;
    return mode === 'day' && (date.getDay() === 0 );
  }
        
        /*var canvas = document.getElementById("line");
        var img = document.getElementById("laimagen");
        
        //Conseguir el campo de la imagen
        var png = document.getElementById("png");
        png.addEventListener("click", function () {
            img.src = canvas.toDataURL("image/png");
        }, false);
        */
        $scope.colors = ['#45b7cd', '#ff6384', '#ff8e72'];

        $scope.datasetOverride2 = {
            hoverBackgroundColor: ['#45b7cd', '#ff6384', '#ff8e72'],
            hoverBorderColor: ['#45b7cd', '#ff6384', '#ff8e72']
        };
    }]);
