app.controller("horarioEscolarCtrl",["$rootScope", "$scope","crud","modal", function ($rootScope, $scope,crud,modal){
        
    $scope.plan = {};
    $scope.jornadaSel = {};
    $scope.gradoSel = {};
    
    var ORG_ID = $rootScope.usuMaster.organizacion.organizacionID;
    
    $scope.horario = {horas:[],hora:new Date("2016-01-01 07:00:00")};
        
    //$scope.fechaFinal = ;
    //$scope.hora = new Date( convertirFecha2( new Date())+"T11:00:00Z");
//    $scope.aulas = [{aulaID:1,nombre:"aula 1",disponible:false},{aulaID:2,nombre:"aula 2",disponible:false},
//                    {aulaID:3,nombre:"aula 3",disponible:false},{aulaID:4,nombre:"aula 4",disponible:false},
//                    {aulaID:5,nombre:"aula 5",disponible:false}];

    $scope.aulas = [];
                
    $scope.dias = [{diaID:'L',nombre:"Lunes"},{diaID:'M',nombre:"Martes"},{diaID:'W',nombre:"Miercoles"},
                    {diaID:'J',nombre:"Jueves"},{diaID:'V',nombre:"Viernes"},{diaID:'S',nombre:"Sabado"}];
    
    $scope.buscarHorario = function(){
        
        if($scope.seccionSel && $scope.seccionSel!=""){
            var request = crud.crearRequest('horario',1,'buscarHorario');
            request.setData({planNivelID:$scope.jornadaSel.planNivelID,gradoID:$scope.gradoSel.gradoID,seccionID:$scope.seccionSel});
            crud.listar("/cuadroHoras",request,function(response){
                if(response.responseSta){
                    $scope.areas = areas;
                    $scope.horario = response.data;
                    $scope.horario.hora = new Date( "2016-01-01 "+response.data.horaInicio );
                    $scope.cambiarInicio();
                }
                else{
                    reiniciarHorario();
                }
            },function(data){
                console.info(data);
            });
        }
    };    
    $scope.calcularGrados = function(jornada){
        
        var disenoActual = $scope.disenoActual;
        if(jornada && disenoActual.grados ){
            var nivelID = jornada.nivelID;
            
            var grados = [];
            var metas = 0;
            disenoActual.grados.forEach(function(item){

                if( nivelID == item.nivelID ){                    
                    
                    //buscando las metas de atencion
                    var meta = buscarObjeto(jornada.metas,'gradoID',item.gradoID);
                    if(meta)
                        item.meta = meta.secciones;
                    else
                        item.meta = 0;
                    
                    metas += item.meta;
                    
                    item.totalH = 0;
                    item.totalT = 0;
                    insertarElemento(grados,item);
                }
            });

            $scope.grados = grados;
            $scope.gradoSel = '';
            $scope.seccionSel = '';
            reiniciarHorario();
        }
    
    };
    function reiniciarHorario(){
        $scope.horario = {horas:[],hora:new Date("2016-01-01 07:00:00")};
        $scope.areas = [];
    };
    var areas =[];
    $scope.calcularSecciones = function(grado){
        
        if(grado){
            $scope.seccionSel = '';
            reiniciarHorario();
            areas = [];
            grado.secciones = [];
            var l = 65;        
            for(var i=0;i<grado.meta;i++)
                grado.secciones.push({seccionID:String.fromCharCode(l+i)});
            
            var disenoActual = $scope.disenoActual;
            var gradosAreas = buscarObjetos(disenoActual.gradoAreas,'gradoID',grado.gradoID);
            
            disenoActual.areas.forEach(function (item){
                var area = buscarObjeto(gradosAreas,'areaID',item.areaID);
                if(area ){                    
                    item.hora = 0;
                    item.horaT = 0;
                    insertarElemento(areas,item);
                }
            });
            disenoActual.talleres.forEach(function (item){
                var area = buscarObjeto($scope.jornadaSel.horasDisponibles,'areaID',item.areaID);
                if(area ){
                    item.hora = 0;
                    item.horaT = 0;
                    insertarElemento(areas,item);
                }
            });
            
            //agregando tutoria como un area curricular
            var areaTut = {};
            areaTut.nombre = "tutoria";            
            areaTut.horaT = $scope.jornadaSel.hTutoria;
            areaTut.hora = areaTut.horaT;
            areaTut.areaID = -1;
            insertarElemento(areas,areaTut);
            //agregando tutoria como un area curricular-- FIN
            
            //obteniendo las horas de libre disponibilidad
            var horasDisponibles = buscarObjetos($scope.jornadaSel.horasDisponibles,'gradoID',grado.gradoID);
            horasDisponibles.forEach(function(item){
                var area = buscarObjeto(disenoActual.areas,"areaID",item.areaID);
                if(area){
                    area.horaT += item.horaL;
                    area.hora = area.horaT;
                }
            });            
            //obteniendo las horas establecidas por el plan de estudios
            var jorSel = buscarObjeto($scope.jornadas,'jornadaID',$scope.jornadaSel.jornadaID);
            var horas = buscarObjetos(jorSel.horas,'gradoID',grado.gradoID);
            horas.forEach(function(item){
                var area = buscarObjeto(areas,"areaID",item.areaID);
                if(area){
                    area.horaT += item.hora;
                    area.hora = area.horaT;
                }                
            });
            
            grado.plazas = [];
            grado.plazas = buscarObjetos($scope.plan.distribucion,'gradoID',grado.gradoID);
            //grado.plazasHoraria = $scope.plan.distribucionHoraria;//buscarObjetos($scope.plan.distribucionHoraria,'gradoID',grado.gradoID);
        }
    };
    
    $scope.crearHorario = function(){
        
        var nuevoHorario = {planNivelID:$scope.jornadaSel.planNivelID,gradoID:$scope.gradoSel.gradoID,seccionID:$scope.seccionSel,horaInicio:convertirHora($scope.horario.hora)};
                
        /*
        if(!$scope.tipoTramite.tupa)
            $scope.tipoTramite.tipoOrganizacionID = orgID;*/
        
        var request = crud.crearRequest('horario',1,'insertarHorario');
        request.setData(nuevoHorario);
        
        crud.insertar("/cuadroHoras",request,function(response){
            
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                $scope.areas = areas;
                $scope.horario.horarioID = response.data.horarioID;
                //$scope.horario.hora = new Date( "2016-01-01 "+h );
                $scope.cambiarInicio();
            }            
        },function(data){
            console.info(data);
        });
              
    };
    $scope.cambiarInicio = function(){
        
        if($scope.horario.horarioID){
        
            var horaI = $scope.horario.hora.getHours();
            var minutoI = $scope.horario.hora.getMinutes();
            var formatoI = (horaI>9?horaI:"0"+horaI)+":"+(minutoI>9?minutoI:"0"+minutoI);
            $scope.horario.horas = [];
            var rango = 45;
            for(var i =0;i<12;i++){
                var hora = {horaI:horaI,minutoI:0,horaF:horaI+Math.trunc(rango/100),minutoF: minutoI + rango%100,rango:rango};
                hora.horaA = formatoI;
                //acarreo
                if(hora.minutoF/60>=1){
                    hora.horaF++;
                    hora.minutoF-= 60;
                }
                hora.hora = ""+(hora.horaF>9?hora.horaF:"0"+hora.horaF)+":"+ (hora.minutoF>9?hora.minutoF:"0"+hora.minutoF);
                
                //viendo los dias de la semana
                hora.dias = [];
                $scope.dias.forEach(function(item){
                    hora.dias.push({horarioDetalleID:0,horaInicio:hora.horaA,horaFin:hora.hora,horarioID:$scope.horario.horarioID,plazaID:0,diaID:item.diaID,dia:item.nombre,areaID:0,area:"libre",aulaID:0,ocupado:false});
                });
                
                $scope.horario.horas.push(hora);

                horaI = hora.horaF;
                minutoI = hora.minutoF;
                formatoI = hora.hora;
            }
            $scope.areas.forEach(function (item){
                item.hora = item.horaT;
            });
            if($scope.horario.detalle)
                $scope.horario.detalle.forEach(function(item){

                    var area = buscarObjeto( $scope.areas ,"areaID",item.areaID);
                    area.hora -= 1;

                    var hora = buscarObjeto( $scope.horario.horas ,"hora",item.horaFin.substr(0,5));

                    var diahora = buscarObjeto( hora.dias ,"diaID",item.diaID);
                    diahora.plazaID = ""+item.plazaID;
                    diahora.aulaID = item.aulaID;
                    diahora.areaID = ""+item.areaID;                
                    diahora.area = area.nombre;
                    diahora.ocupado = true;
                    diahora.horarioDetalleID = item.horarioDetalleID;
                });
        }
    };
    $scope.cambiarHora = function(posi,hora){
        
        var horaI = hora.horaF;
        var minI = hora.minutoF;
        var formatoI = hora.hora;
        for(var i =posi+1;i<12;i++){
            //copiando la hora anterior
            $scope.horario.horas[i].horaI = horaI;
            $scope.horario.horas[i].minutoI = minI;
            $scope.horario.horas[i].horaA = formatoI;
            
            //calculando horas y minutos
            $scope.horario.horas[i].horaF = $scope.horario.horas[i].horaI + Math.trunc($scope.horario.horas[i].rango/100) ;
            $scope.horario.horas[i].minutoF = $scope.horario.horas[i].minutoI + $scope.horario.horas[i].rango%100 ;            
            //acarreo
            if($scope.horario.horas[i].minutoF/60>=1){
                $scope.horario.horas[i].horaF++;
                $scope.horario.horas[i].minutoF-= 60;
            }
            
            //formato de la hora
            $scope.horario.horas[i].hora = ""+($scope.horario.horas[i].horaF>9?$scope.horario.horas[i].horaF:"0"+$scope.horario.horas[i].horaF)+":"+ ( $scope.horario.horas[i].minutoF>9?$scope.horario.horas[i].minutoF:"0"+$scope.horario.horas[i].minutoF);
            
            horaI = $scope.horario.horas[i].horaF;
            minI = $scope.horario.horas[i].minutoF;
            formatoI = $scope.horario.horas[i].hora;
             
        }
    };
    $scope.editarHora = function(h,i){
        //si estamso editando
        if(h.edi){
            
            h.copia.rango = (h.copia.horaF - h.copia.horaI)*100 + h.minutoF - h.minutoI;
            h.copia.hora = ""+(h.copia.horaF>9?h.copia.horaF:"0"+h.copia.horaF)+":"+ (h.copia.minutoF>9?h.copia.minutoF:"0"+h.copia.minutoF);
            $scope.horario.horas[i] = h.copia;
            delete h.copia;
            $scope.cambiarHora(i,$scope.horario.horas[i]);
            
            h.edi = false;
        }
        //si queremos editar
        else{
            h.copia = h;
            h.edi = true;
        }
    };
    $scope.prepararAsignacion = function(dia,i,j){               
        
        $scope.diaSel = JSON.parse(JSON.stringify(dia));
        $scope.diaSel.j = j;
        $scope.diaSel.i = i;
        
        $scope.listarAmbientesDisponibles(ORG_ID, $scope.diaSel.diaID, $scope.diaSel.horaInicio, $scope.diaSel.horaFin, $scope.diaSel.aulaID);
        
        $scope.plazasSel = [];
        var ocupados = [];
        var disponibles = [];
        $scope.plan.distribucionHoraria.forEach(function(item){
            //console.log(item);
            //var horas = buscarObjetosD( item.detalle ,"horaFin",$scope.diaSel.horaFin+":00");
            
            var entro = false;
            var lista = item.dias;
            
            for(var i=0;i<lista.length;i++ ){
                //buscando le dia
                if(lista[i].dia == $scope.diaSel.diaID ){
                    var intervalos = lista[i].intervalos;
                    for(var j=0;j<intervalos.length;j++ ){
                        if(intervalos[j].horaFin == $scope.diaSel.horaFin+":00"){
                            entro = true;
                            break;                            
                        }
                    }
                    if(entro){
                        ocupados.push(item);
                        break;
                    }
                }
            }
            if(!entro && item.gradoID == $scope.gradoSel.gradoID){
                disponibles.push(item);
            }
        });        
        $scope.gradoSel.plazas.forEach(function (item){
            var gra = buscarObjeto(ocupados,"plazaID",item.plazaID);
            
            //los que estan ocupando un intervalo
            if(gra){
                //si es la plaza que esta ocupando
                if($scope.diaSel.plazaID == item.plazaID){
                    gra.hora = item.hora;
                    gra.horaDis = item.hora - gra.horasAsignadas;
                    $scope.plazasSel.push(gra);
                }
            }
            //los que NO estan ocupando un intervalo
            else{
                
                var gra2 = buscarObjeto(disponibles,"plazaID",item.plazaID);
                
                if(gra2 ){                    
                    gra2.hora = item.hora;
                    gra2.horaDis = item.hora - gra2.horasAsignadas;
                    $scope.plazasSel.push(gra2);
                }
                else{
                
                    var nuevo = {};
                    nuevo.plazaID = item.plazaID
                    nuevo.hora = item.hora;
                    nuevo.horaDis = item.hora;
                    nuevo.docente = item.docente;
                    
                    nuevo.horasAsignadas = 0;
                    $scope.plazasSel.push(nuevo);
                }
            }
        });
        
        console.log($scope.diaSel);
        
        $('#modalHora').modal('show');
    };
    $scope.asignarHora = function(){
        
        var dis = buscarPorPlazaYGrado($scope.diaSel.plazaID,$scope.gradoSel.gradoID);
        
        $scope.diaSel.orgId = ORG_ID;
        /*
        if( dis && dis.horasAsignadas >= dis.hora){
            modal.mensaje("ALERTA","No se puede asignar una hora a esta plaza porque excede de lo establecido");
            return false;
        }*/            
        
        //console.log($scope.plan.distribucionHoraria);
        var request = crud.crearRequest('horario',1,'insertarDetalleHorario');
        request.setData($scope.diaSel);
        
        //console.log(request);

        crud.insertar("/cuadroHoras",request,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                
                $scope.diaSel.horarioDetalleID = response.data.horarioDetalleID;                
                
                
                if($scope.diaSel.ocupado){
                    //si cambio el area asignada
                    //debemos actualizar las horas
                    if($scope.horario.horas[$scope.diaSel.i].dias[$scope.diaSel.j].areaID != $scope.diaSel.areaID){
                        var areaAnterior = buscarObjeto( $scope.areas ,"areaID",Number($scope.horario.horas[$scope.diaSel.i].dias[$scope.diaSel.j].areaID));
                        areaAnterior.hora +=1;
                        
                        var areaNueva = buscarObjeto( $scope.areas ,"areaID",Number($scope.diaSel.areaID));
                        areaNueva.hora -=1;
                        $scope.diaSel.area = areaNueva.nombre;
                    }
                    //si cambio la plaza
                    if($scope.horario.horas[$scope.diaSel.i].dias[$scope.diaSel.j].plazaID != $scope.diaSel.plazaID){
                        var disAnterior = buscarPorPlazaYGrado($scope.horario.horas[$scope.diaSel.i].dias[$scope.diaSel.j].plazaID,$scope.gradoSel.gradoID);
                        quitarIntervalo(disAnterior,$scope.diaSel.diaID,$scope.diaSel.horaFin);
                        
                        añadirIntervalo(dis,$scope.diaSel);
                    }
                    //si cambio el aula
                    if($scope.horario.horas[$scope.diaSel.i].dias[$scope.diaSel.j].aulaID != $scope.diaSel.aulaID){
                        
                    }
                }
                else{
                    var areaNueva = buscarObjeto( $scope.areas ,"areaID",Number($scope.diaSel.areaID));
                    areaNueva.hora -=1;
                    $scope.diaSel.area = areaNueva.nombre;
                    //insertamos un nuevo intervalo
                    añadirIntervalo(dis,$scope.diaSel);
                }
                $scope.diaSel.ocupado= true;
                $scope.horario.horas[$scope.diaSel.i].dias[$scope.diaSel.j] = $scope.diaSel;
                
                $('#modalHora').modal('hide');                
            }
            else{
                //reiniciarHorario();
            }
        },function(data){
            console.info(data);
        });
    };
    $scope.desasignarHora = function(){
        
        var request = crud.crearRequest('horario',1,'eliminarDetalleHorario');
        request.setData({detalleHorarioID:$scope.diaSel.horarioDetalleID});
        crud.eliminar("/cuadroHoras",request,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                
                $scope.horario.horas[$scope.diaSel.i].dias[$scope.diaSel.j] = {horarioDetalleID:0,horaInicio:$scope.diaSel.horaInicio,horaFin:$scope.diaSel.horaFin,horarioID:$scope.horario.horarioID,plazaID:0,diaID:$scope.diaSel.diaID,dia:$scope.diaSel.dia,areaID:0,area:"libre",aulaID:0,ocupado:false};
                              
                var a = buscarObjeto( $scope.areas ,"areaID",Number($scope.diaSel.areaID));
                a.hora +=1;
                
                var distri = buscarPorPlazaYGrado($scope.diaSel.plazaID,$scope.gradoSel.gradoID);
                quitarIntervalo(distri,$scope.diaSel.diaID,$scope.diaSel.horaFin);
                
                $('#modalHora').modal('hide');
            }
            else{
                //reiniciarHorario();
            }
        },function(data){
            console.info(data);
        });
    };
    function buscarPorPlazaYGrado(plaza,grado){
        for(var i =0;i<$scope.plan.distribucionHoraria.length;i++){
            var o = $scope.plan.distribucionHoraria[i];
            if( o.plazaID == plaza && o.gradoID == grado )
                return o;
        }
        return null;
    }
    function quitarIntervalo(distribucion,dia,hora){
        for(var i =0;i<distribucion.dias.length;i++){
            var o = distribucion.dias[i];
            if( o.dia == dia ){
                var intervalos = o.intervalos;
                
                for(var j =0;j<intervalos.length;j++){
                    if(intervalos[j].horaFin == hora+":00"){
                        distribucion.horasAsignadas--;
                        intervalos.splice(j,1);
                        return;
                    }
                }                
            }
        }
    }
    function añadirIntervalo(distribucion,diaSel){
        
        if(!distribucion){
            distribucion = {gradoID:$scope.gradoSel.gradoID,plazaID:Number(diaSel.plazaID),horasAsignadas:0,dias:[]};
            $scope.plan.distribucionHoraria.push(distribucion);
        }
        
        if(!distribucion.dias)
            distribucion.dias = [];
        
        for(var i =0;i<distribucion.dias.length;i++){
            var o = distribucion.dias[i];
            if( o.dia == diaSel.diaID ){
                o.intervalos.push({areaID:Number(diaSel.areaID),aulaID:diaSel.aulaID,horaFin:diaSel.horaFin+":00",plazaID:Number(diaSel.plazaID)});
                distribucion.horasAsignadas++;
                return;              
            }
        }
        distribucion.dias.push({dia:diaSel.diaID,diaID:diaSel.diaID.charCodeAt(0),intervalos:[{areaID:Number(diaSel.areaID),aulaID:diaSel.aulaID,horaFin:diaSel.horaFin+":00",plazaID:Number(diaSel.plazaID)}]});
        distribucion.horasAsignadas++;
        console.log(distribucion);
    }
    
    $scope.buscarPlanEstudios = function(orgID){
        //preparamos un objeto request
        var request = crud.crearRequest('planEstudios',1,'buscarVigente');
        request.setData({organizacionID:orgID});
        crud.listar("/cuadroHoras",request,function(response){
            
            if(response.responseSta){
                $scope.plan = response.data;
                
                request = crud.crearRequest('cuadroHoras',1,'listarDistribucionPlazas');
                request.setData({planID:$scope.plan.planID});
                crud.listar("/cuadroHoras",request,function(res){
                    if(res.responseSta){
                        $scope.plan.distribucion = res.data;
                    }
                },function(data){
                    console.info(data);
                });
                
                request = crud.crearRequest('horario',1,'listarDistribucionHoraria');
                request.setData({planID:$scope.plan.planID});
                crud.listar("/cuadroHoras",request,function(res){
                    if(res.responseSta){
                        $scope.plan.distribucionHoraria = res.data;
                    }
                },function(data){
                    console.info(data);
                });
                
                $scope.plan.niveles.forEach(function(item){
                    var j = buscarObjeto($scope.disenoActual.jornadas,"jornadaID",item.jornadaID);
                    item.jornada = j.nombre;
                    item.hLibre = j.hLibre;
                    item.hObligatoria = j.hObligatoria;
                    item.hTotal = j.hTotal;
                    item.hTutoria = j.hTutoria;
                    item.nivelID = j.nivelID;
                    item.nivel = buscarContenido($scope.disenoActual.niveles,"nivelID","nombre",item.nivelID);
                    item.nivel = item.nivel.toLowerCase();
                });
            }
        },function(data){
            console.info(data);
        });
    };
    $scope.diseoCurricularVigente = function(orgID){
        var request = crud.crearRequest('diseñoCurricular',1,'buscarVigentePorOrganizacion');
        request.setData({organizacionID:orgID});
        crud.listar("/cuadroHoras",request,function(response){
            if(response.responseSta){
                $scope.disenoActual = response.data;
                $scope.disenoActual.talleres = [];
                $scope.disenoActual.areas.forEach(function(item){
                    if(item.tipo)
                        $scope.disenoActual.talleres.push(item);
                });
                $scope.buscarPlanEstudios(orgID);
            }
        },function(data){
            console.info(data);
        });
        
        request = crud.crearRequest('diseñoCurricular',1,'listarJornadaEscolar');
        crud.listar("/cuadroHoras",request,function(response){
            $scope.jornadas = response.data;
        },function(data){
            console.info(data);
        });
    };
    
    
    $scope.listarAmbientesDisponibles = function(orgId, diaSemana, horaInicio, horaFin, aulaId) {
        
        var request = crud.crearRequest('cuadroHoras',1,'obtenerAulasPorOrganizacion');
        request.setData({org_id: orgId, dia_semana: diaSemana, hora_inicio: horaInicio, hora_fin: horaFin, amb_id: aulaId});
        
        console.log(request);
        
        crud.listar("/cuadroHoras",request,function(response){
            if (response.responseSta) {
                $scope.aulas = response.data;
            }
        },function(data){
            console.info(data);
        });
        
    }
}]);
