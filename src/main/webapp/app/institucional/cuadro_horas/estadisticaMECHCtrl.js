app.controller("estadisticaMECHCtrl",["$scope","NgTableParams","crud","modal", function ($scope,NgTableParams,crud,modal){
        
    var params = {count: 10};
    var setting = { counts: []};
    $scope.miTabla = new NgTableParams(params, setting);
    
    $scope.tipoTramites = [];
    $scope.prioridades = [];
    
    var inicio = new Date();
    inicio.setMonth(0);
    inicio.setDate(1);
    $scope.busqueda = { desde: inicio , hasta:new Date("dd/mm/aaaa")};
    
    var params = {count: 10};
    var setting = { counts: []};
    $scope.tabla = new NgTableParams(params, setting);
    
    //metas de atencion
    var metas = [];
    
    $scope.generarEstadistica= function(){
        var datos = [];
        metas.forEach(function(item){
            if( $scope.busqueda.nivelID == item.nivelID  ){
                item.metas.forEach(function(ite){
                    datos.push(ite);
                });                
            }
        });
        
        //var nivel = buscarObjeto(metas,"nivelID",$scope.busqueda.nivelID);
        var grados = buscarObjetos($scope.disenoActual.grados,"nivelID",Number($scope.busqueda.nivelID));
        var labels = [];
        grados.forEach(function(item){
            labels.push(item.abreviacion);
        });
        
        
        var años = [];
        
        var actual = (new Date()).getFullYear();
        
        for(var i = 2016;i<=actual;i++){            
            var añometas = buscarObjetos(datos,"año",""+i);
            var data = [];
            var data2 = [];
            var dataT = 0;
            var data2T = 0;
            grados.forEach(function(item){
                var g = buscarObjeto(añometas,"gradoID",item.gradoID);
                if(g){
                    dataT+=g.meta;
                    data2T+=g.meta2;
                    data.push(g.meta);
                    data2.push(g.meta2);
                }
                else{
                    data.push(0);
                    data2.push(0);
                }
                    
            });            
            años.push({nombre:""+i,data:data,data2:data2,dataT:dataT,data2T:data2T});
        }
        
        $scope.gradosSel = grados;
        $scope.anosSel = años;        
        crearGraficoBarras("metasAtencion",labels,años);
        
    };
    /*
    $scope.generarEstadistica3= function(){        
        //preparamos un objeto request
        var request = crud.crearRequest('expediente',1,'estadisticaResumen');
        request.setData({desde:convertirFecha($scope.busqueda.desde),hasta:convertirFecha($scope.busqueda.hasta)});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/tramiteDocumentario",request,function(response){
            if(!response.responseSta){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                return;
            }
            if(response.data){               
                
                setting.dataset = response.data;
                iniciarPosiciones(setting.dataset);
                $scope.tabla.settings(setting);
                
            }
        },function(data){
            console.info(data);
        });
    };*/
    $scope.buscarPlanEstudios = function(orgID){
        //preparamos un objeto request
        var request = crud.crearRequest('planEstudios',1,'buscarVigente');
        request.setData({organizacionID:orgID});
        crud.listar("/cuadroHoras",request,function(response){
            
            if(response.responseSta){
                $scope.plan = response.data;
                
                request = crud.crearRequest('cuadroHoras',1,'listarDistribucionPlazas');
                request.setData({planID:$scope.plan.planID});
                crud.listar("/cuadroHoras",request,function(res){
                    if(res.responseSta){
                        $scope.plan.distribucion = res.data;
                    }
                },function(data){
                    console.info(data);
                });
                
                $scope.plan.niveles.forEach(function(item){
                    var j = buscarObjeto($scope.disenoActual.jornadas,"jornadaID",item.jornadaID);
                    item.jornada = j.nombre;
                    item.hLibre = j.hLibre;
                    item.hObligatoria = j.hObligatoria;
                    item.hTotal = j.hTotal;
                    item.hTutoria = j.hTutoria;
                    item.nivelID = j.nivelID;
                    item.nivel = buscarContenido($scope.disenoActual.niveles,"nivelID","nombre",item.nivelID);                    
                    item.nivel = item.nivel.toLowerCase();
                });
            }
        },function(data){
            console.info(data);
        });
        
    };
    $scope.generarEstadistica3 = function(jornada){
        
        $scope.matris = {};
        
        
        var disenoActual = $scope.disenoActual;
        if(jornada && disenoActual.grados ){
            
            var nivelID = jornada.nivelID;
            
            
            var grados = [];
            var metas = 0;
            disenoActual.grados.forEach(function(item){

                if( nivelID == item.nivelID ){                    
                    
                    //buscando las metas de atencion
                    var meta = buscarObjeto(jornada.metas,'gradoID',item.gradoID);
                    if(meta)
                        item.meta = meta.secciones;
                    else
                        item.meta = 0;
                    
                    metas += item.meta;
                    
                    item.totalH = 0;
                    item.totalT = 0;
                    item.totalL = 0;
                    insertarElemento(grados,item);
                }
            });
            

            $scope.matris.grados = grados;
            $scope.matris.c = grados.length;
            $scope.matris.f = 0;
            $scope.matris.metas = metas;
            
            $scope.matris.gradoAreas = [];
            
            $scope.matris.areas = [];
            var gradosAreas = buscarObjetos(disenoActual.gradoAreas,'nivelID',nivelID);
            
            disenoActual.areas.forEach(function (item){
                var area = buscarObjeto(gradosAreas,'areaID',item.areaID);
                if(area ){
                    var n = $scope.matris.areas.length;
                    
                    item.totalH = 0;
                    item.totalT = 0;
                    item.area = 0;
                    insertarElemento($scope.matris.areas,item);
                    $scope.matris.gradoAreas[n] = [];
                    
                    grados.forEach(function(gra){
                        $scope.matris.gradoAreas[n].push({horaDisponibleID:0,planNivelID:jornada.planNivelID,areaID:area.areaID,gradoID:gra.gradoID,hora:0,horaL:0,total:0});
                    });
                }
            });
            
            disenoActual.talleres.forEach(function (item){
                var area = buscarObjeto(jornada.horasDisponibles,'areaID',item.areaID);

                if(area ){
                    var n = $scope.matris.areas.length;

                    item.totalH = 0;
                    item.totalT = 0;
                    item.area = 0;
                    insertarElemento($scope.matris.areas,item);
                    $scope.matris.gradoAreas[n] = [];

                    grados.forEach(function(gra){
                        $scope.matris.gradoAreas[n].push({horaDisponibleID:0,planNivelID:jornada.planNivelID,areaID:area.areaID,gradoID:gra.gradoID,hora:0,horaL:0,total:0});
                    });
                }
            });
            
            //agregando tutoria como un area curricular
            var areaTut = {};
            areaTut.nombre = "tutoria";
            areaTut.totalH = 0;
            areaTut.totalT = 0;
            areaTut.area = 0;
            areaTut.areaID = -1;
            insertarElemento($scope.matris.areas,areaTut);
            $scope.matris.gradoAreas[areaTut.i] = [];
            grados.forEach(function(gra){
                $scope.matris.gradoAreas[areaTut.i].push({horaDisponibleID:0,planNivelID:jornada.planNivelID,areaID:-1,gradoID:gra.gradoID,hora:jornada.hTutoria,horaL:jornada.hTutoria*gra.meta,total:jornada.hTutoria*gra.meta});
                gra.totalH += $scope.matris.gradoAreas[areaTut.i][gra.i].hora;
                gra.totalT += $scope.matris.gradoAreas[areaTut.i][gra.i].total;
                gra.totalL = gra.totalT;

                areaTut.totalH += $scope.matris.gradoAreas[areaTut.i][gra.i].hora;
                areaTut.totalT += $scope.matris.gradoAreas[areaTut.i][gra.i].total;
                
            });
            //agregando tutoria como un area curricular-- FIN
            
            $scope.matris.f = $scope.matris.areas.length;
            var jorSel = buscarObjeto($scope.jornadas,'jornadaID',jornada.jornadaID);
            
            //obteniendo las horas de libre disponibilidad
            jornada.horasDisponibles.forEach(function(item){
                var area = buscarObjeto(disenoActual.talleres,"areaID",item.areaID);
                if(area){
                    var grado = buscarObjeto($scope.matris.grados,"gradoID",item.gradoID);                    
                    if(grado){
                        $scope.matris.gradoAreas[area.i][grado.i].hora = item.horaL;
                        $scope.matris.gradoAreas[area.i][grado.i].total = item.horaL*grado.meta;
                        $scope.matris.gradoAreas[area.i][grado.i].horaL = $scope.matris.gradoAreas[area.i][grado.i].total;
                        
                        grado.totalH += $scope.matris.gradoAreas[area.i][grado.i].hora;
                        grado.totalT += $scope.matris.gradoAreas[area.i][grado.i].total;
                        grado.totalL = grado.totalT;
                        
                        area.totalH += $scope.matris.gradoAreas[area.i][grado.i].hora;
                        area.totalT += $scope.matris.gradoAreas[area.i][grado.i].total; 
                    }
                }
                else{
                    area = buscarObjeto($scope.matris.areas,"areaID",item.areaID);
                    grado = buscarObjeto($scope.matris.grados,"gradoID",item.gradoID); 
                    
                    if(area && grado){
                        $scope.matris.gradoAreas[area.i][grado.i].hora = item.horaL;
                    }
                    
                }
            });
            //obteniendo las horas establecidas por el plan de estudios
            jorSel.horas.forEach(function(item){
                var area = buscarObjeto($scope.matris.areas,"areaID",item.areaID);
                if(area){
                    var grado = buscarObjeto($scope.matris.grados,"gradoID",item.gradoID);                    
                    if(grado){
                        $scope.matris.gradoAreas[area.i][grado.i].hora += item.hora;
                        $scope.matris.gradoAreas[area.i][grado.i].total = $scope.matris.gradoAreas[area.i][grado.i].hora*grado.meta;
                        $scope.matris.gradoAreas[area.i][grado.i].horaL = $scope.matris.gradoAreas[area.i][grado.i].total;
                        
                        grado.totalH += $scope.matris.gradoAreas[area.i][grado.i].hora;
                        grado.totalT += $scope.matris.gradoAreas[area.i][grado.i].total;
                        grado.totalL = grado.totalT;
                        
                        area.totalH += $scope.matris.gradoAreas[area.i][grado.i].hora;
                        area.totalT += $scope.matris.gradoAreas[area.i][grado.i].total;
                    }
                }                
            });
            
            //obteniendo las horas distribuidas hasta el momento
            /*
            var totalL = 0;
            $scope.plan.distribucion.forEach(function(item){
                var grado = buscarObjeto($scope.matris.grados,"gradoID",item.gradoID);                
                if(grado){
                    totalL += item.hora;
                    grado.totalL -= item.hora;
                }
            });*/
            
            
            var totalT = 0;
            var totalH = 0;
            var labels = [];
            var data = [];
            $scope.matris.areas.forEach(function(item){
                totalH += item.totalH;
                totalT += item.totalT;
                labels.push(item.nombre);
                data.push(item.totalT);
            });
            $scope.matris.totalT = totalT ;
            $scope.matris.totalH = totalH ;
               
            crearGraficoDona("ditribucionAreas",labels,data);
            
            //$scope.matris.totalL = $scope.matris.totalT - totalL ;
        }
    };
    $scope.generarEstadistica4= function(){
        //preparamos un objeto request
        var request = crud.crearRequest('cuadroHoras',1,'listarPlazasPorOrganizacion');
        request.setData({organizacionID:$scope.busqueda.orgID});
        crud.listar("/cuadroHoras",request,function(res){
            if(!res.responseSta){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                return;
            }
            if(res.data){
                setting.dataset = res.data;
                iniciarPosiciones(setting.dataset);
                $scope.tendeciaPlazas();
                $scope.tabla.settings(setting);
            }
        },function(data){
            console.info(data);
        });
    };
    
    $scope.imprimirGrafico = function(){
        var grafico = new MyFile("Reporte Metas de Atencion");
        grafico.parseDataURL( document.getElementById("metasAtencion").toDataURL("image/png") );
        
        var request = crud.crearRequest('planEstudios',1,'reporte');
        request.setData(grafico);
        crud.insertar("/cuadroHoras",request,function(response){
            if(response.responseSta){                
                verDocumento( response.data.reporte );
            }            
        },function(data){
            console.info(data);
        });       
    };
    $scope.imprimirGrafico3 = function(){
        var grafico = new MyFile("Distribucion de horas por grado");
        grafico.parseDataURL( document.getElementById("ditribucionAreas").toDataURL("image/png") );
        
        var request = crud.crearRequest('planEstudios',1,'reporte');
        request.setData(grafico);
        crud.insertar("/cuadroHoras",request,function(response){
            if(response.responseSta){                
                verDocumento( response.data.reporte );
            }            
        },function(data){
            console.info(data);
        });      
    };
    $scope.imprimirGrafico4 = function(org){
        var request = crud.crearRequest('planEstudios',1,'reporte');
        request.setData({resumen:setting.dataset,organizacion:org});
        crud.insertar("/cuadroHoras",request,function(response){
            if(response.responseSta){                
                verDocumento( response.data.reporte );
            }            
        },function(data){
            console.info(data);
        }); 
    };
    listarDatos();
    function listarDatos(){
        request = crud.crearRequest('organizacion',1,'listarOrganizaciones');
        crud.listar("/configuracionInicial",request,function(data){
            $scope.organizaciones = data.data;
        },function(data){
            console.info(data);
        });
    };
    
    $scope.diseoCurricularVigente = function(){
        var request = crud.crearRequest('diseñoCurricular',1,'buscarVigentePorOrganizacion');
        request.setData({organizacionID:$scope.busqueda.orgID});
        crud.listar("/cuadroHoras",request,function(response){
            if(response.responseSta){
                $scope.disenoActual = response.data;
                $scope.disenoActual.talleres = [];
                $scope.disenoActual.areas.forEach(function(item){
                    if(item.tipo)
                        $scope.disenoActual.talleres.push(item);
                });
                $scope.buscarPlanEstudios($scope.busqueda.orgID);
            }
        },function(data){
            console.info(data);
        });
        
        //preparamos un objeto request
        request = crud.crearRequest('planEstudios',1,'listarMetasAtencion');
        request.setData({organizacionID:$scope.busqueda.orgID,desde:convertirFecha($scope.busqueda.desde),hasta:convertirFecha($scope.busqueda.hasta)});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/cuadroHoras",request,function(response){            
            if(!response.responseSta){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                return;
            }
            if(response.data)
                metas = response.data;
            
        },function(data){
            console.info(data);
        });
        
        request = crud.crearRequest('diseñoCurricular',1,'listarJornadaEscolar');
        crud.listar("/cuadroHoras",request,function(response){
            $scope.jornadas = response.data;
        },function(data){
            console.info(data);
        });
        
    };
    $scope.buscarPlanEstudios = function(orgID){
        //preparamos un objeto request
        var request = crud.crearRequest('planEstudios',1,'buscarVigente');
        request.setData({organizacionID:orgID});
        crud.listar("/cuadroHoras",request,function(response){
            
            if(response.responseSta){
                $scope.plan = response.data;                
                $scope.plan.niveles.forEach(function(item){
                    var j = buscarObjeto($scope.disenoActual.jornadas,"jornadaID",item.jornadaID);
                    item.jornada = j.nombre;
                    item.hLibre = j.hLibre;
                    item.hObligatoria = j.hObligatoria;
                    item.hTotal = j.hTotal;
                    item.hTutoria = j.hTutoria;
                    item.nivelID = j.nivelID;
                    item.nivel = buscarContenido($scope.disenoActual.niveles,"nivelID","nombre",item.nivelID);
                });
                //$scope.calcularMatriz();
            }
        },function(data){
            console.info(data);
        });
    };
    
    $scope.tendeciaPlazas = function(){
        var request = crud.crearRequest('planEstudios',1,'buscarPlanDesde');
        request.setData({organizacionID:$scope.busqueda.orgID, anio:2017});
        crud.listar("/cuadroHoras",request,function(res){
            if(res.responseSta){
                $scope.años = [];
                var labels = [];
                var plazasInicial = [];
                var plazasPrimaria = [];
                var plazasSecundaria = [];
                $scope.tendenciaPlazas = res.data;
                jQuery.each(res.data, function(index, anio) {
                    //var countPlazas = 0;
                    var countPrimaria = 0;
                    var countSecundaria = 0;
                    var countInicial = 0;
                    jQuery.each(anio, function(index, plaza) {
                        if(plaza.Nivel === "secundaria")
                            countSecundaria++;
                        else if(plaza.Nivel === "primaria")
                            countPrimaria++;
                        else if(plaza.Nivel === "inicial")
                            countInicial++;
                        //countPlazas++;
                    });
                    //plazas.push(countPlazas);
                    plazasInicial.push(countInicial);
                    plazasPrimaria.push(countPrimaria);
                    plazasSecundaria.push(countSecundaria);
                    labels.push(index);
                });
                
                var data = [];
                data.push({
                    name: 'Inicial',
                    data: plazasInicial
                }, {
                    name: 'Primaria',
                    data: plazasPrimaria
                }, {
                    name: 'Secundaria',
                    data: plazasSecundaria
                });
                crearGraficoArea('stackChart', 'Tendencia de Plazas', false, 'plazas', 'plazas', labels, data);
               
            }
        },function(data){
            console.info(data);
        });
    };
}]);
