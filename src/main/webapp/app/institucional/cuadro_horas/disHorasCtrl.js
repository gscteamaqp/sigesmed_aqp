app.controller("disHorasCtrl",["$scope","NgTableParams","crud","modal", function ($scope,NgTableParams,crud,modal){
        
    $scope.plan = {};
    $scope.jornadaSel = {};
    $scope.plazaSel = {};
    
    $scope.dis = {hora:0};
    $scope.disHoras = false;//variable que me indica si hay distribucion por areas
    
    var params = {count: 15};
    var setting = { counts: []};
    $scope.tablaPlazas = new NgTableParams(params, setting);
    
    $scope.condiciones = [{condicionID:'T',nombre:"titular"},{condicionID:'V',nombre:"vacante"}];
    $scope.naturalezas = [{naturalezaID:'O',nombre:"organica"},{naturalezaID:'E',nombre:"eventual"}];
    var plazasTodas = [];
    
    $scope.prepararEditar= function(o){
        $scope.nuevo = false;
        $scope.plazaSel = JSON.parse(JSON.stringify(o));        
        $scope.disHoras = false;
        
        if(o.docenteID){
            var request = crud.crearRequest('cuadroHoras',1,'buscarDocente');
            request.setData({docenteID:o.docenteID});
            crud.listar("/cuadroHoras",request,function(res){
                if(res.responseSta){
                    $scope.plazaSel.titular = res.data.nombre;
                    $scope.plazaSel.codigoModular = res.data.codigoModular;
                    $scope.plazaSel.nivelMagisterial = res.data.nivelMagisterial;
                }
            },function(data){
                console.info(data);
            });
        }
        
        $('#modalEditar').modal('show');
    };
    
    $scope.verDocenteHover = function(o){
        $scope.nuevo = false;
        $scope.plazaSel = JSON.parse(JSON.stringify(o));        
        $scope.disHoras = false;
        
        if(o.docenteID){
            var request = crud.crearRequest('cuadroHoras',1,'buscarDocente');
            request.setData({docenteID:o.docenteID});
            crud.listar("/cuadroHoras",request,function(res){
                if(res.responseSta){
                    $scope.plazaSel.titularNombre = res.data.nombre;
                }
            },function(data){
                console.info(data);
            });
        }        
    };
    $scope.editarPlaza = function(orgID){
        
        if(!$scope.nuevo){
            var request = crud.crearRequest('cuadroHoras',1,'actualizarPlazaMagisterial');

            request.setData($scope.plazaSel);
            crud.actualizar("/cuadroHoras",request,function(res){
                modal.mensaje("CONFIRMACION",res.responseMsg);
                if(res.responseSta){
                    setting.dataset[$scope.plazaSel.i] = $scope.plazaSel;
                    $scope.tablaPlazas.reload();
                    $scope.plazaSel = {};
                    $('#modalEditar').modal('hide');
                }            
            },function(data){
                console.info(data);
            });
        }
        else{
            var request = crud.crearRequest('cuadroHoras',1,'insertarPlazaMagisterial');
            
            $scope.plazaSel.organizacionID = orgID;
        
            request.setData($scope.plazaSel);
            crud.insertar("/cuadroHoras",request,function(res){
                modal.mensaje("CONFIRMACION",res.responseMsg);
                if(res.responseSta){
                    $scope.plazaSel.plazaID = res.data.plazaID;
                    insertarElemento(setting.dataset,$scope.plazaSel);
                    $scope.tablaPlazas.reload();
                    $('#modalEditar').modal('hide');
                }            
            },function(data){
                console.info(data);
            });
        }
    };
    $scope.vacarPlaza = function(){
        //declarar la plaza vacante
        if($scope.plazaSel.condicionID=='V'){
            $scope.plazaSel.docenteID = "";
            $scope.plazaSel.titular = ""
            $scope.plazaSel.codigoModular = "";
            $scope.plazaSel.nivelMagisterial = "";
        }
    };
    $scope.prepararAsignarDocente= function(o){
        
        $scope.busqueda = {tipo:"DNI"};
        $scope.plazaSel = JSON.parse(JSON.stringify(o)); 
        
        $('#modalAsignarDocente').modal('show');
    };
    $scope.buscarDocente = function(){
        $scope.disHoras = false;
        
        var request = crud.crearRequest('cuadroHoras',1,'buscarDocente');
        request.setData($scope.busqueda);
        crud.listar("/cuadroHoras",request,function(res){
            if(res.responseSta){
                $scope.plazaSel.docenteID = res.data.docenteID;
                $scope.plazaSel.titular = res.data.nombre;
                $scope.plazaSel.codigoModular = res.data.codigoModular;
                $scope.plazaSel.nivelMagisterial = res.data.nivelMagisterial;
                $scope.plazaSel.especialidad = res.data.especialidad;
            }
            else
                $scope.plazaSel.titular = "No se encontraron resultados";
        },function(data){
            console.info(data);
        });
    };
    $scope.asignarDocente = function(){
        var request = crud.crearRequest('cuadroHoras',1,'actualizarPlazaMagisterial');

        $scope.plazaSel.condicionID = 'T';
        request.setData($scope.plazaSel);
        
        crud.actualizar("/cuadroHoras",request,function(res){
            modal.mensaje("CONFIRMACION",res.responseMsg);
            if(res.responseSta){
                setting.dataset[$scope.plazaSel.i] = $scope.plazaSel;
                $scope.tablaPlazas.reload();
                $scope.plazaSel = {};
                $('#modalAsignarDocente').modal('hide');
            }            
        },function(data){
            console.info(data);
        });
    };
    $scope.prepararNuevo= function(){
        $scope.nuevo = true;
        $scope.plazaSel = {};
        $scope.plazaSel.nivel = $scope.jornadaSel.nivel;
        $scope.plazaSel.modalidad = "EBR";
        $scope.plazaSel.jornadaPedagogica = 0;
        $('#modalEditar').modal('show');
    };
    var disGrados = [];//DISTRIBUCION ACTUAL DE LA PLAZA
    $scope.prepararAsignacion = function(o){
        $scope.plazaSel = JSON.parse(JSON.stringify(o));
        
        $scope.disHoras = false;
        if($scope.jornadaSel.nivel.toLowerCase() == "secundaria")
            $scope.disHoras = true;
        
        //obteniendo las horas distribuidas hasta el momento
        disGrados = buscarObjetos($scope.plan.distribucion,"plazaID",$scope.plazaSel.plazaID);
        $scope.plazaSel.distribucion = [];
        $scope.plazaSel.eliminados = [];
        disGrados.forEach(function(item){
            var grado = buscarObjeto($scope.matris.grados,"gradoID",item.gradoID);
            if($scope.disHoras){
                item.areas.forEach(function(item2){
                    var area = buscarObjeto($scope.matris.areas,"areaID",item2.areaID);
                    var dis = {disGradoI:item.disGradoID,hora:item2.hora,g:grado,gradoID:grado.gradoID,seccionID:item.seccionID,a:area,areaID:area.areaID};
                    $scope.plazaSel.distribucion.push(dis);
                });

            }else{
                var dis = {disGradoI:item.disGradoID,hora:item.hora,g:grado,gradoID:grado.gradoID,seccionID:item.seccionID};
                $scope.plazaSel.distribucion.push(dis);
            }
        });
        
        $scope.dis = {hora:0};
        
        $('#modalAsignacion').modal('show');
    };
    $scope.agregarDis = function(){
        
        if(!$scope.dis.g || $scope.dis.g.gradoID == 0 ){
            modal.mensaje("ALERTA","seleccione el grado");
            return false;
        }
        if( $scope.disHoras && (!$scope.dis.a.areaID || $scope.dis.a.areaID == 0 ) ){
            modal.mensaje("ALERTA","seleccione el area curricular");
            return false;
        }
        
        $scope.dis.horaT = $scope.disHoras?$scope.matris.gradoAreas[$scope.dis.a.i][$scope.dis.g.i].horaL:$scope.matris.grados[$scope.dis.g.i].totalL;
        
        var horaD = verificarDistribucion($scope.dis);
        if(!horaD){
            console.log("DISTRIBUCION ERRONEA "+ horaD);
            return;
            
        }
        
        $scope.dis.areaID = $scope.dis.a?$scope.dis.a.areaID:null;
        $scope.dis.gradoID = $scope.dis.g.gradoID;
        
        $scope.plazaSel.jornadaPedagogica = horaD;
        
        $scope.plazaSel.distribucion.push($scope.dis);
        $scope.dis = {hora:0};
    };
    
    $scope.editarDis = function(i,d){
        //si estamso editando
        if(d.edi){
            
            if(!d.copia.g || d.copia.g.gradoID == 0 ){
                modal.mensaje("ALERTA","seleccione el grado");
                return false;
            }
            if( $scope.disHoras && (!d.copia.a.areaID || d.copia.a.areaID == 0 ) ){
                modal.mensaje("ALERTA","seleccione el area curricular");
                return false;
            }
            
            d.copia.horaT = $scope.disHoras?$scope.matris.gradoAreas[d.copia.a.i][d.copia.g.i].horaL:$scope.matris.grados[d.copia.g.i].totalL;
            
            var horaD = verificarDistribucion(d.copia,d);
            if(!horaD){
                console.log("DISTRIBUCION ERRONEA "+ horaD);
                return;
            }
            
            d.copia.areaID = d.copia.a?d.copia.a.areaID:null;
            d.copia.gradoID = d.copia.g.gradoID;
            
            $scope.plazaSel.jornadaPedagogica = horaD;
            d.copia.horaAntes = $scope.plazaSel.distribucion[i].hora;
            $scope.plazaSel.distribucion[i] = d.copia;
            
            console.log($scope.plazaSel.distribucion);
            
            delete d.copia;
        }
        //si queremos editar
        else{
            d.copia = JSON.parse(JSON.stringify(d));
            d.edi =true;            
        }
    };
    $scope.eliminarDis = function(i,d){
        //si estamso cancelando la edicion
        if(d.edi){
            d.edi = false;
            delete d.copia;
        }
        //si queremos eliminar el elemento
        else{
            $scope.plazaSel.eliminados.push($scope.plazaSel.distribucion[i]);
            $scope.plazaSel.distribucion.splice(i,1);
            $scope.plazaSel.jornadaPedagogica -= d.hora;
        }
    };
    //busca la distribucion por grado y area
    function verificarDistribucion(dis,disA){
        
        
        var antes = disA?disA.hora:0;
        if(!antes || disA.g.gradoID != dis.g.gradoID ){            
            var lista = buscarObjetos($scope.plazaSel.distribucion,"gradoID",dis.g.gradoID);            
            if( $scope.disHoras && ( !antes || disA.a.areaID != dis.a.areaID) )
                lista = buscarObjetos(lista,"areaID",dis.a.areaID);
            
            if(lista.length>0){
                modal.mensaje("ALERTA","la distribucion que desea realizar ya existe");
                return false;
            }
        }        
        if(!dis.hora || dis.hora == 0 ){
            modal.mensaje("ALERTA","debe ingresar una cantidad de horas");
            return false;
        }
        //comprobamos que las horas no sobrepasen el total de horas por area
        if( dis.hora > dis.horaT + antes ){
            if($scope.disHoras)
                modal.mensaje("ALERTA","la hora asignada sobrepasa, lo establecido para el area: "+dis.a.nombre);
            else
                modal.mensaje("ALERTA","la hora asignada sobrepasa, lo establecido para el grado: "+dis.g.nombre);
            return false;
        }
        
        var horaDictado = $scope.plazaSel.jornadaPedagogica - antes + dis.hora;
        if( horaDictado > $scope.plazaSel.jornadaLaboral){
            modal.mensaje("ALERTA","la hora asignada sobrepasa la jornada laboral de la plaza");
            return false;
        }
        
        if($scope.disHoras){
            dis.horaT = $scope.matris.gradoAreas[dis.a.i][dis.g.i].horaL + antes - dis.hora;
        }
        else{
            dis.horaT = $scope.matris.grados[dis.g.i].totalL + antes - dis.hora;
        }
        
        return horaDictado;
    }
    
    $scope.guardarDistribucion = function(){
        var request = crud.crearRequest('cuadroHoras',1,'insertarDistribucionGrado');
        
        //asignando el plan de estudios
        $scope.plazaSel.planID = $scope.plan.planID;
        
        //si hacemos la distribucion a secundaria (distribucion de grados y areas)
        if($scope.disHoras){
            $scope.plazaSel.distribucion.forEach(function(item){
                
                //solo si editamos o añadimos una nueva distribucion
                if(item.horaT||item.horaT==0){
                    $scope.matris.gradoAreas[item.a.i][item.g.i].horaL = item.horaT;
                    var dis = buscarObjeto(disGrados,"disGradoID",item.disGradoI);
                    //si estamos editando
                    if(item.horaAntes && item.horaAntes >0){                        
                        var disArea = buscarObjeto(dis.areas,"areaID",item.areaID);
                        disArea.hora = item.hora;
                        
                        $scope.matris.grados[item.g.i].totalL = $scope.matris.grados[item.g.i].totalL + item.horaAntes - item.hora ;                        
                    }
                    //si estamos añadiendo
                    else{
                        //si hay distribucion de grado, añadimos la distribucion de areas
                        if(dis){
                            var nuevaArea = {disAreaID:dis.areas.length,disGradoID:dis.disGradoID,hora:item.hora,areaID:item.areaID}
                            dis.areas.push(nuevaArea);
                        }
                        else{
                            var nuevo = {disGradoID:$scope.plan.distribucion.length,plazaID:$scope.plazaSel.plazaID,hora:item.hora,gradoID:item.gradoID,areas:[]}
                            var nuevaArea = {disAreaID:nuevo.areas.length,disGradoID:nuevo.disGradoID,hora:item.hora,areaID:item.areaID}
                            nuevo.areas.push(nuevaArea);
                            $scope.plan.distribucion.push(nuevo);
                        }
                            
                        $scope.matris.grados[item.g.i].totalL = $scope.matris.grados[item.g.i].totalL - item.hora ;
                    }
                }
            });
            //verificando los eliminados
            $scope.plazaSel.eliminados.forEach(function(item){
                $scope.matris.gradoAreas[item.a.i][item.g.i].horaL = item.horaAntes?item.horaAntes:item.hora;
                $scope.matris.grados[item.g.i].totalL += item.horaAntes?item.horaAntes:item.hora;
                
                var dis = buscarObjeto(disGrados,"disGradoID",item.disGradoI);
                eliminarObjeto(dis.areas,"areaID",item.areaID);
                //si depues de eliminar las horas asignadas al area , ya no queda ninguna disdtibucion se procede a eliminar la distribucion de horas
                if(dis.areas.length==0)
                    eliminarObjeto($scope.plan.distribucion,"disGradoID",item.disGradoI);
            });
        }
        //si hacemos la distribucion a inicial y primaria (distribucion de grados)
        else{
            $scope.plazaSel.areas = $scope.disenoActual.areas;
            $scope.plazaSel.distribucion.forEach(function(item){
                
                //solo si editamos o añadimos una nueva distribucion
                if(item.horaT || item.horaT ==0){
                    $scope.matris.grados[item.g.i].totalL = item.horaT;
                    var dis = buscarObjeto(disGrados,"disGradoID",item.disGradoI);
                    if(dis)
                        dis.hora = item.hora;
                    else{
                        var nuevo = {disGradoID:$scope.plan.distribucion.length,plazaID:$scope.plazaSel.plazaID,hora:item.hora,gradoID:item.gradoID}
                        $scope.plan.distribucion.push(nuevo);
                    }
                        
                }
            });
            //verificando los eliminados
            $scope.plazaSel.eliminados.forEach(function(item){
                $scope.matris.grados[item.g.i].totalL += item.horaAntes?item.horaAntes:item.hora;
                eliminarObjeto($scope.plan.distribucion,"disGradoID",item.disGradoI);
            });
        }
        var totalL = 0;
        
        $scope.matris.grados.forEach(function(item){
            totalL += item.totalL;
        });
        $scope.matris.totalL = totalL;
        //console.log($scope.matris.totalT - totalL );
        //$scope.matris.totalL = $scope.matris.totalT - totalL ;
        
        request.setData($scope.plazaSel);
        crud.insertar("/cuadroHoras",request,function(res){            
            modal.mensaje("CONFIRMACION",res.responseMsg);
            if(res.responseSta){
                $scope.disHoras = false;
                setting.dataset[$scope.plazaSel.i].jornadaPedagogica = $scope.plazaSel.jornadaPedagogica;
                $('#modalAsignacion').modal('hide');
            }            
        },function(data){
            console.info(data);
        });
    };
    $scope.buscarPlanEstudios = function(orgID){
        //preparamos un objeto request
        var request = crud.crearRequest('planEstudios',1,'buscarVigente');
        request.setData({organizacionID:orgID});
        crud.listar("/cuadroHoras",request,function(response){
            
            if(response.responseSta){
                $scope.plan = response.data;
                
                request = crud.crearRequest('cuadroHoras',1,'listarDistribucionPlazas');
                request.setData({planID:$scope.plan.planID});
                crud.listar("/cuadroHoras",request,function(res){
                    if(res.responseSta){
                        $scope.plan.distribucion = res.data;
                    }
                },function(data){
                    console.info(data);
                });
                
                $scope.plan.niveles.forEach(function(item){
                    var j = buscarObjeto($scope.disenoActual.jornadas,"jornadaID",item.jornadaID);                    
                    item.jornada = j.nombre;
                    item.hLibre = j.hLibre;
                    item.hObligatoria = j.hObligatoria;
                    item.hTotal = j.hTotal;
                    item.hTutoria = j.hTutoria;
                    item.nivelID = j.nivelID;
                    item.nivel = buscarContenido($scope.disenoActual.niveles,"nivelID","nombre",item.nivelID);                    
                    item.nivel = item.nivel.toLowerCase();
                    /*item.turno = buscarContenido($scope.turnos,"turnoID","nombre",item.turnoID);
                    item.periodo = buscarContenido($scope.periodos,"periodoID","nombre",item.periodoID);*/
                });
            }
        },function(data){
            console.info(data);
        });
        
    };
    $scope.diseoCurricularVigente = function(orgID){
        var request = crud.crearRequest('diseñoCurricular',1,'buscarVigentePorOrganizacion');
        request.setData({organizacionID:orgID});
        crud.listar("/cuadroHoras",request,function(response){
            if(response.responseSta){
                $scope.disenoActual = response.data;
                $scope.disenoActual.talleres = [];
                $scope.disenoActual.areas.forEach(function(item){
                    if(item.tipo)
                        $scope.disenoActual.talleres.push(item);
                });
                $scope.buscarPlanEstudios(orgID);
            }
        },function(data){
            console.info(data);
        });
        
        request = crud.crearRequest('diseñoCurricular',1,'listarJornadaEscolar');
        crud.listar("/cuadroHoras",request,function(response){
            $scope.jornadas = response.data;
        },function(data){
            console.info(data);
        });
    };
    $scope.listarPlazas = function(orgID){
        var request = crud.crearRequest('cuadroHoras',1,'listarPlazasPorOrganizacion');
        request.setData({organizacionID:orgID});
        crud.listar("/cuadroHoras",request,function(res){
            if(res.responseSta){
                plazasTodas = res.data;
                
                plazasTodas.forEach(function (item){
                    if(item.docenteID)
                        item.asignado = 'Asignado';
                    else
                        item.asignado = 'No Asignado';
                });
            }
        },function(data){
            console.info(data);
        });
    };
    function filtrarPlazas(){
        var plazasFilter = plazasTodas.filter(function(value){
            return value.nivel == $scope.jornadaSel.nivel;
        });        
        setting.dataset = plazasFilter;
        iniciarPosiciones(setting.dataset);
        $scope.tablaPlazas.settings(setting);
    };
    $scope.calcularMatriz = function(jornada){
        
        $scope.matris = {};
        
        
        var disenoActual = $scope.disenoActual;
        if(jornada && disenoActual.grados ){
            
            var nivelID = jornada.nivelID;
            
            
            var grados = [];
            var metas = 0;
            disenoActual.grados.forEach(function(item){

                if( nivelID == item.nivelID ){                    
                    
                    //buscando las metas de atencion
                    var meta = buscarObjeto(jornada.metas,'gradoID',item.gradoID);
                    if(meta)
                        item.meta = meta.secciones;
                    else
                        item.meta = 0;
                    
                    metas += item.meta;
                    
                    item.totalH = 0;
                    item.totalT = 0;
                    item.totalL = 0;
                    insertarElemento(grados,item);
                }
            });
            

            $scope.matris.grados = grados;
            $scope.matris.c = grados.length;
            $scope.matris.f = 0;
            $scope.matris.metas = metas;
            
            $scope.matris.gradoAreas = [];
            
            $scope.matris.areas = [];
            var gradosAreas = buscarObjetos(disenoActual.gradoAreas,'nivelID',nivelID);
            
            disenoActual.areas.forEach(function (item){
                var area = buscarObjeto(gradosAreas,'areaID',item.areaID);
                if(area ){
                    var n = $scope.matris.areas.length;
                    
                    item.totalH = 0;
                    item.totalT = 0;
                    item.area = 0;
                    insertarElemento($scope.matris.areas,item);
                    $scope.matris.gradoAreas[n] = [];
                    
                    grados.forEach(function(gra){
                        $scope.matris.gradoAreas[n].push({horaDisponibleID:0,planNivelID:jornada.planNivelID,areaID:area.areaID,gradoID:gra.gradoID,hora:0,horaL:0,total:0});
                    });
                }
            });
            
            disenoActual.talleres.forEach(function (item){
                var area = buscarObjeto(jornada.horasDisponibles,'areaID',item.areaID);

                if(area ){
                    var n = $scope.matris.areas.length;

                    item.totalH = 0;
                    item.totalT = 0;
                    item.area = 0;
                    insertarElemento($scope.matris.areas,item);
                    $scope.matris.gradoAreas[n] = [];

                    grados.forEach(function(gra){
                        $scope.matris.gradoAreas[n].push({horaDisponibleID:0,planNivelID:jornada.planNivelID,areaID:area.areaID,gradoID:gra.gradoID,hora:0,horaL:0,total:0});
                    });
                }
            });
            
            //agregando tutoria como un area curricular
            var areaTut = {};
            areaTut.nombre = "tutoria";
            areaTut.totalH = 0;
            areaTut.totalT = 0;
            areaTut.area = 0;
            areaTut.areaID = -1;
            insertarElemento($scope.matris.areas,areaTut);
            $scope.matris.gradoAreas[areaTut.i] = [];
            grados.forEach(function(gra){
                $scope.matris.gradoAreas[areaTut.i].push({horaDisponibleID:0,planNivelID:jornada.planNivelID,areaID:-1,gradoID:gra.gradoID,hora:jornada.hTutoria,horaL:jornada.hTutoria*gra.meta,total:jornada.hTutoria*gra.meta});
                gra.totalH += $scope.matris.gradoAreas[areaTut.i][gra.i].hora;
                gra.totalT += $scope.matris.gradoAreas[areaTut.i][gra.i].total;
                gra.totalL = gra.totalT;

                areaTut.totalH += $scope.matris.gradoAreas[areaTut.i][gra.i].hora;
                areaTut.totalT += $scope.matris.gradoAreas[areaTut.i][gra.i].total;
                
            });
            //agregando tutoria como un area curricular-- FIN
            
            $scope.matris.f = $scope.matris.areas.length;
            var jorSel = buscarObjeto($scope.jornadas,'jornadaID',jornada.jornadaID);
            
            //obteniendo las horas de libre disponibilidad
            jornada.horasDisponibles.forEach(function(item){
                var area = buscarObjeto(disenoActual.talleres,"areaID",item.areaID);
                if(area){
                    var grado = buscarObjeto($scope.matris.grados,"gradoID",item.gradoID);                    
                    if(grado){
                        $scope.matris.gradoAreas[area.i][grado.i].hora = item.horaL;
                        $scope.matris.gradoAreas[area.i][grado.i].total = item.horaL*grado.meta;
                        $scope.matris.gradoAreas[area.i][grado.i].horaL = $scope.matris.gradoAreas[area.i][grado.i].total;
                        
                        grado.totalH += $scope.matris.gradoAreas[area.i][grado.i].hora;
                        grado.totalT += $scope.matris.gradoAreas[area.i][grado.i].total;
                        grado.totalL = grado.totalT;
                        
                        area.totalH += $scope.matris.gradoAreas[area.i][grado.i].hora;
                        area.totalT += $scope.matris.gradoAreas[area.i][grado.i].total; 
                    }
                }
                else{
                    area = buscarObjeto($scope.matris.areas,"areaID",item.areaID);
                    grado = buscarObjeto($scope.matris.grados,"gradoID",item.gradoID); 
                    
                    if(area && grado){
                        $scope.matris.gradoAreas[area.i][grado.i].hora = item.horaL;
                    }
                    
                }
            });
            //obteniendo las horas establecidas por el plan de estudios
            jorSel.horas.forEach(function(item){
                var area = buscarObjeto($scope.matris.areas,"areaID",item.areaID);
                if(area){
                    var grado = buscarObjeto($scope.matris.grados,"gradoID",item.gradoID);                    
                    if(grado){
                        $scope.matris.gradoAreas[area.i][grado.i].hora += item.hora;
                        $scope.matris.gradoAreas[area.i][grado.i].total = $scope.matris.gradoAreas[area.i][grado.i].hora*grado.meta;
                        $scope.matris.gradoAreas[area.i][grado.i].horaL = $scope.matris.gradoAreas[area.i][grado.i].total;
                        
                        grado.totalH += $scope.matris.gradoAreas[area.i][grado.i].hora;
                        grado.totalT += $scope.matris.gradoAreas[area.i][grado.i].total;
                        grado.totalL = grado.totalT;
                        
                        area.totalH += $scope.matris.gradoAreas[area.i][grado.i].hora;
                        area.totalT += $scope.matris.gradoAreas[area.i][grado.i].total;
                    }
                }                
            });
            
            //obteniendo las horas distribuidas hasta el momento
            var totalL = 0;
            $scope.plan.distribucion.forEach(function(item){
                var grado = buscarObjeto($scope.matris.grados,"gradoID",item.gradoID);                
                if(grado){
                    totalL += item.hora;
                    grado.totalL -= item.hora;
                    
                    item.areas.forEach(function(item2){
                        
                        var area = buscarObjeto($scope.matris.areas,"areaID",item2.areaID);
                        $scope.matris.gradoAreas[area.i][grado.i].horaL -= item2.hora;
                    });
                }
            });
            
            
            var totalT = 0;
            var totalH = 0;
            $scope.matris.areas.forEach(function(item){
                totalH += item.totalH;
                totalT += item.totalT;
            });
            $scope.matris.totalT = totalT ;
            $scope.matris.totalH = totalH ;
            $scope.matris.totalL = $scope.matris.totalT - totalL ;
            
            filtrarPlazas();
        }
    };
}]);
