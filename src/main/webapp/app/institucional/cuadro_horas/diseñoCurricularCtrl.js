app.controller("diseñoCurricularCtrl",["$scope","NgTableParams","crud","modal", function ($scope,NgTableParams,crud,modal){
        
    $scope.nuevoDiseno = {nombre:"",descripcion:"",resolucion:"",tipo:'N',estado:'A'};
    $scope.disenoSel = {};
    
    var params = {count: 1};
    var setting = { counts: []};
    $scope.tabla = new NgTableParams(params, setting);
    
    $scope.nivel = {};
    $scope.ciclo = {};    
    $scope.jornada = {};
    $scope.grado = {};
    $scope.area = {tipo:false};
    $scope.gradoArea = {};
    
    $scope.matris = {};
    $scope.matris2 = {};
    
    $scope.listarDisenos = function(orgID){
        //preparamos un objeto request
        var request = crud.crearRequest('diseñoCurricular',1,'listarDiseñoCurricular');
        crud.listar("/cuadroHoras",request,function(response){
            setting.dataset = response.data;
            for(var i=0; i<response.data.length;i++)
                if(response.data[i].organizacionID == orgID){
                    $scope.estaCreado = true;
                    break;
                }
            $scope.tabla.settings(setting);
            $scope.tabla.calcularMatriz();
            $scope.tabla.calcularMatrizDetalle();
            
            //$scope.calcularMatriz();
        },function(data){
            console.info(data);
        });
        
        request = crud.crearRequest('diseñoCurricular',1,'listarJornadaEscolar');
        crud.listar("/cuadroHoras",request,function(response){
            $scope.jornadas = response.data;
        },function(data){
            console.info(data);
        });
    };
    $scope.agregarDiseno = function(orgID){
        
        $scope.nuevoDiseno.organizacionID = orgID;
        
        var request = crud.crearRequest('diseñoCurricular',1,'insertarDiseñoCurricular');
        request.setData($scope.nuevoDiseno);
        
        crud.insertar("/cuadroHoras",request,function(response){
            
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                //recuperamos las variables que nos envio el servidor
                $scope.nuevoDiseno.diseñoID = response.data.diseñoID;
                $scope.nuevoDiseno.fecha = response.data.fecha;
                $scope.nuevoDiseno.modalidad = response.data.modalidad;
                
                //insertamos el elemento a la lista
                setting.dataset.push($scope.nuevoDiseno);
                $scope.tabla.reload();
                //reiniciamos las variables
                $scope.nuevoDiseno = {nombre:"",descripcion:"",resolucion:"",tipo:'N',estado:'A'};
                $scope.estaCreado = true;
                
                //cerramos la ventana modal
                $('#modalNuevo').modal('hide');
            }            
        },function(data){
            console.info(data);
        });
        
    };
    $scope.eliminarDiseno = function(i,o){
        modal.mensajeConfirmacion($scope,"seguro que desea eliminar este registro",function(){
            var request = crud.crearRequest('diseñoCurricular',1,'eliminarDiseñoCurricular');
            request.setData({diseñoID:o.diseñoID});
            crud.eliminar("/cuadroHoras",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    eliminarElemento(setting.dataset,i);
                    $scope.tabla.reload();
                }
            },function(data){
                console.info(data);
            });            
        });
    };
    $scope.prepararEditar = function(d){
        $scope.disenoSel = JSON.parse(JSON.stringify(d));
        $('#modalEditar').modal('show');
    };
    $scope.editarDiseno = function(){
        
        var request = crud.crearRequest('diseñoCurricular',1,'actualizarDiseñoCurricular');
        request.setData($scope.disenoSel);
                
        crud.actualizar("/cuadroHoras",request,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                //actualizando
                setting.dataset[$scope.disenoSel.i] = $scope.disenoSel;
                $scope.tabla.reload();
                $('#modalEditar').modal('hide');
            }
        },function(data){
            console.info(data);
        });
    };
    
    /* CICLO EDUCATIVO */    
    $scope.agregarCiclo = function(){
        
        var request = crud.crearRequest('diseñoCurricular',1,'insertarCiclo');
        
        $scope.ciclo.diseñoID = $scope.disenoActual.diseñoID;
        request.setData($scope.ciclo);
        
        modal.mensajeConfirmacion($scope,"seguro que desea agregar el ciclo educativo",function(){
            crud.insertar("/cuadroHoras",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    $scope.ciclo.cicloID = response.data.cicloID;
                    $scope.disenoActual.ciclos.push($scope.ciclo);
                    $scope.ciclo = {};
                }            
            },function(data){
                console.info(data);
            });
        },'400');
    };
    
    $scope.editarCiclo = function(i,c){
        //si estamso editando
        if(c.edi){
            var request = crud.crearRequest('diseñoCurricular',1,'insertarCiclo');        
            request.setData(c.copia);
            modal.mensajeConfirmacion($scope,"seguro que desea editar el ciclo",function(){
                crud.insertar("/cuadroHoras",request,function(response){
                    modal.mensaje("CONFIRMACION",response.responseMsg);
                    if(response.responseSta){
                        $scope.disenoActual.ciclos[i] = c.copia;
                    }            
                },function(data){
                    console.info(data);
                });
            },'400');
        }
        //si queremos editar
        else{
            c.copia = JSON.parse(JSON.stringify(c));
            c.edi =true;            
        }
    };
    
    $scope.eliminar = function(i,r){
        //si estamso cancelando la edicion
        if(r.edi){
            r.edi = false;
            delete r.copia;
        }
        //si queremos eliminar el elemento
        /*else{
            $scope.usuarioSel.sessiones.splice(i,1);
        }*/
    };
    
    $scope.eliminarCiclo = function(i, c){
        var request = crud.crearRequest('diseñoCurricular',1,'eliminarCiclo');
        request.setData(c);
        modal.mensajeConfirmacion($scope,"seguro que desea eliminar el ciclo?",function(){
            crud.eliminar("/cuadroHoras",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    eliminarElemento($scope.disenoActual.ciclos,i);
                }            
            },function(data){
                console.info(data);
            });
        },'400');
    }
    
    
    /* CICLOS FIN*/
    /* NIVEL EDUCATIVO */    
    $scope.agregarNivel = function(){
        
        var request = crud.crearRequest('diseñoCurricular',1,'insertarNivel');
        
        $scope.nivel.diseñoID = $scope.disenoActual.diseñoID;
        $scope.nivel.modalidadID = $scope.disenoActual.modalidad.modalidadID;
        request.setData($scope.nivel);
        
        modal.mensajeConfirmacion($scope,"seguro que desea agregar el nivel educativo",function(){
            crud.insertar("/cuadroHoras",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    $scope.nivel.nivelID = response.data.nivelID;
                    $scope.disenoActual.niveles.push($scope.nivel);
                    $scope.nivel = {};
                    $scope.tabla.calcularMatriz();
                    $scope.tabla.calcularMatrizDetalle();
                }
            },function(data){
                console.info(data);
            });
        },'400');
    };
    $scope.editarNivel = function(i,n){
        //si estamso editando
        if(n.edi){
            var request = crud.crearRequest('diseñoCurricular',1,'insertarNivel');        
            request.setData(n.copia);
            modal.mensajeConfirmacion($scope,"seguro que desea editar el nivel educativo",function(){
                crud.insertar("/cuadroHoras",request,function(response){
                    modal.mensaje("CONFIRMACION",response.responseMsg);
                    if(response.responseSta){
                        $scope.disenoActual.niveles[i] = n.copia;
                    }            
                },function(data){
                    console.info(data);
                });
            },'400');
        }
        //si queremos editar
        else{
            n.copia = JSON.parse(JSON.stringify(n));
            n.edi =true;            
        }
    };
    
     $scope.eliminarNivel = function(i,n){
        var request = crud.crearRequest('diseñoCurricular',1,'eliminarNivel');
        request.setData(n);
        modal.mensajeConfirmacion($scope,"seguro que desea eliminar el nivel?",function(){
            crud.eliminar("/cuadroHoras",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    eliminarElemento($scope.disenoActual.niveles,i);
                }            
            },function(data){
                console.info(data);
            });
        },'400');
    }
    
    /*NIVEL FIN*/
    /* GRADO ESCOLAR */    
    $scope.agregarGrado = function(){
        
        var request = crud.crearRequest('diseñoCurricular',1,'insertarGrado');
        
        $scope.grado.diseñoID = $scope.disenoActual.diseñoID;
        request.setData($scope.grado);
        
        modal.mensajeConfirmacion($scope,"seguro que desea agregar el grado",function(){
            crud.insertar("/cuadroHoras",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    $scope.grado.gradoID = response.data.gradoID;
                    $scope.grado.ciclo = buscarContenido($scope.disenoActual.ciclos,"cicloID","abreviacion",$scope.grado.cicloID);
                    $scope.grado.nivel = buscarContenido($scope.disenoActual.niveles,"nivelID","nombre",$scope.grado.nivelID);                    
                    insertarElemento($scope.disenoActual.grados,$scope.grado);
                    $scope.grado = {};
                    $scope.tabla.calcularMatriz();
                }
            },function(data){
                console.info(data);
            });
        },'400');
    };
    $scope.editarGrado = function(i,g){
        //si estamso editando
        if(g.edi){
            var request = crud.crearRequest('diseñoCurricular',1,'insertarGrado');        
            request.setData(g.copia);
            modal.mensajeConfirmacion($scope,"seguro que desea editar el grado",function(){
                crud.insertar("/cuadroHoras",request,function(response){
                    modal.mensaje("CONFIRMACION",response.responseMsg);
                    if(response.responseSta){
                        g.copia.ciclo = buscarContenido($scope.disenoActual.ciclos,"cicloID","abreviacion",g.copia.cicloID);
                        g.copia.nivel = buscarContenido($scope.disenoActual.niveles,"nivelID","nombre",g.copia.nivelID); 
                        $scope.disenoActual.grados[i] = g.copia;
                    }            
                },function(data){
                    console.info(data);
                });
            },'400');
        }
        //si queremos editar
        else{
            g.copia = JSON.parse(JSON.stringify(g));
            g.edi =true;            
        }
    };
    
    $scope.eliminarGrado = function(i,g){
        var request = crud.crearRequest('diseñoCurricular',1,'eliminarGrado');
        request.setData(g);
        modal.mensajeConfirmacion($scope,"seguro que desea eliminar el grado?",function(){
            crud.eliminar("/cuadroHoras",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    eliminarElemento($scope.disenoActual.grados,i);
                }            
            },function(data){
                console.info(data);
            });
        },'400');
    }
    /*GRADO FIN*/
    /* AREA CURRICULAR*/    
    $scope.agregarArea = function(){
        
        var request = crud.crearRequest('diseñoCurricular',1,'insertarArea');
        
        $scope.area.diseñoID = $scope.disenoActual.diseñoID;
        request.setData($scope.area);
        
        modal.mensajeConfirmacion($scope,"seguro que desea agregar el area curricular",function(){
            crud.insertar("/cuadroHoras",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    $scope.area.areaID = response.data.areaID;
                    insertarElemento($scope.disenoActual.areas,$scope.area);
                    $scope.area = {tipo:false};
                }            
            },function(data){
                console.info(data);
            });
        },'400');
    };
    $scope.editarArea = function(i,a){
        //si estamso editando
        if(a.edi){
            var request = crud.crearRequest('diseñoCurricular',1,'insertarArea');        
            request.setData(a.copia);
            modal.mensajeConfirmacion($scope,"seguro que desea editar el area escolar",function(){
                crud.insertar("/cuadroHoras",request,function(response){
                    modal.mensaje("CONFIRMACION",response.responseMsg);
                    if(response.responseSta){
                        $scope.disenoActual.areas[i] = a.copia;
                    }            
                },function(data){
                    console.info(data);
                });
            },'400');
        }
        //si queremos editar
        else{
            a.copia = JSON.parse(JSON.stringify(a));
            a.edi =true;            
        }
    };
    
    
    $scope.eliminarArea = function(i,a){
        var request = crud.crearRequest('diseñoCurricular',1,'eliminarArea');        
        request.setData(a);
        modal.mensajeConfirmacion($scope,"seguro que desea eliminar el area?",function(){
            crud.eliminar("/cuadroHoras",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    eliminarElemento($scope.disenoActual.areas,i);
                }            
            },function(data){
                console.info(data);
            });
        },'400');
        
    }
    /*AREA CURRICULAR FIN*/
    
    /* JORNADA ESCOLAR*/    
    $scope.agregarJornada = function(){
        
        var request = crud.crearRequest('diseñoCurricular',1,'insertarJornada');
        
        $scope.jornada.diseñoID = $scope.disenoActual.diseñoID;
        request.setData($scope.jornada);
        
        modal.mensajeConfirmacion($scope,"seguro que desea agregar la jornada",function(){
            crud.insertar("/cuadroHoras",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    $scope.jornada.jornadaID = response.data.jornadaID;
                    $scope.jornada.hTotal = response.data.total;
                    $scope.jornada.nivel = buscarContenido($scope.disenoActual.niveles,"nivelID","nombre",$scope.jornada.nivelID);
                    $scope.disenoActual.jornadas.push($scope.jornada);
                    $scope.jornada.areas = [];
                    insertarElemento($scope.jornadas,$scope.jornada);
                    $scope.jornada = {};
                }            
            },function(data){
                console.info(data);
            });
        },'400');
    };   
    $scope.editarJornada = function(i,j){
        //si estamso editando
        if(j.edi){
            var request = crud.crearRequest('diseñoCurricular',1,'insertarJornada');        
            request.setData(j.copia);
            modal.mensajeConfirmacion($scope,"seguro que desea editar la jornada escolar",function(){
                crud.insertar("/cuadroHoras",request,function(response){
                    modal.mensaje("CONFIRMACION",response.responseMsg);
                    if(response.responseSta){
                        j.copia.nivel = buscarContenido($scope.disenoActual.niveles,"nivelID","nombre",j.copia.nivelID);
                        $scope.disenoActual.jornadas[i] = j.copia;
                    }            
                },function(data){
                    console.info(data);
                });
            },'400');
        }
        //si queremos editar
        else{
            j.copia = JSON.parse(JSON.stringify(j));
            j.edi =true;            
        }
    };
    /*JORNADA ESCOLAR FIN*/
    
    /* GRADO AREA*/    
    $scope.agregarGradoArea = function(){
        
        if(!$scope.gradoArea.grado || !$scope.gradoArea.area){
            modal.mensaje("ALERTA","seleccione el area para el grado");
            return;
        }
        
        if($scope.matris.gradoAreas[$scope.gradoArea.area.i] && $scope.matris.gradoAreas[$scope.gradoArea.area.i][$scope.gradoArea.grado.i]){
            modal.mensaje("ALERTA","ya se registro el area seleccionada");
            $scope.gradoArea.area = {};
            return;
        }
        
        var request = crud.crearRequest('diseñoCurricular',1,'insertarGradoArea');
        
        var nuevoGradoHora = {gradoID:$scope.gradoArea.grado.gradoID,gradoPos:$scope.gradoArea.grado.i ,areaID:$scope.gradoArea.area.areaID,areaPos:$scope.gradoArea.area.i};
        
        request.setData(nuevoGradoHora);
        
        modal.mensajeConfirmacion($scope,"seguro que desea agregar el area al grado",function(){
            crud.insertar("/cuadroHoras",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    
                    var areas = $scope.matris.gradoAreas[$scope.gradoArea.area.i];
                    if(!areas)
                        areas = $scope.matris.gradoAreas[$scope.gradoArea.area.i] = [];
                    
                    nuevoGradoHora.nivelID = $scope.gradoArea.grado.nivelID;
                    nuevoGradoHora.nombre = $scope.gradoArea.area.nombre;
                    areas[$scope.gradoArea.grado.i] = nuevoGradoHora;
                    
                    $scope.disenoActual.gradoAreas.push(nuevoGradoHora);

                    //areas[$scope.gradoArea.grado.i] = JSON.parse(JSON.stringify($scope.gradoArea.area));
                    
                    $scope.gradoArea = {};
                    
                }            
            },function(data){
                console.info(data);
            });
        },'400');
    };
    $scope.editarGradoArea = function(o){
        if(!o)
            return;
        
        if( o.edi){
            o.edi =false;
        }
        //si queremos editar
        else{
            o.copia = JSON.parse(JSON.stringify(o));
            o.edi =true;            
        }
    };
    
    $scope.eliminarGradoArea = function(o){
        modal.mensajeConfirmacion($scope,"seguro que desea quitar el area seleccionada al grado",function(){
            var request = crud.crearRequest('diseñoCurricular',1,'eliminarGradoArea');
            request.setData({gradoID:o.gradoID,areaID:o.areaID});
            crud.eliminar("/cuadroHoras",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    $scope.matris.gradoAreas[o.areaPos][o.gradoPos] = {};
                }
            },function(data){
                console.info(data);
            });
        });
    };
    
    
    /*GRADO AREA FIN*/
    
    /* AREA HORA*/    
    $scope.agregarAreaHora = function(o,jor,i){
                
        if(($scope.matris2.grados[i].total - o.hora) + o.copia.hora > jor.hObligatoria ){
            modal.mensaje("ALERTA","la hora sobrepasa lo establecido por la jornada");
            return;
        }
            
        
        var request = crud.crearRequest('diseñoCurricular',1,'insertarAreaHora');
        request.setData({areaHoraID:o.areaHoraID,jornadaID:jor.jornadaID ,gradoID:o.gradoID,areaID:o.areaID,hora:o.copia.hora});
        
        modal.mensajeConfirmacion($scope,"seguro que desea cambiar la hora asignada al area y al grado",function(){
            crud.insertar("/cuadroHoras",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    $scope.matris2.grados[i].total = ($scope.matris2.grados[i].total - o.hora)+ o.copia.hora;
                    $scope.matris2.gradoAreas[o.areaPos][o.gradoPos].areaHoraID = response.data.areaHoraID;
                    $scope.matris2.gradoAreas[o.areaPos][o.gradoPos].hora = o.copia.hora;
                    $scope.matris2.gradoAreas[o.areaPos][o.gradoPos].edi = null;
                    
                    var jorSel = buscarObjeto($scope.jornadas,'jornadaID',jor.jornadaID);
                    for(var k =0;jorSel.horas && k< jorSel.horas.length; k++ ){
                        if(jorSel.horas[k].gradoID == o.gradoID && jorSel.horas[k].areaID == o.areaID ){
                            jorSel.horas[k].hora = o.hora;
                            break;
                        }
                    }
                }
            },function(data){
                console.info(data);
            });
        },'400');
    };
    $scope.editarAreaHora = function(o){
        if(!o)
            return;
        
        if( o.edi){
            o.edi =false;
        }
        //si queremos editar
        else{
            o.copia = JSON.parse(JSON.stringify(o));
            o.edi =true;            
        }
    };
    
    $scope.eliminarAreaHora = function(o,jor,i){
        if(($scope.matris2.grados[i].total - o.hora) + o.copia.hora > jor.hObligatoria ){
            modal.mensaje("ALERTA","la hora sobrepasa lo establecido por la jornada");
            return;
        }
            
        var request = crud.crearRequest('diseñoCurricular',1,'eliminarAreaHora');
        request.setData({areaHoraID:o.areaHoraID,jornadaID:jor.jornadaID ,gradoID:o.gradoID,areaID:o.areaID,hora:0});
        
        modal.mensajeConfirmacion($scope,"seguro que desea elimanar la hora asignada al area y al grado",function(){
            crud.eliminar("/cuadroHoras",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    $scope.matris2.grados[i].total = ($scope.matris2.grados[i].total - o.hora)+ o.copia.hora;
                    $scope.matris2.gradoAreas[o.areaPos][o.gradoPos].areaHoraID = response.data.areaHoraID;
                    $scope.matris2.gradoAreas[o.areaPos][o.gradoPos].hora = 0;
                    $scope.matris2.gradoAreas[o.areaPos][o.gradoPos].edi = null;
                    
                    var jorSel = buscarObjeto($scope.jornadas,'jornadaID',jor.jornadaID);
                    for(var k =0;jorSel.horas && k< jorSel.horas.length; k++ ){
                        if(jorSel.horas[k].gradoID == o.gradoID && jorSel.horas[k].areaID == o.areaID ){
                            jorSel.horas[k].hora = o.hora;
                            break;
                        }
                    }
                }
            },function(data){
                console.info(data);
            });
        },'400');
    }
    
    /*AREA HORA FIN*/
    
    
    $scope.tabla.calcularMatriz = function(){
        $scope.disenoActual = setting.dataset[$scope.tabla.page()-1];
        var disenoActual = $scope.disenoActual;
        if(disenoActual && disenoActual.grados ){
            var niveles = [];
            var nAnt = {id:0,c:1,nom:""};
            var ciclos = [];
            var cAnt = {id:0,c:1,nom:""};
            
            disenoActual.grados.forEach(function(item){

                if( nAnt.id == item.nivelID )
                    nAnt.c++;
                else{
                    nAnt = {id:item.nivelID,c:1,nom:item.nivel};
                    niveles.push(nAnt);
                }
                if( cAnt.id == item.cicloID )
                    cAnt.c++;
                else{
                    cAnt = {id:item.cicloID,c:1,nom:item.ciclo};
                    ciclos.push(cAnt);
                }
            });

            $scope.matris.niveles = niveles;
            $scope.matris.ciclos = ciclos;
            $scope.matris.c = disenoActual.grados.length; 
            
            $scope.matris2 = {};
        }
    };
    
    $scope.tabla.calcularMatrizDetalle = function(){
        $scope.matris.gradoAreas = [];
        if( $scope.disenoActual)
        $scope.disenoActual.gradoAreas.forEach(function (item){
            var areas = $scope.matris.gradoAreas[item.areaPos];
            if(!areas )
                areas = $scope.matris.gradoAreas[item.areaPos] = [];
            areas[item.gradoPos] = JSON.parse(JSON.stringify(item));
            areas[item.gradoPos].nombre = $scope.disenoActual.areas[item.areaPos].nombre;
            
        });
    };
    
    $scope.calcularMatriz2 = function(jornada){
        
        var disenoActual = $scope.disenoActual;
        if(jornada && disenoActual.grados ){
            
            var nivelID = jornada.nivelID;
            
            
            var grados = [];
            var areas = [];
            var ciclos = [];
            var cAnt = {id:0,c:1,nom:""};
            
            disenoActual.grados.forEach(function(item){

                if( nivelID == item.nivelID ){
                    item.total = 0;
                    grados.push(item);
                    if( cAnt.id == item.cicloID )
                        cAnt.c++;
                    else{
                        cAnt = {id:item.cicloID,c:1,nom:item.ciclo};
                        ciclos.push(cAnt);
                    }
                }
            });

            $scope.matris2.grados = grados;
            $scope.matris2.ciclos = ciclos;
            $scope.matris2.c = grados.length;
            
            $scope.matris2.gradoAreas = [];
            
            if(grados.length>0){
                var jorSel = buscarObjeto($scope.jornadas,'jornadaID',jornada.jornadaID);

                var posIni = grados[0].i;
                $scope.matris.gradoAreas.forEach(function(item,index){
                    if(item.length > 0 ){
                        var gradosN = [];
                        item.forEach(function(item2,index2){
                            if( nivelID == item2.nivelID ){
                                var nuevo = JSON.parse(JSON.stringify(item2));
                                nuevo.gradoPos = index2 - posIni;
                                nuevo.areaPos = $scope.matris2.gradoAreas.length;

                                nuevo.areaHoraID = 0;
                                nuevo.hora = 0;
                                for(var k =0;jorSel.horas && k< jorSel.horas.length; k++ ){
                                    if(jorSel.horas[k].gradoID == nuevo.gradoID && jorSel.horas[k].areaID == nuevo.areaID ){
                                        nuevo.hora = jorSel.horas[k].hora;
                                        nuevo.areaHoraID = jorSel.horas[k].areaHoraID;
                                        break;
                                    }
                                }
                                gradosN[index2 - posIni] = nuevo;
                                $scope.matris2.grados[index2 - posIni].total += nuevo.hora;

                            }
                        });                    
                        if(gradosN.length>0){
                            areas.push(disenoActual.areas[index].nombre);
                            $scope.matris2.gradoAreas.push(gradosN);
                        }
                    }
                });
            }
            $scope.matris2.areas = areas;
            
        }
    };
}]);
