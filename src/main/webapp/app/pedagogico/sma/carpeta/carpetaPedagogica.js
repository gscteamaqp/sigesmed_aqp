/**
 * Created by Administrador on 20/12/2016.
 */
/**
 * Created by Administrador on 20/12/2016.
 */
angular.module('app')
    .config(['$routeProvider',function($routeProvider){
        $routeProvider.when('/carpeta_maestro/:car_dig',{
            templateUrl:'pedagogico/sma/carpeta/detalle_carpeta_pedagogica.html',
            controller:'detalleCarpetaMaestroCtrl',
            controllerAs:'ctrl'
        })
    }])
    .controller('carpetaPedagogica',['$log','$location','$rootScope','NgTableParams','modal','UtilAppServices','crud',function($log,$location,$rootScope,NgTableParams,modal,util,crud){
        var self = this;
        self.tablaCarpetas = new NgTableParams({count:15},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });
        listarCarpetas();
        function listarCarpetas(){
            var request = crud.crearRequest('carpeta_digital',1,'listarCarpetas');
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.tablaCarpetas.settings().dataset = response.data;
                    self.tablaCarpetas.reload();
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde');
            });
        }
        self.verDetalles = function(row){
            $location.url('/carpeta_maestro/'+btoa(JSON.stringify(row)));
        }
    }]).controller('detalleCarpetaMaestroCtrl',['$log','$routeParams','$rootScope','$scope','NgTableParams','modal','UtilAppServices','crud',function($log,$routeParams,$rootScope,$scope,NgTableParams,modal,util,crud){
        var self = this;
        self.car = JSON.parse(atob($routeParams.car_dig));
        self.tipos = [{id:0,nom:"DREMO"},{id:1,nom:"UGEL"},{id:2,nom:"DIRECTOR IE"},{id:3,nom:"DOCENTE"}]
        cargarSecciones();
        function cargarSecciones(){
            var request = crud.crearRequest('carpeta_digital',1,'listarDetalleCarpetaDocente');
            request.setData({car:self.car.cod,org:$rootScope.usuMaster.organizacion.organizacionID,doc:$rootScope.usuMaster.usuario.usuarioID});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){

                    var auxData = _.groupBy(response.data,function(obj){
                        return obj.secNom;
                    });

                    self.secciones = [];
                    angular.forEach(auxData,function(obj,key){
                        self.secciones.push({nom:key,contenidos:new NgTableParams({count:15},{
                            counts: [],
                            paginationMaxBlocks: 13,
                            paginationMinBlocks: 2,
                            dataset:auxData[key]
                        })});
                    });
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
            });
        }
        self.mostrarTipo = function(tip){
            var index = _.findIndex(self.tipos,function(obj){
                return obj.id == tip;
            });
            if(index != -1) return self.tipos[index].nom;
            return "";
        }
        self.subirArchivoCarpeta = function(row){
            var resolve = {
                data : function () {
                    return row;
                }
            }
            var modalInstance = util.openModal('nuevo_archivo_carpeta.html','registrarArchivoCarpetaCtrl','lg','ctrl',resolve);
            modalInstance.result.then(function(archivo){
               angular.extend(row,archivo);
            },function(cdata){});
        }
    }])
    .controller('registrarArchivoCarpetaCtrl',['$log','NgTableParams','$uibModalInstance','$rootScope','modal','data','crud',function($log,NgTableParams,$uibModalInstance,$rootScope,modal,data,crud){
        var self = this;
        self.arc = new Object();
        angular.extend(self.arc,data);
        self.cancelar = function () {
            $uibModalInstance.dismiss('cancel');
        }

        self.save = guardar;

        function guardar() {
            self.arc.org = $rootScope.usuMaster.organizacion.organizacionID;
            self.arc.doc = $rootScope.usuMaster.usuario.usuarioID;
            var request = crud.crearRequest('carpeta_digital',1,'registrarArchivoCarpeta');
            request.setData(self.arc);
            crud.insertar('/maestro',request,function(response){
                if(response.responseSta){
                    angular.extend(self.arc,response.data);
                    $uibModalInstance.close(self.arc);
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){modal.mensaje('ERROR','El servidor no responde');});
        }
    }]);