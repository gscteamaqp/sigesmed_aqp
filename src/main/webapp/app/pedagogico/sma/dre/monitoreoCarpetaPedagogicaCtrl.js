app.controller("monitoreoCarpetaPedagogicaCtrl",["$rootScope","$scope","NgTableParams","crud","modal",function ($rootScope,$scope,NgTableParams,crud,modal){

    
    //Listar Monitoreos
    
    $scope.nuevoMonitoreo = {monId:"", codigo:"",anio:"",nombreMon:"",nombreURL:"" ,FechaRegistro:"", datosIE:"", nombreIE:"" ,nivelIE:"" ,numeroEsp:"" , numeroDoc:"" }; 
    
    var paramsMonitoreos = {count: 10};
    var settingMonitoreos = {counts: []};
    $scope.tablaMonitoreos = new NgTableParams(paramsMonitoreos, settingMonitoreos);
    
    $scope.listarMonitoreos = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('programacion_monitoreo', 1, 'listarMonitoreosPorUgel');
            //ugelID es el dato que necesito
            //usuMaster es el usuario actual
            //organizacion es la organizacion del usuario
            //organizacionID es la ID de la organizacion del usuario
            request.setData({ugelId:$rootScope.usuMaster.organizacion.organizacionID});
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sma", request, function (data) {
                settingMonitoreos.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones(settingMonitoreos.dataset);
                 $scope.tablaMonitoreos.settings(settingMonitoreos);
            }, function (data) {
                console.info(data);
            });
    };
    
    //Listar Monitoreos Detalle
    
    $scope.nuevoMonitoreoDetalle = {especialista:"",docente:"",area:"",grado:"" ,seccion:"", fechaDeVisita:"" ,etapa:"" }; 
    var paramsMonitoreoDetalle = {count: 10};
    var settingMonitoreoDetalle = {counts: []};
    $scope.tablaMonitoreoDetalle = new NgTableParams(paramsMonitoreoDetalle, settingMonitoreoDetalle);
    
    $scope.monitoreoSeleccionado = {};
   
    $scope.listarMonitoreoDetalle = function (monId) {
            
            //preparamos un objeto request
            var request = crud.crearRequest('programacion_monitoreo', 1, 'listarMonitoreoDetalle');
            //
            request.setData({monId:monId});
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sma", request, function (data) {
                settingMonitoreoDetalle.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones(settingMonitoreoDetalle.dataset);
                $scope.tablaMonitoreoDetalle.settings(settingMonitoreoDetalle);
                $('#modalNuevoDetalle').modal('show'); 
                 
            }, function (data) {
                console.info(data);
            });
    };
    
    
    $scope.listarIEs = function () {
            
            //preparamos un objeto request
            var request = crud.crearRequest('programacion_monitoreo', 1, 'listarInstitucionesPorUgel');
            //
            request.setData({ugelId:$rootScope.usuMaster.organizacion.organizacionID});
            
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sma", request, function (data) {
                console.log(JSON.stringify(data));
                $scope.institucionesEducativas = data.data;
                 
            }, function (data) {
                console.info(data);
            });
    };
    
    $scope.agregarMonitoreo = function () {
            var request = crud.crearRequest('programacion_monitoreo', 1, 'agregarMonitoreo');
            request.setData($scope.nuevoMonitoreo);
            crud.insertar("/sma", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    insertarElemento(settingMonitoreos.dataset, response.data);
                    $scope.tablaMonitoreos.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
    
    //graficas 

    $scope.labels = ['Inicial', 'Primaria', 'Secundaria'];
    $scope.series = ['Monitoreos Totales', 'Monitoreos Realizados'];

    $scope.data = [
     [50, 100, 50, 0 ],
        [25, 25, 30, 0]
    ];
    
    //graficas 1
    
    $scope.labels1 = ['Inicial', 'Primaria', 'Secundaria'];
    $scope.series1 = ['Primera Visita', 'Segunda Visita', 'Tercera Visita'];

    $scope.data1 = [
     [9, 3, 9, 0 ],
     [7, 6, 17, 0 ],
     [8, 4, 3, 0]
    ];

    
    
   
   
}]);