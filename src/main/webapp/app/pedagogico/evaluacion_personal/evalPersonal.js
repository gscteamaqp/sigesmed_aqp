/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var mepEvalApp = angular.module('app');
mepEvalApp.config(['$routeProvider',function($routeProvider){
    $routeProvider.when('/evaluacionPersonal/nueva_evaluacion',{
            templateUrl:'pedagogico/evaluacion_personal/nueva_evaluacion.html',
            controller:'nuevaEvaluacionCtrl',
            controllerAs:'ctrlEvaluacionPer'
        })
        .when('/evaluacionPersonal/resumen_evaluacion',{
            templateUrl:'pedagogico/evaluacion_personal/resumen_evaluacion.html',
            controller:'resumenEvaluacion',
            controllerAs:'ctrlresumenEvaluacion'
        });
}]);
mepEvalApp.factory('mepServiceEvaluacion', ['$log','$uibModal','$mdDialog','crud',function ($log,$uibModal,$mdDialog,crud) {
    var fichas_evaluacion = null;
    var employee = {};
    return {
        setCurrentEmployee : function(e){
            employee = e;
        },
        getCurrentEmployee : function(){
            return employee;
        },
        changeSelection: function(row,preSelection){
            if (preSelection) {
                preSelection.$selected = false;
                //row.$selected = true;
                //preSelection = row;
            }
            if (preSelection === row) {
                row.$selected = false;
                preSelection = null;
            }
            else{
                row.$selected = true;
                preSelection = row;
            }
            return preSelection;
        },
        openModal: function(nameHtml,nameCtrl,size,alias,params){
            var modalInstance = $uibModal.open({
                animation:true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                keyboard:true,
                backdrop:true,
                templateUrl: nameHtml,
                controller: nameCtrl,
                controllerAs:alias,
                size: size,
                resolve:params
            });
            return modalInstance;
        },
        openDialog: function (title,content,ev,succ,err) {
            var confirm = $mdDialog.confirm()
                .title(title)
                .textContent(content)
                .ariaLabel('Lucky day')
                .targetEvent(ev)
                .ok('Aceptar')
                .cancel('Cancelar');
            $mdDialog.show(confirm).then(succ,err);
        },
        insertEvaluacionService: function(data,type,succ,err){
            var request  = crud.crearRequest('mep',1,'registrarEvaluacion');
            request.setMetadataValue('type',type);
            request.setData(data);
            console.log(data);
            crud.insertar('/evaluacion_personal',request,succ,err);
        },
        compartirEvaluacionService: function(data,succ,err){
            var request  = crud.crearRequest('mep',1,'compartirEvaluacion');
            request.setMetadataValue('id',data);
            crud.actualizar('/evaluacion_personal',request,succ,err);
        },
        getReportService: function (id,type,succ,err) {
            var request  = crud.crearRequest('mep',1,'getReportEvaluacion');
            request.setMetadataValue('resumen',''+id);
            crud.listar('/evaluacion_personal',request,succ,err);
        },
        getDetalleEvaluacionPersonal: function(idResumen,type,succ,err){
            var request = crud.crearRequest('mep',1,'listarDetallesEvaluacion');
            request.setMetadataValue('id',angular.isNumber(idResumen)? '' + idResumen : idResumen);
            request.setMetadataValue('ty',type);
            crud.listar('/evaluacion_personal',request,succ,err);
        },
        getDataEstadistica : function (type,data,succ,err) {
            var request = crud.crearRequest('mep',1,'getDataEstadistica');
            request.setMetadataValue('type',type);
            request.setData(data);
            crud.listar('/evaluacion_personal',request,succ,err);
        }
    }
}]);
mepEvalApp.controller('evalPersonal',['$log','$location','$window','crud','NgTableParams','mepServiceEvaluacion','modal',function($log,$location,$window,crud,NgTableParams,mepServiceEvaluacion,modal){
    var self = this;
    self.personalIE = new NgTableParams(
        {count:15},
        {
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        }
    );
    function listarPersonal(){
        var organizacion = JSON.parse( window.atob(localStorage.getItem('organizacion')));
        var request = crud.crearRequest('mep',1,'listarTrabajadores');
        request.setMetadataValue('institucion',organizacion.organizacionID);
        crud.listar('/evaluacion_personal',request,function(response){
            self.personalIE.settings().dataset = response.data;
            self.personalIE.reload();
        },function(response){});
    }
    listarPersonal();
    var preSelection = null;
    self.changeSelection = function(row){
        preSelection = mepServiceEvaluacion.changeSelection(row,preSelection);
    };
    self.evaluarPersonal = function(row){
        /*if(!preSelection) {
            $window.alert('Debe seleccionar un trabajador');
            return
        }*/
        mepServiceEvaluacion.setCurrentEmployee(row);
        $location.url('/evaluacionPersonal/nueva_evaluacion');
    };
    self.obtenerResumen = obtenerResumen;
    function obtenerResumen(row){
        //var idTrabajador = row.tra;
        mepServiceEvaluacion.setCurrentEmployee(row);
        $location.url('/evaluacionPersonal/resumen_evaluacion');
    }
    self.showEstadIndiv = estadIndiv;
    function estadIndiv(row){
        var data = {tra:row.tra};
        mepServiceEvaluacion.getDataEstadistica('indiv',data,function(response){
            var resolve = {
                data : function (){
                    response.data.car = row.car;
                    return response.data;}
            }
            
            mepServiceEvaluacion.openModal('mod_estad_indiv.html','estadIndivCtrl','lg','ctrl',resolve);
        },function(response){
            modal.mensaje("ERROR","No se pudo obtener los datos");
        });
    }
    self.showEstadGener = estadGrup;
    function estadGrup(){
        var data = {org:JSON.parse( window.atob(localStorage.getItem('organizacion'))).organizacionID}
        mepServiceEvaluacion.getDataEstadistica('grup_rol',data,function(response){
            var resolve = {
                data : function (){return response.data;}
            }
            mepServiceEvaluacion.openModal('mod_estad_grup.html','estadGroupCtrl','lg','ctrl',resolve);
        },function(response){});
    }
}]);
 mepEvalApp.controller('nuevaEvaluacionCtrl',['$log','$window','$location','$uibModal','mepServiceEvaluacion','crud','modal',function($log,$window,$location,$uibModal,mepServiceEvaluacion,crud,modal){

    var self = this;
    self.showProgress = false;
    self.employee = mepServiceEvaluacion.getCurrentEmployee();
    self.etapasEvaluacion =[
        {nom:'Inicio',abr:'i',selected:true},
        {nom:'Proceso',abr:'p',selected:false},
        {nom:'Fin',abr:'f',selected:false}];
    var etapaSeleccionada = self.etapasEvaluacion[0].abr;
     self.selectEtapa = function(e,i){
         angular.forEach(self.etapasEvaluacion,function(e,index){
             if(i !== index)
                 e.selected = false;
         });
         if(e.selected) etapaSeleccionada = e.abr;
     }
    function getFichaEvaluacion(car){
        if(!car){
            $location.url('/evaluacionPersonal/');
        };
        self.showProgress = true;
        var request = crud.crearRequest('mep',1,'ObtenerFichaEvaluacionByRol');
        request.setMetadataValue('tipo',car);
        crud.listar('/evaluacion_personal',request,function(response){
            $log.log('data enviada',response);
            if(response.response === 'BAD'){
                $location.url('/evaluacionPersonal/');
                modal.mensaje('ERROR',response.responseMsg);
            }
            else{
                self.fichas_evaluacion = response.data;
                self.fichas_evaluacion.cont = _.orderBy(self.fichas_evaluacion.cont,['tip','nom'],['desc']);
                self.fichas_evaluacion.esc = _.orderBy(self.fichas_evaluacion.esc,['val'],['asc']);
                _.forEach(self.fichas_evaluacion.cont,function(c){
                    if(c.tip !== 'd'){
                        _.forEach(c.indicadores,function(ind){
                            ind.val = 0;
                            ind.esc = angular.copy(self.fichas_evaluacion.esc);
                        });
                    }else if(c.tip === 'd'){
                        var hijos = c.cont;
                        _.forEach(hijos,function(h){
                            _.forEach(h.indicadores,function(i){
                                i.val = 0;
                                i.esc = angular.copy(self.fichas_evaluacion.esc);
                            });
                        });
                    }

                });
            }
            self.showProgress = false;
        },function(response){
            self.showProgress = false;
            modal.mensaje("ERROR","El servidor no response");
        });
    };
    getFichaEvaluacion(self.employee.car);
    function evaluarDocente(fdominios,frangos,traId){
        var sumDominio = Array.apply(null, new Array(fdominios.length)).map(Number.prototype.valueOf,0);
        var detalle = [];
        var inds = [];

        for(var j = 0; j < sumDominio.length; j++ ){
            var d = fdominios[j];
            _.forEach(d.cont,function(c){
                var sumIndi = 0;
                _.forEach(c.indicadores,function(i){
                        sumIndi += i.val;
                        sumDominio[j] += i.val;
                        inds.push({id:i.id,val:i.val});
                    }
                );
                c.sumComp = sumIndi;
                //c.detalle = detalle;
                //detalle.push({tip:'c',nom:c.nom,subt:sumIndi,dt:sumIndi});
                detalle.push({tip: c.tip,nom: c.nom, dt: c.sumComp,subt:c.sumComp,con:c.id});
            });
            if(d.nom === 'Enseñanza para el aprendizaje de los estudiantes') {
                sumDominio[j] /= 2;
            }
            d.sumDom =  sumDominio[j];
            detalle.push({tip: d.tip,nom: d.nom, dt: d.sumDom,subt:d.sumDom,con:d.id});
        }
        var tot = _.sum(sumDominio)/sumDominio.length;
        var des;
        for(var i = 0; i < frangos.length; i++ ){
            var rango = frangos[i];
            var auxtot =  Math.round(tot);
            if((auxtot >= rango.min) && (auxtot <= rango.max)){
                des = rango.des;
                break;
            }
        }
        return{
            total : tot,
            des:des,
            etapa: etapaSeleccionada,
            inds:inds,
            sumaDom:sumDominio,
            employee :traId,
            dominios: fdominios,
            detalle: detalle,
            save:true
        }
    }
    function obtenerEvaluacion(){
        var resultDet = {};
        var detalle = [];
        var fsubTotal = 0;
        var fcant = 0;
        var inds = [];
        var fvals = Array.apply(null, new Array(self.fichas_evaluacion.esc.length)).map(Number.prototype.valueOf,0);
        _.forEach(self.fichas_evaluacion.cont,function(c){
            if(c.tip === 'f'){
                _.forEach(c.indicadores,function(ind){
                    inds.push({id:ind.id,val:ind.val});
                    fsubTotal += ind.val;
                    fvals[ind.val-1] += ind.val;
                    fcant++;
                });
            }else if(c.tip === 'c'){
                var csubTotal = 0;
                var cvals = Array.apply(null, new Array(self.fichas_evaluacion.esc.length)).map(Number.prototype.valueOf,0);
                var ccant = 0;
                _.forEach(c.indicadores,function(ind){
                    inds.push({id:ind.id,val:ind.val});
                    csubTotal += ind.val;
                    cvals[ind.val-1] += ind.val;
                    ccant++;
                });
                detalle.push({tip:'c',nom:c.nom,subt:csubTotal,vals:cvals,cant:ccant,punt:csubTotal*(100/(ccant*self.fichas_evaluacion.esc.length)),con: c.id});
            }
        });
        detalle.push({tip:'f',nom:'Funciones',subt:fsubTotal,vals:fvals,cant:fcant,punt:fsubTotal*(100/(fcant*self.fichas_evaluacion.esc.length))});
        var tipContenidos = _.partition(self.fichas_evaluacion.cont,function(o){return o.tip === 'c'});
        resultDet.total = 0;
        _.forEach(detalle,function(det){
            if(det.tip === 'c'){
                det.dt = det.punt*0.1;
            }else if(det.tip === 'f'){
                det.dt = det.punt * (1-tipContenidos[0].length*0.1);
            }
            resultDet.total += det.dt;
        });
        for(var i = 0; i < self.fichas_evaluacion.ran.length; i++ ){
            var rango = self.fichas_evaluacion.ran[i];
            if((resultDet.total >= rango.min) && (resultDet.total <= rango.max)){
                resultDet.des = rango.des;
                break;
            }
        }
        resultDet.etapa =  etapaSeleccionada;
        resultDet.inds = inds;
        resultDet.detalle = detalle;
        resultDet.employee = self.employee.tra;
        return resultDet;
    }
    self.evaluarTrabajador = function(){
        if(self.employee.car == 'Docente'){
            var docModal = mepServiceEvaluacion.openModal('pedagogico/evaluacion_personal/mod_resumen_evaluacion_docente.html','evaluarDocenteCtrl','lg','ctrl',{data:function(){return evaluarDocente(self.fichas_evaluacion.cont,self.fichas_evaluacion.ran,self.employee.tra)}});
            docModal.result.then(function(response){
                $location.url('/evaluacionPersonal/');
            },function(response){
            });
        }else{
            var perModal = mepServiceEvaluacion.openModal('calificacion_evaluacion.html','calificacionEvaluacionCtrl','lg','ctrlCalificacion',{
                resumen: function(){return obtenerEvaluacion()},
                columnas: function(){return self.fichas_evaluacion.esc}
            });
            perModal.result.then(function(response){
                $location.url('/evaluacionPersonal/');
            },function(response){
                
            });
        }
    };
    self.setValue = function (cont,ind,esc,position,dom){
        angular.forEach(ind.esc,function(col,index){
            if(position !== index)
                col.isSelected = false;
        });
        if(esc.isSelected){
            var oCont = dom ? _.find(dom.cont,{id:cont.id}) :_.find(self.fichas_evaluacion.cont,{id:cont.id});
            _.find(oCont.indicadores,{id:ind.id}).val = esc.val;
        }
        
    }
 }]);
mepEvalApp.controller('estadIndivCtrl',['$log','$uibModalInstance','data','$scope',function($log,$uibModalInstance,data,$scope){
    var self = this;
    self.labels = data.labels;
    self.data = data.values;
    self.series = data.series;
    self.dataset = [{ yAxisID: 'y-axis-1', lineTension:0}];
    self.options = {
        scales:{
            yAxes:[
                {
                    id: 'y-axis-1',
                    type: 'linear',
                    display: true,
                    position: 'left',
                    ticks: {
                       min: 0, 
                       max: 100
                    }
                }
            ]
        }
    };
    if (data.car === "Docente"){
        self.options.scales.yAxes[0].ticks.max = 20;
    }
    self.aceptar = function(){
        $uibModalInstance.close('close');
    };

}]);
mepEvalApp.controller('estadGroupCtrl',['$log','$uibModalInstance','data','$scope','$timeout',function($log,$uibModalInstance,data,$scope,$timeout){
    var self = this;
    self.labels = data.labels;
    self.series = data.series;
    self.data = data.values;
    self.aceptar = function(){
        $uibModalInstance.close('close');
    }
}]);
mepEvalApp.controller('calificacionEvaluacionCtrl',['$log','$uibModalInstance','resumen','columnas','mepServiceEvaluacion','modal',function($log,$uibModalInstance,resumen,columnas,mepServiceEvaluacion,modal){

    var self = this;
    self.optionsg = [{id:1,nom:"puntajes"},{id:2,nom:"Resultados"}];
    self.showProgress = false;
    self.resumen = resumen;
    self.cols = columnas;
    self.registroRealizado = false;
    var resumenid;
    self.guardar = function(){
        if(!self.registroRealizado){
            self.showProgress = true;
            mepServiceEvaluacion.insertEvaluacionService(resumen,'other',function(response){
                self.showProgress = false;
                if(response.response == 'OK'){
                    self.registroRealizado = true;
                    resumenid = response.data[0];
                    modal.mensaje("CONFIRMACION","Se realizó correctamente el registo");
                }else{
                    modal.mensaje("ERROR","No se realizó el registro");
                    $uibModalInstance.dismiss(response);
                }
            },function(response){
                self.showProgress = false;
                modal.mensaje("ERROR","NO se pudo realizar la transacción");
                $uibModalInstance.dismiss(response);
            });
        }else{
            $uibModalInstance.close('close');
        }
    }
    self.cancelar = function(){
        $uibModalInstance.dismiss('cancel');
    }
    self.compartir = function(){
        if(resumenid){
            mepServiceEvaluacion.compartirEvaluacionService(''+resumenid,function(response){
                if(response.response === 'OK'){
                    modal.mensaje("CONFIRMACION","Se compartio el archivo con el usuario evaluado");
                    $uibModalInstance.close('close');
                }else{
                    modal.mensaje("ERROR","NO se pudo compartir los resultados");
                    $uibModalInstance.dismiss(response);
                }
            },function(response){
                modal.mensaje("ERROR","NO se pudo compartir los resultados");
                $uibModalInstance.dismiss(response);
            });
        }
    }
}]);
mepEvalApp.controller('evaluarDocenteCtrl',['$log','$uibModalInstance','data','mepServiceEvaluacion','modal',function($log,$uibModalInstance,data,mepServiceEvaluacion,modal){
    var self = this;
    self.showProgress = false;
    self.data = data;
    self.registroRealizado = false;
    self.namebutton =  (self.data.employee)? 'Guardar' : 'Aceptar';
    self.showbuttonShare = (self.data.employee)? true : false;
    var resumen;
    self.aceptar = (self.data.employee)? guardarEvaluacion : function(){
        $uibModalInstance.close('close');
    }
    function guardarEvaluacion(){
        if(!self.registroRealizado){
            self.showProgress = true;
            mepServiceEvaluacion.insertEvaluacionService(data,'docente',function(response){
                self.showProgress = false;
                if(response.response == 'OK'){
                    self.registroRealizado = true;
                    resumen = response.data[0];
                    modal.mensaje("CONFIRMACION","Se realizó correctamente el registo");
                }else{
                    modal.mensaje("ERROR","No se realizó el registro");
                    $uibModalInstance.dismiss(response);
                }
            },function(response){
                self.showProgress = false;
                modal.mensaje("ERROR","NO se pudo realizar la transacción");
                $uibModalInstance.dismiss(response);
            });
        }else{
            $uibModalInstance.close('close');
        }

    }
    self.cancelar = function(){
        $uibModalInstance.dismiss('cancel');
    }
    self.compartir = function(){
        if(resumen){
            mepServiceEvaluacion.compartirEvaluacionService(''+resumen,function(response){
                if(response.response === 'OK'){
                    modal.mensaje("CONFIRMACION","Se compartio el archivo con el usuario evaluado");
                    $uibModalInstance.close('close');
                }else{
                    modal.mensaje("ERROR","NO se pudo compartir los resultados");
                    $uibModalInstance.dismiss(response);
                }
            },function(response){
                modal.mensaje("ERROR","NO se pudo compartir los resultados");
                $uibModalInstance.dismiss(response);
            });
        }
    }
}]);
mepEvalApp.controller('resumenEvaluacion',['$log','$window','$location','$uibModal','NgTableParams','mepServiceEvaluacion','crud','modal',function($log,$window,$location,$uibModal,NgTableParams,mepServiceEvaluacion,crud,modal){
    if(!mepServiceEvaluacion.getCurrentEmployee() || (!mepServiceEvaluacion.getCurrentEmployee().tra)){
        $location.url('/evaluacionPersonal/');
        return;
    }
    var self = this;
    self.employee = mepServiceEvaluacion.getCurrentEmployee();
    self.resumenes = new NgTableParams({count:10},{
        counts: [],
        paginationMaxBlocks: 13,
        paginationMinBlocks: 5,
        dataset: []
    });
    obtenerResumenTrabajador(self.employee.tra);
    function obtenerResumenTrabajador(idTrab){
        var request = crud.crearRequest('mep',1,'listarResumenEvaluacion');
        request.setMetadataValue('trabajador',''+idTrab);
        crud.listar('/evaluacion_personal',request,function(response){
            self.resumenes.settings().dataset = response.data;
            self.resumenes.reload();
        },function(response){
            modal.mensaje("ERROR","No se pudo obtener la lista de evaluaciones")
        });
    }
    var preSelection = null;
    self.changeSelection = changeSelection;
    function changeSelection(row){
        preSelection = mepServiceEvaluacion.changeSelection(row,preSelection);
    }
    self.obtenerDetalle = obtenerDetalle;
    function obtenerDetalle(row){

        self.showProgress = true;
        var currentCargo = self.employee.car;
        if(currentCargo === 'Docente')
            mepServiceEvaluacion.getDetalleEvaluacionPersonal(row.det,self.employee.car,function(response){
                self.showProgress = false;
                var modalInstance = mepServiceEvaluacion.openModal('pedagogico/evaluacion_personal/mod_resumen_evaluacion_docente.html','evaluarDocenteCtrl','lg','ctrl',{data:function(){return response.data;}});
            },function(response){
                self.showProgress = false;
            });
        else
            mepServiceEvaluacion.getDetalleEvaluacionPersonal(row.det,self.employee.car,function(response){
                self.showProgress = false;
                var modalInstance = mepServiceEvaluacion.openModal('modal_detalle_evaluacion.html','modalDetalleEvaCtrl','lg','ctrl',{data:function(){return response.data;}});
            },function(response){
                self.showProgress = false;
            });
    }
    self.verReporte = verReporte;
    function verReporte(row){
        mepServiceEvaluacion.getReportService(row.det,'',function (response) {
            if(response.response === 'OK')
                window.open(response.data[0]);
            else{
                modal.mensaje("ERROR","No se pudo obtener el reporte")
            }

        },function (response) {

        });
    }
}]);
mepEvalApp.controller('modalDetalleEvaCtrl',['$log','$uibModalInstance','data',function($log,$uibModalInstance,data){
    var self = this;
    self.data = data;
    self.ok = ok;
    self.cancel = cancel;
    calcularSumaIndicadores(self.data.dets,self.data.cols,self.data.inds);
    function ok(){
        $uibModalInstance.close('');
    }
    function cancel(){
        $uibModalInstance.dismiss('cancel');
    }
    function calcularSumaIndicadores(dets,escalas,inds){

        var data = _.partition(inds,function(o){return o.tip === 'f'}) ;
        _.forEach(dets,function(d){
            var cv = d.tip === 'f' ? data[0] : obtenerContenido(data[1],d.tip,d.nom);
            d.sum = sumarIndicadores(escalas.length,cv);

        });
    }
    function obtenerContenido(data,tip,nom){
        return _.find(data,function(c){
            return (c.tip === tip && c.nom === nom);
        });
    }
    function sumarIndicadores(esSize,contenido) {
        if(!contenido) return;
        var sumResumen = Array.apply(null, new Array(esSize)).map(Number.prototype.valueOf,0);
        if(Object.prototype.toString.call(contenido) === '[object Array]'){
            _.forEach(contenido,function(f){
                _.forEach(f.indicadores,function(i){
                    sumResumen[i.pun-1] += i.pun;
                });
            });
        }else{
            var indicadores = contenido.indicadores;
            _.forEach(indicadores,function(i){
                sumResumen[i.pun-1] += i.pun;
            });
        }

        return sumResumen;
    }
}]);