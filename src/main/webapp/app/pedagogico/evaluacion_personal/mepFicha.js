/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var mepApp = angular.module('app');
mepApp.config(['$routeProvider',function($routeProvider){
        $routeProvider.when('/fichaEvaluacion/:ficha_id/:nom_fic',{
            templateUrl:'pedagogico/evaluacion_personal/detalle_ficha.html',
            controller:'detalleFichaCtrl',
            controllerAs:'ctrlDetalle'
        })
        .when('/evaluacionPersonal/nueva_evaluacion',{
            templateUrl:'pedagogico/evaluacion_personal/nueva_evaluacion.html',
            controller:'nuevaEvaluacionCtrl',
            controllerAs:'ctrlEvaluacionPer'
        })
        .when('/evaluacionPersonal/resumen_evaluacion',{
            templateUrl:'pedagogico/evaluacion_personal/resumen_evaluacion.html',
            controller:'resumenEvaluacion',
            controllerAs:'ctrlresumenEvaluacion'
        });
}]);
mepApp.factory('mepService', ['$log','$uibModal','$mdDialog','crud',function ($log,$uibModal,$mdDialog,crud) {
    var fichas_evaluacion = null;
    var employee = {};
    return {
        listarFichasEvaluacion: function(){
            if(!fichas_evaluacion){
                var request = crud.crearRequest('mep',1,'listarFichas');
                request.setMetadataValue('order','fecha');
                crud.listar('/evaluacion_personal',request,function(response){
                    fichas_evaluacion = response.data;
                    
                },function(response){
                    
                });
            }
        },
        getFichasEvaluacion :function(){
            return fichas_evaluacion;
        },
        setCurrentEmployee : function(e){
            employee = e;
        },
        getCurrentEmployee : function(){
            return employee;
        },
        changeSelection: function(row,preSelection){
            if (preSelection) {
                preSelection.$selected = false;
                //row.$selected = true;
                //preSelection = row;
            }
            if (preSelection === row) {
                row.$selected = false;
                preSelection = null;
            }
            else{
                row.$selected = true;
                preSelection = row;
            }
            return preSelection;
        },
        openModal: function(nameHtml,nameCtrl,size,alias,params){
            var modalInstance = $uibModal.open({
                animation:true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                keyboard:true,
                backdrop:true,
                templateUrl: nameHtml,
                controller: nameCtrl,
                controllerAs:alias,
                size: size,
                resolve:params
            });
            return modalInstance;
        },
        openDialog: function (title,content,ev,succ,err) {
            var confirm = $mdDialog.confirm()
                .title(title)
                .textContent(content)
                .ariaLabel('Lucky day')
                .targetEvent(ev)
                .ok('Aceptar')
                .cancel('Cancelar');
            $mdDialog.show(confirm).then(succ,err);
        },
        deleteElementService : function(tx,id,type,succ,err){
            var request  = crud.crearRequest('mep',1,tx);
            request.setMetadataValue("id",id);
            request.setMetadataValue("tipo",type);
            crud.eliminar('/evaluacion_personal',request,succ,err);
        },
        updateElementService: function(element,type,succ,err){
            var request = crud.crearRequest('mep',1,'updateElementoFicha');
            request.setMetadataValue("type",type);
            request.setData(element);
            crud.actualizar('/evaluacion_personal',request,succ,err);
        }
    }
}]);
mepApp.controller('mepFicha',['$log','$location','$uibModal','NgTableParams','crud','mepService',function ($log,$location,$uibModal,NgTableParams,crud,mepService){
    var self = this;
    self.showProgress = false;
    self.org = JSON.parse( window.atob(localStorage.getItem('organizacion')));
    self.user = JSON.parse( window.atob(localStorage.getItem('usuario')));
    self.rol = JSON.parse( window.atob(localStorage.getItem('rol')));
    
    self.tableFichaParams = new NgTableParams({count:8},{
        counts: [],
        paginationMaxBlocks: 13,
        paginationMinBlocks: 2,
        dataset:[]
    });
    function listarFichas(){
        var request = crud.crearRequest('mep',1,'listarFichas');
        request.setMetadataValue('order','fecha');
        crud.listar('/evaluacion_personal',request,function(response){
            //self.fichas = response.data;
            self.tableFichaParams.settings().dataset = response.data;
            self.tableFichaParams.reload();
        },function(response){

        });
    }
    listarFichas();
    function deleteElementNgTable(ngtable,row,tx){
        self.showProgress = true;
        mepService.deleteElementService(tx,''+row.id,'ficha',function (response) {
            switch(response.response){
                case "OK":
                    _.remove(ngtable.settings().dataset,function(item){
                        return row === item;
                    });
                    ngtable.reload();
                    break;
                case "BAD":
                    //mostrar mensaje que el servidor no realizao correctamente la eliminacion
                    break;
            }
            self.showProgress = false;
        },function (response) {
            self.showProgress = false;
            //mostrar mensaje en caso de que haya un error de coneccion
        });
    }
    self.deleteFicha = function(ev,row){
        mepService.openDialog('¿Estás SEGURO de eliminar la ficha?','Se eliminará la ficha y todos sus detalles',ev,
            function () {
                deleteElementNgTable(self.tableFichaParams,row,'deleteFichaEvaluacion');
            },function(){});
    }
    self.viewDetalles = function(fic){
        $location.url('/fichaEvaluacion/'+fic.id+'/'+fic.tipcod);
    }
    self.registrarFicha = function(){
        var modalInstance = $uibModal.open({
            animation:true,
            keyboard:true,
            backdrop:true,
            templateUrl: 'nuevaFicha.html',
            controller: 'nuevaFichaCtrl',
            controllerAs:'ctrlNuevaFicha',
            resolve:{
                tipos: function(){
                    return self.tipoPersonal;
                }
            }
        });
        modalInstance.result.then(function(ficha){
            self.tableFichaParams.settings().dataset.push(ficha.data);
            self.tableFichaParams.reload();
        },function(){
            
        });
    }
    

}]);
mepApp.controller('nuevaFichaCtrl',['$uibModalInstance','tipos','urls','crud','modal',function($uibModalInstance,tipos,urls,crud,modal){
    var self = this;
    ///self.ltipos = tipos;
    listarCargos();
    function listarCargos(){
        var request  = crud.crearRequest('mep',1,'listarCargosTrabajador');
        crud.listar('/evaluacion_personal',request,function (response) {
            if(response.response === 'OK'){
                self.ltipos = response.data;
            }else if(response.response === 'BAD'){
                modal.mensaje('ERROR',response.responseMsg);
            }
        },function (response) {
            modal.mensaje('ERROR','El servidor no responde');
        });
    }
    self.guardar = function(){
        var request  = crud.crearRequest('mep',1,'registrarFicha');
        //request.setCmd('mep',1,'registrarFicha');
        request.setData(self.ficha);
        crud.insertar('/evaluacion_personal',request,
            function(response){
                if(response.response === 'OK'){
                    modal.mensaje("CONFIRMACION","Se hizo el registro correctamente");
                    $uibModalInstance.close(response);
                }else if(response.response === 'BAD'){
                    modal.mensaje("ERROR",response.responseMsg);
                }

            },
            function(response){
                modal.mensaje("ERROR","No se pudieron obtener los datos");
                $uibModalInstance.dismiss('cancel');
            });
    }
    self.cancelar = function(){
        $uibModalInstance.dismiss('cancel');
    }
}]);
mepApp.controller('progressCircleCtrl',['$uibModalInstance',function($uibModalInstance){
    
}]);
mepApp.controller('detalleFichaCtrl',['$routeParams','$log','$uibModal','NgTableParams','crud','mepService',function($routeParams,$log,$uibModal,NgTableParams,crud,mepService){
    
    var self = this;
    self.showProgress = false;
    var ficha = $routeParams.ficha_id;
    var originalData = {};
    self.tipf = $routeParams.nom_fic;

    self.contenidoActual = {selected:false};
    self.elementosFicha = {
        escalas: new NgTableParams({count:3},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        }),
        rangos: new NgTableParams({count:3},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        }),
        funciones: new NgTableParams({count:3},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        }),
        competencias: new NgTableParams({count:3},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        }), 
        indicador:new NgTableParams({count:10},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        })
    }
    function detalleFicha(){
        var request = crud.crearRequest('mep',1,'getDetalles');
        request.setMetadataValue('fic',''+ficha);
        crud.listar('/evaluacion_personal',request,function(response){

            if(response.response === 'succ'){                                
                self.detalle = response.data;
                var now = new Date(self.detalle.fichaDetails.vig);
                self.detalle.fichaDetails.vig = now;
                self.desTmp = self.detalle.fichaDetails.des; 
                self.vigTmp = self.detalle.fichaDetails.vig;
//                self.vigTmpNew = ; 
                originalData = angular.copy(response.data);
                self.elementosFicha.escalas.settings().dataset = angular.copy(originalData.escalas);
                self.elementosFicha.escalas.reload();
                
                self.elementosFicha.rangos.settings().dataset = angular.copy(originalData.rangos);
                self.elementosFicha.rangos.reload();
                if(Number(self.tipf) !== 1){
                    var tipContenidos = _.partition(originalData.contenido,function(o){return o.tip === 'f'});
                    self.elementosFicha.funciones.settings().dataset = tipContenidos[0];
                    self.elementosFicha.competencias.settings().dataset = tipContenidos[1];
                    self.elementosFicha.funciones.reload();
                    self.elementosFicha.competencias.reload();
                }else if(Number(self.tipf) === 1){
                    self.dominios = originalData.contenido;
                }


            }
        },function(response){
            
        });
    }
    detalleFicha();
    self.cancel = function(row,rowForm,tipo){
        var originalRow = resetRow(row,rowForm,tipo);
        angular.extend(row,originalRow);
    }
    function updateNgTableElements(tx,type,row){
        var selectTable = self.elementosFicha[type];//
        self.showProgress = true;
        mepService.deleteElementService(tx,''+row.id,type,function(response){
            //se hizo la consulta y el servidor devolvio algo bueno o malo
            self.showProgress = false;
            if(response.response === 'OK'){
                _.remove(selectTable.settings().dataset,function(item){
                    return row === item;
                });
                selectTable.reload();
                //mostramosmensaje de que se hizo bien
            }else{
                //mostramos mensaje de que se hizo mal
            }
        },function(request){
            self.showProgress = false;
            //mostramos mensaje de error;
        });
        /*switch (type){
            case 'escalas':

                break;
            case
        }*/
    }
    self.upd= updateElementoficha;
    function updateElementoficha(row,type){
        angular.extend(row,{
           idFicha:ficha
        });
        var realType = (type === 'funciones' || type === 'competencias')? 'contenido' : type;
        var resolve = {
            data:function(){
                return{
                    element: row,
                    type: type
                }
            }
        };
        var modalInstance = mepService.openModal(realType + '.html','updateCtrl','lg','ctrl',resolve);
        modalInstance.result.then(function(response){
            switch(response.response){
                case "OK":
                    //el servicio se ejecuto correctamente
                    angular.extend(row,response.data);
                    break;
                case "BAD":
                    //el servicio fue llamado pero se ejecuto incorrectamente
                    break;
            }
        },function(response){});
    }
    self.del = function(row,tipo,ev){
        mepService.openDialog('¿Desea eliminar el elemento?','El elemento se quitara de la ficha seleccionada',ev,
        function(){
            updateNgTableElements('deleteElementoFicha',tipo,row);
        },function () {
                //no se hace nada
        });
    }
    function resetRow(row,rowForm,tipo){
        row.isEditing = false;
        rowForm.$setPristine();
        return _.find(originalData[tipo],function(r){
            return r.id === row.id;
        });
    }
    self.save = function (row,rowForm,tipo){
        var originalRow = resetRow(row,rowForm,tipo);
        angular.extend(originalRow,row);
    }
    self.mostrarIndicadores = function(elemento){
        self.elementosFicha.indicador.settings().dataset = [];
        elemento.isSelected = true;
        self.contenidoActual.selected = true;
        angular.extend(self.contenidoActual,elemento);
        self.elementosFicha.indicador.settings().dataset = elemento.indicadores;
        self.elementosFicha.indicador.reload();
    }
    self.registrarElemento = function(tipo){
        if(tipo === 'indicador' && !self.contenidoActual.selected){
            return;
        }
        var padre = tipo === 'indicador' ? self.contenidoActual.id : ficha;
        var elementoModal = $uibModal.open({
            animation:true,
            keyboard:false,
            backdrop:'static',
            templateUrl: tipo+'.html',
            controller: 'elementoFichaCtrl',
            controllerAs: 'ctrl',
            resolve:{
                data: function(){
                    return {
                        ficha:padre,
                        tipo:tipo,
                        padre: self.contenidoActual.nom
                    }
                }
            }
        });
        elementoModal.result.then(function(elemento){
            if(elemento.response === 'succ'){
                if(tipo === 'contenido'){
                    switch (elemento.tip){
                        case 'c': 
                            self.elementosFicha.competencias.settings().dataset.push(elemento);
                            self.elementosFicha.competencias.reload();
                            break;
                        case 'f': 
                            self.elementosFicha.funciones.settings().dataset.push(elemento);
                            self.elementosFicha.funciones.reload();
                            break;    
                    }
                }
                else{
                    self.elementosFicha[tipo].settings().dataset.push(elemento);
                    self.elementosFicha[tipo].reload();
                }
            }else if(elemento.response === 'err'){}
            
            //self.fichas.push({'id':ficha.data.id,'nom':ficha.data.nom,'fcre':ficha.data.fcre});
        },function(){
            
        });
    };
    self.updateInfoFicha = function() {
        var request = crud.crearRequest('mep',1,'actualizarFichaEvaluacion');
        var ficha = {id: self.detalle.fichaDetails.id, des: self.desTmp, vig: self.vigTmp};
        console.log(ficha);
        request.setData(ficha);
                
        crud.actualizar("/evaluacion_personal",request,function(response){
//            modal.mensaje("CONFIRMACION",response.responseMsg);
            console.log("ok");
            console.log(response);
            if(response.responseSta){                
                //actualizando
            }
        },function(data){
            console.log("no");
            console.info(data);
        });
    };
}]);
mepApp.controller('elementoFichaCtrl',['$uibModalInstance','data','crud',function($uibModalInstance,data,crud){
    var self = this;
    self.showProgress = false;
    var titles = {escalas:'Nueva Escala de Valoración',rangos:'Nuevo Rango de Valoración',funciones:'Editar Funcion del personal',competencias:"Editar Competencia del personal",indicador:'Nuevo Indicador de Evaluación'};
    self.title = titles[data.tipo];
    self.elemento = {};
    if(data.tipo === 'indicador') self.elemento.padre = data.padre;
    if(data.tipo === 'contenido')self.ltipos = [{id:0 ,abr:'f',nom:'Funcion'},{id:1,abr:'c',nom:'Competencia'}];
    self.elemento.fic = parseInt(data.ficha);
    //if(data.tipo !== 'indicador')self.elemento.fic = parseInt(data.ficha);
    //else self.elemento.contenido = data.ficha;
    self.accept = function(){
        var request  = crud.crearRequest('mep',1,'addElement');
        request.setMetadataValue("tipo",data.tipo);
        request.setData(self.elemento);
        console.log('request',request);
        crud.insertar('/evaluacion_personal',request,
            function(resp){
                angular.extend(self.elemento,resp.data);
                angular.extend(self.elemento,{
                    tipo:data.tipo,
                    response:resp.response,
                    responseMsg:resp.responseMsg
                });
                console.log('elemento',self.elemento);
                $uibModalInstance.close(self.elemento);
            },
            function(response){
                $uibModalInstance.dismiss('cancel');
            });
    };
    self.cancel = function(){
        $uibModalInstance.dismiss('cancel');
    };
}]);

mepApp.controller('updateCtrl',['$uibModalInstance','$log','mepService','data',function($uibModalInstance,$log,mepService,data){

    var self = this;
    self.showProgress = false;
    var titles = {escalas:'Editar Escala de Valoracion',rangos:'Editar Rangos de Valoración',funciones:'Editar Funcion del personal',competencias:"Editar Competencia del personal",indicador:'Editar indicador de evaluación'};
    self.elemento = data.element;
    var type = data.type;
    self.title = titles[type];
    ////enviamos los datos al servidor
    self.accept = function(){
        self.showProgress = true;
        mepService.updateElementService(self.elemento,(type === 'funciones' || type === 'competencias')?'contenido' :type,
        function(response){// se ejecuto correctamente la llamada al servicio
            self.showProgress = false;
            $uibModalInstance.close(response);
        },function(response){
            self.showProgress = false;
            //Error en la llamada al servicio
            $uibModalInstance.dismiss(response);
        });
    };
    self.cancel = function(){
        $uibModalInstance.dismiss('cancel');
    };
}]);