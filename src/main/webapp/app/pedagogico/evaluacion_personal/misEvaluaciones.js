/**
 * Created by Administrador on 01/09/2016.
 */
var misEvaluaciones = angular.module('app');
misEvaluaciones.config(['$routeProvider',function($routeProvider){
    $routeProvider.when('/evaluacionPersonal/resumen_evaluacion',{
            templateUrl:'pedagogico/evaluacion_personal/resumen_evaluacion.html',
            controller:'resumenEvaluacion',
            controllerAs:'ctrlresumenEvaluacion'
        });
}]);
misEvaluaciones.factory('mepServiceMisEva', ['$log','$uibModal','$mdDialog','crud',function ($log,$uibModal,$mdDialog,crud) {
    var employee = {};
    return {
        changeSelection: function(row,preSelection){
            if (preSelection) {
                preSelection.$selected = false;
                //row.$selected = true;
                //preSelection = row;
            }
            if (preSelection === row) {
                row.$selected = false;
                preSelection = null;
            }
            else{
                row.$selected = true;
                preSelection = row;
            }
            return preSelection;
        },
        openModal: function(nameHtml,nameCtrl,size,alias,params){
            var modalInstance = $uibModal.open({
                animation:true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                keyboard:true,
                backdrop:true,
                templateUrl: nameHtml,
                controller: nameCtrl,
                controllerAs:alias,
                size: size,
                resolve:params
            });
            return modalInstance;
        },
        getReportService: function (id,type,succ,err) {
            var request  = crud.crearRequest('mep',1,'getReportEvaluacion');
            request.setMetadataValue('resumen',''+id);
            crud.listar('/evaluacion_personal',request,succ,err);
        },
        getDetalleEvaluacionPersonal: function(idResumen,type,succ,err){
            var request = crud.crearRequest('mep',1,'listarDetallesEvaluacion');
            request.setMetadataValue('id',angular.isNumber(idResumen)? '' + idResumen : idResumen);
            request.setMetadataValue('ty',type);
            crud.listar('/evaluacion_personal',request,succ,err);
        }
    }
}]);

misEvaluaciones.controller('misEvaluaciones',['$log','$window','$location','$uibModal','NgTableParams','mepServiceMisEva','crud','modal',function($log,$window,$location,$uibModal,NgTableParams,mepServiceMisEva,crud,modal){


    var self = this;
    //obtenemos los datos del usuario actual.
    var usuario = JSON.parse( window.atob( localStorage.getItem('usuario')));
    var organizacion = JSON.parse( window.atob(localStorage.getItem('organizacion')));
    self.showProgress = true;
    self.resumenes = new NgTableParams({count:10},{
        counts: [],
        paginationMaxBlocks: 13,
        paginationMinBlocks: 5,
        dataset: []
    });
    obtenerResumenTrabajador(usuario.usuarioID,organizacion.organizacionID);
    function obtenerResumenTrabajador(idUser,idOrg){
        var request = crud.crearRequest('mep',1,'getEvaluacionesCompartidas');
        request.setMetadataValue('tra',''+idUser);
        request.setMetadataValue('org',''+idOrg);
        crud.listar('/evaluacion_personal',request,function(response){
            self.showProgress = false;
            if(response.response === 'OK'){
                self.employee = response.data.trab;
                self.resumenes.settings().dataset = response.data.eval;
                self.resumenes.reload();    
            }else modal.mensaje("ERROR",response.responseMsg);
            
        },function(response){
            self.showProgress = false;
            modal.mensaje("ERROR","No se pudo obtener la lista de evaluaciones");
        });
    }
    var preSelection = null;
    self.changeSelection = changeSelection;
    function changeSelection(row){
        preSelection = mepServiceMisEva.changeSelection(row,preSelection);
    }
    self.obtenerDetalle = obtenerDetalle;
    function obtenerDetalle(row){
        
        self.showProgress = true;
        var currentCargo = self.employee.car;
        if(currentCargo.toUpperCase() === 'DOCENTE')
            mepServiceMisEva.getDetalleEvaluacionPersonal(row.det,self.employee.car,function(response){
                self.showProgress = false;
                var modalInstance = mepServiceMisEva.openModal('pedagogico/evaluacion_personal/mod_resumen_evaluacion_docente.html','evaluarDocenteCtrl','lg','ctrl',{data:function(){return response.data;}});
            },function(response){
                self.showProgress = false;
            });
        else
            mepServiceMisEva.getDetalleEvaluacionPersonal(row.det,self.employee.car,function(response){
                self.showProgress = false;
                var modalInstance = mepServiceMisEva.openModal('modal_detalle_evaluacion_trabajador.html','modalDetalleEvaCtrl','lg','ctrl',{data:function(){return response.data;}});
            },function(response){
                self.showProgress = false;
            });
    }
    self.verReporte = verReporte;
    function verReporte(row){
        mepServiceMisEva.getReportService(row.det,'',function (response) {
            if(response.response === 'OK')
                window.open(response.data[0]);
            else{
                modal.mensaje("ERROR","No se pudo obtener el reporte seleccionado")
            }

        },function (response) {

        });
    }
}]);
misEvaluaciones.controller('evaluarDocenteCtrl',['$log','$uibModalInstance','data','modal',function($log,$uibModalInstance,data,modal){
    var self = this;
    self.showProgress = false;
    self.data = data;
    self.registroRealizado = false;
    self.namebutton =  (self.data.employee)? 'Guardar' : 'Aceptar';
    self.showbuttonShare = (self.data.employee)? true : false;
    var resumen;
    self.aceptar = (self.data.employee)? guardarEvaluacion : function(){
        $uibModalInstance.close('close');
    }
    function guardarEvaluacion(){
        $uibModalInstance.close('close');

    }
    self.cancelar = function(){
        $uibModalInstance.dismiss('cancel');
    }
}]);
misEvaluaciones.controller('modalDetalleEvaCtrl',['$log','$uibModalInstance','data',function($log,$uibModalInstance,data){
    var self = this;
    self.data = data;
    self.ok = ok;
    self.cancel = cancel;
    calcularSumaIndicadores(self.data.dets,self.data.cols,self.data.inds);
    function ok(){
        $uibModalInstance.close('');
    }
    function cancel(){
        $uibModalInstance.dismiss('cancel');
    }
    function calcularSumaIndicadores(dets,escalas,inds){

        var data = _.partition(inds,function(o){return o.tip === 'f'}) ;
        _.forEach(dets,function(d){
            var cv = d.tip === 'f' ? data[0] : obtenerContenido(data[1],d.tip,d.nom);
            d.sum = sumarIndicadores(escalas.length,cv);

        });
    }
    function obtenerContenido(data,tip,nom){
        return _.find(data,function(c){
            return (c.tip === tip && c.nom === nom);
        });
    }
    function sumarIndicadores(esSize,contenido) {
        if(!contenido) return;
        var sumResumen = Array.apply(null, new Array(esSize)).map(Number.prototype.valueOf,0);
        if(Object.prototype.toString.call(contenido) === '[object Array]'){
            _.forEach(contenido,function(f){
                _.forEach(f.indicadores,function(i){
                    sumResumen[i.pun-1] += i.pun;
                });
            });
        }else{
            var indicadores = contenido.indicadores;
            _.forEach(indicadores,function(i){
                sumResumen[i.pun-1] += i.pun;
            });
        }

        return sumResumen;
    }
}]);

