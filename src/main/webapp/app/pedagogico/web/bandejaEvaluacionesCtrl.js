app.controller("bandejaEvaluacionesCtrl",["$scope","NgTableParams","crud","modal", function ($scope,NgTableParams,crud,modal){
    
    $scope.plan = {};    
    $scope.evaluacion = {};
    $scope.documentoSel = {archivo:{}};
    var ENUNCIADO_VF = "Escriba (V) en caso que sea Verdadero o (F) en caso de ser falso para las siguientes premisas :";
    var ENUNCIADO_REL = "Relacione los siguientes enunciados dados a continuacion :";
    $scope.enunciado = "";
    var tareas = [];
    
    //tabla de tareas
    var paramsEvaluacion = {count: 10};
    var settingEvaluacion = { counts: []};
    $scope.tablaEvaluacion = new NgTableParams(paramsEvaluacion, settingEvaluacion);
    
    $scope.estados = [{id:'N',title:"nuevo"},{id:'E',title:"enviado"},{id:'C',title:"calificado"},{id:'F',title:"fuera tiempo"}];
    
    $scope.tiempo_actual = 0;
    
    $scope.evaluaciones = [];
    $scope.preguntas_evaluacion = [];
    $scope.pregunta_actual;
    $scope.alternativas = [];
    $scope.alternativas_alumno = [];
    $scope.activar_calificar = false;
    
    $scope.reloj;
    
    $scope.resp_alu = []; // Respuesta Alumno para el caso de Preguntas VF
    
    $scope.prepararEnviar = function(o){
        $scope.evaluacion = JSON.parse(JSON.stringify(o));
        $('#modalEnviarEvaluacion').modal('show');
    };
    $scope.enviarTarea = function(){
        
        if($scope.evaluacion.documentos.length == 0){
            modal.mensaje("CONFIRMACION","Debe enviar por lo menos un archivo");
            return;
        }
        
        modal.mensajeConfirmacion($scope,"seguro que enviara la tarea",function(){

            var request = crud.crearRequest('tarea',1,'resolverTarea');
            request.setData($scope.evaluacion);        
            crud.insertar("/web",request,function(res){            
                modal.mensaje("CONFIRMACION",res.responseMsg);
                if(res.responseSta){
                    //recuperamos las variables que nos envio el servidor
                    $scope.evaluacion.estado = res.data.estado;
                    
                    res.data.documentos.forEach(function(item,i){
                        $scope.evaluacion.documentos[i].tareaDocumentoID = item.tareaDocumentoID;
                        $scope.evaluacion.documentos[i].documento = item.documento;
                        $scope.evaluacion.documentos[i].url = item.url;
                    });
                    
                    settingEvaluacion.dataset[$scope.evaluacion.i] = $scope.evaluacion;
                    $scope.tablaEvaluacion.reload(); 
                    //cerramos la ventana modal
                    $('#modalEnviarTarea').modal('hide');
                }
                else{
                    $scope.evaluacion.estado = res.data.estado;
                    settingEvaluacion.dataset[$scope.evaluacion.i] = $scope.evaluacion;
                    $scope.tablaEvaluacion.reload(); 
                    //cerramos la ventana modal
                    $('#modalEnviarTarea').modal('hide');
                }
            },function(data){
                console.info(data);
            });
            
        },"400");        
    };
    
    $scope.agregarDocumento = function(){
        if(!$scope.documentoSel.documento || $scope.documentoSel.documento =="" ){
            modal.mensaje("CONFIRMACION","seleccione un documento adjunto");
            return;
        }
        if($scope.evaluacion.numeroDoc <= $scope.evaluacion.documentos.length ){
            modal.mensaje("CONFIRMACION","NO debe superar el numero de documentos permitidos");
            return;
        }
        
        $scope.evaluacion.documentos.push($scope.documentoSel);
        $scope.documentoSel = {archivo:{}};  
    };
    $scope.editarDocumento = function(i,d){
        //si estamso editando
        if(d.edi){
            $scope.evaluacion.documentos[i] = d.copia;
        }
        //si queremos editar
        else{
            d.copia = JSON.parse(JSON.stringify(d));
            d.edi =true;            
        }
    };
    $scope.eliminarDocumento = function(i,d){
        //si estamso cancelando la edicion
        if(d.edi){
            d.edi = false;
            delete d.copia;
        }
        //si queremos eliminar el elemento
        else{
            $scope.evaluacion.documentos.splice(i,1);
        }
    };
    
    $scope.filtrarAreas = function(){
              
        $scope.tablaEvaluacion.filter({'areaID': $scope.evaluacion.areaID });
    };
    $scope.buscarPlanEstudios = function(orgID,usuarioID){
        //preparamos un objeto request
        var request = crud.crearRequest('planEstudios',1,'buscarPlanEstudios');
        request.setData({organizacionID:orgID});
        crud.listar("/cuadroHoras",request,function(response){
            
            if(response.responseSta){
                $scope.plan = response.data;                
                
                request = crud.crearRequest('evaluacion',1,'listarEvaluacionesPorAlumno');
                request.setData({planID:$scope.plan.planID,alumnoID:usuarioID});
                crud.listar("/web",request,function(response){

                    if(response.responseSta){
                        $scope.evaluaciones = response.data;
                        
                        $scope.evaluaciones.forEach(function (item){
                           item.area = buscarContenido($scope.plan.areas,"areaID","area",item.areaID);
                        });
                        
                        settingEvaluacion.dataset = $scope.evaluaciones;
                        
                        $scope.tablaEvaluacion.settings(settingEvaluacion);
                    }
                },function(data){
                    console.info(data);
                });
            }
        },function(data){
            console.info(data);
        });
        
        
    };
    
    $scope.preparar_evaluacion = function(){
        
        modal.mensajeConfirmacion($scope," ¿ Seguro que desea empezar la Evaluacion ?",function(){
           var request = crud.crearRequest('evaluacion',1,'listarPreguntasEvaluacion');
            request.setData({eva_esc_id:$scope.evaluacion.evaluacionID});
            crud.listar("/web",request,function(response){
                   if(response.responseSta){
                        $scope.preguntas_evaluacion = response.data;
                        $scope.active2= 1;
                        $scope.elegir_pregunta();
                   }
            });
          
            
        
        });
    }
    
    $scope.elegir_pregunta = function(){
        var size = ($scope.preguntas_evaluacion.length)-1;
        var nro_preg = 0;
        do{
            nro_preg = Math.round(Math.random()*size);
        }while($scope.preguntas_evaluacion[nro_preg].sel != false);
        $scope.pregunta_actual = $scope.preguntas_evaluacion[nro_preg];
        $scope.preguntas_evaluacion[nro_preg].sel = true;
        $scope.generar_pregunta();
     
    }
    
    $scope.iniciarCronometro = function(){
       
        $scope.time = $scope.pregunta_actual.tie ;
        
        $scope.cronometro = new Date(); 
        if(parseInt($scope.time)>60){$scope.cronometro.setHours(parseInt($scope.time)/60);$scope.cronometro.setMinutes(parseInt($scope.time)%60);}
        else{
            $scope.cronometro.setHours(0);
            $scope.cronometro.setMinutes(parseInt($scope.time));
        }
        $scope.cronometro.setSeconds(59);
        $scope.pregunta_actual;
        //$scope.$apply();
        
        
        $scope.iniciarClock();
        
    }
    
    $scope.iniciarClock = function () {
            
            var context;
            
            function getClock()
            {
                
                if(typeof  clock=='undefined')
                {
                    clearInterval($scope.reloj);
                    return;
                }
                
                //Get Current Time
                $scope.cronometro.setSeconds($scope.cronometro.getSeconds() - 1);
                if($scope.cronometro.getHours()=== 0 && $scope.cronometro.getMinutes()=== 0 && $scope.cronometro.getSeconds()=== 0){  
                    $scope.verificar_preguntas();
                    if($scope.preguntas_resueltas!=true){
                        //clearInterval($scope.reloj);
                       
                        $scope.guardar_resultado();
                        
                        //$scope.elegir_pregunta();
                        
                        
                    }else{
                        clearInterval($scope.reloj);
                        str = prefixZero($scope.cronometro.getHours(), $scope.cronometro.getMinutes(), $scope.cronometro.getSeconds());
                        $scope.calificar(); 
                        
                    }
                    clearInterval($scope.reloj);
                    //Verificar si se resolvieron todas las preguntas
                    //Elegir la siguiente Pregunta
                    
                }
                str = prefixZero($scope.cronometro.getHours(), $scope.cronometro.getMinutes(), $scope.cronometro.getSeconds());
                //Get the Context 2D or 3D
                context = clock.getContext("2d");
                context.clearRect(0, 0, 500, 200);
                context.font = "60px Arial";
                context.fillStyle = "#000";
                //context.fillText(str, 42, 125);
                context.fillText(str, 20, 75);
            }

            function prefixZero(hour, min, sec)
            {
                var curTime;
                if (hour < 10)
                    curTime = "0" + hour.toString();
                else
                    curTime = hour.toString();

                if (min < 10)
                    curTime += ":0" + min.toString();
                else
                    curTime += ":" + min.toString();

                if (sec < 10)
                    curTime += ":0" + sec.toString();
                else
                    curTime += ":" + sec.toString();
                return curTime;
            }

            $scope.reloj=setInterval(getClock, 1000);
    };
    
    $scope.verificar_preguntas = function(){
        $scope.preguntas_resueltas = false;
        var size = $scope.preguntas_evaluacion.length;
        for(var i = 0 ; i<size ; i++){
            if($scope.preguntas_evaluacion[i].sel!=true){
                return;
            }
        }
        $scope.preguntas_resueltas = true; //finalizo todas las preguntas de evaluacion
    }
    
    
    $scope.guardar_resultado = function(){  
        var size = $scope.alternativas.length;
        for(var i =0;i<=size ; i++){
             var alt_alu = {nro:"",nombre:""};
             if(i == size){
                alt_alu.nro = $scope.pregunta_actual.preguntaID;
                alt_alu.nombre = "";
                $scope.alternativas_alumno.push(alt_alu);
               
                break;
            }
            if($scope.alternativas[i].select == true ){
               
                alt_alu.nro = $scope.pregunta_actual.preguntaID;
                alt_alu.nombre = $scope.alternativas[i].nombre;
                $scope.alternativas_alumno.push(alt_alu);
                
                break;
            }
        }
        if($scope.alternativas_alumno.length == $scope.preguntas_evaluacion.length){
                    //$scope.calificar();
                    return;
        }
        else{
            $scope.elegir_pregunta();
        }
        
        
    }
    
    $scope.generar_pregunta = function(){
       
        
        switch($scope.pregunta_actual.tipo){ 
            case "P":
                    $scope.enunciado = $scope.pregunta_actual.cont;
                    $scope.crear_alternativas();
                
                break;
            case "V":
                    $scope.enunciado = ENUNCIADO_VF;
                    $scope.generar_enunciados_vf();
                   
                break;
            case "R":
                    $scope.enunciado = ENUNCIADO_REL;
                    $scope.generar_enunciados_relacionar();
                    
                break;
        }

        $scope.iniciarCronometro();

    }
    
    $scope.crear_alternativas = function(){
        $scope.alternativas = [];
        var altern = $scope.pregunta_actual.alt;
        var size = $scope.pregunta_actual.alt.length;
        var alt = {id:"",nombre:"",select:false,descarte:true};
        var j = 0;
        
        for(var i = 0 ; i<=size ; i++){
            if(altern[i] =='&' | i == size){
                alt.id = String.fromCharCode(97 + j);j++;
                $scope.alternativas.push(angular.copy(alt));
                alt.id="";alt.nombre="";
                continue;
            }
            else{
                alt.nombre = alt.nombre + altern[i];
            }
        }
            j=0;
    }
    
    $scope.generar_enunciados_vf = function(){
        var premisa = {id:0,nombre:""};
        $scope.pregunta_actual.enunciados = [];
        var size = $scope.pregunta_actual.cont.length;
        var enun = $scope.pregunta_actual.cont;
        var j = 1;
        for(var i = 0 ; i<=size ; i++){
             if(enun[i] =='&' | i == size){
                 premisa.id = j;j++;
                 $scope.pregunta_actual.enunciados.push(angular.copy(premisa));
                 premisa.nombre = "";
             }else{
                 premisa.nombre = premisa.nombre + enun[i];
             }
        }
        $scope.crear_alternativas();
    }
    $scope.generar_enunciados_relacionar=function(){

            // Ordenar las Premisas
            $scope.pregunta_actual.enunciados = [];
           
            var premisa = {id:0,nombre:"",respuesta:"",select:false};
            var orden_premisas = $scope.pregunta_actual.orden;
            var size = $scope.pregunta_actual.cont.length;
            var str_premisas = $scope.pregunta_actual.cont;
           // $scope.premisas = [];
            var nro_premisa = 0;
            for(var i=0;i<=size;i++){
                if(str_premisas[i] == '&' | i== size){
                    var premisa_id = parseInt(orden_premisas[nro_premisa]);
                    premisa.id = premisa_id+1;
                    //$scope.premisas.push(angular.copy(premisa));
                    $scope.pregunta_actual.enunciados[premisa_id] = angular.copy(premisa);
                    
                    
                    nro_premisa+=1;
                    premisa.nombre = "";
                }else{
                    premisa.nombre = premisa.nombre + str_premisas[i];
                }
            }
         
            // Listar orden de Respuestas
            var str_resp = $scope.pregunta_actual.resp;
            var size_resp = $scope.pregunta_actual.resp.length;
            var resp_temp = "";
            var cont = 0; 
            for(var j=0;j<=size_resp;j++){
                if(str_resp[j] == '&' | j==size_resp){
                    $scope.pregunta_actual.enunciados[cont].respuesta = angular.copy(resp_temp);
                    cont++;
                    resp_temp = "";
                }
                else{
                    resp_temp = resp_temp + str_resp[j];
                }
            }

        $scope.crear_alternativas();
    }
    
    $scope.buscar_alternativa = function(enunciado,resp){
        enunciado.res = resp;
        var flag = true;
        var size = $scope.pregunta_actual.enunciados.length;
        
        for(var i =0;i<size;i++){
            if(!($scope.pregunta_actual.enunciados[i].res)){
                flag = false;
                break;
            }
        }
        if(flag == true){
            
           var resp_alu = "";
           for(var r=0;r<size;r++){
               resp_alu+=$scope.pregunta_actual.enunciados[r].res;
          } 
          
          var size_alt = $scope.alternativas.length;  
          for(var k=0;k<size_alt;k++){
              $scope.alternativas[k].select = false;
          }
          for(var i = 0 ; i<size_alt ; i++){
                
                    $scope.alternativa_sin_comas = null;
                    $scope.eliminar_comas($scope.alternativas[i].nombre);
                    
                    if($scope.alternativa_sin_comas === resp_alu){
                          $scope.alternativas[i].select = true;
                          break;
                    }
                    /*
                    for(var j=0 ; j<=$scope.alternativa_sin_comas.length ; j++){ 
                        //var alt_generada = $scope.alternativas[i].nombre;
                        var alt_generada = $scope.alternativa_sin_comas;
                        var cont = 1;
                        if(!(enunciado.res == alt_generada[j])){
                            $scope.alternativas[i].descarte = false;
                            break;
                        }
                        else{
                            cont+=1;
                        }
                        if(cont==$scope.alternativa_sin_comas.length){
                            $scope.alternativas[i].select = true;
                        }
                        
                    }
                    */
                }

          
      
        }

        
    }
    
    
    
    $scope.eliminar_comas = function(alternativa){
        var alt_generada = "";
        for(var i =0;i<alternativa.length;i++){
            if(!(alternativa[i] == ',' || alternativa[i]== '')){
              alt_generada +=alternativa[i];
            }
        }
        $scope.alternativa_sin_comas = alt_generada;
    }
    
    $scope.marcar_alternativa = function(alternativa){
        
        var size = $scope.alternativas.length;
        for(var i =0;i<size ; i++){
            if($scope.alternativas[i].select === true){
                $scope.alternativas[i].select = false;
                break;
            }
        }
        alternativa.select = true;
        if($scope.pregunta_actual.tipo == 'V'){
            
        }
    }
    
    $scope.calificar = function(){
        
        var request = crud.crearRequest('evaluacion',1,'calificarEvaluacion');
            request.setData({preguntas:$scope.preguntas_evaluacion,resp_alu:$scope.alternativas_alumno,band_id:$scope.evaluacion.bandejaEvaluacionID});       
            crud.insertar("/web",request,function(res){            
                modal.mensaje("CONFIRMACION",res.responseMsg);
                if(res.responseSta){
                    $scope.nota_sistema = res.data.puntaje;
                    $scope.activar_calificar = true;
                }
            });

    }
    $scope.generar_nueva = function(){
        
        modal.mensajeConfirmacion($scope,"¿Realmente desea continuar con la siguiente Pregunta?",function(){
            $scope.verificar_preguntas();
            if($scope.preguntas_resueltas != true){
                clearInterval($scope.reloj); 
                $scope.guardar_resultado(); 
            }else{
                $scope.guardar_resultado();
                $scope.cronometro.setHours(0);
                $scope.cronometro.setMinutes(0);
                $scope.cronometro.setSeconds(1);
                
            }
                
               // $scope.calificar();
              
        });
        
    }
    
    
    
}]);
