app.controller("evaluacionesCtrl",["$scope","NgTableParams","crud","modal","$rootScope", function ($scope,NgTableParams,crud,modal,$rootScope){
        
    $scope.plan = {};
    $scope.gradoSel = {};
    $scope.seccionSel = '';
    $scope.areaSel = '';
    $scope.evaluacion = {};
    $scope.numSel = true; // (Nota numerica o Literal)
    
    $scope.tipoPreguntas = [{id:'P',nombre:"pregunta"},{id:'V',nombre:"verdadero falso"},{id:'R',nombre:"relacionar"}];
    
    var evaluacions = [];
    
    // nota tarea actual seleccionada
    $scope.nota_act_sel = {
        nota:""
    };
    $scope.configuracion_nota_docente = [];
    
    //tabla de evaluacions
    var paramsEvaluacion = {count: 10};
    var settingEvaluacion = { counts: []};
    $scope.tablaEvaluacion = new NgTableParams(paramsEvaluacion, settingEvaluacion);
    
    $scope.estados = [{id:'N',title:"nuevo"},{id:'E',title:"enviado"},{id:'C',title:"calificado"}];
    
    //tabla de alumnos
    var paramsAlumno = {count: 10};
    var settingAlumno = { counts: []};
    $scope.tablaAlumno = new NgTableParams(paramsAlumno, settingAlumno);
    $scope.evaluacionActual;
    
    $scope.prepararAgregar = function(seccionID,areaID){
        $scope.evaluacion = {seccionID:seccionID,areaID:areaID,preguntas:[],mensajes:[]};
        $scope.pregunta = {opciones:[],alternativas:[],tiempo:0};
        $scope.opcion = {};
        $scope.mensaje = {};
        $scope.active2 = 0;
        $('#modalNuevaEvaluacion').modal('show');
    };
    $scope.agregarEvaluacion = function(usuarioID){
        
        if(!$scope.evaluacion.evaluacionID){
            $scope.evaluacion.planID = $scope.plan.planID;
            $scope.evaluacion.gradoID = $scope.gradoSel.gradoID;
            $scope.evaluacion.seccionID = $scope.seccionSel.seccionID;
            $scope.evaluacion.areaID = $scope.areaSel;
            $scope.evaluacion.docenteID = usuarioID;
            
            var request = crud.crearRequest('evaluacion',1,'insertarEvaluacion');
            request.setData($scope.evaluacion);        
            crud.insertar("/web",request,function(res){            
                modal.mensaje("CONFIRMACION",res.responseMsg);
                if(res.responseSta){
                    //recuperamos las variables que nos envio el servidor
                    $scope.evaluacion.evaluacionID = res.data.evaluacionID;
                    $scope.evaluacion.estado = res.data.estado;

                    $scope.evaluacion.grado = buscarContenido($scope.plan.grados,"gradoID","grado",$scope.evaluacion.gradoID);
                    $scope.evaluacion.area = buscarContenido($scope.plan.areas,"areaID","area",$scope.evaluacion.areaID);

                    //insertamos el elemento a la lista
                    insertarElemento(settingEvaluacion.dataset,$scope.evaluacion);
                    $scope.tablaEvaluacion.reload();
                    
                    $scope.active2 = 1;
                }            
            },function(data){
                console.info(data);
            });
        }
        else{
            modal.mensajeConfirmacion($scope,"Esta seguro que desea guardar los cambios de la evaluacion",function(){    
                
            var request = crud.crearRequest('evaluacion',1,'actualizarEvaluacion');
            request.setData($scope.evaluacion);        
            crud.actualizar("/web",request,function(res){            
                modal.mensaje("CONFIRMACION",res.responseMsg);
                if(res.responseSta){
                    settingEvaluacion.dataset[$scope.evaluacion.i] = $scope.evaluacion;
                    $scope.tablaEvaluacion.reload();
                    $scope.active2 = 1;
                }            
            },function(data){
                console.info(data);
            });
            
            },"400");
        }
        
    };
    $scope.agregarPreguntas = function(){
        modal.mensajeConfirmacion($scope,"Esta seguro que desea guardar las preguntas de evaluacion",function(){        
        var request = crud.crearRequest('evaluacion',1,'insertarPreguntas');
        request.setData({evaluacionID:$scope.evaluacion.evaluacionID,preguntas:$scope.evaluacion.preguntas});
        crud.insertar("/web",request,function(res){            
            modal.mensaje("CONFIRMACION",res.responseMsg);
            if(res.responseSta){
                //recuperamos las variables que nos envio el servidor
                $scope.active2 = 2;
            }            
        },function(data){
            console.info(data);
        });
        
        },"400");
    };
    $scope.agregarMensajes = function(){
        modal.mensajeConfirmacion($scope,"Esta seguro que desea guardar los mensajes de evaluacion",function(){    
        var request = crud.crearRequest('evaluacion',1,'insertarMensajes');
        request.setData({evaluacionID:$scope.evaluacion.evaluacionID,mensajes:$scope.evaluacion.mensajes});
        crud.insertar("/web",request,function(res){            
            modal.mensaje("CONFIRMACION",res.responseMsg);
            if(res.responseSta){
                //recuperamos las variables que nos envio el servidor
                $scope.active2 = 3;
            }            
        },function(data){
            console.info(data);
        });
        
        },"400");
    };
    $scope.prepararEditar = function(o){
        $scope.evaluacion = JSON.parse(JSON.stringify(o));
        if(o.estado!='N'){
            $scope.evaluacion.fechaI = new Date(o.fechaInicio);
            $scope.evaluacion.fechaF = new Date(o.fechaFin);
        }
        
        if(!$scope.evaluacion.preguntas)
            $scope.evaluacion.preguntas = [];
        else{
            $scope.evaluacion.preguntas.forEach(function(item){
                var alter = item.alternativa.split("&");
                item.alternativas = [];
                alter.forEach(function(ite,i){
                    item.alternativas.push( {res:ite,id:String.fromCharCode(97+i)} );
                });
                
                var preg = item.pregunta.split("&");
                var resp = item.respuesta.split("&");
                item.opciones = [];
                item.respuestaS = "";
                preg.forEach(function(ite,i){
                    item.opciones[i] = {contenido:preg[i],respuesta:resp[i]};
                    
                    if(i>0)
                        item.respuestaS += ",";
                    
                    if(item.tipo=='R'){
                        item.opciones[i].concepto = resp[item.orden.indexOf(""+i)];
                        item.respuestaS += Number(item.orden.charAt(i))+1;
                    }
                    else
                        item.respuestaS += resp[i];
                    
                });
            });
            
        }
        if(!$scope.evaluacion.mensajes){
            var request = crud.crearRequest('evaluacion',1,'listarMensajes');
            request.setData({evaluacionID:$scope.evaluacion.evaluacionID});

            crud.listar("/web",request,function(res){
                if(res.responseSta){
                    $scope.evaluacion.mensajes = res.data;
                }
                else
                    $scope.evaluacion.mensajes = [];

            },function(data){
                console.info(data);
            });
        }
        
        $scope.pregunta = {opciones:[],alternativas:[],tiempo:0};
        $scope.opcion = {};
        $scope.mensaje = {};
        $scope.active2 = 0;
        $('#modalNuevaEvaluacion').modal('show');
    };
    $scope.eliminarEvaluacion = function(i,idDato){
        
        modal.mensajeConfirmacion($scope,"seguro que desea eliminar la evaluacion",function(){
            
            var request = crud.crearRequest('evaluacion',1,'eliminarEvaluacion');
            request.setData({evaluacionID:idDato});

            crud.eliminar("/web",request,function(response){

                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    eliminarElemento(settingEvaluacion.dataset,i);
                    $scope.tablaEvaluacion.reload();                }

            },function(data){
                console.info(data);
            });
            
        },"400");
    };
    $scope.dateOptions = {
        formatYear: 'yy',
        //maxDate: new Date(),
        minDate: new Date(),
        startingDay: 1
    };
    $scope.enviarEvaluacion = function(orgID){
        
        modal.mensajeConfirmacion($scope,"Esta seguro que enviara la evaluacion",function(){
            $scope.evaluacion.fechaInicio = convertirFecha2($scope.evaluacion.fechaI) +" "+convertirHora($scope.evaluacion.fechaI);
            $scope.evaluacion.fechaFin = convertirFecha2($scope.evaluacion.fechaF) +" "+convertirHora($scope.evaluacion.fechaF);
            
            $scope.evaluacion.organizacionID = orgID;

            var request = crud.crearRequest('evaluacion',1,'enviarEvaluacion');
            request.setData($scope.evaluacion);        
            crud.insertar("/web",request,function(res){            
                modal.mensaje("CONFIRMACION",res.responseMsg);
                if(res.responseSta){
                    //recuperamos las variables que nos envio el servidor
                    $scope.evaluacion.estado = res.data.estado;
                    settingEvaluacion.dataset[$scope.evaluacion.i] = $scope.evaluacion;
                    $scope.tablaEvaluacion.reload(); 
                    //cerramos la ventana modal
                    $('#modalNuevaEvaluacion').modal('hide');
                }            
            },function(data){
                console.info(data);
            });
        },"400");
    };
    
    $scope.prepararEvaluacion = function(evaluacion){
        
        $scope.evaluacion = JSON.parse(JSON.stringify(evaluacion));
        $scope.eva_act_sel=evaluacion.evaluacionID;
        //preparamos un objeto request
        var request = crud.crearRequest('evaluacion',1,'listarAlumnosCumplieronEvaluacion');
        request.setData({evaluacionID:evaluacion.evaluacionID});
        crud.listar("/web",request,function(res){
            
            if(res.responseSta){
                
                settingAlumno.dataset = res.data;
                $scope.tablaAlumno.settings(settingAlumno);
                
                $scope.evaluacionSel = evaluacion;
                $scope.active = 1;
            }
            
        },function(data){
            console.info(data);
        });
        
    };
    
    $scope.prepararCalificar = function(alumno){
         alumno.nota_ant = alumno.nota;
         alumno.edi = true;
         
    };
    
    $scope.desactivar = function(alumno){
        alumno.edi = false;
        alumno.nota = alumno.nota_ant;
    }
    
    $scope.obtener_nota_literal = function(alumno){
        var size = $scope.configuracion_nota_docente.length;
        var nota_alu = alumno.nota;
        for(var i = 0 ; i< size ;i++){
            var conf_act = $scope.configuracion_nota_docente[i];
            if( parseInt(nota_alu) >= conf_act.not_min && parseInt(nota_alu) <= conf_act.not_max){
                alumno.not_lit = conf_act.cod;
                break;
            }
        }
    }
    
    $scope.editarnota = function(alumno){
        
        if($scope.numSel == false){
            alumno.nota = parseInt($scope.nota_act_sel.nota.not_max); // Por defecto se tomara la nota Mayor (OJO)
            alumno.not_lit = $scope.nota_act_sel.nota.cod;
        }
        else{
            if(!alumno.nota || isNaN(alumno.nota)){
                modal.mensaje("CONFIRMACION","La nota no es un valor valido");
                return;
            }
            else{
                $scope.obtener_nota_literal(alumno);
            }
        }

        var request = crud.crearRequest('evaluacion',1,'actualizarbandeja');
        request.setData({alumnos:alumno});
        crud.actualizar("/web",request,function(res){
            modal.mensaje("CONFIRMACION",res.responseMsg);
            if(res.responseSta){
                alumno.edi = false;
                alumno.nota = res.data[0].not;
                alumno.not_lit = res.data[0].not_lit;
                
                settingAlumno.dataset[alumno.i] = alumno;
                $scope.tablaAlumno.reload();
                
            }
        });
    }
    
    $scope.verEvaluacionAlumno = function(bandejaID){
        
        var request = crud.crearRequest('evaluacion',1,'obtenerEvaluacionPreguntas');
            request.setData({ban_eva_id:bandejaID});
            crud.listar("/web",request,function(res){
                $scope.evaluacionActual = res.data;
                $scope.generar_alternativas();
                $('#modalVerEvaluacion').modal('show');
                
            });
            
    }
    
    $scope.generar_alternativas = function(){
        var size = $scope.evaluacionActual.preguntas.length;
        //var altern = $scope.pregunta_actual.alt;
        
        for(i = 0 ;i<size ; i++){
            var altern = $scope.evaluacionActual.preguntas[i].pregAlt;
            var alt = {id:"",nombre:"",select:false};
            var size_preg = altern.length;
            var k = 0;
            var alternativas = [];
            var flag = true;
            for(j = 0 ; j<=size_preg;j++){
                if(altern[j] =='&' | j == size_preg){
                    alt.id = String.fromCharCode(97 + k);k++;
                    if(alt.nombre == $scope.evaluacionActual.preguntas[i].resAlu && flag){
                        alt.select = true;
                        flag = false;
                    }
                    alternativas.push(angular.copy(alt));
                    alt.id="";alt.nombre="";alt.select=false;
                    continue;
                }else{
                    alt.nombre = alt.nombre + altern[j];
                }
            }
            $scope.evaluacionActual.preguntas[i].alternativas=angular.copy(alternativas);
        }
    }
    
    $scope.finalizarCalificacion = function(){
   
        
       modal.mensajeConfirmacion($scope,"¿ Esta Seguro que desea Actualizar el Registro Auxiliar de Notas ?",function(){
            $scope.datos_alumno = {org:0,gra:'',sec:'',uni:'',per:'',numper:0,tip:'N'};
            $scope.datos_alumno.org = $rootScope.usuMaster.organizacion.organizacionID;
            $scope.datos_alumno.gra = $scope.evaluacionSel.gradoID;
            $scope.datos_alumno.sec = $scope.evaluacionSel.seccionID;
                var request = crud.crearRequest('evaluacion',1,'obtenerUnidadDidactica');
                request.setData({eva_maes_id:$scope.evaluacionSel.eva_maes_id});
                crud.listar("/web",request,function(res){
                    $scope.datos_alumno.uni = res.data.uni_did_id;
                    request = crud.crearRequest('cuadroHoras',1,'obtener_periodos_plan');
                    request.setData({unidadID:$scope.datos_alumno.uni});
                    crud.listar("/cuadroHoras",request,function(res){
                        $scope.datos_alumno.per = res.data.tip_per;
                        $scope.datos_alumno.numper = res.data.num_per; 
                        $scope.registrarNotas();
                    });
                    
                });

            });
    };
    
    $scope.registrarNotas = function(){
        var request = crud.crearRequest('notas_estudiante',1,'listarNotasIndicadoresAlumnos');
         request.setData($scope.datos_alumno);
            crud.listar('/submodulo_academico',request,function(response){
                if(response.responseSta){
                     $scope.est_general = response.data.estudiantes;
                     for(var i=0;i<settingAlumno.total;i++){
                        for(j=0;j<$scope.est_general.length;j++){
                            if(settingAlumno.dataset[i].per_id == $scope.est_general[j].id){
                                // Actualizamos la Nota de la Evaluacion en cada Indicador por Unidad Didactica
                                for(k=0;k<$scope.est_general[j].notas.length;k++){
                                    if($scope.est_general[j].notas[k].not != "0" && $scope.est_general[j].notas[k].not != "0.0" && $scope.est_general[j].notas[k].not != " " && $scope.est_general[j].notas[k].not != "NaN"){
                                       $scope.est_general[j].notas[k].not = (parseInt((parseFloat($scope.est_general[j].notas[k].not) + settingAlumno.dataset[i].nota)/2)).toString();
                                    }
                                    else{ 
                                       $scope.est_general[j].notas[k].not = (settingAlumno.dataset[i].nota).toString();
                                    }
                                    
                                }
                            }
                        }
                    }
                    $scope.datos_alumno.estudiantes = $scope.est_general;
                    var request = crud.crearRequest('notas_estudiante',1,'registrarNotasIndicador');
                    request.setData($scope.datos_alumno);
                    crud.insertar('/submodulo_academico',request,function(response){
                        modal.mensaje("CONFIRMACION",response.responseMsg);
                           if(response.responseSta){
                               modal.mensajeConfirmacion("Se Actualizo el Registro Auxiliar con exito");
                           }
                     },function(data){
                        console.info(data);
                     }); 
                }
                
            });
    }
    
    
    
    
    $scope.seleccionarTipo = function(){
        $scope.pregunta.opciones = [];
        $scope.opcion = {};
    };
    
    $scope.agregarOpcion = function(){
        if( verificarOpcion($scope.pregunta.tipo,$scope.opcion,$scope.pregunta.opciones.length) ){
            $scope.pregunta.opciones.push($scope.opcion);
            $scope.opcion = {};
        }
    };
    $scope.editarOpcion = function(i,o,p){
        if(o.edi){
            if(p){
                p.opciones[i] = o.copia;
                if(p.tipo=='R'){
                    p.opciones[p.orden.charAt(i)].concepto = p.opciones[i].respuesta;
                }
            }
            else                
                $scope.pregunta.opciones[i] = o.copia;
        }
        //si queremos editar
        else{
            o.copia = JSON.parse(JSON.stringify(o));
            o.edi =true;            
        }
    };
    $scope.eliminarOpcion = function(i,o){
        //si estamso cancelando la edicion
        if(o.edi){
            o.edi = false;
            delete o.copia;
        }
        //si queremos eliminar el elemento
        else{
            $scope.pregunta.opciones.splice(i,1);
        }
    };    
    $scope.agregarPregunta = function(){
        
        if(verificarPregunta($scope.pregunta)){
        
            $scope.evaluacion.preguntas.push($scope.pregunta);
        
            $scope.generarOrden($scope.pregunta);

            $scope.pregunta = {opciones:[],alternativas:[],tiempo:0};
        }
    };
    $scope.agregarMensaje = function(){
        
        if(verificarMensaje($scope.mensaje)){
        
            $scope.evaluacion.mensajes.push($scope.mensaje);

            $scope.mensaje = {};
        }
    };
    $scope.editarMensaje = function(i,o){
        if(o.edi){            
            $scope.evaluacion.mensajes[i] = o.copia;
        }
        //si queremos editar
        else{
            o.copia = JSON.parse(JSON.stringify(o));
            o.edi =true;            
        }
    };
    $scope.eliminarMensaje = function(i,o){
        //si estamso cancelando la edicion
        if(o.edi){
            o.edi = false;
            delete o.copia;
        }
        //si queremos eliminar el elemento
        else{
            $scope.evaluacion.mensajes.splice(i,1);
        }
    }; 
    $scope.generarOrden = function(p){
        
        JSON.parse(JSON.stringify(p.opciones)); 
        
        //inicializando los valores de la pregunta
        p.pregunta = "";
        p.respuesta = "";
        p.alternativa = "";
        p.orden = desordenarLista(p.opciones);
        
        console.log(p.orden);
        
        //buscando la respuesta
        var respuestaArray = [];
        p.opciones.forEach(function(item,i){
            if(i>0){
                p.pregunta+="&";
                p.respuesta+="&";
            }
            p.pregunta += item.contenido;
            p.respuesta += item.respuesta;
            
            if(p.tipo=='R'){
                respuestaArray.push( Number(p.orden.charAt(i))+1 );
                p.opciones[p.orden.charAt(i)].concepto = item.respuesta;
            }
            else
                respuestaArray.push( item.respuesta );
        });
        p.respuestaS = respuestaArray.toString();
        //fin de busqueda de respuesta
        
        var aAlternativas = [];
        //agregando la respuesta como un alternativa mas
        aAlternativas.push(p.respuestaS);
        //generando las alternativas
        for(var i = 0; i<4; ){
            var copia = [];
            var copiaS = "";
            if(p.tipo=='R'){
                copia = JSON.parse(JSON.stringify(respuestaArray));            
                desordenarLista(copia);
                copiaS = copia.toString();
            }
            else if(p.tipo=='V'){
                copiaS = desordenarVF(p.opciones.length);
            }
            else{
                aAlternativas.push("na");aAlternativas.push("na");aAlternativas.push("na");aAlternativas.push("na");
                break;
            }
            
            //buscando si ya existe la alternativa
            var existe = false;
            for(var j=0;j<aAlternativas.length;j++ ){
                if(aAlternativas[j] === copiaS)
                    existe = true;
            }
            //nueva alternativa
            if(!existe){
                i++;
                aAlternativas.push(copiaS);
            }          
            console.log("veces que entra para buscar alternativas");
        }
        
        //desordenando alternativas
        desordenarLista(aAlternativas);
        
        aAlternativas.forEach(function(item,i){
            p.alternativas[i] = {res:item,id:String.fromCharCode(97+i)};
            if(i>0){
                p.alternativa+="&";
            }
            p.alternativa += item;
        });
    };
    verificarOpcion = function(tipo,o,numero){
        if(!o.contenido || o.contenido==''){
            modal.mensaje("ADVERTENCIA","La pregunta debe tener contenido");
            return false;
        }
        if(!o.respuesta || o.respuesta==''){
            modal.mensaje("ADVERTENCIA","La pregunta debe tener una respuesta");
            return false;
        }        
        if(tipo == 'V'){
            if( o.respuesta=='F' || o.respuesta=='V' )
                return true;
            modal.mensaje("ADVERTENCIA","El tipo de pregunta solo adminte respuesta V o F");
            return false;
        }
        if(tipo == 'P'){
            if( numero == 0 )
                return true;
            modal.mensaje("ADVERTENCIA","El tipo de pregunta solo admite una pregunta");
            return false;
        }
        return true;
    };
    verificarPregunta = function(p){
        if(!p.titulo || p.titulo=='' ){
            modal.mensaje("ADVERTENCIA","Ingrese el titulo a la pregunta");
            return false;
        }
        if(!p.puntos || p.puntos<=0 ){
            modal.mensaje("ADVERTENCIA","Ingrese los puntos de la pregunta");
            return false;
        }
        if(p.tipo == 'P'){
            if( p.opciones.length == 1 )
                return true;
            modal.mensaje("ADVERTENCIA","El tipo de pregunta solo admite una pregunta");
            return false;
        }
        if( p.opciones.length >= 3 )
            return true;
        modal.mensaje("ADVERTENCIA","El tipo de pregunta admite como minimo tres opciones");
        return false;
    };
    verificarMensaje = function(m){
        if(!m.mensaje || m.mensaje=='' ){
            modal.mensaje("ADVERTENCIA","Ingrese el contenido del mensaje");
            return false;
        }
        if(!m.puntos || m.puntos<=0 ){
            modal.mensaje("ADVERTENCIA","Ingrese los puntos asignados al mensaje");
            return false;
        }
        return true;
    };
    function desordenarLista(a){
        var orden = [];
        
        for(var i = 0; i<a.length ; i++){
            orden.push(i)
        }
        for(var i = a.length,j=0,temp=0,temp2=0; 1<i ; i--){
            j = Math.floor( Math.random() * (i) );
            temp = a[i-1];
            
            a[i-1] = a[j];
            a[j] = temp;
            
            temp2 = orden[i-1];
            orden[i-1] = orden[j];
            orden[j] = temp2;
        }
        var oS = "";
        for(var i = 0; i<a.length ; i++){
            oS +=orden[i];
        }
        return oS;
    };
    function desordenarVF(n){
        var orden = ['V','F','V','F','V','F'];
        var valor = "";
        
        for(var i = 0; i<n; i++){
            if(valor.length>0)
                valor+=",";
            
            j = Math.floor( Math.random() * (6) );
            valor += orden[j];
        }
        return valor;
    };
    $scope.calcularSecciones = function(grado){
        $scope.seccionSel = '';
        $scope.areaSel = '';
        if(grado)      
            $scope.tablaEvaluacion.filter({ 'gradoID': grado.gradoID });
        else
            $scope.tablaEvaluacion.filter({});
    };
    $scope.filtrarSecciones = function(){
        $scope.areaSel = '';
        if($scope.seccionSel)
            $scope.tablaEvaluacion.filter({'gradoID': $scope.gradoSel.gradoID, 'seccionID': $scope.seccionSel.seccionID });
        else
            $scope.tablaTarea.filter({ 'gradoID': grado.gradoID });
    };
    $scope.filtrarAreas = function(){        
        if($scope.areaSel)
            $scope.tablaEvaluacion.filter({'gradoID': $scope.gradoSel.gradoID, 'seccionID': $scope.seccionSel.seccionID, 'areaID': $scope.areaSel });
        else        
            $scope.tablaEvaluacion.filter({'gradoID': $scope.gradoSel.gradoID, 'seccionID': $scope.seccionSel.seccionID });
    };
    
    $scope.filtrarEvaluacionporNombre = function(nomEva){
          $scope.tablaEvaluacion.filter({'gradoID': $scope.gradoSel.gradoID, 'seccionID': $scope.seccionSel.seccionID , 'nombre':nomEva});
    }
    
    
    
    
    $scope.buscarPlanEstudios = function(orgID,usuarioID){
        //preparamos un objeto request
        var request = crud.crearRequest('planEstudios',1,'buscarPlanEstudios');
        request.setData({organizacionID:orgID});
        crud.listar("/cuadroHoras",request,function(response){
            
            if(response.responseSta){
                $scope.plan = response.data;
                
                request = crud.crearRequest('cuadroHoras',1,'buscarDistribucionPorDocente');
                request.setData({planID:$scope.plan.planID,docenteID:usuarioID});
                crud.listar("/cuadroHoras",request,function(response){

                    if(response.responseSta){
                        $scope.grados = response.data;
                        $scope.grados.forEach(function (item){
                           var g = buscarObjeto($scope.plan.grados,"gradoID",item.gradoID);
                           item.grado = g.grado;
                           item.meta = g.meta;
                           var areas = buscarObjetos($scope.plan.areas,"gradoID",item.gradoID);                           
                           item.secciones.forEach(function (item2){
                                //si no tiene distribucion de areas, es docente de primaria se leasigna todas las areas segun el nivel
                                 if(item2.areas.length == 0)
                                     item2.areas = areas;
                                 else{
                                     item2.areas.forEach(function (item3){
                                         item3.area = buscarContenido(areas,"areaID","area",item3.areaID);
                                     });
                                 }
                            });
                        });
                        
                        $scope.obtener_evaluacion();
                    }
                },function(data){
                    console.info(data);
                });
                $scope.buscarEvaluaciones(usuarioID,orgID);
            }
        },function(data){
            console.info(data);
        });
    };
    
    $scope.obtener_evaluacion = function(){
        
       $scope.evaluacion_maestro = JSON.parse( window.atob( localStorage.getItem('evaluacion_maestro')) ); 
        
        var size_grados = $scope.grados.length;
            for(var i = 0 ; i<size_grados ; i++){
                if($scope.grados[i].gradoID == $scope.evaluacion_maestro.grad_id){
                    $scope.gradoSel = $scope.grados[i] ;
                    $scope.calcularSecciones($scope.gradoSel);
                    break;
                }
            }
            
            // SECCION
            var size_seccion = $scope.gradoSel.secciones.length;
            for(var j=0 ; j<size_seccion ; j++){
                if($scope.gradoSel.secciones[j].seccionID == $scope.tarea_maestro.sec_id){
                    $scope.seccionSel = $scope.gradoSel.secciones[j];
                    break;
                }
            }
            
            //AREA
            var size_areas = $scope.seccionSel.areas.length;
            for(k=0 ; k<size_areas ; k++){
                if($scope.seccionSel.areas[k].areaID == $scope.tarea_maestro.are_id){
                    $scope.areaSel = $scope.seccionSel.areas[k];
                    break;
                }
            }
            
              //Filtro valido para la busqueda de la tarea desde el Modulo Maestro
            $scope.filtrarEvaluacionporNombre($scope.evaluacion_maestro.nom);

            localStorage.removeItem('evaluacion_maestro');
 
    }
    
    $scope.buscarEvaluaciones = function(usuarioID,orgID){
        var request = crud.crearRequest('evaluacion',1,'listarEvaluacionesPorDocente');
        request.setData({planID:$scope.plan.planID,docenteID:usuarioID});
        crud.listar("/web",request,function(response){

            if(response.responseSta){
                evaluacions = response.data;

                evaluacions.forEach(function (item){
                   item.grado = buscarContenido($scope.plan.grados,"gradoID","grado",item.gradoID);

                   item.area = buscarContenido($scope.plan.areas,"areaID","area",item.areaID);
                });

                settingEvaluacion.dataset = evaluacions;

                $scope.tablaEvaluacion.settings(settingEvaluacion);
                $scope.buscarEquivalenciaNota(usuarioID,orgID);
            }
        },function(data){
            console.info(data);
        });
        
    };
    
    $scope.buscarEquivalenciaNota = function(usuarioID,orgID){
         var request = crud.crearRequest('notas_estudiante',1,'listarConfiguracionNota');
        request.setData({organizacionID:orgID,docenteID:usuarioID});
        crud.listar("/submodulo_academico",request,function(response){
           if(response.responseSta){
               $scope.configuracion_nota_docente = response.data;
           }  
        },function(data){
            console.info(data);
        });
    }
    
}]);
