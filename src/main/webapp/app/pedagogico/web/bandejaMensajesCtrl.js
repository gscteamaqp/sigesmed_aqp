app.controller("bandejaMensajesCtrl",["$scope","NgTableParams","crud","modal", function ($scope,NgTableParams,crud,modal){
        
    $scope.documentoSel = {archivo:{}};
    
    $scope.usuarios = [];   //usuarios y grupos de la organizacion
    
    //tabla de tareas
    var paramsMensaje = {count: 10};
    var settingMensaje = { counts: []};
    $scope.tablaMensaje = new NgTableParams(paramsMensaje, settingMensaje);
    
    $scope.estados = [{id:'N',title:"nuevo"},{id:'V',title:"visto"}];
    
    var paramsMensajeEnviados = {count: 10};
    var settingMensajeEnviados = { counts: []};
    $scope.tablaMensajeEnviados = new NgTableParams(paramsMensajeEnviados, settingMensajeEnviados);
    
    $scope.prepararEnviar = function(){
        $scope.mensaje = {documentos:[],destinatarios:[]};
        
        $('#modalNuevoMensaje').modal('show');
    };
    $scope.enviarMensaje = function(){
            
        var request = crud.crearRequest('mensaje',1,'enviarMensaje');
        request.setData($scope.mensaje);        
        crud.insertar("/web",request,function(res){            
            modal.mensaje("CONFIRMACION",res.responseMsg);
            if(res.responseSta){
                //recuperamos las variables que nos envio el servidor
                $scope.mensaje.mensajeID = res.data.mensajeID;
                $scope.mensaje.fechaEnvio = res.data.fechaEnvio;
                $scope.mensaje.usuarios = res.data.usuarios;

                //insertamos el elemento a la lista
                insertarElemento(settingMensajeEnviados.dataset,$scope.mensaje);
                $scope.tablaMensajeEnviados.reload();
                
                //cerramos la ventana modal
                $('#modalNuevoMensaje').modal('hide');
            }            
        },function(data){
            console.info(data);
        });
    };
    
    $scope.prepararVerEnviado = function(m){
        $scope.mensaje = m;
        
        //buscando los destinatarios
        $scope.mensaje.destinatarios = [];
        var usuarios = m.usuarios.split(" ");
        usuarios.forEach(function(item){            
            var u = buscarObjeto($scope.usuarios,"sessionID",Number(item));
            if(u)
                $scope.mensaje.destinatarios.push(u);                
        });
        
        
        //preparamos un objeto request
        var request = crud.crearRequest('mensaje',1,'listarDocumentos');
        request.setData({mensajeID:m.mensajeID});
        crud.listar("/web",request,function(res){
            
            if(res.responseSta){                
                $scope.mensaje.documentos = res.data;
            }
            $('#modalVerMensaje').modal('show');            
        },function(data){
            console.info(data);
        });        
    };
    $scope.prepararVerNuevo = function(m){
        $scope.mensaje = m;
        
        //buscando el remitente
        $scope.mensaje.destinatarios = [];
        var u = buscarObjeto($scope.usuarios,"sessionID",$scope.mensaje.remitente);
        if(u)
            $scope.mensaje.destinatarios.push(u);        
        
        //preparamos un objeto request
        var request = crud.crearRequest('mensaje',1,'listarDocumentos');
        request.setData({mensajeID:m.mensajeID});
        crud.listar("/web",request,function(res){
            
            if(res.responseSta){                
                $scope.mensaje.documentos = res.data;
            }
            $('#modalVerMensaje').modal('show');            
        },function(data){
            console.info(data);
        });   
        
        if($scope.mensaje.estado!='V'){
            
            var request = crud.crearRequest('mensaje',1,'actualizarBandejaMensaje');
            request.setData({bandejaMensajeID:m.bandejaMensajeID});
            crud.actualizar("/web",request,function(res){            
                if(res.responseSta){                
                    $scope.mensaje.estado = res.data.estado;
                }         
            },function(data){
                console.info(data);
            });
            
        }
    };
    
    
    $scope.agregarDocumento = function(){
        if(!$scope.documentoSel.documento || $scope.documentoSel.documento =="" ){
            modal.mensaje("CONFIRMACION","seleccione un documento adjunto");
            return;
        }
        //como maximo 5 archivos
        if(5 <= $scope.mensaje.documentos.length ){
            modal.mensaje("CONFIRMACION","NO debe superar el numero de documentos permitidos");
            return;
        }
        
        $scope.mensaje.documentos.push($scope.documentoSel);
        $scope.documentoSel = {archivo:{}};
    };
    $scope.editarDocumento = function(i,d){
        //si estamso editando
        if(d.edi){
            $scope.mensaje.documentos[i] = d.copia;
        }
        //si queremos editar
        else{
            d.copia = JSON.parse(JSON.stringify(d));
            d.edi =true;            
        }
    };
    $scope.eliminarDocumento = function(i,d){
        //si estamso cancelando la edicion
        if(d.edi){
            d.edi = false;
            delete d.copia;
        }
        //si queremos eliminar el elemento
        else{
            $scope.mensaje.documentos.splice(i,1);
        }
    };
    
    $scope.elegirUsuario = function(item){
        if( !item.sel ){
            item.sel = true;
            $scope.mensaje.destinatarios.push(item);            
        }
        $scope.selected = "";
    };
    $scope.deselegirUsuario = function(item,i){
        item.sel = false;
        $scope.mensaje.destinatarios.splice( i ,1);
    };
    
    $scope.buscarMensajes = function(){
        //bandeja de mensaje recividos
        var request = crud.crearRequest('mensaje',1,'listarMensajes');
        crud.listar("/web",request,function(res){
            
            if(res.responseSta){                        
                settingMensaje.dataset = res.data;                        
                $scope.tablaMensaje.settings(settingMensaje);
            }
        },function(data){
            console.info(data);
        });
        //bandeja de mensajes enviados
        request = crud.crearRequest('mensaje',1,'listarMensajesEnviados');
        crud.listar("/web",request,function(res){            
            if(res.responseSta){                        
                settingMensajeEnviados.dataset = res.data;                        
                $scope.tablaMensajeEnviados.settings(settingMensajeEnviados);
            }
        },function(data){
            console.info(data);
        });
    };
    $scope.listarUsuarios = function(orgID){
        //preparamos un objeto request
        var request = crud.crearRequest('usuarioSistema',1,'listarUsuarioSessionesPorOrganizacion');        
        request.setData({organizacionID:orgID});
        crud.listar("/configuracionInicial",request,function(data){
            $scope.usuarios = data.data;
        },function(data){
            console.info(data);
        });
    };
    
}]);
