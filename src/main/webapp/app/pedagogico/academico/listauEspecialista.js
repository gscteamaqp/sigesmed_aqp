/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function UtilesEstudianteService($rootScope,crud){
    this.listarArticulos = function(succ,err){
        var request = crud.crearRequest('utiles',1,'listarArticulo');
        crud.listar('/submodulo_academico',request,succ,err);
    };
    
    this.listarListas = function(succ,err){
        var org = $rootScope.usuMaster.organizacion.organizacionID;
        var user = $rootScope.usuMaster.usuario.usuarioID;
        var request = crud.crearRequest('utiles',1,'listarListaUtiles');
        request.setData({org:org,usr:user});
        //request.setMetadataValue('org',''+org);
        
        crud.listar('/submodulo_academico',request,succ,err);
    };
    
    this.registrarArticulo = function(articulo,succ,err){
        var request = crud.crearRequest('utiles',1,'registrarArticulo');
        request.setData(articulo);
        crud.insertar('/submodulo_academico',request,succ,err);
    };
    this.registrarLista = function(data,succ,err){
        var org = $rootScope.usuMaster.organizacion.organizacionID;
        var user = $rootScope.usuMaster.usuario.usuarioID;
        var request = crud.crearRequest('utiles',1,'registrarListaUtiles');
        //request.setMetadataValue('org',''+org);
        request.setData({org:org,usr:user});
        //request.setData(data);
        crud.insertar('/submodulo_academico',request,succ,err);
    };
    
    this.editarArticulo = function (articulo,succ,err) {
        var request = crud.crearRequest('utiles',1,'actualizarArticulo');
        request.setData(articulo);
        crud.actualizar('/submodulo_academico',request,succ,err);
    };

    this.eliminarArticulo = function (id,succ,err) {
        var request = crud.crearRequest('utiles',1,'eliminarArticulo');
        request.setMetadataValue('id',''+id);
        crud.eliminar('/submodulo_academico',request,succ,err);
    };
    
    this.buscarArticulos = function (arti,succ,err){
        var request = crud.crearRequest('utiles',1,'buscarArticuloParaLista');
        request.setData(arti);
        crud.listar('/submodulo_academico',request,succ,err);
    };
}

angular.module('app')
        .service('UtilesEstudianteService',['$rootScope','crud',UtilesEstudianteService])
        .controller('listauEspecialista',['$log','$location','modal','UtilAppServices','NgTableParams','UtilesEstudianteService','crud',function($log,$location,modal,util,NgTableParams,utilesService,crud){
        var self = this;
       // ------------------ Articulos ----------------------------------------
        self.tablaArticulos = new NgTableParams({count:8},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });
        try{
            self.showProgress = true;
            utilesService.listarArticulos(function(response){
                $log.log('reponse',response);
                self.showProgress = false;
                self.tablaArticulos.settings().dataset = response.data;
                self.tablaArticulos.reload();
            },function(errResponse){
                self.showProgress = false;
                modal.mensaje("ERROR","No se pudieron obtener los datos");
            });
        }catch(err){
            modal.mensaje("ERROR","No se pudieron obtener los datos");
        }
        // -----------------Mi LISTA DE UTILES --------------------------------
         
        self.tablaLista = new NgTableParams({count:8},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });
        try{
            self.showProgress = true;
            utilesService.listarListas(function(response){
                $log.log('reponse',response);
                self.showProgress = false;
                self.tablaLista.settings().dataset = response.data;
                self.tablaLista.reload();
            },function(errResponse){
                self.showProgress = false;
                modal.mensaje("ERROR","No se pudieron obtener los datos");
            });
        }catch(err){
            modal.mensaje("ERROR","No se pudieron obtener los datos");
        }
        //***+++++++++++++++++++++++++++++++++++
        self.nuevoArticulo = function(){
            var modalInstance = util.openModal('nuevoArticulo.html','registrarArticuloCtrl','lg','ctrl',null);
            modalInstance.result.then(function(articulo){
                self.tablaArticulos.settings().dataset.push(articulo);
                self.tablaArticulos.reload();
                modal.mensaje("ver","se ingreso al modal")
          
            },function(response){
                modal.mensaje("CANCELAR","Se cancelo la accion");
                
            });
        }
        self.eliminarArticulo = function($e,row){
            util.openDialog('Eliminar Articulo','¿Seguro que desea eliminar\n el articulo?',$e,
                function (response) {
                    utilesService.eliminarArticulo(row.id,function(response){
                        if(response.response === 'OK'){
                            _.remove(self.tablaArticulos.settings().dataset,function(item){
                                return row === item;
                            });
                            self.tablaArticulos.reload();
                        }else {
                            modal.mensaje("ERROR","No se puede eliminar el cargo por defecto");
                        }

                    },function(errResponse){
                        modal.mensaje("ERROR","El servidor no responde");
                    });
            },function () {
                    modal.mensaje("CANCELAR","Se cancelo la accion");
                });
        }
        self.editarArticulo = function(row){
            var resolve = {
                data:function(){
                    return{
                        articulo : angular.extend(new Object(),row)
                    }
                }
            };
            var modalInstance = util.openModal('nuevoArticulo.html','editarArticuloCtrl','lg','ctrl',resolve);
            modalInstance.result.then(function (response) {
                if(response.response === 'OK'){
                    angular.extend(row,response.data);
                }else if(response.response === 'BAD'){
                    modal.mensaje("ERROR","Hubo un error en el servidor");
                }
            },function (errResponse) {
                modal.mensaje("CANCELAR","Se cancelo la accion");
            });
        }
        //***********************************
        self.verMonto = function(){
            var modalInstance = util.openModal('montos.html','verMontoCtrl','lg','ctrl');
            /*modalInstance.result.then(function (response) {
                if(response.response === 'OK'){
                    angular.extend(row,response.data);
                }else if(response.response === 'BAD'){
                    modal.mensaje("ERROR","Hubo un error en el servidor");
                }
            },function (errResponse) {
                modal.mensaje("CANCELAR","Se cancelo la accion");
            });*/
        }
        //**************************************
        self.nuevaLista = function(){
            var resolve = {
                lista:function(){
                    return {
                        edit:false
                    };
                }
            };
            var modalInstance = util.openModal('nuevaLista.html','registrarListaCtrl','lg','ctrl',resolve);
            modalInstance.result.then(function(response){
                if(response.response === 'BAD'){
                    modal.mensaje("ERROR","No se pudieron obtener los datos");
                }else {
                    self.tablaLista.settings().dataset.push(response.data);
                    self.tablaLista.reload();
                    modal.mensaje("CORRECTO","Se realizo la transaccion");
                }
          
            },function(response){
                modal.mensaje("CANCELAR","Se cancelo la accion");
                
            });
        }
        self.nuevaPlantilla = function($e,row){
          
            var request = crud.crearRequest('utiles',1,'registrarPlantillaLista');
            request.setData({idLis:row.id});  
            crud.insertar('/submodulo_academico',request,function(r){
    
                if(r.response === 'BAD'){
                    $log.log(r.responseMsg);
                }
                else if(r.response === 'OK'){
                    $log.log(r.responseMsg);
                    
                }
             },function (errResponse){
                 $log.log(errResponse);
                
            });
        }
        self.reporteLista = function(row){
            var request = crud.crearRequest("reportes",1,"reporteLista");
            var obj = {lis: row.id}
            request.setData(obj);
            
            crud.insertar('/submodulo_academico',request,function(response){
                if(response.responseSta){
                        $log.log("reponse rr",response.data);
                    verDocumento(response.data.archivo)
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function (errResponse) {
                modal.mensaje('ERROR','El servidor no responde');
            });
        }
    }])
    .controller('registrarArticuloCtrl',['$log','$uibModalInstance','UtilesEstudianteService',function($log,$uibModalInstance,utilesService){
        var self = this;
        self.titulo = 'Registrar Articulo';
        self.tipo = [{id: "1", nomt: "Aseo"},{id: "2", nomt: "Escritorio"},{id: "3", nomt: "Cartuchera"},{id: "4", nomt: "Otro"}];
        self.guardar = function (){
            utilesService.registrarArticulo(self.articulo,function(r){
               
                if(r.response === 'BAD'){
                    $log.log(r.responseMsg);
                }
                
                else if(r.response === 'OK'){
                    $log.log(r.responseMsg);
                    $uibModalInstance.close(r.data);
                }
             },function (errResponse){
                 $log.log(errResponse);
                 $uibModalInstance.dismiss('cancel');
            });
        }
        self.cancelar = function(){
            $uibModalInstance.dismiss('cancel');
        }
    }])

    .controller('editarArticuloCtrl',['$log','$uibModalInstance','UtilesEstudianteService','data',function($log,$uibModalInstance,utilesService,data){
        var self = this;
        self.tipo = [{id: "1", nomt: "Aseo"},{id: "2", nomt: "Escritorio"},{id: "3", nomt: "Cartuchera"},{id: "4", nomt: "Otro"}];
        
        self.titulo = 'Editar Articulo';
        self.articulo = data.articulo;
        var auxArt = _.find(self.tipo,function(o){
            return o.nomt.toLowerCase() === self.articulo.tip.toLowerCase();
        });
        self.articulo.tip = auxArt;
        
        self.guardar = function(){
            utilesService.editarArticulo(self.articulo,function (response) {
                response.data = self.articulo;
                response.data.tip = self.articulo.tip.nomt;
                $uibModalInstance.close(response);
            },function (errResponse) {
                $log.log(errResponse);
                $uibModalInstance.dismiss('cancel');
            });
        }
        self.cancelar = function(){
            $uibModalInstance.dismiss('cancel');
        }
    }])

    .controller('registrarListaCtrl',['$log','$uibModalInstance','UtilesEstudianteService','NgTableParams','crud','modal','$rootScope',function($log,$uibModalInstance,utilesService,NgTableParams,crud,modal,$rootScope){
        var self = this;
        self.cos = 0.0;
        self.can = 2;
        self.tot = 0.0;
        //self.pre = self.pr
        self.anios = [{id: "1", anio: "2016"},{id: "2", anio: "2017"},{id: "3", anio: "2018"},{id: "4", anio: "2019"},{id: "5", anio: "2020"},{id: "6", anio: "2021"},{id: "7", anio: "2022"},
        {id: "8", anio: "2023"},{id: "9", anio: "2024"},{id: "10", anio: "2025"},{id: "11", anio: "2026"},{id: "12", anio: "2027"},{id: "13", anio: "2028"},{id: "14", anio: "2029"},{id: "15", anio: "2030"},
        {id: "16", anio: "2031"},{id: "17", anio: "2032"},{id: "18", anio: "2033"},{id: "19", anio: "2034"},{id: "20", anio: "2035"},{id: "21", anio: "2036"},{id: "22", anio: "2037"},{id: "23", anio: "2038"}];
            //self.titulo = !data.edit? 'Nueva Lista' : 'Editar Lista';
        listarGrados();
        function listarGrados(){
            var request = crud.crearRequest('anecdotario',1,'listarGrados');
            request.setData({org:$rootScope.usuMaster.organizacion.organizacionID, doc:$rootScope.usuMaster.usuario.usuarioID});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.niveles = response.data.niveles;
                    self.areas = response.data.areas;
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
            });
        }
         self.mostrarGrados =  function(nivel){
            self.grados = nivel.grados;
        }
        self.mostrarSecciones =  function(grado){
            self.secciones = grado.secciones;
        }
        
        //------------- Agregar Articulos a lista ------------------------
        self.tablaArtiLista = new NgTableParams({count:4},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });
        
        self.buscarArticuloLista = function(arti){
            if(arti === undefined || ( (arti.nom === undefined || arti.nom ==="" ))){
                modal.mensaje("ERROR","Debe llenar el campo de busqueda");
            }else{
                //hacemos la busqueda de los articulos
                self.showProgress = true;
                utilesService.buscarArticulos(arti,function(response){
                self.showProgress = false;
                   
                    if(response.response === 'OK'){
                        self.tablaArtiLista.settings().dataset = response.data;
                        self.tablaArtiLista.reload();
                        $log.log("response : ",response.data)
                    }else if(response.response === 'BAD'){
                        self.tablaArtiLista.settings().dataset = [];
                        self.tablaArtiLista.reload();
                        modal.mensaje('ERROR',response.responseMsg);
                    }
                },function(errResponse){
                    self.showProgress = false;
                    modal.mensaje("ERROR","El Servidor no responde")
                });
            }
        }
        self.limpiarArticulos = function(){
            self.tablaArtiLista.settings().dataset = [];
            self.tablaArtiLista.reload();
        }
        
        self.articulosSeleccionados = new NgTableParams({count:4},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });
        
        self.agregarArticuloSelect = function (row){
            row.can=self.can;
            row.tot=row.pre * self.can;
            self.articulosSeleccionados.settings().dataset.push(row);
            self.articulosSeleccionados.reload();
            
            self.tot = row.pre * self.can;
            self.cos = self.cos + self.tot;
        }
        self.eliminarArticuloSelect = function (row){
            _.remove(self.articulosSeleccionados.settings().dataset,function(item){
                return row === item;
            });
            self.articulosSeleccionados.reload();
            self.cos = self.cos - self.tot;
        }
        self.editarArticuloSelect = function (row){
            
        }
        
        // ------------Fin Agregar Articulos a lista ---------------------
        self.guardar = function (){
            self.lista.cos = self.cos;
            self.lista.can = self.can;
            self.lista.tip = "P";
            $log.log("self area",self.area);
            var data = {
                org: $rootScope.usuMaster.organizacion.organizacionID,
                user:$rootScope.usuMaster.usuario.usuarioID,
                niv: self.nivel.id,
                gra: self.grado.id,
                sec: self.seccion,
                are: self.area.cod,           
                art: self.articulosSeleccionados,
                lis: self.lista
            }
            console.log("ver Lista a enviar", data);
        
            var request = crud.crearRequest('utiles',1,'registrarListaUtiles');
            request.setData(data);
            crud.insertar('/submodulo_academico',request,function(r){
                console.log("ver Lista enviada a RegistrarListaTX", data);
                if(r.response === 'BAD'){
                    $log.log(r.responseMsg);
                }
                else if(r.response === 'OK'){
                    $log.log(r.responseMsg);
                    $uibModalInstance.close(r.data);
                }
             },function (errResponse){
                 $log.log(errResponse);
                 $uibModalInstance.dismiss('cancel');
            });
        
        }
        self.cancelar = function(){
            $uibModalInstance.dismiss('cancel');
        }
    }])

    .controller('verMontoCtrl',['$log','$uibModalInstance','UtilesEstudianteService',function($log,$uibModalInstance,utilesService){
        var self = this;
        self.titulo = 'Ver Monto';
        self.anios = [{id: "1", anio: "2016"},{id: "2", anio: "2017"},{id: "3", anio: "2018"},{id: "4", anio: "2019"},{id: "5", anio: "2020"},{id: "6", anio: "2021"},{id: "7", anio: "2022"},
        {id: "8", anio: "2023"},{id: "9", anio: "2024"},{id: "10", anio: "2025"},{id: "11", anio: "2026"},{id: "12", anio: "2027"},{id: "13", anio: "2028"},{id: "14", anio: "2029"},{id: "15", anio: "2030"},
        {id: "16", anio: "2031"},{id: "17", anio: "2032"},{id: "18", anio: "2033"},{id: "19", anio: "2034"},{id: "20", anio: "2035"},{id: "21", anio: "2036"},{id: "22", anio: "2037"},{id: "23", anio: "2038"}];
        self.anio = self.anios[1];
        self.cancelar = function(){
            $uibModalInstance.dismiss('cancel');
        }
    }]);