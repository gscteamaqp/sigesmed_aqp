app.controller("configuracion_equivalencia_notaCtrl",["$scope","NgTableParams","crud","modal","$rootScope", function ($scope,NgTableParams,crud,modal,$rootScope){
 
    $scope.equivalencia_notas = [];
    
    $scope.nueva_equivalencia = {titulo:"",conf_id:0,cod:"",des:"",not_min:0,not_max:0,estado:'N'};
    
     /*Tabla de la Configuracion Nota del Docente*/
    var params = {count: 10};
    var setting = {counts: []};
    $scope.tabla_configuracion_nota = new NgTableParams(params, setting);
    
    $scope.listarEquivalenciasDocente = function(orgID,docID){
        var request = crud.crearRequest('notas_estudiante',1,'listarConfiguracionNota');
        request.setData({organizacionID:orgID,docenteID:docID});
        crud.listar("/submodulo_academico",request,function(response){
           if(response.responseSta){
               setting.dataset = response.data;
               iniciarPosiciones(setting.dataset);
               $scope.tabla_configuracion_nota.settings(setting);
               $scope.tabla_configuracion_nota.reload();
           }  
        },function(data){
            console.info(data);
        });
    };
    
    $scope.crudEquivalenciaNota = function(docID,orgID,estado_){
        
         modal.mensajeConfirmacion($scope, "Seguro que desea Realizar dicha Accion ?", function () {
            var request = crud.crearRequest('notas_estudiante',1,'crudConfiguracionNota');
            request.setData({organizacionID:orgID,docenteID:docID,equivalDoc:$scope.nueva_equivalencia,estado:estado_});
            crud.insertar("/submodulo_academico",request,function(response){
            if(response.responseSta){
                switch(estado_){
                    case 1:
                       $scope.nueva_equivalencia.conf_id = response.data.conf_id;
                       insertarElemento(setting.dataset,angular.copy($scope.nueva_equivalencia));           
                       break; 
                    case 2:
                        var index = $scope.nueva_equivalencia.i;
                        setting.dataset[index] = angular.copy($scope.nueva_equivalencia);     
                        break; 
                    case 3:
                        var index = $scope.nueva_equivalencia.i;
                        eliminarElemento(setting.dataset,index);
                        break;
                }
                $scope.tabla_configuracion_nota.reload();
                $('#modalNuevo').modal('hide');
            }
            },function(data){
                console.info(data);
            });
         });
    }
    
    $scope.prepararEditar = function(equivalencia){
        $scope.nueva_equivalencia = {conf_id:equivalencia.conf_id,
                                     cod:equivalencia.cod,
                                     des:equivalencia.des,
                                     not_min:equivalencia.not_min,
                                     not_max:equivalencia.not_max,
                                     estado:'E',
                                     i:equivalencia.i       
        };
        $scope.nueva_equivalencia.titulo = "EDITAR CONFIGURACION NOTA";
        $('#modalNuevo').modal('show');
    }
    $scope.prepararNuevo = function(){
         $scope.nueva_equivalencia = {conf_id:0,cod:"",des:"",not_min:0,not_max:0,estado:'N'};
         $scope.nueva_equivalencia.titulo = "NUEVA CONFIGURACION NOTA";
         $('#modalNuevo').modal('show');
    }
    $scope.prepararEliminar = function(equivalencia,docID,orgID){
        $scope.nueva_equivalencia = {conf_id:equivalencia.conf_id,
                                     cod:equivalencia.cod,
                                     des:equivalencia.des,
                                     not_min:equivalencia.not_min,
                                     not_max:equivalencia.not_max,
                                     estado:'E',
                                     i:equivalencia.i
        };
        $scope.crudEquivalenciaNota(docID,orgID,3);
    }
    

    }]);