/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular.module('app')
        .controller('notasAcademico', ['$log', '$location', 'modal', 'UtilAppServices', 'NgTableParams', '$rootScope', 'crud', function ($log, $location, modal, util, NgTableParams, $rootScope, crud) {
                var self = this;
                self.abrirCalendar = false;
                self.openCalendar = function () {
                    self.abrirCalendar = true;
                };
                self.dateOptions = {
                    formatYear: 'yyyy',
                    maxDate: new Date(),
                    minDate: new Date(1900, 1, 1),
                    startingDay: 1
                };

                self.tablaAsistenciaEst = new NgTableParams({count: 8}, {
                    counts: [],
                    paginationMaxBlocks: 13,
                    paginationMinBlocks: 2,
                    dataset: []
                });
                self.patron = /^\d+$/;
                self.numSel = true; // Nota numerica seleccionada
                
                listarNiveles();
                function listarNiveles() {
                    var request = crud.crearRequest('anecdotario', 1, 'listarGrados');
                    request.setData({org: $rootScope.usuMaster.organizacion.organizacionID, doc: $rootScope.usuMaster.usuario.usuarioID});
                    crud.listar('/maestro', request, function (response) {
                        if (response.responseSta) {
                            self.niveles = response.data.niveles;
                        } else {
                            modal.mensaje('ERROR', response.responseMsg);
                        }
                    }, function (errResponse) {
                    });
                }
                ;
                function listarPeriodos(nivel) {
                    var request = crud.crearRequest('notas_estudiante', 1, 'listarPeridosPlan');
                    request.setData({org: $rootScope.usuMaster.organizacion.organizacionID, niv: nivel.id});
                    crud.listar('/submodulo_academico', request, function (response) {
                        if (response.responseSta) {
                            $log.log('respuesta p', response.data);
                            self.periodos = [];
                            var meses = 9;
                            var numEva = Math.floor(meses / response.data.fac);
                            for (var i = 1; i <= numEva; i++) {
                                self.periodos.push({
                                    id: response.data.id,
                                    nom: response.data.id == 'B' ? 'Bimestre ' + i : 'Trimestre' + i,
                                    num: i
                                });
                            }

                        } else {
                            modal.mensaje('ERROR', response.responseMsg);
                        }
                    }, function (errResponse) {
                    });
                }
                ;
                self.mostrarGrados = function (nivel) {
                    self.grados = nivel.grados;
                    listarPeriodos(nivel);
                };
                self.mostrarSecciones = function (grado) {
                    self.secciones = grado.secciones;

                };
                self.mostrarAlumnos = function () {
                    var request = crud.crearRequest('anecdotario', 1, 'listarEstudiantes');
                    request.setData({org: $rootScope.usuMaster.organizacion.organizacionID, gra: self.grado.id, sec: self.seccion});
                    crud.listar('/maestro', request, function (response) {
                        if (response.responseSta) {
                            self.estudiantes = response.data;
                            angular.forEach(self.estudiantes, function (objest, keyest) {
                                objest.notas = [];
                                angular.forEach(self.indicadores, function (objind, keyind) {
                                    objest.notas.push({ind: objind.id, not: ""});
                                });
                            });
                        } else {
                            modal.mensaje('ERROR', response.responseMsg);
                        }
                    }, function (errResponse) {
                    });
                };
                self.mostrarAreas = function (seccion) {
                    var request = crud.crearRequest('acomp', 1, 'listarCursosDocente');
                    request.setData({doc: $rootScope.usuMaster.usuario.usuarioID, org: $rootScope.usuMaster.organizacion.organizacionID, gra: self.grado.id, secc: seccion});
                    crud.listar('/maestro', request, function (response) {
                        if (response.responseSta) {
                            self.areas = response.data;
                        } else {
                            modal.mensaje('ERROR', response.responseMsg);
                        }
                    }, function (errResponse) {});
                };
                self.mostrarUnidades = function (area) {
                    var request = crud.crearRequest('notas_estudiante', 1, 'listarUnidadesDidacticas');
                    request.setData({usr: $rootScope.usuMaster.usuario.usuarioID, org: $rootScope.usuMaster.organizacion.organizacionID, gra: self.grado.id, are: area.id});
                    crud.listar('/submodulo_academico', request, function (response) {
                        if (response.responseSta) {
                            self.unidades = response.data;
                        } else {
                            modal.mensaje('ERROR', response.responseMsg);
                        }
                    }, function (errResponse) {});
                };
                self.mostrarSesiones = function (unidad) {
                    var request = crud.crearRequest('sesiones', 1, 'listarSesiones');
                    request.setData({unid: unidad.cod});
                    crud.listar('/maestro', request, function (response) {
                        if (response.responseSta) {
                            self.sesiones = response.data;
                        } else {
                            modal.mensaje('ERROR', response.responseMsg);
                        }
                    }, function (errResponse) {});
                };
                self.mostrarCompetencias = function (unidad) {
                    var request = crud.crearRequest('unidades', 1, 'listarCompetenciasUnidad');
                    request.setData({cod: unidad.cod, tip: 1});
                    crud.listar('/maestro', request, function (response) {
                        if (response.responseSta) {
                            self.competencias = response.data;
                        } else {
                            modal.mensaje('ERROR', response.responseMsg);
                        }
                    }, function (errResponse) {});
                };
                self.mostrarCapacidades = function (competencia) {
                    self.capacidades = competencia.capacidades;
                };
                self.sesionMostrarIndicadores = function (sesion) {
                    if (self.periodo != undefined && self.periodo != null) {
                        self.mostrarIndicadores(self.periodo);
                    }
                };
                self.mostrarIndicadores = function (periodo) {
                    if (self.sesion == undefined || self.sesion == null) {
                        return;
                    }
                    var request = crud.crearRequest('notas_estudiante', 1, 'listarNotasIndicadoresAlumnos');
                    request.setData({
                        org: $rootScope.usuMaster.organizacion.organizacionID,
                        gra: self.grado.id,
                        sec: self.seccion,
                        ses: self.sesion.id,
                        per: periodo.id,
                        numper: periodo.num
                    });
                    crud.listar('/submodulo_academico', request, function (response) {
                        if (response.responseSta) {
                            $log.log('data response', response.data);
                            self.indicadores = response.data.indicadores;
                            self.estudiantes = response.data.estudiantes;
                        } else {
                            modal.mensaje('ERROR', response.responseMsg);
                        }
                    }, function (errResponse) {});
                };
                self.registrarNotasIndicador = function () {
                    
                            var request = crud.crearRequest('notas_estudiante', 1, 'registrarPromedioIndicadores');
                            request.setData({
                                ses: self.sesion.id,
                                per: self.periodo.id,
                                numper: self.periodo.num,
                                are: self.area.id,
                                usr: $rootScope.usuMaster.usuario.usuarioID,
                                org: $rootScope.usuMaster.organizacion.organizacionID,
                                estudiantes: self.estudiantes
                            });
                            crud.insertar('/submodulo_academico', request, function (response) {
                                if (response.responseSta) {
                                    $log.log('data response', response.data);
                                    modal.mensaje('CONFIRMACION', response.responseMsg);

                                    var request = crud.crearRequest('notas_estudiante', 1, 'registrarPromedioCompetencias');
                                    request.setData({
                                        ses: self.sesion.id,
                                        per: self.periodo.id,
                                        numper: self.periodo.num,
                                        are: self.area.id,
                                        usr: $rootScope.usuMaster.usuario.usuarioID,
                                        org: $rootScope.usuMaster.organizacion.organizacionID,
                                        estudiantes: self.estudiantes
                                    });
                                    crud.insertar('/submodulo_academico', request, function (response) {
                                        if (response.responseSta) {
                                            $log.log('data response', response.data);
                                            modal.mensaje('CONFIRMACION', response.responseMsg);

                                            var request = crud.crearRequest('notas_estudiante', 1, 'registrarPromedioAreaPorPeriodo');
                                            request.setData({
                                                ses: self.sesion.id,
                                                per: self.periodo.id,
                                                numper: self.periodo.num,
                                                are: self.area.id,
                                                org: $rootScope.usuMaster.organizacion.organizacionID,
                                                usr: $rootScope.usuMaster.usuario.usuarioID,
                                                estudiantes: self.estudiantes
                                            });
                                            crud.insertar('/submodulo_academico', request, function (response) {
                                                if (response.responseSta) {
                                                    $log.log('data response', response.data);
                                                    modal.mensaje('CONFIRMACION', response.responseMsg);
                                                } else {
                                                    modal.mensaje('ERROR', response.responseMsg);
                                                }
                                            }, function (errResponse) {});
                                        } else {
                                            modal.mensaje('ERROR', response.responseMsg);
                                        }
                                    }, function (errResponse) {});

                                } else {
                                    modal.mensaje('ERROR', response.responseMsg);
                                }
                            }, function (errResponse) {});

                        } 
            }]);
