document.write('<script src="pedagogico/capacitacion/evaluacionCapacitacionCtrl.js"></script>');
var mcApp = angular.module('app');
mcApp.config(['$routeProvider',function($routeProvider){
    $routeProvider.when('/misCapacitaciones/:codSed',{
            templateUrl:'pedagogico/capacitacion/temaCapacitacion.html',
            controller:'temaCapacitacionCtrl',
            controllerAs:'temaCtrl'
        }).when('/misCapacitaciones/foro/:temCod',{
            templateUrl:'pedagogico/capacitacion/foroTema.html',
            controller:'foroTemaCtrl',
            controllerAs:'foroCtrl'
        }).when('/misCapacitaciones/desarrollo/:temCod',{
            templateUrl:'pedagogico/capacitacion/desarrolloTema.html',
            controller:'desarrolloTemaCtrl',
            controllerAs:'desarrolloCtrl'
        }).when('/misCapacitaciones/evaluaciones/:temCod',{
            templateUrl:'pedagogico/capacitacion/evaluacionCapacitacion.html',
            controller:'evaluacionCapacitacionCtrl',
            controllerAs:'evaCtrl'
        }).when('/misCapacitaciones/evaluacion/:evaCod',{
            templateUrl:'pedagogico/capacitacion/evaluacion.html',
            controller:'misEvaluacionesCtrl',
            controllerAs:'misEvaCtrl'
        });
}]);

mcApp.factory('shareVariables', function () {
    var sede = {
        sedCod: '',
        sedEst: '',
        sedNom: '',
        capNom: '',
        capTip: '',
        fecIni: '',
        fecFin: ''
    };
    
    var docCap = '';
    
    var tema = {
        temCod: '',
        temNom: ''
    };
    
    var evaluacion = {};

    return {
        getSedCod: function () {
            return sede.sedCod;
        },
        setSedCod: function (sedCod) {
            sede.sedCod = sedCod;
        },
        getSedEst: function () {
            return sede.sedEst;
        },
        setSedEst: function (sedEst) {
            sede.sedEst = sedEst;
        },
        getSedNom: function () {
            return sede.sedNom;
        },
        setSedNom: function (sedNom) {
            sede.sedNom = sedNom;
        },
        getCapNom: function () {
            return sede.capNom;
        },        
        setCapNom: function (capNom) {
            sede.capNom = capNom;
        },
        getCapTip: function () {
            return sede.capTip;
        },        
        setCapTip: function (capTip) {
            sede.capTip = capTip;
        },
        getFecIni: function () {
            return sede.fecIni;
        },
        setFecIni: function (fecIni) {
            sede.fecIni = fecIni;
        },
        getFecFin: function () {
            return sede.fecFin;
        },
        setFecFin: function (fecFin) {
            sede.fecFin = fecFin;
        },
        getDocCap: function () {
            return docCap;
        },
        setDocCap: function (state) {
            docCap = state;
        },
        getTemCod: function () {
            return tema.temCod;
        },
        setTemCod: function (temCod) {
            tema.temCod = temCod;
        },
        getTemNom: function () {
            return tema.temNom;
        },
        setTemNom: function (temNom) {
            tema.temNom = temNom;
        },
        setEva: function(test) {
            evaluacion = test;
        },
        getEva: function() {
            return evaluacion;
        }
    };
});

app.controller("misCapacitacionesCtrl", ["$rootScope", "$scope", "NgTableParams", "crud", "modal", "$location", "shareVariables", "$uibModal", '$window', 'urls', function ($rootScope, $scope, NgTableParams, crud, modal, $location, shareVariables, $uibModal, $window, urls) {
    var settingCapacitacion = {counts: []};
    $scope.tablaCapacitaciones = new NgTableParams({count: 10}, settingCapacitacion);    
    
    $scope.listarCursosCapacitacion = function() {
        var request = crud.crearRequest('cursos', 1, 'listarCursos');
        request.setData({opt: 6,
                         usu: $rootScope.usuMaster.usuario.usuarioID,
                         rol: $rootScope.usuMaster.rol.rolID});

        crud.listar('/curso_capacitacion', request, function (success) {
            if(success.data.cursos.length > 0) {
                $scope.state = success.data.state;
                settingCapacitacion.dataset = success.data.cursos;
                iniciarPosiciones(settingCapacitacion.dataset);
                $scope.tablaCapacitaciones.settings(settingCapacitacion);
            } else
                modal.mensaje("MENSAJE", "No existen capacitaciones para listar");
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar los cursos de capacitación " + error.responseMsg);
        });
    };
    
    $scope.verTemas = function(objSede) {   
        shareVariables.setSedCod(objSede.sedCod);
        shareVariables.setSedEst(objSede.sedEst);
        shareVariables.setCapNom(objSede.curNom);
        shareVariables.setCapTip(objSede.curTip);
        shareVariables.setFecIni(objSede.curIni);
        shareVariables.setFecFin(objSede.curFin);
        shareVariables.setDocCap($scope.state);
        
        $location.url('/misCapacitaciones/' + objSede.sedCod);
    };
    
    $scope.registrarAsistencia = function(objSede) {
        var request = crud.crearRequest('cursos', 1, 'listarHorarios');
        request.setData({opt: 0, sedCod: objSede.sedCod});

        crud.listar('/curso_capacitacion', request, function (success) {
            if(success.data.length > 0) {
                $uibModal.open({
                    animation: true,
                    keyboard: true,
                    backdrop: true,
                    templateUrl: 'shift.html',
                    controller: 'shiftCtrl',
                    controllerAs: 'shift',
                    resolve:{
                        shift: function() {
                            return {
                                sede: angular.copy(objSede),
                                shifts: success.data
                            };
                        }
                    }
                });
            } else
                modal.mensaje("MENSAJE", "La sede no posee horarios el día hoy para esta hora");
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar los horarios de la sede " + error.responseMsg);
        });
    };  
    
    $scope.verAsistencia = function(objSede) {
        var request = crud.crearRequest('cursos', 1, 'listarHorarios');
        request.setData({opt: 1, sedCod: objSede.sedCod});

        crud.listar('/curso_capacitacion', request, function (success) {
            if(success.data.length > 0) {
                $uibModal.open({
                    animation: true,
                    keyboard: true,
                    backdrop: true,
                    templateUrl: 'attendance.html',
                    controller: 'attendanceCtrl',
                    controllerAs: 'attendance',
                    resolve:{
                        shift: function() {
                            return {
                                sede: angular.copy(objSede),
                                shifts: success.data
                            };
                        }
                    }
                });
            } else
                modal.mensaje("MENSAJE", "La sede no posee horarios el día hoy para esta hora");
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar los horarios de la sede " + error.responseMsg);
        });
    };  
    
    $scope.justificaciones = function(objSede) {
        var request = crud.crearRequest('cursos', 1, 'listarAsistenciaParticipante');
        request.setData({opt: 2, codSed: objSede.sedCod, url: $window.location.origin + urls.BASELOGIN});

        crud.listar('/curso_capacitacion', request, function (success) {
            if(success.data.length > 0) {
                $uibModal.open({
                    animation: true,
                    keyboard: true,
                    backdrop: true,
                    templateUrl: 'justification.html',
                    controller: 'justificationCtrl',
                    controllerAs: 'justification',
                    resolve:{
                        shift: function() {
                            return {
                                sede: angular.copy(objSede),
                                shifts: success.data
                            };
                        }
                    }
                });
            } else
                modal.mensaje("MENSAJE", "No se han registrado justificaciones");
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar las justificaciones " + error.responseMsg);
        });
    };
    
    $scope.misEvaluaciones = function(objSede) {
        $uibModal.open({
            animation: true,
            keyboard: true,
            backdrop: true,
            templateUrl: 'evaluation.html',
            controller: 'evaluationCtrl',
            controllerAs: 'evaluation',
            resolve:{
                location: function() {
                    return {
                        sedCod: objSede.sedCod,
                        sedEst: objSede.sedEst,
                        capNom: objSede.curNom
                    };
                }
            }
        });
    };
    
    $scope.editarAvance = function(objSede) {
        shareVariables.setSedCod(objSede.sedCod);
        shareVariables.setCapNom(objSede.curNom);
        
        $uibModal.open({
            animation: true,
            keyboard: true,
            backdrop: true,
            templateUrl: 'progress.html',
            controller: 'progressCtrl',
            
        });
    };
    
    $scope.evaluacionesCapacitacion = function(objSede) {
        var capNom = objSede.curNom;
        var sedNom = objSede.curNom;
        
        if(objSede.curTip === 'Presencial') {
            var nom = objSede.curNom.indexOf('SEDE');
            capNom = objSede.curNom.substring(0, nom - 1);
            sedNom = objSede.curNom.substring(nom + 6);
        }
        
        $rootScope.dataCap = {
            capNom: capNom,
            capTip: objSede.curTip,
            sedCod: objSede.sedCod,
            sedNom: sedNom,
            sedIni: objSede.curIni,
            sedFin: objSede.curFin,
            sedEst: objSede.sedEst
        };
        
        $location.url('/misCapacitaciones/evaluaciones/' + objSede.sedCod);
    };
}]);

mcApp.controller("temaCapacitacionCtrl", ["$rootScope", "$scope", "NgTableParams", "crud", "modal", "$routeParams", "shareVariables", "$uibModal", "$location", function ($rootScope, $scope, NgTableParams, crud, modal, $routeParams, shareVariables, $uibModal, $location) {
    var self = this;
    var settingTopics = {counts: []};
    self.tablaTemas = new NgTableParams({count: 10}, settingTopics);
    self.capNom = shareVariables.getCapNom();
    self.est = (shareVariables.getSedEst() !== 'F');
    self.estCur = shareVariables.getSedEst();
    self.docCap = shareVariables.getDocCap();
    
    self.verify = function() {
        if(shareVariables.getCapNom() === '')
            $location.url('/misCapacitaciones');
    };
    
    self.verify();
    
    self.registrarAsistencia = function() {
        if(!self.docCap && shareVariables.getCapTip() === 'Online') {
            var request = crud.crearRequest('cursos', 1, 'registrarAsistencia');
            request.setData({doc: $rootScope.usuMaster.usuario.usuarioID, sed: shareVariables.getSedCod()});

            crud.insertar('/curso_capacitacion', request, function (success) {
                if (success.response === 'OK') {
                    var objAt = success.data;

                    if(objAt.est) {
                        objAt.shifts.forEach(function(obj) {
                            if(obj.est) {
                                obj.states.forEach(function(objAt) {
                                    var time = new Date(objAt.time);
                                    modal.mensaje("MENSAJE", "Se ha registrado la asistencia a esta sede " + time.getHours() + ":" + time.getMinutes());
                                });
                            }                                
                        });
                    }                  
                } else if (success.response === 'BAD')
                    modal.mensaje("ERROR", success.responseMsg);
            }, function (error) {
                modal.mensaje("MENSAJE", "No se pudo registrar su asistencia " + error.responseMsg);
            });
        }
    };
    
    self.registrarAsistencia();
    
    self.listarTemas = function() {
        var request = crud.crearRequest('cursos', 1, 'listarTemas');
        request.setData({opt: 0, sedCod: $routeParams.codSed});

        crud.listar('/curso_capacitacion', request, function (success) {
            if(success.data.length > 0) {
                settingTopics.dataset = success.data;
                iniciarPosiciones(settingTopics.dataset);
                self.tablaTemas.settings(settingTopics);
            } else
                modal.mensaje("MENSAJE", "No existen temas para listar");
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar los temas de este curso de capacitación " + error.responseMsg);
        });
    };
    
    self.agregarTema = function() {
        var modalInstance = $uibModal.open({
            animation: true,
            keyboard: true,
            backdrop: true,
            templateUrl: 'topic.html',
            controller: 'topicCtrl',
            resolve:{
                sede: function() {
                    return self.sede = {
                        cod: shareVariables.getSedCod(),
                        ini: shareVariables.getFecIni(),
                        fin: shareVariables.getFecFin(),
                        est: true
                    };
                }
            }
        });
        
        modalInstance.result.then(function(tema) {
            var ini = new Date(tema.ini);
            var fin = new Date(tema.fin);
            tema.ini = ini.getDate() + '-' + (Number(ini.getMonth())+1) + '-' + ini.getFullYear() ;
            tema.fin = fin.getDate() + '-' + (Number(fin.getMonth())+1) + '-' + fin.getFullYear() ;
            
            if(self.tablaTemas.settings().total > 0) {
                self.tablaTemas.settings().dataset.push(tema);
                self.tablaTemas.reload();
            } else {
                settingTopics.dataset = [tema];
                iniciarPosiciones(settingTopics.dataset);
                self.tablaTemas.settings(settingTopics);
            }
        }, function(){});
    };
    
    self.editarTema = function(element) {
        var modalInstance = $uibModal.open({
            animation: true,
            keyboard: true,
            backdrop: true,
            templateUrl: 'topic.html',
            controller: 'topicCtrl',
            resolve:{
                sede: function() {
                    return self.sede = {
                        cod: shareVariables.getSedCod(),
                        ini: shareVariables.getFecIni(),
                        fin: shareVariables.getFecFin(),
                        top: angular.copy(element),
                        est: false
                    };
                }
            }
        });
        
        modalInstance.result.then(function(tema) {
            var ini = new Date(tema.ini);
            var fin = new Date(tema.fin);
            tema.ini = ini.getDate() + '-' + (Number(ini.getMonth())+1) + '-' + ini.getFullYear() ;
            tema.fin = fin.getDate() + '-' + (Number(fin.getMonth())+1) + '-' + fin.getFullYear() ;
            
            var original = self.tablaTemas.settings().dataset.filter(function(r) {
                return r.i === tema.i;
            })[0];
            
            angular.extend(original, tema);
        }, function(){});
    };
    
    self.eliminarTema = function(element) {
        var request = crud.crearRequest('cursos', 1, 'listarTemas');
        request.setData({opt: 2, temCod: element.cod});
                            
        crud.listar('/curso_capacitacion', request, function (success) {
            if (success.response === 'OK') {
                if(success.data.state)
                    modal.mensaje("CONFIRMACION", "El tema no puede ser eliminado debido a que el foro de docentes posee comentarios");
                else {
                    modal.mensajeConfirmacion($scope, "Se eliminarán las tareas y evaluaciones asociados. ¿Desea continuar?", function () {
                        var request = crud.crearRequest('cursos', 1, 'eliminarTemario');
                        request.setData({usuMod: $rootScope.usuMaster.usuario.usuarioID,
                                         temCod: element.cod
                                        });

                        crud.eliminar('/curso_capacitacion', request, function (success) {
                            if (success.response === 'OK') {
                                _.remove(self.tablaTemas.settings().dataset,function(item){
                                    return element === item;
                                });
                                self.tablaTemas.reload();
                                modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                            } else if (success.response === 'BAD')
                                modal.mensaje("ERROR", success.responseMsg);
                        }, function (error) {
                            modal.mensaje("MENSAJE", error.responseMsg);
                        });
                    }, '400');
                }
            } else if (success.response === 'BAD')
                modal.mensaje("ERROR", success.responseMsg);
        }, function (error) {
            modal.mensaje("MENSAJE", error.responseMsg);
        });
    }; 
    
    self.agregarContenido = function(element) {
        var modalInstance = $uibModal.open({
            animation: true,
            keyboard: true,
            backdrop: true,
            templateUrl: 'content.html',
            controller: 'contentCtrl',
            controllerAs: 'content',
            resolve: {
                tema: function() {
                    return self.tema = {
                        top: angular.copy(element),
                        sed: shareVariables.getSedCod()
                    };
                }
            }
        });
        
        modalInstance.result.then(function() {
            modal.mensajeConfirmacion($scope, "¿Desea enviar una notificación a los participantes?", function () {
                var request = crud.crearRequest('cursos', 1, 'enviarMensaje');
                request.setData({usuAut: $rootScope.usuMaster.usuario.ID,
                                 codSed: shareVariables.getSedCod(),
                                 cont: 'Se ha añadido nuevo contenido a la capacitación: ' + shareVariables.getCapNom()});
                    
                crud.insertar('/curso_capacitacion', request, function (success) {
                    if (success.response === 'OK') {
                        modal.mensaje("CONFIRMACION", "Los mensajes han sido enviados correctamente");
                    } else if (success.response === 'BAD')
                        modal.mensaje("ERROR", success.responseMsg);
                }, function (error) {
                    modal.mensaje("MENSAJE", error.responseMsg);
                });                
            }, '400');  
        }, function(){});
    };
    
    self.irForo = function(element) {
        shareVariables.setTemNom(element.nom);
        shareVariables.setTemCod(element.cod);
        
        $location.url('/misCapacitaciones/foro/' + element.cod);
    };
    
    self.verContenido = function(element) {
        shareVariables.setTemNom(element.nom);
        shareVariables.setTemCod(element.cod);
        
        $location.url('/misCapacitaciones/desarrollo/' + element.cod);
    };
}]);

mcApp.controller("foroTemaCtrl", ["$rootScope", "$scope", "crud", "modal", "shareVariables", "$uibModal", "$location", "urls", "$window", "$route", function ($rootScope, $scope, crud, modal, shareVariables, $uibModal, $location, urls, $window, $route) {
    var self = this;
    self.temNom = shareVariables.getTemNom();
    self.est = shareVariables.getSedEst();
    self.usu = $rootScope.usuMaster.usuario.usuarioID;    
    
    self.verify = function() {
        if(shareVariables.getTemNom() === '')
            $location.url('/misCapacitaciones');
    };
    
    self.verify();
        
    self.listarComentarios = function() {
        var request = crud.crearRequest('cursos', 1, 'listarComentarios');
        request.setData({temCod: shareVariables.getTemCod(),url: $window.location.origin + urls.BASELOGIN});

        crud.listar('/curso_capacitacion', request, function (success) {
            if(success.data.length > 0) {
                self.comentarios = success.data;
            } else
                modal.mensaje("MENSAJE", "Aún no existen comentarios de este tema");
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar los temas de este curso de capacitación " + error.responseMsg);
        });
    };
    
    self.agregarComentario = function() {
        var modalInstance = $uibModal.open({
            animation: true,
            keyboard: true,
            backdrop: true,
            templateUrl: 'comment.html',
            controller: 'commentCtrl',
            controllerAs: 'comment',
            resolve: {
                state: function() {
                    return {
                        est: 0
                    };
                }
            }
        });
        
        modalInstance.result.then(function() {
            $route.reload();
        }, function(){});
    };
    
    self.responder = function(element, event) {
        event.preventDefault();
        event.stopPropagation();
        
        var modalInstance = $uibModal.open({
            animation: true,
            keyboard: true,
            backdrop: true,
            templateUrl: 'comment.html',
            controller: 'commentCtrl',
            controllerAs: 'comment',
            resolve: {
                state: function() {
                    return {
                        est: 1,
                        com: element.com,
                        doc: element.usu
                    };
                }
            }
        });
        
        modalInstance.result.then(function() {
            $route.reload();
        }, function(){});
    };
    
    self.eliminar = function(element, event, type) {
        event.preventDefault();
        event.stopPropagation();
        var message;
        
        if(type) 
            message = "Se eliminarán los comentarios asociados (respuestas). ¿Desea continuar?";
        else
            message = "Se eliminará el comentario seleccionado. ¿Desea continuar?";
        
        modal.mensajeConfirmacion($scope, message, function () {
            var request = crud.crearRequest('cursos', 1, 'eliminarComentario');
            request.setData({usuMod: $rootScope.usuMaster.usuario.usuarioID,
                             cod: element.com
                            });
                    
            crud.eliminar('/curso_capacitacion', request, function (success) {
                if (success.response === 'OK') {                    
                    modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    $route.reload();
                } else if (success.response === 'BAD')
                    modal.mensaje("ERROR", success.responseMsg);
            }, function (error) {
                modal.mensaje("MENSAJE", error.responseMsg);
            });
        }, '400');   
    };
    
    self.editar = function(element, event) {
        event.preventDefault();
        event.stopPropagation();
        
        var modalInstance = $uibModal.open({
            animation: true,
            keyboard: true,
            backdrop: true,
            templateUrl: 'comment.html',
            controller: 'commentCtrl',
            controllerAs: 'comment',
            resolve: {
                state: function() {
                    return {
                        est: 2,
                        com: element.com,
                        doc: element.usu,
                        adj: angular.copy(element.adj),
                        men: angular.copy(element.tex)
                    };
                }
            }
        });
        
        modalInstance.result.then(function() {
            $route.reload();
        }, function(){});
    };
}]);

mcApp.controller('desarrolloTemaCtrl',['$rootScope', 'crud', 'modal', 'shareVariables', '$location', '$window', 'urls', '$scope', '$uibModal','$sce', function($rootScope, crud, modal, shareVariables, $location, $window, urls, $scope, $uibModal,$sce) {
    var self = this;
    self.temNom = shareVariables.getTemNom();
    self.state = shareVariables.getDocCap();
    self.currentProjectUrl = null;
    self.currentProjectUrl2 = null;
    var cont = 1;
    
    self.verify = function() {
        if(shareVariables.getTemNom() === '')
            $location.url('/misCapacitaciones');
    };
    
    self.verify();
    
    
    self.verAdjunto = function(adjunto , tipo){

        if(cont === 1){
             self.currentProjectUrl = $sce.trustAsResourceUrl(adjunto); 
             cont = 2;
        }
        else{
            self.currentProjectUrl2 = $sce.trustAsResourceUrl(adjunto);
            cont = 1;
        }  
           
            
       
    };
    
    self.listarDesarrollo = function() {
        var request = crud.crearRequest('cursos', 1, 'listarDesarrollo');
        request.setData({cod: shareVariables.getTemCod(), url: $window.location.origin + urls.BASELOGIN});

        crud.listar('/curso_capacitacion', request, function (success) {
            self.cont = success.data.cont;
            self.tare = success.data.tare;
            self.eval = success.data.eval; 
            self.vide = success.data.vide;
            self.i = 0;
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar los temas de este curso de capacitación " + error.responseMsg);
        });
    };
    
    
    
    $scope.$watch('desarrolloCtrl.i', function(newValue) {
        switch(newValue) {
            case 0: if(self.cont !== undefined && self.cont.length === 0)
                        modal.mensaje("MENSAJE", "No existe contenido teórico registrado para mostrar");
                    break;
                    
            case 1: if(self.tare !== undefined && self.tare.length === 0)
                        modal.mensaje("MENSAJE", "No existen tareas registradas para mostrar");
                    break;
                    
            case 2: if(self.eval !== undefined && self.eval.length === 0)
                        modal.mensaje("MENSAJE", "No existen evaluaciones registradas para mostrar");
                    break;
                    
            case 3: if(self.vide !== undefined && self.vide.length === 0)
                        modal.mensaje("MENSAJE", "No existen Videos para mostrar");
                    break;        
                    
        }        
    });
    
    self.revisarDesarrollo = function(element) {
        $uibModal.open({
            animation: true,
            keyboard: true,
            backdrop: true,
            templateUrl: 'development.html',
            controller: 'developmentCtrl',
            controllerAs: 'dev',
            resolve: {
                desarrollo: function() {
                    return {
                        cod: element.cod,
                        tem: shareVariables.getTemNom(),
                        nom: element.nom,
                        tip: element.tip
                    };
                }
            }
        });
    };
    
    self.enviarDesarrollo = function(element) {
        $uibModal.open({
            animation: true,
            keyboard: true,
            backdrop: true,
            templateUrl: 'sendDevelopment.html',
            controller: 'sendDevelopmentCtrl',
            controllerAs: 'send',
            resolve: {
                desarrollo: function() {
                    return {
                        cod: element.cod,
                        tem: shareVariables.getTemNom(),
                        nom: element.nom,
                        tip: element.tip
                    };
                }
            }
        });
    };
}]);

mcApp.controller('misEvaluacionesCtrl',['$rootScope', 'crud', 'modal', 'shareVariables', '$location', '$window', 'urls', '$scope', '$uibModal', function($rootScope, crud, modal, shareVariables, $location, $window, urls, $scope, $uibModal) {
    var self = this;
    
    
    
    
    self.verify = function() {
        if(angular.equals(shareVariables.getEva(), {}))
            $location.url('/misCapacitaciones');
        else {
            var evaluation = shareVariables.getEva();
            self.evaEst = evaluation.est;
            
            if(self.evaEst !== 'I') {
                modal.mensaje("MENSAJE", "La evaluacion o encuesta ya no se encuentra disponible");
                $location.url('/misCapacitaciones');
            } else {
                self.evaCod = evaluation.id;
                self.evaNom = evaluation.nom;
                self.evaObs = evaluation.obs;
                self.evaOrd = evaluation.ord;
                self.evaTip = evaluation.tip;
                self.time = evaluation.time;
                
            }           
        }
    };
    
    self.verify();  
    
    self.listarPreguntas = function() {
        var request = crud.crearRequest('cursos', 1, 'listarPreguntas');
        request.setData({evaCod: self.evaCod});

        crud.listar('/curso_capacitacion', request, function (success) {
            self.questions = (self.evaOrd)?self.shuffle(success.data):success.data;
            self.result = false;
            self.iniciarCronometro();
            
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar las preguntas de la evaluación " + error.responseMsg);
        });
    };
    
    self.iniciarCronometro = function(){
        
        self.time = self.time -1 ;
        
        self.cronometro = new Date(); 
        if(parseInt(self.time)>60){self.cronometro.setHours(parseInt(self.time)/60);self.cronometro.setMinutes(parseInt(self.time)%60);}
        else{
            self.cronometro.setHours(0);
            self.cronometro.setMinutes(parseInt(self.time));
        }
        self.cronometro.setSeconds(59);
        
        self.iniciarClock();
        
    }
    
    self.iniciarClock = function () {
            
            var context;

            function getClock()
            {
                if(typeof  clock=='undefined')
                {
                    clearInterval(reloj);
                    return;
                }
                
                //Get Current Time
                self.cronometro.setSeconds(self.cronometro.getSeconds() - 1);
                if(self.cronometro.getHours()=== 0 && self.cronometro.getMinutes()=== 0 && self.cronometro.getSeconds()=== 0){  
                    self.calificar();
                }
                str = prefixZero(self.cronometro.getHours(), self.cronometro.getMinutes(), self.cronometro.getSeconds());
                //Get the Context 2D or 3D
                context = clock.getContext("2d");
                context.clearRect(0, 0, 500, 200);
                context.font = "80px Arial";
                context.fillStyle = "#000";
                context.fillText(str, 42, 125);
            }

            function prefixZero(hour, min, sec)
            {
                var curTime;
                if (hour < 10)
                    curTime = "0" + hour.toString();
                else
                    curTime = hour.toString();

                if (min < 10)
                    curTime += ":0" + min.toString();
                else
                    curTime += ":" + min.toString();

                if (sec < 10)
                    curTime += ":0" + sec.toString();
                else
                    curTime += ":" + sec.toString();
                return curTime;
            }

            var reloj=setInterval(getClock, 1000);
        };
    
    
    
    
    
    self.cronometro = function(){
        if (self.centesimas < 99) 
        {
		self.centesimas++;
		if (self.centesimas < 10) 
                {
                    self.centesimas = "0"+self.centesimas ;
                }
		
	}
	if (self.centesimas == 99) 
        {
		self.centesimas = -1;
	}
	if (self.centesimas == 0) 
        {
		self.segundos ++;
		if (self.segundos < 10) { self.segundos = "0"+self.segundos; }	
	}
	if (segundos == 59) {
		self.segundos = -1;
	}
	if ( (self.centesimas == 0)&&(self.segundos == 0) ) {
		self.minutos++;
		if (self.minutos < 10) { self.minutos = "0"+self.minutos }	
	}
	if (self.minutos == 59) {
		self.minutos = -1;
	}
    }
    

    self.cronometro2 = function(){
         if(self.minutos === 0){
            modal.mensajeConfirmacion("El tiempo del examen ha finalizado");
            self.calificar();
        }
        else{
            if(self.milisegundos === 0){
                self.segundos--;
                self.milisegundos = 59;
                if(self.segundos < 10){
                    self.segundos = "0"+self.segundos;
                }
                self.milisegundos = 59;
                if(self.segundos === 0){
                    self.minutos--;
                    self.segundos = 59;
                }
            }else{
                self.milisegundos--;
            }
        }
    };
    
    
    
    
    
    
    self.shuffle = function(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;

        while (0 !== currentIndex) {
          randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex -= 1;

        temporaryValue = array[currentIndex];
          array[currentIndex] = array[randomIndex];
          array[randomIndex] = temporaryValue;
        }

        return array;
    };
    
    self.calificar = function() {
        if(self.evaTip === 'P')
            self.calificarEvaluacion();
        else
            self.calificarEncuesta();
    };
    
    self.calificarEvaluacion = function() {
        self.startScore = 0;
        self.finalScore = 0;
        
        self.questions.forEach(function(question) {
            switch(question.tip) {
                case 'M':
                    var point = question.pun/question.res.length;
                    var score = 0;
                    
                    question.res.forEach(function(option) {
                        if(option.resUsu !== undefined) {
                            if(option.sel === option.resUsu)
                                score += point;
                            else
                                score += 0;
                        } else if(option.sel === false) {
                            score += point;
                        } else
                            score += 0;
                    });
                    
                    question.punUsu = parseFloat((score).toFixed(2));
                    break;
                    
                case 'S':
                    if(question.resUsu !== undefined) {
                        if(question.resUsu === question.res)
                            question.punUsu = question.pun;                            
                        else
                            question.punUsu = 0;
                    } else
                        question.punUsu = 0;
                    
                    break;    
                
                case 'L':
                    if(question.resUsu !== 'NN') {
                        if(question.resUsu === question.res)
                            question.punUsu = question.pun;                            
                        else
                            question.punUsu = 0;
                    } else
                        question.punUsu = 0;
                    
                    break;  
            }
            
            self.startScore += question.pun;
            self.finalScore += question.punUsu;
        });
        
        var request = crud.crearRequest('cursos', 1, 'editarEvaluacionParticipante');
        request.setData({not: self.finalScore, codEva: self.evaCod, codDoc: $rootScope.usuMaster.usuario.usuarioID});

        crud.actualizar('/curso_capacitacion', request, function (success) {
            if (success.response === 'OK') {
                self.startScore = parseFloat((self.startScore).toFixed(2));
                self.finalScore = parseFloat((self.finalScore).toFixed(2));
                self.perScore = parseFloat((self.finalScore/self.startScore*100).toFixed(2));
                self.result = true;
            } else if (success.response === 'BAD')
                modal.mensaje("ERROR", success.responseMsg);
        }, function (error) {
            modal.mensaje("MENSAJE", error.responseMsg);
        });
    };
    
    self.calificarEncuesta = function() {
        var state_Q = true;
        var i = 0;
        
        for (; i < self.questions.length && state_Q; i++) {
            switch(self.questions[i].tip) {
                case 'M':      
                    var state_O = true;
                    
                    for(var j = 0;j < self.questions[i].res.length && state_O; j++) {
                        if(self.questions[i].res[j].resUsu) {
                            state_O = false;
                            break;
                        }
                    }
                    
                    if(state_O)
                        state_Q = false;
                    
                    break;  
                
                case 'S':
                    if(self.questions[i].resUsu === undefined || self.questions[i].resUsu === '')
                        state_Q = false;
                    break;   
                
                case 'L':
                    if(self.questions[i].resUsu === 'NN')
                        state_Q = false;
                    break;
            }
        }
        
        if(!state_Q)
            modal.mensaje("ERROR", "La pregunta " + i + " no se ha respondido.");
        else {
            var request = crud.crearRequest('cursos', 1, 'registrarRespuestaEncuesta');
            request.setData({codEva: self.evaCod,
                             codDoc: $rootScope.usuMaster.usuario.usuarioID,
                             questions: self.questions});

            crud.insertar('/curso_capacitacion', request, function (success) {
                if (success.response === 'OK') {
                    modal.mensaje("CONFIRMACION", "El registro se realizó correctamente");
                    $location.url('/misCapacitaciones');
                } else if (success.response === 'BAD')
                    modal.mensaje("ERROR", success.responseMsg);
            }, function (error) {
                modal.mensaje("MENSAJE", error.responseMsg);
            });
        }        
    };
}]);

mcApp.controller('topicCtrl',['$uibModalInstance', 'sede', '$rootScope', 'crud', 'modal', '$scope', function($uibModalInstance, sede, $rootScope, crud, modal, $scope){
    var ini = new Date(sede.ini);
    ini.setDate(ini.getDate() + 1);
    var fin = new Date(sede.fin);
    fin.setDate(fin.getDate() + 1);

    $scope.dateOptions = {
        maxDate: fin,
        minDate: ini,
        initDate: ini,
        startingDay: 1
    };

    if(sede.est) {
        $scope.title = "Nuevo Tema";
    } else {
        $scope.title = "Edición Tema";
        $scope.tema = sede.top;
        
        var parts = sede.top.ini.split('-');
        $scope.tema.ini = new Date(parts[2],parts[1]-1,parts[0]); 
        parts = sede.top.fin.split('-');
        $scope.tema.fin = new Date(parts[2],parts[1]-1,parts[0]);
    }
    
    $scope.aceptar = function() {
        if($scope.tema.fin.getTime() >= $scope.tema.ini.getTime()) {
            if (sede.est) {
                var request = crud.crearRequest('cursos', 1, 'registrarTemario');
                request.setData({usuMod: $rootScope.usuMaster.usuario.usuarioID,
                                contenidos: [{
                                    sed: sede.cod,
                                    ini: $scope.tema.ini,
                                    fin: $scope.tema.fin,
                                    tem: $scope.tema.nom
                                }]});
                    
                crud.insertar('/curso_capacitacion', request, function (success) {
                    if (success.response === 'OK') {
                        modal.mensaje("CONFIRMACION", "El registro se realizó correctamente");
                        $uibModalInstance.close(success.data[0]);
                    } else if (success.response === 'BAD')
                        modal.mensaje("ERROR", success.responseMsg);
                }, function (error) {
                    modal.mensaje("MENSAJE", error.responseMsg);
                    $uibModalInstance.dismiss('cancel');
                });
            } else {
                var request = crud.crearRequest('cursos', 1, 'editarTemario');
                request.setData({opt: 0, 
                                 usuMod: $rootScope.usuMaster.usuario.usuarioID,
                                 tema: $scope.tema});

                crud.actualizar('/curso_capacitacion', request, function (success) {
                    if (success.response === 'OK') {
                        modal.mensaje("CONFIRMACION", "La modificación se realizó correctamente");
                        $uibModalInstance.close($scope.tema);
                    } else if (success.response === 'BAD')
                        modal.mensaje("ERROR", success.responseMsg);
                }, function (error) {
                    modal.mensaje("MENSAJE", error.responseMsg);
                    $uibModalInstance.dismiss('cancel');
                });
            }
        } else
            modal.mensaje("ERROR", "La fecha de inicio debe ser mayor o igual a la fecha de fin");
    };
    
    $scope.cancelar = function(){
        $uibModalInstance.dismiss('cancel');
    };
}]);

mcApp.controller('contentCtrl',['$uibModalInstance', 'tema', '$rootScope', 'crud', 'modal', function($uibModalInstance, tema, $rootScope, crud, modal){
    var self = this;
    self.tipos = [{id: 'C', nom: 'Contenido Teórico'},{id: 'E', nom: 'Evaluación'},{id: 'T', nom: 'Tarea'},{id: 'V', nom:'Video'}];
    var parts = tema.top.fin.split('-');
    self.contenido = new Object();
    self.contenido.adjuntos = [];
    //Para el caso de Incluir Links
    self.contenido.links = [];
    
    self.dateOptions = {
        maxDate: new Date(parts[2],parts[1]-1,parts[0]),
        minDate: new Date(),
        initDate: new Date(),
        startingDay: 1
    };
    
    self.agregarAdjunto = function() {
        self.contenido.adjuntos.push(angular.copy(self.doc));

        self.doc.nom = '';
        self.doc.arc = '';
    };
    
    self.eliminarAdjunto = function(index) {
        self.contenido.adjuntos.splice(index,1);        
    };
    
    /*Agregar-Eliminar Links*/
    self.agregarLink = function(){
        self.contenido.links.push(angular.copy(self.link));
        
        self.link.nom = '-';
        
    }
    
    self.eliminarLink = function(index){
         self.contenido.links.splice(index,1);
    }
    
    
    self.aceptar = function() {
        var request = crud.crearRequest('cursos', 1, 'registrarDesarrollo');
        request.setData({tem: tema.top.cod, sed: tema.sed, cont: self.contenido, usuMod: $rootScope.usuMaster.usuario.usuarioID});

        crud.insertar('/curso_capacitacion', request, function (success) {
            if (success.response === 'OK') {
                
                modal.mensaje("CONFIRMACION", "El registro se realizó correctamente");
                $uibModalInstance.close(success.data[0]);
            } else if (success.response === 'BAD')
                modal.mensaje("ERROR", success.responseMsg);
        }, function (error) {
            modal.mensaje("MENSAJE", error.responseMsg);
            $uibModalInstance.dismiss('cancel');
        });
    };
    
    self.cancelar = function(){
        $uibModalInstance.dismiss('cancel');
    };
    
    
    
}]);

mcApp.controller('progressCtrl',['$uibModalInstance', 'crud', 'modal', 'NgTableParams', 'shareVariables', '$scope', '$rootScope',  function($uibModalInstance, crud, modal, NgTableParams, shareVariables, $scope, $rootScope){
    var settingTopics = {counts: []};
    $scope.tablaTemas = new NgTableParams({count: 8}, settingTopics);
    $scope.nombre = shareVariables.getCapNom();
    $scope.cods = [];
    
    $scope.listarTemas = function() {
        var request = crud.crearRequest('cursos', 1, 'listarTemas');
        request.setData({opt: 1, sedCod: shareVariables.getSedCod()});

        crud.listar('/curso_capacitacion', request, function (success) {
            if(success.data.temas.length > 0) {
                settingTopics.dataset = success.data.temas;
                iniciarPosiciones(settingTopics.dataset);
                $scope.tablaTemas.settings(settingTopics);
                $scope.value = Math.round(success.data.avn);               
                $scope.factor = (100-$scope.value)/(success.data.res);
            } else {
                modal.mensaje("MENSAJE", "No existen temas para listar");
                $scope.value = 0;
            }
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar los temas de este curso de capacitación " + error.responseMsg);
        });
    };
    $scope.listarTemas();
    
    $scope.seleccionar = function(objTem) {
        if(objTem.sel) {
            $scope.value -= $scope.factor;
            $scope.value = parseFloat($scope.value.toFixed(2));
            objTem.sel = false;
            
            $scope.cods = $.grep($scope.cods, function(obj){ 
                return obj !== objTem.cod; 
            });
        } else {
            $scope.value += $scope.factor;
            $scope.value = parseFloat($scope.value.toFixed(2));
            objTem.sel = true;
            $scope.cods.push(objTem.cod);
        }
    };
    
    $scope.$watch('value', function() {
        if ($scope.value < 25) {
            $scope.type = 'danger';
        } else if ($scope.value < 50) {
            $scope.type = 'warning';
        } else if ($scope.value < 75) {
            $scope.type = 'info';
        } else {
            $scope.type = 'success';
            if($scope.value >= 99)
                $scope.value = Math.round($scope.value);
        }
    });
    
    $scope.$watchCollection('cods', function() {
        if($scope.cods.length > 0)
            $scope.state = false;
        else
            $scope.state = true;
    });
    
    $scope.aceptar = function() {
        var request = crud.crearRequest('cursos', 1, 'editarTemario');
        request.setData({opt: 1, cods: $scope.cods, usuMod: $rootScope.usuMaster.usuario.usuarioID,});

        crud.actualizar('/curso_capacitacion', request, function (success) {
            if (success.response === 'OK') {
                modal.mensaje("CONFIRMACION", "El avance ha sido actualizado");
                $uibModalInstance.close();
            } else if (success.response === 'BAD')
                modal.mensaje("ERROR", success.responseMsg);
        }, function (error) {
            modal.mensaje("MENSAJE", error.responseMsg);
            $uibModalInstance.dismiss('cancel');
        });
    };
    
    $scope.cancelar = function() {
        $uibModalInstance.dismiss('cancel');
    };
}]);

mcApp.controller('commentCtrl',['$uibModalInstance', '$rootScope', 'crud', 'modal', 'shareVariables', 'state', function($uibModalInstance, $rootScope, crud, modal, shareVariables, state) {
    var self = this;
    self.state = state;
    self.comentario = new Object();
    self.comentario.adjuntos = [];
    var deleteAttachments = [];
    var newAttachments = [];
    
    if(self.state.est === 2) {
        self.comentario.com = self.state.men;        
        self.comentario.adjuntos = self.state.adj;
    }
    
    self.agregarAdjunto = function() {
        if(self.state.est === 2)
            newAttachments.push(angular.copy(self.doc));
        
        self.comentario.adjuntos.push(angular.copy(self.doc));

        self.doc.nom = '';
        self.doc.arc = '';
    };
    
    self.eliminarAdjunto = function(index) {
        if(self.state.est === 2 && self.comentario.adjuntos[index].cod !== undefined) {
            var i = self.comentario.adjuntos[index].url.lastIndexOf("/");
            deleteAttachments.push({cod: self.comentario.adjuntos[index].cod, nom: self.comentario.adjuntos[index].url.substring(i + 1)});
        } else if(self.state.est === 2) {
            var i = newAttachments.indexOf(self.comentario.adjuntos[index]);
            newAttachments.splice(i, 1); 
        }
        
        self.comentario.adjuntos.splice(index, 1); 
    };
    
    self.aceptar = function() {
        if(self.state.est === 2) {
            var request = crud.crearRequest('cursos', 1, 'editarComentario');
            request.setData({
                cod: self.state.com,
                com: self.comentario.com, 
                del: deleteAttachments,
                new: newAttachments,
                usuMod: $rootScope.usuMaster.usuario.usuarioID
                });
            crud.actualizar('/curso_capacitacion', request, function (success) {
                if (success.response === 'OK') {
                    modal.mensaje("CONFIRMACION", "El registro se actualizó correctamente");
                    $uibModalInstance.close();
                } else if (success.response === 'BAD')
                    modal.mensaje("ERROR", success.responseMsg);
            }, function (error) {
                modal.mensaje("MENSAJE", error.responseMsg);
                $uibModalInstance.dismiss('cancel');
            });    
        } else {
            var request = crud.crearRequest('cursos', 1, 'registrarComentario');
            request.setData({
                com: self.comentario, 
                sed: shareVariables.getSedCod(), 
                tem: shareVariables.getTemCod(), 
                doc: $rootScope.usuMaster.usuario.usuarioID,
                ses: $rootScope.usuMaster.usuario.ID,
                est: self.state.est,
                res: self.state.com,
                rep: self.state.doc,
                cap: shareVariables.getCapNom()});

            crud.insertar('/curso_capacitacion', request, function (success) {
                if (success.response === 'OK') {
                    modal.mensaje("CONFIRMACION", "El registro se realizó correctamente");
                    $uibModalInstance.close();
                } else if (success.response === 'BAD')
                    modal.mensaje("ERROR", success.responseMsg);
            }, function (error) {
                modal.mensaje("MENSAJE", error.responseMsg);
                $uibModalInstance.dismiss('cancel');
            });
        }
    };
    
    self.cancelar = function(){
        $uibModalInstance.dismiss('cancel');
    };
}]);

mcApp.controller('developmentCtrl',['$uibModalInstance', 'crud', 'modal', 'desarrollo', 'NgTableParams', '$window', 'urls', function($uibModalInstance, crud, modal, desarrollo, NgTableParams, $window, urls) {
    var self = this;
    self.tem = desarrollo.tem;
    self.nom = desarrollo.nom;
    self.tip = (desarrollo.tip === 'T')?"Tarea":"Evaluación";
    
    var settingCompetitor = {counts: []};
    self.tableCompetitor = new NgTableParams({count: 10}, settingCompetitor);
    
    self.listarParticipantes = function() {        
        var request = crud.crearRequest('cursos', 1, 'listarEvaluacionesDesarrollo');
        request.setData({opt: 0, desCod: desarrollo.cod, url: $window.location.origin + urls.BASELOGIN});

        crud.listar('/curso_capacitacion', request, function (success) {
            settingCompetitor.dataset = success.data;
            iniciarPosiciones(settingCompetitor.dataset);
            self.tableCompetitor.settings(settingCompetitor);
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar los temas de este curso de capacitación " + error.responseMsg);
        });  
    };
    
    self.salir = function(){
        $uibModalInstance.dismiss('cancel');
    };
    
    self.editar = function(row) {
        self.row = angular.copy(row);
        row.isEditing = true;
    };
    
    self.buscar = function(row, rowForm){
        row.isEditing = false;
        rowForm.$setPristine();
        return settingCompetitor.dataset.filter(function(r) {
            return r.i === row.i;
        })[0];
    };
    
    self.cancelar = function(row, rowForm) {
        var originalRow = self.buscar(row, rowForm);
        angular.extend(row, originalRow);
    };
    
    self.guardar = function(row, rowForm) {
        var originalRow = self.buscar(row, rowForm);
        
        var request = crud.crearRequest('cursos', 1, 'editarEvaluacionDesarrollo');
        request.setData({des: row.des, doc: row.doc, est: row.state, not: row.grade});

        crud.actualizar('/curso_capacitacion', request, function (success) {
            if (success.response === 'OK') {
                if(success.data.state) {
                    row.state = 'C';
                    angular.extend(originalRow, row);
                } else {
                    angular.extend(originalRow, self.row);
                    modal.mensaje("MENSAJE", "El docente no ha enviado su '" + self.tip + "', por lo tanto aun no se puede calificar");
                }
            } else if (success.response === 'BAD')
                modal.mensaje("ERROR", success.responseMsg);
        }, function (error) {
            modal.mensaje("MENSAJE", error.responseMsg);
            $uibModalInstance.dismiss('cancel');
        });
    };
}]);

mcApp.controller('sendDevelopmentCtrl',['$uibModalInstance', 'crud', 'modal', 'desarrollo', '$rootScope', '$window', 'urls', function($uibModalInstance, crud, modal, desarrollo, $rootScope, $window, urls) {
    var self = this;
    self.tem = desarrollo.tem;
    self.nom = desarrollo.nom;
    self.tip = (desarrollo.tip === 'T')?"Tarea":"Evaluación";;
    self.adjuntos = [];
    
    self.consultar = function() {
        var request = crud.crearRequest('cursos', 1, 'listarEvaluacionesDesarrollo');
        request.setData({opt: 1,
                        des: desarrollo.cod,
                        doc: $rootScope.usuMaster.usuario.usuarioID,
                        url: $window.location.origin + urls.BASELOGIN});

        crud.listar('/curso_capacitacion', request, function (success) { 
            self.est = success.data.est;
            self.not = success.data.not;
            
            switch(success.data.est) {
                case 'A':   self.disabled = false;                            
                            self.state = "Activa"; 
                            break;
                            
                case 'E':   self.disabled = true;
                            self.adjuntos = success.data.adj;
                            self.state = "Enviada"; 
                            break;
                            
                case 'C':   self.disabled = true;
                            self.adjuntos = success.data.adj;
                            self.state = "Calificada"; 
                            break;
                            
                case 'N':   self.disabled = true;
                            self.state = "No Enviada"; 
                            break;
            }
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar los cursos de capacitación " + error.responseMsg);
        });
    };
    
    self.agregarAdjunto = function() {
        self.adjuntos.push(angular.copy(self.doc));

        self.doc.nom = '';
        self.doc.arc = '';
    };
    
    self.eliminarAdjunto = function(index) {
        self.adjuntos.splice(index,1);        
    };
    
    self.aceptar = function() {
        var request = crud.crearRequest('cursos', 1, 'registrarEvaluacionDesarrollo');
        request.setData({des: desarrollo.cod, doc: $rootScope.usuMaster.usuario.usuarioID, adj: self.adjuntos});

        crud.insertar('/curso_capacitacion', request, function (success) {
            if (success.response === 'OK') {
                modal.mensaje("CONFIRMACION", "Se envió su tarea correctamente");
                $uibModalInstance.close();
            } else if (success.response === 'BAD')
                modal.mensaje("ERROR", success.responseMsg);
        }, function (error) {
            modal.mensaje("MENSAJE", error.responseMsg);
            $uibModalInstance.dismiss('cancel');
        });
    };
    
    self.cancelar = function(){
        $uibModalInstance.dismiss('cancel');
    };
}]);

mcApp.controller('evaluationCtrl',['$uibModalInstance', 'crud', 'modal', 'NgTableParams', 'shareVariables', '$rootScope', 'location', '$location', '$scope', function($uibModalInstance, crud, modal, NgTableParams, shareVariables, $rootScope, location, $location, $scope){
    var self = this;
    
    self.sedCod = location.sedCod;
    self.sedEst = location.sedEst;

    var index = location.capNom.indexOf('SEDE:');
    if(index !== -1) {
        self.capNom = location.capNom.substring(0, index - 1);
        self.tipCap = 'P';
        self.sedNom = location.capNom.substring(index + 6);
    } else {
        self.capNom = location.capNom;
        self.tipCap = 'O';
    }
        
    var settingTests = {counts: []};
    self.tablaEvaluaciones = new NgTableParams({count: 8}, settingTests);
        
    self.listarEvaluaciones = function() {
        var request = crud.crearRequest('cursos', 1, 'listarEvaluaciones');
        request.setData({opt: 3, sedCod: self.sedCod, codDoc: $rootScope.usuMaster.usuario.usuarioID});

        crud.listar('/curso_capacitacion', request, function (success) {
            if (success.response === 'OK') {
                if(success.data.length > 0) {
                    settingTests.dataset = success.data;
                    iniciarPosiciones(settingTests.dataset);
                    self.tablaEvaluaciones.settings(settingTests);
                } else
                    modal.mensaje("MENSAJE", "No existen evaluaciones y/o encuestas para listar");
            } else if (success.response === 'BAD')
                modal.mensaje("ERROR", success.responseMsg);
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar los temas de este curso de capacitación " + error.responseMsg);
        });
    };
    
    self.rendirEvaluacion = function(test) {
        if(test.estUlt === 'C') {
            modal.mensajeConfirmacion($scope, "Se generará una nueva evaluación y sólo se considerará la nota que obtenga en esta nueva. ¿Desea continuar?", function () {
                var request = crud.crearRequest('cursos', 1, 'registrarEvaluacionParticipante');
                request.setData({opt: 0, codEva: test.id, codDoc: $rootScope.usuMaster.usuario.usuarioID});

                crud.insertar('/curso_capacitacion', request, function (success) {
                    if (success.response === 'OK') {
                        shareVariables.setEva(test);
                        $location.url('/misCapacitaciones/evaluacion/' + test.id);
                        self.cerrar();
                    } else if (success.response === 'BAD')
                        modal.mensaje("ERROR", success.responseMsg);
                }, function (error) {
                    modal.mensaje("MENSAJE", error.responseMsg);
                });                
            }, '400');
        } else {
            shareVariables.setEva(test);
            $location.url('/misCapacitaciones/evaluacion/' + test.id);
            self.cerrar();
        }
    };
    
    self.cerrar = function(){
        $uibModalInstance.dismiss('cancel');
    };
}]);

mcApp.controller('shiftCtrl',['$uibModalInstance', 'crud', 'modal', 'NgTableParams', 'shift', '$rootScope', function($uibModalInstance, crud, modal, NgTableParams, shift, $rootScope){
    var self = this;
    self.name = shift.sede.curNom;
    self.horarios = shift.shifts;
    
    var settingShifts = {counts: []};
    self.tableShifts = new NgTableParams({count: 10}, settingShifts);
    
    self.cambiarHorario = function() {
        self.state = false;
        var request = crud.crearRequest('cursos', 1, 'listarAsistenciaParticipante');
        request.setData({opt: 0, horCod: self.codHor});

        crud.listar('/curso_capacitacion', request, function (success) {
            if (success.response === 'OK') {
                if(success.data.length > 0) {
                    settingShifts.dataset = success.data;
                    iniciarPosiciones(settingShifts.dataset);
                    self.tableShifts.settings(settingShifts);
                    self.state = true;
                } else
                    modal.mensaje("MENSAJE", "No existen participantes para listar");
            } else if (success.response === 'BAD')
                modal.mensaje("ERROR", success.responseMsg);
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar los docentes " + error.responseMsg);
        });
    };
    
    if(shift.shifts.length === 1) {        
        self.codHor = String(shift.shifts[0].horCod);
        self.cambiarHorario();
    } else
        self.codHor = '';
    
    self.cambiarEstado = function(row) {
        switch(row.est) {
            case 'A':   row.est = 'T'; break;
            case 'T':   row.est = 'F'; break;
            case 'F':   row.est = 'A'; break;
        }
    };
    
    self.getState = function(state) {
        switch(state) {
            case 'G': return "No Disponible";
            case 'A': return "Presente";
            case 'T': return "Tarde";
            case 'F': return "Falta";
            case 'J': return "Justificada";
            case 'P': return "Pendiente";
        }
    };
    
    self.aceptar = function() {
        var request = crud.crearRequest('cursos', 1, 'editarAsistenciaParticipante');
        request.setData({opt: 0, usuMod: $rootScope.usuMaster.usuario.usuarioID, attendance: self.tableShifts.settings().dataset});

        crud.actualizar('/curso_capacitacion', request, function (success) {
            if (success.response === 'OK') {
                modal.mensaje("CONFIRMACION", "Los registros han sido modificados correctamente");
            } else if (success.response === 'BAD')
                modal.mensaje("ERROR", success.responseMsg);
        }, function (error) {
            modal.mensaje("MENSAJE", error.responseMsg);
        });     
    };
    
    self.cerrar = function(){
        $uibModalInstance.dismiss('cancel');
    };
}]);

mcApp.controller('attendanceCtrl',['$uibModalInstance', 'crud', 'modal', 'NgTableParams', '$rootScope', 'shift', '$window', 'urls', function($uibModalInstance, crud, modal, NgTableParams, $rootScope, shift, $window, urls){
    var self = this;
    self.name = shift.sede.curNom;
    self.horarios = shift.shifts;
    
    var settingAttendances = {counts: []};
    self.tableAttendances = new NgTableParams({count: 10}, settingAttendances);
    
    self.cambiarHorario = function() {
        self.state = false;
        var request = crud.crearRequest('cursos', 1, 'listarAsistenciaParticipante');
        request.setData({opt: 1, 
                         codDoc: $rootScope.usuMaster.usuario.usuarioID, 
                         codHor: self.codHor, 
                         url: $window.location.origin + urls.BASELOGIN});

        crud.listar('/curso_capacitacion', request, function (success) {
            if(success.data.length > 0) {
                settingAttendances.dataset = success.data;
                iniciarPosiciones(settingAttendances.dataset);
                self.tableAttendances.settings(settingAttendances);
                self.state = true;
            } else
                modal.mensaje("MENSAJE", "La sede no posee horarios el día hoy para esta hora");
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar los horarios de la sede " + error.responseMsg);
        });
    };
        
    if(shift.shifts.length === 1) {        
        self.codHor = String(shift.shifts[0].horCod);
        self.cambiarHorario();
    } else
        self.codHor = '';
    
    self.getState = function(state) {
        switch(state) {
            case 'G': return "No Disponible";
            case 'A': return "Presente";
            case 'T': return "Tarde";
            case 'F': return "Falta";
            case 'J': return "Justificada";
            case 'P': return "Pendiente";
        }
    };
    
    self.justificarInasistencia = function(row) {
        self.watch = true;
        var shift = self.horarios.filter(function(obj) {
            return obj.horCod == self.codHor;
        })[0].des;
        
        var index = shift.indexOf(" ");
        var pieces = row.fec.split("-");
        var month = self.getMonth(pieces[1]);
        
        self.just = {
            tim: shift.substring(0, index) + " " + pieces[2] + " de " + month + " del " + pieces[0],
            shift: shift.substring(index+1),
            est: (row.est === 'F'),
            i: row.i,
            cod: row.cod,
            adj: row.adj
        };
    };
    
    self.agregarAdjunto = function() {
        self.just.adj.push(angular.copy(self.doc));

        self.doc.nom = '';
        self.doc.arc = '';
    };
    
    self.eliminarAdjunto = function(index) {
        self.just.adj.splice(index, 1);        
    };
    
    self.justificar = function() {
        var request = crud.crearRequest('cursos', 1, 'registrarJustificacion');
        request.setData({asi: self.just.cod, 
                         adj: self.just.adj, 
                         usuCod: $rootScope.usuMaster.usuario.usuarioID,
                         url: $window.location.origin + urls.BASELOGIN});

        crud.insertar('/curso_capacitacion', request, function (success) {
            if (success.response === 'OK') {                
                var original = self.tableAttendances.settings().dataset.filter(function(r) {
                    return r.i === self.just.i;
                })[0];
                
                original.est = "P";
                original.adj = success.data;
                self.watch = false;
            } else if (success.response === 'BAD')
                modal.mensaje("ERROR", success.responseMsg);
        }, function (error) {
            modal.mensaje("MENSAJE", error.responseMsg);
            $uibModalInstance.dismiss('cancel');
        });
    };
    
    self.getMonth = function(month) {
        switch(month) {
            case "01": return "Enero";
            case "02": return "Febrero";
            case "03": return "Marzo";
            case "04": return "Abril";
            case "05": return "Mayo";
            case "06": return "Junio";
            case "07": return "Julio";
            case "08": return "Agosto";
            case "09": return "Septiembre";
            case "10": return "Octubre";
            case "11": return "Noviembre";
            case "12": return "Diciembre";
        }
    }
    
    self.cerrar = function(){
        $uibModalInstance.dismiss('cancel');
    };
}]);

mcApp.controller('justificationCtrl',['$uibModalInstance', 'crud', 'modal', 'NgTableParams', 'shift', '$rootScope', function($uibModalInstance, crud, modal, NgTableParams, shift, $rootScope){
    var self = this;
    self.name = shift.sede.curNom;
    
    var settingJusts = {counts: []};
    self.tableJusts = new NgTableParams({count: 10}, settingJusts);
    
    self.listarJustificaciones = function() {
        settingJusts.dataset = shift.shifts;
        iniciarPosiciones(settingJusts.dataset);
        self.tableJusts.settings(settingJusts);
    };
        
    self.justificar = function(row) {
        var request = crud.crearRequest('cursos', 1, 'editarAsistenciaParticipante');
        request.setData({opt: 1, usuMod: $rootScope.usuMaster.usuario.usuarioID, codAsi: row.cod});

        crud.actualizar('/curso_capacitacion', request, function (success) {
            if (success.response === 'OK') {
                _.remove(self.tableJusts.settings().dataset,function(item){
                        return row === item;
                    });
                self.tableJusts.reload();
                modal.mensaje("CONFIRMACION", "El registro ha sido modificado correctamente");
            } else if (success.response === 'BAD')
                modal.mensaje("ERROR", success.responseMsg);
        }, function (error) {
            modal.mensaje("MENSAJE", error.responseMsg);
        });     
    };
    
    self.cerrar = function(){
        $uibModalInstance.dismiss('cancel');
    };
}]);
