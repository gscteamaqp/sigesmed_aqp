var mcApp = angular.module('app');
mcApp.config(['$routeProvider',function($routeProvider){
    $routeProvider.when('/evaluaciones/:codSed/:codEva',{
            templateUrl:'pedagogico/capacitacion/preguntaEvaluacion.html',
            controller:'preguntaEvaluacionCtrl',
            controllerAs:'preguntaCtrl'
        }).when('/evaluaciones/bancoPreguntas',{
            templateUrl:'pedagogico/capacitacion/bancoPreguntas.html',
            controller:'bancoPreguntasCtrl',
            controllerAs:'bancoCtrl'
        });
}]);

mcApp.factory('variablesTest', function () {
    var data = {};
    
    return {
        getData: function () {
            return data;
        },
        setData: function (obj) {
            data = obj;
        }
    };
});

app.controller("evaluacionCapacitacionCtrl", ["$rootScope", "$scope", "NgTableParams", "crud", "modal", "$location", '$uibModal', 'variablesTest', function ($rootScope, $scope, NgTableParams, crud, modal, $location, $uibModal, variablesTest) {
    var self = this;
    
    self.verify = function() {
        if($rootScope.dataCap === undefined) {
            if($rootScope.usuMaster.rol.rolID === "11")
                $location.url('/misCapacitaciones');
            else
                $location.url('/cursoCapacitacion');
        } else {
            self.capNom = $rootScope.dataCap.capNom;
            self.tipCap = $rootScope.dataCap.capTip;
            self.sedNom = $rootScope.dataCap.sedNom;
            self.sedCod = $rootScope.dataCap.sedCod;   
            self.sedEst = $rootScope.dataCap.sedEst;
            self.sedIni = new Date($rootScope.dataCap.sedIni);
            self.sedFin = new Date($rootScope.dataCap.sedFin);
        }
    };
    
    self.verify();
    
    var settingTests = {counts: []};
    self.tablaEvaluaciones = new NgTableParams({count: 10}, settingTests);    
    
    self.listarEvaluaciones = function() {
        if(self.sedCod !== undefined) {
            var request = crud.crearRequest('cursos', 1, 'listarEvaluaciones');
            request.setData({opt: 0, sedCod: self.sedCod});

            crud.listar('/curso_capacitacion', request, function (success) {
                if(success.data.length > 0) {
                    settingTests.dataset = success.data;
                    iniciarPosiciones(settingTests.dataset);
                    self.tablaEvaluaciones.settings(settingTests);
                } else
                    modal.mensaje("MENSAJE", "No existen evaluaciones para listar");
            }, function (error) {
                modal.mensaje("MENSAJE", "No se pudo listar las evaluaciones de la capacitación " + error.responseMsg);
            });
        }        
    };
       
    self.agregarEvaluacion = function() {
        var modalInstance = $uibModal.open({
            animation: true,
            keyboard: true,
            backdrop: true,
            templateUrl: 'test.html',
            controller: 'testCtrl',
            controllerAs: 'test',
            resolve: {
                evaluation: function() {
                    return {
                        cod: self.sedCod,
                        ini: self.sedIni,
                        fin: self.sedFin,
                        est: true
                    };
                }
            }
        });
        
        modalInstance.result.then(function(test) {
            if(self.tablaEvaluaciones.settings().total > 0) {
                self.tablaEvaluaciones.settings().dataset.push(test);
                self.tablaEvaluaciones.reload();
            } else {
                settingTests.dataset = [test];
                iniciarPosiciones(settingTests.dataset);
                self.tablaEvaluaciones.settings(settingTests);
            }
        }, function(){});
    };
    
    self.editarEvaluacion = function(test) {
        var modalInstance = $uibModal.open({
            animation: true,
            keyboard: true,
            backdrop: true,
            templateUrl: 'test.html',
            controller: 'testCtrl',
            controllerAs: 'test',
            resolve: {
                evaluation: function() {
                    return {
                        cod: self.sedCod,
                        ini: self.sedIni,
                        fin: self.sedFin,
                        obj: angular.copy(test),
                        est: false
                    };
                }
            }
        });
        
        modalInstance.result.then(function(test) {
            var original = self.tablaEvaluaciones.settings().dataset.filter(function(r) {
                return r.i === test.i;
            })[0];
            
            angular.extend(original, test);
        }, function(){});
    };
    
    self.eliminarEvaluacion = function(test) {
        var request = crud.crearRequest('cursos', 1, 'listarEvaluaciones');
        request.setData({opt: 2, evaCod: test.id});

        crud.listar('/curso_capacitacion', request, function (success) {
            if (success.response === 'OK') {
                var message = ((success.data.state)?"Se eliminarán las preguntas asociadas.":("Se eliminará la " + ((test.tip === 'E')?"encuesta.":"evaluación parcial."))) + " ¿Desea continuar?";
                modal.mensajeConfirmacion($scope, message, function () {
                    var request = crud.crearRequest('cursos', 1, 'eliminarEvaluacion');
                    request.setData({id: test.id});

                    crud.eliminar('/curso_capacitacion', request, function (success) {
                        if (success.response === 'OK') {
                            _.remove(self.tablaEvaluaciones.settings().dataset,function(item){
                                return test === item;
                            });
                            self.tablaEvaluaciones.reload();
                            modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                        } else if (success.response === 'BAD')
                            modal.mensaje("ERROR", success.responseMsg);
                    }, function (error) {
                        modal.mensaje("MENSAJE", error.responseMsg);
                    });
                }, '400');
                
                if(success.data.state) {
                    
                } else
                    modal.mensaje("MENSAJE", "Esta " + (test.tip === 'P' ? "evaluación parcial" : "encuesta") + " no posee preguntas por lo tanto no se puede aplicar a los participantes");
                    
            } else if (success.response === 'BAD')
                modal.mensaje("ERROR", success.responseMsg);
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo obtener las preguntas de la evaluación " + error.responseMsg);
        });
    };
        
    self.gestionarPreguntas = function(test) { 
        variablesTest.setData({
            capNom: self.capNom,
            tipCap: self.tipCap,
            sedNom: self.sedNom,
            sedCod: self.sedCod,
            sedEst: self.sedEst,
            evaCod: test.id,
            evaNom: test.nom,
            evaEst: test.est
        });
        
        $location.url('/evaluaciones/' + self.sedCod + '/' + test.id);
    }; 
    
    self.aplicarEvaluacion = function(test) {
        var request = crud.crearRequest('cursos', 1, 'listarEvaluaciones');
        request.setData({opt: 2, evaCod: test.id});

        crud.listar('/curso_capacitacion', request, function (success) {
            if (success.response === 'OK') {
                if(success.data.state) {
                    modal.mensajeConfirmacion($scope, "Al aplicar la evaluación no se podrá editar las preguntas y se enviarán notificaciones a los participantes. ¿Desea continuar?", function () {
                        $("#modalAplicarEvaluacion").modal({backdrop: 'static',keyboard: true});
                        $("#modalAplicarEvaluacion").modal('show');

                        var request = crud.crearRequest('cursos', 1, 'registrarEvaluacionParticipante');
                        request.setData({opt: 1, codEva: test.id, usuId: $rootScope.usuMaster.usuario.ID});

                        crud.insertar('/curso_capacitacion', request, function (success) {
                            if (success.response === 'OK') {
                                test.est = 'I';
                                var original = self.tablaEvaluaciones.settings().dataset.filter(function(r) {
                                    return r.i === test.i;
                                })[0];

                                angular.extend(original, test);
                                $("#modalAplicarEvaluacion").modal('toggle');
                                modal.mensaje("CONFIRMACION", "Los mensajes fueron enviados exitosamente");
                            } else if (success.response === 'BAD')
                                modal.mensaje("ERROR", success.responseMsg);
                        }, function (error) {
                            modal.mensaje("MENSAJE", error.responseMsg);
                        });
                    }, '400');
                } else
                    modal.mensaje("MENSAJE", "Esta " + (test.tip === 'P' ? "evaluación parcial" : "encuesta") + " no posee preguntas por lo tanto no se puede aplicar a los participantes");
                    
            } else if (success.response === 'BAD')
                modal.mensaje("ERROR", success.responseMsg);
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo obtener las preguntas de la evaluación " + error.responseMsg);
        });
    };
}]);

app.directive('capitalize', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {
            var capitalize = function(inputValue) {
                if (inputValue == undefined) inputValue = '';
                var capitalized = inputValue.toUpperCase();
                if (capitalized !== inputValue) {
                    modelCtrl.$setViewValue(capitalized);
                    modelCtrl.$render();
                }
                
                return capitalized;
            };
            
            modelCtrl.$parsers.push(capitalize);
            capitalize(scope[attrs.ngModel]);
        }
    };
});
  
mcApp.controller('testCtrl',['$uibModalInstance', '$rootScope', 'crud', 'modal', 'evaluation', function($uibModalInstance, $rootScope, crud, modal, evaluation) {
    var self = this;
    self.tipoEvaluacion = [{id: 'P', nom: "Evaluación Parcial"}, {id: 'E', nom: "Encuesta"}];
    self.est = evaluation.est;
    
    var minDate = new Date();
    if(minDate.getTime() >= evaluation.ini.getTime())
        minDate.setDate(minDate.getDate() + 1);
    else
        minDate = evaluation.ini;
    
    self.dateOptions = {
        maxDate: evaluation.fin,
        minDate: minDate,
        initDate: minDate,
        startingDay: 1
    };
    
    if(self.est) {
        self.title = "Nueva Evaluación";
        self.action = "Crear Nuevo";
    } else {
        self.title = "Edición Evaluación";
        self.test = evaluation.obj;
        self.action = "Modificar";
    }
    
    self.aceptar = function() {
        self.test.ini = new Date(self.test.ini);
        self.test.fin = new Date(self.test.fin);
        
        if(self.test.fin.getTime() >= self.test.ini.getTime()) {
            if(self.est) {
                self.test.sed = evaluation.cod;
                var request = crud.crearRequest('cursos', 1, 'registrarEvaluacion');            
                request.setData({tip: "N", evaluaciones: [self.test]});

                crud.insertar('/curso_capacitacion', request, function (success) {                
                    if (success.response === 'OK') {
                        modal.mensaje("CONFIRMACION", "El registro se realizó correctamente");
                        var object = angular.copy(self.test);
                        object.ini = object.ini.getTime();
                        object.fin = object.fin.getTime();
                        object.id = success.data.id;
                        object.est = success.data.est;
                        $uibModalInstance.close(object);
                    } else if (success.response === 'BAD')
                        modal.mensaje("ERROR", success.responseMsg);
                }, function (error) {
                    modal.mensaje("MENSAJE", error.responseMsg);
                    $uibModalInstance.dismiss('cancel');
                });
            } else {
                var request = crud.crearRequest('cursos', 1, 'editarEvaluacion');            
                request.setData({eva: self.test});
                
                crud.actualizar('/curso_capacitacion', request, function (success) {
                    if (success.response === 'OK') {
                        modal.mensaje("CONFIRMACION", "La modificación se realizó correctamente");
                        $uibModalInstance.close(self.test);
                    } else if (success.response === 'BAD')
                        modal.mensaje("ERROR", success.responseMsg);
                }, function (error) {
                    modal.mensaje("MENSAJE", error.responseMsg);
                    $uibModalInstance.dismiss('cancel');
                });
            }            
        } else
            modal.mensaje("ERROR", "Verifique las fechas de entrega");
    };
    
    self.cancelar = function(){
        $uibModalInstance.dismiss('cancel');
    };
}]);

mcApp.controller('preguntaEvaluacionCtrl',['$rootScope', 'crud', 'modal', 'variablesTest', '$location', '$uibModal', 'NgTableParams', '$scope', function($rootScope, crud, modal, variablesTest, $location, $uibModal, NgTableParams, $scope) {
    var self = this;
    
    self.verify = function() {
        if(angular.equals(variablesTest.getData(), {})) {
            if($rootScope.usuMaster.rol.rolID === "11")
                $location.url('/misCapacitaciones');
            else
                $location.url('/cursoCapacitacion');
        } else {
            var data = variablesTest.getData();
    
            self.capNom = data.capNom;
            self.tipCap = data.tipCap;
            self.sedNom = data.sedNom;
            self.sedCod = data.sedCod;
            self.sedEst = data.sedEst;   
            self.evaCod = data.evaCod;
            self.evaNom = data.evaNom;
            self.evaEst = data.evaEst;
        }
    };
    
    self.verify();
    
    var settingQuestions = {counts: []};
    self.tablaPreguntas = new NgTableParams({count: 10}, settingQuestions);    
    
    self.listarPreguntas = function() {
        var request = crud.crearRequest('cursos', 1, 'listarPreguntas');
        request.setData({evaCod: self.evaCod});

        crud.listar('/curso_capacitacion', request, function (success) {
            if(success.data.length > 0) {
                settingQuestions.dataset = success.data;
                iniciarPosiciones(settingQuestions.dataset);
                self.tablaPreguntas.settings(settingQuestions);
            } else
                modal.mensaje("MENSAJE", "No existen preguntas para listar");
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar las preguntas de la evaluación " + error.responseMsg);
        });
    };
    
    self.agregarPregunta = function() {
        var modalInstance = $uibModal.open({
            animation: true,
            keyboard: true,
            backdrop: true,
            templateUrl: 'question.html',
            controller: 'questionCtrl',
            controllerAs: 'question',
            resolve: {
                question: function() {
                    return {
                        eva: self.evaCod,
                        est: true
                    };
                }
            }
        });
        
        modalInstance.result.then(function(question) {
            if(self.tablaPreguntas.settings().total > 0) {
                self.tablaPreguntas.settings().dataset.push(question);
                iniciarPosiciones(self.tablaPreguntas.settings().dataset);
                self.tablaPreguntas.reload();
            } else {
                settingQuestions.dataset = [question];
                iniciarPosiciones(settingQuestions.dataset);
                self.tablaPreguntas.settings(settingQuestions);
            }
        }, function(){});        
    };
    
    self.editarPregunta = function(question) {
        var modalInstance = $uibModal.open({
            animation: true,
            keyboard: true,
            backdrop: true,
            templateUrl: 'question.html',
            controller: 'questionCtrl',
            controllerAs: 'question',
            resolve: {
                question: function() {
                    return {
                        eva: self.evaCod,
                        obj: angular.copy(question),
                        est: false
                    };
                }
            }
        });
        
        modalInstance.result.then(function(question) {
            var original = self.tablaPreguntas.settings().dataset.filter(function(r) {
                return r.i === question.i;
            })[0];
            
            angular.extend(original, question);
        }, function(){});
    };
    
    self.eliminarPregunta = function(question) {
        modal.mensajeConfirmacion($scope, "La pregunta dejará de estar disponible en el banco de preguntas ¿Desea continuar?", function () {
            var request = crud.crearRequest('cursos', 1, 'eliminarPregunta');
            request.setData({id: question.id, usuMod: $rootScope.usuMaster.usuario.usuarioID});
                    
            crud.eliminar('/curso_capacitacion', request, function (success) {
                if (success.response === 'OK') {
                    _.remove(self.tablaPreguntas.settings().dataset,function(item){
                        return question === item;
                    });
                    self.tablaPreguntas.reload();
                    modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                } else if (success.response === 'BAD')
                    modal.mensaje("ERROR", success.responseMsg);
            }, function (error) {
                modal.mensaje("MENSAJE", error.responseMsg);
            });
        }, '400'); 
    };
    
    self.bancoPreguntas = function() {
        $location.url('/evaluaciones/bancoPreguntas');
    };  
}]);

mcApp.controller('bancoPreguntasCtrl',['$rootScope', 'crud', 'modal', 'variablesTest', '$location', '$uibModal', 'NgTableParams', '$scope', function($rootScope, crud, modal, variablesTest, $location, $uibModal, NgTableParams, $scope) {
    var self = this;
    
    self.verify = function() {
        if(angular.equals(variablesTest.getData(), {})) {
            if($rootScope.usuMaster.rol.rolID === "11")
                $location.url('/misCapacitaciones');
            else
                $location.url('/cursoCapacitacion');
        } else {
            var data = variablesTest.getData();   
            self.evaCod = data.evaCod;
        }
    };
    
    self.verify();
    
    var settingQuestions = {counts: []};
    self.tablaPreguntas = new NgTableParams({count: 5}, settingQuestions);    
    self.state = {orgCod: '', orgEst: false, capCod: '', capTip: '', capEst: false, sedCod: '', sedEst: false, evaCod: '', evaEst: false};
    
    self.listarOrg = function() {        
        var request = crud.crearRequest('cursos', 1, 'listarOrganizaciones');
        request.setData({opt: 1});

        crud.listar('/curso_capacitacion', request, function (success) {
            if(success.data.length > 0)
                self.organizaciones = success.data;
            else
                modal.mensaje("MENSAJE", "No existen organizaciones para listar");
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar las organizaciones " + error.responseMsg);
        });
    };
    
    self.cambiarOrg = function() {
        self.limpiar('O');
        
        var request = crud.crearRequest('cursos', 1, 'listarCursos');
        request.setData({opt: 7, codOrg: self.state.orgCod});

        crud.listar('/curso_capacitacion', request, function (success) {
            if(success.data.length > 0) {
                self.capacitaciones = success.data;
                self.state.orgEst = true;
            } else
                modal.mensaje("MENSAJE", "No existen capacitaciones para listar");
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar las capacitaciones " + error.responseMsg);
        });
    };
    
    self.cambiarCap = function() {
        self.state.capTip = self.capacitaciones.filter(function(r){
            return r.cod === parseInt(self.state.capCod);
        })[0].tip;
        
        self.limpiar('C');
        
        var request = crud.crearRequest('cursos', 1, 'listarSedes');
        request.setData({opt: 1, codCap: self.state.capCod});

        crud.listar('/curso_capacitacion', request, function (success) {
            if(success.data.length > 0) {
                if(self.state.capTip === 'Presencial') {
                    self.sedes = success.data;
                    self.state.capEst = true;
                } else {
                    self.state.sedCod = success.data[0].cod;
                    self.cambiarSed();
                }
            } else
                modal.mensaje("MENSAJE", "No existen capacitaciones para listar");
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar las capacitaciones " + error.responseMsg);
        });
    };
    
    self.cambiarSed = function() {
        self.limpiar('S');
        
        var request = crud.crearRequest('cursos', 1, 'listarEvaluaciones');
        request.setData({opt: 1, sedCod: self.state.sedCod});

        crud.listar('/curso_capacitacion', request, function (success) {
            if(success.data.length > 0) {
                self.evaluaciones = success.data;
                self.state.sedEst = true;
            } else
                modal.mensaje("MENSAJE", "No existen evaluaciones para listar");
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar las evaluaciones " + error.responseMsg);
        });
    };
    
    self.cambiarEva = function() {
        self.limpiar('E');
        
        var request = crud.crearRequest('cursos', 1, 'listarPreguntas');
        request.setData({evaCod: self.state.evaCod});

        crud.listar('/curso_capacitacion', request, function (success) {
            if(success.data.length > 0) {
                settingQuestions.dataset = success.data;
                iniciarPosiciones(settingQuestions.dataset);
                self.tablaPreguntas.settings(settingQuestions);
                self.state.evaEst = true;
                self.watch = false;
            } else
                modal.mensaje("MENSAJE", "No existen evaluaciones para listar");
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar las evaluaciones " + error.responseMsg);
        });
    };
    
    self.verPregunta = function(question) {
        self.watch = true;
        self.question = question;
    };
    
    self.reasignarPregunta = function(question) {
        var request = crud.crearRequest('cursos', 1, 'reasignarPregunta');            
        request.setData({usuMod: $rootScope.usuMaster.usuario.usuarioID,
                         eva: variablesTest.getData().evaCod,
                         id: question.id});

        crud.insertar('/curso_capacitacion', request, function (success) {                
               if (success.response === 'OK') {
                   modal.mensaje("CONFIRMACION", "La pregunta ha sido añadida a la evaluación " + variablesTest.getData().evaNom);
               } else if (success.response === 'BAD')
                   modal.mensaje("ERROR", success.responseMsg);
           }, function (error) {
               modal.mensaje("MENSAJE", error.responseMsg);
        });
    };
    
    self.limpiar = function(opt) {
        switch(opt) {
            case 'O':
                self.state.orgEst = false;
                self.state.capCod = '';
                self.state.capTip = '';
                self.state.capEst = false;
                self.state.sedCod = ''; 
                self.state.sedEst = false;
                self.state.evaCod = '';
                self.state.evaEst = false;
                break;
                
            case 'C':
                self.state.capEst = false;
                self.state.sedCod = ''; 
                self.state.sedEst = false;
                self.state.evaCod = '';
                self.state.evaEst = false;
                break;
                
            case 'S':
                self.state.sedEst = false;
                self.state.evaCod = '';
                self.state.evaEst = false;
                break;
                
            case 'E':
                self.state.evaEst = false;
                break;
        }        
    };
}]);

mcApp.controller('questionCtrl',['$uibModalInstance', '$rootScope', 'crud', 'modal', 'question', 'NgTableParams', function($uibModalInstance, $rootScope, crud, modal, question, NgTableParams) {
    var self = this;
    self.tipoPregunta = [{id: 'S', nom: "Simple"}, {id: 'M', nom: "Opción Múltiple"}, {id: 'L', nom: "Verdadero o Falso"}];
    self.est = question.est;
    
    var settingQuestions = {counts: []};
    self.tableOptions = new NgTableParams({count: 5}, settingQuestions);   
    self.opt = {
        nom: '',
        sel: false
    };
        
    if(self.est) {
        self.title = "Nueva Pregunta";
        self.action = "Crear Nuevo";
    } else {
        self.title = "Edición Pregunta";
        self.action = "Modificar";
        self.question = question.obj;
        var res = self.question.res;
        self.question.res = new Object();
        
        switch(self.question.tip) {
            case 'S': 
                self.question.res.s = res; 
                break;
            case 'L': 
                self.question.res.l = res; 
                break;
            case 'M':                 
                settingQuestions.dataset = res;
                iniciarPosiciones(settingQuestions.dataset);
                self.tableOptions.settings(settingQuestions);
                self.tableForm = true;
                break;
        }        
    }
    
    self.change = function(type) {
        switch(type) {
            case 'L': 
                if(self.question.res === undefined) {
                    self.question.res = new Object();
                    self.question.res.l = true;
                } else if(self.question.res.l === undefined)
                    self.question.res.l = true;
            break;
        }
    };
    
    self.activar = function(row) {
        row.sel = !row.sel
        if(!self.est)
            self.tableForm = false;
    };
    
    self.aceptar = function () {
        if (self.question.tip === 'M' && (self.tableOptions.settings().dataset === null || self.tableOptions.settings().dataset.length <= 0))
            modal.mensaje("ERROR", "Es necesario ingresar por lo menos una opción de respuesta");
        else {
            if (self.est) {
                var request = crud.crearRequest('cursos', 1, 'registrarPregunta');            

                if(self.question.tip === 'M') {
                    self.question.res = new Object();
                    self.question.res.m = self.tableOptions.settings().dataset;
                }
                    
                request.setData({usuMod: $rootScope.usuMaster.usuario.usuarioID,
                                 question: self.question, 
                                 eva: question.eva});

                 crud.insertar('/curso_capacitacion', request, function (success) {                
                        if (success.response === 'OK') {
                            modal.mensaje("CONFIRMACION", "El registro se realizó correctamente");     
                            self.question.id = success.data.id;
                            self.question.emp = success.data.emp;
                            var res = null;
                           
                            switch(self.question.tip) {
                                case 'S': res = self.question.res.s; break;
                                case 'L': res = self.question.res.l; break;
                                case 'M': res = self.question.res.m; break;
                            }
                            
                            delete self.question.res;
                            self.question.res = res;
                            $uibModalInstance.close(self.question);
                        } else if (success.response === 'BAD')
                            modal.mensaje("ERROR", success.responseMsg);
                    }, function (error) {
                        modal.mensaje("MENSAJE", error.responseMsg);
                    $uibModalInstance.dismiss('cancel');
                 });
            } else {
                var request = crud.crearRequest('cursos', 1, 'editarPregunta');
                
                if(self.question.tip === 'M')
                    self.question.res.m = self.tableOptions.settings().dataset;
                
                request.setData({usuMod: $rootScope.usuMaster.usuario.usuarioID,
                                 question: self.question, 
                                 id: self.question.id});

                crud.actualizar('/curso_capacitacion', request, function (success) {
                    if (success.response === 'OK') {
                        modal.mensaje("CONFIRMACION", "La modificación se realizó correctamente");
                        self.question.emp = success.data.emp;
                        var res = null;
                           
                        switch(self.question.tip) {
                            case 'S': res = self.question.res.s; break;
                            case 'L': res = self.question.res.l; break;
                            case 'M': res = self.question.res.m; break;
                        }
                            
                        delete self.question.res;
                        self.question.res = res;
                        $uibModalInstance.close(self.question);
                    } else if (success.response === 'BAD')
                        modal.mensaje("ERROR", success.responseMsg);
                }, function (error) {
                    modal.mensaje("MENSAJE", error.responseMsg);
                    $uibModalInstance.dismiss('cancel');
                });
            }
        }
    };
    
    self.salir = function(){
        $uibModalInstance.dismiss('cancel');
    };
    
    self.buscar = function(row, rowForm){
        row.isEditing = false;
        rowForm.$setPristine();
        return settingQuestions.dataset.filter(function(r) {
            return r.i === row.i;
        })[0];
    };
    
    self.editar = function(row) {
        self.row = angular.copy(row);
        row.isEditing = true;
    };
    
    self.eliminar = function (row) {
        _.remove(self.tableOptions.settings().dataset, function (item) {
            return row === item;
        });

        self.tableOptions.reload().then(function (data) {
            if (data.length === 0 && self.tableOptions.total() > 0) {
                self.tableOptions.page(self.tableOptions.page() - 1);
                self.tableOptions.reload();
            }
        });
        
        if(!self.est)
            self.tableForm = false;
    };
    
    self.cancelar = function(row, rowForm) {
        var originalRow = self.buscar(row, rowForm);
        angular.extend(row, originalRow);
    };
        
    self.guardar = function(row, rowForm) {
        var originalRow = self.buscar(row, rowForm);
        angular.extend(originalRow, row);
    };
    
    self.agregar = function() {
        if(self.tableOptions.settings().total > 0) {
            self.tableOptions.settings().dataset.push(angular.copy(self.opt));
            iniciarPosiciones(self.tableOptions.settings().dataset);
            self.tableOptions.reload();
        } else {
            settingQuestions.dataset = [angular.copy(self.opt)];
            iniciarPosiciones(settingQuestions.dataset);
            self.tableOptions.settings(settingQuestions);
        }
        
        self.opt = {
            nom: '',
            sel: false
        };
    };
}]);
