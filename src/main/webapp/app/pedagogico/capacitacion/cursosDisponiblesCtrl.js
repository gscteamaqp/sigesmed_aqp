app.controller("cursosDisponiblesCtrl", ['$rootScope', '$scope', 'NgTableParams', 'crud', 'modal', function ($rootScope, $scope, NgTableParams, crud, modal) {
    var settingCapacitacion = {counts: []};
    $scope.tablaCapacitaciones = new NgTableParams({count: 10}, settingCapacitacion);
    
    $scope.listarCursosCapacitacion = function () {
        var request = crud.crearRequest('cursos', 1, 'listarCursos');
        request.setData({opt: 9,
                         perId: $rootScope.usuMaster.usuario.usuarioID});

        crud.listar('/curso_capacitacion', request, function (success) {
            if (success.response === 'OK') {
                if(success.data.length > 0) {
                    settingCapacitacion.dataset = success.data;
                    iniciarPosiciones(settingCapacitacion.dataset);
                    $scope.tablaCapacitaciones.settings(settingCapacitacion);
                } else
                    modal.mensaje("MENSAJE", "No hay cursos de capacitación disponibles");
            } else if (success.response === 'BAD')
                modal.mensaje("ERROR", success.responseMsg);
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar los cursos de capacitación " + error.responseMsg);
        });
    };
    
    $scope.enviarSolicitud = function (sedCod, usuAdm) {
        var objSede = $scope.tablaCapacitaciones.settings().dataset.filter(function(r){
            return r.cod == sedCod;
        })[0];
        
        var request = crud.crearRequest('cursos', 1, 'registrarMensaje');
        request.setData({opt: 1,
                        usuPerId: usuAdm, 
                        perId: $rootScope.usuMaster.usuario.usuarioID, 
                        curNom: objSede.nom, 
                        fecIni: objSede.ini,
                        fecFin: objSede.fin,
                        usuAut: $rootScope.usuMaster.usuario.ID});

        crud.insertar('/curso_capacitacion', request, function (success) {
            if (success.response === 'OK')
                modal.mensaje("MENSAJE", "El mensaje ha sido enviado al administrador del curso de capacitación");
            else if (success.response === 'BAD')
                modal.mensaje("ERROR", success.responseMsg);
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar los cursos de capacitación " + error.responseMsg);
        });
    };
}]);