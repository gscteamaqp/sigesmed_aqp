app.controller("registroParticipantesCtrl", ["$rootScope", "$scope", "$http", "NgTableParams", "crud", "modal", "urls", '$location', function ($rootScope, $scope, $http, NgTableParams, crud, modal, urls, $location) {
    $scope.tiposRegistro = ["Selección", "Público"];
    $scope.niveles = ["Primaria", "Secundaria"];
    $scope.especialidades = ["Ciencias", "Letras"];
    
    var settingDocentes = {counts: []};
    $scope.tablaDocentes = new NgTableParams({count: 5}, settingDocentes);    
    $scope.capacitacionesBusqueda = [];
    $scope.roles = [];
    $scope.state = {
        tipo: 'Selección',
        capTip: '',
        tipEst: false,
        capCod: '',
        capEst: false,
        sedCod: '',
        sedEst: false,
        sedIni: '',
        sedFin: '',
        orgCod: '',
        orgNiv: '',
        orgEsp: '',
        orgEst: false,
        modEst: false,
        parCod: '',
        parNom: '',
        parCor: '',
        parEst: false,
        modPub: false,
        rolId: 8
    };
    
    $scope.$watch('state.capTip', function(newValue) {
        if($scope.state.capTip !== '' && $scope.state.capTip !== null) {    
            $scope.limpiar('T');
            var request = crud.crearRequest('cursos', 1, 'listarCursos');
            request.setData({opt: 5, tip: newValue});
                
            crud.listar('/curso_capacitacion', request, function (success) {
                if(success.data.length > 0) {                    
                    $scope.capacitaciones = success.data;
                    $scope.state.tipEst = true;
                } else
                    modal.mensaje("MENSAJE", "No se encontraron capacitaciones disponibles del tipo seleccionado");
            }, function (error) {
                modal.mensaje("MENSAJE", "No se pudo listar los cursos de capacitación " + error.responseMsg);
            });
        }        
    });
    
    $scope.listarSedes = function() {
        $scope.limpiar('C');
        var request = crud.crearRequest('cursos', 1, 'listarSedes');
        request.setData({opt: 0, cod: $scope.state.capCod});

        crud.listar('/curso_capacitacion', request, function (success) {
            if(success.data.length > 0) {
                $scope.state.capEst = true;
                $scope.sedes = success.data;
                
                if($scope.state.capTip === 'Online') {
                    $scope.state.sedCod = $scope.sedes[0].cod;
                    
                    if($scope.state.tipo === 'Público') {
                        $scope.state.sedIni = $scope.sedes[0].ini;
                        $scope.state.sedFin = $scope.sedes[0].fin;
                        $scope.cargarDocentes();                        
                    } else 
                        $scope.listarInstituciones();
                }
            } else
                modal.mensaje("MENSAJE", "No existen capacitaciones para listar");
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar las capacitaciones " + error.responseMsg);
        });
    };
        
    $scope.listarInstituciones = function() {
        $scope.limpiar('S');
        var request = crud.crearRequest('cursos', 1, 'listarOrganizaciones');
        request.setData({opt: 0});
        
        crud.listar('/curso_capacitacion', request, function (success) {
            $scope.state.sedEst = true;
            $scope.organizaciones = success.data;            
        }, function (error) {
            $scope.registro.estSed = false;
            modal.mensaje("MENSAJE", "No se pudo listar las organizaciones " + error.responseMsg);
        });
    };
    
    $scope.listarPersonas = function(){
         $scope.limpiar('O');
        var request = crud.crearRequest('cursos', 1, 'listarPersonas');
        request.setData({opt: 2, orgId: $scope.state.orgCod, sedId: $scope.state.sedCod});

    }
    
        
    $scope.listarDocentes = function() {
        $scope.limpiar('O');
        var request = crud.crearRequest('cursos', 1, 'listarPersonas');
        request.setData({opt: 2, orgId: $scope.state.orgCod, sedId: $scope.state.sedCod});

        crud.listar('/curso_capacitacion', request, function (success) {
            if(success.data.length > 0) {
                $scope.state.orgEst = true;
                $scope.docentes = success.data;                
                settingDocentes.dataset = success.data;
                iniciarPosiciones(settingDocentes.dataset);
                $scope.tablaDocentes.settings(settingDocentes);
            } else
                modal.mensaje("MENSAJE", "No se encontraron personas disponibles en esta entidad para ser agregados a la capacitación");                   
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar Personas " + error.responseMsg);
        });
    };
    
    
    
    $scope.cargarDocentes = function() {
        $scope.limpiar('D');
        var request = crud.crearRequest('cursos', 1, 'listarPersonas');
        request.setData({opt: 3, sedCod: $scope.state.sedCod});
        crud.listar('/curso_capacitacion', request, function (success) {
            if (success.response === 'OK') {
                $scope.teachers = success.data;
                $scope.state.modEst = true;
            } else if (success.response === 'BAD')
                modal.mensaje("ERROR", success.responseMsg);
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar a los docentes " + error.responseMsg);
        });
    };
            
    $scope.cambiarSeleccion = function(obj) {
        var index = settingDocentes.dataset.indexOf(obj);
        settingDocentes.dataset[index].sel = !settingDocentes.dataset[index].sel;
    };
        
    $scope.seleccionarTodos = function() {        
        settingDocentes.dataset.forEach(function(obj) {
            obj.sel = true;
        });
    };
    
    $scope.completeList = function() {
        $scope.limpiar('P');
        if($scope.state.parNom === '') 
            $scope.hide = true; 
        else {
            $scope.hide = false;  
            var output = [];  
            angular.forEach($scope.teachers, function(teacher){
                if(teacher.nom.toLowerCase().indexOf($scope.state.parNom.toLowerCase()) >= 0) 
                    output.push(teacher);
            });
            
            $scope.filterTeachers = output;
        }
    };
    
    $scope.fillInformation = function(parCod){
        $scope.state.parCod = parCod;
        $scope.docente = $scope.teachers.filter(function(teacher){
            return teacher.cod === $scope.state.parCod;
        })[0];
        $scope.state.parNom = $scope.docente.nom;
        $scope.state.parCor = $scope.docente.cor;
        $scope.hide = true;  
        $scope.state.parEst = true;
    };  

    $scope.mostrarDatos = function() {
        var location = $scope.sedes.filter(function(obj) {
            return obj.cod == $scope.state.sedCod;
        })[0];
        
        $scope.state.sedIni = location.ini;
        $scope.state.sedFin = location.fin;
        $scope.cargarDocentes();
    };
    
    //Para asignar Roles a Usuarios Externos al Sistema
    $scope.listarRoles = function() {
         var request = crud.crearRequest('rol', 1, 'listarRoles');
          crud.listar("/configuracionInicial",request,function(data){
              if(data.data){
                  $scope.roles = data.data;
              }
          },function(data){
            console.info(data);
        });
    }
    
    $scope.enviarCorreos = function() {
        var request = crud.crearRequest('cursos', 1, 'registrarParticipante');
        var correct = false;
        
        $http.get('../recursos/json/configuracion.json').success(function (data) {            
            if($scope.state.tipo === 'Selección') {
                if($scope.state.orgEst) {
                    var docentes = $scope.docentes.filter(function(obj) {
                        return obj.sel === true;
                    });

                    if(docentes.length > 0) {
                        request.setData({pue: $location.port(), pro: $location.protocol(), ip: data.ip_publica, opt: $scope.state.tipo, usu: $rootScope.usuMaster.usuario.usuarioID, url: urls.BASELOGIN, sedCod: $scope.state.sedCod, parCur: docentes});
                        correct = true;
                    } else
                        modal.mensaje("ERROR", "No se ha seleccionado a ningún docente");
                } else
                    modal.mensaje("ERROR", "Aún no se ha listado a los docentes, complete la búsqueda");
            } else {
                if($scope.state.modEst) {
                    if($scope.state.modPub) {
                        if($scope.state.parEst) {
                            request.setData({pue: $location.port(), pro: $location.protocol(), ip: data.ip_publica, opt: $scope.state.tipo, usu: $rootScope.usuMaster.usuario.usuarioID, url: urls.BASELOGIN, sedCod: $scope.state.sedCod, tipMod: $scope.state.modPub, parCod: $scope.state.parCod, parCor: $scope.state.parCor});
                            correct = true;
                        } else
                            modal.mensaje("ERROR", "No se ha seleccionado a ningún docente");
                    } else {
                        if(!$scope.myForm.email.$error.email && !$scope.myForm.email.$error.required) {
                            request.setData({pue: $location.port(), pro: $location.protocol(), ip: data.ip_publica, opt: $scope.state.tipo, usu: $rootScope.usuMaster.usuario.usuarioID, url: urls.BASELOGIN, sedCod: $scope.state.sedCod, tipMod: $scope.state.modPub, parCor: $scope.state.parCor ,rolId: $scope.state.rolId });
                            correct = true;
                        } else
                            modal.mensaje("ERROR", "No se ha ingresado ningún correo electrónico válido");
                    }
                } else
                    modal.mensaje("ERROR", "Aún no se ha completado la búsqueda del docente");
            }

            if(correct) {
                $scope.msm = 'El/Los mensaje(s) está(n) siendo enviado(s) al/los docente(s)';
                $("#modalRegistroParticipantes").modal({backdrop: 'static',keyboard: true});
                $("#modalRegistroParticipantes").modal('show');

                crud.insertar('/curso_capacitacion', request, function (success) {
                    if (success.response === 'OK') {
                        $("#modalRegistroParticipantes").modal('toggle');
                        location.reload();
                    } else if (success.response === 'BAD')
                        modal.mensaje("ERROR", success.responseMsg);
                }, function (errorTraining) {
                    modal.mensaje("MENSAJE", errorTraining.responseMsg);
                });
            }
        });
    };
    
    $scope.limpiar = function(type) {
        switch(type) {
            case 'G':
                $scope.state.capTip = '';
                $scope.state.tipEst = false;
                $scope.state.capCod = '';
                $scope.state.capEst = false;
                $scope.state.sedCod = '';                
                $scope.state.sedIni = '';
                $scope.state.sedFin = '';
                $scope.state.sedEst = false;
                $scope.state.orgCod = '';
                $scope.state.orgNiv = '';
                $scope.state.orgEsp = '';
                $scope.state.orgEst = false;
                $scope.state.modEst = false;
                $scope.state.parCod = '';
                $scope.state.parNom = '';
                $scope.state.parCor = '';
                $scope.state.parEst = false;
                $scope.state.modPub = false;
                break;
                
            case 'T':
                $scope.state.tipEst = false;
                $scope.state.capCod = '';
                $scope.state.capEst = false;
                $scope.state.sedCod = '';                
                $scope.state.sedIni = '';
                $scope.state.sedFin = '';
                $scope.state.sedEst = false;
                $scope.state.orgCod = '';
                $scope.state.orgNiv = '';
                $scope.state.orgEsp = '';
                $scope.state.orgEst = false;
                $scope.state.modEst = false;
                $scope.state.parCod = '';
                $scope.state.parNom = '';
                $scope.state.parCor = '';
                $scope.state.parEst = false;
                $scope.state.modPub = false;
                break;
                
            case 'C':
                $scope.state.capEst = false;
                $scope.state.sedCod = '';                
                $scope.state.sedIni = '';
                $scope.state.sedFin = '';
                $scope.state.sedEst = false;
                $scope.state.orgCod = '';
                $scope.state.orgNiv = '';
                $scope.state.orgEsp = '';
                $scope.state.orgEst = false;
                $scope.state.modEst = false;
                $scope.state.parCod = '';
                $scope.state.parNom = '';
                $scope.state.parCor = '';
                $scope.state.parEst = false;
                $scope.state.modPub = false;
                break;
            
            case 'S':
                $scope.state.sedEst = false;
                $scope.state.orgCod = '';
                $scope.state.orgNiv = '';
                $scope.state.orgEsp = '';
                $scope.state.orgEst = false;
                $scope.state.parCod = '';
                $scope.state.parNom = '';
                $scope.state.parCor = '';
                $scope.state.parEst = false;
                $scope.state.modPub = false;
                break;
                
            case 'O':
                $scope.state.orgNiv = '';
                $scope.state.orgEsp = '';
                $scope.state.orgEst = false;
                $scope.state.parCod = '';
                $scope.state.parNom = '';
                $scope.state.parCor = '';
                $scope.state.parEst = false;
                $scope.state.modPub = false;
                break;
            
            case 'D':
                $scope.state.parCod = '';  
                $scope.state.parNom = '';
                $scope.state.parCor = '';
                $scope.state.parEst = false;
                $scope.hide = true; 
                break;
            
            case 'P':
                $scope.state.parCod = '';                
                $scope.state.parCor = '';
                $scope.state.parEst = false;
                break;
        }
    };
}]);