app.controller("proyectosExtra",["$rootScope","$scope","$sce","NgTableParams","crud","modal", function ($rootScope,$scope,$sce,NgTableParams,crud,modal){

    $scope.content = "";
    $scope.dataBase64 = "";

    //PROYECTOS

    $scope.proyectoNuevo = {};

    var paramsPro = {count: 10};
    var settingPro = {counts: []};
    $scope.tablaProyectos = new NgTableParams(paramsPro, settingPro);

    $scope.guardarProyecto = function() {

        if($scope.proid !== 0){

            modal.mensajeConfirmacion($scope,"seguro que desea actualizar este proyecto",function(){
                var request = crud.crearRequest('proyectos',1,"actualizarProyectos");

                var fechaini =  [$scope.proyectoNuevo.proini.getDate(), $scope.proyectoNuevo.proini.getMonth()+1, $scope.proyectoNuevo.proini.getFullYear()].join('/');
                var fechafin =  [$scope.proyectoNuevo.profin.getDate(), $scope.proyectoNuevo.profin.getMonth()+1, $scope.proyectoNuevo.profin.getFullYear()].join('/');

//                var actividades = $scope.tablaActividades.data;
//                
//                for(var k=0; k < actividades.length; ++k){
//                    actividades[k].pacini = [actividades[k].pacini.getDate(), actividades[k].pacini.getMonth()+1, actividades[k].pacini.getFullYear()].join('/');;
//                    actividades[k].pacfin = [actividades[k].pacfin.getDate(), actividades[k].pacfin.getMonth()+1, actividades[k].pacfin.getFullYear()].join('/');;;
//                }

                if($scope.proyectoNuevo.archivo === undefined){
                    request.setData({prodoc:$scope.proyectoNuevo.prodoc, pronom:$scope.proyectoNuevo.pronom, protip:$scope.proyectoNuevo.protip, prores:$scope.proyectoNuevo.prores.traId, proini:fechaini, profin:fechafin, proid:$scope.proid}); //actividades:actividades
                }
                else{
                    request.setData({prodoc:$scope.proyectoNuevo.prodoc, archivo:$scope.proyectoNuevo.archivo, pronom:$scope.proyectoNuevo.pronom, protip:$scope.proyectoNuevo.protip, prores:$scope.proyectoNuevo.prores.traId, proini:fechaini, profin:fechafin, proid:$scope.proid}); //actividades:actividades
                }

                crud.actualizar("/smdg",request,function(response){
                    modal.mensaje("CONFIRMACION",response.responseMsg);
                    $scope.listarProyectos();
                    //            REINICIAR VARIABLES
                    $scope.proyectoNuevo = {};

                    settingActividades.dataset = [];
                    iniciarPosiciones(settingActividades.dataset);
                    $scope.tablaActividades.settings(settingActividades);

                    settingRecursos.dataset = [];
                    iniciarPosiciones(settingRecursos.dataset);
                    $scope.tablaRecursos.settings(settingRecursos);

                    $scope.proid = 0;
                },function(data){
                    console.info(data);
                });
            });
            return;
        }

        var request = crud.crearRequest('proys',1,"registrarProyectoDocente");
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error          
        var fechaini =  [$scope.proyectoNuevo.proini.getDate(), $scope.proyectoNuevo.proini.getMonth()+1, $scope.proyectoNuevo.proini.getFullYear()].join('/');
        var fechafin =  [$scope.proyectoNuevo.profin.getDate(), $scope.proyectoNuevo.profin.getMonth()+1, $scope.proyectoNuevo.profin.getFullYear()].join('/');

        var actividades = $scope.tablaActividades.data;

        for(var k=0; k < actividades.length; ++k){
            actividades[k].pacini = [actividades[k].pacini.getDate(), actividades[k].pacini.getMonth()+1, actividades[k].pacini.getFullYear()].join('/');;
            actividades[k].pacfin = [actividades[k].pacfin.getDate(), actividades[k].pacfin.getMonth()+1, actividades[k].pacfin.getFullYear()].join('/');;;
        }

        request.setData({org:$rootScope.usuMaster.organizacion.organizacionID,doc:$rootScope.usuMaster.usuario.usuarioID,prodoc:$scope.proyectoNuevo.prodoc, archivo:$scope.proyectoNuevo.archivo ,pronom:$scope.proyectoNuevo.pronom, protip:$scope.proyectoNuevo.protip, prores:$scope.proyectoNuevo.prores.traId ,proini:fechaini, profin:fechafin, actividades:actividades});
        crud.insertar("/maestro",request,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            $scope.listarProyectos();
//            REINICIAR VARIABLES
            $scope.proyectoNuevo = {};

            settingActividades.dataset = [];
            iniciarPosiciones(settingActividades.dataset);
            $scope.tablaActividades.settings(settingActividades);

            settingRecursos.dataset = [];
            iniciarPosiciones(settingRecursos.dataset);
            $scope.tablaRecursos.settings(settingRecursos);

        },function(data){
            console.info(data);
        });
    };

    $scope.eliminarProyecto = function(i, proid){
        modal.mensajeConfirmacion($scope,"seguro que desea eliminar este registro",function(){

            var request = crud.crearRequest('proyectos',1,'eliminarProyectos');
            request.setData({proid:proid});

            crud.actualizar("/smdg",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    eliminarElemento(settingPro.dataset,i);
                    $scope.tablaProyectos.reload();
                }
            },function(data){
                console.info(data);
            });
        });
    };

    $scope.proid = 0;
    $scope.guardaroactualizar = "Guardar";

    $scope.editarProyecto = function(p){
        $scope.proid = p.proid;
        //preparamos un objeto request           
        var request = crud.crearRequest('proyectos',1,'listarProyecto');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        request.setData({proid:$scope.proid});
        crud.listar("/smdg",request,function(data){

            $scope.proyectoNuevo = JSON.parse(JSON.stringify(p));
            $scope.proyectoNuevo.prores = $scope.trabajadores2[$scope.proyectoNuevo.prores];
            $scope.proyectoNuevo.proini = new Date($scope.proyectoNuevo.proini);
            $scope.proyectoNuevo.profin = new Date($scope.proyectoNuevo.profin);

            for(var i = 0; i < data.data.length ;++i){
                data.data[i].pacini = new Date(data.data[i].pacini);
                data.data[i].pacfin = new Date(data.data[i].pacfin);
                data.data[i].pacres = fun1(data.data[i].pacres);
            }

            settingActividades.dataset = data.data;
            iniciarPosiciones(settingActividades.dataset);
            $scope.tablaActividades.settings(settingActividades);

            $('#modalNuevoProyecto').modal('show');

        },function(data){
            console.info(data);
        });
    };

    fun1 = function (ideres){
        return $scope.trabajadores2[ideres];
    };

    $scope.trabajadores2 = {};
    $scope.listarTrabajadores = function() {
        //preparamos un objeto request        
        var request = crud.crearRequest('trabajador',1,'listarTrabajadoresPorOrganizacionyTipo');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        request.setData({orgId:$rootScope.usuMaster.organizacion.organizacionID, traTip:""});
        crud.listar("/directorio",request,function(data){
            $scope.trabajadores = data.data;
            fun2();
            $scope.trabajadores2;
        },function(data){
            console.info(data);
        });
    };
    $scope.listarTrabajadores();

    fun2 = function (){

        for(var i = 0; i<$scope.trabajadores.length; ++i){
            $scope.trabajadores2[$scope.trabajadores[i].traId] = $scope.trabajadores[i];
        }

    };

    //FIN PROYECTOS

    //ACTIVIDADES

    var paramsActividades = {count: 5};
    var settingActividades = {counts: [], dataset:[]};

    $scope.tablaActividades = new NgTableParams(paramsActividades, settingActividades);

    $scope.nuevaActividad = {};

    $scope.prepararActividad = function() {
        // Jquery draggable
//        $('#modalNuevaActividad').draggable({
//            handle: ".modal-header"
//        });
        $('#modalNuevaActividad').modal('show');

    };

    $scope.agregarActividad = function() {

        var pacini = [$scope.nuevaActividad.pacini.getFullYear(), $scope.nuevaActividad.pacini.getMonth() + 1, $scope.nuevaActividad.pacini.getDate()].join('/');
        var pacfin = [$scope.nuevaActividad.pacfin.getFullYear(), $scope.nuevaActividad.pacfin.getMonth() + 1, $scope.nuevaActividad.pacfin.getDate()].join('/');

        //reiniciamos las variables        

        if($scope.proid !== 0){
            var request = crud.crearRequest('proyectos',1,'registrarActividades');
            request.setData({pacdes:$scope.nuevaActividad.pacdes,
                pacini:pacini,
                pacfin:pacfin,
                pacres:$scope.nuevaActividad.pacres.traId,
                proid:$scope.proid,
                pacava: 0
            });
            crud.insertar("/smdg",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                objeto = {
                    pacid:response.data.pacid,
                    pacdes:$scope.nuevaActividad.pacdes,
                    pacini:new Date(pacini),
                    pacfin:new Date(pacfin),
                    pacres:$scope.nuevaActividad.pacres,
                    pacresid:$scope.nuevaActividad.pacres.traId,
                    pacava:0
                };
                insertarElemento(settingActividades.dataset, objeto);
                $scope.tablaActividades.reload();
                $scope.nuevaActividad = {};
                $('#modalNuevaActividad').modal('hide');
            },function(data){
                console.info(data);
            });
        }

        else{
            objeto = {
                pacdes:$scope.nuevaActividad.pacdes,
                pacini:new Date(pacini),
                pacfin:new Date(pacfin),
                pacres:$scope.nuevaActividad.pacres,
                pacresid:$scope.nuevaActividad.pacres.traId,
                pacava: 0
            };
            insertarElemento(settingActividades.dataset, objeto);
            $scope.tablaActividades.reload();
            $scope.nuevaActividad = {};
            $('#modalNuevaActividad').modal('hide');
        }

    };

    $scope.editarActividad = function(r) {
        //si estamso editando
        if(r.edi){
            //se actualiza       
            if(r.pacid !== undefined){

                modal.mensajeConfirmacion($scope,"seguro que desea actualizar esta actividad",function(){

                    var request = crud.crearRequest('proyectos',1,'actualizarActividades');

                    var fechaini =  [r.copia.pacini.getDate(), r.copia.pacini.getMonth()+1, r.copia.pacini.getFullYear()].join('/');
                    var fechafin =  [r.copia.pacfin.getDate(), r.copia.pacfin.getMonth()+1, r.copia.pacfin.getFullYear()].join('/');

                    request.setData({proid:$scope.proid, pacid: r.pacid, pacdes:r.copia.pacdes, pacini:fechaini, pacfin:fechafin ,pacres:r.copia.pacres.traId, pacava:r.copia.pacava}); //actividades:actividades

                    crud.actualizar("/smdg",request,function(response){

                        modal.mensaje("CONFIRMACION",response.responseMsg);
                        if(response.responseSta){
                            console.log("se actualizo");
                            r.i = r.copia.i;
                            r.pacdes = r.copia.pacdes;
                            r.pacini = r.copia.pacini;//new Date(pacini);
                            r.pacfin = r.copia.pacfin;//new Date(pacfin);
                            r.pacres = r.copia.pacres;
                            r.pacresid = r.copia.pacres.traId;
                            r.pacava = r.copia.pacava;

                            delete r.copia;
                            r.edi = false;
                        }
                    },function(data){
                        console.info(data);
                        return;
                    });
                });

            }
            else{
                r.i = r.copia.i;
                r.pacdes = r.copia.pacdes;
                r.pacini = r.copia.pacini;//new Date(pacini);
                r.pacfin = r.copia.pacfin;//new Date(pacfin);
                r.pacres = r.copia.pacres;
                r.pacresid = r.copia.pacres.traId;
                r.pacava = r.copia.pacava;

                delete r.copia;
                r.edi = false;
            }


        }
        //si queremos editar
        else{
            r.copia = JSON.parse(JSON.stringify(r));
            r.copia.pacini = r.pacini;
            r.copia.pacfin = r.pacfin;
            r.edi = true;
        }
    };

    $scope.eliminarActividad = function(i,r){
        //si estamso cancelando la edicion
        if(r.edi){
            r.edi = false;
            delete r.copia;
        }
        //si queremos eliminar el elemento
        else{
            if(r.pacid !== undefined){
                var request = crud.crearRequest('proyectos',1,'eliminarActividades');
                request.setData({proid:$scope.proid, pacid: r.pacid});

                crud.actualizar("/smdg",request,function(response){
                    modal.mensaje("CONFIRMACION",response.responseMsg);
                    if(response.responseSta){
                        //actualizando
                        eliminarElemento(settingActividades.dataset,i);
                        $scope.tablaActividades.reload();
                    }
                },function(data){
                    console.info(data);
                });
            }
            else{
                eliminarElemento(settingActividades.dataset,i);
                $scope.tablaActividades.reload();
            }
        }
        //        
    };

    //FIN ACTIVIDADES

    //RECURSOS

    var paramsRecursos = {count: 5};
    var settingRecursos = {counts: [], dataset:[]};

    $scope.tablaRecursos = new NgTableParams(paramsRecursos, settingRecursos);
    $scope.nuevoRecurso = {};

    $scope.actividadActual = 0;

    $scope.prepararRecursos = function(i, a){
        $scope.actividadActual = i;
        $scope.pacid = a.pacid;
        if($scope.tablaActividades.data[i].recursos === undefined ){
//            settingRecursos = {counts: [], dataset:[]};
//            $scope.tablaRecursos = new NgTableParams(paramsRecursos, settingRecursos);            
            settingRecursos.dataset = [];
            iniciarPosiciones(settingRecursos.dataset);
            $scope.tablaRecursos.settings(settingRecursos);
        }
        else{
//            settingRecursos.dataset = $scope.tablaActividades.data[i].recursos;
//            $scope.tablaRecursos = new NgTableParams(paramsRecursos, settingRecursos);            
            settingRecursos.dataset = $scope.tablaActividades.data[i].recursos;
            iniciarPosiciones(settingRecursos.dataset);
            $scope.tablaRecursos.settings(settingRecursos);
        }

//        $scope.disabled= true;        
//        $scope.trabajador = perId;
//        $scope.trabajador.i = i;
//        
//        //preparamos un objeto request        
//        var request = crud.crearRequest('parientes',1,'listarParientesPorTrabajador');
//        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
//        //y las funciones de exito y error
//        request.setData({traId:traId});
//        crud.listar("/directorio",request,function(data){
//            settingParientes.dataset = data.data;
//            //asignando la posicion en el arreglo a cada objeto
//            iniciarPosiciones(settingParientes.dataset);            
//            
////            settingParientes.dataset.forEach(function(item,index){
////                item.edi = false;
////            });            
//            $scope.tablaParientes.settings(settingParientes);                        
//        },function(data){
//            console.info(data);
//        });
//        
//        $scope.reiniciarModal();   

        $('#modalNuevoRecurso').modal('show');
    };

    $scope.agregarRecurso = function (){

        if($scope.pacid !== undefined){
            var request = crud.crearRequest('proyectos',1,'registrarRecursos');
            request.setData({recnom:$scope.nuevoRecurso.recnom,
                reccos:$scope.nuevoRecurso.reccos,
                pacid:$scope.pacid
            });
            crud.insertar("/smdg",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                objeto = {
                    recid: response.data.recid,
                    recnom:$scope.nuevoRecurso.recnom,
                    reccos:$scope.nuevoRecurso.reccos
                };
                insertarElemento(settingRecursos.dataset, objeto);
                $scope.tablaRecursos.reload();
                $scope.nuevoRecurso = {};
            },function(data){
                console.info(data);
            });
        }

        else{
            objeto = {
                recnom:$scope.nuevoRecurso.recnom,
                reccos:$scope.nuevoRecurso.reccos
            };
            insertarElemento(settingRecursos.dataset, objeto);
            $scope.tablaRecursos.reload();
            $scope.nuevoRecurso = {};
        }

    };

    $scope.editarRecurso = function (r){
        //si estamso editando
        if(r.edi){
            //se actualiza            
            if(r.recid !== undefined){
                modal.mensajeConfirmacion($scope,"seguro que desea actualizar este recurso",function(){

                    var request = crud.crearRequest('proyectos',1,'actualizarRecursos');

                    request.setData({recid:r.recid, pacid: $scope.pacid, recnom:r.copia.recnom, reccos:r.copia.reccos});

                    crud.actualizar("/smdg",request,function(response){

                        modal.mensaje("CONFIRMACION",response.responseMsg);
                        if(response.responseSta){
                            console.log("se actualizo");
                            r.i = r.copia.i;
                            r.recnom = r.copia.recnom;
                            r.reccos = r.copia.reccos;

                            delete r.copia;
                            r.edi = false;
                        }
                    },function(data){
                        console.info(data);
                        return;
                    });
                });
            }
            else{

                r.i = r.copia.i;
                r.recnom = r.copia.recnom;
                r.reccos = r.copia.reccos;

                delete r.copia;
                r.edi = false;

            }

        }
        //si queremos editar
        else{
            r.copia = JSON.parse(JSON.stringify(r));
            r.edi = true;
        }

    };

    $scope.eliminarRecurso = function (i,r){
        //si estamso cancelando la edicion
        if(r.edi){
            r.edi = false;
            delete r.copia;
        }
        //si queremos eliminar el elemento
        else{
            if(r.recid !== undefined){
                var request = crud.crearRequest('proyectos',1,'eliminarRecursos');
                request.setData({recid: r.recid});

                crud.actualizar("/smdg",request,function(response){
                    modal.mensaje("CONFIRMACION",response.responseMsg);
                    if(response.responseSta){
                        //actualizando
                        eliminarElemento(settingRecursos.dataset,i);
                        $scope.tablaRecursos.reload();
                    }
                },function(data){
                    console.info(data);
                });
            }
            else{
                eliminarElemento(settingRecursos.dataset,i);
                $scope.tablaRecursos.reload();
            }

        }
        //        
    };

    $scope.guardarRecursos = function (){

        $scope.tablaActividades.data[$scope.actividadActual].recursos = $scope.tablaRecursos.data;
//        
//        $scope.tablaRecursos.data;
//        $scope.actividadActual.recursos = $scope.tablaRecursos.data;
//        $scope.tablaRecursos.data = [];
//        $scope.tablaRecursos.reload();
//        delete $scope.tablaRecursos;
    };

    //FIN RECURSOS

    //Para las obsiones de reportes, los nombres de los campos sirven para hacer la consulta 
    //a la base de datos
    $scope.checkModel = {
        'p.dni': false,
        'p.ape_mat': false,
        'p.ape_pat': false,
        'p.nom':false,
        'p.per_dir':false,
        'p.email':false,
        'p.fij':false,
        'tc.crg_tra_nom':false,
        'o.nom':false
    };

    $scope.checkModel2 = {
        'p.dni': {0:'DNI',1:3},
        'p.ape_mat': {0:'Ape. Materno',1:3},
        'p.ape_pat': {0:'Ape. Paterno',1:3},
        'p.nom':{0:'Nombre',1:3},
        'p.per_dir':{0:'Direccion',1:5},
        'p.email':{0:'Email',1:4},
        'p.fij':{0:'Telefono',1:3},
        'tc.crg_tra_nom':{0:'Cargo',1:3},
        'o.nom':{0:'Institucion',1:4}
    };

    //variables para el reporte
    $scope.page = false; //horientacion de la pagina
    $scope.checkResults = [];
    $scope.checkWeight = [];
    $scope.checkTitles = [];
    //

    $scope.$watchCollection('checkModel', function () {
        $scope.checkResults = [];
        $scope.checkWeight = [];
        $scope.checkTitles = [];
        angular.forEach($scope.checkModel, function (value, key) {
            if (value) {
                $scope.checkResults.push(key);
                $scope.checkTitles.push($scope.checkModel2[key][0]);
            }
        });
    })
    //fin

    $scope.perId = 0;
    $scope.trabajador = 0;

    $scope.tipoParientes = [];

    $scope.disabled= true;

    $scope.directorioInterno = [];
    $scope.directorioTipo = "";
    $scope.pariente = {
        parId:0,
        perId:0,
        tpaId:0,
        parDni:"",
        parPat:"",
        parMat:"",
        parNom:"",
        parDir:"",
        parTel:"",
        tpaDes:""
    };

    //variable del tipo de trabajador(directivo, docente,..)
    $scope.tipo = "";

    $scope.listarProyectos = function(){

        var request = crud.crearRequest('proys',1,'listarProyectosDocente');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        request.setData({org:$rootScope.usuMaster.organizacion.organizacionID,doc:$rootScope.usuMaster.usuario.usuarioID});
        crud.listar("/maestro",request,function(data){
            settingPro.dataset = data.data;
            iniciarPosiciones(settingPro.dataset);
            $scope.tablaProyectos.settings(settingPro);
        },function(data){
            console.info(data);
        });
    }

    $scope.eliminarPariente = function(i,r, modo){

        //si estamso cancelando la edicion
        if(r.edi){
            r.edi = false;
            delete r.copia;
        }
        //si queremos eliminar el elemento
        else{

            modal.mensajeConfirmacion($scope,"seguro que desea eliminar este registro",function(){

                var request = crud.crearRequest('parientes',1,'eliminarPariente');
                request.setData({parId:r.parId, perId:$scope.trabajador});

                crud.actualizar("/directorio",request,function(response){

                    modal.mensaje("CONFIRMACION",response.responseMsg);
                    if(response.responseSta){
                        eliminarElemento(settingActividades.dataset,i);
                        $scope.tablaActividades.reload();
                    }
                },function(data){
                    console.info(data);
                });
            });
        }
        //        
    };

    $scope.prepararEditar = function(i,traId, perId){
        //$scope.funcionSel = JSON.parse(JSON.stringify(traId));
        //$scope.funcionSel.i = i;             
        $scope.disabled= true;
        $scope.trabajador = perId;
        $scope.trabajador.i = i;

        //preparamos un objeto request        
        var request = crud.crearRequest('parientes',1,'listarParientesPorTrabajador');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        request.setData({traId:traId});
        crud.listar("/directorio",request,function(data){
            settingActividades.dataset = data.data;
            //asignando la posicion en el arreglo a cada objeto
            iniciarPosiciones(settingActividades.dataset);

//            settingActividades.dataset.forEach(function(item,index){
//                item.edi = false;
//            });            
            $scope.tablaActividades.settings(settingActividades);
        },function(data){
            console.info(data);
        });

        $scope.reiniciarModal();
        $('#modalEditar').modal('show');
    };

    $scope.editarPariente = function(i,r,modo){
        //si estamso editando
        if(r.edi){
            //se actualiza            
            modal.mensajeConfirmacion($scope,"seguro que desea actualizar este registro",function(){

                var request = crud.crearRequest('parientes',1,'actualizarPariente');
                r.copia.perId = $scope.trabajador;
                request.setData(r.copia);

                crud.actualizar("/directorio",request,function(response){

                    modal.mensaje("CONFIRMACION",response.responseMsg);
                    if(response.responseSta){
                        console.log("se actualizo");
                        //eliminarElemento(settingActividades.dataset,i);
                        //$scope.tablaActividades.reload();
                        r.i = r.copia.i;
                        r.parDni = r.copia.parDni;
                        r.parPat = r.copia.parPat;
                        r.parMat = r.copia.parMat;
                        r.parNom = r.copia.parNom;
                        r.parDir = r.copia.parDir;
                        r.parTel = r.copia.parTel;
                        r.tpaDes = r.copia.tpaDes;
                        r.tpaId = r.copia.tpaId;
                        r.parId = r.copia.parId;
                        delete r.copia;
                    }

                },function(data){
                    console.info(data);
                    return;
                });
            });
            r.edi = false;
        }
        //si queremos editar
        else{
            r.copia = JSON.parse(JSON.stringify(r));
            r.edi = true;
        }
    };

    $scope.agregarPariente = function(pariente){

        var request = crud.crearRequest('parientes',1,'insertarPariente');

        pariente.perId = $scope.trabajador;
        request.setData(pariente);

        crud.insertar("/directorio",request,function(response){

            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                //recuperamos las variables que nos envio el servidor
                objeto = {
                    parId:response.data.parId,
                    tpaId:$scope.pariente.tpaId,
                    parDni:$scope.pariente.parDni,
                    parPat:$scope.pariente.parPat,
                    parMat:$scope.pariente.parMat,
                    parNom:$scope.pariente.parNom,
                    parDir:$scope.pariente.parDir,
                    parTel:$scope.pariente.parTel,
                    tpaDes:$scope.tipoParientes[$scope.pariente.tpaId - 1].tpaDes
                };
                insertarElemento(settingActividades.dataset,objeto);
                $scope.tablaActividades.reload();
                //reiniciamos las variables
                $scope.pariente = {
                    parId:0,
                    perId:0,
                    tpaId:0,
                    parDni:"",
                    parPat:"",
                    parMat:"",
                    parNom:"",
                    parDir:"",
                    parTel:"",
                    tpaDes:""
                };
                $scope.disabled= true;
            }
        },function(data){
            console.info(data);
        });
    };

    $scope.verificarDni = function(dni){
        //verificar el dni ingresado, para saber si existe en la tabla persona        
        var request = crud.crearRequest('persona',1,'buscarPersonaxDni');
        request.setData({perDni:dni});
        crud.listar("/directorio",request,function(data){
            // si existe una persona con ese DNI en la tabla Persona
            if(Object.keys(data.data).length != 0){

                $scope.pariente.parId = data.data.parId;
                $scope.pariente.parDni = dni;
                $scope.pariente.parPat = data.data.parPat;
                $scope.pariente.parMat = data.data.parMat;
                $scope.pariente.parNom = data.data.parNom;
                $scope.pariente.parDir = data.data.parDir;
                $scope.pariente.parTel = data.data.parTel;
            }
        },function(data){
            console.info(data);
        });
        //
        //habilitamos los campos bloqueados        
        $scope.disabled = false;
    };

    listarTipoParientes();

    function listarTipoParientes(){
        //preparamos un objeto request
        var request = crud.crearRequest('tipoPariente',1,'listarTipos');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/directorio",request,function(data){
            $scope.tipoParientes = data.data;
        },function(data){
            console.info(data);
        });
    };

    //Visualizar de archivos en formato PDF
    $scope.reporte = function () {

        $scope.checkWeight = [];
        for(var k=0;k<$scope.checkResults.length;++k){
            $scope.checkWeight.push($scope.checkModel2[$scope.checkResults[k]][1]);
        }

        var reporte = '';

        if($scope.tipo == 'Pa') reporte = 'reporteApoderados';
        else if ($scope.tipo == 'Es')   reporte = 'reporteEstudiantes';
        else    reporte = 'reporte';

        var objeto ={
            Organizacion:"II.EE Tupac Amaru II ILO",
            Director:"Jose Manuel Ramos Rondon",
            Nivel:"Primaria - Secundaria",
            Provincia:"ILO",
            Distrito:"Alto Mar"
        };

        var request = crud.crearRequest('trabajador',1,reporte);
        request.setData({campos:$scope.checkResults, titulos:$scope.checkTitles, pesos:$scope.checkWeight, traTip:$scope.tipo, orgId:$rootScope.usuMaster.organizacion.organizacionID, objeto:objeto, page:$scope.page});
        crud.listar("/directorio",request,function(data){
            $scope.dataBase64 = data.data[0].datareporte;
            window.open($scope.dataBase64);
        },function(data){
            console.info(data);
        });
    };
    //fin
    $scope.modalreporte = function(){

        $scope.checkModel = {
            'p.dni': false,
            'p.ape_mat': false,
            'p.ape_pat': false,
            'p.nom':false,
            'p.per_dir':false,
            'p.email':false,
            'p.fij':false,
            'tc.crg_tra_nom':false,
            'o.nom':false
        };

        if($scope.tipo === "Pa" || $scope.tipo === "Es")  $scope.ocultar = true;
        else $scope.ocultar = false;

        $('#modalReportes').modal('show');
    };

    $scope.reiniciarModal = function(){
        $scope.pariente = {
            parId:0,
            perId:0,
            tpaId:0,
            parDni:"",
            parPat:"",
            parMat:"",
            parNom:"",
            parDir:"",
            parTel:"",
            tpaDes:""
        };
    };

    $scope.cerrarModalPariente = function(){
        $scope.listarProyectos();
        $('#modalEditar').modal('show');
    };

}]);