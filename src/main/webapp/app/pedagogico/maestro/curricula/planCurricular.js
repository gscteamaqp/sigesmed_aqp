/**
 * Created by Administrador on 19/10/2016.
 */
/**
 * Created by Administrador on 13/10/2016.
 */
function PlanCurrService($log,$rootScope,crud){
    var tipoUnidad = [{id:1,tip:"u",nom:"Unidad de Aprendizaje"},{id:2,tip:"p",nom:"Proyecto de Aprendizaje"},{id:3,tip:"m",nom:"Modulo de Aprendizaje"}];
    this.listarTiposUnidades = function(){
        return tipoUnidad;
    }
    this.listarElemento = function(meth,data,succ,err){
        var request = crud.crearRequest('plan_anual',1,meth);
        request.setData(data);
        crud.listar('/maestro',request,succ,err);
    };
    this.guardarElemento = function(meth,data,succ,err){
        var request = crud.crearRequest('plan_anual',1,meth);
        request.setData(data);
        crud.insertar('/maestro',request,succ,err);
    };
    this.detalleElemento = function(fun,eleme,data,succ,err){
        var request = crud.crearRequest('plan_anual',1,'detallePlanAnual');
        request.setMetadataValue("func",fun);
        request.setMetadataValue("elem",eleme);
        request.setData(data);

        crud.insertar('/maestro',request,succ,err);
    };
    this.editarElemento = function(meth,data,succ,err){
        var request = crud.crearRequest('plan_anual',1,meth);
        request.setData(data);
        crud.actualizar('/maestro',request,succ,err);
    };
    this.editarElemento = function(scope,meth,data,succ,err){
        var request = crud.crearRequest(scope,1,meth);
        request.setData(data);
        crud.actualizar('/maestro',request,succ,err);
    };
    this.eliminarElemento = function(meth,data,succ,err){
        var request = crud.crearRequest('plan_anual',1,meth);
        request.setData(data);
        crud.eliminar('/maestro',request,succ,err);
    };
    this.eliminarElemento = function(scope,meth,data,succ,err){
        var request = crud.crearRequest(scope,1,meth);
        request.setData(data);
        crud.eliminar('/maestro',request,succ,err);
    };
    this.listarCompetenciasAreas = function(area,succ,err){
        var request = crud.crearRequest('unidades',1,'listarCompetenciasArea');
        request.setData({are:area});
        crud.listar('/maestro',request,succ,err);
    }
}
angular.module('app')
    .config(['$routeProvider',function($routeProvider){
        $routeProvider.when('/plan_curricular/:curr',{
            templateUrl:'pedagogico/maestro/curricula/detalle_plan_anual.html',
            controller:'detallePlanAnualCtrl',
            controllerAs:'ctrl'
        })
    }])
    .service('PlanCurrService',['$log','$rootScope','crud',PlanCurrService])
    .controller('planCurricular',['$log','$location','$rootScope','NgTableParams','modal','UtilAppServices','PlanCurrService','crud',function($log,$location,$rootScope,NgTableParams,modal,util,mService,crud){
        var self = this;
        self.curriculas = new NgTableParams({count:15},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });

        //self.listar = listar;
        listar();
        $rootScope.$on("ListarPlanesAnuales", function() {
            listar(); 
        });
        function listar(){
            self.showProgress = true;
            mService.listarElemento('listarPlanesAnuales',{org:$rootScope.usuMaster.organizacion.organizacionID, usr:$rootScope.usuMaster.usuario.usuarioID}, function(response){
                self.showProgress = false;
                if(response.responseSta){
                    self.curriculas.settings().dataset = response.data.list;
                    self.curriculas.reload();
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                self.showProgress = false;
                modal.mensaje('ERROR','El Servidor no responde');
            });
        }
        self.nuevo = function(){

            var resolve = {
                data : function () {
                    return {
                        edit: false
                    }
                }
            }
            var modalInstance = util.openModal('nuevo_plan_anual.html','nuevoPlanAnualCtrl','lg','ctrl',resolve);
            modalInstance.result.then(function(data){
                self.curriculas.settings().dataset.push(data);
                self.curriculas.reload();
            },function(cdata){});
        }
        self.editar = function(row){
            var resolve = {
                data : function () {
                    return {                        
                        edit: true,
                        plan: angular.copy(row)
                    }
                }
            }
            var modalInstance = util.openModal('nuevo_plan_anual.html','nuevoPlanAnualCtrl','lg','ctrl',resolve);
            modalInstance.result.then(function(data){
                angular.extend(row,data);
            },function(cdata){});
        }
        self.generarReporte = function(row){
            var request = crud.crearRequest('plan_anual',1,'reporteProgramacionAnual');
            request.setData({prog:row.cod});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    verDocumento(response.data.b64)
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){});
        }
        self.eliminar = function($event,row){
            util.openDialog('Eliminar Programacion ','¿Seguro que desea eliminar la programacion anual?',$event,
                function (response) {
                    var request = crud.crearRequest('plan_anual',1,'eliminarPlanAnual');
                    request.setData({cod:row.cod});
                    crud.eliminar('/maestro',request,function(response){
                        if(response.responseSta){
                             _.remove(self.curriculas.settings().dataset,function(item){
                                return row.cod === item.cod;
                             });
                            self.curriculas.reload();
                        }else {
                            modal.mensaje("ERROR",response.responseMsg);
                        }

                    },function(errResponse){
                        modal.mensaje("ERROR","El servidor no responde");
                    });
                },function () {
                    //modal.mensaje("CANCELAR","Se cancelo la accion");
                });
        }
        self.ver = function(row){
            console.log(row);
            $location.url('/plan_curricular/'+btoa(JSON.stringify(row)));
        }
    }])
    .controller('detallePlanAnualCtrl',['$log','$routeParams', '$scope', '$rootScope', 'NgTableParams','modal','UtilAppServices','PlanCurrService',function($log,$routeParams,$scope,$rootScope,NgTableParams,modal,util,mService){
        var self = this;
        self.prog = JSON.parse(atob($routeParams.curr));
        self.objetivos = {
            pr : new NgTableParams({count:15},{
                counts: [],
                paginationMaxBlocks: 13,
                paginationMinBlocks: 2,
                dataset:[]
            }),
            es : new NgTableParams({count:15},{
                counts: [],
                paginationMaxBlocks: 13,
                paginationMinBlocks: 2,
                dataset:[]
            })
        };
        self.tablaUnidades = new NgTableParams({count:15},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });
        self.tablaValores = new NgTableParams({count:15},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });
        self.objDisab = [{tip:'pr'},{tip:'es'}];
        var objSave = [];
        //self.editDes = true;
        listarObjetivos();
        self.nuevoObjetivo = function(tip){
            self.isSaving = true;
            self.objetivos[tip].settings().dataset.unshift({
                nom: "",
                edit:true,
                save:true,
                tip:tip
            });

            self.objetivos[tip].sorting({});
            self.objetivos[tip].page(1);
            self.objetivos[tip].reload();
        }
        self.eliminarObjetivo =  function($event,row,tip){
            util.openDialog('Quitar Objetivo','¿Seguro que desea quitar el objetivo?',$event,
                function (response) {
                    var data = {cod:row.cod,prog:self.prog.cod}
                    mService.detalleElemento('eliminar','obj',data,function(response){
                        if(response.responseSta){
                            _.remove(self.objetivos[tip].settings().dataset,function(o){
                                return o.cod === row.cod;
                            });
                            self.objetivos[tip].reload();
                        }else{
                            modal.mensaje('ERROR',response.responseMsg);
                        }
                    },function(errResponse){
                        modal.mensaje('ERROR','Error en el servidor');
                    });
                },function () {
                    //modal.mensaje("CANCELAR","Se cancelo la accion");
                });
        }
        function listarObjetivos(){
            var data = {mod:"prog-all",cod:self.prog.cod};
            mService.detalleElemento('listar','obj',data,function(response){
                if(response.responseSta){
                    objSave = angular.copy(response.data);
                    var a = _.partition(response.data,function(o){return o.tip === 'pr'});
                    self.objetivos.pr.settings().dataset = a[0];
                    self.objetivos.es.settings().dataset = a[1];

                    self.objetivos.pr.reload();
                    self.objetivos.es.reload();
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','Error en el servidor');
            });
        }

        self.editarObjetivo = function(row,tip){

            row.edit = true;
        }
        self.cancelarEdicionObjetivo = function(row,tip){
            if(row.save !== undefined && row.save){
                var index = _.remove(self.objetivos[tip].settings().dataset,function(o){
                    return o.save;
                });
                self.objetivos[tip].reload();
                self.isSaving = false;
                return;
            }
            var index = _.findIndex(objSave,function(o){
                return row.cod === o.cod;
            });
            angular.extend(row,objSave[index]);
            row.edit = false;
        }
        self.aceptarEdicionObjetivo = function(row,tip){
            if(!row.nom || row.nom ===""){
                modal.mensaje("Error", "El campo no puede estar vacio");
                return;
            }
            if(row.save !== undefined && row.save){
                self.showProgress = true;
                var data = {nom:row.nom,tip:tip,cod:self.prog.cod};
                mService.detalleElemento('registrar','obj',data,function(response){
                    self.showProgress = false;
                    if(response.responseSta){
                        angular.extend(row,response.data);
                        row.nom = row.nom.toUpperCase();
                        objSave.unshift(row);
                        row.edit = false;
                        row.save = false;
                        self.isSaving = false;
                    }else{
                        modal.mensaje('ERROR',response.responseMsg);
                    }
                },function(errResponse){
                    self.showProgress = false;
                    modal.mensaje('ERROR','Error en el servidor');
                });
                return;
            }
        }

        ///Valores y Actitudes///
        ////////////////////////////
        listarValores();
        self.agregarValor = agregarValor;
        self.eliminarValor = eliminarValor;
        function agregarValor(){
            var resolve = {
                data: function(){
                    return{
                        edit:false,
                        prog: self.prog
                    }
                }
            }
            var modalInstance = util.openModal('nuevo_valor.html','nuevoValorCtrl','lg','ctrl',resolve);
            modalInstance.result.then(function(data){
                self.tablaValores.settings().dataset.push(data);
                self.tablaValores.reload();
            },function(cdata){});
        }
        function editarValor(row){
            var resolve = {
                data: function(){
                    return{
                        edit:true,
                        prog: self.prog,
                        val: angular.copy(row)
                    }
                }
            }
            var modalInstance = util.openModal('nuevo_valor.html','nuevoValorCtrl','lg','ctrl',resolve);
            modalInstance.result.then(function(data){

            },function(cdata){});
        }      
        function listarValores(){
            var data = {cod:self.prog.cod};
            mService.detalleElemento('listar','val',data,function(response){
                if(response.responseSta){
                    self.tablaValores.settings().dataset = response.data;
                    self.tablaValores.reload();
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','Error en el servidor');
            });
        }
        function eliminarValor($event,row){
            var data = {prog:self.prog.cod,cod:row.cod};
            util.openDialog('Quitar el valor','¿Seguro que desea quitar el valor?',$event,
                function (response) {
                    //var data = {cod:row.cod,prog:self.prog.cod}
                    mService.detalleElemento('eliminar','val',data,function(response){
                        if(response.responseSta){
                            _.remove(self.tablaValores.settings().dataset,function(o){
                                return o.cod === row.cod;
                            });
                            self.tablaValores.reload();
                        }else{
                            modal.mensaje('ERROR',response.responseMsg);
                        }
                    },function(errResponse){
                        modal.mensaje('ERROR','Error en el servidor');
                    });
                },function () {
                    //modal.mensaje("CANCELAR","Se cancelo la accion");
                });
        }
        ////////////////////////////

        ///UNIDADES DIDACTICAS///
        ////////////////////////////
        listarUnidadesDidacticas();
        self.agregarUnidad = agregarUnidad;
        self.mostrarTipoUnidad = function(tip){
            switch(tip){
                case 'u':return 'Unidad de Aprendizaje';
                case 'm':return 'Modulo de Aprendizaje';
                case 'p':return 'Proyecto de Aprendizaje';
                default : return tip;
            }

        }
        function listarUnidadesDidacticas(){
            var data = {cod:self.prog.cod};
            mService.detalleElemento('listar','uni',data,function(response){
                if(response.responseSta){
                    self.tablaUnidades.settings().dataset = response.data;
                    self.tablaUnidades.reload();
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','Error en el servidor');
            });
        }

        function agregarUnidad(){
            var resolve = {
                data: function(){
                    return{
                        edit:false,
                        prog: self.prog
                    }
                }
            }
            mService.listarCompetenciasAreas(self.prog.are.id,function(response){
                if(response.responseSta){
                    var resolve = {
                        data: function(){
                            return{
                                edit:false,
                                prog: self.prog,
                                data : response.data
                            }
                        }
                    }
                    var modalInstance = util.openModal('nueva_unidad.html','nuevaUnidadCtrl','lg','ctrl',resolve);
                    modalInstance.result.then(function(data){
                        self.tablaUnidades.settings().dataset.push(data);
                        self.tablaUnidades.reload();
                    },function(cdata){});
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){});
        }
        self.editarUnidad = function(row){
            var resolve = {
                data: function(){
                    return{
                        edit:true,
                        unidad:angular.copy(row),
                        prog: self.prog
                    }
                }
            }
            var modalInstance = util.openModal('nueva_unidad.html','nuevaUnidadCtrl','lg','ctrl',resolve);
            modalInstance.result.then(function(data){
                angular.extend(row,data);
            },function(cdata){});
        }
        self.eliminarUnidad = function($event,row){
            util.openDialog('Eliminar Unidad Didactica','¿Seguro que desea eliminar la unidad didactica?',$event,
                function (response) {
                    mService.eliminarElemento('unidades','eliminarUnidadDidactica',{cod:row.cod},function(response){
                        if(response.responseSta){
                            _.remove(self.tablaUnidades.settings().dataset,function(item){
                                return item.cod === row.cod;
                            });
                            self.tablaUnidades.reload();
                        }else {
                            modal.mensaje("ERROR",response.responseMsg);
                        }

                    },function(errResponse){
                        modal.mensaje("ERROR","El servidor no responde");
                    });
                },function () {
                    //modal.mensaje("CANCELAR","Se cancelo la accion");
                });
        }
        ////////////////////////////

    }])
    .controller('nuevoPlanAnualCtrl',['$log','$uibModalInstance','$rootScope','modal','data','PlanCurrService','crud',function($log,$uibModalInstance,$rootScope,modal,data,mService,crud){
        var self = this;
        self.abrirCalendar = false;
        self.openCalendar = function () {self.abrirCalendar = true;}
        self.nivel_sel = null;
        self.dateOptions = {
            formatYear: 'yyyy',
            maxDate: new Date(2040, 5, 22),
//            maxDate: new Date(),
            minDate: new Date(),
            startingDay: 1,
            minMode: 'year',
            datepickerMode:'year'
        };
        self.years = _.range(2000,2051);
        self.titulo = !data.edit ? 'Nueva Programacion Curricular' : 'Editar Programacion Curricular';
        self.plan = !data.edit ? new Object():data.plan;
        self.cancelar = function () {
            $uibModalInstance.dismiss('cancel');
        }

        listarNiveles();
        function listarNiveles(){
            var request = crud.crearRequest('anecdotario',1,'listarGrados');
            request.setData({org:$rootScope.usuMaster.organizacion.organizacionID, doc:$rootScope.usuMaster.usuario.usuarioID});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.niveles = response.data.niveles;
                    if(data.edit){
                        self.plan.nom = data.plan.nom;
                        self.nivel = _.find(self.niveles,function(obj){
                            return obj.id == data.plan.grad.nivid;
                        });
                        self.mostrarGrados(self.nivel);
                    }
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
            });
        }
        self.mostrarGrados =  function(nivel){
            self.nivel_sel = nivel.id;
            self.grados = nivel.grados;
            if(data.edit){
                self.plan.grad = _.find(self.grados,function(obj){
                    return obj.id == data.plan.grad.id;
                });
                self.mostrarAreas(self.plan.grad);
            }
        }
        self.mostrarAreas = function(grado){
            var request = crud.crearRequest('acomp',1,'listarCursosDocente');
            request.setData({doc:$rootScope.usuMaster.usuario.usuarioID,org:$rootScope.usuMaster.organizacion.organizacionID,gra:grado.id,niv:self.nivel_sel});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.areas = response.data;
                    if(data.edit){
                        self.plan.are = _.find(self.areas,function(obj){
                            return obj.id == data.plan.are.id;
                        });
                    }
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){});
        }
        /*cargarDatos();
        function cargarDatos() {
            mService.listarElemento('listarComponentesPlan',{},function (response) {
                if(response.responseSta){
                    self.grados = response.data.grados;
                    self.areas = response.data.areas;
                    if(data.edit){
                        self.plan = data.plan;
                        self.plan.fec = new Date(data.plan.fec);
                    }
                }else{
                    $uibModalInstance.dismiss('cancel');
                    modal.mensaje('ERROR',response.responseMsg);

                }
            },function (errResponse) {
                $uibModalInstance.dismiss('cancel');
                modal.mensaje('ERROR','El servidor no responde');
            });
        }*/
        self.save = !data.edit ? guardar : editar;
        function editar(){ 
            if(self.plan.fec != undefined )
                self.plan.fec = self.plan.fec;
            var request = crud.crearRequest('plan_anual',1,'editarPlanAnual');
            request.setData(self.plan);
            crud.actualizar('/maestro',request,function (response) {
                if(response.responseSta){
                    $uibModalInstance.close(self.plan);
                }else{
                    $uibModalInstance.dismiss('cancel');
                    modal.mensaje('ERROR',response.responseMsg);

                }
                $rootScope.$emit("ListarPlanesAnuales", {});
            },function (errResponse) {
                self.plan.fec = new Date(self.plan.fec);
                $uibModalInstance.dismiss('cancel');
                modal.mensaje('ERROR','El servidor no responde');
            });
        }
        function guardar() {
            if(self.plan.are == null || self.plan.fec == null || self.plan.nom == null) {
                modal.mensaje('ERROR',"Llenar todos los campos.");
                return;
            }
            self.plan.org = $rootScope.usuMaster.organizacion.organizacionID;
            self.plan.usr = $rootScope.usuMaster.usuario.usuarioID;
            mService.guardarElemento('registrarPlanAnual',self.plan,function (response) {
                if(response.responseSta){
                    angular.extend(self.plan,response.data);
                    $uibModalInstance.close(self.plan);
                }else{
                    $uibModalInstance.dismiss('cancel');
                    modal.mensaje('ERROR',response.responseMsg);

                }
            },function (errResponse) {
                self.plan.fec = new Date(self.plan.fec);
                $uibModalInstance.dismiss('cancel');
                modal.mensaje('ERROR','El servidor no responde');
            });
        }
    }])
    .controller('nuevaUnidadCtrl',['$log','$uibModalInstance','$rootScope','modal','data','PlanCurrService','SectionTableParams','crud',function($log,$uibModalInstance,$rootScope,modal,data,mService,SectionTableParams,crud){
        var self = this;
        self.edit = data.edit;
        self.data = self.edit? null : data.data;

        self.titles = ["Competencias","Capacidades"];
        self.model = new SectionTableParams(self.titles,self.data);
        self.tipoUnidad = [{id:1,tip:"u",nom:"Unidad de Aprendizaje"},{id:2,tip:"p",nom:"Proyecto de Aprendizaje"},{id:3,tip:"m",nom:"Modulo de Aprendizaje"}];
        self.abrirIniCalendar = false;
        self.abrirFinCalendar = false;
        self.titulo = self.edit ? 'Editar Unidad Didactica' : 'Registrar Nueva Unidad Didactica';
        self.uni = self.edit ? data.unidad : new Object();
        if(self.edit){
            self.uni.ini = new Date(self.uni.ini);
            self.uni.fin = new Date (self.uni.fin);
            self.uni.tip = _.find(self.tipoUnidad,function(o){
                return o.tip == self.uni.tip;
            });
        }
        self.openIniCalendar = function () {self.abrirIniCalendar = true;}
        self.openFinCalendar = function () {
            if(self.uni.ini !== undefined && self.uni.ini !== null){
                self.dateFinOptions.minDate = self.uni.ini;
                self.abrirFinCalendar = true;

            }else{
                modal.mensaje("Mensaje",'Seleccione una fecha de inicio')
            }
        }
        self.dateIniOptions = {
            formatYear: 'yyyy',
            maxDate: new Date(2050,12,12),
            minDate: new Date(),
            startingDay: 1
        };
        self.dateFinOptions = {
            formatYear: 'yyyy',
            maxDate: new Date(2050,12,12),
            minDate: new Date(),
            startingDay: 1
        };

        self.cancelar = function () {
            $uibModalInstance.dismiss('cancel');
        }
        self.save = self.edit ? editar : guardar;
        listarPeriodos();
        function listarPeriodos(){
            var request = crud.crearRequest('unidades',1,'listarPeriodosPlanEstudios');
            request.setData({org:$rootScope.usuMaster.organizacion.organizacionID, niv:data.prog.grad.nivid});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.periodos = response.data;
                    if(self.edit){
                        self.periodo = _.find(self.periodos,function(obj){
                            return obj.id == data.unidad.per;
                        });
                    }
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
            });
        }
        self.cambiarFechas = function(periodo){
            self.dateIniOptions.maxDate = new Date(periodo.fin);
            self.dateIniOptions.minDate = new Date(periodo.ini);
            self.dateIniOptions.initDate = new Date(periodo.ini);
            ///
            self.dateFinOptions.maxDate = new Date(periodo.fin);
            self.dateFinOptions.minDate = new Date(periodo.ini);
            self.dateFinOptions.initDate = new Date(periodo.ini);
        }
        function editar(){
            self.uni.tip = self.uni.tip.tip;
            self.uni.ini = self.uni.ini.getTime();
            self.uni.fin = self.uni.fin.getTime();
            mService.editarElemento('unidades','editarProductosUnidad',self.uni,function(response){
                if(response.responseSta){
                    $uibModalInstance.close(self.uni);
                }else{
                    modal.mensaje("ERROR",response.responseMsg);
                }
            },function(errResponse){});
        }
        function guardar() {
            self.showProgress = true;
            self.uni.cod = data.prog.cod;
            self.uni.tip = self.uni.tip.tip;
            self.uni.ini = self.uni.ini.getTime();
            self.uni.fin = self.uni.fin.getTime();
            self.uni.per = self.periodo.id;
            var comps = angular.copy(self.model.getData());

            angular.forEach(self.model.getData(),function(obj,keyComp){
                angular.forEach(obj.capacidades,function(obj,keyCap){
                    if(obj.selec === undefined || !obj.selec){
                        _.remove(comps[keyComp].capacidades,function(cap){
                            return cap.id === obj.id;
                        });
                    }
                });
            });
            _.remove(comps,function(comp){
                return comp.capacidades.length === 0;
            });
            self.uni.competencias = comps;
            mService.detalleElemento('registrar','uni',self.uni,function(response){
                self.showProgress = false;
                if(response.responseSta){
                    angular.extend(self.uni,response.data);
                    $uibModalInstance.close(self.uni);
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                self.showProgress = false;
                modal.mensaje('ERROR','Error en el servidor');
            });
        }
    }])
    .controller('nuevoValorCtrl',['$log','$uibModalInstance','$rootScope','modal','data','PlanCurrService',function($log,$uibModalInstance,$rootScope,modal,data,mService){
        var self = this;
        self.titulo = data.edit ? 'Editar Valor y Actitud' : 'Registrar Nuevo Valor y Actitud';
        self.val = data.edit ? data.val : new Object();
        self.cancelar = function () {
            $uibModalInstance.dismiss('cancel');
        }
        self.valores = [{id:1,nom:"Bondad"},
            {id:2,nom:"Autosuficiencia"},
            {id:3,nom:"Humildad"},
            {id:4,nom:"Respeto Mutuo"},
            {id:5,nom:"Amor"},
            {id:6,nom:"Justicia"},
            {id:7,nom:"Libertad"},
            {id:8,nom:"Coraje"},
            {id:9,nom:" Limpieza de cuerpo y mente"},{id:10,nom:"Honestidad / Integridad"},{id:11,nom:"DILIGENCIA"}];
        self.save = guardar;
        function guardar() {
            self.showProgress = true;
            self.val.cod = data.prog.cod;
            mService.detalleElemento('registrar','val',self.val,function(response){
                self.showProgress = false;
                if(response.responseSta){
                    angular.extend(self.val,response.data);
                    $uibModalInstance.close(self.val);
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                self.showProgress = false;
                modal.mensaje('ERROR','Error en el servidor');
            });
        }
    }]);
