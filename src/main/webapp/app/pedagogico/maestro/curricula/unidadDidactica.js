angular.module('app')
    .config(['$routeProvider',function($routeProvider){
        $routeProvider.when('/unidades_didacticas/:unid',{
            templateUrl:'pedagogico/maestro/curricula/detalle_unidad.html',
            controller:'detalleUnidadCtrl',
            controllerAs:'ctrl'
        })
    }])
    .controller('unidadDidactica',['$log','$location','$rootScope','NgTableParams','modal','UtilAppServices','crud','$q',function($log,$location,$rootScope,NgTableParams,modal,util,crud,$q){
        var self = this;
        self.planes = [];
        self.areas = [];
        self.grados = [];
        self.grados = [];
        self.window = [true,false];
        self.tipoUnidad = [{id:"u",title:"Unidad de Aprendizaje"},{id:"p",title:"Proyecto de Aprendizaje"},{id:"m",title:"Modulo de Aprendizaje"}];
        self.tablaUnidades = new NgTableParams({count:20},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });

        listarUnidades();
        function listarUnidades(){
            var data = {org:$rootScope.usuMaster.organizacion.organizacionID, usr:$rootScope.usuMaster.usuario.usuarioID}
            var request = crud.crearRequest('unidades',1,'listarUnidadesDidacticas');
            request.setData(data);
            crud.listar('/maestro',request,function(response){
                for(var i = 0; i < response.data.areas.length; i++){
                    var d = response.data.areas[i];
                    self.areas.push({id: d.nom,title: d.nom});
                }
                for(var i = 0; i < response.data.grados.length; i++){
                    var d = response.data.grados[i];
                    self.grados.push({id: d.nom,title: d.nom});
                }
                for(var i = 0; i < response.data.planes.length; i++){
                    var d = response.data.planes[i];
                    self.planes.push({id: d.nom,title: d.nom});
                }
                self.tablaUnidades.settings().dataset = response.data.list;
                self.tablaUnidades.reload();
            },function(errResponse){
            });
        }

        self.mostrarTipoUnidad = function(tip){
            var index = _.findIndex(self.tipoUnidad,function(o){
                return o.id === tip;
            });
            return self.tipoUnidad[index].title;

        }
        self.eliminarUnidad = function($event,row){
            util.openDialog('Eliminar Unidad Didactica','¿Seguro que desea eliminar la unidad didactica?',$event,
                function (response) {
                    var request = crud.crearRequest('unidades',1,'eliminarUnidadDidactica');
                    request.setData({cod:row.cod,tip:row.tip});
                    crud.eliminar('/maestro',request,function(response){
                        if(response.responseSta){
                            _.remove(self.tablaUnidades.settings().dataset,function(item){
                                return item.cod === row.cod;
                            });
                            self.tablaUnidades.reload();
                        }else {
                            modal.mensaje("ERROR",response.responseMsg);
                        }

                    },function(errResponse){
                        modal.mensaje("ERROR","El servidor no responde");
                    });
                },function () {
                    //modal.mensaje("CANCELAR","Se cancelo la accion");
                });
        }
        ///detalle unidad didactica
        self.ver = ver;
        function ver(row){
            $location.url('/unidades_didacticas/'+btoa(JSON.stringify(row)));
            /*self.currUni = row;
            _.fill(self.window,false);
            self.window[1] = true;*/

        }
    }])
    .controller('detalleUnidadCtrl',['$log','$routeParams','$rootScope','NgTableParams','modal','$location','UtilAppServices','crud',function($log,$routeParams,$rootScope,NgTableParams,modal,$location,util,crud){
        var self = this;
        self.unidad = JSON.parse(atob($routeParams.unid));
        self.titles = ["competencias","capacidades","Indicadores"];
        self.data = [{id:1,nom:"competencia 1"},{id:2,nom:"competencia 2"},{id:3,nom:"competencia 3"}];
        self.idSelectedComp = null;
        self.competenciasapr = [];
        self.editIndicadores = false;
        self.sequenciasDidacticas = [];
        self.indsUniSelec = [];
        listarAprendizajesEsperados();
        
        function listarAprendizajesEsperados(){
            var request = crud.crearRequest('unidades',1,'listarCompetenciasUnidad');
            request.setData({cod:self.unidad.cod});
            crud.listar('/maestro',request,function(response){
                /*self.aprendizajesEsperados.competencias.settings().dataset = response.data;
                self.aprendizajesEsperados.competencias.reload();*/
                if(response.responseSta){
                    self.competenciasapr = response.data;
                    angular.forEach(self.competenciasapr,function(c,key){
                        angular.forEach(c.capacidades,function(ca,key){
                            angular.forEach(ca.indicadores,function(i,key){
                                if(i.sele !== null && i.sele){
                                    self.indsUniSelec.push({com:c.id,cap:ca.id,ind:i.id});
                                }
                            });
                        });
                    });
                }
  
            //    self.competenciasapr = response.data;
            },function(errResponse){});
        }
        self.quitarAprendizajeEsperado = function($event,tip,idCom,idCap){
            var nomtit = tip === 'comp' ? 'Competencia' : 'Capacidad';
            util.openDialog('Eliminar Aprendizaje Esperado','¿Seguro que desea eliminar la ' + nomtit + ' seleccionada?',$event,
                function (response) {
                    var request = crud.crearRequest('unidades',1,'eliminarCompetenciaUnidad');
                    request.setData({tip:tip,unid:self.unidad.cod,comp:idCom,cap:idCap});
                    crud.eliminar('/maestro',request,function(response){
                        if(response.responseSta){
                            if(tip == 'comp'){
                                //self.competenciasapr
                                _.remove(self.competenciasapr,function(o){
                                    return o.id == idCom;
                                });
                                _.remove(self.competenciasSesapr,function(o){
                                    return o.id == idCom;
                                });
                            }else if(tip == 'cap'){
                                var index = _.findIndex(self.competenciasapr,function(o){
                                    return o.id == idCom;
                                });
                                var indexCap = _.findIndex(self.competenciasapr[index].capacidades,function(o){
                                    return o.id == idCap;
                                });
                                self.competenciasapr[index].numInd = self.competenciasapr[index].numInd - self.competenciasapr[index].capacidades[indexCap].indicadores.length;
                                self.competenciasapr[index].capacidades.splice(indexCap, 1);
                            }
                        }else{
                            modal.mensaje('ERROR',response.responseMsg);
                        }
                    },function(errResponse){modal.mensaje('ERROR','El servidor no responde');});
                },function () {
                    //modal.mensaje("CANCELAR","Se cancelo la accion");
                });
        }
        var indicadoresSeleccionadosUni= [];
        var indicadoresEliminadosUni = [];
        self.agregarIndicadorUnidad = function(comp,cap,ind){
            //
            if(ind.sele){
                //eliminar si existe de los eliminados
                _.remove(indicadoresEliminadosUni,function(o){
                    return o.ind === ind.id;
                });
                indicadoresSeleccionadosUni.push({com:comp.id,cap:cap.id,ind:ind.id});
                /*var objAux = _.find(indicadoresSeleccionadosUni,function(o){
                    return o.ind === ind.id
                });
                if(objAux === undefined){
                    indicadoresSeleccionadosUni.push({com:comp.id,cap:cap.id,ind:ind.id});
                }*/
            }else{
                _.remove(indicadoresSeleccionadosUni,function(o){
                    return o.ind === ind.id;
                });
                indicadoresEliminadosUni.push({com:comp.id,cap:cap.id,ind:ind.id});
            }
            //
            $log.log('Indicadores Agregados',indicadoresSeleccionadosUni);
            $log.log('Indicadores Eliminados',indicadoresEliminadosUni);
        }
        self.registrarIndicadores = function(){
            var request = crud.crearRequest('unidades',1,'registrarIndicadoresUnidad');
            request.setData({idUnidad:self.unidad.cod,nuevos:indicadoresSeleccionadosUni,eliminados:indicadoresEliminadosUni});
            crud.insertar('/maestro',request,function(response){
                if(response.responseSta){
                    modal.mensaje('Exito',response.responseMsg);
                }else{
                    modal.mensaje('Error',response.responseMsg);
                }
                self.editIndicadores = false;
            },function(errRes){
                modal.mensaje('Error','El servidor no responde');
            });
        }
        self.agregarCompetencia = function(){
            var request = crud.crearRequest('unidades',1,'listarCompetenciasArea');
            request.setData({are:self.unidad.are.cod});
            crud.listar('/maestro',request,function(response){
                self.showProgress = false;
                if(response.responseSta){
                    var resolve =  {
                        data : function(){
                            return{
                                edit: false,
                                data : response.data,
                                unidad: self.unidad
                            }
                        }
                    }
                    var modalInstance = util.openModal('aprendizajes_esperados.html','aprendizajesEsperadosCtrl','lg','ctrl',resolve);
                    modalInstance.result.then(function(respData){
                        angular.forEach(respData,function(obj,key){
                            var indexComp = _.findIndex(self.competenciasapr,function(o){
                                return o.id == obj.id;
                            });
                            if(indexComp == -1){
                                //no existe entonces insertamos
                                self.competenciasapr.push(obj);
                            }else{
                                //existe entonces solo añadimos las competencias
                                var sumInd = 0;
                                angular.forEach(obj.capacidades,function(obj,key){
                                    sumInd += obj.indicadores.length;
                                });
                                $log.log('suma de indicadoresw',sumInd);
                                self.competenciasapr[indexComp].numInd += sumInd;

                                self.competenciasapr[indexComp].capacidades.push.apply(self.competenciasapr[indexComp].capacidades,obj.capacidades);
                                $log.log('competenciasaa',self.competenciasapr[indexComp]);
                            }
                        });
                    },function(){});
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                self.showProgress = false;
                modal.mensaje('ERROR','El Servidor no responde');
            });
        }

        /// materiales y productos de unidad

        self.tablaMaterialesUnidad =  new NgTableParams({count:10},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });
        self.saveProdUnidad = false;
        /*self.productosUnidad = [{id:1,nom:"Panel informativo sobre la importancia de una alimentación sana",edit:false},
            {id:2,nom:"Panel 2 informativo sobre la importancia de una alimentación sana",edit:false}];*/
        self.productosUnidad = [];
        listarMateriales();
        function listarMateriales(){
            var request = crud.crearRequest('unidades',1,'listarMaterialUnidad');
            request.setData({unid:self.unidad.cod});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.tablaMaterialesUnidad.settings().dataset = response.data.mats;
                    self.productosUnidad = response.data.prods;
                    self.tablaMaterialesUnidad.reload();
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){modal.mensaje('ERROR','El servidor no responde');});
        }
        self.editarMaterialUnidad = function(row){
            var resolve =  {
                data : function(){
                    return{
                        edit: true,
                        material : angular.copy(row)
                    }
                }
            }
            var modalInstance = util.openModal('material_recurso_unidad.html','materialUnidadCtrl','sm','ctrl',resolve);

            modalInstance.result.then(function(material){
                angular.extend(row,material);
            },function(){});
        }
        self.eliminarMaterial = function($event,row){
            util.openDialog('Eliminar Material','¿Seguro que desea eliminar el material?',$event,
                function (response) {
                    var request = crud.crearRequest('unidades',1,'eliminarMaterialesUnidad');
                    request.setData({id:row.id});
                    crud.eliminar('/maestro',request,function(response){
                        if(response.responseSta){
                            _.remove(self.tablaMaterialesUnidad.settings().dataset,function(o){
                                return o.id == row.id;
                            })
                            self.tablaMaterialesUnidad.reload();
                        }else{
                            modal.mensaje('ERROR',response.responseMsg);
                        }
                    },function(errResponse){modal.mensaje('ERROR','El servidor no responde');});
                },function () {
                    //modal.mensaje("CANCELAR","Se cancelo la accion");
            });
        }
        self.agregarMaterialUnidad = function(){
            var resolve =  {
                data : function(){
                    return{
                        edit: false,
                        unidad: self.unidad
                    }
                }
            }
           
            var modalInstance = util.openModal('material_recurso_unidad.html','materialUnidadCtrl','sm','ctrl',resolve);
            $('#material_recurso_unidad').modal({backdrop: 'static', keyboard: false});
            modalInstance.result.then(function(material){
                $log.log('material',material);
                self.tablaMaterialesUnidad.settings().dataset.push(material);
                self.tablaMaterialesUnidad.reload();
            },function(){});
        }

        self.agregarProductosUnidad = function(){
            self.saveProdUnidad = true;
            self.productosUnidad.push({edit:true,save:true,nom:''});
        }
        self.guardarProductosUnidad = function(){
            var prodAux= self.productosUnidad[self.productosUnidad.length - 1];
            if( prodAux.nom !== ''){
                prodAux.unid = self.unidad.cod;
                prodAux.untip = self.unidad.tip;
                var request = crud.crearRequest('unidades',1,'registrarProductosUnidad');
                request.setData(prodAux);
                crud.insertar('/maestro',request,function(response){
                    if(response.responseSta){
                        prodAux.edit = false;
                        prodAux.save = false;
                    }else{
                        modal.mensaje('ERROR',response.responseMsg);
                    }
                },function(errResponse){modal.mensaje('ERROR','El servidor no responde');});
            }else{
                self.productosUnidad.pop();
                modal.mensaje('ERROR','Debe definir el producto');
            }
            self.saveProdUnidad = false;

        }
        self.eliminarProductosUnidad = function($event,pro){
            util.openDialog('Eliminar Producto','¿Seguro que desea eliminar el producto?',$event,
                function (response) {
                    var request = crud.crearRequest('unidades',1,'eliminarProductosUnidad');
                    request.setData({id:pro.id});
                    crud.eliminar('/maestro',request,function(response){
                        if(response.responseSta){
                            _.remove(self.productosUnidad,function(o){
                                return o.id == pro.id;
                            })
                        }else{
                            modal.mensaje('ERROR',response.responseMsg);
                        }
                    },function(errResponse){modal.mensaje('ERROR','El servidor no responde');});
                },function () {
                    //modal.mensaje("CANCELAR","Se cancelo la accion");
                });
        }
        /// sesiones de aprendizaje

        self.status = [{open:false},{open:false},{open:false},{open:false}];
        self.showSesiones = true;
        self.currentSession = new Object();
        self.indsSesSelec = [];
        self.sesionesUnidad=  new NgTableParams({count:10},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });
        listarSesiones();
        function listarSesiones(){
            var request = crud.crearRequest('sesiones',1,'listarSesiones');
            request.setData({unid:self.unidad.cod});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.sesionesUnidad.settings().dataset = response.data;
                    self.sesionesUnidad.reload();
                }else{
                    modal.mensaje('Error',response.responseMsg);
                }
            },function(errRes){
                modal.mensaje('Error','El servidor no responde');
            });
        }
        self.competenciasSesapr = [];
        function listarAprendizajesEsperadosUnidad(idSesion){
            var request = crud.crearRequest('sesiones',1,'listarAprendizajesUnidad');
            request.setData({unid:self.unidad.cod,sesid:idSesion});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.competenciasSesapr = response.data;
                    angular.forEach(self.competenciasSesapr,function(c,key){
                        angular.forEach(c.capacidades,function(ca,key){
                            angular.forEach(ca.indicadores,function(i,key){
                                if(i.sele !== null && i.sele){
                                    self.indsSesSelec.push({com:c.id,cap:ca.id,ind:i.id});
                                }
                            });
                        });
                    });
                   // self.competenciasEvaapr =  self.competenciasSesapr.slice();
                   // self.competenciasEvaapr = angular.copy(self.competenciasSesapr);
                    
                }else{
                    modal.mensaje('Error',response.responseMsg);
                }
            },function(errRes){
                modal.mensaje('Error','El servidor no responde');
            });
        }
        
        
        self.competenciasEva = [];
        self.indsEvSelec = [];
        self.listarAprendizajesUnidad = function(){
            var request = crud.crearRequest('sesiones',1,'listarAprendizajesEvaluacion');
            request.setData({unid:self.unidad.cod});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.competenciasEva = response.data;
                    angular.forEach(self.competenciasEva,function(c,key){
                        angular.forEach(c.capacidades,function(ca,key){
                            angular.forEach(ca.indicadores,function(i,key){
                                if(i.sele !== null && i.sele){
                                    self.indsEvSelec.push({com:c.id,cap:ca.id,ind:i.id});
                                }
                            });
                        });
                    });
                }
            });
        }
        
        self.agregarSesionAprendizaje = function(){
            var resolve =  {
                data : function(){
                    return{
                        edit: false,
                        unidad: self.unidad
                    }
                }
            }
            var modalInstance = util.openModal('sesion_aprendizaje.html','sesionAprendizajeCtrl','sm','ctrl',resolve);

            modalInstance.result.then(function(sesion){
                self.sesionesUnidad.settings().dataset.push(sesion);
                self.sesionesUnidad.reload();
            },function(){});
        }
        self.mostrarNivelAvance = function(est){
            switch (est){
                case 'A': return 'Activo';
                case 'F': return 'Finalizado';
                case 'I': return 'Inactivo';
            }
        }
        self.eliminarSesionAprendizaje = function($event,row){
            util.openDialog('Eliminar Sesion de Aprendizaje','¿Seguro que desea eliminar la sesion?',$event,
                function (response) {
                    var request = crud.crearRequest('sesiones',1,'eliminarSesionAprendizaje');
                    request.setData({id:row.id});
                    crud.eliminar('/maestro',request,function(response){
                        if(response.responseSta){
                            _.remove(self.sesionesUnidad.settings().dataset,function(o){
                                return o.id == row.id;
                            })
                            self.sesionesUnidad.reload();
                        }else{
                            modal.mensaje('ERROR',response.responseMsg);
                        }
                    },function(errResponse){modal.mensaje('ERROR','El servidor no responde');});
                },function () {
                    //modal.mensaje("CANCELAR","Se cancelo la accion");
                });
        }
        self.editarSesionAprendizaje = function(row){
            var resolve =  {
                data : function(){
                    return{
                        edit: true,
                        sesion:angular.copy(row),
                        unidad:self.unidad
                    }
                }
            }
            var modalInstance = util.openModal('sesion_aprendizaje.html','sesionAprendizajeCtrl','sm','ctrl',resolve);

            modalInstance.result.then(function(sesion){
                angular.extend(row,sesion);
            },function(){});
        }
        self.desarrollarSesion = function(row){
            self.showSesiones = !self.showSesiones;
            self.currentSession = row;
            self.indsSesSelec = [];
            listarAprendizajesEsperadosUnidad(self.currentSession.id);
            listarSecuenciasDidacticas(self.currentSession.id);
            listarTareasSesion(self.currentSession.id);
        }
        self.backSesionList = function(){
            self.showSesiones = true;
            self.editIndicadoresSes = false;
        }
        self.finalizarSesion = function(){
            var request = crud.crearRequest('sesiones',1,'finalizarSesion');
            request.setData({id: self.currentSession.id}); 
            crud.insertar('/maestro',request,function(response){
                if(response.responseSta){
                    self.currentSession.est = 'F';
                    if(response.data != undefined && response.data.sig != undefined){
                        var index = _.findIndex(self.sesionesUnidad.settings().dataset,function(obj){
                            return obj.id === response.data.sig;
                        });
                        if(index != -1){
                            self.sesionesUnidad.settings().dataset[index].est = 'A';
                        }
                    }
                    self.backSesionList();
                }else{
                    modal.mensaje('Error',response.responseMsg);
                }
                self.editIndicadoresSes = false;
            },function(errRes){
                modal.mensaje('Error','El servidor no responde');
            });
        }
        // ///Sesiones de aprendizaje -> Aprendizajes Esperados
        self.agregarIndicadorUnidadSes = function(comp,cap,ind){
            //self.indsSesSelec = [];
            if(ind.sele){
                self.indsSesSelec.push({com:comp.id,cap:cap.id,ind:ind.id});
            }
            else{
                _.remove(self.indsSesSelec,function(o){
                    return o.com === comp.id && o.cap === cap.id && o.ind === ind.id;
                });
            }
        }
        self.registrarIndicadoresSes = function(){

            var request = crud.crearRequest('sesiones',1,'registrarIndicadoresSesion');
            request.setData({sesId: self.currentSession.id,uniId:self.unidad.cod,uniTip:self.unidad.tip,apr:self.indsSesSelec});
            crud.insertar('/maestro',request,function(response){
                if(response.responseSta){
                    modal.mensaje('Exito',response.responseMsg);
                }else{
                    modal.mensaje('Error',response.responseMsg);
                }
                self.editIndicadoresSes = false;
            },function(errRes){
                modal.mensaje('Error','El servidor no responde');
            });

        }

        ///Sesiones de aprendizaje -> Secuencia Didactica
        self.agregarSequenciaDidactica = function(){
            var request = crud.crearRequest('sesiones',1,'listarSeccionesSequencia');
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    var resolve =  {
                        data : function(){
                            return{
                                edit: false,
                                seqSecTabs: response.data,
                                sesion :  self.currentSession
                            }
                        }
                    }
                    var modalInstance = util.openModal('sequencia_didactica.html','sequenciaDidacticaCtrl','sm','ctrl',resolve);

                    modalInstance.result.then(function(sequencia){
                        self.sequenciasDidacticas.push(sequencia);
                    },function(){});
                }else{
                    modal.mensaje('Error',response.responseMsg);
                }
            },function(errRes){
                modal.mensaje('Error','El servidor no responde');
            });


        }
        function listarSecuenciasDidacticas(idSesion){
            var request = crud.crearRequest('sesiones',1,'listarSecuenciasSesion');
            request.setData({ses:idSesion});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.sequenciasDidacticas = response.data;
                }else{
                    modal.mensaje('Error',response.responseMsg);
                }
            },function(errRes){
                modal.mensaje('Error','El servidor no responde');
            });

        }
        self.eliminarSequenciaDidactica = function($event,seq){

            util.openDialog('Eliminar Secuencia de Aprendizaje','¿Seguro que desea eliminar la secuencia?',$event,
                function (response) {
                    var request = crud.crearRequest('sesiones',1,'eliminarSecuenciaDidactica');
                    request.setData({id:seq.id});
                    crud.eliminar('/maestro',request,function(response){
                        if(response.responseSta){
                            _.remove(self.sequenciasDidacticas,function(o){
                                return o.id == seq.id;
                            })
                        }else{
                            modal.mensaje('ERROR',response.responseMsg);
                        }
                    },function(errResponse){modal.mensaje('ERROR','El servidor no responde');});
                },function () {
                    //modal.mensaje("CANCELAR","Se cancelo la accion");
                });
        }
        self.editarSequenciaDidactica = function(){
            $log.log('Se editara la secuencia');
        }

        //TAREAS SESION DE APRENDIZAJE
        self.tareasSesion =  new NgTableParams({count:10},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });
        function listarTareasSesion(idSesion){
            var request = crud.crearRequest('sesiones',1,'listarTareasSesion');
            request.setData({sesid:idSesion});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    $log.log('tareas',response.data);
                    self.tareasSesion.settings().dataset = response.data.tareas;
                    self.tareasSesion.reload();
                }else{
                    modal.mensaje('Error',response.responseMsg);
                }
            },function(errRes){
                modal.mensaje('Error','El servidor no responde');
            });
        }
        self.agregarTareaSesion = function(doc_id){
            
            // Secciones
            self.secciones = [];
            self.sec_sel;
            // OBTENEMOS EL PLAN DE ESTUDIOS ASOCIADO A LA PROGRAMACION ANUAL
            self.plan_id;
            self.tipo_tarea = [{id:'V',nombre:'tareas virtuales'},{id:'F',nombre:'tareas fisicas'}];
            var request = crud.crearRequest('cuadroHoras',1,'obtener_plan_estudios');
            request.setData({prog_id:self.unidad.prog.cod});
            crud.listar('/cuadroHoras',request,function(response){
                if(response.responseSta){
                    self.plan_id = response.data.plan_id;
                     
                    //////////////////////
                    request = crud.crearRequest('cuadroHoras',1,'buscarDistribucionPorDocente');
                    request.setData({planID:self.plan_id,docenteID:doc_id});
                    crud.listar('/cuadroHoras',request,function(response){
                    if(response.responseSta){
                        
                        for(var i = 0 ; i<response.data.length;i++){
                            if(response.data[i].gradoID==self.unidad.grad.cod){
                                var size_sec = response.data[i].secciones.length;
                            for(var j=0 ; j<size_sec;j++){
                                
                                 self.secciones.push(response.data[i].secciones[j].seccionID);
                            }
                            //////////////////////////////////
                            // OBTENER TIP_PER , NUM_PER obtener_periodos_plan
                            request = crud.crearRequest('cuadroHoras',1,'obtener_periodos_plan');
                            request.setData({unidadID:self.unidad.cod});
                            crud.listar('/cuadroHoras',request,function(response){
                                 if(response.responseSta){
                                     self.tip_per = response.data.tip_per;
                                     self.num_per = response.data.num_per;
                                     
                                        ///////////////////////////////
                            var resolve =  {
                                data : function(){
                                    return{
                                        edit: false,
                                        sesion: self.currentSession,
                                        ind_sel:self.indsSesSelec,
                                        unidad_didactica : self.unidad,
                                        planID :self.plan_id, 
                                        secciones : self.secciones,
                                        docente_id : doc_id,
                                        tip_per : self.tip_per,
                                        num_per : self.num_per,
                                        tipo : self.tipo_tarea,
                                        org : $rootScope.usuMaster.organizacion.organizacionID
                                    }
                                }
                            }
                                var modalInstance = util.openModal('registrar_tarea_sesion_aprendizaje.html','registrarTareaSesionCtrl','sm','ctrl',resolve);

                                modalInstance.result.then(function(tarea){
                                    self.tareasSesion.settings().dataset.push(tarea);
                                    self.tareasSesion.reload();
                                },function(){});

                            ///////////////////////////////

                                 }
                            });
   
                            break;
                            }
                        }
                        
                    }else{
                        modal.mensaje('Error',response.responseMsg);
                    }
                });
                    
                    
                    
                    //////////////////////
                    
                }
                else{
                    modal.mensaje('Error',response.responseMsg);
                }
                
                 },function(errRes){
                modal.mensaje('Error','El servidor no responde');
            });
            
            
            

            
        }
        self.verTareaSesion = function($event,row){
            
            
            
        }
        
        
        self.eliminarTareaSesion = function($event,row){
            util.openDialog('Eliminar Tarea ','¿Seguro que desea eliminar la tarea?',$event,
                function (response) {
                    var request = crud.crearRequest('sesiones',1,'eliminarTareaSesion');
                    request.setData({id:row.id});
                    crud.eliminar('/maestro',request,function(response){
                        if(response.responseSta){
                            _.remove(self.tareasSesion.settings().dataset,function(item){
                                return row.id === item.id;
                            });
                            self.tareasSesion.reload();
                        }else {
                            modal.mensaje("ERROR",response.responseMsg);
                        }

                    },function(errResponse){
                        modal.mensaje("ERROR","El servidor no responde");
                    });
                },function () {
                    //modal.mensaje("CANCELAR","Se cancelo la accion");
                });
        }
        self.verTareaWeb = function(row){
            
            self.tarea_web = {
               grad_id :  self.unidad.grad.cod,
               sec_id  :  row.sec ,
               nom     :  row.nom,
               are_id  :  self.unidad.are.cod,
               ses_id  :  self.currentSession,
               tip_per :  self.tip_per,
               num_per :  self.num_per,
               
               
            };
              
              localStorage.setItem('tarea_maestro', window.btoa(JSON.stringify(self.tarea_web)) );
              $location.path('tareas');

            
        }
        
        self.EditarTareaWeb = function(row){
            
            row.fec =  convertirFecha(row.fec);
            row.fec_ant = convertirFecha(row.fec_ant);
            
            var request = crud.crearRequest('sesiones',1,'editarTarea');
            request.setData(row);
            crud.actualizar('/maestro',request,function(response){
                if(response.responseSta){
                    row.edi = false;
                    modal.mensaje('Se actualizo la Tarea Correctamente');
                    
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){modal.mensaje('ERROR','El servidor no responde');});
            
  
        }
        
        self.prepararEditar = function(row){
            row.fec = new Date(row.fec);
            row.fec_ant = row.fec;
            row.edi = true;
        }
        self.convertirfech = function(row){
            row.fec= convertirfech(row.fec);
        }
        
        self.enableTarea = function(row){
            row.edi = false;
        }

        self.prepararEvaEditar = function(row){
         
            row.fec = new Date(row.fec);
            row.fec_ant = row.fec;
            self.minDateInicio = new Date();
            row.edi = true;
        }
       
        // VISUALIZAR EVALUACION EN EL MODULO WEB
        self.verEvaluacion = function(row){
             self.evaluacion_web = {
               grad_id :  self.unidad.grad.cod,
               sec_id  :  row.sec ,
               are_id  :  self.unidad.are.cod,
               nom     :  row.nom,
               tip_per :  self.tip_per,
               num_per :  self.num_per
            };
            
            localStorage.setItem('evaluacion_maestro', window.btoa(JSON.stringify(self.evaluacion_web)) );
            $location.path('evaluaciones');
        }

        //EVALUACIONES UNIDAD DIDACTICA
        self.evaluacionesUnidad =  new NgTableParams({count:10},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });

        self.tiposEvaluaciones = [{id:'1',nom:'Cuestionario'},{id:'2',nom:'Evaluacion en clase'},{id:'3',nom:'Evaluacion oral'}];
        listarEvaluacionesUnidad();
        function listarEvaluacionesUnidad(){
            var request = crud.crearRequest('unidades',1,'listarEvaluacionesUnidad');
            request.setData({uni:self.unidad.cod});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.evaluacionesUnidad.settings().dataset = response.data;
                    self.evaluacionesUnidad.reload();
                }else{
                    modal.mensaje('Error',response.responseMsg);
                }
            },function(errRes){
                modal.mensaje('Error','El servidor no responde');
            });
        }
        self.mostrarTipoEvaluacionUnidad = function(tip){
            var tipEvaUni = _.find(self.tiposEvaluaciones,function(obj){
                return tip == obj.id;
            });
            return tipEvaUni.nom;
        }
        self.agregarEvaluacionUnidad = function(doc_id){
            
            // Secciones
            self.secciones = [];
            self.sec_sel;
            // OBTENEMOS EL PLAN DE ESTUDIOS ASOCIADO A LA PROGRAMACION ANUAL
            self.plan_id;
            var request = crud.crearRequest('cuadroHoras',1,'obtener_plan_estudios');
            request.setData({prog_id:self.unidad.prog.cod});
            crud.listar('/cuadroHoras',request,function(response){
                if(response.responseSta){
                    self.plan_id = response.data.plan_id;  
                    //////////////////////
                    request = crud.crearRequest('cuadroHoras',1,'buscarDistribucionPorDocente');
                    request.setData({planID:self.plan_id,docenteID:doc_id});
                    crud.listar('/cuadroHoras',request,function(response){
                    if(response.responseSta){
                        for(var i = 0 ; i<response.data.length;i++){
                            if(response.data[i].gradoID==self.unidad.grad.cod){
                                var size_sec = response.data[i].secciones.length;
                                for(var j=0 ; j<size_sec;j++){
                                    self.secciones.push(response.data[i].secciones[j].seccionID);
                                }
                            }
                        }
                        
                        
                        var resolve =  {
                            data : function(){
                                return{
                                    edit: false,
                                    unidad: self.unidad,
                                    tiposEvaluaciones: self.tiposEvaluaciones,
                                    secciones : self.secciones,
                                    planID :self.plan_id,
                                    org : $rootScope.usuMaster.organizacion.organizacionID
                                }
                            }
                        }
                        var modalInstance = util.openModal('registrar_evaluacion_unidad_didactica.html','registrarEvaluacionUnidadCtrl','sm','ctrl',resolve);

                        modalInstance.result.then(function(evaluacion){
                            self.evaluacionesUnidad.settings().dataset.push(evaluacion);
                            self.evaluacionesUnidad.reload();
                        },function(){});
                       
                        }
                        });
                }
            });
            
          
        }
        /*
        self.editarEvaluacionUnidad = function(row){
            var resolve =  {
                data : function(){
                    return{
                        edit: true,
                        tiposEvaluaciones: tiposEvaluaciones,
                        evaluacion: angular.copy(row)
                    }
                }
            }
            var modalInstance = util.openModal('registrar_evaluacion_unidad_didactica.html','registrarEvaluacionUnidadCtrl','sm','ctrl',resolve);

            modalInstance.result.then(function(evaluacion){
                angular.extend(row,evaluacion);
            },function(){});
        }
        */
       self.editarEvaluacionUnidad = function(row){
            row.fec =  convertirFecha(row.fec);
            row.fec_ant = convertirFecha(row.fec_ant);
            
            var request = crud.crearRequest('unidades',1,'editarEvaluacionesUnidad');
            request.setData(row);
            crud.actualizar('/maestro',request,function(response){
                if(response.responseSta){
                    row.edi = false;
                    modal.mensaje('Se actualizo la Evaluacion Correctamente');
                    
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){modal.mensaje('ERROR','El servidor no responde');});
            
            
       }
        
        
        
        self.eliminarEvaluacionUnidad = function($event,row){
            util.openDialog('Eliminar Evaluacion ','¿Seguro que desea eliminar la evaluacion?',$event,
                function (response) {
                    var request = crud.crearRequest('unidades',1,'eliminarEvaluacionesUnidad');
                    request.setData({id:row.id});
                    crud.eliminar('/maestro',request,function(response){
                        if(response.responseSta){
                            _.remove(self.evaluacionesUnidad.settings().dataset,function(item){
                                return row.id === item.id;
                            });
                            self.evaluacionesUnidad.reload();
                        }else {
                            modal.mensaje("ERROR",response.responseMsg);
                        }

                    },function(errResponse){
                        modal.mensaje("ERROR","El servidor no responde");
                    });
                },function () {
                    //modal.mensaje("CANCELAR","Se cancelo la accion");
                });
        }

    }]).controller('registrarEvaluacionUnidadCtrl',['$log','$uibModalInstance','modal','crud','data','$rootScope',function($log,$uibModalInstance,modal,crud,data,$rootScope){
        var self = this;
        self.secc = data.secciones;
        self.abrirCalendar = false;
        self.openCalendar = function () {self.abrirCalendar = true;}        
        self.dateOptions = {
            formatYear: 'yyyy',
            maxDate: new Date(2040, 5, 22),
            minDate: new Date(1980, 0, 1),
            startingDay: 1
        };
        self.tiposEvaluaciones = data.tiposEvaluaciones;
        self.titulo = data.edit ? 'Editar Evaluacion  de Unidad de Aprendizaje' : 'Nueva Evaluacion de Unidad de Aprendizaje';
        self.eva  = data.edit ? data.evaluacion : new Object();
        if(data.edit){
            self.eva.fec = new Date(self.eva.fec);
            self.eva.tip = _.find(self.tiposEvaluaciones, function (obj) {
               return  obj.id == self.eva.tip;
            });
        }
        self.save = data.edit ? editar : guardar;
        function guardar(){
            self.eva.uni = data.unidad.cod;
            self.eva.tipUni = data.unidad.tip;
            self.eva.tip = self.eva.tip.id;
            self.eva.planID = data.planID;
            self.eva.gradoID = data.unidad.grad.cod; 
            self.eva.areaID = data.unidad.are.cod;
            self.eva.usuMod = $rootScope.usuMaster.usuario.usuarioID;
            self.eva.org = data.org
            if(self.eva.fec != undefined)
                self.eva.fec = self.eva.fec.getTime();

            var request = crud.crearRequest('unidades',1,'registrarEvaluacionesUnidad');
            request.setData(self.eva);
            crud.insertar('/maestro',request,function(response){
                if(response.responseSta){
                    angular.extend(self.eva,response.data);
                    $uibModalInstance.close(self.eva);
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){modal.mensaje('ERROR','El servidor no responde');});

        }
        function editar(){
            self.eva.tip = self.eva.tip.id;

            if(self.eva.fec != undefined)
                self.eva.fec = self.eva.fec.getTime();


            var request = crud.crearRequest('unidades',1,'editarEvaluacionesUnidad');
            request.setData(self.eva);
            crud.actualizar('/maestro',request,function(response){
                if(response.responseSta){
                    $uibModalInstance.close(self.eva);
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){modal.mensaje('ERROR','El servidor no responde');});

        }
        self.cancelar = function () {
            $uibModalInstance.dismiss('cancel');
        }

    }]).controller('registrarTareaSesionCtrl',['$log','$uibModalInstance','modal','crud','data',function($log,$uibModalInstance,modal,crud,data){
        var self = this;
        self.secc = data.secciones;
        self.tipo = data.tipo;
        self.tarea_web = new Object();
        self.abrirCalendar = false;
        self.openCalendar = function () {self.abrirCalendar = true;}
        var sesionFin = new Date(data.sesion.fin);
        var sesionIni = new Date(data.sesion.ini);
        self.dateOptions = {
            formatYear: 'yyyy',
            maxDate: sesionFin,
            minDate: sesionIni,
            startingDay: 1
        };        
        self.titulo = data.edit ? 'Editar Tarea de Sesion de Aprendizaje' : 'Nueva Tarea para Sesion de Aprendizaje';
        self.tar  = data.edit ? data.tarea : new Object();
        self.tar.fec = sesionFin;
        self.save = data.edit ? editar : guardar;
        if(data.edit){           
            self.tar.nom = data.nom_tar;
            self.tar.des = data.des_tar;
            
            /*
            self.tar.nom =
            self.tar.des =
            self.tar.sec = 
            */
        }
        
        /*REGISTRAMOS LOS INDICADORES PERTENECIENTES A DICHA SESION DE APRENDIZAJE*/
        self.tar.ind = data.ind_sel;

        function guardar(){
            
            self.tar.sesid= data.sesion.id;
            if(self.tar.fec != undefined)
                self.tar.fec = self.tar.fec.getTime();


            var request = crud.crearRequest('sesiones',1,'registrarTareaSesion');
            request.setData(self.tar);
            crud.insertar('/maestro',request,function(response){
                if(response.responseSta){
                    
                    /////// REGISTRAMOS LA TAREA EN EL MODULO WEB //////////////////
                    self.tarea_web.nombre = self.tar.nom;
                    self.tarea_web.descripcion = self.tar.des;
                    self.tarea_web.numeroDoc = 1;
                    self.tarea_web.planID = data.planID;
                    self.tarea_web.gradoID = data.unidad_didactica.grad.cod;
                    self.tarea_web.seccionID = self.tar.sec;
                    self.tarea_web.areaID = data.unidad_didactica.are.cod;
                    self.tarea_web.docenteID = data.docente_id;
                    self.tarea_web.sesionID = data.sesion.id;
                    
                    //falta obtener el tip_per(B,T) , num_per(id_per) , tar_id_ses
                    self.tarea_web.tip_per = data.tip_per;
                    self.tarea_web.num_per = data.num_per;
                    self.tarea_web.tar_id_ses = response.data.id;
                    self.tarea_web.tipo = self.tar.tipo;
                    self.tarea_web.org = data.org;
                    
                    request = crud.crearRequest('tarea',1,'insertarTarea');
                    request.setData(self.tarea_web);
                    crud.insertar('/web',request,function(response){
                        if(response.responseSta){
                              self.seccion_id = self.tarea_web.seccionID;
                             // $('#registrar_tarea_sesion_aprendizaje').modal('hide');
                              angular.extend(self.tar,response.data);
                              $uibModalInstance.close(self.tar);
                           
                              
                        }
                    });
                    ////////////////////////////////////////////////////////////////
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){modal.mensaje('ERROR','El servidor no responde');});

            /*REGISTRAMOS LA TAREA EN EL MODULO WEB*/
            /////Preparamos la Data/////////
            
           
            
            
            
            
            
            
           
           
           ///////////////////////////////////////
           
        }
        function editar(){
            self.tar.fec = self.tar.fec.getTime();
            
            
            
        }
        self.cancelar = function () {
            $uibModalInstance.dismiss('cancel');
        }

    }]).controller('materialUnidadCtrl',['$log','$uibModalInstance','modal','crud','data',function($log,$uibModalInstance,modal,crud,data){
        var self = this;
        self.titulo = data.edit ? 'Editar Material Unidad' : 'Nuevo Material de Unidad';
        self.mat = data.edit ? data.material : new Object();
        self.tiposMaterial = [{cod:1,nom:"Libro",abr:'L'},{cod:2,nom:"Escritorio",abr:'E'}];
        if(data.edit){
            self.mat.tip = _.find(self.tiposMaterial,function(o){
                return o.abr == data.material.tip;
            });
        }
        self.save = data.edit ? editar : guardar;
        function guardar(){
            self.mat.unid = data.unidad.cod;
            self.mat.untip = data.unidad.tip;

            self.mat.tip = self.mat.tip.nom;
            var request = crud.crearRequest('unidades',1,'registrarMaterialesUnidad');
            request.setData(self.mat);
            crud.insertar('/maestro',request,function(response){
                if(response.responseSta){
                    angular.extend(self.mat,response.data);
                    self.mat.nom = self.mat.nom.toUpperCase();
                    $uibModalInstance.close(self.mat);
                }else{
                    modal.mensaje('Error',response.responseMsg);
                }
            },function(errRes){
                modal.mensaje('Error','El servidor no responde');
            });

        }
        function editar(){
            self.mat.tip = self.mat.tip.abr;
            var request = crud.crearRequest('unidades',1,'editarMaterialesUnidad');
            request.setData(self.mat);
            crud.actualizar('/maestro',request,function(response){
                if(response.responseSta){
                    self.mat.nom = self.mat.nom.toUpperCase();
                    $uibModalInstance.close(self.mat);
                }else{
                    modal.mensaje('Error',response.responseMsg);
                }
            },function(errRes){
                modal.mensaje('Error','El servidor no responde');
            });
        }
        self.cancelar = function () {
            $uibModalInstance.dismiss('cancel');
        }

    }]).controller('aprendizajesEsperadosCtrl',['$log','$uibModalInstance','modal','SectionTableParams','crud','data',function($log,$uibModalInstance,modal,SectionTableParams,crud,data){
        var self = this;
        self.titulo = "Agregar Competencia";
        self.titles = ["Competencias","Capacidades"];
        self.data = data.data;
        self.model = new SectionTableParams(self.titles,self.data);
        self.cancelar = function () {
            $uibModalInstance.dismiss('cancel');

        }
        self.save = function(){
            var comps = angular.copy(self.model.getData());

            angular.forEach(self.model.getData(),function(obj,keyComp){
                angular.forEach(obj.capacidades,function(obj,keyCap){
                    if(obj.selec == undefined && !obj.selec){
                        _.remove(comps[keyComp].capacidades,function(cap){
                            return cap.id === obj.id;
                        });
                    }
                });
            });
            _.remove(comps,function(comp){
                return comp.capacidades.length === 0;
            });
            var request = crud.crearRequest('unidades',1,'registrarCapacidadesUnidad');
            request.setData({idUnidad:data.unidad.cod, tipUnidad: data.unidad.tip, competencias:comps});
            crud.insertar('/maestro',request,function(response){
                if(response.responseSta){
                    $log.log('espuesta',response.data);
                    $uibModalInstance.close(response.data);
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde');
            });
        }
    }]).controller('sesionAprendizajeCtrl',['$log','$uibModalInstance','modal','crud','data',function($log,$uibModalInstance,modal,crud,data){
        var self = this;
        self.titulo = data.edit ? 'Editar Sesion de Aprendizaje' : 'Nueva Unidad de Aprendizaje';
        self.ses = data.edit ? data.sesion: new Object();
        self.save = data.edit ? editar : guardar;
        if(data.edit){
            self.ses.ini = new Date(self.ses.ini);
            self.ses.fin = new Date(self.ses.fin);
        }
        //Variables del calendario
        self.abrirIniCalendar = false;
        self.abrirFinCalendar = false;

        //Functiones para abrir el calendario
        self.openIniCalendar = function () {self.abrirIniCalendar = true;}
        self.openFinCalendar = function () {
            self.abrirFinCalendar = true;
        }
        //datos para iniciar el calendario
        self.dateIniOptions = {
            formatYear: 'yyyy',
            maxDate: new Date(data.unidad.fin),
            minDate: new Date(data.unidad.ini),
            initDate: data.edit ? new Date(self.ses.ini) : new Date(data.unidad.ini) ,
            startingDay: 1
        };
        self.dateFinOptions = {
            formatYear: 'yyyy',
            maxDate: new Date(data.unidad.fin),
            minDate: new Date(data.unidad.ini),
            initDate: data.edit ? new Date(self.ses.fin) : new Date(data.unidad.ini) ,
            startingDay: 1
        };
        function guardar(){
            self.ses.ini = self.ses.ini.getTime();
            self.ses.fin = self.ses.fin.getTime();
            self.ses.unidad = data.unidad.cod;
            self.ses.tipUnidad = data.unidad.tip;
            var request = crud.crearRequest('sesiones',1,'registrarSesion');
            request.setData(self.ses);
            crud.insertar('/maestro',request,function(response){
                if(response.responseSta){
                    angular.extend(self.ses,response.data);
                    $uibModalInstance.close(self.ses);
                }else{
                    modal.mensaje('Error',response.responseMsg);
                }
            },function(errRes){
                modal.mensaje('Error','El servidor no responde');
            });

        }
        function editar(){
            self.ses.ini = self.ses.ini.getTime();
            self.ses.fin = self.ses.fin.getTime();
            var request = crud.crearRequest('sesiones',1,'editarSesionAprendizaje');
            request.setData(self.ses);
            crud.actualizar('/maestro',request,function(response){
                if(response.responseSta){
                    $uibModalInstance.close(self.ses);
                }else{
                    modal.mensaje('Error',response.responseMsg);
                }
            },function(errRes){
                modal.mensaje('Error','El servidor no responde');
            });
        }
        self.cancelar = function () {
            $uibModalInstance.dismiss('cancel');
        }

    }]).controller('sequenciaDidacticaCtrl',['$log','$uibModalInstance','modal','NgTableParams','crud','data',function($log,$uibModalInstance,modal,NgTableParams,crud,data){
        var self = this;
        self.titulo = data.edit ? 'Editar Sequencia Didactica' : 'Agregar Sequencia Didactica';
        self.seqSecTabs = data.seqSecTabs;
        self.contenidoSecciones = new Array();
        self.contenidoSeccionesAux = [];
        self.cancelar = function () {
            $uibModalInstance.dismiss('cancel');
        }

        self.save = function(){
            self.seq.ses = data.sesion.id;
            angular.forEach(self.seqSecTabs,function(o,key){
                angular.extend(o,{activs:self.contenidoSecciones[key].settings().dataset});
            });
            angular.extend(self.seq,{cont:self.seqSecTabs});
            var request = crud.crearRequest('sesiones',1,'registrarSecuenciaDidactica');
            request.setData(self.seq);
            crud.insertar('/maestro',request,function(response){
                if(response.responseSta){
                    angular.extend(self.seq,response.data);
                    $uibModalInstance.close(self.seq);
                }else{
                    modal.mensaje("ERROR",response.responseMsg);
                }
            },function(errResponse){
            });
            console.log('secuencia',self.seq);

        }
        angular.forEach(self.seqSecTabs,function(o,key){
            self.contenidoSecciones.push(new NgTableParams({count:4},{
                counts: [],
                paginationMaxBlocks: 13,
                paginationMinBlocks: 2,
                dataset:[]
            }));
            self.contenidoSeccionesAux.push(new Array());
        });

        self.nuevActividadSecc = function(index){
            self.contenidoSecciones[index].settings().dataset.push({
                nom: "",
                des: "",
                edit:true,
                save:true
            });
            self.contenidoSecciones[index].sorting();
            self.contenidoSecciones[index].page(1);
            self.contenidoSecciones[index].reload();
        }
        self.aceptarEdicionActividadSeccion = function(parent,index,row){
            if(!row.nom || row.nom ===""){
                modal.mensaje("Error", "El campo no puede estar vacio");
                return;
            }
            if(row.save !== undefined && row.save){
                self.contenidoSeccionesAux[parent].push(angular.copy(row));
                row.edit = false;
                row.save = false;

            }else if(row.save !== undefined && !row.save){
                row.edit = false;
                row.save = false;
            }
        }
        self.editarActividadSeccion = function(parent,index,row){
            row.edit = true;
            row.save = false;
        }
        self.eliminarActividadSeccion = function(parent,index,row){
            self.contenidoSecciones[parent].settings().dataset.splice(index,1);
            self.contenidoSecciones[parent].reload();

        }
        self.cancelarEdicionActividadSeccion = function(parent,index,row){
            if(row.save !== undefined && row.save){
                self.contenidoSecciones[parent].settings().dataset.splice(index,1);
                self.contenidoSecciones[parent].reload();
            }else if(!row.save){
                angular.extend(row,self.contenidoSeccionesAux[parent][index]);
                row.edit = false;
            }
        }
    }]);