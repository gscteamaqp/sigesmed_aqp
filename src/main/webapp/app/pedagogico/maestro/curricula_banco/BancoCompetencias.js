    /**
 * Created by Administrador on 13/10/2016.
 */
function MaestroService($log,$rootScope,crud){
    this.competencia = new Object();
    var curriculas = [
        {id:0,ali:'CURRICULA_NACIONAL',nom:'Curriculo Nacional'},
        {id:1,ali:'CURRICULA_REGIONAL',nom:'Curriculo Regional'},
        {id:2,ali:'CURRICULA_LOCAL',nom:'Curriculo Local'},
        {id:3,ali:'CURRICULA_OTROS',nom:'Otros'}];

    this.listarCurriculas = function(){
        return curriculas;
    };
    this.setCompetencia = function (comp){
        angular.extend(this.competencia,comp);
    }
    this.getCompetencia = function(){
        return this.competencia;
    }
    this.listarElemento = function(meth,data,succ,err){
        var request = crud.crearRequest('curricula_banco',1,meth);
        request.setData(data);
        crud.listar('/maestro',request,succ,err);
    };
    this.guardarElemento = function(meth,data,succ,err){
        var request = crud.crearRequest('curricula_banco',1,meth);
        request.setData(data);
        crud.insertar('/maestro',request,succ,err);
    };
    this.editarElemento = function(meth,data,succ,err){
        var request = crud.crearRequest('curricula_banco',1,meth);
        request.setData(data);
        crud.actualizar('/maestro',request,succ,err);
    };
    this.eliminarElemento = function(meth,data,succ,err){
        var request = crud.crearRequest('curricula_banco',1,meth);
        request.setData(data);
        crud.eliminar('/maestro',request,succ,err);
    }
}
angular.module('app')
    .config(['$routeProvider',function($routeProvider){
        $routeProvider.when('/curricula/:comstr',{
            templateUrl:'pedagogico/maestro/curricula_banco/detalle_competencia.html',
            controller:'detalleCompetenciaCtrl',
            controllerAs:'ctrl'
        })
    }])
    .service('MaestroService',['$log','$rootScope','crud',MaestroService])
    .controller('BancoCompetencias',['$log','$location','NgTableParams','modal','UtilAppServices','MaestroService',function($log,$location,NgTableParams,modal,util,mService){
        var self = this;
        self.tablaCompetencias = new NgTableParams({count:15},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });

        cargarCompetencias();
        function cargarCompetencias(){
            self.showProgress = true;
            mService.listarElemento('listarBancoCompetencias',null,function(response){
                self.showProgress = false;
                if(response.responseSta){
                    self.tablaCompetencias.settings().dataset = response.data;
                    self.tablaCompetencias.reload();
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                self.showProgress = false;
                modal.mensaje('ERROR','El Servidor no responde');
            });
        }
        self.nuevaCompetencia = function(){

            var resolve = {
                data : function () {
                    return {
                        edit: false
                    }
                }
            }
            var modalInstance = util.openModal('nueva_competencia.html','competenciaBancoCtrl','lg','ctrl',resolve);
            modalInstance.result.then(function(data){
                self.tablaCompetencias.settings().dataset.push(data);
                self.tablaCompetencias.reload();
            },function(cdata){});
        }
        self.editarCompetencia = function(row){
            var resolve = {
                data : function () {
                    return {
                        edit: true,
                        com: angular.copy(row)
                    }
                }
            }
            var modalInstance = util.openModal('nueva_competencia.html','competenciaBancoCtrl','lg','ctrl',resolve);
            modalInstance.result.then(function(data){
                angular.extend(row,data);
            },function(cdata){});
        }
        self.eliminarCompetencia = function($event,row){
            util.openDialog('Eliminar Competencia','¿Seguro que desea eliminar la competencia?',$event,
                function (response) {
                    mService.eliminarElemento('eliminarCompetenciaBanco',row,function(response){
                        if(response.responseSta){
                            _.remove(self.tablaCompetencias.settings().dataset,function(item){
                                return row === item;
                            });
                            self.tablaCompetencias.reload();
                        }else {
                            modal.mensaje("ERROR",response.responseMsg);
                        }

                    },function(errResponse){
                        modal.mensaje("ERROR","El servidor no responde");
                    });
                },function () {
                    //modal.mensaje("CANCELAR","Se cancelo la accion");
                });
        }
        self.verDetalle = function(row){
            //mService.setCompetencia(row);
            //$log.log('El valor de la competencia', mService.getCompetencia());
            $location.url('/curricula/'+btoa(JSON.stringify(row)));
        }
    }])
    .controller('competenciaBancoCtrl',['$log','$uibModalInstance','MaestroService','modal','data',function($log,$uibModalInstance,mService,modal,data){
        var self = this;
        self.titulo = data.edit ? 'Editar Competencia' : 'Nueva Competencia';
        self.currs = mService.listarCurriculas();
        self.com = data.edit ? data.com : new Object();

        listarAreasCurr();
        if(data.edit){
            var index = _.findIndex(self.currs, function(o) { return o.nom === data.com.cur; });
            self.cur = self.currs[index];

        }

        function listarAreasCurr(){
            mService.listarElemento('listarAreasCurriculares',{all:false},function(response){
                if(response.responseSta){
                    self.areas = response.data;
                    self.areas.unshift({nom:""});

                    if(data.edit && data.com.are){
                        var index = _.findIndex(self.areas, function(o) { return o.cod === data.com.are.cod; });
                        self.are = self.areas[index];
                    }

                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El Servidor no responde');
            });
        }
        self.cancelar = function(){
            $uibModalInstance.dismiss('cancel');
        }
        self.cambiarArea = function(row){
            if(row.cod)
                self.com.are = row.cod
        }
        self.save = data.edit ? editar:guardar;
        function guardar (){
            self.com.cur = self.cur.ali;
            //self.com.are = self.are.cod;
            mService.guardarElemento('registrarCompetenciaBanco',{com:self.com},function(response){
                if(response.responseSta){
                    angular.extend(self.com,response.data);
                    self.com.are = self.are;
                    $uibModalInstance.close(self.com);
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no Responde');
            });
        }
        function editar(){
            self.com.cur = self.cur.ali;
            mService.editarElemento('editarCompetenciaBanco',self.com,function(response){
                if(response.responseSta){
                    self.com.cur = self.cur.nom;
                    self.com.are = self.are;
                    $uibModalInstance.close(self.com);
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){});
        }

    }])
    .controller('detalleCompetenciaCtrl',['$log','$routeParams','NgTableParams','modal','UtilAppServices','MaestroService',function($log,$routeParams,NgTableParams,modal,util,mService){
        var self = this;
        self.tablaCapacidades = new NgTableParams({count:15},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });
        self.tablaIndicadores = new NgTableParams({count:15},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });
        var currentCapacidad = null;
        var saveInds = [];
        var saveCap = [];

        self.com = JSON.parse(atob($routeParams.comstr));
        cargarDetalles(self.com);
        function cargarDetalles(com){
            mService.listarElemento('listarCapacidadesCompetencia',{cod:com.cod},function(response){
                $log.log('capacidades',response.data);
                if(response.responseSta){
                    saveCap = angular.copy(response.data);
                    self.tablaCapacidades.settings().dataset = response.data;
                    self.tablaCapacidades.reload();
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde');
            });
        }
        self.mostrarIndicadores = function(row){
            currentCapacidad = row;
            saveInds = angular.copy(row.inds);
            self.tablaIndicadores.settings().dataset = row.inds;
            self.tablaIndicadores.reload();
        }
        self.nuevoIndicador = function () {
            self.isSaving = true;
            self.tablaIndicadores.settings().dataset.unshift({
                nom: "",
                edit:true,
                save:true
            });

            self.tablaIndicadores.sorting({});
            self.tablaIndicadores.page(1);
            self.tablaIndicadores.reload();
        }
        self.nuevaCapacidad = function () {
            self.isSavingCap = true;
            self.tablaCapacidades.settings().dataset.unshift({
                nom: "",
                des:"",
                edit:true,
                save:true
            });

            self.tablaCapacidades.sorting({});
            self.tablaCapacidades.page(1);
            self.tablaCapacidades.reload();
        }
        self.editarElemento = function(row){
            row.edit = true;
        }

        self.cancelarEdicion = function(row){
            if(row.save !== undefined && row.save){
                var index = _.remove(self.tablaIndicadores.settings().dataset,function(o){
                    return o.save;
                });
                self.tablaIndicadores.reload();
                self.isSaving = false;
                return;

            }
            var index = _.findIndex(saveInds,function(o){
                return row.cod === o.cod;
            });
            angular.extend(row,saveInds[index]);
            row.edit = false;
        }
        self.cancelarEdicionCap = function(row){
            if(row.save !== undefined && row.save){
                var index = _.remove(self.tablaCapacidades.settings().dataset,function(o){
                    return o.save;
                });
                self.tablaCapacidades.reload();
                self.isSavingCap = false;
                return;

            }
            var index = _.findIndex(saveCap,function(o){
                return row.cod === o.cod;
            });
            angular.extend(row,saveCap[index]);
            row.edit = false;
        }
        self.aceptarEdicionCap = function(row){
            //Se quiere registrar un nuevo elemeno
            if(row.save !== undefined && row.save){
                row.com = self.com.cod;
                mService.guardarElemento('registrarCapacidadBanco',{cap:row},function(response){
                    if(response.responseSta){
                        saveCap.unshift(angular.extend(row,response.data));
                        row.edit = false;
                        row.save = false;
                        self.isSavingCap = false;
                    }else{
                        modal.mensaje('ERROR',response.responseMsg);
                    }
                },function(errResponse){
                    modal.mensaje('ERROR','Error en el servidor');
                });
                return;
            }
            mService.editarElemento('editarCapacidadBanco',row,function(response){
                if(response.responseSta){
                    var index = _.findIndex(saveCap,function(o){
                        return row.cod === o.cod;
                    });
                    saveCap[index] = angular.extend(saveCap[index],row);
                    row.edit = false;
                }else{
                    modal.mensaje('Error',response.responseMsg);
                    self.cancelarEdicionCap(row);
                }
            },function(errResponse){
                modal.mensaje('Error','El servidor no responde');
                self.cancelarEdicionCap(row);
            });
        }
        self.aceptarEdicion = function(row){
            //Se quiere registrar un nuevo elemeno
            if(row.save !== undefined && row.save){
                mService.guardarElemento('registrarIndicadorBanco',{nom:row.nom, cod:currentCapacidad.cod},function(response){
                    if(response.responseSta){
                        saveInds.unshift(angular.extend(row,response.data));
                        row.edit = false;
                        row.save = false;
                        self.isSaving = false;
                    }else{
                        modal.mensaje('ERROR',response.responseMsg);
                    }
                },function(errResponse){
                    modal.mensaje('ERROR','Error en el servidor');
                });
                return;
            }
            mService.editarElemento('editarIndicadorBanco',row,function(response){
                if(response.responseSta){
                    var index = _.findIndex(saveInds,function(o){
                        return row.cod === o.cod;
                    });
                    saveInds[index] = angular.extend(saveInds[index],row);
                    row.edit = false;
                }else{
                    modal.mensaje('Error',response.responseMsg);
                    self.cancelarEdicion(row);
                }
            },function(errResponse){
                modal.mensaje('Error','El servidor no responde');
                self.cancelarEdicion(row);
                });
        }
        self.eliminarCapacidad = function($event,row){
            util.openDialog('Eliminar Capacidad','¿Seguro que desea eliminar la capacidad?',$event,
                function (response) {
                    eliminarDeTabla(self.tablaCapacidades,'eliminarCapacidadBanco',{cap:row.cod,com:self.com.cod},row);
                },function () {
                    //modal.mensaje("CANCELAR","Se cancelo la accion");
                });
        }
        self.eliminarIndicador = function($event,row){
            util.openDialog('Eliminar Indicador','¿Seguro que desea eliminar el indicador?',$event,
                function (response) {
                    eliminarDeTabla(self.tablaIndicadores,'eliminarIndicadorBanco',{cod:row.cod},row);
                },function () {
                    //modal.mensaje("CANCELAR","Se cancelo la accion");
                });
        }
        function eliminarDeTabla(tabla,meth,data,row){
            mService.eliminarElemento(meth,data,function(response){
                if(response.responseSta){
                    _.remove(tabla.settings().dataset,function(item){
                        return row === item;
                    });
                    tabla.reload();
                }else {
                    modal.mensaje("ERROR",response.responseMsg);
                }

            },function(errResponse){
                modal.mensaje("ERROR","El servidor no responde");
            });
        }
    }]);