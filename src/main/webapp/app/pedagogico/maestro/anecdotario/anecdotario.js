/**
 * Created by Administrador on 20/12/2016.
 */
angular.module('app')
    .config(['$routeProvider',function($routeProvider){
        $routeProvider.when('/anecdotario/estudiante/:est',{
            templateUrl:'pedagogico/maestro/anecdotario/anecdotas_estudiante.html',
            controller:'anecdotarioEstudianteCtrl',
            controllerAs:'ctrl'
        })
    }])
    .controller('anecdotario',['$log','$location','$rootScope','NgTableParams','modal','UtilAppServices','crud','$q',function($log,$location,$rootScope,NgTableParams,modal,util,crud,$q){
        var self = this;
        self.estudiantes = new NgTableParams({count:15},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });
        listarGrados();
        function listarGrados(){
            var request = crud.crearRequest('anecdotario',1,'listarGrados');
            request.setData({org:$rootScope.usuMaster.organizacion.organizacionID, doc:$rootScope.usuMaster.usuario.usuarioID});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.niveles = response.data.niveles;
                    //self.grados = response.data.grados;
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
            });
        }
        self.mostrarGrados =  function(nivel){
            self.grados = nivel.grados;

        }
        self.mostrarSecciones =  function(grado){
            self.secciones = grado.secciones;

        }
        self.buscarAlumnos = function(){
            var request = crud.crearRequest('anecdotario',1,'listarEstudiantes');
            request.setData({org:$rootScope.usuMaster.organizacion.organizacionID, gra:self.grado.id,sec:self.seccion});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.estudiantes.settings().dataset = response.data;
                    self.estudiantes.reload();
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
            });
        }
        self.verAnecdotarioAlumno = function(estudiante){
            estudiante.graId = self.grado.id;
            estudiante.secc = self.seccion;
            estudiante.ie = $rootScope.usuMaster.organizacion.organizacionID;
            estudiante.doc = $rootScope.usuMaster.usuario.usuarioID;
            $location.url('/anecdotario/estudiante/'+btoa(JSON.stringify(estudiante)));
        }

    }])
    .controller('anecdotarioEstudianteCtrl',['$log','$routeParams','NgTableParams','modal','crud','UtilAppServices',function($log,$routeParams,NgTableParams,modal,crud,util){
        var self = this;
        self.est = JSON.parse(atob($routeParams.est));
        var tipoDerivaciones = [{cod:0,nom:'Ninguno',abr:'NI'},{cod:1,nom:'Direccion',abr:'DI'},{cod:2,nom:'Sus Padres',abr:'PA'},{cod:3,nom:'Hospital',abr:'HO'},{cod:4,nom:'Otro',abr:'OT'}];
        self.anecdotas = new NgTableParams({count:15},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });
        listarAnecdotas();
        function listarAnecdotas(){
            var request = crud.crearRequest('anecdotario',1,'listarAnecdotas');
            request.setData({est:self.est.id,doc:self.est.doc});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.anecdotas.settings().dataset = response.data;
                    self.anecdotas.reload();
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){modal.mensaje('ERROR','El servidor no responde');});
        }
        self.eliminarAnecdota = function($event,row){
            util.openDialog('Eliminar Anecdota ','¿Seguro que desea eliminar la anecdota?',$event,
                function (response) {
                    var request = crud.crearRequest('anecdotario',1,'eliminarAnecdota');
                    request.setData({id:row.id});
                    crud.eliminar('/maestro',request,function(response){
                        if(response.responseSta){
                            _.remove(self.anecdotas.settings().dataset,function(item){
                                return row.id === item.id;
                            });
                            self.anecdotas.reload();
                        }else {
                            modal.mensaje("ERROR",response.responseMsg);
                        }

                    },function(errResponse){
                        modal.mensaje("ERROR","El servidor no responde");
                    });
                },function () {
                    //modal.mensaje("CANCELAR","Se cancelo la accion");
                });
        }
        self.nuevaAnecdota = function(){
            var resolve = {
                data : function () {
                    return {
                        edit: false,
                        est: self.est
                    }
                }
            }
            var modalInstance = util.openModal('nueva_anecdota.html','registrarAnecdotaCtrl','lg','ctrl',resolve);
            modalInstance.result.then(function(anecdota){
                self.anecdotas.settings().dataset.push(anecdota);
                self.anecdotas.reload();
            },function(cdata){});
        }
        self.editarAnecdota = function(row){
            var request = crud.crearRequest('anecdotario',1,'listarAAccionesCorrectivas');
            request.setData({id:row.id});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    var resolve = {
                        data : function () {
                            return {
                                edit: true,
                                anec: angular.copy(row),
                                accs:response.data

                            }
                        }
                    }
                    var modalInstance = util.openModal('nueva_anecdota.html','editarAnecdotaCtrl','lg','ctrl',resolve);
                    modalInstance.result.then(function(anecdota){
                        angular.extend(row,anecdota);
                    },function(cdata){});
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){});
        }
        self.showDer = function(abr){
            var index = _.findIndex(tipoDerivaciones,function(o){
                return o.abr == abr;
            });
            if(index != -1) return tipoDerivaciones[index].nom;
            return ""
        }
    }])
    .controller('registrarAnecdotaCtrl',['$log','NgTableParams','$uibModalInstance','$rootScope','modal','data','crud',function($log,NgTableParams,$uibModalInstance,$rootScope,modal,data,crud){
        var self = this;
        self.tipoDerivaciones = [{cod:0,nom:'Ninguno',abr:'NI'},{cod:1,nom:'Direccion',abr:'DI'},{cod:2,nom:'Sus Padres',abr:'PA'},{cod:3,nom:'Hospital',abr:'HO'},{cod:4,nom:'Otro',abr:'OT'}];
        self.abrirCalendar = false;
        self.openCalendar = function () {self.abrirCalendar = true;}
        self.dateOptions = {
            formatYear: 'yyyy',
            maxDate: new Date(2040, 5, 22),
            minDate: new Date(2016, 11, 22),
            startingDay: 1
        };
        self.titulo = !data.edit ? 'Nueva Anecdota' : 'Editar Anecdota';

        self.cancelar = function () {
            $uibModalInstance.dismiss('cancel');
        }
        var accionesSave = [];
        self.tablaAcciones = new NgTableParams({count:5},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset: []
        });
        self.addAccion = function() {
            self.isSaving = true;
            self.tablaAcciones.settings().dataset.push({
                cod: self.tablaAcciones.settings().dataset.length + 1,
                nom: "",
                edit:true,
                save:true
            });

            self.tablaAcciones.sorting({});
            self.tablaAcciones.page(1);
            self.tablaAcciones.reload();
        }
        self.eliminarAccion =  function(row){

            var currCod = row.cod;

            _.remove(self.tablaAcciones.settings().dataset,function(o){
                return o.cod === row.cod;
            });
            _.remove(accionesSave,function(o){
                return o.cod === row.cod;
            });
            angular.forEach(self.tablaAcciones.settings().dataset,function(obj,key){
                if(obj.cod >= currCod + 1){
                    obj.cod -= 1;
                    accionesSave[key].cod -= 1;
                }
            });
            self.tablaAcciones.reload();
        }
        self.editarAccion = function(row){
            row.edit = true;
        }
        self.aceptarEdicionAccion = function(row){
            if(!row.nom || row.nom ===""){
                modal.mensaje("Error", "El campo no puede estar vacio");
                return;
            }
            if(row.save !== undefined && row.save){
                //angular.extend(row,response.data);
                row.nom = row.nom.toUpperCase();
                accionesSave.push(angular.copy(row));
                row.edit = false;
                row.save = false;
                self.isSaving = false;
                return;
            }else if(row.edit){
                var index = _.findIndex(accionesSave,function(o){
                    return row.cod === o.cod;
                });
                row.nom = row.nom.toUpperCase();
                row.edit = false;
                row.save = false;
                angular.extend(accionesSave[index],angular.copy(row));
            }
        }
        self.cancelarEdicionAccion = function(row){
            if(row.save !== undefined && row.save){
                _.remove(self.tablaAcciones.settings().dataset,function(o){
                    return o.save;
                });
                self.tablaAcciones.reload();
                self.isSaving = false;
                return;
            }

            var index = _.findIndex(accionesSave,function(o){
                return row.cod === o.cod;
            });
            angular.extend(row,accionesSave[index]);
            row.edit = false;
        }
        self.save = guardar;
        function guardar() {

            self.anec.org = data.est.ie;
            self.anec.doc = data.est.doc;
            self.anec.est = data.est.id;
            self.anec.gra = data.est.graId;
            self.anec.sec = data.est.secc;
            self.anec.fec = self.anec.fec.getTime();
            self.anec.der = self.anec.tipDer.abr;
            self.anec.acciones = self.tablaAcciones.settings().dataset;

            var request = crud.crearRequest('anecdotario',1,'registrarAnecdota');
            request.setData(self.anec);
            crud.insertar('/maestro',request,function(response){
                if(response.responseSta){
                    angular.extend(self.anec,response.data);
                    $uibModalInstance.close(self.anec);
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){modal.mensaje('ERROR','El servidor no responde');});
        }
    }])
    .controller('editarAnecdotaCtrl',['$log','NgTableParams','$uibModalInstance','$rootScope','modal','data','crud',function($log,NgTableParams,$uibModalInstance,$rootScope,modal,data,crud){
        var self = this;

        self.tipoDerivaciones = [{cod:0,nom:'Ninguno',abr:'NI'},{cod:1,nom:'Direccion',abr:'DI'},{cod:2,nom:'Sus Padres',abr:'PA'},{cod:3,nom:'Hospital',abr:'HO'},{cod:4,nom:'Otro',abr:'OT'}];
        self.abrirCalendar = false;
        self.openCalendar = function () {self.abrirCalendar = true;}
        self.dateOptions = {
            formatYear: 'yyyy',
            maxDate: new Date(2040, 5, 22),
            minDate: new Date(2016, 11, 22),
            startingDay: 1
        };
        self.titulo = 'Editar Anecdota';

        self.anec = data.anec;
        self.anec.tipDer = _.find(self.tipoDerivaciones,function(o){
            return o.abr == data.anec.der;
        });
        self.anec.fec = new Date(data.anec.fec);
        self.cancelar = function () {
            $uibModalInstance.dismiss('cancel');
        }
        var accionesSave = angular.copy(data.accs);
        self.tablaAcciones = new NgTableParams({count:5},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset: data.accs
        });
        initCods();
        function initCods(){
            angular.forEach(self.tablaAcciones.settings().dataset,function(obj,key){
                obj.cod = key + 1;
            });
        }
        self.addAccion = function() {
            self.isSaving = true;
            self.tablaAcciones.settings().dataset.push({
                cod: self.tablaAcciones.settings().dataset.length + 1,
                nom: "",
                edit:true,
                save:true
            });

            self.tablaAcciones.sorting({});
            self.tablaAcciones.page(1);
            self.tablaAcciones.reload();
        }
        self.eliminarAccion =  function(row){

            var currCod = row.cod;

            _.remove(self.tablaAcciones.settings().dataset,function(o){
                return o.cod === row.cod;
            });
            _.remove(accionesSave,function(o){
                return o.cod === row.cod;
            });
            angular.forEach(self.tablaAcciones.settings().dataset,function(obj,key){
                if(obj.cod >= currCod + 1){
                    obj.cod -= 1;
                    accionesSave[key].cod -= 1;
                }
            });
            self.tablaAcciones.reload();
        }
        self.editarAccion = function(row){
            row.edit = true;
        }
        self.aceptarEdicionAccion = function(row){
            if(!row.nom || row.nom ===""){
                modal.mensaje("Error", "El campo no puede estar vacio");
                return;
            }
            if(row.save !== undefined && row.save){
                //angular.extend(row,response.data);
                row.nom = row.nom.toUpperCase();
                accionesSave.push(angular.copy(row));
                row.edit = false;
                row.save = false;
                self.isSaving = false;
                return;
            }else if(row.edit){
                var index = _.findIndex(accionesSave,function(o){
                    return row.cod === o.cod;
                });
                row.nom = row.nom.toUpperCase();
                row.edit = false;
                row.save = false;
                angular.extend(accionesSave[index],angular.copy(row));
            }
        }
        self.cancelarEdicionAccion = function(row){
            if(row.save !== undefined && row.save){
                _.remove(self.tablaAcciones.settings().dataset,function(o){
                    return o.save;
                });
                self.tablaAcciones.reload();
                self.isSaving = false;
                return;
            }

            var index = _.findIndex(accionesSave,function(o){
                return row.cod === o.cod;
            });
            angular.extend(row,accionesSave[index]);
            row.edit = false;
        }
        self.save = editar;
        function editar() {
            self.anec.fec = self.anec.fec.getTime();
            self.anec.der = self.anec.tipDer.abr;
            self.anec.acciones = self.tablaAcciones.settings().dataset;

            var request = crud.crearRequest('anecdotario',1,'editarAnecdota');
            request.setData(self.anec);
            crud.actualizar('/maestro',request,function(response){
                if(response.responseSta){
                    $uibModalInstance.close(self.anec);
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){modal.mensaje('ERROR','El servidor no responde');});
        }
    }]);


