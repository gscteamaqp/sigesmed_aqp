/**
 * Created by Administrador on 29/09/16.
 */
function ComisionService2($rootScope,crud){
    this.listarCargos = function(succ,err){
        var request = crud.crearRequest('cargos',1,'listarCargos');
        crud.listar('/conformacion_comisiones',request,succ,err);
    };
    this.listarComisiones = function(succ,err){
        var org = $rootScope.usuMaster.organizacion.organizacionID;
        var request = crud.crearRequest('comisiones',1,'listarComisionesDirector');
        request.setMetadataValue('org',''+org);
        crud.listar('/conformacion_comisiones',request,succ,err);
    };
    this.listarCargosComision = function(com,succ,err){
        var request = crud.crearRequest('comisiones',1,'listarCargosComision');
        request.setMetadataValue('cod',''+com);
        crud.listar('/conformacion_comisiones',request,succ,err);
    };
    this.listarIntegrantesComision = function (com,succ,err) {
        var request = crud.crearRequest('comisiones',1,'listarIntegrantesComision');
        request.setMetadataValue('cod',''+com);
        crud.listar('/conformacion_comisiones',request,succ,err);
    };
    this.listarDetallesComision = function (com,succ,err) {
        var request = crud.crearRequest('comisiones',1,'listarDetallesComision');
        request.setData({com:com});
        //request.setMetadataValue('com',''+com);
        crud.listar('/conformacion_comisiones',request,succ,err);
    };
    this.listarElementoComision = function(dom,func,data,succ,err){
        var request = crud.crearRequest(dom,1,func);
        request.setMetadataValue('cod',''+data);
        crud.listar('/conformacion_comisiones',request,succ,err);
    };
    this.buscarPersonas = function (per,succ,err){
        var request = crud.crearRequest('comisiones',1,'buscarPersonaParaComision');
        request.setData(per);
        crud.listar('/conformacion_comisiones',request,succ,err);
    };
    this.buscarPersonaPresidente = function (per,succ,err){
        per.opt = "pre";
        per.org = $rootScope.usuMaster.organizacion.organizacionID;
        per.dni = String(per.dni);
        
        console.log('la data enviada',per);
        var request = crud.crearRequest('comisiones',1,'buscarPersonaParaComision');
        request.setData(per);
        crud.listar('/conformacion_comisiones',request,succ,err);
    };
    this.registrarComision = function(data,succ,err){
        //JSON.parse( window.atob(localStorage.getItem('organizacion')));
        var org = $rootScope.usuMaster.organizacion.organizacionID;
        var request = crud.crearRequest('comisiones',1,'registrarComision');
        request.setMetadataValue('org',''+org);
        request.setData(data);
        crud.insertar('/conformacion_comisiones',request,succ,err);
    };
      
    this.editarComision = function (data,succ,err) {
        var request = crud.crearRequest('comisiones',1,'editarComision');
        request.setData(data);
        crud.actualizar('/conformacion_comisiones',request,succ,err);
    };
    //metodo general para editar un elemento del modulo de comisiones,
    // recibe el nombre del dominio(dom), el nombre de la function(met), los datos a editar (data)
    // y las functiones de exito(succ) y error (err)
    this.editarElemento = function (dom,met,data,succ,err) {
        var request = crud.crearRequest(dom,1,met);
        request.setData(data);
        crud.actualizar('/conformacion_comisiones',request,succ,err);
    };
    this.eliminarComision = function (id,succ,err) {
        var request = crud.crearRequest('comisiones',1,'eliminarComision');
        request.setMetadataValue('cod',''+id);
        crud.eliminar('/conformacion_comisiones',request,succ,err);
    };
    this.eliminarElementoComision = function (dom,func,data,succ,err){
        var request = crud.crearRequest(dom,1,func);
        request.setData(data);
        crud.eliminar('/conformacion_comisiones',request,succ,err);
    }
}
angular.module('app')
    
    .service('ComisionService2',['$rootScope','crud',ComisionService2])
    .controller('crearComision',['$log','$location','NgTableParams','modal','UtilAppServices','ComisionService2',function($log,$location,NgTableParams,modal,util,comService){
        var self = this;
        self.tablaComisiones = new NgTableParams({count:8},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });
        try{
            self.showProgress = true;
            comService.listarComisiones(function(response){
                self.showProgress = false;
                self.tablaComisiones.settings().dataset = response.data;
                self.tablaComisiones.reload();
            },function(errResponse){
                self.showProgress = false;
                modal.mensaje("ERROR","No se pudieron obtener los datos");
            });
        }catch(err){
            self.showProgress = false;
            modal.mensaje("ERROR","El servidor no responde");
        }
        self.nuevaComision = function(){
            var resolve = {
                data:function(){
                    return {
                        edit:false
                    };
                }
            };
            var modalInstance = util.openModal('nuevaComision.html','registrarComisionCtrl2','lg','ctrl',resolve);
            modalInstance.result.then(function(response){
                if(response.response === 'BAD'){
                    modal.mensaje("ERROR","No se pudieron obtener los datos");
                }else {
                    self.tablaComisiones.settings().dataset.push(response.data);
                    self.tablaComisiones.reload();
                    modal.mensaje("CORRECTO","Se realizo la transaccion");
                }

            },function(response){
                modal.mensaje("CANCELAR","Se cancelo la accion");
            });
        }
        self.verDetalles = function(row){
            $location.url('/comisiones/'+row.cod);
        }
        self.editarComision = function(row){
            comService.listarDetallesComision(row.cod,function (response) {
                if(response.response === 'BAD'){
                    modal.mensaje("ERROR","No se pudieron obtener los datos");
                }else{
                    console.log("datos",response.data);
                    var resolve = {
                        data:function(){
                            return{
                                edit:true,
                                comision : angular.extend(new Object(),row),
                                cargos : response.data.cargos,
                                pres: response.data.pres
                            }
                        }
                    };
                    var modalInstance = util.openModal('nuevaComision.html','registrarComisionCtrl2','lg','ctrl',resolve);
                    modalInstance.result.then(function (response) {
                        if(response.response === 'OK'){
                            angular.extend(row,response.data);
                        }else if(response.response === 'BAD'){
                            modal.mensaje("ERROR","Hubo un error en el servidor");
                        }
                    },function (errResponse) {
                        modal.mensaje("CANCELAR","Se cancelo la accion");
                    });
                }

            },function (errResponse) {
                modal.mensaje("ERROR","El servidor no responde");
            });
        } 
        self.eliminarComision = function($e,row){
            util.openDialog('Eliminar Comision','Â¿Seguro que desea eliminar\n la comision?',$e,
                function (response) {
                    comService.eliminarComision(row.cod,function(response){
                        if(response.response === 'OK'){
                            _.remove(self.tablaComisiones.settings().dataset,function(item){
                                return row === item;
                            });
                            self.tablaComisiones.reload();
                        }else {
                            modal.mensaje("ERROR","Hubo un error al ejecutar su consulta");
                        }

                    },function(errResponse){
                        modal.mensaje("ERROR","El servidor no responde");
                    });
                },function () {
                    modal.mensaje("CANCELAR","Se cancelo la accion");
            });
        }
    }])
    .controller('registrarComisionCtrl2',['$log','$uibModalInstance','NgTableParams','data','modal','ComisionService2',function($log,$uibModalInstance,NgTableParams,data,modal,comService){
        var self = this;
        self.cargosSeleccionados = new NgTableParams({count:3},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset: !data.edit ? [] : data.cargos
        });
        self.titulo = !data.edit? 'Nueva Comision' : 'Editar Comision';
        self.comision = !data.edit? new Object() : data.comision;
        self.pres = data.edit ? data.pres : new Object();
        comService.listarCargos(function(response){
            self.cargos = response.data;
            //lodash --- libreria
            _.remove(self.cargos,function(o){
                return (o.nom=="PARTICIPANTE" || o.nom=="PRESIDENTE");
            });
        },function(errResponse){
        });
        
        self.addCargo = function () {
            self.cargosSeleccionados.settings().dataset.push(self.carSel);
            self.cargosSeleccionados.reload();
        }
        self.eliminarCargoSelecionado = function(row){
            _.remove(self.cargosSeleccionados.settings().dataset,function(item){
                return row === item;
            });
            self.cargosSeleccionados.reload();
        }
        
        //-------------Presidente comision------------------------
         self.tablaPersonas= new NgTableParams({count:4},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
         });
         self.buscarPersonaPresidente = function(per){
            if(per === undefined || ( (per.dni === undefined || per.dni ==="" )
                && (per.apep === undefined || per.apep === "") && (per.apem === undefined || per.apem === ""))){
                modal.mensaje("ERROR","Debe ingresar al menos uno de los campos de busqueda");
            }else{
                //hacemos la busqueda de las personas
                self.showProgress = true;
                comService.buscarPersonaPresidente(per,function(response){
                    self.showProgress = false;
                    if(response.response === 'OK'){
                        self.tablaPersonas.settings().dataset = response.data;
                        self.tablaPersonas.reload();
                    }else if(response.response === 'BAD'){
                        self.tablaPersonas.settings().dataset = [];
                        self.tablaPersonas.reload();
                        modal.mensaje('ERROR',response.responseMsg);
                    }
                },function(errResponse){
                    self.showProgress = false;
                    modal.mensaje("ERROR","El Servidor no responde")
                });
            }
        }
        self.buscarPersona = function(per){
            if(per === undefined || ( (per.dni === undefined || per.dni ==="" )
                && (per.apep === undefined || per.apep === "") && (per.apem === undefined || per.apem === ""))){
                modal.mensaje("ERROR","Debe ingresar al menos uno de los campos de busqueda");
            }else{
                //hacemos la busqueda de las personas
                self.showProgress = true;
                comService.buscarPersonas(per,function(response){
                    self.showProgress = false;
                    if(response.response === 'OK'){
                        self.tablaPersonas.settings().dataset = response.data;
                        self.tablaPersonas.reload();
                    }else if(response.response === 'BAD'){
                        self.tablaPersonas.settings().dataset = [];
                        self.tablaPersonas.reload();
                        modal.mensaje('ERROR',response.responseMsg);
                    }
                },function(errResponse){
                    self.showProgress = false;
                    modal.mensaje("ERROR","El Servidor no responde")
                });
            }
        }
        self.agregarPresidente = function(row){
            console.log("ver pres", row );
            self.pres = row;
        }
        // ------------------------------------------------------------------
        
        function guardar(){
            var data = {
                com : self.comision ,
                car : self.cargosSeleccionados.settings().dataset,
                pres: self.pres
            }
            comService.registrarComision(data,function (response) {
                $uibModalInstance.close(response);
            },function (errResponse) {
                $uibModalInstance.dismiss(errResponse);
            })
        }
        function editar(){
            var data = {
                com : self.comision,
                car : self.cargosSeleccionados.settings().dataset,
                pres: self.pres
            }
            comService.editarComision(data,function(response){
                response.data = self.comision;
                $uibModalInstance.close(response);
            },function (errResponse){
                $uibModalInstance.dismiss(errResponse);
            })
        }
        self.guardar = !data.edit? guardar : editar;
        self.cancelar = function(){
            $uibModalInstance.dismiss('cancel');
        }
    }])
    
  