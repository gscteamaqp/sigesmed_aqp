/**
 * Created by geank on 29/09/16.
 */

function CargoComisionService($rootScope,crud){
    this.listarCargos = function(succ,err){
        var request = crud.crearRequest('cargos',1,'listarCargos');
        crud.listar('/conformacion_comisiones',request,succ,err);
    };
    this.registrarCargo = function(cargo,succ,err){
        var request = crud.crearRequest('cargos',1,'registrarCargo');
        request.setData(cargo);
        crud.insertar('/conformacion_comisiones',request,succ,err);
    };
    this.editarCargo = function (cargo,succ,err) {
        var request = crud.crearRequest('cargos',1,'actualizarCargo');
        request.setData(cargo);
        crud.actualizar('/conformacion_comisiones',request,succ,err);
    };

    this.eliminarCargo = function (id,succ,err) {
        var request = crud.crearRequest('cargos',1,'eliminarCargo');
        request.setMetadataValue('cod',''+id);
        crud.eliminar('/conformacion_comisiones',request,succ,err);
    };
}
/*app.config(function ($provide) {

    $provide.decorator('$exceptionHandler', function ($delegate) {

        return function (exception, cause) {
            $delegate(exception, cause);

            alert('Error occurred! Please contact admin.');
        };
    });
});*/
angular.module('app')
    .service('CargoComisionService',['$rootScope','crud',CargoComisionService])
    .controller('cargoComision',['$log','NgTableParams','modal','UtilAppServices','CargoComisionService',function($log,NgTableParams,modal,util,cargoService){
        var self = this;
        self.tablaCargos = new NgTableParams({count:8},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });
        try{
            self.showProgress = true;
            cargoService.listarCargos(function(response){
                self.showProgress = false;
                self.tablaCargos.settings().dataset = response.data;
                self.tablaCargos.reload();
            },function(errResponse){
                self.showProgress = false;
                modal.mensaje("ERROR","No se pudieron obtener los datos");
            });
        }catch(err){
            modal.mensaje("ERROR","No se pudieron obtener los datos");
        }
        self.nuevoCargo = function () {

            var modalInstance = util.openModal('nuevoCargo.html','registrarCargoCtrl','lg','ctrl',null);
            modalInstance.result.then(function(cargo){
                self.tablaCargos.settings().dataset.push(cargo);
                self.tablaCargos.reload();
            },function(response){
                modal.mensaje("CANCELAR","Se cancelo la accion");
            });
        }
        self.editarCargo = function(row){
            var resolve = {
                data:function(){
                    return{
                        cargo : angular.extend(new Object(),row)
                    }
                }
            };
            var modalInstance = util.openModal('nuevoCargo.html','editarCargoCtrl','lg','ctrl',resolve);
            modalInstance.result.then(function (response) {
                if(response.response === 'OK'){
                    angular.extend(row,response.data);
                }else if(response.response === 'BAD'){
                    modal.mensaje("ERROR","Hubo un error en el servidor");
                }
            },function (errResponse) {
                modal.mensaje("CANCELAR","Se cancelo la accion");
            });
        }
        self.eliminarCargo = function($e,row){
            util.openDialog('Eliminar Cargo','¿Seguro que desea eliminar\n el cargo?',$e,
                function (response) {
                    cargoService.eliminarCargo(row.cod,function(response){
                        if(response.response === 'OK'){
                            _.remove(self.tablaCargos.settings().dataset,function(item){
                                return row === item;
                            });
                            self.tablaCargos.reload();
                        }else {
                            modal.mensaje("ERROR","No se puede eliminar el cargo por defecto");
                        }

                    },function(errResponse){
                        modal.mensaje("ERROR","El servidor no responde");
                    });
            },function () {
                    modal.mensaje("CANCELAR","Se cancelo la accion");
                });
        }
    }])
    .controller('registrarCargoCtrl',['$log','$uibModalInstance','CargoComisionService',function($log,$uibModalInstance,cargoService){
        var self = this;
        self.titulo = 'Registrar nuevo cargo';

        self.guardar = function () {

            cargoService.registrarCargo(self.cargo,function (r) {
                if(r.response === 'BAD'){
                    $log.log(r.responseMsg);
                }
                else if(r.response === 'OK'){
                    $log.log(r.responseMsg);
                    $uibModalInstance.close(r.data);
                }
            },function (errResponse) {
                $log.log(errResponse);
                $uibModalInstance.dismiss('cancel');
            });
        }
        self.cancelar = function(){
            $uibModalInstance.dismiss('cancel');
        }
    }])
    .controller('editarCargoCtrl',['$log','$uibModalInstance','CargoComisionService','data',function($log,$uibModalInstance,cargoService,data){
        var self = this;
        self.titulo = 'Editar Cargo de Comision';
        self.cargo = data.cargo;
        self.guardar = function(){
            cargoService.editarCargo(self.cargo,function (response) {
                response.data = self.cargo;
                $uibModalInstance.close(response);
            },function (errResponse) {
                $log.log(errResponse);
                $uibModalInstance.dismiss('cancel');
            });
        }
        self.cancelar = function(){
            $uibModalInstance.dismiss('cancel');
        }
    }]);