/**
 * Created by Administrador on 29/09/16.
 */
function ComisionService($rootScope,crud){
    this.listarCargos = function(succ,err){
        var request = crud.crearRequest('cargos',1,'listarCargos');
        crud.listar('/conformacion_comisiones',request,succ,err);
    };
    this.listarComisiones = function(succ,err){
        var org = $rootScope.usuMaster.organizacion.organizacionID;
        var request = crud.crearRequest('comisiones',1,'listarComisiones');
        request.setMetadataValue('org',''+org);
        crud.listar('/conformacion_comisiones',request,succ,err);
    };
    this.listarCargosComision = function(com,succ,err){
        var request = crud.crearRequest('comisiones',1,'listarCargosComision');
        request.setMetadataValue('cod',''+com);
        crud.listar('/conformacion_comisiones',request,succ,err);
    };
    this.listarIntegrantesComision = function (com,succ,err) {
        var request = crud.crearRequest('comisiones',1,'listarIntegrantesComision');
        request.setMetadataValue('cod',''+com);
        crud.listar('/conformacion_comisiones',request,succ,err);
    };
    this.listarReunionesComision = function (com,succ,err) {
        var request = crud.crearRequest('comisiones',1,'listarReunionesComision');
        request.setMetadataValue('com',''+com);
        crud.listar('/conformacion_comisiones',request,succ,err);
    };
    this.listarElementoComision = function(dom,func,data,succ,err){
        var request = crud.crearRequest(dom,1,func);
        request.setMetadataValue('cod',''+data);
        crud.listar('/conformacion_comisiones',request,succ,err);
    }
    this.buscarPersonas = function (per,succ,err){
        var request = crud.crearRequest('comisiones',1,'buscarPersonaParaComision');
        request.setData(per);
        crud.listar('/conformacion_comisiones',request,succ,err);
    };
    /*this.registrarComision = function(data,succ,err){
        //JSON.parse( window.atob(localStorage.getItem('organizacion')));
        var org = $rootScope.usuMaster.organizacion.organizacionID;
        var request = crud.crearRequest('comisiones',1,'registrarComision');
        request.setMetadataValue('org',''+org);
        request.setData(data);
        crud.insertar('/conformacion_comisiones',request,succ,err);
    };*/
    this.registrarReunion = function(data,succ,err){
        var request = crud.crearRequest('comisiones',1,'registrarReunion');
        request.setData(data);
        crud.insertar('/conformacion_comisiones',request,succ,err);
    };
    this.registrarIntegranteComision = function (dni,com,succ,err) {
        var request = crud.crearRequest('comisiones',1,'registrarIntegranteComision');
        var data = {dni:''+dni,com:''+com}
        request.setData(data);
        crud.insertar('/conformacion_comisiones',request,succ,err);
    };
    this.registrarPersona = function(com,car,data,succ,err){
        //reusar la transaccion existente
        var request = crud.crearRequest('comisiones',1,'registrarPersonaAComision');
        request.setMetadataValue('com',''+com);
        request.setMetadataValue('car',''+car);
        request.setData(data);

        crud.insertar('/conformacion_comisiones',request,succ,err);
    };
    this.registrarActa = function(acta,succ,err){
        //reusar la transaccion existente
        var request = crud.crearRequest('actas',1,'registrarActa');
        request.setData(acta);
        crud.insertar('/conformacion_comisiones',request,succ,err);
    };
    this.registrarAcuerdos = function(acta,data,succ,err){
        //reusar la transaccion existente
        var request = crud.crearRequest('actas',1,'registrarAcuerdo');
        request.setMetadataValue('cod',''+acta);
        request.setData(data);
        crud.insertar('/conformacion_comisiones',request,succ,err);
    };
    this.registrarAsistencia= function(com,data,succ,err){
        //reusar la transaccion existente
        var request = crud.crearRequest('comisiones',1,'registrarAsistencia');
        request.setMetadataValue('com',''+com);
        request.setData(data);
        crud.insertar('/conformacion_comisiones',request,succ,err);
    };
    this.editarComision = function (data,succ,err) {
        var request = crud.crearRequest('comisiones',1,'editarComision');
        request.setData(data);
        crud.actualizar('/conformacion_comisiones',request,succ,err);
    };
    //metodo general para editar un elemento del modulo de comisiones,
    // recibe el nombre del dominio(dom), el nombre de la function(met), los datos a editar (data)
    // y las functiones de exito(succ) y error (err)
    this.editarElemento = function (dom,met,data,succ,err) {
        var request = crud.crearRequest(dom,1,met);
        request.setData(data);
        crud.actualizar('/conformacion_comisiones',request,succ,err);
    };
    this.eliminarComision = function (id,succ,err) {
        var request = crud.crearRequest('comisiones',1,'eliminarComision');
        request.setMetadataValue('cod',''+id);
        crud.eliminar('/conformacion_comisiones',request,succ,err);
    };
    this.eliminarElementoComision = function (dom,func,data,succ,err){
        var request = crud.crearRequest(dom,1,func);
        request.setData(data);
        crud.eliminar('/conformacion_comisiones',request,succ,err);
    }
}
angular.module('app')
    .config(['$routeProvider',function($routeProvider){
        $routeProvider.when('/comisiones/:com_id',{
            templateUrl:'pedagogico/comisiones/detalle_comision.html',
            controller:'detalleComisionCtrl',
            controllerAs:'ctrl'
        })
    }])
    .service('ComisionService',['$rootScope','crud',ComisionService])
    .controller('comision',['$log','$location','NgTableParams','modal','UtilAppServices','ComisionService',function($log,$location,NgTableParams,modal,util,comService){
        var self = this;
        self.tablaComisiones = new NgTableParams({count:8},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });
        try{
            self.showProgress = true;
            comService.listarComisiones(function(response){
                self.showProgress = false;
                self.tablaComisiones.settings().dataset = response.data;
                self.tablaComisiones.reload();
            },function(errResponse){
                self.showProgress = false;
                modal.mensaje("ERROR","No se pudieron obtener los datos");
            });
        }catch(err){
            self.showProgress = false;
            modal.mensaje("ERROR","El servidor no responde");
        }
       
        self.verDetalles = function(row){
            $location.url('/comisiones/'+row.cod);
        }
        self.editarComision = function(row){
            comService.listarCargosComision(row.cod,function (response) {
                if(response.response === 'BAD'){
                    modal.mensaje("ERROR","No se pudieron obtener los datos");
                }else{
                    var resolve = {
                        data:function(){
                            return{
                                comision : angular.extend(new Object(),row),
                                cargos : response.data
                            }
                        }
                    };
                    var modalInstance = util.openModal('nuevaComision.html','registrarComisionCtrl','lg','ctrl',resolve);
                    modalInstance.result.then(function (response) {
                        if(response.response === 'OK'){
                            angular.extend(row,response.data);
                        }else if(response.response === 'BAD'){
                            modal.mensaje("ERROR","Hubo un error en el servidor");
                        }
                    },function (errResponse) {
                        modal.mensaje("CANCELAR","Se cancelo la accion");
                    });
                }

            },function (errResponse) {
                modal.mensaje("ERROR","El servidor no responde");
            });
        } 
        self.eliminarComision = function($e,row){
            util.openDialog('Eliminar Comision','¿Seguro que desea eliminar\n la comision?',$e,
                function (response) {
                    comService.eliminarComision(row.cod,function(response){
                        if(response.response === 'OK'){
                            _.remove(self.tablaComisiones.settings().dataset,function(item){
                                return row === item;
                            });
                            self.tablaComisiones.reload();
                        }else {
                            modal.mensaje("ERROR","Hubo un error al ejecutar su consulta");
                        }

                    },function(errResponse){
                        modal.mensaje("ERROR","El servidor no responde");
                    });
                },function () {
                    modal.mensaje("CANCELAR","Se cancelo la accion");
            });
        }
    }])
    .controller('registrarComisionCtrl',['$log','$uibModalInstance','NgTableParams','data','ComisionService',function($log,$uibModalInstance,NgTableParams,data,comService){
        var self = this;
        self.cargosSeleccionados = new NgTableParams({count:3},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:data === null ? [] : data.cargos
        });
        self.titulo = data === null ? 'Nueva Comision' : 'Editar Comision';
        self.comision = data === null ? new Object() : data.comision;

        comService.listarCargos(function(response){
            self.cargos = response.data;
            _.remove(self.cargos,function(o){
                return (o.nom == "PRESIDENTE" || o.nom == "PARTICIPANTE");
            })
            
        },function(errResponse){
        });
        
        self.addCargo = function () {
            self.cargosSeleccionados.settings().dataset.push(self.carSel);
            self.cargosSeleccionados.reload();
        }
        self.eliminarCargoSelecionado = function(row){
            _.remove(self.cargosSeleccionados.settings().dataset,function(item){
                return row === item;
            });
            self.cargosSeleccionados.reload();
        }

        function guardar(){
            var data = {
                com : self.comision ,
                car : self.cargosSeleccionados.settings().dataset
            }
            comService.registrarComision(data,function (response) {
                $uibModalInstance.close(response);
            },function (errResponse) {
                $uibModalInstance.dismiss(errResponse);
            })
        }
        function editar(){
            var data = {
                com : self.comision ,
                car : self.cargosSeleccionados.settings().dataset
            }
            comService.editarComision(data,function(response){
                response.data = self.comision;
                $uibModalInstance.close(response);
            },function (errResponse) {
                $uibModalInstance.dismiss(errResponse);
            })
        }

        self.guardar = data === null ? guardar : editar;
        self.cancelar = function(){
            $uibModalInstance.dismiss('cancel');
        }
    }])
    .controller('detalleComisionCtrl',['$log','$routeParams','NgTableParams','modal','UtilAppServices','ComisionService','crud',function($log,$routeParams,NgTableParams,modal,util,comService,crud){
        /*Carga de l*/
        var self = this;
        self.isCollapsed = true;
        self.tablaPersonas= new NgTableParams({count:4},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });
        self.tablaIntegrantes = new NgTableParams({count:8},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });
        self.tablaReuniones = new NgTableParams({count:20},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });
        self.tablaEstructura = new NgTableParams({count:15},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });
        
        var comid = $routeParams.com_id;
        
        //Listar cargos de comision (Estructura)
        comService.listarCargosComision(comid,function (response) {
           if(response.responseSta){
                self.tablaEstructura.settings().dataset = response.data;
                self.tablaEstructura.reload();
            }else{
                modal.mensaje("ERROR",response.responseMsg);
            }
        },function(errResponse){
            modal.mensaje("ERROR","El servidor no responde");
        });
        
        //Listar Integrantes de la comision
        comService.listarIntegrantesComision(comid,function(response){
            if(response.response === 'OK'){
                self.tablaIntegrantes.settings().dataset = response.data;
                self.tablaIntegrantes.reload();
            }else{
                modal.mensaje("ERROR",response.responseMsg);
            }
        },function(errResponse){
            modal.mensaje("ERROR","El servidor no responde");
        });
        
        self.buscarPersona = function(per){
            if(per === undefined || ( (per.dni === undefined || per.dni ==="" )
                && (per.apep === undefined || per.apep === "") && (per.apem === undefined || per.apem === ""))){
                modal.mensaje("ERROR","Debe ingresar al menos uno de los campos de busqueda");
            }else{
                //hacemos la busqueda de las personas
                self.showProgress = true;
                comService.buscarPersonas(per,function(response){
                    self.showProgress = false;
                    if(response.response === 'OK'){
                        self.tablaPersonas.settings().dataset = response.data;
                        self.tablaPersonas.reload();
                    }else if(response.response === 'BAD'){
                        self.tablaPersonas.settings().dataset = [];
                        self.tablaPersonas.reload();
                        modal.mensaje('ERROR',response.responseMsg);
                    }
                },function(errResponse){
                    self.showProgress = false;
                    modal.mensaje("ERROR","El Servidor no responde")
                });
            }
        }
        self.registrarPersona = function(){
            var resolve = {
                data:function(){
                    return{
                        comision : comid,
                        edit: false
                    }
                }
            };
            var modalInstance = util.openModal('nuevaPersona.html','registrarPersonaCtrl','lg','ctrl',resolve);
            modalInstance.result.then(function(response){
                self.tablaIntegrantes.settings().dataset.push(response );
                self.tablaIntegrantes.reload();
            },function (errResponse) {});
        }
        self.agregarIntegrante = function(row){
            comService.registrarIntegranteComision(row.dni,comid,
                function(response){
                    if(response.response === 'OK'){
                        self.tablaIntegrantes.settings().dataset.push(response.data);
                        self.tablaIntegrantes.reload();
                    }else{
                        modal.mensaje("ERROR",response.responseMsg);    
                    }
                },
                function(errResponse){
                    modal.mensaje("ERROR","El servidor no responde");
                }
            );
        }
        self.eliminarIntegrante = function($event,row){
            $log.log('integrante',row);
            $log.log('comision',comid);
            util.openDialog('Eliminar Integrante','¿Seguro que desea eliminar al integrante?',$event,
                function (response) {
                    comService.eliminarElementoComision('comisiones','eliminarIntegranteComision',{dni:row.dni , com:comid},function(response){
                        if(response.response === 'OK'){
                            _.remove(self.tablaIntegrantes.settings().dataset,function(item){
                                return row === item;
                            });
                            self.tablaIntegrantes.reload();
                        }else {
                            modal.mensaje("ERROR",response.responseMsg);
                        }

                    },function(errResponse){
                        modal.mensaje("ERROR","El servidor no responde");
                    });
                },function () {
                    modal.mensaje("CANCELAR","Se cancelo la accion");
                }
            );
        }
        self.editarIntegrante = function(row){
            var resolve = {
                data:function(){
                    return{
                        comision : comid,
                        per: row,
                        edit:true
                    }
                }
            };
            var modalInstance = util.openModal('nuevaPersona.html','registrarPersonaCtrl','lg','ctrl',resolve);
            modalInstance.result.then(function(data){
                angular.extend(row,data);
            },function (ndata) {});
        }
        ////////Reuniones
        comService.listarReunionesComision(comid,function(response){
            if(response.response === 'OK'){
                self.tablaReuniones.settings().dataset = response.data;
                self.tablaReuniones.reload();
            }else{
                modal.mensaje('ERROR',response.responseMsg)
            }
        },function(errResponse){
            modal.mensaje('ERROR','El servidor no responde')
        });
        self.nuevaReunion = function(){
            var resolve = {
                data:function(){
                    return {
                        com: comid,
                        reu:null,
                        edit:false
                    };
                }
            };
            var modalInstance = util.openModal('nuevaReunion.html','registrarReunionCtrl','lg','ctrl',resolve);
            modalInstance.result.then(function(response){

                self.tablaReuniones.settings().dataset.push(response);
                self.tablaReuniones.reload();
            },function(response){
                modal.mensaje("CANCELAR","Se cancelo la accion");
            });
        }
        self.agregarActa = function(row){
            var resolve = {
                data:function(){
                    return {
                        reu: row,
                        act: null
                    };
                }
            };
            var modalInstance = util.openModal('nuevaActa.html','registrarActaCtrl','lg','ctrl',resolve);
            modalInstance.result.then(function(data){
                row.mosAct = true;
                row.act.push(data);
            },function(){});
        }
        self.reporteReunion = function(row){
            var request = crud.crearRequest("reportes",1,"reporteReunion");
            var obj = {reu: row.cod,com:Number(comid)}
            request.setData(obj);
            crud.insertar('/conformacion_comisiones',request,function(response){
                if(response.responseSta){
                    verDocumento(response.data.b64)
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function (errResponse) {
                modal.mensaje('ERROR','El servidor no responde');
            });
        }
        self.editarReunion = function(row){
            var resolve = {
                data:function(){
                    return {
                        com: comid,
                        reu: row,
                        edit:true
                    };
                }
            };

            var modalInstance = util.openModal('nuevaReunion.html','registrarReunionCtrl','lg','ctrl',resolve);
            modalInstance.result.then(function(data){
                angular.extend(row,data);
            },function(ndata){});
        }
        self.registrarAsistencia = function(row){
            comService.listarElementoComision('comisiones','listarAsistenciasReunion',row.cod,
                function(response){
                    if(response.response === 'BAD'){
                        modal.mensaje('ERROR',response.responseMsg);
                    }else{
                        var resolve = {
                            data:function(){
                                return {
                                    par :response.data,
                                    reu: row,
                                    com:comid
                                };
                            }
                        };

                        var modalInstance = util.openModal('asistenciaReunion.html','asistenciaReunionCtrl','lg','ctrl',resolve);
                    }
                },function (errResponse) {
                    modal.mensaje('ERROR','El servidor no responde');
                });
        }
        self.eliminarReunion = function($event,row){
            util.openDialog('Eliminar Reunión','¿Seguro que desea eliminar la reunión?',$event,
                function (response) {
                    comService.eliminarElementoComision('comisiones','eliminarReunion',{cod:row.cod},function(response){
                        if(response.response === 'OK'){
                            _.remove(self.tablaReuniones.settings().dataset,function(item){
                                return row === item;
                            });
                            self.tablaReuniones.reload();
                        }else {
                            modal.mensaje("ERROR",response.responseMsg);
                        }

                    },function(errResponse){
                        modal.mensaje("ERROR","El servidor no responde");
                    });
                },function () {
                    modal.mensaje("CANCELAR","Se cancelo la accion");
                }
            );
        }
        self.eliminarActa = function($event,row,a){
            util.openDialog('¿Seguro que desea eliminar la reunion?','Los acuerdos tambien seran eliminados',$event,
                function (response) {
                    comService.eliminarElementoComision('actas','eliminarActa',{cod:a.cod},function(response){
                        if(response.response === 'OK'){
                            _.remove(row.act,function(item){
                                return a === item;
                            });
                            //self.tablaReuniones.reload();
                        }else {
                            modal.mensaje("ERROR",response.responseMsg);
                        }

                    },function(errResponse){
                        modal.mensaje("ERROR","El servidor no responde");
                    });
                },function () {
                    modal.mensaje("CANCELAR","Se cancelo la accion");
                }
            );
        }
        self.verAcuerdos = function(a){
            comService.listarElementoComision('actas','listarAcuerdos', a.cod,function(response){
                if(response === 'BAD'){
                    modal.mensaje('ERROR',response.responseMsg);
                }else{
                    var resolve = {
                        data:function(){
                            return{
                                acu:response.data,
                                act:a.cod,
                                inte:self.tablaIntegrantes.settings().dataset
                            }
                        }
                    };
                    util.openModal('acuerdosActa.html','acuerdosActaCtrl','lg','ctrl',resolve);
                }
            },function(errResponse){

            });
        }
        /////////////
    }])
    .controller('registrarPersonaCtrl',['$log','crud','$uibModalInstance','data','modal','ComisionService',function($log,crud,$uibModalInstance,data,modal,comService){
        var self = this;
        self.edit = data.edit;
        self.per = data.edit? data.per : new Object();
        
        self.abrirCalendar = false;
        self.openCalendar = function () {
            self.abrirCalendar = true;
        }
        self.dateOptions = {
            formatYear: 'yyyy',
            maxDate: new Date(),
            minDate: new Date(1900,1,1),
            startingDay: 1
        };
        comService.listarCargosComision(Number(data.comision),function (response){
            self.cargos = response.data;
                    _.remove(self.cargos, function (o){
                        return (o.nom == "PRESIDENTE");
                    })
            if(data.edit){
                
                self.index = _.findIndex(self.cargos, function(o) { return o.nom === data.per.car; });
                self.carSel = self.cargos[self.index];
                $log.log('actual',self.index);
            }
        },function (errResponse) {

        });
        self.cancelar = function(){
            $uibModalInstance.dismiss('cancel');
        }
        self.guardar = data.edit? editar : guardar;
        
        function guardar(){
            if(self.nac !== undefined && self.nac !== null)
                self.per.nac = self.nac.getTime();
            self.per.car = self.carSel.nom;
            comService.registrarPersona(data.comision,self.carSel.cod,self.per,function(response){
                if(response.response === 'OK'){
                    modal.mensaje("CORRECTO",response.responseMsg);
                    angular.extend(self.per,response.data);
                    $log.log('nueva persona',self.per);
                    $uibModalInstance.close(self.per);
                }else if(response.response === 'BAD'){
                    modal.mensaje("ERROR",response.responseMsg);
                    //$uibModalInstance.dismiss('cancel');
                }
            },function(errResponse){
                $log.log('error',errResponse);
                modal.mensaje("ERROR","El servidor no responde");
            });
        }
        function editar(){
            var request = crud.crearRequest("comisiones",1,"editarIntegrante");
            self.per.car = self.carSel.cod;
            self.per.com =  data.comision;
            request.setData(self.per);
            $log.log('persona enviada',self.per);
            crud.actualizar('/conformacion_comisiones',request,function(response){
                if(response.response === 'BAD'){
                    modal.mensaje('ERRROR',response.responseMsg)
                }else{
                    self.per.car = self.carSel.nom;
                    $uibModalInstance.close(self.per);
                }
            },function(errResponse){});
        }
    }])
    .controller('registrarReunionCtrl',['$log','$uibModalInstance','NgTableParams','data','modal','ComisionService',function($log,$uibModalInstance,NgTableParams,data,modal,comService){
        var self = this;
        self.hor = !data.edit? new Date() : new Date(data.reu.fec);
        self.fec = !data.edit? new Date() : new Date(data.reu.fec);
        self.reu = !data.edit? new Object() : angular.copy(data.reu);

        self.esMeridiano = true;
        self.abrirCalendar = false;
        self.openCalendar = function () {
            self.abrirCalendar = true;
        }
        self.dateOptions = {
            formatYear: 'yyyy',
            maxDate: new Date(2050,12,30),
            minDate: new Date(1900,1,1),
            startingDay: 1
        };

        self.guardar = function () {
            if(self.fec !== undefined && self.fec !== null){

                self.fec.setHours(self.hor.getHours());
                self.fec.setMinutes(self.hor.getMinutes());
                self.reu.fec = self.fec.getTime();
            }
            self.reu.com = data.com;
            if(data.reu === null){
                comService.registrarReunion(self.reu,function(response){
                    if(response.response === 'OK'){
                        modal.mensaje("CORRECTO",response.responseMsg);
                        $uibModalInstance.close(response.data);
                    }else if(response.response === 'BAD'){
                        modal.mensaje("ERROR",response.responseMsg);
                        //$uibModalInstance.dismiss('cancel');
                    }
                },function(errResponse){
                    modal.mensaje("ERROR","El servidor no responde");
                });
            }else{
                comService.editarElemento('comisiones','editarReunion',self.reu,function(response){
                    if(response.response === 'OK'){
                        $uibModalInstance.close(self.reu);
                    }else{
                        modal.mensaje('ERROR',response.responseMsg);
                    }
                },function(errResponse){
                    modal.mensaje("ERROR","El servidor no responde");
                });
            }

        }

        self.cancelar = function(){
            $uibModalInstance.dismiss('cancel');
        }
    }])
    .controller('registrarActaCtrl',['$log','$uibModalInstance','NgTableParams','data','modal','ComisionService',function($log,$uibModalInstance,NgTableParams,data,modal,comService){
        var self = this;
        self.guardar = function(){
            self.act.reu = data.reu.cod;
            comService.registrarActa(self.act,function(response){
                if(response.response === 'OK'){
                    $uibModalInstance.close(response.data);
                }else{
                    modal.mensaje("ERROR",response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje("ERROR","El servidor no funciona");
            });
        }
        self.cancelar = function(){
            $uibModalInstance.dismiss('cancel');
        }


    }])
    .controller('acuerdosActaCtrl',['$log','$uibModalInstance','NgTableParams','data','modal','ComisionService',function($log,$uibModalInstance,NgTableParams,data,modal,comService){
        var self = this;
        
        var originalData = angular.copy(data.acu);
        var delItems = [];
        self.deleteCount = 0;
        self.integrantes = data.inte;
        self.tablaAcuerdos = new NgTableParams({count:5},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset: angular.copy(data.acu)
        });
        self.cancelar = function(){
            $uibModalInstance.dismiss('cancel');
        }
        self.mostrarFecha = function(row){
            if(row.est){
                row.dat = new Date().getTime();
                //row.fec = row.dat.getTime();
            }else {
                row.dat = null;
                //row.fec = -1;
            }
        }

        function resetTableStatus() {
            self.isEditing = false;
            self.isAdding = false;
            self.deleteCount = 0;
            self.tableForm.$setPristine();
        }
        self.add = function() {
            self.isEditing = true;
            self.isAdding = true;
            self.tablaAcuerdos.settings().dataset.unshift({
                des: "",
                dat: null,
                est: false,
                afav:0,
                econ:0,
                obs:"",
                fec:-1
            });

            self.tablaAcuerdos.sorting({});
            self.tablaAcuerdos.page(1);
            self.tablaAcuerdos.reload();
        }
        self.cancelChanges = function() {
            resetTableStatus();
            var currentPage = self.tablaAcuerdos.page();
            self.tablaAcuerdos.settings({
                dataset: angular.copy(originalData)
            });
            
            if (!self.isAdding) {
                self.tablaAcuerdos.page(currentPage);
            }
        }
        self.hasChanges = function() {
            return self.tableForm.$dirty || self.deleteCount > 0
        }

        self.saveChanges = function() {
            resetTableStatus();
            originalData = angular.copy(self.tablaAcuerdos.settings().dataset);
        }
        self.del = function(row) {
            if(row.cod != undefined && row.cod != null){
                delItems.push(row.cod);
            }
            _.remove(self.tablaAcuerdos.settings().dataset, function(item) {
                return row === item;
            });
            self.deleteCount++;
            self.tablaAcuerdos.reload().then(function(data) {
                if (data.length === 0 && self.tablaAcuerdos.total() > 0) {
                    self.tablaAcuerdos.page(self.tablaAcuerdos.page() - 1);
                    self.tablaAcuerdos.reload();
                }
            });
        }
        self.guardar = function(){
            var sendData = {del:delItems,arr:self.tablaAcuerdos.settings().dataset}
            comService.registrarAcuerdos(data.act,sendData,function(response){
                if(response.response === 'OK'){
                    $uibModalInstance.close(response.data);
                }else if(response.response === 'BAD'){
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El Servidor no responde');
            });
        }
    }])
    .controller('asistenciaReunionCtrl',['$log','$uibModalInstance','NgTableParams','data','modal','ComisionService',function($log,$uibModalInstance,NgTableParams,data,modal,comService){
        var self = this;
        var elementosCambiados = [];
        self.tablaIntegrantes = new NgTableParams({count:10},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset: data.par
        });
        self.cancelar = function () {
            $uibModalInstance.dismiss('cancel');
        }
        self.addAsistencia = function (row) {
            if(elementosCambiados.length === 0) {
                elementosCambiados.push({cod:row.cod,reu:data.reu.cod,est:row.est});
                return;
            }
            for( var i = 0; i < elementosCambiados.length; i++) {
                if (elementosCambiados[i].cod === row.cod) {
                    elementosCambiados.splice(i, 1);
                    return;
                }                
            }             
            elementosCambiados.push({cod:row.cod,reu:data.reu.cod,est:row.est});
        }
        self.guardar = function () {
            comService.registrarAsistencia(data.com,elementosCambiados,function(response){
                if(response.response === 'OK'){
                    $uibModalInstance.close('close');
                }else{
                    $uibModalInstance.dismiss('cancel');
                }
            },function (errResponse) {
                
            });
        }
    }]);