app.controller("AyudaBasicaCtrl",["$rootScope","$scope","crud","modal", function ($rootScope,$scope,crud,modal) {
       $scope.mod_selec={};
    $scope.sub_mod_selec={};
    $scope.func_selec={};
    
 
    $scope.mostrarBotonAgregarAyuda=true;
    $scope.mostrarBotonesPasos=true;

    $scope.descripcionFuncionalidad="";

 
    $scope.tipoAyuda="General";//esto cambia con el valor de los radiobutton
    $scope.subTipoMensaje="GuiaPasos";//esto cambia con el valor de los radiobutton
    $scope.ayuda={funcionId:0,ayudaTipo:"",ayudaEstReg:""};
    $scope.paso={pasoDes:"",edi:false,pasoEstReg:""};
    $scope.pasos=[];

     
        
    $scope.funcionalidadRuta={
        id_mod: 0,
        id_submod: 0,
        id_func: 0
    }
    $scope.cambiarVistaUsuario=function(){
      //TEMPORAL, SOLO PARA PRUEBAS
      if($rootScope.usuMaster.rol.rolID != 1){//Usuario
        $scope.mostrarUsuario=true;
        $scope.mostrarAdministrador=false;
      }else if($rootScope.usuMaster.rol.rolID ==1){//Administrador
        $scope.mostrarAdministrador=true;
        $scope.mostrarUsuario=false; 
      }
  
  };
    $scope.cambioModulo=function(sub_mod_selec, func_selec){
      sub_mod_selec={};
      func_selec={};
      pasos={};
    };
    $scope.cambioSubMod=function(func_selec){
        func_selec={};
    };
    
    $scope.cargarPasos=function (func_selec){        
      var request=crud.crearRequest('mantenimiento',1,'listarPasosDeFuncionalidad');
        if($scope.tipoAyuda=="General")
          $scope.ayuda.ayudaTipo="G";
        else if ($scope.tipoAyuda="Detallada")
          $scope.ayuda.ayudaTipo="D";
      
        if(func_selec){
            request.setData({funcionId:func_selec.funcionID, ayudaTipo:$scope.ayuda.ayudaTipo});

            crud.listar('/mantenimiento',request,function(data){
                if(data.data.pasos.length > 0){
                    $scope.pasos=data.data.pasos;
                    $scope.descripcion=data.data.desc;
                    $scope.mostrarBotonEliminarAyuda=true;
                    $scope.mostrarBotonesPasos=false;
                    $scope.mostrarBotonAgregarAyuda=false;
                }else{
                    $scope.mostrarBotonEliminarAyuda=false;
                    $scope.mostrarBotonesPasos=true;
                    $scope.mostrarBotonAgregarAyuda=true;
                    $scope.pasos=[];
                    $scope.descripcion="";
                }

          },function(data){
              console.info(data);
          });
          
        }else{
            $scope.pasos=[];
            $scope.mostrarBotonAgregarAyuda=true;
            $scope.mostrarBotonesPasos=true;
            $scope.mostrarBotonEliminarAyuda=false;
        }
      
    };
    
    $scope.cambiarTipoAyuda=function(func_selec){
  
      $scope.cargarPasos(func_selec);          
    };
    $scope.agregarAyuda=function(func_selec){
        if($scope.pasos.length>0 && func_selec.funcionID){
            var request=crud.crearRequest('mantenimiento',1,'insertarAyuda');
            //$scope.ayuda.funcionId=$scope.funcionalidadRuta.id_func            
            
            $scope.ayuda.funcionId=(func_selec.funcionID);
            console.log($scope.ayuda.funcionId);
            
            $scope.ayuda.ayudaEstReg='1';      
              if($scope.tipoAyuda=="General")
                $scope.ayuda.ayudaTipo="G";
            else if ($scope.tipoAyuda="Detallada")
                $scope.ayuda.ayudaTipo="D";

            $scope.ayuda.pasos=$scope.pasos;
            $scope.ayuda.descripcion=$scope.descripcion;
            request.setData($scope.ayuda);      
            crud.insertar("/mantenimiento",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                $scope.mostrarBotonAgregarAyuda=false;//Ocultamos boton agregar
                $scope.mostrarBotonEliminarAyuda=true;//Mostramos boton eliminar
                $scope.mostrarBotonesPasos=false;//Ocultamos botones editar y eliminar de cada paso
            },function(data){
                console.info(data);
            });            
        }else {
            modal.mensaje("CONFIRMACION","No se llenÃ³ la informaciÃ³n correcta");
        }
      
    };

    $scope.agregarPaso=function(tipo){
        if(tipo){//Si se hace la ediciÃ³n, REVISAR tipoTramiteCtrl.js

        }else{//Si se seta insertado
            if($scope.paso.pasoDes==""){
                modal.mensaje=("CONFIRMACION","Ingrese descripciÃ³n del paso");
                return;
            }      
            $scope.paso.pasoEstReg="1";
            $scope.pasos.push($scope.paso);
            $scope.paso={pasoDes:"",pasoEstReg:""};        
        }
    };
    $scope.editarPaso=function(i,r){
        //Si se esta editando, presiona el boton confirmacion de cambio
        if(r.edi){
            $scope.pasos[i] = r.copia;
        }else{
            
            r.copia=JSON.parse(JSON.stringify(r));
            r.edi=true;
        }
    };
    $scope.eliminarPaso=function(i,r){
        //Si se esta cancelando la ediciÃ³n
        if(r.edi){
            r.edi=false;
            delete r.copia;
        }else{//Si eliminamos el elemento
            $scope.pasos.splice(i,1);
        }
    };
    $scope.eliminarAyuda=function(func_selec){
        //Se cambiara de estado de registro en ayuda y los pasos asociados
        var request=crud.crearRequest('mantenimiento',1,'eliminarAyuda');
      $scope.ayuda.funcionId=func_selec.funcionID;
      $scope.ayuda.ayudaEstReg='1';      
        if($scope.tipoAyuda=="General")
          $scope.ayuda.ayudaTipo="G";
      else if ($scope.tipoAyuda="Detallada")
          $scope.ayuda.ayudaTipo="D";
      
      $scope.ayuda.pasos=$scope.pasos;
      request.setData($scope.ayuda);      
      crud.eliminar("/mantenimiento",request,function(response){
                $scope.pasos=[];
                $scope.mostrarBotonEliminarAyuda=false;
                $scope.mostrarBotonAgregarAyuda=true;
                $scope.mostrarBotonesPasos=true;
                modal.mensaje("CONFIRMACION",response.responseMsg);
            },function(data){
                console.info(data);
            });
  
    };
   

  
//  $scope.preparaFuncionEnMensaje=function(func_selec){
//      $scope.mensajeEnviar.funcionID=func_selec.funcionID;
//  }

   $scope.dt=new Date();
  $scope.clear = function() {
    $scope.dt = null;
  };

  $scope.dateOptions = {
    dateDisabled: disabled,
    formatYear: 'yy',
    maxDate: new Date(2020, 5, 22),
    minDate: new Date(),
    startingDay: 1
  };

  

    // Disable weekend selection
  function disabled(data) {
    var date = data.date,
      mode = data.mode;
    return mode === 'day' && (date.getDay() === 0 );
  }

  $scope.open2 = function() {
    $scope.popup2.opened = true;
  };
  $scope.popup2 = {
    opened: false
  };

 $scope.model = {
    name: 'Tabs'
  };
}]);
