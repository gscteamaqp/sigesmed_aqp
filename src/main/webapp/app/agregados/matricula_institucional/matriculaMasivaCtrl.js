app.controller("matriculaMasivaCtrl", ["$scope", "$rootScope", "crud", "NgTableParams", "modal", function ($scope, $rootScope, crud, NgTableParams, modal) {

        var paramsMatriculaMasiva = {count: 10};
        var settingMatriculaMasiva = {counts: []};
        $scope.tablaPrincipal = new NgTableParams(paramsMatriculaMasiva, settingMatriculaMasiva);
        $scope.estMatNomSelect = [{id: true, title: "Activa"}, {id: false, title: "Inactiva"}];

        $scope.myusuario = $rootScope.usuMaster.usuario.usuarioID;
        $scope.myOrganizacion = $rootScope.usuMaster.organizacion.organizacionID;

        $scope.arrays = {
            estudiantes: [],
            DNIsElegidos: [],
            estudiantesElegidos: [],
            organizacionesElegidas: [],
            estudiantesNombres: [],
            estudiantesEstadoMat: [],
            gradosySecciones: [],
            niveles: [],
            jornadas: [],
            turnos: [],
            grados: [],
            secciones: [],
            estudiantesError: [],
            estudiantesMatriculados: []
        };

        $scope.busqueda = {
            nivelBusqueda: "1",
            gradoBusqueda: "0",
            seccionBusqueda: "*",
            orgBusqueda: "1",
            orgUsuario: $scope.myOrganizacion
        };

        $scope.filtros = {
            inicialSelected: false,
            primariaSelected: true,
            secundariaSelected: false
        };

        $scope.reloadFiltro = function () {
            $scope.filtros.inicialSeleted = $scope.busqueda.nivelBusqueda === "0";
            $scope.filtros.primariaSelected = $scope.busqueda.nivelBusqueda === "1";
            $scope.filtros.secundariaSelected = $scope.busqueda.nivelBusqueda === "2";
        };

        $scope.datosVacantes = {
            vacantesxGrado: 0,
            ultGraCul: 0,
            sigGra: 1
        };

        $scope.matriculaMasivaCreate = {
            numEstSel: 0, //numero de estudiantes seleccionados
            estMatNom: "EN PROCESO", //estado de la matricula por defecto
            estmatFec: new Date(), //fecha de la matricula
            conMat: '1', //condicion de la matricula
            matObs: "", //observaciones
            sitMat: "1", //sitacion de matricula
            idEstudiantes: [],
            idOrganizaciones: [],
            idDNIs: [],
            estNombres: [],
            estEstMat: [],
            matRegId: $scope.myusuario,
            orgDesId: $scope.myOrganizacion,
            nivId: -1,
            jorEscId: -1,
            turId: -1,
            graId: -1,
            usuMod: $scope.myusuario
        };

        $scope.paramsNomina = {
            nombreSeccion: "",
            periodoLecIni: new Date(),
            periodoLecFin: new Date(),
            orgId: $scope.myOrganizacion,
            nivId: -1,
            jorEscId: -1,
            turId: -1,
            graId: -1,
            secId: -1
        };

        $scope.buscarEstudiante = function () {
            var request = crud.crearRequest('matriculaMasiva', 1, 'buscarVariosEstudiantes');
            request.setData($scope.busqueda);
            crud.listar("/matriculaInstitucional", request, function (data) {
                $scope.arrays.estudiantes = data.data;
                settingMatriculaMasiva.dataset = data.data;
                iniciarPosiciones(settingMatriculaMasiva.dataset);
                $scope.tablaPrincipal.settings(settingMatriculaMasiva);
                $scope.arrays.estudiantesElegidos = [];
                $scope.arrays.organizacionesElegidas = [];
                $scope.arrays.estudiantesNombres = [];
                $scope.arrays.estudiantesEstadoMat = [];
                $scope.arrays.DNIsElegidos = [];

                $scope.datosVacantes.ultGraCul = parseInt($scope.busqueda.gradoBusqueda);
                $scope.datosVacantes.sigGra = $scope.datosVacantes.ultGraCul + 1;

                if (data.responseSta) {
                    if ($scope.arrays.estudiantes.length === 0) {
                        modal.mensaje("Busqueda vacia", "No se encontraron estudiantes");
                    }
                } else {
                }
            }, function (data) {
                console.info(data);
            });
        };

        $scope.openModalLotes = function () {
            $scope.matriculaMasivaCreate.numEstSel = $scope.arrays.estudiantesElegidos.length;
            if ($scope.matriculaMasivaCreate.numEstSel <= 0) {
                modal.mensaje('Error', 'No ha seleccionado Ningun Estudiante');
            } else {
                $scope.cargarGradosySecciones();
                $scope.datosVacantes.vacantesxGrado = 0;
                $scope.matriculaMasivaCreate.nivId = -1;
                $scope.matriculaMasivaCreate.jorEscId = -1;
                $scope.matriculaMasivaCreate.turId = -1;
                $scope.matriculaMasivaCreate.graId = -1;
                $scope.cargarNiveles();

                $("#modalMatricula").modal("toggle");
            }
        };

        $scope.openModalNomina = function () {
            $scope.cargarGradosySecciones();
            $scope.paramsNomina.nivId = -1;
            $scope.paramsNomina.jorEscId = -1;
            $scope.paramsNomina.turId = -1;
            $scope.paramsNomina.graId = -1;
            $scope.paramsNomina.secId = -1;
            $scope.cargarNiveles2Nomina();
            $("#modalNomina").modal("toggle");
        };

        $scope.generarNominaMatricula = function () {
            if ($scope.paramsNomina.nivId === -1 ||
                    $scope.paramsNomina.jorEscId === -1 ||
                    $scope.paramsNomina.turId === -1 ||
                    $scope.paramsNomina.graId === -1 ||
                    $scope.paramsNomina.secId === -1) {
                modal.mensaje("Error", "Seleccione un Grado correctamente!");
                return;
            }

            var request = crud.crearRequest('matriculaMasiva', 1, 'generarNominaMatricula');

            request.setData($scope.paramsNomina);
            crud.insertar("/matriculaInstitucional", request, function (data) {
                if (data.responseSta) {
                    modal.mensaje("Operacion Exitosa", "Nomina de Matricula Generada");
                    $("#modalNomina").modal("hide");
                    var path = "../archivos/matricula_institucional/nominaMatricula/nominaMatricula_" + $scope.paramsNomina.orgId + $scope.paramsNomina.nivId + $scope.paramsNomina.jorEscId + $scope.paramsNomina.turId + $scope.paramsNomina.graId + $scope.paramsNomina.secId + ".pdf";
                    window.open(path);
                } else {
                    modal.mensaje("Error", "Error al Generar la Nomina de Matricula");
                    $('#modalNomina').modal('hide');
                }
            }, function (data) {
                console.info(data);
            });
        };

        $scope.generarMatriculaMasiva = function () {

            if ($scope.matriculaMasivaCreate.nivId === -1 ||
                    $scope.matriculaMasivaCreate.jorEscId === -1 ||
                    $scope.matriculaMasivaCreate.turId === -1 ||
                    $scope.matriculaMasivaCreate.graId === -1) {
                modal.mensaje("Error", "Seleccione un Grado correctamente!");
                return;
            }

            var request = crud.crearRequest('matriculaMasiva', 1, 'generarMatriculaMasiva');

            $scope.matriculaMasivaCreate.idEstudiantes = JSON.stringify($scope.arrays.estudiantesElegidos);
            $scope.matriculaMasivaCreate.idDNIs = JSON.stringify($scope.arrays.DNIsElegidos);
            $scope.matriculaMasivaCreate.idOrganizaciones = JSON.stringify($scope.arrays.organizacionesElegidas);
            $scope.matriculaMasivaCreate.estNombres = JSON.stringify($scope.arrays.estudiantesNombres);
            $scope.matriculaMasivaCreate.estEstMat = JSON.stringify($scope.arrays.estudiantesEstadoMat);

            request.setData($scope.matriculaMasivaCreate);

            crud.insertar("/matriculaInstitucional", request, function (data) {
                if (data.responseSta) {
                    modal.mensaje("Operacion Exitosa", "Matriculas Generadas");
                    $('#modalMatricula').modal('hide');
                    $scope.buscarEstudiante();
                } else {
                    $('#modalMatricula').modal('hide');
                    $scope.arrays.estudiantesError = data.data.arrayErrores;
                    $scope.arrays.estudiantesMatriculados = data.data.arrayMatriculados;
                    $('#modalError').modal('toggle');
                    $scope.buscarEstudiante();
                }

            }, function (data) {
                console.info(data);
            });
        };

        $scope.cargaInicial = function () {
            $scope.cargarGradosySecciones();
            $scope.reloadFiltro();
        };

        $scope.changeCheck = function (checked, estestId, estOrgId, estNom, estApePat, estApeMat, estMat, estDNI) {
            if (checked) {
                $scope.arrays.estudiantesElegidos.push(estestId);
                $scope.arrays.organizacionesElegidas.push(estOrgId);
                $scope.arrays.estudiantesNombres.push(estNom + " " + estApePat + " " + estApeMat);
                $scope.arrays.estudiantesEstadoMat.push(estMat);
                $scope.arrays.DNIsElegidos.push(estDNI);
            } else {
                var i = $scope.arrays.estudiantesElegidos.indexOf(estestId);
                if (i !== -1) {
                    $scope.arrays.estudiantesElegidos.splice(i, 1);
                    $scope.arrays.organizacionesElegidas.splice(i, 1);
                    $scope.arrays.estudiantesNombres.splice(i, 1);
                    $scope.arrays.estudiantesEstadoMat.splice(i, 1);
                    $scope.arrays.DNIsElegidos.splice(i, 1);
                }
            }
        };

        $scope.cargarGradosySecciones = function () {
            var itmRequest = {
                orgIdUser: $scope.myOrganizacion
            };
            var request = crud.crearRequest('matriculaIndividual', 1, 'listarGradosySecciones');
            request.setData(itmRequest);
            crud.listar("/matriculaInstitucional", request, function (data) {
                $scope.arrays.gradosySecciones = data.data;
            }, function (data) {
                console.info(data);
            });
        };

        $scope.cargarNiveles = function () {
            var size = $scope.arrays.gradosySecciones.length;
            var tempID, tempNom, obj, itmRepeat, graDis;
            var flagRepeat = true;
            $scope.arrays.niveles = [];
            $scope.arrays.jornadas = [];
            $scope.arrays.turnos = [];
            $scope.arrays.grados = [];

            $scope.datosVacantes.vacantesxGrado = 0;
            for (var i = 0; i < size; i++) {
                tempID = $scope.arrays.gradosySecciones[i].nivelId;
                tempNom = $scope.arrays.gradosySecciones[i].nivelNom;
                graDis = $scope.arrays.gradosySecciones[i].gradoId;

                if (parseInt(graDis) !== $scope.datosVacantes.sigGra) {
                    continue;
                }
                obj = {
                    nivelId: tempID,
                    nivelNom: tempNom
                };
                for (var j = 0; j < $scope.arrays.niveles.length; j++) {
                    itmRepeat = $scope.arrays.niveles[j];
                    if (obj.nivelId === itmRepeat.nivelId) {
                        flagRepeat = false;
                    }
                }
                if (flagRepeat) {
                    $scope.arrays.niveles.push(obj);
                }
                flagRepeat = true;
            }
            $scope.cargarJornada();            
        };

        $scope.cargarJornada = function () {
            var size = $scope.arrays.gradosySecciones.length;
            var obj, itm, itmRepeat;
            var flagRepeat = true;
            $scope.arrays.jornadas = [];
            $scope.arrays.turnos = [];
            $scope.arrays.grados = [];
            $scope.datosVacantes.vacantesxGrado = 0;
            for (var i = 0; i < size; i++) {
                itm = $scope.arrays.gradosySecciones[i];
                if (parseInt(itm.gradoId) !== $scope.datosVacantes.sigGra) {
                    continue;
                }
                if (itm.nivelId === $scope.matriculaMasivaCreate.nivId) {
                    obj = {
                        jornadaId: itm.jornadaId,
                        jornadaNom: itm.jornadaNom
                    };
                    for (var j = 0; j < $scope.arrays.jornadas.length; j++) {
                        itmRepeat = $scope.arrays.jornadas[j];
                        if (obj.jornadaId === itmRepeat.jornadaId) {
                            flagRepeat = false;
                        }
                    }
                    if (flagRepeat) {
                        $scope.arrays.jornadas.push(obj);
                    }
                    flagRepeat = true;
                }
            }
            $scope.cargarTurno();
        };

        $scope.cargarTurno = function () {
            var size = $scope.arrays.gradosySecciones.length;
            var obj, itm, itmRepeat;
            var flagRepeat = true;
            $scope.arrays.turnos = [];
            $scope.arrays.grados = [];
            $scope.datosVacantes.vacantesxGrado = 0;
            for (var i = 0; i < size; i++) {
                itm = $scope.arrays.gradosySecciones[i];
                if (parseInt(itm.gradoId) !== $scope.datosVacantes.sigGra) {
                    continue;
                }
                if (itm.jornadaId === $scope.matriculaMasivaCreate.jorEscId) {
                    obj = {
                        turnoId: itm.turnoId,
                        turnoNom: itm.turnoNom
                    };
                    for (var j = 0; j < $scope.arrays.turnos.length; j++) {
                        itmRepeat = $scope.arrays.turnos[j];
                        if (obj.turnoId === itmRepeat.turnoId) {
                            flagRepeat = false;
                        }
                    }
                    if (flagRepeat) {
                        $scope.arrays.turnos.push(obj);
                    }
                    flagRepeat = true;
                }
            }
            $scope.cargarGrados();
        };

        $scope.cargarGrados = function () {
            var size = $scope.arrays.gradosySecciones.length;
            var obj, itm, itmRepeat;
            var flagRepeat = true;
            $scope.arrays.grados = [];
            for (var i = 0; i < size; i++) {
                itm = $scope.arrays.gradosySecciones[i];
                if (parseInt(itm.gradoId) !== $scope.datosVacantes.sigGra) {
                    continue;
                }
                if (itm.turnoId === $scope.matriculaMasivaCreate.turId) {
                    obj = {
                        gradoSecId: itm.gradoSecId,
                        planNivelId: itm.planNivelId,
                        gradoId: itm.gradoId,
                        gradoNom: itm.gradoNom
                    };
                    for (var j = 0; j < $scope.arrays.grados.length; j++) {
                        itmRepeat = $scope.arrays.grados[j];
                        if (obj.gradoId === itmRepeat.gradoId) {
                            flagRepeat = false;
                        }
                    }
                    if (flagRepeat) {
                        $scope.arrays.grados.push(obj);
                    }
                    flagRepeat = true;
                }
            }
            $scope.cargarVacantes();
        };

        $scope.cargarVacantes = function () {
            var size = $scope.arrays.gradosySecciones.length;
            var itm;
            var vacantesXGrado = 0;
            for (var i = 0; i < size; i++) {
                itm = $scope.arrays.gradosySecciones[i];
                if (itm.gradoId === $scope.matriculaMasivaCreate.graId &&
                        itm.turnoId === $scope.matriculaMasivaCreate.turId &&
                        itm.jornadaId === $scope.matriculaMasivaCreate.jorEscId &&
                        itm.nivelId === $scope.matriculaMasivaCreate.nivId) {
                    vacantesXGrado = vacantesXGrado + (itm.gradoSecNumMax - itm.gradoSecMat);
                }
            }
            $scope.datosVacantes.vacantesxGrado = vacantesXGrado;
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };

        $scope.altInputFormats = ['M!/d!/yyyy'];

        $scope.popup1 = {
            opened: false
        };

        $scope.popup2 = {
            opened: false
        };

        $scope.open2 = function () {
            $scope.popup2.opened = true;
        };

        $scope.popup3 = {
            opened: false
        };

        $scope.open3 = function () {
            $scope.popup3.opened = true;
        };

        $scope.cargarNiveles2Nomina = function () {
            var size = $scope.arrays.gradosySecciones.length;
            var tempID, tempNom, obj, itmRepeat;
            var flagRepeat = true;
            $scope.arrays.niveles = [];
            $scope.arrays.jornadas = [];
            $scope.arrays.turnos = [];
            $scope.arrays.grados = [];
            $scope.arrays.secciones = [];

            for (var i = 0; i < size; i++) {
                tempID = $scope.arrays.gradosySecciones[i].nivelId;
                tempNom = $scope.arrays.gradosySecciones[i].nivelNom;
                obj = {
                    nivelId: tempID,
                    nivelNom: tempNom
                };
                for (var j = 0; j < $scope.arrays.niveles.length; j++) {
                    itmRepeat = $scope.arrays.niveles[j];
                    if (obj.nivelId === itmRepeat.nivelId) {
                        flagRepeat = false;
                    }
                }
                if (flagRepeat) {
                    $scope.arrays.niveles.push(obj);
                }
                flagRepeat = true;
            }
            $scope.cargarJornada2Nomina();
        };

        $scope.cargarJornada2Nomina = function () {
            var size = $scope.arrays.gradosySecciones.length;
            var obj, itm, itmRepeat;
            var flagRepeat = true;
            $scope.arrays.jornadas = [];
            $scope.arrays.turnos = [];
            $scope.arrays.grados = [];
            $scope.arrays.secciones = [];

            for (var i = 0; i < size; i++) {
                itm = $scope.arrays.gradosySecciones[i];
                if (itm.nivelId === $scope.paramsNomina.nivId) {
                    obj = {
                        jornadaId: itm.jornadaId,
                        jornadaNom: itm.jornadaNom
                    };
                    for (var j = 0; j < $scope.arrays.jornadas.length; j++) {
                        itmRepeat = $scope.arrays.jornadas[j];
                        if (obj.jornadaId === itmRepeat.jornadaId) {
                            flagRepeat = false;
                        }
                    }
                    if (flagRepeat) {
                        $scope.arrays.jornadas.push(obj);
                    }
                    flagRepeat = true;
                }
            }
            $scope.cargarTurno2Nomina();
        };

        $scope.cargarTurno2Nomina = function () {
            var size = $scope.arrays.gradosySecciones.length;
            var obj, itm, itmRepeat;
            var flagRepeat = true;
            $scope.arrays.turnos = [];
            $scope.arrays.grados = [];
            $scope.arrays.secciones = [];

            for (var i = 0; i < size; i++) {
                itm = $scope.arrays.gradosySecciones[i];
                if (itm.jornadaId === $scope.paramsNomina.jorEscId) {
                    obj = {
                        turnoId: itm.turnoId,
                        turnoNom: itm.turnoNom
                    };
                    for (var j = 0; j < $scope.arrays.turnos.length; j++) {
                        itmRepeat = $scope.arrays.turnos[j];
                        if (obj.turnoId === itmRepeat.turnoId) {
                            flagRepeat = false;
                        }
                    }
                    if (flagRepeat) {
                        $scope.arrays.turnos.push(obj);
                    }
                    flagRepeat = true;
                }
            }
            $scope.cargarGrados2Nomina();
        };

        $scope.cargarGrados2Nomina = function () {
            var size = $scope.arrays.gradosySecciones.length;
            var obj, itm, itmRepeat;
            var flagRepeat = true;
            $scope.arrays.grados = [];
            $scope.arrays.secciones = [];

            for (var i = 0; i < size; i++) {
                itm = $scope.arrays.gradosySecciones[i];
                if (itm.turnoId === $scope.paramsNomina.turId) {
                    obj = {
                        gradoId: itm.gradoId,
                        gradoNom: itm.gradoNom
                    };
                    for (var j = 0; j < $scope.arrays.grados.length; j++) {
                        itmRepeat = $scope.arrays.grados[j];
                        if (obj.gradoId === itmRepeat.gradoId) {
                            flagRepeat = false;
                        }
                    }
                    if (flagRepeat) {
                        $scope.arrays.grados.push(obj);
                    }
                    flagRepeat = true;
                }
            }
            $scope.cargarSecciones2Nomina();
        };

        $scope.cargarSecciones2Nomina = function () {
            var size = $scope.arrays.gradosySecciones.length;
            var obj, itm, itmRepeat;
            var flagRepeat = true;
            $scope.arrays.secciones = [];

            for (var i = 0; i < size; i++) {
                itm = $scope.arrays.gradosySecciones[i];
                if (itm.gradoId === $scope.paramsNomina.graId) {
                    obj = {
                        seccionId: itm.seccId,
                        seccionNom: itm.seccNom

                    };
                    for (var j = 0; j < $scope.arrays.secciones.length; j++) {
                        itmRepeat = $scope.arrays.secciones[j];
                        if (obj.seccionId === itmRepeat.seccionId) {
                            flagRepeat = false;
                        }
                    }
                    if (flagRepeat) {
                        $scope.arrays.secciones.push(obj);
                    }
                    flagRepeat = true;
                }
            }
        };


    }]);


