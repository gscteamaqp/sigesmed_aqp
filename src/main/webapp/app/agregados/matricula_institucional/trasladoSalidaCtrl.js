app.controller("trasladoSalidaCtrl", ["$scope", "$rootScope", "crud", "NgTableParams", "modal", function ($scope, $rootScope, crud, NgTableParams, modal) {

        $scope.myusuario = $rootScope.usuMaster.usuario.usuarioID;
        $scope.orgId = $rootScope.usuMaster.organizacion.organizacionID;
        $scope.orgNom = $rootScope.usuMaster.organizacion.nombre;

        var paramsTrasladoSalida = {count: 10};
        var settingTrasladoSalida = {counts: []};
        $scope.tablaPrincipal = new NgTableParams(paramsTrasladoSalida, settingTrasladoSalida);
        $scope.estadoTraslado = [{id: 'P', title: "En Proceso"}, {id: 'A', title: "Aseptada"}, {id: 'R', title: "Rechazada"}];
        $scope.tipoTraslado = [{id: 1, title: "Por Cambio de Nivel"}, {id: 2, title: "Por Cambio de Año"}, {id: 3, title: "En el Mismo año"}];

        $scope.trasladoSalidaCreate = {
            traIngEst: "EN PROCESO",
            traIngId: -1,
            traIngEstNom: "",
            traIngResolucion: "",
            traIngOrgDesNom: "",
            traIngFec: "",
            traIngTipNom: "",
            traSalEst: "A",
            traSalObs: "",
            usuMod: $scope.myusuario
        };


        $scope.buscarTraslado = function () {
            var busqueda = {
                orgOriId: $scope.orgId
            };
            var request = crud.crearRequest('trasladoSalida', 1, 'listarTrasladosIngresos');
            request.setData(busqueda);
            crud.listar("/matriculaInstitucional", request, function (data) {
                settingTrasladoSalida.dataset = data.data;
                iniciarPosiciones(settingTrasladoSalida.dataset);
                $scope.tablaPrincipal.settings(settingTrasladoSalida);
                if (data.responseSta) {
                    if (settingTrasladoSalida.dataset.length === 0) {
                        modal.mensaje("Busqueda vacia", "No se encontraron Traslados");
                    }
                } else {
                }
            }, function (data) {
                console.info(data);
            });
        };

        $scope.evaluarTraslado = function (traEst, traIngId, orgDesNom, estNom, fec, traTipNom, resolucion) {
            if (traEst === 'P') {
                $scope.trasladoSalidaCreate.traIngFec = fec;                //
                $scope.trasladoSalidaCreate.traIngId = traIngId;            //
                $scope.trasladoSalidaCreate.traIngOrgDesNom = orgDesNom;    //
                $scope.trasladoSalidaCreate.traIngEstNom = estNom;
                $scope.trasladoSalidaCreate.traIngTipNom = traTipNom;       //
                $scope.trasladoSalidaCreate.traIngResolucion = resolucion;

                $('#modalTrasladoSalida').modal('toggle');
            } else {
                modal.mensaje("Advertencia", "Este Traslado ya Fue Procesado");
            }
        };

        $scope.generarTrasladoSalida = function () {
            var request = crud.crearRequest('trasladoSalida', 1, 'generarTrasladoSalida');

            request.setData($scope.trasladoSalidaCreate);
            crud.insertar("/matriculaInstitucional", request, function (data) {
                if (data.responseSta) {
                    $scope.buscarTraslado();
                    modal.mensaje("Operacion Exitosa", "Traslado Procesado");
                    $('#modalTrasladoSalida').modal('hide');
                } else {
                    modal.mensaje("Error", "Error al Procesar el Traslado");
                }
            }, function (data) {
                console.info(data);
            });
        };
    }]);


