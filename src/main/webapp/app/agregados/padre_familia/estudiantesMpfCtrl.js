app.factory('mepServiceEvaluacion', ['$log', '$uibModal', '$mdDialog', 'crud', function ($log, $uibModal, $mdDialog, crud) {
        var fichas_evaluacion = null;
        var employee = {};
        return {
            setCurrentEmployee: function (e) {
                employee = e;
            },
            getCurrentEmployee: function () {
                return employee;
            },
            changeSelection: function (row, preSelection) {
                if (preSelection) {
                    preSelection.$selected = false;
                    //row.$selected = true;
                    //preSelection = row;
                }
                if (preSelection === row) {
                    row.$selected = false;
                    preSelection = null;
                } else {
                    row.$selected = true;
                    preSelection = row;
                }
                return preSelection;
            },
            openModal: function (nameHtml, nameCtrl, size, alias, params) {
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    keyboard: true,
                    backdrop: true,
                    templateUrl: nameHtml,
                    controller: nameCtrl,
                    controllerAs: alias,
                    size: size,
                    resolve: params
                });
                return modalInstance;
            },
            openDialog: function (title, content, ev, succ, err) {
                var confirm = $mdDialog.confirm()
                        .title(title)
                        .textContent(content)
                        .ariaLabel('Lucky day')
                        .targetEvent(ev)
                        .ok('Aceptar')
                        .cancel('Cancelar');
                $mdDialog.show(confirm).then(succ, err);
            },
            insertEvaluacionService: function (data, type, succ, err) {
                var request = crud.crearRequest('mep', 1, 'registrarEvaluacion');
                request.setMetadataValue('type', type);
                request.setData(data);
                console.log(data);
                crud.insertar('/evaluacion_personal', request, succ, err);
            },
            compartirEvaluacionService: function (data, succ, err) {
                var request = crud.crearRequest('mep', 1, 'compartirEvaluacion');
                request.setMetadataValue('id', data);
                crud.actualizar('/evaluacion_personal', request, succ, err);
            },
            getReportService: function (id, type, succ, err) {
                var request = crud.crearRequest('mep', 1, 'getReportEvaluacion');
                request.setMetadataValue('resumen', '' + id);
                crud.listar('/evaluacion_personal', request, succ, err);
            },
            getDetalleEvaluacionPersonal: function (idResumen, type, succ, err) {
                var request = crud.crearRequest('mep', 1, 'listarDetallesEvaluacion');
                request.setMetadataValue('id', angular.isNumber(idResumen) ? '' + idResumen : idResumen);
                request.setMetadataValue('ty', type);
                crud.listar('/evaluacion_personal', request, succ, err);
            },
            getDataEstadistica: function (type, data, succ, err) {
                var request = crud.crearRequest('mep', 1, 'getDataEstadistica');
                request.setMetadataValue('type', type);
                request.setData(data);
                crud.listar('/evaluacion_personal', request, succ, err);
            }
        };
    }]);


//app.controller("estudiantesMpfCtrl", ["$rootScope", "$scope", "NgTableParams", '$window', "crud", "modal", function ($rootScope, $scope, NgTableParams, $window, crud, modal) {
app.controller("estudiantesMpfCtrl", ["$rootScope", "$scope", "NgTableParams", '$location', '$window', "crud", "modal", 'mepServiceEvaluacion', function ($rootScope, $scope, NgTableParams, $location, $window, crud, modal, mepServiceEvaluacion)
    {

//        $scope.today = new Date().toISOString().slice(0,10).replace(/-/g,"");
        var paramsMiTablaDatos = {count: 10};
        var settingMiTablaDatos = {counts: []};
        $scope.miTablaDatos = new NgTableParams(paramsMiTablaDatos, settingMiTablaDatos);

        var paramsMiTablaAreas = {count: 10};
        var settingMiTablaAreas = {counts: []};
        $scope.miTablaAreas = new NgTableParams(paramsMiTablaAreas, settingMiTablaAreas);

        var paramsMiTablaLista = {count: 10};
        var settingMiTablaLista = {counts: []};
        $scope.miTablaLista = new NgTableParams(paramsMiTablaLista, settingMiTablaLista);

        var paramsMiTablaTareas = {count: 10};
        var settingMiTablaTareas = {counts: []};
        $scope.miTablaTareas = new NgTableParams(paramsMiTablaTareas, settingMiTablaTareas);

        var paramsMiTablaAsistencia = {count: 10};
        var settingMiTablaAsistencia = {counts: []};
        $scope.miTablaAsistencia = new NgTableParams(paramsMiTablaAsistencia, settingMiTablaAsistencia);

        var paramsMiTablaNotas = {count: 10};
        var settingMiTablaNotas = {counts: []};
        $scope.miTablaNotas = new NgTableParams(paramsMiTablaNotas, settingMiTablaNotas);

        var paramsTablaDirInt = {count: 10};
        var settingTablaDirInt = {counts: []};
        $scope.tablaDirInt = new NgTableParams(paramsTablaDirInt, settingTablaDirInt);

        var paramsTablaPersonalIE = {count: 15};
        var settingTablaPersonalIE = {
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset: []
        };
        $scope.tablaPersonalIE = new NgTableParams(paramsTablaPersonalIE, settingTablaPersonalIE);

        var paramsTablaResumenEvalPersonal = {count: 15};
        var settingTablaResumenEvalPersonal = {
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset: []
        };
        $scope.tablaResumenEvalPersonal = new NgTableParams(paramsTablaResumenEvalPersonal, settingTablaResumenEvalPersonal);

        $scope.asistencias = [];
        $scope.notasEstudiante = [];
        $scope.mostrarLista = false;
        $scope.estadosTarea = [{id: "N", title: "Nuevo"}, {id: "E", title: "Enviado"}, {id: "C", title: "Calificado"}];
        $scope.estadosAsistencia = [{id: "A", title: "Asistio"}, {id: "F", title: "Falta"}, {id: "T", title: "Tardanza"}];
        $scope.listaEstudiantes = [{personaID: "", matriculaID: "", dni: "", nombre: "", orgId: "", nombreOrganizacion: ""}];
        $scope.estudiante = {
            nombres: "",
            nacionalidad: "",
            dni: "",
            nacimiento: "",
            sexo: "",
            peso: "",
            talla: "",
            alergia: "",
            sangre: "",
            email: "",
            numero1: "",
            apoderadoNombre: "",
            apoderadoNumero: "",
            nivel: "",
            grado: "",
            seccion: "",
            turno: "",
            orden: ""
        };
        $scope.matricula = {
            nivel: "",
            grado: "",
            seccion: "",
            gradoId: "",
            seccionId: "",
            turno: "",
            tutor: ""
        };
        $scope.tipoEstadistica = [{nombre: "Notas por área"}, {nombre: "Notas por competencia"}, {nombre: "Notas por año"}, {nombre: "Asistencia"}];
        $scope.portalTransparenciaOpciones = [{nombre: "Directorio"}, {nombre: "Evaluación Personal"}];
        $scope.showPersona = 0;
        $scope.showPersona2 = 0;
        $scope.funciones = [{nombre: "Datos Estudiante"}, {nombre: "Matricula"}, {nombre: "Tareas"}];
        $scope.areas = [{areaID: "", area: ""}];
        $scope.sel = {areaID: "", plaID: "", matriculaID: "", orgID: "", fechaDesde: "", fechaHasta: "", gradoID: ""};
        $scope.asi = {nAsis: 0, nTjus: 0, Tinj: 0, nFjus: 0, Finj: 0};

        $scope.listarEstudiantes = function () {
            var request = crud.crearRequest('padreFamilia', 1, 'listarHijos');
            request.setData({usuarioID: $rootScope.usuMaster.usuario.usuarioID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    $scope.listaEstudiantes = data.data;
                }
                $scope.listaEstudiantes.forEach(function (entry) {
                    entry.showTipoEstadistica = false;
                });

            }, function (data) {
                console.info(data);
            });
        };

        $scope.datosEstudiante = function (mp, oo) {
            $scope.showPersona = 0;
            $scope.sel.orgID = oo;
            var request = crud.crearRequest('padreFamilia', 1, 'datosEstudiante');
            request.setData({matriculaID: mp});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    $scope.estudiante = data.data;
                    $("#modalDatosEstudiante").modal('show');
                }
                modal.mensaje("CONFIRMACION", data.responseMsg);
            }, function (data) {
                console.info(data);
            });
        };

        $scope.imprimirDatosEstudiante = function () {
            var request = crud.crearRequest('padreFamilia', 1, 'reporteFichaEstudiante');
            request.setData({ficha: $scope.estudiante, orgID: $scope.sel.orgID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.insertar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    $scope.dataBase64 = data.data[0].datareporte;
                    window.open($scope.dataBase64);
                }
                modal.mensaje("CONFIRMACION", data.responseMsg);
            }, function (data) {
                console.info(data);
            });
        };

        $scope.datosMatricula = function (mp, oo) {
            $scope.showPersona = 0;
            $scope.mostrarLista = false;
            $scope.sel.orgID = oo;
            var request = crud.crearRequest('padreFamilia', 1, 'datosMatricula');
            request.setData({matriculaID: mp});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    $scope.matricula = data.data.matricula;
                    settingMiTablaDatos.dataset = data.data.matricula;
                    iniciarPosiciones(settingMiTablaDatos.dataset);
                    $scope.miTablaDatos.settings(settingMiTablaDatos);
                    settingMiTablaAreas.dataset = data.data.areas;
                    iniciarPosiciones(settingMiTablaAreas.dataset);
                    $scope.miTablaAreas.settings(settingMiTablaAreas);
                    $scope.matricula.observaciones = data.data.obs;
                    $("#modalDatosMatricula").modal('show');
                }
                modal.mensaje("CONFIRMACION", data.responseMsg);
            }, function (data) {
                console.info(data);
            });
        };

        $scope.verUtilesArea = function (a) {
            $scope.listaTotal = 0;
            var request = crud.crearRequest('padreFamilia', 1, 'listarUtilesByArea');
            request.setData({organizacionID: $scope.sel.orgID, grado: $scope.matricula[0].gradoId, seccion: $scope.matricula[0].seccionId, area: a});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    if (data.data.length == undefined)
                    {
                        $scope.mostrarLista = false;
                        modal.mensaje("VERIFICACION", data.responseMsg);
                    } else {
                        settingMiTablaLista.dataset = data.data;
                        iniciarPosiciones(settingMiTablaLista.dataset);
                        $scope.miTablaLista.settings(settingMiTablaLista);
                        for (var i = 0; i < settingMiTablaLista.dataset.length; i++) {
                            $scope.listaTotal += settingMiTablaLista.dataset[i].precio;
                        }
                        $scope.mostrarLista = true;
                    }
                } else {
                    $scope.mostrarLista = false;
                }
//                $("#modalDatosTareas").modal('show');
            }, function (data) {
                console.info(data);
            });
        };

        $scope.imprimirListaByArea = function (areaId, area, docente) {
            var request = crud.crearRequest('padreFamilia', 1, 'reporteListaUtilesByArea');
            request.setData({organizacionID: $scope.sel.orgID, matricula: $scope.matricula[0], area: area, areaID: areaId, docente: docente});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.insertar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    $scope.dataBase64 = data.data[0].datareporte;
                    window.open($scope.dataBase64);
                }
                modal.mensaje("CONFIRMACION", data.responseMsg);
            }, function (data) {
                console.info(data);
            });
        };
        //imprimirAsistencia
        $scope.imprimirListaUtiles = function () {
            var request = crud.crearRequest('padreFamilia', 1, 'reporteListaUtiles');
            request.setData({matricula: $scope.matricula[0], organizacionID: $scope.sel.orgID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.insertar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    $scope.dataBase64 = data.data[0].datareporte;
                    window.open($scope.dataBase64);

                }
                modal.mensaje("CONFIRMACION", data.responseMsg);
            }, function (data) {
                console.info(data);
            });
        };

        $scope.datosTareas = function (mp, oo) {
            $scope.showPersona = 0;
            $scope.sel.areaID = "";
            var settingMiTablaTareas = {counts: []};
            $scope.miTablaTareas = new NgTableParams(paramsMiTablaTareas, settingMiTablaTareas);
            $scope.sel.matriculaID = mp;
            var request = crud.crearRequest('padreFamilia', 1, 'listarAreas');
            request.setData({organizacionID: oo, matriculaID: mp});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    $scope.sel.plaID = data.data.planID;
                    $scope.areas = data.data;
                    $("#modalDatosTareas").modal('show');
                }

            }, function (data) {
                console.info(data);
            });
        };

        $scope.buscarTareas = function () {
            if ($scope.sel.areaID === "")
                return;
            var request = crud.crearRequest('padreFamilia', 1, 'listarBandejaTareaByAlumno');
            request.setData({area: $scope.sel.areaID, matriculaID: $scope.sel.matriculaID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    if (data.data.length === 0)
                        modal.mensaje("VERIFICACION", "No se encontro Tareas Disponibles para el Area Seleccionada");
                    settingMiTablaTareas.dataset = data.data;
                    iniciarPosiciones(settingMiTablaTareas.dataset);
                    $scope.miTablaTareas.settings(settingMiTablaTareas);
                }
            }, function (data) {
                console.info(data);
            });
        };

        $scope.datosAsistencia = function (pp, gd, oo, mp) {
            $scope.showPersona = 0;
            $scope.sel.areaID = "";
            $scope.sel.orgID = oo;
            var settingMiTablaAsistencia = {counts: []};
            $scope.miTablaAsistencia = new NgTableParams(paramsMiTablaAsistencia, settingMiTablaAsistencia);
            $scope.asistencias = [];
            $scope.sel.matriculaID = mp;
            $scope.sel.gradoID = gd;
            $scope.sel.plaID = pp;
            var request = crud.crearRequest('padreFamilia', 1, 'listarAreas');
            request.setData({organizacionID: oo, matriculaID: mp});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    $scope.areas = data.data;
                    $("#modalDatosAsistencia").modal('show');
                }
            }, function (data) {
                console.info(data);
            });
        };

        $scope.buscarAsistencia = function () {
            if ($scope.sel.areaID === "")
                return;
            if ($scope.sel.fechaDesde === "")
                return;
            if ($scope.sel.fechaHasta === "")
                return;
            if($scope.sel.fechaHasta < $scope.sel.fechaDesde) {
                $scope.sel.fechaDesde = $scope.sel.fechaHasta;
            } 
            var request = crud.crearRequest('padreFamilia', 1, 'listarAsistenciaByAlumno');
            request.setData({area: $scope.sel.areaID, matriculaID: $scope.sel.matriculaID, desde: convertirFecha2($scope.sel.fechaDesde), hasta: convertirFecha2($scope.sel.fechaHasta)});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    if (data.data.length === 0)
                        modal.mensaje("VERIFICACION", "No se encontro Asistencias");
                    settingMiTablaAsistencia.dataset = data.data.asistencias;
                    $scope.asistencias = data.data.asistencias;
                    console.log("$scope.asistencias");
                    console.log($scope.asistencias);
                    iniciarPosiciones(settingMiTablaAsistencia.dataset);
                    $scope.miTablaAsistencia.settings(settingMiTablaAsistencia);
                    $scope.asi = data.data.resumen;
                }
            }, function (data) {
                console.info(data);
            });
        };

        $scope.imprimirAsistencia = function () {
            if ($scope.asistencias.length === 0) {
                modal.mensaje("VERIFICACION", "Seleccione Área y fechas con asistencias");
                return;
            }
            var request = crud.crearRequest('padreFamilia', 1, 'reporteAsistenciaByArea');
            request.setData({organizacionID: $scope.sel.orgID, area: $scope.sel.areaID, matriculaID: $scope.sel.matriculaID, planID: $scope.sel.plaID, gradoID: $scope.sel.gradoID, desde: convertirFecha2($scope.sel.fechaDesde), hasta: convertirFecha2($scope.sel.fechaHasta)});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.insertar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    $scope.dataBase64 = data.data[0].datareporte;
                    window.open($scope.dataBase64);

                }
                modal.mensaje("CONFIRMACION", data.responseMsg);
            }, function (data) {
                console.info(data);
            });
        };

        $scope.datosNotas = function (mp, oo, pp) {
            $scope.showPersona = 0;
            $scope.sel.matriculaID = mp;
            $scope.sel.orgID = oo;
            $scope.sel.nombre = pp;
            var settingMiTablaNotas = {counts: []};
            $scope.notasEstudiante = [];
            $scope.miTablaNotas = new NgTableParams(paramsMiTablaNotas, settingMiTablaNotas);
//            $scope.sel.matriculaID=mp;
            var request = crud.crearRequest('padreFamilia', 1, 'listarNotas');
            request.setData({organizacionID: oo, matriculaID: mp});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    settingMiTablaNotas.dataset = data.data;
                    $scope.notasEstudiante = data.data;
                    iniciarPosiciones(settingMiTablaNotas.dataset);
                    $scope.miTablaNotas.settings(settingMiTablaNotas);
                }
                $("#modalNotasEstudiante").modal('show');
                console.log($scope.notasEstudiante);
                modal.mensaje("CONFIRMACION", data.responseMsg);
            }, function (data) {
                console.info(data);
            });
        };

        $scope.imprimirNotas = function () {
            if ($scope.notasEstudiante.length === 0) {
                modal.mensaje("CONFIRMACION", "No existe Notas Registradas del Alumno");
                return;
            }
            var request = crud.crearRequest('padreFamilia', 1, 'reporteBoletaNotas');
            request.setData({organizacionID: $scope.sel.orgID, matriculaID: $scope.sel.matriculaID, persona: $scope.sel.nombre});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.insertar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    $scope.dataBase64 = data.data[0].datareporte;
                    window.open($scope.dataBase64);
                }
                modal.mensaje("CONFIRMACION", data.responseMsg);
            }, function (data) {
                console.info(data);
            });
        };

        $scope.estadistica = function (tipoEstadistica, mp, oo) {
            var modalTipoSeleccionado = "";
            switch (tipoEstadistica.nombre) {
                case "Notas por área":
                    modalTipoSeleccionado = "#modalEstadisticaNotasXArea";
                    prepareModalEstadisticaNota(mp, oo);
                    break;
                case "Notas por competencia":
                    modalTipoSeleccionado = "#modalEstadisticaNotasXCompetencia";
                    prepareModalEstadisticaNota(mp, oo);
                    break;
                case "Notas por año":
                    modalTipoSeleccionado = "#modalEstadisticaNotasXAnio";
                    prepareModalEstadisticaNotaXAnio(mp, oo);
                    break;
                case "Asistencia":
                    modalTipoSeleccionado = "#modalEstadisticaAsistencia";
                    prepareModalEstadisticaAsistencia(mp, oo);
                    break;
                default:
            }
            $scope.sel.orgID = oo;
            var request = crud.crearRequest('padreFamilia', 1, 'datosEstudiante');
            request.setData({matriculaID: mp});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    $scope.estudiante = data.data;
                    $(modalTipoSeleccionado).modal('show');
                }
                modal.mensaje("CONFIRMACION", data.responseMsg);
            }, function (data) {
                console.info(data);
            });
        };
        var prepareModalEstadisticaNota = function (mp, oo) {
            $scope.showPersona = 0;
            $scope.sel.areaID = "";
            $scope.sel.orgID = oo;
            $scope.sel.matriculaID = mp;
            $scope.sel.gradoID = $scope.listaEstudiantes.gradoID;
            $scope.sel.plaID = $scope.listaEstudiantes;
            var request = crud.crearRequest('padreFamilia', 1, 'listarAreas');
            request.setData({organizacionID: oo, matriculaID: mp});
            crud.listar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    $scope.sel.plaID = data.data.planID;
                    $scope.areasEstadisticaNota = data.data;
                }
            }, function (data) {
                console.info(data);
            });
//            $scope.areasEstadisticaNota.sort(function (a, b) {
//                return (a.area > b.area) ? 1 : ((b.area > a.area) ? -1 : 0);
//            });
            var request2 = crud.crearRequest('padreFamilia', 1, 'listarNotas');
            request2.setData({organizacionID: $scope.sel.orgID, matriculaID: $scope.sel.matriculaID});
            crud.listar("/padreFamilia", request2, function (data) {
                if (data.responseSta) {
                    $scope.notasEstudianteEstadistica = data.data;
                    $scope.buscarEstadisticaNotaxPeriodo();
                    $scope.buscarEstadisticaNotaxArea();
                    $scope.buscarEstadisticaNotaxCompetencia();
                    $scope.buscarEstadisticaNotaxAnio();
                }
            }, function (data) {
                console.info(data);
            });
            $scope.periodos = [
                {id: 1, descripcion: 'Bimestre 1'},
                {id: 2, descripcion: 'Bimestre 2'},
                {id: 3, descripcion: 'Bimestre 3'},
                {id: 4, descripcion: 'Bimestre 4'}];

        };
        var prepareModalEstadisticaNotaXAnio = function (mp, oo) {
            $scope.sel.orgID = oo;
            $scope.sel.matriculaID = mp;
            var request = crud.crearRequest('padreFamilia', 1, 'listarNotasAnuales');
            request.setData({matriculaID: $scope.sel.matriculaID});
            crud.listar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    //begin Data de prueba
//                    data.data = {
//                        'primer grado de educacion secundaria': {'MATEMATICA': "18", 'COMUNICACION': "17", 'CIENCIAS SOCIALES': "11", 'EDUCACIÓN FÍSICA': "13"},
//                        'segundo grado de educacion secundaria': {'MATEMATICA': "16", 'COMUNICACION': "18", 'CIENCIAS SOCIALES': "05", 'EDUCACIÓN FÍSICA': "12"},
//                        'tercero grado de educacion secundaria': {'MATEMATICA': "14", 'COMUNICACION': "19", 'CIENCIAS SOCIALES': "12", 'EDUCACIÓN FÍSICA': "10"}
//                    };
                    //enf
                    $scope.aniosEstadistica = [];
                    $scope.notaXAniosEstadistica = data.data;
                    for (var name in data.data) {
                        $scope.aniosEstadistica.push(name);
                    }

                }
            }, function (data) {
                console.info(data);
            });
        };
        $scope.buscarEstadisticaNotaxPeriodo = function () {
            if ($scope.sel.periodoSelected === "")
                return;
            $scope.barNotaxPeriodo = {
                areas: [],
                data: [
                    [],
                    [],
                    [],
                    []
                ],
                options: {
                    scales: {
                        yAxes: [{
                                ticks: {
                                    min: 0,
                                    max: 20
                                }
                            }]
                    }
                }
            };
            $scope.notasEstudianteEstadistica.sort(function (a, b) {
                return (a.area > b.area) ? 1 : ((b.area > a.area) ? -1 : 0);
            });
            var vez = 0;
            var cursoTemp = "";
            var numCurso = 0;
            var primeraVez = 0;
            $scope.notasEstudianteEstadistica.forEach(function (valor, index) {
                if (primeraVez === 0) {
                    $scope.barNotaxPeriodo.areas.push(valor.area);
                    cursoTemp = valor.area;
                    $scope.barNotaxPeriodo.data[0].push(0);
                    $scope.barNotaxPeriodo.data[1].push(0);
                    $scope.barNotaxPeriodo.data[2].push(0);
                    $scope.barNotaxPeriodo.data[3].push(0);
//                    console.log($scope.barNotaxPeriodo.data);
                    primeraVez++;
                }
//                console.log(cursoTemp);
//                console.log(valor.area);
                if (cursoTemp !== valor.area) {
                    $scope.barNotaxPeriodo.areas.push(valor.area);
                    cursoTemp = valor.area;
                    numCurso++;
                    vez = 0;
                    $scope.barNotaxPeriodo.data[0][numCurso] = 0;
                    $scope.barNotaxPeriodo.data[1][numCurso] = 0;
                    $scope.barNotaxPeriodo.data[2][numCurso] = 0;
                    $scope.barNotaxPeriodo.data[3][numCurso] = 0;
                }
                $scope.barNotaxPeriodo.data[0][numCurso] = ($scope.barNotaxPeriodo.data[0][numCurso] * vez + parseInt(valor.t1)) / (vez + 1);
                $scope.barNotaxPeriodo.data[1][numCurso] = ($scope.barNotaxPeriodo.data[1][numCurso] * vez + parseInt(valor.t2)) / (vez + 1);
                $scope.barNotaxPeriodo.data[2][numCurso] = ($scope.barNotaxPeriodo.data[2][numCurso] * vez + parseInt(valor.t3)) / (vez + 1);
                $scope.barNotaxPeriodo.data[3][numCurso] = ($scope.barNotaxPeriodo.data[3][numCurso] * vez + parseInt(valor.t4)) / (vez + 1);
                vez++;
            });
        };
        $scope.buscarEstadisticaNotaxArea = function () {
            if ($scope.sel.areasEstadisticaNotaSelected === "")
                return;
            $scope.barNotaxArea = {
                periodos: ['Bimestre 1', 'Bimestre 2', 'Bimestre 3', 'Bimestre 4'],
                data: [
                    [],
                    [],
                    [],
                    []
                ],
                options: {
                    scales: {
                        yAxes: [{
                                ticks: {
                                    min: 0,
                                    max: 20
                                }
                            }]
                    }
                }
            };
            $scope.notasEstudianteEstadistica.sort(function (a, b) {
                return (a.area > b.area) ? 1 : ((b.area > a.area) ? -1 : 0);
            });
            var vez = 0;
            var cursoTemp = "";
            var numCurso = 0;
            var primeraVez = 0;
            $scope.barNotaxPeriodo.data = [[], [], [], []];
            $scope.barNotaxPeriodo.areas = [];
            $scope.notasEstudianteEstadistica.forEach(function (valor, index) {
                if (primeraVez === 0) {
                    $scope.barNotaxPeriodo.areas.push(valor.area);
                    cursoTemp = valor.area;
                    $scope.barNotaxPeriodo.data[0].push(0);
                    $scope.barNotaxPeriodo.data[1].push(0);
                    $scope.barNotaxPeriodo.data[2].push(0);
                    $scope.barNotaxPeriodo.data[3].push(0);
                    primeraVez++;
                }
                if (cursoTemp !== valor.area) {
                    $scope.barNotaxPeriodo.areas.push(valor.area);
                    cursoTemp = valor.area;
                    numCurso++;
                    vez = 0;
                    $scope.barNotaxPeriodo.data[0][numCurso] = 0;
                    $scope.barNotaxPeriodo.data[1][numCurso] = 0;
                    $scope.barNotaxPeriodo.data[2][numCurso] = 0;
                    $scope.barNotaxPeriodo.data[3][numCurso] = 0;
                }
                $scope.barNotaxPeriodo.data[0][numCurso] = ($scope.barNotaxPeriodo.data[0][numCurso] * vez + parseInt(valor.t1)) / (vez + 1);
                $scope.barNotaxPeriodo.data[1][numCurso] = ($scope.barNotaxPeriodo.data[1][numCurso] * vez + parseInt(valor.t2)) / (vez + 1);
                $scope.barNotaxPeriodo.data[2][numCurso] = ($scope.barNotaxPeriodo.data[2][numCurso] * vez + parseInt(valor.t3)) / (vez + 1);
                $scope.barNotaxPeriodo.data[3][numCurso] = ($scope.barNotaxPeriodo.data[3][numCurso] * vez + parseInt(valor.t4)) / (vez + 1);
                vez++;
            });
//            $scope.barNotaxArea.data = $scope.barNotaxPeriodo.data;
            var n = $scope.barNotaxPeriodo.data[0].length;
            var nuevam = new Array(n);
            for (var a = 0; a < n; a++)
                nuevam[a] = new Array($scope.barNotaxPeriodo.data.length);

            for (var i = 0; i < n; i++) {
                for (var j = 0; j < $scope.barNotaxPeriodo.data.length; j++) {
                    nuevam[i][j] = $scope.barNotaxPeriodo.data[j][i];
                }
            }
            $scope.barNotaxArea.data = nuevam;
//            console.log($scope.barNotaxArea.data);
//            console.log($scope.barNotaxPeriodo.data);

        };
        $scope.buscarEstadisticaNotaxCompetencia = function () {
            if ($scope.sel.areasEstadisticaNotaSelected === "")
                return;
            $scope.lineNotaxCompetencia = {
                labels: ['Bimestre 1', 'Bimestre 2', 'Bimestre 3', 'Bimestre 4'],
                series: [],
                data: [],
                options: {
                    fill: false,
                    legend: {
                        display: true,
                        labels: {
                            fontColor: 'rgb(255, 99, 132)'
                        }
                    },
                    scales: {
                        yAxes: [{
                                ticks: {
                                    min: 0,
                                    max: 20
                                }
                            }]
                    }
                },
                colors: [
                    {
                        backgroundColor: '#0062ff',
                        pointBackgroundColor: '#0062ff',
                        pointHoverBackgroundColor: '#0062ff',
                        borderColor: '#0062ff',
                        pointBorderColor: '#0062ff',
                        pointHoverBorderColor: '#0062ff',
                        fill: false /* this option hide background-color */
                    },
                    {
                        backgroundColor: '#FDB45C',
                        pointBackgroundColor: '#FDB45C',
                        pointHoverBackgroundColor: '#FDB45C',
                        borderColor: '#FDB45C',
                        pointBorderColor: '#FDB45C',
                        pointHoverBorderColor: '#FDB45C',
                        fill: false /* this option hide background-color */
                    },
                    {
                        backgroundColor: '#00ADF9',
                        pointBackgroundColor: '#00ADF9',
                        pointHoverBackgroundColor: '#00ADF9',
                        borderColor: '#00ADF9',
                        pointBorderColor: '#00ADF9',
                        pointHoverBorderColor: '#00ADF9',
                        fill: false /* this option hide background-color */
                    },
                    {
                        backgroundColor: '#46BFBD',
                        pointBackgroundColor: '#46BFBD',
                        pointHoverBackgroundColor: '#46BFBD',
                        borderColor: '#46BFBD',
                        pointBorderColor: '#46BFBD',
                        pointHoverBorderColor: '#46BFBD',
                        fill: false /* this option hide background-color */
                    }
                ]
            };
            $scope.notasEstudianteEstadistica.sort(function (a, b) {
                return (a.area > b.area) ? 1 : ((b.area > a.area) ? -1 : 0);
            });
            var vez = 0;
            var cursoTemp = "";
            var numCurso = 0;
            var primeraVez = 0;
            $scope.notasEstudianteEstadistica.forEach(function (valor, index) {
                if (primeraVez === 0) {
                    $scope.lineNotaxCompetencia.series.push([]);
                    $scope.lineNotaxCompetencia.series[0].push(valor.logro);
                    $scope.lineNotaxCompetencia.data.push([]);
                    $scope.lineNotaxCompetencia.data[0].push([]);
                    $scope.lineNotaxCompetencia.data[0][0].push(parseInt(valor.t1));
                    $scope.lineNotaxCompetencia.data[0][0].push(parseInt(valor.t2));
                    $scope.lineNotaxCompetencia.data[0][0].push(parseInt(valor.t3));
                    $scope.lineNotaxCompetencia.data[0][0].push(parseInt(valor.t4));

                    cursoTemp = valor.area;
                    primeraVez++;
                } else {
                    if (cursoTemp !== valor.area) {
                        cursoTemp = valor.area;
                        numCurso++;
                        vez = 0;
                        $scope.lineNotaxCompetencia.series.push([]);
                        $scope.lineNotaxCompetencia.data.push([]);

                    }
                    $scope.lineNotaxCompetencia.series[numCurso].push(valor.logro);
                    $scope.lineNotaxCompetencia.data[numCurso].push([]);
                    $scope.lineNotaxCompetencia.data[numCurso][vez].push(parseInt(valor.t1));
                    $scope.lineNotaxCompetencia.data[numCurso][vez].push(parseInt(valor.t2));
                    $scope.lineNotaxCompetencia.data[numCurso][vez].push(parseInt(valor.t3));
                    $scope.lineNotaxCompetencia.data[numCurso][vez].push(parseInt(valor.t4));
                }
                vez++;
            });
        };
        $scope.buscarEstadisticaNotaxAnio = function () {
//            console.log($scope.notaXAniosEstadistica);
//            console.log($scope.anioSelectedEstadistica);
//            console.log($scope.notaXAniosEstadistica[$scope.anioSelectedEstadistica]);
            $scope.barNotasXAnio = {
                labels: [],
                data: [],
                options: {
                    scales: {
                        yAxes: [{
                                ticks: {
                                    min: 0,
                                    max: 20
                                }
                            }]
                    },
                    fill: false
                }
            };
            for (var name in $scope.notaXAniosEstadistica[$scope.anioSelectedEstadistica]) {
                $scope.barNotasXAnio.labels.push(name);
                $scope.barNotasXAnio.data.push($scope.notaXAniosEstadistica[$scope.anioSelectedEstadistica][name]);
            }
            
        };
        var prepareModalEstadisticaAsistencia = function (mp, oo) {
            $scope.showPersona = 0;
            $scope.sel.areaID = "";
            $scope.sel.orgID = oo;
            $scope.sel.matriculaID = mp;
            $scope.sel.gradoID = $scope.listaEstudiantes.gradoID;
            $scope.sel.plaID = $scope.listaEstudiantes;
            var request = crud.crearRequest('padreFamilia', 1, 'listarAreas');
            request.setData({organizacionID: oo, matriculaID: mp});
            crud.listar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    $scope.sel.plaID = data.data.planID;
                    $scope.areasEstadisticaAsistencia = data.data;
                }
                $scope.buscarAsistenciaTodosEstadistica();
                $scope.buscarAsistenciaEstadistica();
            }, function (data) {
                console.info(data);
            });

        };
        $scope.buscarAsistenciaTodosEstadistica = function () {
            if ($scope.sel.fechaDesdeBB === "")
                return;
            if ($scope.sel.fechaHastaBB === "")
                return;
//            if($scope.sel.fechaHastaBB < $scope.sel.fechaDesdeBB) {
//                console.log($scope.sel.fechaDesdeBB);
//                console.log($scope.sel.fechaHastaBB);
//                $scope.sel.fechaDesdeBB = $scope.sel.fechaHastaBB;
//                //return;
//            }                
            $scope.barAsistencia = {
                labels: [],
                series: ['Asistencia', 'Falta', 'Tardanza'],
                data: [
                    [], [], []
                ],
                options: {
                    scales: {
                        yAxes: [{
                                ticks: {
                                    min: 0
                                }
                            }]
                    },
                    fill: false,
                    legend: {
                        display: true,
                        labels: {
                            fontColor: 'rgb(255, 99, 132)'
                        }
                    }
                }
            };

            $scope.areasEstadisticaAsistencia.forEach(function (valor, index) {
                var request = crud.crearRequest('padreFamilia', 1, 'listarAsistenciaByAlumno');
                request.setData({area: valor.areaID, matriculaID: $scope.sel.matriculaID, desde: convertirFecha2($scope.sel.fechaDesdeBB), hasta: convertirFecha2($scope.sel.fechaHastaBB)});
                crud.listar("/padreFamilia", request, function (data) {
                    if (data.responseSta) {
                        if (data.data.length === 0)
                            modal.mensaje("VERIFICACION", "No se encontró Asistencias");
                        $scope.asistenciasTodos = data.data.asistencias;
//                        $scope.barAsistencia.labels.push(valor.area);
                        $scope.barAsistencia.labels[index] = valor.area;
                        $scope.barAsistencia.data[0][index] = 0;
                        $scope.barAsistencia.data[1][index] = 0;
                        $scope.barAsistencia.data[2][index] = 0;
                        $scope.asistenciasTodos.forEach(function (valor2) {
                            if (valor2.estado === 'A')
                                $scope.barAsistencia.data[0][index]++;
                            if (valor2.estado === 'F')
                                $scope.barAsistencia.data[1][index]++;
                            if (valor2.estado === 'T')
                                $scope.barAsistencia.data[2][index]++;
                        });

                    }
                }, function (data) {
                    console.info(data);
                });
                request = null;
            });

        };
        $scope.buscarAsistenciaEstadistica = function () {
            if ($scope.sel.areaSelectedAsistenciaBB === "")
                return;
            if ($scope.sel.fechaDesdeBB === "")
                return;
            if ($scope.sel.fechaHastaBB === "")
                return;     
            if($scope.sel.fechaHastaBB < $scope.sel.fechaDesdeBB) {
                $scope.sel.fechaDesdeBB = $scope.sel.fechaHastaBB;
            } 
            var request = crud.crearRequest('padreFamilia', 1, 'listarAsistenciaByAlumno');
            request.setData({area: $scope.sel.areaSelectedAsistenciaBB, matriculaID: $scope.sel.matriculaID, desde: convertirFecha2($scope.sel.fechaDesdeBB), hasta: convertirFecha2($scope.sel.fechaHastaBB)});
            crud.listar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    if (data.data.length === 0)
                        modal.mensaje("VERIFICACION", "No se encontró Asistencias");
                    $scope.asistencias = data.data.asistencias;
                    $scope.pieDonutAsistencia = {
                        labels: ['Asistencia', 'Falta', 'Tardanza'],
                        data: [0, 0, 0],
                        options: {
                            fill: false,
                            legend: {
                                display: true,
                                labels: {
                                    fontColor: 'rgb(255, 99, 132)'
                                }
                            }
                        }
                    };
                    $scope.asistencias.forEach(function (valor) {
                        if (valor.estado === 'A')
                            $scope.pieDonutAsistencia.data[0] = $scope.pieDonutAsistencia.data[0] + 1;
                        if (valor.estado === 'F')
                            $scope.pieDonutAsistencia.data[1]++;
                        if (valor.estado === 'T')
                            $scope.pieDonutAsistencia.data[2]++;
                    });
                }
            }, function (data) {
                console.info(data);
            });
        };

////////PORTAL TRANSPARENCIA////////////////////////
        $scope.portalTransparencia = function (opcionPortalTransparencia, mp, oo) {
            var modalTipoSeleccionado = "";
            switch (opcionPortalTransparencia.nombre) {
                case "Directorio":
                    modalTipoSeleccionado = "#modalDirectorioIE";
                    break;
                case "Evaluación Personal":
                    modalTipoSeleccionado = "#modalEvaluacionPersonal";
                    ///////////////Evaluacion Personal
                    listarPersonal();
                    break;
                default:
            }
            $scope.sel.orgID = oo;
            var request = crud.crearRequest('padreFamilia', 1, 'datosEstudiante');
            request.setData({matriculaID: mp});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/padreFamilia", request, function (data) {
                if (data.responseSta) {
                    $scope.estudiante = data.data;
                    $(modalTipoSeleccionado).modal('show');
                }
                modal.mensaje("CONFIRMACION", data.responseMsg);
            }, function (data) {
                console.info(data);
            });
        };

        $scope.changePersonSelected = function (idPersonaSeleccionada) {
            if ($scope.showPersona === idPersonaSeleccionada) {
                $scope.showPersona = 0;
            } else {
                $scope.showPersona = idPersonaSeleccionada;
            }
            $scope.showPersona2 = 0;
        };

        $scope.changePersonSelected2 = function (idPersonaSeleccionada) {
            if ($scope.showPersona2 === idPersonaSeleccionada) {
                $scope.showPersona2 = 0;
            } else {
                $scope.showPersona2 = idPersonaSeleccionada;
            }
            $scope.showPersona = 0;
        };

        //ESTADISTICA NOTAS POR PERIODO Y AREA
        $scope.updateGrafNotasXPeriodo = function () {
            console.log("Por Periodo: " + $scope.periodoSelected);
        };
        $scope.updateGrafNotasXNotas = function () {
            console.log($scope.areaSelected);
        };
        $scope.bar1 = {
            periodos: [
                {id: 1, descripcion: 'Bimestre 1'},
                {id: 2, descripcion: 'Bimestre 2'},
                {id: 3, descripcion: 'Bimestre 3'},
                {id: 4, descripcion: 'Bimestre 4'}],
            areas: ['Matematica', 'Comunicacion', 'Ciencias', 'Computacion', 'Geografia', 'Historia', 'Educacion Fisica'],
            data: [
                [12, 12, 11, 19, 16, 7, 9],
                [5, 12, 11, 9, 16, 17, 19],
                [12, 12, 18, 18, 16, 8, 19],
                [15, 18, 11, 18, 16, 8, 6]
            ],
            options: {
                scales: {
                    yAxes: [{
                            ticks: {
                                min: 0,
                                max: 20
                            }
                        }]
                }
            }
        };

        $scope.periodos = [
            {id: 1, descripcion: 'Bimestre 1'},
            {id: 2, descripcion: 'Bimestre 2'},
            {id: 3, descripcion: 'Bimestre 3'},
            {id: 4, descripcion: 'Bimestre 4'}];
        $scope.areas = [
            {id: 1, descripcion: 'Matematica'},
            {id: 2, descripcion: 'Comunicacion'},
            {id: 3, descripcion: 'Ciencias'},
            {id: 4, descripcion: 'Computacion'},
            {id: 5, descripcion: 'Geografia'},
            {id: 6, descripcion: 'Historia'},
            {id: 7, descripcion: 'Educacion Fisica'}];

        //////////
        $scope.bar2 = {
            areas: ['Matematica', 'Comunicacion', 'Ciencias', 'Computacion', 'Geografia', 'Historia', 'Educacion Fisica'],
            periodos: ['Bimestre 1', 'Bimestre 2', 'Bimestre 3', 'Bimestre 4'],
            data: [
                [12, 5, 12, 15],
                [12, 12, 12, 18],
                [11, 18, 11, 11],
                [19, 9, 18, 18],
                [16, 16, 16, 16],
                [7, 17, 8, 8],
                [9, 19, 19, 6]
            ],
            options: {
                scales: {
                    yAxes: [{
                            ticks: {
                                min: 0,
                                max: 20
                            }
                        }]
                }
            }
        };
        //ESTADISTICA NOTAS POR COMPETENCIA

        $scope.line1 = {
            labels: ['Bimestre 1', 'Bimestre 2', 'Bimestre 3', 'Bimestre 4'],
            series: [
                ['CompetA-Matematica', 'CompetB-Matematica', 'CompetC-Matematica', 'CompetD-Matematica'],
                ['CompetA-Comunicacion', 'CompetB-Comunicacion', 'CompetC-Comunicacion', 'CompetD-Comunicacion'],
                ['CompetA-Ciencias', 'CompetB-Ciencias', 'CompetC-Ciencias', 'CompetD-Ciencias'],
                ['CompetA-Computacion', 'CompetB-Computacion', 'CompetC-Computacion', 'CompetD-Computacion'],
                ['CompetA-Geografia', 'CompetB-Geografia', 'CompetC-Geografia', 'CompetD-Geografia'],
                ['CompetA-Historia', 'CompetB-Historia', 'CompetC-Historia', 'CompetD-Historia'],
                ['CompetA-EFisica', 'CompetB-EFisica', 'CompetC-EFisica', 'CompetD-EFisica']
            ],
            data: [
                [[9, 5, 12, 15], [12, 15, 8, 6], [12, 5, 9, 15], [7, 5, 12, 18]],
                [[7, 5, 9, 11], [17, 5, 17, 18], [7, 5, 18, 19], [19, 5, 20, 18]],
                [[13, 15, 12, 11], [17, 14, 13, 12], [17, 7, 8, 9], [18, 13, 11, 12]],
                [[9, 5, 12, 15], [7, 5, 9, 11], [13, 15, 12, 11], [20, 19, 18, 11]],
                [[17, 14, 13, 12], [12, 15, 8, 6], [17, 5, 17, 18], [11, 11, 11, 12]],
                [[17, 7, 8, 9], [7, 5, 18, 19], [12, 5, 9, 15], [15, 17, 18, 19]],
                [[18, 13, 11, 12], [18, 20, 19, 5], [19, 5, 20, 18], [7, 5, 12, 18]]
            ],
            options: {
                fill: false,
                legend: {
                    display: true,
                    labels: {
                        fontColor: 'rgb(255, 99, 132)'
                    }
                },
                scales: {
                    yAxes: [{
                            ticks: {
                                min: 0,
                                max: 20
                            }
                        }]
                }
            },
            onClick: function (points, evt) {
                console.log(points, evt);
            },
            colors: [
                {
                    backgroundColor: '#0062ff',
                    pointBackgroundColor: '#0062ff',
                    pointHoverBackgroundColor: '#0062ff',
                    borderColor: '#0062ff',
                    pointBorderColor: '#0062ff',
                    pointHoverBorderColor: '#0062ff',
                    fill: false /* this option hide background-color */
                },
                {
                    backgroundColor: '#FDB45C',
                    pointBackgroundColor: '#FDB45C',
                    pointHoverBackgroundColor: '#FDB45C',
                    borderColor: '#FDB45C',
                    pointBorderColor: '#FDB45C',
                    pointHoverBorderColor: '#FDB45C',
                    fill: false /* this option hide background-color */
                },
                {
                    backgroundColor: '#00ADF9',
                    pointBackgroundColor: '#00ADF9',
                    pointHoverBackgroundColor: '#00ADF9',
                    borderColor: '#00ADF9',
                    pointBorderColor: '#00ADF9',
                    pointHoverBorderColor: '#00ADF9',
                    fill: false /* this option hide background-color */
                },
                {
                    backgroundColor: '#46BFBD',
                    pointBackgroundColor: '#46BFBD',
                    pointHoverBackgroundColor: '#46BFBD',
                    borderColor: '#46BFBD',
                    pointBorderColor: '#46BFBD',
                    pointHoverBorderColor: '#46BFBD',
                    fill: false /* this option hide background-color */
                }
            ]
        };

        ////
        $scope.line = {
            labels: ['2010', '2011', '2012', '2013', '2014', '2015', '2016'],
            data: [
                [12, 5, 12, 15, 20, 16, 14],
                [12, 12, 20, 16, 14, 12, 18],
                [11, 18, 11, 20, 16, 14, 11],
                [19, 5, 12, 15, 9, 18, 18],
                [16, 16, 16, 16, 20, 16, 14],
                [7, 5, 12, 15, 17, 8, 8],
                [9, 19, 15, 9, 18, 19, 6]
            ],
            options: {
                scales: {
                    yAxes: [{
                            ticks: {
                                min: 0,
                                max: 20
                            }
                        }]
                }
            },
            onClick: function (points, evt) {
                console.log(points, evt);
            }
        };
        $scope.bar = {
            labels: ['Matematica', 'Comunicacion', 'Ciencias', 'Computacion', 'Geografia', 'Historia', 'Educacion Fisica'],
            series: ['Asistencia', 'Falta', 'Tardanza'],

            data: [
                [[15, 16, 18, 18, 16, 15, 14], [3, 2, 0, 2, 2, 0, 1], [2, 5, 4, 6, 5, 8, 1]],
                [[19, 19, 14, 20, 16, 11, 18], [1, 0, 0, 1, 0, 1, 0], [5, 6, 2, 2, 7, 3, 2]],
                [[18, 18, 18, 18, 19, 11, 14], [3, 2, 0, 2, 0, 0, 1], [5, 1, 0, 6, 3, 8, 3]],
                [[17, 15, 14, 18, 16, 15, 19], [0, 1, 0, 1, 2, 5, 3], [6, 0, 4, 3, 7, 5, 5]]

            ],
            options: {
                scales: {
                    yAxes: [{
                            ticks: {
                                min: 0,
                                max: 20
                            }
                        }]
                },
                fill: false,
                legend: {
                    display: true,
                    labels: {
                        fontColor: 'rgb(255, 99, 132)'
                    }
                }
            }

        };
//        $scope.donut = {
//            labels: ['Asistencia', 'Falta', 'Tardanza'],
//            data: [
//                [[15, 1, 2], [18, 3, 5], [17, 1, 5], [18, 1, 3], [19, 5, 5], [16, 5, 1], [16, 3, 1]],
//                [[16, 3, 1], [16, 5, 1], [19, 5, 7], [17, 1, 5], [16, 3, 1], [18, 3, 5], [15, 3, 2]],
//                [[18, 7, 5], [17, 1, 5], [16, 5, 1], [16, 3, 1], [19, 7, 5], [16, 3, 7], [15, 13, 2]],
//                [[16, 5, 1], [16, 3, 1], [16, 5, 3], [18, 3, 5], [15, 1, 2], [17, 1, 5], [19, 19, 19]]
//
//            ],
//            options: {
//                fill: false,
//                legend: {
//                    display: true,
//                    labels: {
//                        fontColor: 'rgb(255, 99, 132)'
//                    }
//                }
//            }
//
//        };
//
//
//
//        $scope.pie = {
//            labels: ['Asistencia', 'Falta', 'Tardanza'],
//            data: [
//                [[15, 1, 2], [18, 3, 5], [17, 1, 5], [18, 1, 3], [19, 5, 5], [16, 5, 1], [16, 3, 1]],
//                [[16, 3, 1], [16, 5, 1], [19, 5, 7], [17, 1, 5], [16, 3, 1], [18, 3, 5], [15, 3, 2]],
//                [[18, 7, 5], [17, 1, 5], [16, 5, 1], [16, 3, 1], [19, 7, 5], [16, 3, 7], [15, 13, 2]],
//                [[16, 5, 1], [16, 3, 1], [16, 5, 3], [18, 3, 5], [15, 1, 2], [17, 1, 5], [19, 19, 19]]
//
//            ],
//            options: {
//                fill: false,
//                legend: {
//                    display: true,
//                    labels: {
//                        fontColor: 'rgb(255, 99, 132)'
//                    }
//                }
//            }
//        };




        $scope.listar = function () {
            var activo = $scope.active;
            switch (activo) {
                case 0:
                    $scope.listarInterno("");
                    break;
                case 1:
                    $scope.listarInterno("Di");
                    break;
                case 2:
                    $scope.listarInterno("Do");
                    break;
                case 3:
                    $scope.listarInterno("Ad");
                    break;
                case 4:
                    $scope.listarPadres();
                    $scope.tipo = "Pa";
                    break;
                case 5:
                    $scope.listarEstudiantes();
                    $scope.tipo = "Es";
                    break;
            }

        };

        $scope.listarInternoDirectorio = function (traTip) {
            $scope.tipo = traTip;
            //preparamos un objeto request        
            var request = crud.crearRequest('trabajador', 1, 'listarTrabajadoresPorOrganizacionyTipo');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las funciones de exito y error
            request.setData({orgId: $rootScope.usuMaster.organizacion.organizacionID, traTip: traTip});
            crud.listar("/directorio", request, function (data) {
                settingTablaDirInt.dataset = data.data;
                iniciarPosiciones(settingTablaDirInt.dataset);
                $scope.tablaDirInt.settings(settingTablaDirInt);
            }, function (data) {
                console.info(data);
            });
        };

        $scope.listarPadresDirectorio = function () {
            $scope.tipo = "Pa";
            //preparamos un objeto request        
            var request = crud.crearRequest('trabajador', 1, 'listarPadresPorOrganizacion');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las funciones de exito y error
            request.setData({orgId: $rootScope.usuMaster.organizacion.organizacionID});
            crud.listar("/directorio", request, function (data) {
                settingTablaDirInt.dataset = data.data;
                iniciarPosiciones(settingTablaDirInt.dataset);
                $scope.tablaDirInt.settings(settingTablaDirInt);
            }, function (data) {
                console.info(data);
            });
        };

        $scope.listarEstudiantesDirectorio = function () {
            $scope.tipo = "Es";
            //preparamos un objeto request        
            var request = crud.crearRequest('trabajador', 1, 'listarEstudiantesPorOrganizacion');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las funciones de exito y error
            request.setData({orgId: $rootScope.usuMaster.organizacion.organizacionID});
            crud.listar("/directorio", request, function (data) {
                settingTablaDirInt.dataset = data.data;
                iniciarPosiciones(settingTablaDirInt.dataset);
                $scope.tablaDirInt.settings(settingTablaDirInt);
            }, function (data) {
                console.info(data);
            });
        };

        function listarPersonal() {
            var organizacion = JSON.parse(window.atob(localStorage.getItem('organizacion')));
            var request = crud.crearRequest('mep', 1, 'listarTrabajadores');
            request.setMetadataValue('institucion', organizacion.organizacionID);
            crud.listar('/evaluacion_personal', request, function (response) {
                settingTablaPersonalIE.dataset = response.data;
                iniciarPosiciones(settingTablaPersonalIE.dataset);
                $scope.tablaPersonalIE.settings(settingTablaPersonalIE);
            }, function (response) {
                console.info(response);
            });
        }
        ;
        $scope.obtenerResumenEvaluacion = function (row) {
            mepServiceEvaluacion.setCurrentEmployee(row);
            $scope.trabajador = row;
            var self = this;
            self.employee = mepServiceEvaluacion.getCurrentEmployee();
            var request = crud.crearRequest('mep', 1, 'listarResumenEvaluacion');
            request.setMetadataValue('trabajador', '' + row.tra);
            crud.listar('/evaluacion_personal', request, function (data) {
                settingTablaResumenEvalPersonal.dataset = data.data;
                iniciarPosiciones(settingTablaResumenEvalPersonal.dataset);
                $scope.tablaResumenEvalPersonal.settings(settingTablaResumenEvalPersonal);
            }, function (data) {
                modal.mensaje("ERROR", "No se pudo obtener la lista de evaluaciones");
                console.info(data);
            });
            $("#modalEvaluacionPersonalResumen").modal('show');

        };
        $scope.evalPersEsta = {};
        $scope.showEstadIndiv = function (row) {
            var data = {tra: row.tra};
            mepServiceEvaluacion.getDataEstadistica('indiv', data, function (response) {
                var resolve = {
                    data: function () {
                        return response.data;
                    }
                };
                $scope.evalPersEsta.labels = response.data.labels;
                $scope.evalPersEsta.data = response.data.values;
                $scope.evalPersEsta.series = response.data.series;
                $scope.evalPersEsta.datasetOverride = [{yAxisID: 'y-axis-1', lineTension:0}];
                $scope.evalPersEsta.options = {
                    scales: {
                        yAxes: [
                            {
                                id: 'y-axis-1',
                                type: 'linear',
                                display: true,
                                position: 'left',
                                ticks: {
                                    min: 0, 
                                    max: 100
                                }
                            }
                        ]
                    }
                };
                if (row.car === "Docente"){
                    $scope.evalPersEsta.options.scales.yAxes[0].ticks.max = 20;
                }
            }, function (response) {
                modal.mensaje("ERROR", "No se pudo obtener los datos");
            });

            $("#modalEvaluacionPersonalEstadistica").modal('show');

        };
    }]);

