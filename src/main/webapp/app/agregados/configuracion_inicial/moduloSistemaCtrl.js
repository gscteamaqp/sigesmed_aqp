app.controller("moduloSistemaCtrl",["$scope","crud","modal", function ($scope,crud,modal){
        
    $scope.modulos = [];
    $scope.nuevoModulo = {codigo:"",nombre:"",descripcion:"",icono:"",estado:'A'};
    $scope.subModulosSel = [];
    $scope.moduloSel = {};
    
    
    $scope.listarModulos = function(){
        //preparamos un objeto request
        var request = crud.crearRequest('moduloSistema',1,'listarModulos');
        request.setData({listar:true});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/configuracionInicial",request,function(data){
            $scope.modulos = data.data;
        },function(data){
            console.info(data);
        });
    };    
    $scope.agregarModulo = function(){
        
        var request = crud.crearRequest('moduloSistema',1,'insertarModulo');
        request.setData($scope.nuevoModulo);
        
        crud.insertar("/configuracionInicial",request,function(response){
            
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                //recuperamos las variables que nos envio el servidor
                $scope.nuevoModulo.moduloID = response.data.moduloID;
                $scope.nuevoModulo.fecha = response.data.fecha;
                
                //insertamos el elemento a la lista
                $scope.modulos.push($scope.nuevoModulo);                
                //reiniciamos las variables
                $scope.nuevoModulo = {codigo:"",nombre:"",descripcion:"",icono:"",estado:'A'};
                //cerramos la ventana modal
                $('#modalNuevo').modal('hide');
            }            
        },function(data){
            console.info(data);
        });
        
    };
    $scope.eliminarModulo = function(i,idDato){
        
        modal.mensajeConfirmacion($scope,"seguro que desea eliminar este registro",function(){
            
            var request = crud.crearRequest('moduloSistema',1,'eliminarModulo');
            request.setData({moduloID:idDato});

            crud.eliminar("/configuracionInicial",request,function(response){

                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta)
                    $scope.modulos.splice(i,1);

            },function(data){
                console.info(data);
            });
            
        });
        
        
        
    };
    $scope.prepararEditar = function(i,t){
        $scope.moduloSel = JSON.parse(JSON.stringify(t));
        $scope.moduloSel.i = i;
        $('#modalEditar').modal('show');
    };
    $scope.editarModulo = function(){
        
        var request = crud.crearRequest('moduloSistema',1,'actualizarModulo');
        request.setData($scope.moduloSel);
                
        crud.actualizar("/configuracionInicial",request,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){                
                //actualizando
                $scope.modulos[$scope.moduloSel.i] = $scope.moduloSel;
                $('#modalEditar').modal('hide');
            }
        },function(data){
            console.info(data);
        });
    };
    $scope.eliminarModulos = function(){
    };
    $scope.verSubModulos = function(m){
        if(m.subModulos && m.subModulos.length>0 ){
            $scope.subModulosSel = m.subModulos;
            $('#modalVer').modal('show');
        }
    };
    
    
    
}]);