app.controller("orgCtrl",["$scope","crud","modal", function ($scope,crud,modal){
    
    $scope.organizaciones = [];    
    $scope.tipoOrganizaciones = [];
    $scope.org = {
        codigo:"",
        nombre:"",
        alias:"",
        descripcion:"",
        direccion:"",
        estado:'A',
        tipoOrganizacionID:"0",
        organizacionPadreID:"0",
        orgImgInstFile: "",
        imgInst: {
            nombre: "", archivo: "", nombre_archivo: "", doc_ref_url:""
        }
    };
    var organizacion = {};
        
    $scope.buscarOrganizacion = function(orgID){
        //preparamos un objeto request
        var request = crud.crearRequest('organizacion',1,'buscarOrganizacion');
        request.setData({organizacionID:orgID});
        crud.listar("/configuracionInicial",request,function(data){
            $scope.org = data.data;            
            console.log($scope.org);
            organizacion = JSON.parse(JSON.stringify(data.data));
            
        },function(data){
            console.info(data);
        });
    };
    $scope.editarOrganizacion = function(){
        
        modal.mensajeConfirmacion($scope,"Esta seguro que desea cambiar los datos de la organizacion!!",function(){
        
            var request = crud.crearRequest('organizacion',1,'actualizarOrganizacion');
            $scope.org.orgImgInstFile = $scope.org.imgInst.archivo;
            request.setData($scope.org);
            
            console.log(request);
            //return;

            crud.actualizar("/configuracionInicial",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    //actualizando
                    organizacion = JSON.parse(JSON.stringify($scope.org));
                }
            },function(data){
                console.info(data);
            });
        
        });
    };
    
    $scope.cancelar = function(){
        $scope.org = JSON.parse(JSON.stringify(organizacion));
    };
    
    
    listarTipoOrganizaciones();
    listarOrganizaciones();
    function listarTipoOrganizaciones(){
        //preparamos un objeto request
        var request = crud.crearRequest('tipoOrganizacion',1,'listarTipoOrganizaciones');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las organizaciones de exito y error
        crud.listar("/configuracionInicial",request,function(data){
            $scope.tipoOrganizaciones  = data.data;
        },function(data){
            console.info(data);
        });
    };
    function listarOrganizaciones(){
        //preparamos un objeto request
        var request = crud.crearRequest('organizacion',1,'listarOrganizaciones');
        crud.listar("/configuracionInicial",request,function(data){
            $scope.organizaciones = data.data;
        },function(data){
            console.info(data);
        });
    };
    
}]);