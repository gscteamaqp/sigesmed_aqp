app.requires.push('angularModalService');
app.requires.push('ngAnimate');

app.controller("configuracionCpeCtrl", ["$rootScope", "$scope", "NgTableParams", '$window', "crud", "modal", "ModalService", function ($rootScope, $scope, NgTableParams, $window, crud, modal, ModalService) {

    //Implenetacion del controlador
  
    var params = {count: 10};
    var setting = {counts: []};
    $scope.miTabla = new NgTableParams(params, setting);
    $scope.estados = [{id: 1, title:"Activo"},{id: 2, title:"Inactivo"},{id: 3, title:"Cerrado"}];
    $scope.configuracion = {anho:"",documento:"", descripcion: "", responsable: "",idResponsable:"", fechaInicio: "", fechaFin: "", estado: "",org:"",horaTol:0,minTol:0}; 


    $scope.listarConfiguraciones = function () {
        //preparamos un objeto request
        var request = crud.crearRequest('configuracionControl', 1, 'listarConfiguraciones');
        request.setData({organizacionID: $rootScope.usuMaster.organizacion.organizacionID});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/controlPersonal", request, function (data) {
            if (data.responseSta) {
                setting.dataset = data.data;
                iniciarPosiciones(setting.dataset);
                $scope.miTabla.settings(setting);
            }
            else{
                modal.mensaje("CODNFIRMACION","No se Encontro ningun Libro de Asistencia");
            }

        }, function (data) {
            console.info(data);
        });
    };

    $scope.editarConfiguracion = function (t) {
        $scope.configuracionSel = JSON.parse(JSON.stringify(t));


        ModalService.showModal({
            templateUrl: "administrativa/control_personal/configuracionResponsable.html",
            controller: "editarConfiguracionCtrl",
            inputs: {
                configuracion: $scope.configuracionSel,
                title: "Editar Configuracion"
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                if (result.flag) {
                    $scope.configuracion = result.configuracion;
                    //insertamos el elemento a la lista
                  //  setting.dataset[$scope.configuracionSel.i] = $scope.configuracion;
                    //eliminarElemento(setting.dataset, t.i);
                    $scope.listarConfiguraciones();
                    $scope.miTabla.reload();
                    console.log($scope.configuracion);
                }

            });
        });

    };
    $scope.eliminarConfiguracion = function (t) {
        modal.mensajeConfirmacionAuth($scope, "Esta seguro que desea eliminar la Configuracion?", function () {
            $scope.configuracionSel = JSON.parse(JSON.stringify(t));

            var request = crud.crearRequest('configuracionControl', 1, 'eliminarConfiguracion');
            request.setData({id: $scope.configuracionSel.id});

            crud.eliminar("/controlPersonal", request, function (data) {
                modal.mensaje("CONFIRMACION", data.responseMsg);
                if (data.responseSta) {
                    eliminarElemento(setting.dataset, t.i);
                    $scope.miTabla.reload();
                }
            }, function (data) {
                console.info(data);
            });
        });
    };
    $scope.showNuevaConfiguracion = function () {

        ModalService.showModal({
            templateUrl: "administrativa/control_personal/configuracionResponsable.html",
            controller: "configuracionResponsableCtrl",
            inputs: {
                title: "Nueva Configuracion"
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                if (result.flag) {
                    $scope.listarConfiguraciones();

                }
            });
        });
    };

    $scope.iniciarDatos = function () {
        $scope.listarConfiguraciones();
    };
    
}]);


app.controller('configuracionResponsableCtrl', [
'$rootScope','$scope', '$element', 'title', 'close', 'crud', 'modal', 'ModalService', 'NgTableParams',
function ($rootScope,$scope, $element, title, close, crud, modal, ModalService, NgTableParams) {

    //Variable que servira para crear una nueva configuracion
    $scope.configuracion = {anho:"",documento:"", descripcion: "", responsable: "",idResponsable:"", fechaInicio: new Date, fechaFin: new Date, estado: "",org:"",horaTol:0,minTol:0}; 
    $scope.title = title;
    $scope.trabajadores = [];

    $scope.minDateInicio = new Date();
    $scope.maxDateInicio;
    $scope.minDateCierre = new Date();

    $scope.trabajador =  {personaID: '', dni: "", datos: "",cargo: "" };
    $scope.estados = [{id: 1, nom:"Activo"},{id: 2, nom:"Inactivo"},{id: 3, nom:"Cerrado"}];
    $scope.configuracion.estado = $scope.estados[1];

    $scope.showBuscarTrabajador = function () {

        //Variables para manejo de la tabla
        var paramsTrabajadores = {count: 10};
        var settingTrabajadores = {counts: []};
        $scope.tablaTrabajadores = new NgTableParams(paramsTrabajadores, settingTrabajadores);

        listarTablaTrabajadores();

        function  listarTablaTrabajadores() {
            //preparamos un objeto request
            var request = crud.crearRequest('configuracionControl', 1, 'listarTrabajadorUsuario');
            request.setData({organizacionID: $rootScope.usuMaster.organizacion.organizacionID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/controlPersonal", request, function (response) {
                if(response.responseSta)
                {
                    console.log(response.toString());
                    settingTrabajadores.dataset = response.data;
                    //asignando la posicion en el arreglo a cada objeto
                    iniciarPosiciones(settingTrabajadores.dataset);
                    $scope.tablaTrabajadores.settings(settingTrabajadores);
                    console.log($scope.tablaTrabajadores);
                }

            }, function (data) {
                console.info(data);
            });
        };

        ModalService.showModal({
            templateUrl: "administrativa/control_personal/buscarTrabajador.html",
            controller: "buscarTrabajadorCtrl",
            inputs: {
                title: "Seleccionar Trabajador",
                tabla: $scope.tablaTrabajadores
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                if (result.flag) {
                    $scope.trabajador.personaID = result.data.personaID;
                    $scope.trabajador.dni = result.data.dni;
                    $scope.trabajador.datos = result.data.datos;

                }

            });
        });
        console.log($scope.trabajador);
    };

    //  This close function doesn't need to use jQuery or bootstrap, because
    //  the button has the 'data-dismiss' attribute.
    $scope.close = function () {
        close({
            trabajador: $scope.trabajador,
            flag: false
        }, 500); // close, but give 500ms for bootstrap to animate
    };

    //  This cancel function must use the bootstrap, 'modal' function because
    //  the doesn't have the 'data-dismiss' attribute.

    $scope.agregarConfiguracion = function ()
    {
        if ($scope.trabajador.dni == undefined || $scope.trabajador.dni.length == 0)
        {
            modal.mensaje("VALIDACION", "Complete todos los datos");
        }
        else if ($scope.configuracion.descripcion.length < 1 || $scope.trabajador.dni == null || $scope.configuracion.fechaInicio == null || $scope.configuracion.fechaFin == null ||  $scope.configuracion.horaTol==null  || $scope.configuracion.minTol==null)
        {
            modal.mensaje("VALIDACION", "Complete todos los datos");
        }
        else
        {
            var request = crud.crearRequest('configuracionControl', 1, 'agregarConfiguracion');
            $scope.configuracion.responsable = $scope.trabajador.dni;
            $scope.configuracion.org=$rootScope.usuMaster.organizacion.organizacionID;

            request.setData($scope.configuracion);

            crud.insertar("/controlPersonal", request, function (response) {

                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    //  Manually hide the modal.
                    $element.modal('hide');

                    //  Now call close, returning control to the caller.
                    close({
                        trabajador: $scope.trabajador,
                        configuracion:$scope.configuracion,
                        flag: true
                    }, 500); // close, but give 500ms for bootstrap to animate
                }

            }, function (data) {
                console.info(data);
            });
            console.log($scope.trabajador);
        }

    };

    $scope.cancel = function () {

        //  Manually hide the modal.
        $element.modal('hide');

        //  Now call close, returning control to the caller.
        close({
            trabajador: $scope.trabajador,
            flag: false
        }, 500); // close, but give 500ms for bootstrap to animate
    };

    $scope.seleccion_fecha_inicial = function () {

        $scope.minDateCierre = $scope.configuracion.fechaInicio;
        $scope.configuracion.fechaFin = $scope.configuracion.fechaInicio;
    }

    $scope.seleccion_fecha_cierre = function () {

        $scope.maxDateInicio = $scope.configuracion.fechaFin;

    }

}]);

app.controller('buscarTrabajadorCtrl', [
'$scope', '$element', 'title', 'tabla', 'close', 'crud', 'modal', 'NgTableParams',
function ($scope, $element, title, tabla, close, crud, modal, NgTableParams) {


    $scope.title = title;
    $scope.tablaTrabajadores = tabla;
    $scope.search = true;
    $scope.setClickedRow = function (d) {

        //  Manually hide the modal.
        $element.modal('hide');

        //  Now call close, returning control to the caller.
        close({
            data: d,
            flag: true
        }, 500); // close, but give 500ms for bootstrap to animate

        console.log(d);
    };

    $scope.close = function () {
        close({
            flag: false
        }, 500); // close, but give 500ms for bootstrap to animate
    };

    //  This cancel function must use the bootstrap, 'modal' function because
    //  the doesn't have the 'data-dismiss' attribute.
    $scope.cancel = function () {

        //  Manually hide the modal.
        $element.modal('hide');

        //  Now call close, returning control to the caller.
        close({
            flag: false
        }, 500); // close, but give 500ms for bootstrap to animate
    };



}]);

app.controller('editarConfiguracionCtrl', [
'$rootScope','$scope', '$element', 'title', 'configuracion', 'close', 'crud', 'modal', 'ModalService', 'NgTableParams',
function ($rootScope,$scope, $element, title, configuracion, close, crud, modal, ModalService, NgTableParams) {

    //Variable que servira para crear una nueva configuracion
    $scope.configuracion = {id:"",anho:"",documento:"", descripcion: "", responsable: "",idResponsable:"", fechaInicio: "", fechaFin: "", estado: "",org:"",horaTol:0,minTol:0}; 
    $scope.title = title;
    $scope.trabajadores = [];

    $scope.trabajador = {personaID: '', dni: "", datos: "", cargo: ""};

    $scope.estados = [{id: 1, nom: "Activo"}, {id: 2, nom: "Inactivo"}, {id: 3, nom: "Cerrado"}];

    $scope.configuracion=configuracion;
    $scope.trabajador.dni=$scope.configuracion.dniResponsable;
    $scope.trabajador.datos=$scope.configuracion.responsable;
    $scope.configuracion.fechaInicio = stringToDate($scope.configuracion.fechaInicio,"dd/mm/yyyy","/");
    $scope.configuracion.fechaFin = stringToDate($scope.configuracion.fechaFin,"dd/mm/yyyy","/");
    $scope.configuracion.horaTol=Number($scope.configuracion.horaTol);
    $scope.configuracion.minTol=Number($scope.configuracion.minTol);
    $scope.configuracion.estado = buscarObjeto($scope.estados,"id",Number($scope.configuracion.estado))//$scope.estados[1];
    $scope.showBuscarTrabajador = function () {

        //Variables para manejo de la tabla
        var paramsTrabajadores = {count: 10};
        var settingTrabajadores = {counts: []};
        $scope.tablaTrabajadores = new NgTableParams(paramsTrabajadores, settingTrabajadores);

        listarTablaTrabajadores();

        function  listarTablaTrabajadores() {
            //preparamos un objeto request
            var request = crud.crearRequest('configuracionControl', 1, 'listarTrabajadorUsuario');
            request.setData({organizacionID: $rootScope.usuMaster.organizacion.organizacionID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/controlPersonal", request, function (response) {
                if (response.responseSta)
                {
                    console.log(response.toString());
                    settingTrabajadores.dataset = response.data;
                    //asignando la posicion en el arreglo a cada objeto
                    iniciarPosiciones(settingTrabajadores.dataset);
                    $scope.tablaTrabajadores.settings(settingTrabajadores);
                    console.log($scope.tablaTrabajadores);
                }

            }, function (data) {
                console.info(data);
            });
        }
        ;

        ModalService.showModal({
            templateUrl: "administrativa/control_personal/buscarTrabajador.html",
            controller: "buscarTrabajadorCtrl",
            inputs: {
                title: "Seleccionar Trabajador",
                tabla: $scope.tablaTrabajadores
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                if (result.flag) {
                    $scope.trabajador.personaID = result.data.personaID;
                    $scope.trabajador.dni = result.data.dni;
                    $scope.trabajador.datos = result.data.datos;

                }

            });
        });
        console.log($scope.trabajador);
    };

    //  This close function doesn't need to use jQuery or bootstrap, because
    //  the button has the 'data-dismiss' attribute.
    $scope.close = function () {
        close({
            trabajador: $scope.trabajador,
            flag: false
        }, 500); // close, but give 500ms for bootstrap to animate
    };

    //  This cancel function must use the bootstrap, 'modal' function because
    //  the doesn't have the 'data-dismiss' attribute.

    $scope.actualizarConfiguracion = function ()
    {
        if ($scope.trabajador.dni == undefined || $scope.trabajador.dni.length == 0)
        {
            modal.mensaje("VALIDACION", "Complete todos los datos");
        }
        else if ($scope.configuracion.descripcion.length < 1 || $scope.trabajador.dni == null || $scope.configuracion.fechaInicio == null || $scope.configuracion.fechaFin == null || $scope.configuracion.horaTol == null || $scope.configuracion.minTol == null)
        {
            modal.mensaje("VALIDACION", "Complete todos los datos");
        }
        else
        {
            var request = crud.crearRequest('configuracionControl', 1, 'actualizarConfiguracion');
            $scope.configuracion.responsable = $scope.trabajador.dni;
            $scope.configuracion.org = $rootScope.usuMaster.organizacion.organizacionID;
            request.setData($scope.configuracion);

            crud.insertar("/controlPersonal", request, function (response) {

                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    //  Manually hide the modal.
                    $element.modal('hide');

                    //  Now call close, returning control to the caller.
                    close({
                        trabajador: $scope.trabajador,
                        configuracion: $scope.configuracion,
                        flag: true
                    }, 500); // close, but give 500ms for bootstrap to animate
                }

            }, function (data) {
                console.info(data);
            });
            console.log($scope.trabajador);
        }

    };



    $scope.cancel = function () {

        //  Manually hide the modal.
        $element.modal('hide');

        //  Now call close, returning control to the caller.
        close({
            trabajador: $scope.trabajador,
            flag: false
        }, 500); // close, but give 500ms for bootstrap to animate
    };

}]);