app.controller("reportesCpeCtrl", ["$rootScope", "$scope", "NgTableParams", '$window', "crud", "modal", function ($rootScope, $scope, NgTableParams, $window, crud, modal) {

        //Implenetacion del controlador


        var params = {count: 10};
        var setting = {counts: []};
        $scope.miTabla = new NgTableParams(params, setting);
        $scope.tipoOrganizacion="";
        $scope.direcciones=[];
        $scope.ugeles=[];
        $scope.organizaciones=[];
        $scope.niveles=[];
        $scope.listaTipos=[{id:"1",des:"Reporte Listado"},{id:"2",des:"Reporte Barras"},{id:"3",des:"Reporte Circular"},{id:"4",des:"Reporte Lineal"}];
        $scope.labels = [];
        $scope.data = [];
        $scope.reporte={title:""};
        $scope.tipoReporte={id:""};
        
        $scope.trabajador = {
            id: "",
            dni: "",
            nombre: "",
            datos:"",
            materno: "",
            paterno: "",
            condicion: "",
            jornada: "",
            organizacion: ""
        };
        $scope.intervalo={
            desde:"",
            hasta:""
        };
        $scope.sel={
            direccion:"",
            ugel:"",
            organizacion:"",
            reporte:"",
            dia:"",
            mes:"",
            nivel:""
        };
        $scope.meses=[{id:1,nom:"Enero"},{id:2,nom:"Febrero"},{id:3,nom:"Marzo"},{id:4,nom:"Abril"},{id:5,nom:"Mayo"},{id:6,nom:"Junio"},{id:7,nom:"Julio"},{id:8,nom:"Agosto"},{id:9,nom:"Septiembre"},{id:10,nom:"Octubre"},{id:11,nom:"Noviembre"},{id:12,nom:"Diciembre"}];
        
        $scope.reportesHE = [
            {id: 1, nom: "Horas Efectivas por Dia"},
            {id: 2, nom:"Horas Efectivas por Mes"}
        ];
        
        
        $scope.options = {legend: {display: true},
            title: {
                    display: true,
                    text: 'Registro de Asistencia e Inasistencia'
            },
            tooltips: {
                enabled: true,	//no tooltips on mose hover
                cornerRadius: 0, //no border-radius
            }        
        };
        $scope.onClick = function (points, evt) {
            console.log(points, evt);
        };
        $scope.datasetOverride = [{yAxisID: 'y-axis-1'}, {yAxisID: 'y-axis-2'}];
        $scope.options2 = {
            scales: {
                yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    },
                    {
                        id: 'y-axis-2',
                        type: 'linear',
                        display: true,
                        position: 'right'
                    }
                ]
            }
        };
        $scope.bandera=false;
        $scope.verificarDni=function(keyEvent)
        {
            if($scope.bandera)
            {
                $scope.trabajador.dni="";
                $scope.bandera=false;
            }
            if (keyEvent.which > 47  && keyEvent.which <58)
            {
               return;
            }   
//            $scope.reiniciarDatos();
        };
        
        $scope.reiniciarDatos = function () {

            if($scope.trabajador.dni.length!=8)
            {
                $scope.miTabla = new NgTableParams(params, setting);
                setting.dataset = [];
                iniciarPosiciones(setting.dataset);
                $scope.miTabla.settings(setting);
                
                $scope.trabajador.id="";
                $scope.trabajador.nombre="";
                $scope.trabajador.datos="";
                $scope.trabajador.materno="";
                $scope.trabajador.paterno="";
                $scope.trabajador.condicion="";
                $scope.trabajador.jornada="";
                $scope.trabajador.organizacion="";
                
                $scope.organizacionesTrab = [];
                $scope.intervalo.desde="";
                $scope.intervalo.hasta="";
                $scope.tipoReporte.id="";
                $scope.reporte={title:""};
            
            }
            else
            {
                //$scope.buscarOrganizacionesByTrabajador();
                $scope.buscarPersona();
                $scope.bandera=true;
            }
        };
        
        $scope.buscarOrganizacionesByTrabajador = function()
        {
            var request = crud.crearRequest('reportes', 1, 'verificarOrganizacionesByTrabajador');
                request.setData({dni:$scope.trabajador.dni+"",tipoOrg:$scope.tipoOrganizacion});
                //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
                //y las usuarios de exito y error
                crud.listar("/controlPersonal", request, function (data) {
                    if (data.responseSta) {
                        $scope.organizacionesTrab = data.data;                       
                    }     

                }, function (data) {
                    console.info(data);
                });     
        };
        
        $scope.listarDirecciones = function()
        {
            
            var request = crud.crearRequest('reportes', 1, 'listarDR');
            
                console.log(request);
                
                request.setData({organizacionID: $rootScope.usuMaster.organizacion.organizacionID});
                //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
                //y las usuarios de exito y error
                crud.listar("/controlPersonal", request, function (data) {
                    
                    console.log(data);
                    if (data.responseSta) {
                        if(data.data[0].mostrarDR==false)
                        {
                            $scope.direcciones=data.data;
                        }
                        else{
                            
                            if(data.data[0].mostrarUGEL==false)
                            {
                                $scope.direcciones = data.data;     
                                $scope.ugeles = data.data[0].ugel;
                                $scope.sel.organizacion = $scope.direcciones[0].ugel[0].id;
                                $scope.listarOrganizaciones();
                                $scope.sel.direccion = $scope.direcciones[0].id;
                                $scope.sel.ugel = $scope.direcciones[0].ugel[0].id;
                            }
                            else
                            {
                                if(data.data[0].ugel[0].cole.mostrarIE==false)
                                {
                                    $scope.listarOrganizaciones();
                                    $scope.direcciones=data.data;
                                    $scope.ugeles=data.data[0].ugel;
                                    $scope.sel.direccion=$scope.direcciones[0].id;
                                    $scope.sel.ugel=$scope.ugeles[0].id;
                                }
                                else
                                {
                                    $scope.direcciones=data.data;
                                    $scope.ugeles=data.data[0].ugel;
                                    $scope.sel.direccion=$scope.direcciones[0].id;
                                    $scope.sel.ugel=$scope.ugeles[0].id;
                                    $scope.organizaciones=data.data[0].ugel[0].cole;
                                    $scope.sel.organizacion=$scope.organizaciones[0].id;
                                    $scope.listarNiveles();
                                }
                                
                            }
                            
                        }
                        
                    }     
                  //  modal.mensaje("CONFIRMACION",data.responseMsg);
                }, function (data) {
                    console.info(data);
                });     
        };

        $scope.listarUgeles = function()
        {
            $scope.ugeles=[];
            $scope.organizaciones=[];
            var request = crud.crearRequest('reportes', 1, 'listarUgeles');
                request.setData({organizacionID: $scope.sel.ugel});
                //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
                //y las usuarios de exito y error
                console.log($scope.sel);
                console.log(request);
                crud.listar("/controlPersonal", request, function (data) {
                    if (data.responseSta) {
                        $scope.ugeles=data.data;
                    }     
                    modal.mensaje("CONFIRMACION",data.responseMsg);
                }, function (data) {
                    console.info(data);
                });     
        };
        
        $scope.listarOrganizaciones = function()
        {
            $scope.organizaciones=[];
            var request = crud.crearRequest('reportes', 1, 'listarIE');
                request.setData({organizacionID: $scope.sel.organizacion});
                //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
                //y las usuarios de exito y error
                crud.listar("/controlPersonal", request, function (data) {
                    if (data.responseSta) {
                        $scope.organizaciones=data.data;
                    }     
                    modal.mensaje("CONFIRMACION",data.responseMsg);
                }, function (data) {
                    console.info(data);
                });     
        };
        
        $scope.listarNiveles = function()
        {
            $scope.niveles=[];
            var request = crud.crearRequest('reportes', 1, 'listarNiveles');
                request.setData({organizacionID: $scope.sel.organizacion});
                //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
                //y las usuarios de exito y error
                crud.listar("/controlPersonal", request, function (data) {
                    if (data.responseSta) {
                        $scope.niveles=data.data;
                    }     
                    //modal.mensaje("CONFIRMACION",data.responseMsg);
                }, function (data) {
                    console.info(data);
                });     
        };
        
        $scope.reporteHE = function()
        {
            if($scope.sel.direccion===undefined||$scope.sel.direccion==="")
            {
                modal.mensaje("VERIFICACION","Seleccione Direccion Regional");
                return;
            }
            if($scope.sel.ugel===undefined||$scope.sel.ugel==="")
            {
                modal.mensaje("VERIFICACION","Seleccione Ugel");
                return;
            }
            if($scope.sel.organizacion===undefined||$scope.sel.organizacion==="")
            {
                modal.mensaje("VERIFICACION","Seleccione IE");
                return;
            }
            if($scope.sel.reporte===undefined||$scope.sel.reporte==="")
            {
                modal.mensaje("VERIFICACION","Seleccione el tipo de Reporte");
                return;
            }    
            if($scope.sel.reporte===1)//dia
            {
                if($scope.sel.dia===undefined||$scope.sel.dia==="")
                {
                    modal.mensaje("VERIFICACION","Seleccione el dia");
                    return;
                }
                if($scope.sel.nivel===undefined||$scope.sel.nivel==="")
                {
                    modal.mensaje("VERIFICACION","Seleccione el Nivel");
                    return;
                }
                var request = crud.crearRequest('reportes', 1, 'reporteHorasEfectivasDia');
                request.setData({organizacionID: $scope.sel.organizacion,organizacionNombre: buscarContenido($scope.organizaciones,"id","nom",$scope.sel.organizacion),dia:convertirFecha2($scope.sel.dia),nivel:$scope.sel.nivel,nivelNombre:buscarContenido($scope.niveles,"id","nom",$scope.sel.nivel)});
                //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
                //y las usuarios de exito y error
                crud.insertar("/controlPersonal", request, function (data) {
                    if(data.responseSta){
                        window.open("/SIGESMED/archivos/control_personal/reporteDia.xlsx");
                        
                    }  
                    modal.mensaje("CONFIRMACION",data.responseMsg);
                }, function (data) {
                    console.info(data);
                });    
            }
            if($scope.sel.reporte===2)//mes
            {
                if($scope.sel.mes===undefined||$scope.sel.mes==="")
                {
                    modal.mensaje("VERIFICACION","Seleccione el mes");
                    return;
                }
                if($scope.sel.nivel===undefined||$scope.sel.nivel==="")
                {
                    modal.mensaje("VERIFICACION","Seleccione el Nivel");
                    return;
                }
                var request = crud.crearRequest('reportes', 1, 'reporteHorasEfectivasMes');
                request.setData({organizacionID: $scope.sel.organizacion,organizacionNombre: buscarContenido($scope.organizaciones,"id","nom",$scope.sel.organizacion),mes:$scope.sel.mes,nivel:$scope.sel.nivel,nivelNombre:buscarContenido($scope.niveles,"id","nom",$scope.sel.nivel)});
                //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
                //y las usuarios de exito y error
                crud.insertar("/controlPersonal", request, function (data) {
                    if(data.responseSta){
                        window.open("/SIGESMED/archivos/control_personal/reporteMes.xlsx");
                        modal.mensaje("CONFIRMACION",data.responseMsg);
                    }   
                    
                }, function (data) {
                    console.info(data);
                });    
            }  
                
             
        };
        
        $scope.buscarPersona = function()
        {
            
            var request = crud.crearRequest('reportes', 1, 'buscarPersonaxDni');
            request.setData({perDni: $scope.trabajador.dni + "", orgId: $rootScope.usuMaster.organizacion.organizacionID, rolId: $rootScope.usuMaster.rol.rolID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/controlPersonal", request, function (data) {
                console.log(data);
                if (data.responseSta) {
                    /*
                    if(typeof data.data.orgId != 'undefined') {
                        var userOrgId = $rootScope.usuMaster.organizacion.organizacionID;
                        var perOrgId = data.data.orgId;
                        
                        if (userOrgId != perOrgId) {
                            modal.mensaje("CONFIRMACION", "El persona no pertenece a la institucion");
                            $scope.organizaciones = [];
                            return; //No puede visualizar al trabajador si no pertenece a su institucion
                        }
                    }*/
                    $scope.buscarOrganizacionesByTrabajador();
                    $scope.trabajador.datos = data.data.datos;
                    modal.mensaje("CONFIRMACION", data.responseMsg);
                } else {
                    modal.mensaje("CONFIRMACION", data.responseMsg);    
                }

            }, function (data) {
                console.info(data);
            });     
        };
        
        $scope.generarReporte = function()
        {
            if($scope.miTabla.data.length==0)
            {
                modal.mensaje("VERIFICACION", "Datos vacios en el intervalo de fechas");
                return;
            }
            if($scope.tipoReporte.id=="")
            {
                modal.mensaje("VERIFICACION", "Seleccione el Reporte a generar");
                return;
            }
            
            switch($scope.tipoReporte.id) {
                case "1":
                    $scope.reporteListado();
                    break;
                case "2":
                    $scope.preparaReporte(2);
                    break;
                case "3":
                    $scope.preparaReporte(3);
                    break;
                case "4":
                    $scope.preparaReporte(4);
                    break;    
                default:
                     modal.mensaje("ERROR", "Contacte con el administrador");
            }
            
        };
        $scope.preparaReporte = function (t) {
            
            $scope.labels2 = ["Asistencia","Tardanza","Tardanza Justificadas","Faltas","Faltas Justificadas"];
            
            
            if (t == 2)
            {
                var asistenciaN=0;
                var tardanzaN=0;
                var tardanzaJustificadaN=0;
                var faltasN=0;
                var faltasJustificadaN=0;
                var asistenciaC=0;
                var tardanzaC=0;
                var tardanzaJustificadaC=0;
                var faltasC=0;
                var faltasJustificadaC=0;
                $scope.series = ['Nombrado', 'Contratado'];
                $scope.reporte.title = $scope.listaTipos[1].des;
                
                for (var i = 0; i < $scope.miTabla.data.length; i++)
                {
                    if ($scope.miTabla.data[i].condicion == "NOMBRADO")
                    {
                        if ($scope.miTabla.data[i].estado == "ASISTIO")
                        {
                            asistenciaN++;
                        }
                        else if ($scope.miTabla.data[i].estado == "TARDANZA")
                        {
                            if ($scope.miTabla.data[i].obs == "JUSTIFICADA")
                                tardanzaJustificadaN++;
                            else
                                tardanzaN++;
                        }
                        else if ($scope.miTabla.data[i].estado == "FALTA")
                        {
                            if ($scope.miTabla.data[i].obs == "JUSTIFICADA")
                                faltasJustificadaN++;
                            else
                                faltasN++;
                        }
                    }
                    else if($scope.miTabla.data[i].condicion == "CONTRATADO")
                    {
                        if ($scope.miTabla.data[i].estado == "ASISTIO")
                        {
                            asistenciaC++;
                        }
                        else if ($scope.miTabla.data[i].estado == "TARDANZA")
                        {
                            if ($scope.miTabla.data[i].obs == "JUSTIFICADA")
                                tardanzaJustificadaC++;
                            else
                                tardanzaC++;
                        }
                        else if ($scope.miTabla.data[i].estado == "FALTA")
                        {
                            if ($scope.miTabla.data[i].obs == "JUSTIFICADA")
                                faltasJustificadaC++;
                            else
                                faltasC++;
                        }
                    }

                }
                $scope.data2 = [[asistenciaN, tardanzaN, tardanzaJustificadaN, faltasN, faltasJustificadaN],
                    [asistenciaC, tardanzaC, tardanzaJustificadaC, faltasC, faltasJustificadaC]];



                $('#modalReporteBarras').modal('show');
            }
            if (t == 3)
            {
                var asistencia=0;
                var tardanza=0;
                var tardanzaJustificada=0;
                var faltas=0;
                var faltasJustificada=0;
                $scope.reporte.title = $scope.listaTipos[2].des;
                for (var i = 0; i < $scope.miTabla.data.length; i++)
                {
                    
                        if ($scope.miTabla.data[i].estado == "ASISTIO")
                        {
                            asistencia++;
                        }
                        else if ($scope.miTabla.data[i].estado == "TARDANZA")
                        {
                            if ($scope.miTabla.data[i].obs == "JUSTIFICADA")
                                tardanzaJustificada++;
                            else
                                tardanza++;
                        }
                        else if ($scope.miTabla.data[i].estado == "FALTA")
                        {
                            if ($scope.miTabla.data[i].obs == "JUSTIFICADA")
                                faltasJustificada++;
                            else
                                faltas++;
                        }

                }
                $scope.data2 = [asistencia, tardanza, tardanzaJustificada, faltas, faltasJustificada];

                $('#modalReporteCircular').modal('show');
            }
            if (t == 4)
            {
                var asistenciaN=0;
                var tardanzaN=0;
                var tardanzaJustificadaN=0;
                var faltasN=0;
                var faltasJustificadaN=0;
                var asistenciaC=0;
                var tardanzaC=0;
                var tardanzaJustificadaC=0;
                var faltasC=0;
                var faltasJustificadaC=0;
                $scope.series = ['Nombrado', 'Contratado'];
                $scope.reporte.title = $scope.listaTipos[3].des;
                
                for (var i = 0; i < $scope.miTabla.data.length; i++)
                {
                    if ($scope.miTabla.data[i].condicion == "NOMBRADO")
                    {
                        if ($scope.miTabla.data[i].estado == "ASISTIO")
                        {
                            asistenciaN++;
                        }
                        else if ($scope.miTabla.data[i].estado == "TARDANZA")
                        {
                            if ($scope.miTabla.data[i].obs == "JUSTIFICADA")
                                tardanzaJustificadaN++;
                            else
                                tardanzaN++;
                        }
                        else if ($scope.miTabla.data[i].estado == "FALTA")
                        {
                            if ($scope.miTabla.data[i].obs == "JUSTIFICADA")
                                faltasJustificadaN++;
                            else
                                faltasN++;
                        }
                    }
                    else if($scope.miTabla.data[i].condicion == "CONTRATADO")
                    {
                        if ($scope.miTabla.data[i].estado == "ASISTIO")
                        {
                            asistenciaC++;
                        }
                        else if ($scope.miTabla.data[i].estado == "TARDANZA")
                        {
                            if ($scope.miTabla.data[i].obs == "JUSTIFICADA")
                                tardanzaJustificadaC++;
                            else
                                tardanzaC++;
                        }
                        else if ($scope.miTabla.data[i].estado == "FALTA")
                        {
                            if ($scope.miTabla.data[i].obs == "JUSTIFICADA")
                                faltasJustificadaC++;
                            else
                                faltasC++;
                        }
                    }

                }
                $scope.data2 = [[asistenciaN, tardanzaN, tardanzaJustificadaN, faltasN, faltasJustificadaN],
                    [asistenciaC, tardanzaC, tardanzaJustificadaC, faltasC, faltasJustificadaC]];



                $('#modalReporteLineal').modal('show');
            }

        };

        $scope.reporteListado = function ()
        {

            var request = crud.crearRequest('reportes', 1, 'reporteListadoAsistencias');
            request.setData({nom: $scope.trabajador.datos, nomOrg: buscarContenido($scope.organizaciones, "id", "nom", $scope.trabajador.organizacion), perDni: $scope.trabajador.dni + "", orgId: $scope.trabajador.organizacion, desde: convertirFecha2($scope.intervalo.desde) + " 00:00:00", hasta: convertirFecha2($scope.intervalo.hasta) + " 00:00:00"});
            crud.listar("/controlPersonal", request, function (data) {
                $scope.dataBase64 = data.data[0].datareporte;
                window.open($scope.dataBase64);
            }, function (data) {
                console.info(data);
            });
        }
        $scope.listarDatos = function ()
        {
            if ($scope.intervalo.desde == "" || $scope.intervalo.desde == null)
            {
                return;
            }
            if ($scope.intervalo.hasta == "" || $scope.intervalo.hasta == null)
            {
                return;
            }
            if ($scope.trabajador.organizacion == "" || $scope.trabajador.organizacion == null)
            {
                modal.mensaje("VERIFICACION", "Seleccione Organizacion");
                return;
            }

            var request = crud.crearRequest('reportes', 1, 'buscarAsistenciaByFecha');
            request.setData({perDni: $scope.trabajador.dni + "", orgId: $scope.trabajador.organizacion, desde: convertirFecha2($scope.intervalo.desde) + " 00:00:00", hasta: convertirFecha2($scope.intervalo.hasta) + " 00:00:00"});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/controlPersonal", request, function (data) {
                console.log(data);
                if (data.responseSta) {
                    setting.dataset = data.data;
                    //$scope.organizaciones = data.data;
                    iniciarPosiciones(setting.dataset);
                    $scope.miTabla.settings(setting);
                    $scope.miTabla.reload();
                }
                modal.mensaje("CONFIRMACION", data.responseMsg);
            }, function (data) {
                console.info(data);
            });

        };

        $scope.imprimir = function(id){
            console.log(id);
            var grafico= new MyFile("Reporte Asistencias e Inasistencia del Personal Docente y Auxiliar");
            grafico.parseDataURL(document.getElementById(id+"").toDataURL("image/png") );
            grafico.size=0;
            var request=crud.crearRequest('reportes',1,'reporteListadoAsistenciasImagen');
//            request.setData(grafico);
            request.setData({grafico: grafico, nom: $scope.trabajador.datos, nomOrg: buscarContenido($scope.organizaciones, "id", "nom", $scope.trabajador.organizacion), perDni: $scope.trabajador.dni + "", orgId: $scope.trabajador.organizacion, desde: convertirFecha2($scope.intervalo.desde) + " 00:00:00", hasta: convertirFecha2($scope.intervalo.hasta) + " 00:00:00"});
            crud.insertar("/controlPersonal", request,function(data){
                if(data.responseSta){
                    $scope.dataBase64 = data.data[0].datareporte;
                    window.open($scope.dataBase64);
                }
            },function(data){
                console.info(data);
            });
        };

        $scope.iniciarDatos = function ()
        {
            var request = crud.crearRequest('configuracionControl', 1, 'verificarOrganizacion');
            request.setData({organizacionID: $rootScope.usuMaster.organizacion.organizacionID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
                crud.listar("/controlPersonal", request, function (data) {
                    if (data.responseSta) {
                        $scope.tipoOrganizacion=data.data.tipo;
//                        if(data.data.tipo===4)
//                            $scope.organizacion.nom=$rootScope.usuMaster.organizacion.nombre;
                    }     

                }, function (data) {
                    console.info(data);
                });     
        };
        
       
        
    }]);

