
app.controller("registroTrabajadorCtrl", ["$rootScope", "$scope", "NgTableParams", '$window', "crud", "modal", "ModalService", function ($rootScope, $scope, NgTableParams, $window, crud, modal, ModalService) {

        //Implenetacion del controlador


        var params = {count: 10};
        var setting = {counts: []};
        $scope.miTabla = new NgTableParams(params, setting);
        $scope.nuevaPersona = {perId:-1,dni:"",nombre:"",materno:"",paterno:"",nacimiento:"",email:"",numero1:"",numero2:"",fijo:"",direccion:"",existe:true};
        $scope.nuevoTrabajador={fechaIngreso:"",salario:"",cargo:"",usuario:"",org:"",perId:-1,tipo:"",existe:true};
        $scope.tipoTrabajadores=[{id:"Di",title:"Directivo"},{id:"Ad",title:"Administrativo"},{id:"Do",title:"Docente"}];
        $scope.cargoTrabajadores=[{id:"",nombre:""}];
//        $scope.tipoTrabajadorSel={};

        $rootScope.showLoading();
       
        $scope.listarTrabajador = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('registroTrabajador', 1, 'listarTrabajadorDetallado');
            request.setData({organizacionID: $rootScope.usuMaster.organizacion.organizacionID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las funciones de exito y error
            crud.listar("/controlPersonal", request, function (data) {
                                
                if (data.responseSta)
                {
                    setting.dataset = data.data;
                    iniciarPosiciones(setting.dataset);
                    $scope.miTabla.settings(setting);
                    $rootScope.hideLoading();
                }
                else{
                    modal.mensaje("MENSAJE","No se encontro ningun trabajador");
                }
                
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.listarCargoTrabajador = function () {
            //preparamos un objeto request
            if ($scope.nuevoTrabajador.tipo == null)
            {
                $scope.cargoTrabajadores=[{id:"",nombre:""}];
                return ;
            }               
            else
            {
                var request = crud.crearRequest('registroTrabajador', 1, 'listarCargoTrabajador');
                request.setData({tipoTrab: $scope.nuevoTrabajador.tipo.id});
                //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
                //y las funciones de exito y error
                crud.listar("/controlPersonal", request, function (data) {
                    $scope.cargoTrabajadores = data.data;

                }, function (data) {
                    console.info(data);
                });
            }

        };

        $scope.buscarPersona = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('registroTrabajador', 1, 'buscarPersona');
            request.setData({dni: $scope.nuevaPersona.dni,organizacionID: $rootScope.usuMaster.organizacion.organizacionID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/controlPersonal", request, function (data) {
                modal.mensaje("CONFIRMACION", data.responseMsg);
                if (data.responseSta) {
                    $scope.nuevaPersona = data.data.persona;
                    $scope.nuevaPersona.nacimiento = stringToDate($scope.nuevaPersona.nacimiento,"yyyy-mm-dd","-");
                    if (data.data.trabajador)
                    {
                        
                        $scope.nuevoTrabajador = data.data.trabajador;
                        if($scope.nuevoTrabajador.tipo==$scope.tipoTrabajadores[0].id)
                        {
                            $scope.nuevoTrabajador.tipo=$scope.tipoTrabajadores[0];
                        }else if($scope.nuevoTrabajador.tipo==$scope.tipoTrabajadores[1].id)
                        {
                            $scope.nuevoTrabajador.tipo=$scope.tipoTrabajadores[1];
                        }else if($scope.nuevoTrabajador.tipo==$scope.tipoTrabajadores[2].id)
                        {
                            $scope.nuevoTrabajador.tipo=$scope.tipoTrabajadores[2];
                        }
                        $scope.listarCargoTrabajador();
//                        $scope.nuevoTrabajador.cargo=buscarObjeto($scope.cargoTrabajadores,"id",$scope.nuevoTrabajador.cargo.id);
                        $scope.nuevoTrabajador.fechaIngreso= stringToDate($scope.nuevoTrabajador.fechaIngreso,"yyyy-mm-dd","-");
                    }
                    else {
//                        $scope.nuevoTrabajador.nombre = $scope.nuevaPersona.dni;
                          $scope.nuevoTrabajador.existe = false;
                          $scope.nuevoTrabajador.tipo="";
                          $scope.nuevoTrabajador.cargo="";
                          $scope.cargoTrabajadores=[{id:"",nombre:""}];
                    }
                }
                else {
                    
                    $('#registrarUsuario').modal('show');
            
                    $scope.nuevaPersona.existe = false;
                    /*$scope.nuevaPersona.nombre="";
                     $scope.nuevaPersona.paterno="";
                     $scope.nuevaPersona.materno="";
                     $scope.nuevaPersona.nacimiento="";
                     $scope.nuevaPersona.email="";
                     $scope.nuevaPersona.numero1="";
                     $scope.nuevaPersona.numero2="";  */
//                    $scope.nuevoUsuario.nombre = $scope.nuevaPersona.dni;
                    $scope.nuevoTrabajador.tipo="";
                    $scope.nuevoTrabajador.cargo="";
                    $scope.nuevoTrabajador.existe = false;
                }
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.cancelarUsuarioRegistro = function () {
            
            $("#registrarUsuario, #modalNuevo").modal("hide");
        }
        
        $scope.registroUsuario = function () {
            
            $scope.cancelarUsuarioRegistro();
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            
            $window.location.href = '/SIGESMED/app/#/usuarioEmpresa';
        }

        $scope.prepararAgregar = function () {
            
            $scope.nuevaPersona = {perId:-1,dni:"",nombre:"",materno:"",paterno:"",nacimiento:"",email:"",numero1:"",numero2:"",fijo:"",direccion:"",existe:true};
            $scope.nuevoTrabajador={fechaIngreso:"",salario:"",cargo:"",usuario:"",org:"",perId:-1,tipo:"",existe:true};
            $scope.cargoTrabajadores=[{id:"",nombre:""}];

            $("#modalNuevo").modal('show');
        };

        $scope.reiniciarDatos = function () {
            var p = $scope.nuevaPersona;
            var t = $scope.nuevoTrabajador;
            p.existe = true;
            p.perId = -1;
            p.nombre = "";
            p.paterno = "";
            p.materno = "";
            p.nacimiento = "";
            p.email = "";
            p.numero1 = "";
            p.numero2 = "";
            p.direccion = "";
            p.fijo = "";
            t.cargo = "";
            t.existe = true;
            t.fechaIngreso = "";
            t.perId = -1;
            t.salario = "";
            t.tipo="";
//            t.org="";
//            t.usuario="";
           
        };

        $scope.iniciarDatos = function () {
            $scope.listarTrabajador();
            
        };
        $scope.agregarUsuario = function () {
            $scope.nuevoTrabajador.org =$rootScope.usuMaster.organizacion.organizacionID;
            $scope.nuevoTrabajador.tipo=$scope.nuevoTrabajador.tipo.id;
            var request = crud.crearRequest('registroTrabajador', 1, 'nuevoTrabajador');
            request.setData({persona: $scope.nuevaPersona, trabajador: $scope.nuevoTrabajador});

            crud.insertar("/controlPersonal", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    //recuperamos las variables que nos envio el servidor
//                    $scope.nuevoTrabajador.usuarioID = response.data.usuarioID;
//                    $scope.nuevoUsuario.sessionID = response.data.sessionID;
//                    $scope.nuevoUsuario.password = response.data.password;
//                    $scope.nuevoUsuario.rol = buscarContenido($scope.roles, "rolID", "nombre", $scope.nuevoUsuario.rolID);
//                    $scope.nuevoUsuario.organizacion = $scope.oSel.nombre;

                    //insertamos el elemento a la lista
                    insertarElemento(setting.dataset, response.data);
                    $scope.miTabla.reload();
                    //reiniciamos las variables
                    //$scope.nuevoUsuario = {nombre:"",password:"",estado:'A',rolID:0,organizacionID:0};
                    //$scope.nuevaPersona = {dni:"",nombre:"",materno:"",paterno:"",nacimiento:"",email:"",numero1:"",numero2:"",existe:true};
                    //cerramos la ventana modal
                    $('#modalNuevo').modal('hide');
                }
            }, function (data) {
                console.info(data);
            });
        };
    }]);
