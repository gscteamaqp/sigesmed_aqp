app.requires.push('angularModalService');
app.requires.push('ngAnimate');

app.controller("controlAsistenciaCtrl", ["$rootScope", "$scope", "NgTableParams", '$window', "crud", "modal", "ModalService", function ($rootScope, $scope, NgTableParams, $window, crud, modal, ModalService) {

        //Implenetacion del controlador

        var params = {count: 10};
        var setting = {counts: []};
        $scope.miTabla = new NgTableParams(params, setting);
        $scope.datosControl = {
            cabecera: "",
            semana: ""
        };
        $scope.fechaActual = "";
        $scope.fechaSimulatoria = "";
        $scope.registros = [];
        
        $scope.organizaciones = [];
        
        $scope.justificacion = {
            id: "",
            doc: "",
            tipo: "",
            obs: ""

        };
        $scope.iniciarDatos = function () {
            $scope.getHoraServidor();
        };
        
        /**
         * Listar organizaciones      
         * @return {array} Organizaciones que pertenecen a una determinada ugel,
         * o la organizacion del director
         */
        
        $scope.listarOrganizaciones = function() {
            
            var orgId = $rootScope.usuMaster.organizacion.organizacionID;
            
            if ($rootScope.usuMaster.rol.rolID == "7") { //Rol director
                $scope.organizaciones = [{id: orgId, title: $rootScope.usuMaster.organizacion.nombre}];
            
            } else if ($rootScope.usuMaster.rol.rolID == "28") { //Rol Responsable de Planillas Ugel
                $scope.obtenerIE(orgId);
            }
            
            console.log($rootScope.usuMaster);
        }
        
        /**
         * Obtener Instituciones educativas por UGEL
         * @param {int} orgId, organizacion Id
         */
        
        $scope.obtenerIE = function (orgId) {
            
            var request = crud.crearRequest('reportes', 1, 'listarIE');
            request.setData({organizacionID: orgId});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            console.log(request);
            crud.listar("/controlPersonal", request, function (data) {
                if (data.responseSta) {
                                        
                    // Se necesita id, title
                    for (var i=0; i < data.data.length; i++) {
                        var IE = {id: data.data[i].id, title: data.data[i].nom};
                        $scope.organizaciones.push(IE);
                    }                    
                }     
                //modal.mensaje("CONFIRMACION",data.responseMsg);
            }, function (data) {
                console.info(data);
            });
        }

        $scope.listarHorarioHoy = function ()
        {
            var request = crud.crearRequest('controlAsistencia', 1, 'listarControlAsistenchaHoy');
            request.setData({organizacionID: $rootScope.usuMaster.organizacion.organizacionID, rolId: $rootScope.usuMaster.rol.rolID });
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            
            crud.listar("/controlPersonal", request, function (data) {
                if (data.responseSta) {
                    console.log(data);
                    setting.dataset = data.data.trabajadores;
                    $scope.datosControl = data.data.cabecera;
                    iniciarPosiciones(setting.dataset);
                    $scope.miTabla.settings(setting);

                    $rootScope.hideLoading();
                }
                //modal.mensaje("CONFIRMACION", data.responseMsg);

            }, function (data) {
                console.info(data);
            });
        };

        $scope.listarHorarioNext = function ()
        {
            var request = crud.crearRequest('controlAsistencia', 1, 'listarControlAsistenchaNext');
            request.setData({diaActual: convertirFecha2($scope.fechaSimulatoria), organizacionID: $rootScope.usuMaster.organizacion.organizacionID, rolId: $rootScope.usuMaster.rol.rolID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/controlPersonal", request, function (data) {
                if (data.responseSta) {
                    setting.dataset = data.data.trabajadores;
                    $scope.datosControl = data.data.cabecera;
                    iniciarPosiciones(setting.dataset);
                    $scope.miTabla.settings(setting);
                    $scope.fechaSimulatoria = new Date(data.data.fecha + " 00:00:00");

                }
                modal.mensaje("CONFIRMACION", data.responseMsg);

            }, function (data) {
                console.info(data);
            });
        };

        $scope.listarHorarioPrev = function ()
        {
            var request = crud.crearRequest('controlAsistencia', 1, 'listarControlAsistenchaPrev');
            request.setData({diaActual: convertirFecha2($scope.fechaSimulatoria), organizacionID: $rootScope.usuMaster.organizacion.organizacionID, rolId: $rootScope.usuMaster.rol.rolID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/controlPersonal", request, function (data) {
                if (data.responseSta) {
                    setting.dataset = data.data.trabajadores;
                    $scope.datosControl = data.data.cabecera;
                    iniciarPosiciones(setting.dataset);
                    $scope.miTabla.settings(setting);
                    console.log($scope.miTabla);
                    $scope.fechaSimulatoria = new Date(data.data.fecha + " 00:00:00");

                }
                modal.mensaje("CONFIRMACION", data.responseMsg);

            }, function (data) {
                console.info(data);
            });
        };

        $scope.getHoraServidor = function () {
            
            $rootScope.showLoading();
            
            //preparamos un objeto request
            var request = crud.crearRequest('controlAsistencia', 1, 'horaServidor');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/controlPersonal", request, function (data) {

                if (data.responseSta) {
                    $scope.fechaActual = new Date(data.data.fecha);
                    //$scope.fechaSimulatoria=new Date(data.data.fecha.);
                    $scope.fechaSimulatoriaActual = new Date(convertirFecha2($scope.fechaActual) + " 00:00:00");
                    $scope.fechaSimulatoria = new Date(convertirFecha2($scope.fechaActual) + " 00:00:00");
                    $scope.listarHorarioHoy();
                }

            }, function (data) {
                console.info(data);
            });
        };
        //verificarFechaDia
        $scope.verificarFechaDia = function (fecha, hor) {
            if (new Date(fecha) > $scope.fechaSimulatoriaActual)
                return true;

            return  !hor;
        };
        $scope.showJustificacion = function (index, day, id, tn, ttd, f, tid)
        {

            if (id == null)
            {
                var request = crud.crearRequest('controlAsistencia', 1, 'verJustificacionInasistencia');
                request.setData({idTrab: tid, fecha: f});
                //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
                //y las usuarios de exito y error
                crud.listar("/controlPersonal", request, function (data) {
                    if (data.responseSta) {
                        if (data.data.justificacion == "")
                        {
                            $scope.regularizarInasistencia(tn, ttd, data.data.id, index, day);
                        }
                        else
                        {
                            $scope.justificacion = data.data.justificacion;
                            $scope.editarJustificacionInasistencia(tn, ttd, data.data.id, $scope.justificacion);
                        }

                    }
                    modal.mensaje("CONFIRMACION", data.responseMsg);

                }, function (data) {
                    console.info(data);
                });
            }
            else
            {
                var request = crud.crearRequest('controlAsistencia', 1, 'verJustificacion');
                request.setData({regId: id, fecha: f, trabId: tid});
                //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
                //y las usuarios de exito y error
                
                console.log(request);
                
                crud.listar("/controlPersonal", request, function (data) {
                    
                    console.log(data);
                    
                    if (data.responseSta) {
                        $scope.registros = data.data.registros;
                        $scope.regularizarAsistencia(tn, ttd, $scope.registros, index, day);
                    }
                    modal.mensaje("CONFIRMACION", data.responseMsg);

                }, function (data) {
                    console.info(data);
                });
            }

        };


        $scope.regularizarAsistencia = function (nom, dni, reg, index, day)
        {
            ModalService.showModal({
                templateUrl: "administrativa/control_personal/verJustificacionAsistencia.html",
                controller: "justificacionAsistenciaCtrl",
                inputs: {
                    title: "Regularizacion",
                    nombres: nom,
                    dni: dni,
                    registros: reg
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                        var row_temp = $scope.miTabla.data[index]; //Obtenemos todo el registro de la tabla by index
                        row_temp[day] = 4; //Le asignamos el numero para cambiar de clase el boton   
                    }

                });
            });
        };
        $scope.regularizarInasistencia = function (nom, dni, idi, index, day)
        {
            ModalService.showModal({
                templateUrl: "administrativa/control_personal/verJustificacion.html",
                controller: "justificacionInasistenciaCtrl",
                inputs: {
                    title: "Justificacion",
                    nombres: nom,
                    dni: dni,
                    justificacion: $scope.justificacion,
                    inasistencia: idi
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) { 
                        
                        var row_temp = $scope.miTabla.data[index]; //Obtenemos todo el registro de la tabla by index
                        row_temp[day] = 5; //Le asignamos el numero para cambiar de clase el boton                        
                    }

                });
            });
        };

        $scope.editarJustificacionInasistencia = function (nom, dni, idi, jj)
        {
            ModalService.showModal({
                templateUrl: "administrativa/control_personal/verJustificacion.html",
                controller: "justificacionInasistenciaCtrl",
                inputs: {
                    title: "Ver Justificacion",
                    nombres: nom,
                    dni: dni,
                    justificacion: jj,
                    inasistencia: idi
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                        $scope.justificacion = {
                            id: "",
                            doc: "",
                            tipo: "",
                            obs: ""
                        };
                    }

                });
            });
        };

        $scope.showHorasAdicionales = function () {
            ModalService.showModal({
                templateUrl: "administrativa/control_personal/horasAdicionales.html",
                controller: "horasAdicionalesCtrl",
                inputs: {
                    title: "Horas Adicionales",
                    tablaTrabajadores: $scope.miTabla
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {

                    }

                });
            });
        };

    }]);

app.controller('justificacionInasistenciaCtrl', [
    '$scope', '$element', 'title', 'nombres', 'dni', 'justificacion', 'inasistencia', 'close', 'crud', 'modal', 'NgTableParams',
    function ($scope, $element, title, nombres, dni, justificacion, inasistencia, close, crud, modal, NgTableParams) {



        $scope.title = title;
        $scope.justificacion = {datos: nombres, dni: dni, idJus: justificacion.id, tipoJustificacion: justificacion.tipo, tramite: justificacion.doc, observacion: justificacion.obs};
        $scope.tipos = [
                {id: "1", nom: "Licencia"},
                {id: "2", nom: "Permiso"},
                {id: "3", nom: "Comision"},
                {id: "4", nom: "Vacaciones"},
                {id: "5", nom: "Onomastico"},
                {id: "6", nom: "At.Medica"},
                {id: "7", nom: "Justificada"}];
       

        $scope.guardarJustificacion = function ()
        {
            if ($scope.justificacion.tramite == "")
            {
                modal.mensaje("VERIFICACION", "Ingreso el Numero de su Expediente");
                return;
            }
            if ($scope.justificacion.tipoJustificacion == "" || $scope.justificacion.tipoJustificacion == null)
            {
                modal.mensaje("VERIFICACION", "Seleccione el tipo de Regularizacion");
                return;
            }
            var request = crud.crearRequest('controlAsistencia', 1, 'guardarJustificacionInasistencia');
            request.setData({idIna: inasistencia, tipo: $scope.justificacion.tipoJustificacion, tramite: $scope.justificacion.tramite, obs: $scope.justificacion.observacion});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las funciones de exito y error
            crud.insertar("/controlPersonal", request, function (data) {
                if (data.responseSta) {

                    modal.mensaje("CONFIRMACION", data.responseMsg);
                    $element.modal('hide');
                    close({
                        flag: true
                    }, 500); // close, but give 500ms for bootstrap to animate
                }

            }, function (data) {
                console.info(data);
            });
        };
        //editarJustificacion
    //        $scope.editarJustificacion = function ()
    //        {
    //            if ($scope.justificacion.tramite == "")
    //            {
    //                modal.mensaje("VERIFICACION", "Ingreso el Numero de su Expediente");
    //                return;
    //            }
    //            if ($scope.justificacion.tipoJustificacion == "" || $scope.justificacion.tipoJustificacion == null)
    //            {
    //                modal.mensaje("VERIFICACION", "Seleccione el tipo de Regularizacion");
    //                return;
    //            }
    //            var request = crud.crearRequest('controlAsistencia', 1, 'editarJustificacionInasistencia');
    //            request.setData({idIna: inasistencia, idJusti: $scope.justificacion.idJus, tipo: $scope.justificacion.tipoJustificacion, tramite: $scope.justificacion.tramite, obs: $scope.justificacion.observacion});
    //            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
    //            //y las funciones de exito y error
    //            crud.insertar("/controlPersonal", request, function (data) {
    //                if (data.responseSta) {
    //
    //                    modal.mensaje("CONFIRMACION", data.responseMsg);
    //                    $element.modal('hide');
    //                    close({
    //                        flag: true
    //                    }, 500); // close, but give 500ms for bootstrap to animate
    //                }
    //
    //            }, function (data) {
    //                console.info(data);
    //            });
    //        };

        $scope.close = function () {
            close({
                flag: false
            }, 500); // close, but give 500ms for bootstrap to animate
        };

        //  This cancel function must use the bootstrap, 'modal' function because
        //  the doesn't have the 'data-dismiss' attribute.
        $scope.cancel = function () {

            //  Manually hide the modal.
            $element.modal('hide');

            //  Now call close, returning control to the caller.
            close({
                flag: false
            }, 500); // close, but give 500ms for bootstrap to animate
        };
    }]);

app.controller('justificacionAsistenciaCtrl', [
    '$scope', '$element', 'title', 'nombres', 'dni', 'registros', 'close', 'crud', 'modal', 'NgTableParams',
    function ($scope, $element, title, nombres, dni, registros, close, crud, modal, NgTableParams) {
        var params = {count: 10};
        var setting = {counts: []};
        $scope.miTabla = new NgTableParams(params, setting);
        setting.dataset = registros;
        iniciarPosiciones(setting.dataset);
        $scope.miTabla.settings(setting);             
        
        $scope.title = title;
        $scope.justificacion = {datos: nombres, dni: dni, idJus: "", tipoJustificacion: "", tramite: "", observacion: ""};
        $scope.tipos = [
                {id: "1", nom: "Licencia"},
                {id: "2", nom: "Permiso"},
                {id: "3", nom: "Comision"},
                {id: "4", nom: "Vacaciones"},
                {id: "5", nom: "Onomastico"},
                {id: "6", nom: "At.Medica"},
                {id: "7", nom: "Justificada"}];

        $scope.setClickedRow = function (d) {
            $scope.obj = JSON.parse(JSON.stringify(d));
            $scope.justificacion.idJus = $scope.obj.justificacion.id;
            $scope.justificacion.tipoJustificacion = $scope.obj.justificacion.estado;
            $scope.justificacion.tramite = $scope.obj.justificacion.num;
            $scope.justificacion.observacion = $scope.obj.justificacion.obs;
            $scope.obj.estadoEsp = $scope.obj.estado === 1 ? "Asistio" : $scope.obj.estado === 2 ? "Tardanza" : "Otros";
            if ($scope.justificacion.idJus === "")
            {
                $scope.title = "Regularizacion";
            }
            else
            {
                $scope.title = "Ver Regularizacion";
            }
        };
        $scope.guardarRegularizacion = function ()
        {
            if ($scope.obj == null)
            {
                modal.mensaje("VERIFICACION", "Seleccione el Registro a Regularizar");
                return;
            }
            if ($scope.justificacion.tramite == "")
            {
                modal.mensaje("VERIFICACION", "Ingreso el Numero de su Expediente");
                return;
            }
            if ($scope.justificacion.tipoJustificacion == "" || $scope.justificacion.tipoJustificacion == null)
            {
                modal.mensaje("VERIFICACION", "Seleccione el tipo de Regularizacion");
                return;
            }
            var request = crud.crearRequest('controlAsistencia', 1, 'guardarJustificacionAsistencia');
            request.setData({idReg: $scope.obj.id, tipo: $scope.justificacion.tipoJustificacion, tramite: $scope.justificacion.tramite, obs: $scope.justificacion.observacion});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las funciones de exito y error
            crud.insertar("/controlPersonal", request, function (data) {
                if (data.responseSta) {
                    $scope.obj = null;
                    modal.mensaje("CONFIRMACION", data.responseMsg);
                    $element.modal('hide');
                    close({
                        flag: true
                        
                    }, 500); // close, but give 500ms for bootstrap to animate
                }

            }, function (data) {
                console.info(data);
            });
        };
        //editarJustificacion
//        $scope.editarRegularizacion = function ()
//        {
//            if ($scope.obj == null)
//            {
//                modal.mensaje("VERIFICACION", "Seleccione el Registro a Regularizar");
//                return;
//            }
//            if ($scope.justificacion.tramite == "")
//            {
//                modal.mensaje("VERIFICACION", "Ingreso el Numero de su Expediente");
//                return;
//            }
//            if ($scope.justificacion.tipoJustificacion == "" || $scope.justificacion.tipoJustificacion == null)
//            {
//                modal.mensaje("VERIFICACION", "Seleccione el tipo de Regularizacion");
//                return;
//            }
//            var request = crud.crearRequest('controlAsistencia', 1, 'editarJustificacionAsistencia');
//            request.setData({idReg: $scope.obj.id, idJusti: $scope.justificacion.idJus, tipo: $scope.justificacion.tipoJustificacion, tramite: $scope.justificacion.tramite, obs: $scope.justificacion.observacion});
//            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
//            //y las funciones de exito y error
//            crud.insertar("/controlPersonal", request, function (data) {
//                if (data.responseSta) {
//                    $scope.obj = null;
//                    modal.mensaje("CONFIRMACION", data.responseMsg);
//                    $element.modal('hide');
//                    close({
//                        flag: true
//                    }, 500); // close, but give 500ms for bootstrap to animate
//                }
//
//            }, function (data) {
//                console.info(data);
//            });
//        };

        $scope.close = function () {
            close({
                flag: false
            }, 500); // close, but give 500ms for bootstrap to animate
        };

        //  This cancel function must use the bootstrap, 'modal' function because
        //  the doesn't have the 'data-dismiss' attribute.
        $scope.cancel = function () {

            //  Manually hide the modal.
            $element.modal('hide');

            //  Now call close, returning control to the caller.
            close({
                flag: false
            }, 500); // close, but give 500ms for bootstrap to animate
        };
    }]);

app.controller('horasAdicionalesCtrl', [
    '$scope', '$element', 'title', 'tablaTrabajadores', 'close', 'crud', 'modal','ModalService', 'NgTableParams',
    function ($scope, $element, title, tablaTrabajadores, close, crud, modal,ModalService, NgTableParams) {
        var params = {count: 10};
        var setting = {counts: []};
        $scope.miTablaRegistros = new NgTableParams(params, setting);
        $scope.copiaAsistencias=[];
        $scope.registros=[];
        $scope.title = title;
        $scope.sumMin=0;
        $scope.trabajador={
            id:"",
            dni:"",
            datos:""
        };
        
        $scope.registro={
            fecha:"",
            documento:"",
            minTol:0,
            descripcion:"",
            documentoAdj:{nombreArchivo:"",archivo:{},url:""}
        };
        $scope.showBuscarTrabajador = function () {

            ModalService.showModal({
                templateUrl: "administrativa/control_personal/buscarTrabajador.html",
                controller: "buscarTrabajadorAddCtrl",
                inputs: {
                    title: "Seleccionar Trabajador",
                    tabla: tablaTrabajadores
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                        $scope.trabajador.id = result.data.id;
                        $scope.trabajador.dni = result.data.dni;
                        $scope.trabajador.datos = result.data.datos;
                        console.log("listar asisitencia");
                        $scope.listarAsistenciaPosibles();
                    }

                });
            });
            console.log($scope.trabajador);
        };
        //listarAsistenciaPosibles
        $scope.listarAsistenciaPosibles=function()
        {
            console.log(" request listar Asistencias");
            $scope.registros=[];
            setting.dataset=[];
            $scope.miTablaRegistros.settings(setting);
            if($scope.trabajador.id==="" || $scope.trabajador.id===null)
            {
                return;
            }
            if($scope.registro.fecha==="" || $scope.registro.fecha==null)
            {
                return;
            }
            
            var request = crud.crearRequest('controlAsistencia', 1, 'listarAsistenciaPosibleToAdicional');
                request.setData({idTrab:$scope.trabajador.id,fecha:convertirFecha2($scope.registro.fecha)});
                //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
                //y las usuarios de exito y error
                console.log(request);
                crud.listar("/controlPersonal", request, function (data) {
                    console.log(data);
                    if (data.responseSta) {
                        setting.dataset = data.data.registros;
                        iniciarPosiciones(setting.dataset);
                        $scope.miTablaRegistros.settings(setting);
                        $scope.registros=data.data.registros;
                        $scope.actualizarSumatoria();
                    }
                    else
                    {
                         modal.mensaje("VERIFICACION", data.responseMsg);
                         $scope.registro.minTol=0;
                    }
                   

                }, function (data) {
                    console.info(data);
                });
        };
        
        $scope.actualizarSumatoria=function(){
            $scope.registro.minTol=0;
            for(var a=0;a<$scope.registros.length ;a++)
            {
                $scope.registro.minTol=$scope.registro.minTol+$scope.registros[a].adicional;
            }
        };
        
        $scope.agregarHoraAdicional=function(){
            if($scope.trabajador.id==="" || $scope.trabajador.id==null)
            {
                modal.mensaje("VERIFICACION","Seleccione un Trabajador");
                return;
            }
            if($scope.registro.fecha==="" || $scope.registro.fecha==null)
            {
                modal.mensaje("VERIFICACION","Ingrese Fecha");
                return;
            }
            if($scope.registro.documento==="" || $scope.registro.documento==null)
            {
                modal.mensaje("VERIFICACION","Ingrese Numero de Documento de Autorizacion");
                return;
            }
            if($scope.registro.minTol===0 || $scope.registro.minTol==null)
            {
                modal.mensaje("VERIFICACION","Ingrese los minutos adicionales");
                return;
            }
            if($scope.registro.descripcion==="" || $scope.registro.descripcion==null)
            {
                modal.mensaje("VERIFICACION","Ingrese Numero una descripcion");
                return;
            }
            
            var request = crud.crearRequest('controlAsistencia', 1, 'registrarHoraAdicional');
            
            console.log({idTrab:$scope.trabajador.id,registro:$scope.registro,registros:$scope.registros});
            
            request.setData({idTrab:$scope.trabajador.id,registro:$scope.registro,registros:$scope.registros});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las funciones de exito y error
            crud.insertar("/controlPersonal", request, function (data) {
                if (data.responseSta) {
                    $scope.trabajador={
                        id:"",
                        dni:"",
                        datos:""
                    };

                    $scope.registro={
                        fecha:"",
                        documento:"",
                        minTol:0,
                        descripcion:""
                    };
                    
                    modal.mensaje("CONFIRMACION", data.responseMsg);
                    $element.modal('hide');
                    close({
                        flag: true
                    }, 500); // close, but give 500ms for bootstrap to animate
                }

            }, function (data) {
                console.info(data);
            });
        };

         $scope.editarRegistro = function (i, d) {
            if (d.edi) {
                if(d.copia.adicional <=d.maximo)
                {
                    $scope.registros[i] = d.copia;
                    d.edi = false;
                    $scope.miTablaRegistros.reload();
                    $scope.actualizarSumatoria();
                }
                else
                {
                    modal.mensaje("VERIFICACION", "Minutos maximo a adicionar es "+d.maximo);
                }

            }
            else {
                d.copia = JSON.parse(JSON.stringify(d));
                d.edi = true;
            }
        };
        
        $scope.eliminarRegistro = function (i, d) {
            //si estamso cancelando la edicion
            if (d.edi) {
                d.edi = false;
                //delete d.copia;
                d.copia = null;
            }
            //si queremos eliminar el elemento
            else {
                $scope.registros.splice(i, 1);
                $scope.miTablaRegistros.reload();
                $scope.actualizarSumatoria();
            }
        };

        $scope.close = function () {
            close({
                flag: false
            }, 500); // close, but give 500ms for bootstrap to animate
        };

        //  This cancel function must use the bootstrap, 'modal' function because
        //  the doesn't have the 'data-dismiss' attribute.
        $scope.cancel = function () {

            //  Manually hide the modal.
            $element.modal('hide');

            //  Now call close, returning control to the caller.
            close({
                flag: false
            }, 500); // close, but give 500ms for bootstrap to animate
        };
    }]);


app.controller('buscarTrabajadorAddCtrl', [
    '$scope', '$element', 'title', 'tabla', 'close', 'crud', 'modal', 'NgTableParams',
    function ($scope, $element, title, tabla, close, crud, modal, NgTableParams) {


        $scope.title = title;
        $scope.tablaTrabajadores = tabla;
        $scope.search = true;
        $scope.setClickedRow = function (d) {

            //  Manually hide the modal.
            $element.modal('hide');

            //  Now call close, returning control to the caller.
            close({
                data: d,
                flag: true
            }, 500); // close, but give 500ms for bootstrap to animate

            console.log(d);
        };

        $scope.close = function () {
            close({
                flag: false
            }, 500); // close, but give 500ms for bootstrap to animate
        };

        //  This cancel function must use the bootstrap, 'modal' function because
        //  the doesn't have the 'data-dismiss' attribute.
        $scope.cancel = function () {

            //  Manually hide the modal.
            $element.modal('hide');

            //  Now call close, returning control to the caller.
            close({
                flag: false
            }, 500); // close, but give 500ms for bootstrap to animate
        };



    }]);