
app.controller("asistenciaAulaCtrl", ["$rootScope", "$scope", "NgTableParams", '$window', "crud", "modal", "ModalService", function ($rootScope, $scope, NgTableParams, $window, crud, modal, ModalService) {

        //Implenetacion del controlador
        var params = {count: 10};
        var setting = {counts: []};
        $scope.miTabla = new NgTableParams(params, setting);
        $scope.historial=[{
            id:"",
            ingreso:"",
            salida:"",
            estado:""
        }];
        $scope.ingreso=false;
        $scope.nuevoRegistro={
            id:"",
            ingreso:"",
            salida:"",
            estado:""
        };
        $scope.tolerancia={
            hora:0,
            min:0
        };
        $scope.docente = {
            id: "",
            dni: "",
            nombre: "",
            materno: "",
            paterno: ""
        };
        $scope.control=false;
        $scope.hora={fecha:""};
        $scope.iniciarClock = function () {
            
            var context;

            function getClock()
            {
                if(typeof  clock=='undefined')
                {
                    clearInterval(reloj);
                    return;
                }
                
                //Get Current Time
                $scope.hora.setSeconds($scope.hora.getSeconds() + 1);
                str = prefixZero($scope.hora.getHours(), $scope.hora.getMinutes(), $scope.hora.getSeconds());
                //Get the Context 2D or 3D
                context = clock.getContext("2d");
                context.clearRect(0, 0, 500, 200);
                context.font = "80px Arial";
                context.fillStyle = "#000";
                context.fillText(str, 42, 125);
            }

            function prefixZero(hour, min, sec)
            {
                var curTime;
                if (hour < 10)
                    curTime = "0" + hour.toString();
                else
                    curTime = hour.toString();

                if (min < 10)
                    curTime += ":0" + min.toString();
                else
                    curTime += ":" + min.toString();

                if (sec < 10)
                    curTime += ":0" + sec.toString();
                else
                    curTime += ":" + sec.toString();
                return curTime;
            }

            var reloj=setInterval(getClock, 1000);
        };

        $scope.bandera=false;
        $scope.verificarDni=function(keyEvent)
        {
            if (keyEvent.which === 13)
                $scope.marcarAsistencia();
            
            else if($scope.bandera)
            {
                $scope.docente.dni="";
                $scope.bandera=false;
            }
        };
        
        $scope.reiniciarDatos = function (keyEvent) {
           
            if (keyEvent.which === 13)
                return;
            
            if($scope.docente.dni.length!=8)
            {
                $scope.miTabla = new NgTableParams(params, setting);
                setting.dataset = [];
                iniciarPosiciones(setting.dataset);
                $scope.miTabla.settings(setting);
                
                
                $scope.docente.id="";
                $scope.docente.nombre="";
                $scope.docente.materno="";
                $scope.docente.paterno="";
                        
                    
               

                $scope.historial = [{
                    id: "",
                    ingreso: "",
                    salida: "",
                    estado: ""
                }];
            }
            else
            {
                $scope.buscarTrabajador();
                $scope.bandera=true;
            }
            

        };
        //()
        $scope.marcarAsistencia = function () {
            
            if($scope.docente.id=="")
            {
                modal.mensaje("VALIDACION", "Ingrese un DNI valido y verifiquelo");
                return;
            }
            if(!$scope.ingreso)
            {
                modal.mensaje("VALIDACION", "El docente no Ingreso a la Institucion");
                return;
            }
            var horaRegistro=convertirFecha2($scope.hora)+" "+convertirHora($scope.hora);
            if($scope.historial.length>0)
            {
                $scope.ultimoRegistro=$scope.historial[$scope.historial.length-1];              
            }
            else
            {
                $scope.ultimoRegistro={
                    id: -1,
                    ingreso: "",
                    salida: ""
                   
                };
            }
            
            var request = crud.crearRequest('controlAsistencia', 1, 'marcarAsistenciaAula');
            request.setData({id: $scope.docente.perId,hora:horaRegistro,lastReg:$scope.ultimoRegistro});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            console.log(request);
            crud.listar("/controlPersonal", request, function (data) { 
                if (data.responseSta) {
                    if(data.data.existe)
                    {
                        $scope.historial[$scope.historial.length-1].salida=data.data.salida;
                        $scope.historial[$scope.historial.length-1].hrs=data.data.hrs;
                         $scope.miTabla.reload();
                    }
                    else
                    {
                        $scope.nuevoRegistro.id = data.data.id;
                        $scope.nuevoRegistro.ingreso = data.data.ingreso;
                        $scope.nuevoRegistro.salida = data.data.salida;
                        $scope.nuevoRegistro.estado = data.data.estado;
                        $scope.nuevoRegistro = JSON.parse(JSON.stringify($scope.nuevoRegistro));

                        insertarElemento(setting.dataset, $scope.nuevoRegistro);
                        $scope.miTabla.reload();
                    }
                    
                   
                    $scope.nuevoRegistro = {
                        id: "",
                        ingreso: "",
                        salida: ""
                       
                    };
                }
                modal.mensaje("CONFIRMACION",data.responseMsg);

            }, function (data) {
                console.info(data);
            });

        };
        
        $scope.iniciarDatos=function ()
        {
            $scope.getHoraServidor();
            
        };
        
        $scope.verificarLibroAsistencia = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('controlAsistencia', 1, 'verificarLibroAsistencia');
            request.setData({organizacionID: $rootScope.usuMaster.organizacion.organizacionID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/controlPersonal", request, function (data) {

                if (data.responseSta) {
                    $scope.control = data.data.control;
                    if($scope.control)
                    {    
                       $scope.tolerancia.hora=data.data.toleranciaHora;
                       $scope.tolerancia.min=data.data.toleranciaMin;
                    }
                   
                }
                 modal.mensaje("CONFIRMACION",data.responseMsg);    
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.getHoraServidor = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('controlAsistencia', 1, 'horaServidor');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/controlPersonal", request, function (data) {

                if (data.responseSta) {
                    $scope.hora = new Date(data.data.fecha);
                    $scope.iniciarClock();
                    $scope.verificarLibroAsistencia();
                }

            }, function (data) {
                console.info(data);
            });
        };

        $scope.buscarTrabajador = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('controlAsistencia', 1, 'verificarDocente');
            request.setData({dni: $scope.docente.dni, organizacionID: $rootScope.usuMaster.organizacion.organizacionID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            console.log(request);
            crud.listar("/controlPersonal", request, function (data) {
                
                console.log(data);
                if (data.responseSta) {
                    $scope.docente = data.data.persona;
                    
                    if(data.data.trabajador==undefined)
                    {
                        modal.mensaje("VERIFICACION",data.responseMsg);
                    }
                    else if(data.data.trabajador.ingreso)
                    {
                        $scope.docente.id = data.data.trabajador.perId;
                        $scope.ingreso=true;
                        $scope.historial=data.data.registros;
                        setting.dataset = data.data.registros;
                        iniciarPosiciones(setting.dataset);
                        $scope.miTabla.settings(setting);
                        modal.mensaje("VERIFICACION", data.responseMsg);
                    }
                    else
                    {
                        $scope.docente.id = data.data.trabajador.perId;
                        $scope.ingreso=false;
                        modal.mensaje("VERIFICACION", "El Docente no Ingreso A la Institucion");
                    }
                    
                    
                }

            }, function (data) {
                console.info(data);
            });
        };
        
        
        
    }]);
