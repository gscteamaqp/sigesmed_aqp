app.requires.push('angularModalService');
app.requires.push('ngAnimate');
app.requires.push('ui.bootstrap');
app.controller("listaHorariosCtrl", ["$rootScope", "$scope", "NgTableParams", '$window', "crud", "modal", "ModalService", function ($rootScope, $scope, NgTableParams, $window, crud, modal, ModalService) {

    //Implenetacion del controlador
    console.log("asd");

    var params = {count: 5};
    var setting = {counts: []};
    $scope.miTabla = new NgTableParams(params, setting);

    $scope.horario=[];

    $scope.dia =
            {
                id: "",
                lunDesde: "",
                lunHasta: "",
                marDesde: "",
                marHasta: "",
                mieDesde: "",
                mieHasta: "",
                jueDesde: "",
                jueHasta: "",
                vieDesde: "",
                vieHasta: "",
                sabDesde: "",
                sabHasta: "",
                domDesde: "",
                domHasta: ""
            };

    $scope.calendario =
            [{
                    id: "",
                    fec: "",
                    des: "",
                    reg: ""
                }];
    $scope.diaSemana =
            [{
                    id: "",
                    nom: ""
                }];


    $scope.listarHorarios = function () {
        //preparamos un objeto request
        var request = crud.crearRequest('registroHorario', 1, 'listarHorarios');
        request.setData({organizacionID: $rootScope.usuMaster.organizacion.organizacionID});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/controlPersonal", request, function (data) {
            if (data.responseSta && data.data.length>0) {

                setting.dataset = data.data;
                iniciarPosiciones(setting.dataset);
                $scope.miTabla.settings(setting);
//                    $scope.horarios=[];
//                   

            }


        }, function (data) {
            console.info(data);
        });
    };





    $scope.listarDiaSemana = function () {
        //preparamos un objeto request
        var request = crud.crearRequest('registroHorario', 1, 'listarDiaSemana');

        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/controlPersonal", request, function (data) {
            if (data.responseSta) {

                $scope.diaSemana = data.data;


            }

        }, function (data) {
            console.info(data);
        });
    };

    $scope.listarCalendario = function () {
        //preparamos un objeto request
        var request = crud.crearRequest('registroHorario', 1, 'listarCalendario');
        request.setData({organizacionID: $rootScope.usuMaster.organizacion.organizacionID});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/controlPersonal", request, function (data) {
            if (data.responseSta) {
                $scope.calendario = data.data;
            }

        }, function (data) {
            console.info(data);
        });
    };

    $scope.editarHorario = function (t) {
        $scope.horario=[];
        $scope.configuracionSel = JSON.parse(JSON.stringify(t));
        var request = crud.crearRequest('registroHorario', 1, 'listarHorarioById');
        request.setData({horCabId: $scope.configuracionSel.id});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/controlPersonal", request, function (data) {
            if (data.responseSta) {
                for (var m = 0; m < data.data.length; m++)
                {

                    $scope.diaDet = data.data[m];

                    if ($scope.diaDet.nom == "L")
                    {
                        $scope.dia.lunDesde = $scope.diaDet.lunDesde;
                        $scope.dia.lunHasta = $scope.diaDet.lunHasta;
                        $scope.dia.id = $scope.diaDet.id;
                        var i = 0;

                        if ($scope.horario.length > 0)
                        {
                            for (i = 0; i < $scope.horario.length; i++)
                            {
                                if (!$scope.horario[i].lunDesde)
                                {
                                    break;
                                }

                            }
                            if (i == $scope.horario.length)
                            {
                                $scope.horario.push($scope.dia);
                            }
                            else
                            {
                                $scope.horario[i].lunDesde = $scope.dia.lunDesde;
                                $scope.horario[i].lunHasta = $scope.dia.lunHasta;
                            }

                        }
                        else
                        {
                            $scope.horario.push($scope.dia);
                        }


                    }
                    else if ($scope.diaDet.nom == "M")
                    {

                        $scope.dia.marDesde = $scope.diaDet.marDesde;
                        $scope.dia.marHasta = $scope.diaDet.marHasta;
                        $scope.dia.id = $scope.diaDet.id;

                        var i = 0;
                        if ($scope.horario.length > 0)
                        {
                            for (i = 0; i < $scope.horario.length; i++)
                            {
                                if (!$scope.horario[i].marDesde)
                                {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                                    break;
                                }
                            }
                            if (i == $scope.horario.length)
                            {
                                $scope.horario.push($scope.dia);
                            }
                            else
                            {
                                $scope.horario[i].marDesde = $scope.dia.marDesde;
                                $scope.horario[i].marHasta = $scope.dia.marHasta;
                            }

                        }
                        else
                        {
                            $scope.horario.push($scope.dia);
                        }
                    }
                    else if ($scope.diaDet.nom == "W")
                    {
                        $scope.dia.mieDesde = $scope.diaDet.mieDesde;
                        $scope.dia.mieHasta = $scope.diaDet.mieHasta;
                        $scope.dia.id = $scope.diaDet.id;

                        var i = 0;
                        if ($scope.horario.length > 0)
                        {
                            for (i = 0; i < $scope.horario.length; i++)
                            {
                                if (!$scope.horario[i].mieDesde)
                                {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                                    break;
                                }
                            }
                            if (i == $scope.horario.length)
                            {
                                $scope.horario.push($scope.dia);
                            }
                            else
                            {
                                $scope.horario[i].mieDesde = $scope.dia.mieDesde;
                                $scope.horario[i].mieHasta = $scope.dia.mieHasta;
                            }

                        }
                        else
                        {
                            $scope.horario.push($scope.dia);
                        }
                    }
                    else if ($scope.diaDet.nom == "J")
                    {
                        $scope.dia.jueDesde = $scope.diaDet.jueDesde;
                        $scope.dia.jueHasta = $scope.diaDet.jueHasta;
                        $scope.dia.id = $scope.diaDet.id;

                        var i = 0;
                        if ($scope.horario.length > 0)
                        {
                            for (i = 0; i < $scope.horario.length; i++)
                            {
                                if (!$scope.horario[i].jueDesde)
                                {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                                    break;
                                }

                            }
                            if (i == $scope.horario.length)
                            {
                                $scope.horario.push($scope.dia);
                            }
                            else
                            {
                                $scope.horario[i].jueDesde = $scope.dia.jueDesde;
                                $scope.horario[i].jueHasta = $scope.dia.jueHasta;
                            }

                        }
                        else
                        {
                            $scope.horario.push($scope.dia);
                        }
                    }
                    else if ($scope.diaDet.nom == "V")
                    {
                        $scope.dia.vieDesde = $scope.diaDet.vieDesde;
                        $scope.dia.vieHasta = $scope.diaDet.vieHasta;
                        $scope.dia.id = $scope.diaDet.id;

                        var i = 0;
                        if ($scope.horario.length > 0)
                        {
                            for (i = 0; i < $scope.horario.length; i++)
                            {
                                if (!$scope.horario[i].vieDesde)
                                {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                                    break;
                                }
                            }
                            if (i == $scope.horario.length)
                            {
                                $scope.horario.push($scope.dia);
                            }
                            else
                            {
                                $scope.horario[i].vieDesde = $scope.dia.vieDesde;
                                $scope.horario[i].vieHasta = $scope.dia.vieHasta;
                            }

                        }
                        else
                        {
                            $scope.horario.push($scope.dia);
                        }
                    }
                    else if ($scope.diaDet.nom == "S")
                    {
                        $scope.dia.sabDesde = $scope.diaDet.sabDesde;
                        $scope.dia.sabHasta = $scope.diaDet.sabHasta;
                        $scope.dia.id = $scope.diaDet.id;

                        var i = 0;
                        if ($scope.horario.length > 0)
                        {
                            for (i = 0; i < $scope.horario.length; i++)
                            {
                                if (!$scope.horario[i].sabDesde)
                                {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                                    break;
                                }
                            }
                            if (i == $scope.horario.length)
                            {
                                $scope.horario.push($scope.dia);
                            }
                            else
                            {
                                $scope.horario[i].sabDesde = $scope.dia.sabDesde;
                                $scope.horario[i].sabHasta = $scope.dia.sabHasta;
                            }

                        }
                        else
                        {
                            $scope.horario.push($scope.dia);
                        }
                    }
                    else if ($scope.diaDet.nom == "D")
                    {
                        $scope.dia.domDesde = $scope.diaDet.domDesde;
                        $scope.dia.domHasta = $scope.diaDet.domHasta;
                        $scope.dia.id = $scope.diaDet.id;

                        var i = 0;
                        if ($scope.horario.length > 0)
                        {
                            for (i = 0; i < $scope.horario.length; i++)
                            {
                                if (!$scope.horario[i].domDesde)
                                {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                                    break;
                                }
                            }
                            if (i == $scope.horario.length)
                            {
                                $scope.horario.push($scope.dia);
                            }
                            else
                            {
                                $scope.horario[i].domDesde = $scope.dia.domDesde;
                                $scope.horario[i].domHasta = $scope.dia.domHasta;
                            }

                        }
                        else
                        {
                            $scope.horario.push($scope.dia);
                        }
                    }

                    $scope.dia = {
                        id: "",
                        lunDesde: "",
                        lunHasta: "",
                        marDesde: "",
                        marHasta: "",
                        mieDesde: "",
                        mieHasta: "",
                        jueDesde: "",
                        jueHasta: "",
                        vieDesde: "",
                        vieHasta: "",
                        sabDesde: "",
                        sabHasta: "",
                        domDesde: "",
                        domHasta: ""
                    };

                }
                ModalService.showModal({
                    templateUrl: "administrativa/control_personal/agregarHorario.html",
                    controller: "editarHorarioCtrl",
                    inputs: {
                        title: "Editar Horario",
                        horario: $scope.horario,
                        semana: $scope.diaSemana,
                        cabecera:t
                    }
                }).then(function (modal) {
                    modal.element.modal();
                    modal.close.then(function (result) {
                        if (result.flag) {

                            $scope.listarHorarios();
                            $scope.miTabla.reload();

                        }

                    });
                });
            }

        }, function (data) {
            console.info(data);
        });




    };

    $scope.eliminarHorario = function (t) {
        modal.mensajeConfirmacionAuth($scope, "Esta seguro que desea eliminar?", function () {
            $scope.horarioSel = JSON.parse(JSON.stringify(t));

            var request = crud.crearRequest('registroHorario', 1, 'eliminarHorario');
            request.setData({horarioID: $scope.horarioSel.id});

            crud.eliminar("/controlPersonal", request, function (data) {
                modal.mensaje("CONFIRMACION", data.responseMsg);
                if (data.responseSta) {
                    eliminarElemento(setting.dataset, t.i);
                    $scope.miTabla.reload();
                }
            }, function (data) {
                console.info(data);
            });
        });
    };

    $scope.showConfigurarCalendario = function () {

        ModalService.showModal({
            templateUrl: "administrativa/control_personal/configuracionCalendario.html",
            controller: "configuracionCalendarioCtrl",
            inputs: {
                title: "Configurar Calendario",
                calendario: $scope.calendario
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                if (result.flag) {
                    $scope.listarHorarios();
                    $scope.listarCalendario();
                }
            });
        });
    };
    $scope.showNuevaHorario = function () {

        ModalService.showModal({
            templateUrl: "administrativa/control_personal/agregarHorario.html",
            controller: "agregarNuevoHorarioCtrl",
            inputs: {
                title: "Nuevo Horario",
                semana: $scope.diaSemana

            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                if (result.flag) {
                    $scope.listarHorarios();
                    $scope.miTabla.reload();
                }
            });
        });
    };

    $scope.verDetallado = function (o) {
        //preparamos un objeto request
        $scope.horario=[];
        var request = crud.crearRequest('registroHorario', 1, 'listarHorarioById');
        request.setData({horCabId: o.id});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/controlPersonal", request, function (data) {
            if (data.responseSta) {
                for (var m = 0; m < data.data.length; m++)
                {

                    $scope.diaDet = data.data[m];

                    if ($scope.diaDet.nom == "L")
                    {
                        $scope.dia.lunDesde = $scope.diaDet.lunDesde;
                        $scope.dia.lunHasta = $scope.diaDet.lunHasta;
                        $scope.dia.id = $scope.diaDet.id;
                        var i = 0;

                        if ($scope.horario.length > 0)
                        {
                            for (i = 0; i < $scope.horario.length; i++)
                            {
                                if (!$scope.horario[i].lunDesde)
                                {
                                    break;
                                }

                            }
                            if (i == $scope.horario.length)
                            {
                                $scope.horario.push($scope.dia);
                            }
                            else
                            {
                                $scope.horario[i].lunDesde = $scope.dia.lunDesde;
                                $scope.horario[i].lunHasta = $scope.dia.lunHasta;
                            }

                        }
                        else
                        {
                            $scope.horario.push($scope.dia);
                        }


                    }
                    else if ($scope.diaDet.nom == "M")
                    {

                        $scope.dia.marDesde = $scope.diaDet.marDesde;
                        $scope.dia.marHasta = $scope.diaDet.marHasta;
                        $scope.dia.id = $scope.diaDet.id;

                        var i = 0;
                        if ($scope.horario.length > 0)
                        {
                            for (i = 0; i < $scope.horario.length; i++)
                            {
                                if (!$scope.horario[i].marDesde)
                                {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                                    break;
                                }
                            }
                            if (i == $scope.horario.length)
                            {
                                $scope.horario.push($scope.dia);
                            }
                            else
                            {
                                $scope.horario[i].marDesde = $scope.dia.marDesde;
                                $scope.horario[i].marHasta = $scope.dia.marHasta;
                            }

                        }
                        else
                        {
                            $scope.horario.push($scope.dia);
                        }
                    }
                    else if ($scope.diaDet.nom == "W")
                    {
                        $scope.dia.mieDesde = $scope.diaDet.mieDesde;
                        $scope.dia.mieHasta = $scope.diaDet.mieHasta;
                        $scope.dia.id = $scope.diaDet.id;

                        var i = 0;
                        if ($scope.horario.length > 0)
                        {
                            for (i = 0; i < $scope.horario.length; i++)
                            {
                                if (!$scope.horario[i].mieDesde)
                                {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                                    break;
                                }
                            }
                            if (i == $scope.horario.length)
                            {
                                $scope.horario.push($scope.dia);
                            }
                            else
                            {
                                $scope.horario[i].mieDesde = $scope.dia.mieDesde;
                                $scope.horario[i].mieHasta = $scope.dia.mieHasta;
                            }

                        }
                        else
                        {
                            $scope.horario.push($scope.dia);
                        }
                    }
                    else if ($scope.diaDet.nom == "J")
                    {
                        $scope.dia.jueDesde = $scope.diaDet.jueDesde;
                        $scope.dia.jueHasta = $scope.diaDet.jueHasta;
                        $scope.dia.id = $scope.diaDet.id;

                        var i = 0;
                        if ($scope.horario.length > 0)
                        {
                            for (i = 0; i < $scope.horario.length; i++)
                            {
                                if (!$scope.horario[i].jueDesde)
                                {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                                    break;
                                }

                            }
                            if (i == $scope.horario.length)
                            {
                                $scope.horario.push($scope.dia);
                            }
                            else
                            {
                                $scope.horario[i].jueDesde = $scope.dia.jueDesde;
                                $scope.horario[i].jueHasta = $scope.dia.jueHasta;
                            }

                        }
                        else
                        {
                            $scope.horario.push($scope.dia);
                        }
                    }
                    else if ($scope.diaDet.nom == "V")
                    {
                        $scope.dia.vieDesde = $scope.diaDet.vieDesde;
                        $scope.dia.vieHasta = $scope.diaDet.vieHasta;
                        $scope.dia.id = $scope.diaDet.id;

                        var i = 0;
                        if ($scope.horario.length > 0)
                        {
                            for (i = 0; i < $scope.horario.length; i++)
                            {
                                if (!$scope.horario[i].vieDesde)
                                {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                                    break;
                                }
                            }
                            if (i == $scope.horario.length)
                            {
                                $scope.horario.push($scope.dia);
                            }
                            else
                            {
                                $scope.horario[i].vieDesde = $scope.dia.vieDesde;
                                $scope.horario[i].vieHasta = $scope.dia.vieHasta;
                            }

                        }
                        else
                        {
                            $scope.horario.push($scope.dia);
                        }
                    }
                    else if ($scope.diaDet.nom == "S")
                    {
                        $scope.dia.sabDesde = $scope.diaDet.sabDesde;
                        $scope.dia.sabHasta = $scope.diaDet.sabHasta;
                        $scope.dia.id = $scope.diaDet.id;

                        var i = 0;
                        if ($scope.horario.length > 0)
                        {
                            for (i = 0; i < $scope.horario.length; i++)
                            {
                                if (!$scope.horario[i].sabDesde)
                                {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                                    break;
                                }
                            }
                            if (i == $scope.horario.length)
                            {
                                $scope.horario.push($scope.dia);
                            }
                            else
                            {
                                $scope.horario[i].sabDesde = $scope.dia.sabDesde;
                                $scope.horario[i].sabHasta = $scope.dia.sabHasta;
                            }

                        }
                        else
                        {
                            $scope.horario.push($scope.dia);
                        }
                    }
                    else if ($scope.diaDet.nom == "D")
                    {
                        $scope.dia.domDesde = $scope.diaDet.domDesde;
                        $scope.dia.domHasta = $scope.diaDet.domHasta;
                        $scope.dia.id = $scope.diaDet.id;

                        var i = 0;
                        if ($scope.horario.length > 0)
                        {
                            for (i = 0; i < $scope.horario.length; i++)
                            {
                                if (!$scope.horario[i].domDesde)
                                {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                                    break;
                                }
                            }
                            if (i == $scope.horario.length)
                            {
                                $scope.horario.push($scope.dia);
                            }
                            else
                            {
                                $scope.horario[i].domDesde = $scope.dia.domDesde;
                                $scope.horario[i].domHasta = $scope.dia.domHasta;
                            }

                        }
                        else
                        {
                            $scope.horario.push($scope.dia);
                        }
                    }

                    $scope.dia = {
                        id: "",
                        lunDesde: "",
                        lunHasta: "",
                        marDesde: "",
                        marHasta: "",
                        mieDesde: "",
                        mieHasta: "",
                        jueDesde: "",
                        jueHasta: "",
                        vieDesde: "",
                        vieHasta: "",
                        sabDesde: "",
                        sabHasta: "",
                        domDesde: "",
                        domHasta: ""
                    };

                }

            $scope.showHorarioDetalle(o);   



            }
               // $scope.horario = data.data;

        }, function (data) {
            console.info(data);
        });
    };    

    $scope.showHorarioDetalle = function (o) {

        ModalService.showModal({
            templateUrl: "administrativa/control_personal/agregarHorario.html",
            controller: "verHorarioCtrl",
            inputs: {
                title: "Ver Horario",
                semana: $scope.diaSemana,
                horario:$scope.horario,
                cabecera:o
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                if (result.flag) {
                    //$scope.listarHorarios();
                    $scope.horario=[];
                    //$scope.miTabla.reload();
                }
            });
        });
    };

    $scope.iniciarDatos = function () {
        $scope.listarHorarios();
        $scope.listarCalendario();
        $scope.listarDiaSemana();
    };
}]);


app.controller('configuracionCalendarioCtrl', [
"$rootScope", '$scope', '$element', 'title', 'calendario', 'close', 'crud', 'modal', 'ModalService', 'NgTableParams',
function ($rootScope, $scope, $element, title, calendario, close, crud, modal, ModalService, NgTableParams) {

    $scope.title = title;
    $scope.dt = "";
    $scope.descripciondt = "";
    $scope.nuevoDia = {id: "", des: "", reg: "", fec: "", edi: false};

//        $scope.dt = {
//            id: "",
//            fec: "",
//            des: "",
//            reg: ""
//        };
    $scope.events = calendario;
    $scope.estado = [{id: 1, nom: "No-Laborable"}, {id: 2, nom: "Dia-Recuperacion"}];


    $scope.today = function () {
        $scope.dt = new Date();
    };
    $scope.today();
    $scope.options = {
        customClass: getDayClass,
        minDate: new Date(),
        showWeeks: true,
        dateDisabled: disabled
    };
    $scope.clear = function () {
        $scope.dt = null;
    };

    $scope.setDate = function (year, month, day) {
        $scope.dt = new Date(year, month, day);
    };
    function disabled(data) {
        var date = data.date,
                mode = data.mode;
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    }
    function getDayClass(data) {
        var date = data.date,
                mode = data.mode;
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

            for (var i = 0; i < $scope.events.length; i++) {
                var currentDay = new Date(stringToDate($scope.events[i].fec, "dd/MM/yyyy", "/")).setHours(0, 0, 0, 0);

                if (dayToCheck === currentDay) {
                    return $scope.events[i].reg;
                }
            }
        }

        return '';
    }

    $scope.actualizardt = function () {
        $scope.descripciondt = buscarObjeto($scope.events, "fec", convertirFecha3($scope.dt));
    };

    $scope.editarDia = function (i, d) {
        if (d.edi) {
            var dia = d.copia.fec.getDate()>9?d.copia.fec.getDate():'0'+d.copia.fec.getDate();
            var mes = d.copia.fec.getMonth()>8?d.copia.fec.getMonth()+1:'0'+(d.copia.fec.getMonth()+1);
            var año = d.copia.fec.getFullYear();
            $scope.dt = d.copia.fec;
            d.copia.fec = dia + "/" + mes + "/" + año;
            var idReg = d.copia.reg;
            d.copia.reg = idReg.id;
            var request = crud.crearRequest('registroCalendario', 1, 'editarDiaEspecial');

            request.setData({dia: d.copia, organizacionID: $rootScope.usuMaster.organizacion.organizacionID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las funciones de exito y error
            crud.actualizar("/controlPersonal", request, function (data) {
                if (data.responseSta) {
                    d.copia.id = data.data.id;
                    d.copia.reg = idReg.nom;
                    $scope.events[i] = d.copia;
                    $scope.dt = new Date();
                    $scope.actualizardt();
                    modal.mensaje("CONFIRMACION", data.responseMsg);
                }

            }, function (data) {
                console.info(data);
            });

        }
        else {
            d.copia = JSON.parse(JSON.stringify(d));
            d.copia.fec = stringToDate(d.copia.fec, "dd/MM/yyyy", "/");
            d.copia.reg = buscarObjeto($scope.estado, "nom", d.copia.reg);//(lista, labelClave, labelContenido, idBuscado)
            d.edi = true;

        }
    };

    $scope.agregarDia = function () {
        if ($scope.nuevoDia.fec == null) {
            modal.mensaje("CONFIRMACION", "Seleccione una fecha");
            return;
        }
        if ($scope.nuevoDia.des == "") {
            modal.mensaje("CONFIRMACION", "Ingrese descripcion del dia");
            return;
        }
        if ($scope.nuevoDia.reg == "") {
            modal.mensaje("CONFIRMACION", "Ingrese Motivo");
            return;
        }
        $scope.nuevoDia.copia = JSON.parse(JSON.stringify($scope.nuevoDia));
        $scope.nuevoDia.copia.fec = convertirFecha3($scope.nuevoDia.fec);
        var tempReg = $scope.nuevoDia.copia.reg;
        $scope.nuevoDia.copia.reg = tempReg.id;

        var request = crud.crearRequest('registroCalendario', 1, 'nuevoDiaEspecial');
        request.setData({nuevoDia: $scope.nuevoDia.copia, organizacionID: $rootScope.usuMaster.organizacion.organizacionID});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.insertar("/controlPersonal", request, function (data) {
            if (data.responseSta) {
                $scope.nuevoDia.copia.reg = tempReg.nom;
                $scope.nuevoDia.copia.id = data.data.id;
                $scope.events.push($scope.nuevoDia.copia);
                $scope.dt = stringToDate($scope.nuevoDia.copia.fec, "dd/mm/yyyy", "/");
                $scope.actualizardt();
                $scope.nuevoDia = {id: "", des: "", reg: "", fec: "", edi: false};
                modal.mensaje("CONFIRMACION", data.responseMsg);
            }

        }, function (data) {
            console.info(data);
        });


    };

    $scope.eliminarDia = function (i, d) {
        //si estamso cancelando la edicion
        if (d.edi) {

            d.edi = false;
            //delete d.copia;
            d.copia = null;
        }
        //si queremos eliminar el elemento
        else {
            var request = crud.crearRequest('registroCalendario', 1, 'eliminarDiaEspecial');
            request.setData({dia: d});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las funciones de exito y error
            crud.eliminar("/controlPersonal", request, function (data) {
                if (data.responseSta) {
                    $scope.events.splice(i, 1);
                    $scope.dt = new Date();
                    $scope.actualizardt();
                    modal.mensaje("CONFIRMACION", data.responseMsg);
                }

            }, function (data) {
                console.info(data);
            });

        }
    };
    $scope.close = function () {
        close({
            flag: false
        }, 500); // close, but give 500ms for bootstrap to animate
    };
    $scope.cancel = function () {

        //  Manually hide the modal.
        $element.modal('hide');

        //  Now call close, returning control to the caller.
        close({
            flag: false
        }, 500); // close, but give 500ms for bootstrap to animate
    };
}]);

app.controller('agregarNuevoHorarioCtrl', [
"$rootScope", '$scope', '$element', 'title', 'semana', 'close', 'crud', 'modal', 'ModalService', 'NgTableParams',
function ($rootScope, $scope, $element, title, semana, close, crud, modal, ModalService, NgTableParams) {

    $scope.title = title;

    console.log("Agregar Horario");

    $scope.horarioGeneral = {
        id: "",
        des: "",
        horario: []
    };
    $scope.diaSemana = semana;
    $scope.diaNuevo = {dia: "", desde: "", hasta: ""};
    $scope.nuevo = {
        id: "",
        des: "",
        lunDesde: "",
        lunHasta: "",
        marDesde: "",
        marHasta: "",
        mieDesde: "",
        mieHasta: "",
        jueDesde: "",
        jueHasta: "",
        vieDesde: "",
        vieHasta: "",
        sabDesde: "",
        sabHasta: "",
        domDesde: "",
        domHasta: "",
        edi: false
    };

    $scope.horario = [];
    $scope.agregarDia = function () {

        if ($scope.diaNuevo.desde == null || $scope.diaNuevo.desde == "") {
            modal.mensaje("CONFIRMACION", "Seleccione una Hora de Entrada");
            return;
        }
        if ($scope.diaNuevo.hasta == null || $scope.diaNuevo.hasta == "") {
            modal.mensaje("CONFIRMACION", "Seleccione una Hora de Salida");
            return;
        }
        if ($scope.diaNuevo.dia == "") {
            modal.mensaje("CONFIRMACION", "Seleccione un Dia de la Semana");
            return;
        }

        if ($scope.diaNuevo.dia[0] == "L")
        {

            $scope.nuevo.lunDesde = $scope.diaNuevo.desde;
            $scope.nuevo.lunHasta = $scope.diaNuevo.hasta;

            var i = 0;

            if ($scope.horario.length > 0)
            {
                for (i = 0; i < $scope.horario.length; i++)
                {
                    if (!$scope.horario[i].lunDesde)
                    {
                        break;
                    }

                }
                if (i == $scope.horario.length)
                {
                    $scope.horario.push($scope.nuevo);
                }
                else
                {
                    $scope.horario[i].lunDesde = $scope.nuevo.lunDesde;
                    $scope.horario[i].lunHasta = $scope.nuevo.lunHasta;
                }

//                    $scope.horario.splice(i, 0, $scope.nuevo);
            }
            else
            {
                $scope.horario.push($scope.nuevo);
            }


        }
        else if ($scope.diaNuevo.dia[0] == "M")
        {

            $scope.nuevo.marDesde = $scope.diaNuevo.desde;
            $scope.nuevo.marHasta = $scope.diaNuevo.hasta;

            var i = 0;
            if ($scope.horario.length > 0)
            {
                for (i = 0; i < $scope.horario.length; i++)
                {
                    if (!$scope.horario[i].marDesde)
                    {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                        break;
                    }
                }
                if (i == $scope.horario.length)
                {
                    $scope.horario.push($scope.nuevo);
                }
                else
                {
                    $scope.horario[i].marDesde = $scope.nuevo.marDesde;
                    $scope.horario[i].marHasta = $scope.nuevo.marHasta;
                }

            }
            else
            {
                $scope.horario.push($scope.nuevo);
            }
        }
        else if ($scope.diaNuevo.dia[0] == "W")
        {
            $scope.nuevo.mieDesde = $scope.diaNuevo.desde;
            $scope.nuevo.mieHasta = $scope.diaNuevo.hasta;

            var i = 0;
            if ($scope.horario.length > 0)
            {
                for (i = 0; i < $scope.horario.length; i++)
                {
                    if (!$scope.horario[i].mieDesde)
                    {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                        break;
                    }
                }
                if (i == $scope.horario.length)
                {
                    $scope.horario.push($scope.nuevo);
                }
                else
                {
                    $scope.horario[i].mieDesde = $scope.nuevo.mieDesde;
                    $scope.horario[i].mieHasta = $scope.nuevo.mieHasta;
                }

            }
            else
            {
                $scope.horario.push($scope.nuevo);
            }
        }
        else if ($scope.diaNuevo.dia[0] == "J")
        {
            $scope.nuevo.jueDesde = $scope.diaNuevo.desde;
            $scope.nuevo.jueHasta = $scope.diaNuevo.hasta;

            var i = 0;
            if ($scope.horario.length > 0)
            {
                for (i = 0; i < $scope.horario.length; i++)
                {
                    if (!$scope.horario[i].jueDesde)
                    {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                        break;
                    }

                }
                if (i == $scope.horario.length)
                {
                    $scope.horario.push($scope.nuevo);
                }
                else
                {
                    $scope.horario[i].jueDesde = $scope.nuevo.jueDesde;
                    $scope.horario[i].jueHasta = $scope.nuevo.jueHasta;
                }

            }
            else
            {
                $scope.horario.push($scope.nuevo);
            }
        }
        else if ($scope.diaNuevo.dia[0] == "V")
        {
            $scope.nuevo.vieDesde = $scope.diaNuevo.desde;
            $scope.nuevo.vieHasta = $scope.diaNuevo.hasta;

            var i = 0;
            if ($scope.horario.length > 0)
            {
                for (i = 0; i < $scope.horario.length; i++)
                {
                    if (!$scope.horario[i].vieDesde)
                    {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                        break;
                    }
                }
                if (i == $scope.horario.length)
                {
                    $scope.horario.push($scope.nuevo);
                }
                else
                {
                    $scope.horario[i].vieDesde = $scope.nuevo.vieDesde;
                    $scope.horario[i].vieHasta = $scope.nuevo.vieHasta;
                }

            }
            else
            {
                $scope.horario.push($scope.nuevo);
            }
        }
        else if ($scope.diaNuevo.dia[0] == "S")
        {
            $scope.nuevo.sabDesde = $scope.diaNuevo.desde;
            $scope.nuevo.sabHasta = $scope.diaNuevo.hasta;

            var i = 0;
            if ($scope.horario.length > 0)
            {
                for (i = 0; i < $scope.horario.length; i++)
                {
                    if (!$scope.horario[i].sabDesde)
                    {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                        break;
                    }
                }
                if (i == $scope.horario.length)
                {
                    $scope.horario.push($scope.nuevo);
                }
                else
                {
                    $scope.horario[i].sabDesde = $scope.nuevo.sabDesde;
                    $scope.horario[i].sabHasta = $scope.nuevo.sabHasta;
                }

            }
            else
            {
                $scope.horario.push($scope.nuevo);
            }
        }
        else if ($scope.diaNuevo.dia[0] == "D")
        {
            $scope.nuevo.domDesde = $scope.diaNuevo.desde;
            $scope.nuevo.domHasta = $scope.diaNuevo.hasta;

            var i = 0;
            if ($scope.horario.length > 0)
            {
                for (i = 0; i < $scope.horario.length; i++)
                {
                    if (!$scope.horario[i].domDesde)
                    {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                        break;
                    }
                }
                if (i == $scope.horario.length)
                {
                    $scope.horario.push($scope.nuevo);
                }
                else
                {
                    $scope.horario[i].domDesde = $scope.nuevo.domDesde;
                    $scope.horario[i].domHasta = $scope.nuevo.domHasta;
                }

            }
            else
            {
                $scope.horario.push($scope.nuevo);
            }
        }

        $scope.nuevo = {
            id: "",
            des: "",
            lunDesde: "",
            lunHasta: "",
            marDesde: "",
            marHasta: "",
            mieDesde: "",
            mieHasta: "",
            jueDesde: "",
            jueHasta: "",
            vieDesde: "",
            vieHasta: "",
            sabDesde: "",
            sabHasta: "",
            domDesde: "",
            domHasta: "",
            edi: false};
//            $scope.nuevoDia.copia = JSON.parse(JSON.stringify($scope.nuevoDia));
//            $scope.nuevoDia.copia.fec=convertirFecha($scope.nuevoDia.fec);
//            var tempReg=$scope.nuevoDia.copia.reg;
//            $scope.nuevoDia.copia.reg=tempReg.id;
//           



    };
    $scope.editarFila = function (i, d) {
        if (d.edi) {
            $scope.horario[i] = d.copia;
//                $scope.horario[i]=JSON.parse(JSON.stringify($scope.horario[i]));
            d.edi = false;
        }
        else {
            d.copia = JSON.parse(JSON.stringify(d));
            //d.copia.
            if (d.copia.lunDesde != "")
            {
                d.copia.lunDesde = new Date(d.copia.lunDesde);
                d.copia.lunHasta = new Date(d.copia.lunHasta);
            }
            if (d.copia.marDesde != "")
            {
                d.copia.marDesde = new Date(d.copia.marDesde);
                d.copia.marHasta = new Date(d.copia.marHasta);
            }
            if (d.copia.mieDesde != "")
            {
                d.copia.mieDesde = new Date(d.copia.mieDesde);
                d.copia.mieHasta = new Date(d.copia.mieHasta);
            }
            if (d.copia.jueDesde != "")
            {
                d.copia.jueDesde = new Date(d.copia.jueDesde);
                d.copia.jueHasta = new Date(d.copia.jueHasta);
            }
            if (d.copia.vieDesde != "")
            {
                d.copia.vieDesde = new Date(d.copia.vieDesde);
                d.copia.vieHasta = new Date(d.copia.vieHasta);
            }
            if (d.copia.sabDesde != "")
            {
                d.copia.sabDesde = new Date(d.copia.sabDesde);
                d.copia.sabHasta = new Date(d.copia.sabHasta);
            }
            if (d.copia.domDesde != "")
            {
                d.copia.domDesde = new Date(d.copia.domDesde);
                d.copia.domHasta = new Date(d.copia.domHasta);
            }


            d.edi = true;
        }
    };
    $scope.eliminarFila = function (i, d) {
        //si estamso cancelando la edicion
        if (d.edi) {
            d.edi = false;
            //delete d.copia;
            d.copia = null;
        }
        //si queremos eliminar el elemento
        else {
            $scope.horario.splice(i, 1);
        }
    };
    $scope.guardarHorario = function () {
        if ($scope.horarioGeneral.des == "" || $scope.horarioGeneral.des == null)
        {
            modal.mensaje("CONFIRMACION", "Ingrese una Descripcion");
            return;
        }
        if ($scope.horario.length == 0 || $scope.horario == null)
        {
            modal.mensaje("CONFIRMACION", "Agregue el horario respectivo");
            return;
        }
        $scope.horarioGeneral.horario = JSON.parse(JSON.stringify($scope.horario));

        var request = crud.crearRequest('registroHorario', 1, 'nuevoHorario');
        request.setData({horario: $scope.horarioGeneral, organizacionID: $rootScope.usuMaster.organizacion.organizacionID});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.insertar("/controlPersonal", request, function (data) {
            if (data.responseSta) {

                $scope.horario = [];
                $scope.horarioGeneral = {
                    id: "",
                    des: "",
                    horario: []
                };
                modal.mensaje("CONFIRMACION", data.responseMsg);
                $element.modal('hide');
                close({
                    flag: true
                }, 500); // close, but give 500ms for bootstrap to animate
            }

        }, function (data) {
            console.info(data);
        });
    };

//        $scope.eliminarFila = function (o) {
//            $scope.horario.splice(o.i,1)
//        };


    $scope.close = function () {
        close({
            flag: false
        }, 500); // close, but give 500ms for bootstrap to animate
    };
    $scope.cancel = function () {

        //  Manually hide the modal.
        $element.modal('hide');

        //  Now call close, returning control to the caller.
        close({
            flag: false
        }, 500); // close, but give 500ms for bootstrap to animate
    };
}]);

app.controller('verHorarioCtrl', [
"$rootScope", '$scope', '$element', 'title', 'semana','horario','cabecera', 'close', 'crud', 'modal', 'ModalService', 'NgTableParams',
function ($rootScope, $scope, $element, title, semana,horario,cabecera, close, crud, modal, ModalService, NgTableParams) {

    $scope.title = title;
    $scope.cabecera=cabecera.des;
    $scope.horarioGeneral = {
        id: "",
        des: "",
        horario: []
    };

    $scope.diaSemana = semana;
    $scope.diaNuevo = {dia: "", desde: "", hasta: ""};
    $scope.nuevo = {
        id: "",
        des: "",
        lunDesde: "",
        lunHasta: "",
        marDesde: "",
        marHasta: "",
        mieDesde: "",
        mieHasta: "",
        jueDesde: "",
        jueHasta: "",
        vieDesde: "",
        vieHasta: "",
        sabDesde: "",
        sabHasta: "",
        domDesde: "",
        domHasta: "",
        edi: false
    };

    $scope.horario = horario;
    $scope.agregarDia = function () {

        if ($scope.diaNuevo.desde == null || $scope.diaNuevo.desde == "") {
            modal.mensaje("CONFIRMACION", "Seleccione una Hora de Entrada");
            return;
        }
        if ($scope.diaNuevo.hasta == null || $scope.diaNuevo.hasta == "") {
            modal.mensaje("CONFIRMACION", "Seleccione una Hora de Salida");
            return;
        }
        if ($scope.diaNuevo.dia == "") {
            modal.mensaje("CONFIRMACION", "Seleccione un Dia de la Semana");
            return;
        }

        if ($scope.diaNuevo.dia[0] == "L")
        {

            $scope.nuevo.lunDesde = $scope.diaNuevo.desde;
            $scope.nuevo.lunHasta = $scope.diaNuevo.hasta;

            var i = 0;

            if ($scope.horario.length > 0)
            {
                for (i = 0; i < $scope.horario.length; i++)
                {
                    if (!$scope.horario[i].lunDesde)
                    {
                        break;
                    }

                }
                if (i == $scope.horario.length)
                {
                    $scope.horario.push($scope.nuevo);
                }
                else
                {
                    $scope.horario[i].lunDesde = $scope.nuevo.lunDesde;
                    $scope.horario[i].lunHasta = $scope.nuevo.lunHasta;
                }

//                    $scope.horario.splice(i, 0, $scope.nuevo);
            }
            else
            {
                $scope.horario.push($scope.nuevo);
            }


        }
        else if ($scope.diaNuevo.dia[0] == "M")
        {

            $scope.nuevo.marDesde = $scope.diaNuevo.desde;
            $scope.nuevo.marHasta = $scope.diaNuevo.hasta;

            var i = 0;
            if ($scope.horario.length > 0)
            {
                for (i = 0; i < $scope.horario.length; i++)
                {
                    if (!$scope.horario[i].marDesde)
                    {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                        break;
                    }
                }
                if (i == $scope.horario.length)
                {
                    $scope.horario.push($scope.nuevo);
                }
                else
                {
                    $scope.horario[i].marDesde = $scope.nuevo.marDesde;
                    $scope.horario[i].marHasta = $scope.nuevo.marHasta;
                }

            }
            else
            {
                $scope.horario.push($scope.nuevo);
            }
        }
        else if ($scope.diaNuevo.dia[0] == "W")
        {
            $scope.nuevo.mieDesde = $scope.diaNuevo.desde;
            $scope.nuevo.mieHasta = $scope.diaNuevo.hasta;

            var i = 0;
            if ($scope.horario.length > 0)
            {
                for (i = 0; i < $scope.horario.length; i++)
                {
                    if (!$scope.horario[i].mieDesde)
                    {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                        break;
                    }
                }
                if (i == $scope.horario.length)
                {
                    $scope.horario.push($scope.nuevo);
                }
                else
                {
                    $scope.horario[i].mieDesde = $scope.nuevo.mieDesde;
                    $scope.horario[i].mieHasta = $scope.nuevo.mieHasta;
                }

            }
            else
            {
                $scope.horario.push($scope.nuevo);
            }
        }
        else if ($scope.diaNuevo.dia[0] == "J")
        {
            $scope.nuevo.jueDesde = $scope.diaNuevo.desde;
            $scope.nuevo.jueHasta = $scope.diaNuevo.hasta;

            var i = 0;
            if ($scope.horario.length > 0)
            {
                for (i = 0; i < $scope.horario.length; i++)
                {
                    if (!$scope.horario[i].jueDesde)
                    {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                        break;
                    }

                }
                if (i == $scope.horario.length)
                {
                    $scope.horario.push($scope.nuevo);
                }
                else
                {
                    $scope.horario[i].jueDesde = $scope.nuevo.jueDesde;
                    $scope.horario[i].jueHasta = $scope.nuevo.jueHasta;
                }

            }
            else
            {
                $scope.horario.push($scope.nuevo);
            }
        }
        else if ($scope.diaNuevo.dia[0] == "V")
        {
            $scope.nuevo.vieDesde = $scope.diaNuevo.desde;
            $scope.nuevo.vieHasta = $scope.diaNuevo.hasta;

            var i = 0;
            if ($scope.horario.length > 0)
            {
                for (i = 0; i < $scope.horario.length; i++)
                {
                    if (!$scope.horario[i].vieDesde)
                    {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                        break;
                    }
                }
                if (i == $scope.horario.length)
                {
                    $scope.horario.push($scope.nuevo);
                }
                else
                {
                    $scope.horario[i].vieDesde = $scope.nuevo.vieDesde;
                    $scope.horario[i].vieHasta = $scope.nuevo.vieHasta;
                }

            }
            else
            {
                $scope.horario.push($scope.nuevo);
            }
        }
        else if ($scope.diaNuevo.dia[0] == "S")
        {
            $scope.nuevo.sabDesde = $scope.diaNuevo.desde;
            $scope.nuevo.sabHasta = $scope.diaNuevo.hasta;

            var i = 0;
            if ($scope.horario.length > 0)
            {
                for (i = 0; i < $scope.horario.length; i++)
                {
                    if (!$scope.horario[i].sabDesde)
                    {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                        break;
                    }
                }
                if (i == $scope.horario.length)
                {
                    $scope.horario.push($scope.nuevo);
                }
                else
                {
                    $scope.horario[i].sabDesde = $scope.nuevo.sabDesde;
                    $scope.horario[i].sabHasta = $scope.nuevo.sabHasta;
                }

            }
            else
            {
                $scope.horario.push($scope.nuevo);
            }
        }
        else if ($scope.diaNuevo.dia[0] == "D")
        {
            $scope.nuevo.domDesde = $scope.diaNuevo.desde;
            $scope.nuevo.domHasta = $scope.diaNuevo.hasta;

            var i = 0;
            if ($scope.horario.length > 0)
            {
                for (i = 0; i < $scope.horario.length; i++)
                {
                    if (!$scope.horario[i].domDesde)
                    {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                        break;
                    }
                }
                if (i == $scope.horario.length)
                {
                    $scope.horario.push($scope.nuevo);
                }
                else
                {
                    $scope.horario[i].domDesde = $scope.nuevo.domDesde;
                    $scope.horario[i].domHasta = $scope.nuevo.domHasta;
                }

            }
            else
            {
                $scope.horario.push($scope.nuevo);
            }
        }

        $scope.nuevo = {
            id: "",
            des: "",
            lunDesde: "",
            lunHasta: "",
            marDesde: "",
            marHasta: "",
            mieDesde: "",
            mieHasta: "",
            jueDesde: "",
            jueHasta: "",
            vieDesde: "",
            vieHasta: "",
            sabDesde: "",
            sabHasta: "",
            domDesde: "",
            domHasta: "",
            edi: false};
//            $scope.nuevoDia.copia = JSON.parse(JSON.stringify($scope.nuevoDia));
//            $scope.nuevoDia.copia.fec=convertirFecha($scope.nuevoDia.fec);
//            var tempReg=$scope.nuevoDia.copia.reg;
//            $scope.nuevoDia.copia.reg=tempReg.id;
//           



    };
    $scope.editarFila = function (i, d) {
        if (d.edi) {
            $scope.horario[i] = d.copia;
//                $scope.horario[i]=JSON.parse(JSON.stringify($scope.horario[i]));
            d.edi = false;
        }
        else {
            d.copia = JSON.parse(JSON.stringify(d));
            //d.copia.
            if (d.copia.lunDesde != "")
            {
                d.copia.lunDesde = new Date(d.copia.lunDesde);
                d.copia.lunHasta = new Date(d.copia.lunHasta);
            }
            if (d.copia.marDesde != "")
            {
                d.copia.marDesde = new Date(d.copia.marDesde);
                d.copia.marHasta = new Date(d.copia.marHasta);
            }
            if (d.copia.mieDesde != "")
            {
                d.copia.mieDesde = new Date(d.copia.mieDesde);
                d.copia.mieHasta = new Date(d.copia.mieHasta);
            }
            if (d.copia.jueDesde != "")
            {
                d.copia.jueDesde = new Date(d.copia.jueDesde);
                d.copia.jueHasta = new Date(d.copia.jueHasta);
            }
            if (d.copia.vieDesde != "")
            {
                d.copia.vieDesde = new Date(d.copia.vieDesde);
                d.copia.vieHasta = new Date(d.copia.vieHasta);
            }
            if (d.copia.sabDesde != "")
            {
                d.copia.sabDesde = new Date(d.copia.sabDesde);
                d.copia.sabHasta = new Date(d.copia.sabHasta);
            }
            if (d.copia.domDesde != "")
            {
                d.copia.domDesde = new Date(d.copia.domDesde);
                d.copia.domHasta = new Date(d.copia.domHasta);
            }


            d.edi = true;
        }
    };
    $scope.eliminarFila = function (i, d) {
        //si estamso cancelando la edicion
        if (d.edi) {
            d.edi = false;
            //delete d.copia;
            d.copia = null;
        }
        //si queremos eliminar el elemento
        else {
            $scope.horario.splice(i, 1);
        }
    };
    $scope.guardarHorario = function () {
        if ($scope.horarioGeneral.des == "" || $scope.horarioGeneral.des == null)
        {
            modal.mensaje("CONFIRMACION", "Ingrese una Descripcion");
            return;
        }
        if ($scope.horario.length == 0 || $scope.horario == null)
        {
            modal.mensaje("CONFIRMACION", "Agregue el horario respectivo");
            return;
        }
        $scope.horarioGeneral.horario = JSON.parse(JSON.stringify($scope.horario));
        ;
        var request = crud.crearRequest('registroHorario', 1, 'nuevoHorario');
        request.setData({horario: $scope.horarioGeneral, organizacionID: $rootScope.usuMaster.organizacion.organizacionID});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.insertar("/controlPersonal", request, function (data) {
            if (data.responseSta) {


                $scope.horario = [];
                $scope.horarioGeneral = {
                    id: "",
                    des: "",
                    horario: []
                };
                modal.mensaje("CONFIRMACION", data.responseMsg);

            }

        }, function (data) {
            console.info(data);
        });
    };

//        $scope.eliminarFila = function (o) {
//            $scope.horario.splice(o.i,1)
//        };


    $scope.close = function () {
        close({
            flag: false
        }, 500); // close, but give 500ms for bootstrap to animate
    };
    $scope.cancel = function () {

        //  Manually hide the modal.
        $element.modal('hide');

        //  Now call close, returning control to the caller.
        close({
            flag: false
        }, 500); // close, but give 500ms for bootstrap to animate
    };
}]);

app.controller('editarHorarioCtrl', [
"$rootScope", '$scope', '$element', 'title', 'semana','horario','cabecera', 'close', 'crud', 'modal', 'ModalService', 'NgTableParams',
function ($rootScope, $scope, $element, title, semana,horario,cabecera, close, crud, modal, ModalService, NgTableParams) {

    $scope.title = title;
    $scope.cabecera=cabecera;
    $scope.horarioGeneral = {
        id: "",
        des: "",
        horario: []
    };

    $scope.diaSemana = semana;
    $scope.diaNuevo = {dia: "", desde: "", hasta: ""};
    $scope.nuevo = {
        id: "",
        des: "",
        lunDesde: "",
        lunHasta: "",
        marDesde: "",
        marHasta: "",
        mieDesde: "",
        mieHasta: "",
        jueDesde: "",
        jueHasta: "",
        vieDesde: "",
        vieHasta: "",
        sabDesde: "",
        sabHasta: "",
        domDesde: "",
        domHasta: "",
        edi: false
    };

    $scope.horario = horario;
    for (var i = 0; i < $scope.horario.length; i++)
    {
        if ($scope.horario[i].lunDesde)
        {
            var fec = new Date("1970-01-01T00:00:00.000Z");
            var a = $scope.horario[i].lunDesde.split(" ");

            var b = a[0].split(":");
            fec.setHours(b[0]);
            fec.setMinutes(b[1]);

            $scope.horario[i].lunDesde = fec.toISOString();
        }
        if ($scope.horario[i].lunHasta)
        {
            var fec = new Date("1970-01-01T00:00:00.000Z");
            var a = $scope.horario[i].lunHasta.split(" ");

            var b = a[0].split(":");
            fec.setHours(b[0]);
            fec.setMinutes(b[1]);

            $scope.horario[i].lunHasta = fec.toISOString();
        }
        if ($scope.horario[i].marDesde)
        {
            var fec = new Date("1970-01-01T00:00:00.000Z");
            var a = $scope.horario[i].marDesde.split(" ");

            var b = a[0].split(":");
            fec.setHours(b[0]);
            fec.setMinutes(b[1]);

            $scope.horario[i].marDesde = fec.toISOString();
        }
        if ($scope.horario[i].marHasta)
        {
            var fec = new Date("1970-01-01T00:00:00.000Z");
            var a = $scope.horario[i].marHasta.split(" ");

            var b = a[0].split(":");
            fec.setHours(b[0]);
            fec.setMinutes(b[1]);

            $scope.horario[i].marHasta = fec.toISOString();
        }
        if ($scope.horario[i].mieDesde)
        {
            var fec = new Date("1970-01-01T00:00:00.000Z");
            var a = $scope.horario[i].mieDesde.split(" ");

            var b = a[0].split(":");
            fec.setHours(b[0]);
            fec.setMinutes(b[1]);

            $scope.horario[i].mieDesde = fec.toISOString();
        }
        if ($scope.horario[i].mieHasta)
        {
            var fec = new Date("1970-01-01T00:00:00.000Z");
            var a = $scope.horario[i].mieHasta.split(" ");

            var b = a[0].split(":");
            fec.setHours(b[0]);
            fec.setMinutes(b[1]);

            $scope.horario[i].mieHasta = fec.toISOString();
        }
        if ($scope.horario[i].jueDesde)
        {
            var fec = new Date("1970-01-01T00:00:00.000Z");
            var a = $scope.horario[i].jueDesde.split(" ");

            var b = a[0].split(":");
            fec.setHours(b[0]);
            fec.setMinutes(b[1]);

            $scope.horario[i].jueDesde = fec.toISOString();
        }
        if ($scope.horario[i].jueHasta)
        {
            var fec = new Date("1970-01-01T00:00:00.000Z");
            var a = $scope.horario[i].jueHasta.split(" ");

            var b = a[0].split(":");
            fec.setHours(b[0]);
            fec.setMinutes(b[1]);

            $scope.horario[i].jueHasta = fec.toISOString();
        }
        if ($scope.horario[i].vieDesde)
        {
            var fec = new Date("1970-01-01T00:00:00.000Z");
            var a = $scope.horario[i].vieDesde.split(" ");

            var b = a[0].split(":");
            fec.setHours(b[0]);
            fec.setMinutes(b[1]);

            $scope.horario[i].vieDesde = fec.toISOString();
        }
        if ($scope.horario[i].vieHasta)
        {
            var fec = new Date("1970-01-01T00:00:00.000Z");
            var a = $scope.horario[i].vieHasta.split(" ");

            var b = a[0].split(":");
            fec.setHours(b[0]);
            fec.setMinutes(b[1]);

            $scope.horario[i].vieHasta = fec.toISOString();
        }
        if ($scope.horario[i].sabDesde)
        {
            var fec = new Date("1970-01-01T00:00:00.000Z");
            var a = $scope.horario[i].sabDesde.split(" ");

            var b = a[0].split(":");
            fec.setHours(b[0]);
            fec.setMinutes(b[1]);


            $scope.horario[i].sabDesde = fec.toISOString();
        }
        if ($scope.horario[i].sabHasta)
        {
            var fec = new Date("1970-01-01T00:00:00.000Z");
            var a = $scope.horario[i].sabHasta.split(" ");

            var b = a[0].split(":");
            fec.setHours(b[0]);
            fec.setMinutes(b[1]);

            $scope.horario[i].sabHasta = fec.toISOString();
        }
        if ($scope.horario[i].domDesde)
        {
            var fec = new Date("1970-01-01T00:00:00.000Z");
            var a = $scope.horario[i].domDesde.split(" ");

            var b = a[0].split(":");
            fec.setHours(b[0]);
            fec.setMinutes(b[1]);


            $scope.horario[i].domDesde = fec.toISOString();
        }
        if ($scope.horario[i].domHasta)
        {
            var fec = new Date("1970-01-01T00:00:00.000Z");
            var a = $scope.horario[i].domHasta.split(" ");

            var b = a[0].split(":");
            fec.setHours(b[0]);
            fec.setMinutes(b[1]);

            $scope.horario[i].domHasta = fec.toISOString();
        }
    }
    $scope.agregarDia = function () {

        if ($scope.diaNuevo.desde == null || $scope.diaNuevo.desde == "") {
            modal.mensaje("CONFIRMACION", "Seleccione una Hora de Entrada");
            return;
        }
        if ($scope.diaNuevo.hasta == null || $scope.diaNuevo.hasta == "") {
            modal.mensaje("CONFIRMACION", "Seleccione una Hora de Salida");
            return;
        }
        if ($scope.diaNuevo.dia == "") {
            modal.mensaje("CONFIRMACION", "Seleccione un Dia de la Semana");
            return;
        }

        if ($scope.diaNuevo.dia[0] == "L")
        {

            $scope.nuevo.lunDesde = $scope.diaNuevo.desde;
            $scope.nuevo.lunHasta = $scope.diaNuevo.hasta;

            var i = 0;

            if ($scope.horario.length > 0)
            {
                for (i = 0; i < $scope.horario.length; i++)
                {
                    if (!$scope.horario[i].lunDesde)
                    {
                        break;
                    }

                }
                if (i == $scope.horario.length)
                {
                    $scope.horario.push($scope.nuevo);
                }
                else
                {
                    $scope.horario[i].lunDesde = $scope.nuevo.lunDesde;
                    $scope.horario[i].lunHasta = $scope.nuevo.lunHasta;
                }

//                    $scope.horario.splice(i, 0, $scope.nuevo);
            }
            else
            {
                $scope.horario.push($scope.nuevo);
            }


        }
        else if ($scope.diaNuevo.dia[0] == "M")
        {

            $scope.nuevo.marDesde = $scope.diaNuevo.desde;
            $scope.nuevo.marHasta = $scope.diaNuevo.hasta;

            var i = 0;
            if ($scope.horario.length > 0)
            {
                for (i = 0; i < $scope.horario.length; i++)
                {
                    if (!$scope.horario[i].marDesde)
                    {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                        break;
                    }
                }
                if (i == $scope.horario.length)
                {
                    $scope.horario.push($scope.nuevo);
                }
                else
                {
                    $scope.horario[i].marDesde = $scope.nuevo.marDesde;
                    $scope.horario[i].marHasta = $scope.nuevo.marHasta;
                }

            }
            else
            {
                $scope.horario.push($scope.nuevo);
            }
        }
        else if ($scope.diaNuevo.dia[0] == "W")
        {
            $scope.nuevo.mieDesde = $scope.diaNuevo.desde;
            $scope.nuevo.mieHasta = $scope.diaNuevo.hasta;

            var i = 0;
            if ($scope.horario.length > 0)
            {
                for (i = 0; i < $scope.horario.length; i++)
                {
                    if (!$scope.horario[i].mieDesde)
                    {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                        break;
                    }
                }
                if (i == $scope.horario.length)
                {
                    $scope.horario.push($scope.nuevo);
                }
                else
                {
                    $scope.horario[i].mieDesde = $scope.nuevo.mieDesde;
                    $scope.horario[i].mieHasta = $scope.nuevo.mieHasta;
                }

            }
            else
            {
                $scope.horario.push($scope.nuevo);
            }
        }
        else if ($scope.diaNuevo.dia[0] == "J")
        {
            $scope.nuevo.jueDesde = $scope.diaNuevo.desde;
            $scope.nuevo.jueHasta = $scope.diaNuevo.hasta;

            var i = 0;
            if ($scope.horario.length > 0)
            {
                for (i = 0; i < $scope.horario.length; i++)
                {
                    if (!$scope.horario[i].jueDesde)
                    {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                        break;
                    }

                }
                if (i == $scope.horario.length)
                {
                    $scope.horario.push($scope.nuevo);
                }
                else
                {
                    $scope.horario[i].jueDesde = $scope.nuevo.jueDesde;
                    $scope.horario[i].jueHasta = $scope.nuevo.jueHasta;
                }

            }
            else
            {
                $scope.horario.push($scope.nuevo);
            }
        }
        else if ($scope.diaNuevo.dia[0] == "V")
        {
            $scope.nuevo.vieDesde = $scope.diaNuevo.desde;
            $scope.nuevo.vieHasta = $scope.diaNuevo.hasta;

            var i = 0;
            if ($scope.horario.length > 0)
            {
                for (i = 0; i < $scope.horario.length; i++)
                {
                    if (!$scope.horario[i].vieDesde)
                    {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                        break;
                    }
                }
                if (i == $scope.horario.length)
                {
                    $scope.horario.push($scope.nuevo);
                }
                else
                {
                    $scope.horario[i].vieDesde = $scope.nuevo.vieDesde;
                    $scope.horario[i].vieHasta = $scope.nuevo.vieHasta;
                }

            }
            else
            {
                $scope.horario.push($scope.nuevo);
            }
        }
        else if ($scope.diaNuevo.dia[0] == "S")
        {
            $scope.nuevo.sabDesde = $scope.diaNuevo.desde;
            $scope.nuevo.sabHasta = $scope.diaNuevo.hasta;

            var i = 0;
            if ($scope.horario.length > 0)
            {
                for (i = 0; i < $scope.horario.length; i++)
                {
                    if (!$scope.horario[i].sabDesde)
                    {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                        break;
                    }
                }
                if (i == $scope.horario.length)
                {
                    $scope.horario.push($scope.nuevo);
                }
                else
                {
                    $scope.horario[i].sabDesde = $scope.nuevo.sabDesde;
                    $scope.horario[i].sabHasta = $scope.nuevo.sabHasta;
                }

            }
            else
            {
                $scope.horario.push($scope.nuevo);
            }
        }
        else if ($scope.diaNuevo.dia[0] == "D")
        {
            $scope.nuevo.domDesde = $scope.diaNuevo.desde;
            $scope.nuevo.domHasta = $scope.diaNuevo.hasta;

            var i = 0;
            if ($scope.horario.length > 0)
            {
                for (i = 0; i < $scope.horario.length; i++)
                {
                    if (!$scope.horario[i].domDesde)
                    {
//                            $scope.horario.splice(i, 0, $scope.nuevo);
                        break;
                    }
                }
                if (i == $scope.horario.length)
                {
                    $scope.horario.push($scope.nuevo);
                }
                else
                {
                    $scope.horario[i].domDesde = $scope.nuevo.domDesde;
                    $scope.horario[i].domHasta = $scope.nuevo.domHasta;
                }

            }
            else
            {
                $scope.horario.push($scope.nuevo);
            }
        }

        $scope.nuevo = {
            id: "",
            des: "",
            lunDesde: "",
            lunHasta: "",
            marDesde: "",
            marHasta: "",
            mieDesde: "",
            mieHasta: "",
            jueDesde: "",
            jueHasta: "",
            vieDesde: "",
            vieHasta: "",
            sabDesde: "",
            sabHasta: "",
            domDesde: "",
            domHasta: "",
            edi: false};
//            $scope.nuevoDia.copia = JSON.parse(JSON.stringify($scope.nuevoDia));
//            $scope.nuevoDia.copia.fec=convertirFecha($scope.nuevoDia.fec);
//            var tempReg=$scope.nuevoDia.copia.reg;
//            $scope.nuevoDia.copia.reg=tempReg.id;
//           



    };
    $scope.editarFila = function (i, d) {
        if (d.edi) {
            $scope.horario[i] = d.copia;
//                $scope.horario[i]=JSON.parse(JSON.stringify($scope.horario[i]));
            d.edi = false;
        }
        else {
            d.copia = JSON.parse(JSON.stringify(d));
            //d.copia.
            if (d.copia.lunDesde != "")
            {
                d.copia.lunDesde = new Date(d.copia.lunDesde);
                d.copia.lunHasta = new Date(d.copia.lunHasta);
            }
            if (d.copia.marDesde != "")
            {
                d.copia.marDesde = new Date(d.copia.marDesde);
                d.copia.marHasta = new Date(d.copia.marHasta);
            }
            if (d.copia.mieDesde != "")
            {
                d.copia.mieDesde = new Date(d.copia.mieDesde);
                d.copia.mieHasta = new Date(d.copia.mieHasta);
            }
            if (d.copia.jueDesde != "")
            {
                d.copia.jueDesde = new Date(d.copia.jueDesde);
                d.copia.jueHasta = new Date(d.copia.jueHasta);
            }
            if (d.copia.vieDesde != "")
            {
                d.copia.vieDesde = new Date(d.copia.vieDesde);
                d.copia.vieHasta = new Date(d.copia.vieHasta);
            }
            if (d.copia.sabDesde != "")
            {
                d.copia.sabDesde = new Date(d.copia.sabDesde);
                d.copia.sabHasta = new Date(d.copia.sabHasta);
            }
            if (d.copia.domDesde != "")
            {
                d.copia.domDesde = new Date(d.copia.domDesde);
                d.copia.domHasta = new Date(d.copia.domHasta);
            }


            d.edi = true;
        }
    };
    $scope.eliminarFila = function (i, d) {
        //si estamso cancelando la edicion
        if (d.edi) {
            d.edi = false;
            //delete d.copia;
            d.copia = null;
        }
        //si queremos eliminar el elemento
        else {
            $scope.horario.splice(i, 1);
        }
    };


    $scope.editarHorario = function () {
        $scope.horarioGeneral.des=$scope.cabecera.des;
        if ($scope.horarioGeneral.des == "" || $scope.horarioGeneral.des == null)
        {
            modal.mensaje("CONFIRMACION", "Ingrese una Descripcion");
            return;
        }
        if ($scope.horario.length == 0 || $scope.horario == null)
        {
            modal.mensaje("CONFIRMACION", "Agregue el horario respectivo");
            return;
        }
        $scope.horarioGeneral.horario = JSON.parse(JSON.stringify($scope.horario));

        var request = crud.crearRequest('registroHorario', 1, 'editarHorario');
        request.setData({horario: $scope.horarioGeneral, organizacionID: $rootScope.usuMaster.organizacion.organizacionID,cabecera:$scope.cabecera.id});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.insertar("/controlPersonal", request, function (data) {
            if (data.responseSta) {

                $scope.horario = [];
                $scope.horarioGeneral = {
                    id: "",
                    des: "",
                    horario: []
                };
                modal.mensaje("CONFIRMACION", data.responseMsg);
                $element.modal('hide');
                close({
                    flag: true
                }, 500); // close, but give 500ms for bootstrap to animate
            }

        }, function (data) {
            console.info(data);
        });
    };

//        $scope.eliminarFila = function (o) {
//            $scope.horario.splice(o.i,1)
//        };


    $scope.close = function () {
        close({
            flag: false
        }, 500); // close, but give 500ms for bootstrap to animate
    };
    $scope.cancel = function () {

        //  Manually hide the modal.
        $element.modal('hide');

        //  Now call close, returning control to the caller.
        close({
            flag: false
        }, 500); // close, but give 500ms for bootstrap to animate
    };
}]);