app.controller("registroExpedienteCtrl",["$rootScope","$scope","crud","modal", function ($rootScope,$scope,crud,modal){
        
    $scope.tipoTramites = [];
    $scope.tipoPersona = true;
    $scope.organizacionId = $rootScope.usuMaster.organizacion.organizacionID;
    $scope.expediente = {modo:true};
    $scope.persona = {dni:"",existe:true};
    $scope.empresa = {ruc:"",existe:true};
    
    $scope.documentos = [];
    $scope.documento = {tipoDocumentoID:0,descripcion:"",nombreArchivo:"",archivo:{},edi:false};
    //$scope.ruta = {areaOrigenId:0,areaDestinoId:0,descripcion:"",edi:false};

    $scope.registrarExpediente = function(orgID,areID,resID){
        
        if(!areID ){
            modal.mensaje("ALERTA","esta funcion requiere que el usuario tenga asignado un area en la organizacion");
            return;
        }
        if($scope.tipoPersona)
        {
            if(!$scope.persona.personaID || $scope.persona.personaID == 0){
                modal.mensaje("CONFIRMACION","ingrese DNI del Tramitante");
                return;
            }
        }   
        else
        {
            if(!$scope.empresa.empresaID || $scope.empresa.empresaID==0){
                modal.mensaje("CONFIRMACION","ingrese RUC del tramitante");
                return;
            }
        }
        
        modal.mensajeConfirmacion2($scope,"Seguro que desea registrar el nuevo Tramite, al Solicitante",function(){
            if($scope.tipoPersona)
            {
                $scope.expediente.personaID = $scope.persona.personaID;
            }
            else
            {
                $scope.expediente.empresaID = $scope.empresa.empresaID;
            }
            
            $scope.expediente.organizacionID = orgID;
            $scope.expediente.areaInicialID = areID;
            $scope.expediente.responsableID = resID;
            
            $scope.expediente.documentos = $scope.documentos;
           

            var request = crud.crearRequest('expediente',1,'insertarExpediente');
            request.setData($scope.expediente);

            crud.insertar("/tramiteDocumentario",request,function(response){

                if(response.responseSta){
                    
                    modal.mensajeConfirmacion2($scope,"codigo de expediente",null,"<div align='center' style='font-size:20px'><b>"+response.data.codigo+"</b></div>");
                    //recuperamos las variables que nos envio el servidor
                    $scope.expediente.expedienteID = response.data.expedienteID;
                    $scope.expediente.codigo = response.data.codigo;

                    $scope.expediente = {modo:true};
                    $scope.persona = {dni:"",existe:true};
                    $scope.empresa = {ruc:"",existe:true};
                    $scope.documentos = [];
                    $scope.requisitos = [];
                    $scope.rutas = [];
                    $scope.documento = {tipoDocumentoID:0,descripcion:"",nombreArchivo:"",archivo:{},edi:false};
                }            
            },function(data){
                console.info(data);
            });
        
        },$scope.tipoPersona?"<div align='center' style='font-size:16px'><b>"+$scope.persona.nombre+" "+$scope.persona.materno+" "+$scope.persona.paterno+"</b></div><br><div align='center' style='font-size:16px'><b>"+$scope.persona.dni+"</b></div><br>":"<div align='center' style='font-size:16px'><b>"+$scope.empresa.razonSocial+"</b></div><br><div align='center' style='font-size:16px'><b>"+$scope.empresa.ruc+"</b></div><br>");
    };
    //alert($rootScope.usuMaster.usuario.usuarioID);
    $scope.buscarPersona = function(){
        //preparamos un objeto request
        var request = crud.crearRequest('usuarioSistema',1,'buscarPersona');
        
//        if(!$scope.tipoPersona)
//            request.setData({dni:0,usuarioID:$scope.persona.dni});
//        else
            request.setData({dni:$scope.persona.dni});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las usuarios de exito y error
        crud.listar("/configuracionInicial",request,function(data){
            modal.mensaje("CONFIRMACION",data.responseMsg);
            if(data.responseSta)
                $scope.persona = data.data.persona;
            else
                $scope.persona.existe = false;
        },function(data){
            console.info(data);
        });
    };
    $scope.registrarPersona = function(){
        
        var request = crud.crearRequest('usuarioSistema',1,'insertarPersona');
        
//        if(!$scope.tipoPersona){
//            $scope.persona.usuarioID = $scope.persona.dni;
//            $scope.persona.dni = 0;
//        }
        
        request.setData($scope.persona);
        
        crud.insertar("/configuracionInicial",request,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                $scope.persona.personaID = response.data.personaID;
                $scope.persona.existe = true;
                $('#modalNuevo').modal('hide');
            }            
        },function(data){
            console.info(data);
        });
        
    };
    
    $scope.buscarEmpresa = function(){
        //preparamos un objeto request
        var request = crud.crearRequest('empresas',1,'buscarEmpresa');
        request.setData({ruc:$scope.empresa.ruc});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las usuarios de exito y error
        crud.listar("/configuracionInicial",request,function(data){
            modal.mensaje("CONFIRMACION",data.responseMsg);
            if(data.responseSta){
                $scope.empresa = data.data;
            }
            else{
                $scope.empresa.existe = false;
                
            }
        },function(data){
            console.info(data);
        });
    };
    
    $scope.agregarEmpresa = function(){
        
        
        var request = crud.crearRequest('empresas',1,'insertarEmpresa');
        request.setData({empresa:$scope.empresa});
        
        crud.insertar("/configuracionInicial",request,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                //recuperamos las variables que nos envio el servidor
                $scope.empresa.empresaID = response.data.empresaID;   
                $scope.empresa.existe=true;
                $('#modalNuevoEmpresa').modal('hide');
            }            
        },function(data){
            console.info(data);
        });
        
    };
    $scope.cambiarDni = function(){
        $scope.persona = {dni:"",existe:true};
    };
    $scope.cambiarRuc = function(){
        $scope.empresa = {ruc:"",existe:true};
    };
    $scope.reiniciarDatos = function(){
        var p = $scope.persona;
        p.existe = true;
        p.nombre=p.paterno=p.materno=p.nacimiento=p.email=p.numero1=p.numero2="";
        
        var q = $scope.empresa;
        q.existe = true;
        q.razonSocial=q.pagWeb=q.telefono="";
    };
    $scope.elegirTipoTramite = function(tipTraID){
        var tipoTramite = null;
        for(var i=0;i<$scope.tipoTramites.length;i++ )
            if($scope.tipoTramites[i].tipoTramiteID == tipTraID){
                tipoTramite = $scope.tipoTramites[i];
                break;
            }
        if(tipoTramite){
            var request = crud.crearRequest('tipoTramite',1,'listarRequisito');
            request.setData({tipoTramiteID:tipoTramite.tipoTramiteID,tupa:tipoTramite.tupa});
            crud.listar("/tramiteDocumentario",request,function(data){
                $scope.requisitos = data.data.requisitos;
                $scope.rutas = data.data.rutas;
                $('#modalEditarTramite').modal('show');
            },function(data){
                console.info(data);
            });
        }
        else{
            $scope.requisitos = [];
            $scope.rutas = [];
        }
    };
    $scope.agregarDocumento = function(){
        
        if($scope.documento.tipoDocumentoID == 0 ){
            modal.mensaje("CONFIRMACION","ingrese un tipo de documento");
            return;
        }
        if($scope.documento.descripcion =="" ){
            modal.mensaje("CONFIRMACION","ingrese descripcion del documento");
            return;
        }
        var cadena1 = $scope.documento.nombreArchivo;
        var cadena2 = cadena1.substring(cadena1.length - 4, cadena1.length);
       if($scope.documento.nombreArchivo != ""){
            if (cadena2 != ".pdf") {
                modal.mensaje("CONFIRMACION","Selecione solo archivos PDF");
                return;
            }
        }
        
        $scope.documento.nombre = buscarContenido($scope.tipoDocumentos,"tipoDocumentoID","nombre",$scope.documento.tipoDocumentoID);
       // console.log($scope.documento);
        $scope.documentos.push($scope.documento);
        $scope.documento= {tipoDocumentoID:0,descripcion:"",nombreArchivo:"",archivo:{},edi:false};
    };
    /*$scope.agregarRuta = function(){
        if($scope.ruta.origen == 0 ){
            modal.mensaje("CONFIRMACION","ingrese un tipo de documento");
            return;
        }
        if($scope.documento.destino == 0 ){
            modal.mensaje("CONFIRMACION","ingrese descripcion del documento");
            return;
        }
        if($scope.ruta.descripcion == "" ){
            modal.mensaje("CONFIRMACION","ingrese descripcion del documento");
            return;
        }
        $scope.documento.nombre = buscarContenido($scope.tipoDocumentos,"tipoDocumentoID","nombre",$scope.documento.tipoDocumentoID);
        $scope.documentos.push($scope.documento);
        $scope.documento= {tipoDocumentoID:0,descripcion:"",nombreArchivo:"",archivo:{},edi:false};
    };*/
    $scope.editarDocumento = function(i,d){
        //si estamso editando
        if(d.edi){
            d.copia.nombre = buscarContenido($scope.tipoDocumentos,"tipoDocumentoID","nombre",d.tipoDocumentoID);
            $scope.documentos[i] = d.copia;
            
        }
        //si queremos editar
        else{
            d.copia = JSON.parse(JSON.stringify(d));
            d.edi =true;
        }
    };
    $scope.verDocumento = function(d){
        verDocumento( d.archivo.bbuildDataURL());
    };
    $scope.eliminarDocumento = function(i,d){
        //si estamso cancelando la edicion
        if(d.edi){
            d.edi = false;            
            //delete d.copia;
            d.copia = null;
        }
        //si queremos eliminar el elemento
        else{
            $scope.documentos.splice(i,1);
        }
    };

    listarTiposTramites();
    function listarTiposTramites(orgId){
        //preparamos un objeto request
        var request = crud.crearRequest('tipoTramite',1,'listarTipoTramite');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/tramiteDocumentario",request,function(data){
            $scope.tipoTramites = data.data;
            
        },function(data){
            console.info(data);
        });
    };
    listarDatos();
    function listarDatos(){
        //preparamos un objeto request
        var request = crud.crearRequest('tramite_datos',1,'listarEstadoExpedientes');
        crud.listar("/tramiteDocumentario",request,function(data){
            $scope.estados = data.data;
        },function(data){
            console.info(data);
        });
        request = crud.crearRequest('tramite_datos',1,'listarPrioridadExpedientes');
        crud.listar("/tramiteDocumentario",request,function(data){
            $scope.prioridades = data.data;
        },function(data){
            console.info(data);
        });
        
        request = crud.crearRequest('tramite_datos',1,'listarTipoDocumentos');
        crud.listar("/tramiteDocumentario",request,function(data){
            $scope.tipoDocumentos = data.data;
        },function(data){
            console.info(data);
        });
        /*
        var request = crud.crearRequest('area',1,'listarAreasPorOrganizacion');
        alert("organizacion"+$scope.organizacionId);
        request.setData({organizacionID:$scope.organizacionId});

        crud.listar("/configuracionInicial",request,function(data){
            $scope.tipoAreas = data.data;
        },function(data){
            console.info(data);
        });*/
    };
   
    
	
}]);

