app.controller("estadisticaTDCtrl",["$scope","NgTableParams","crud","modal", function ($scope,NgTableParams,crud,modal){
        
    var params = {count: 10};
    var setting = { counts: []};
    $scope.miTabla = new NgTableParams(params, setting);
    $scope.tabla2 = new NgTableParams(params, setting);
    
    $scope.tipoTramites = [];
    $scope.prioridades = [];
    
    var inicio = new Date();
    inicio.setMonth(0);
    inicio.setDate(1);
    $scope.busqueda = { desde: inicio , hasta:new Date("dd/mm/aaaa")};
    //$scope.Institucion = {
    var params = {count: 10};
    var setting = { counts: []};
    $scope.tabla = new NgTableParams(params, setting);
    
    $scope.generarEstadistica= function(){        
        //preparamos un objeto request
        var request = crud.crearRequest('expediente',1,'estadisticaExpedienteArea');
        request.setData({organizacionID:$scope.busqueda.orgID,desde:convertirFecha($scope.busqueda.desde),hasta:convertirFecha($scope.busqueda.hasta)});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/tramiteDocumentario",request,function(response){            
            if(!response.responseSta){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                return;
            }
            if(response.data){
              
                var data = [{nombre:"rechazados",data:response.data.devueltos},
                            {nombre:"recibidos",data:response.data.expedientes},
                            {nombre:"derivados",data:response.data.derivados},
                            {nombre:"finalizados",data:response.data.finalizados}];
             console.log( response.data.labels);
                crearGraficoBarras("graExpedienteArea",response.data.labels,data);
                
            }
        },function(data){
            console.info(data);
        });
    };
    $scope.generarEstadistica2= function(){        
        //preparamos un objeto request
        var request = crud.crearRequest('expediente',1,'estadisticaTipoTramite');
        request.setData({organizacionID:$scope.busqueda.orgID,desde:convertirFecha($scope.busqueda.desde),hasta:convertirFecha($scope.busqueda.hasta)});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/tramiteDocumentario",request,function(response){
            if(!response.responseSta){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                return;
            }
            if(response.data){
                var data = [{nombre:"expedientes",data:response.data.expedientes},
                            {nombre:"finalizados",data:response.data.finalizados},
                            {nombre:"entregados",data:response.data.entregados}];
                
                crearGraficoBarras("graTipoTramite",response.data.labels,data);
                
            }
        },function(data){
            console.info(data);
        });
    };
    
    $scope.generarEstadistica3= function(){        
        //preparamos un objeto request
        var request = crud.crearRequest('expediente',1,'estadisticaResumen');
        request.setData({desde:convertirFecha($scope.busqueda.desde),hasta:convertirFecha($scope.busqueda.hasta)});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/tramiteDocumentario",request,function(response){
            if(!response.responseSta){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                return;
            }
            if(response.data){
                
                
                setting.dataset = response.data;
                iniciarPosiciones(setting.dataset);
                
                $scope.tabla.settings(setting);
                
            }
        },function(data){
            console.info(data);
        });
    };
    $scope.generarEstadistica4= function(){
        //preparamos un objeto request
        var request = crud.crearRequest('expediente',1,'estadisticaExpedienteTrabajador');
        request.setData({areaID:$scope.busqueda.areID,desde:convertirFecha($scope.busqueda.desde),hasta:convertirFecha($scope.busqueda.hasta)});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/tramiteDocumentario",request,function(response){
            if(!response.responseSta){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                return;
            }
            if(response.data){
                var data = [{nombre:"recibidos",data:response.data.expedientes},
                            {nombre:"derivados",data:response.data.derivados},
                            {nombre:"rechazados",data:response.data.devueltos},
                            {nombre:"finalizados",data:response.data.finalizados}];
                console.log(response.data);
               // setting.dataset = response.data;
               $scope.reporteTrabajador ();
                //   $scope.tabla.settings(setting); 
                
                crearGraficoBarras("graExpedienteTrab",response.data.labels,data);
                
            }
        },function(data){
            console.info(data);
        });
    };
    
    $scope.imprimirGrafico = function(){
        var grafico = new MyFile("Reporte Tramites por Area");
        grafico.parseDataURL( document.getElementById("graExpedienteArea").toDataURL("image/png") );
        
        var request = crud.crearRequest('expediente',1,'reporte');
        request.setData(grafico);
        crud.insertar("/tramiteDocumentario",request,function(response){
            if(response.responseSta){                
                verDocumento( response.data.reporte );
            }            
        },function(data){
            console.info(data);
        });       
    };
    $scope.imprimirGrafico2 = function(){
        var grafico = new MyFile("Tipo de Tramite");
        grafico.parseDataURL( document.getElementById("graTipoTramite").toDataURL("image/png") );
        
        var request = crud.crearRequest('expediente',1,'reporte');
        request.setData(grafico);
        crud.insertar("/tramiteDocumentario",request,function(response){
            if(response.responseSta){                
                verDocumento( response.data.reporte );
            }            
        },function(data){
            console.info(data);
        });       
    };
    
    $scope.imprimirGrafico3 = function(){
        var request = crud.crearRequest('expediente',1,'reporte');
        request.setData({resumen:setting.dataset});
        crud.insertar("/tramiteDocumentario",request,function(response){
            if(response.responseSta){                
                verDocumento( response.data.reporte );
            }            
        },function(data){
            console.info(data);
        });       
    };
    $scope.reporteRuta = function(){
          //preparamos un objeto request
          alert("codgio ingresado "+$scope.busqueda.codigo);
        var request = crud.crearRequest('expediente',1,'reporteEstadisticaExpedienteRuta');
        request.setData({expedienteID:$scope.busqueda.codigo});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/tramiteDocumentario",request,function(response){            
            if(!response.responseSta){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                return;
            }
            if(response.data){
                
                console.log(response.data);
                
                setting.dataset = response.data;
                  alert("OKA");
                $scope.tabla.settings(setting); 
              
            }
        },function(data){
            console.info(data);
        });
    };
    $scope.reporteArea = function(){
          //preparamos un objeto request
        var request = crud.crearRequest('expediente',1,'reporteEstadisticaExpedienteArea');
        request.setData({organizacionID:$scope.busqueda.orgID,desde:convertirFecha($scope.busqueda.desde),hasta:convertirFecha($scope.busqueda.hasta)});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/tramiteDocumentario",request,function(response){            
            if(!response.responseSta){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                return;
            }
            if(response.data){
                
                console.log(response.data);
                
                setting.dataset = response.data;
                
                $scope.tabla.settings(setting); 
            }
        },function(data){
            console.info(data);
        });
    };
    $scope.reporteTipo = function(){
          //preparamos un objeto request
        var request = crud.crearRequest('expediente',1,'reporteEstadisticaTipoTramite');
        request.setData({organizacionID:$scope.busqueda.orgID,desde:convertirFecha($scope.busqueda.desde),hasta:convertirFecha($scope.busqueda.hasta)});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/tramiteDocumentario",request,function(response){            
            if(!response.responseSta){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                return;
            }
            if(response.data){
                
                //console.log(response.data);
                
                setting.dataset = response.data;
                
                $scope.tabla.settings(setting); 
            }
        },function(data){
            console.info(data);
        });
    };
    $scope.reporteTrabajador = function(){
          //preparamos un objeto request
        var request = crud.crearRequest('expediente',1,'reporteEstadisticaExpedienteTrabajadorPorArea');
        request.setData({areaID:$scope.busqueda.areID,desde:convertirFecha($scope.busqueda.desde),hasta:convertirFecha($scope.busqueda.hasta)});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/tramiteDocumentario",request,function(response){            
            if(!response.responseSta){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                return;
            }
            if(response.data){
                
                console.log(response.data);
                
                setting.dataset = response.data;
                
                $scope.tabla.settings(setting); 
            }
        },function(data){
            console.info(data);
        });
    };    
    $scope.imprimirGrafico4 = function(){
        var grafico = new MyFile("Reporte Tramites por Trabajador");
        grafico.parseDataURL( document.getElementById("graExpedienteTrab").toDataURL("image/png") );
        
        //var request = crud.crearRequest('expediente',1,'reporte');
        var request = crud.crearRequest('expediente',1,'reporteGraficoTablaTrabajador');
        //request.setData(grafico);
        //console.log(setting.dataset);
        //return;
        request.setData({resumen:setting.dataset, desde:$scope.busqueda.desde, grafico:grafico});
        crud.insertar("/tramiteDocumentario",request,function(response){
            if(response.responseSta){                
                verDocumento( response.data.reporte );
            }            
        },function(data){
            console.info(data);
        });       
    };
    $scope.imprimirReporteArea =function(){
        var request = crud.crearRequest('expediente',1,'reporteArea');
        request.setData({resumen:setting.dataset, desde:$scope.busqueda.desde});
        //request.setData({resumen:setting.dataset, desde:$scope.busqueda.desde, hasta:$scope.busqueda.hasta});
        //console.log(request);
        //return;
        crud.insertar("/tramiteDocumentario",request,function(response){
            if(response.responseSta){                
                verDocumento( response.data.reporte );
            }            
        },function(data){
            console.info(data);
        });     
    };
    $scope.imprimirReporteTipoTramite =function(){
        var request = crud.crearRequest('expediente',1,'reporteTipoTramite');
        request.setData({resumen:setting.dataset, desde:$scope.busqueda.desde});
        //request.setData({resumen:setting.dataset, desde:$scope.busqueda.desde, hasta:$scope.busqueda.hasta});
        //console.log(request);
        //return;
        crud.insertar("/tramiteDocumentario",request,function(response){
            if(response.responseSta){                
                verDocumento( response.data.reporte );
            }            
        },function(data){
            console.info(data);
        });     
    };
    $scope.imprimirReporteTrabajador =function(){
       var request = crud.crearRequest('expediente',1,'reporteTrabajor');
        request.setData({resumen:setting.dataset, desde:$scope.busqueda.desde});
        //request.setData({resumen:setting.dataset, desde:$scope.busqueda.desde, hasta:$scope.busqueda.hasta});
        //console.log(request);
        //return;
        crud.insertar("/tramiteDocumentario",request,function(response){
            if(response.responseSta){                
                verDocumento( response.data.reporte );
            }            
        },function(data){
            console.info(data);
        });     
    };
    $scope.imprimirReporteRuta =function(){
       var request = crud.crearRequest('expediente',1,'reporteRuta');
        request.setData({resumen:setting.dataset, desde:$scope.busqueda.desde});
        //request.setData({resumen:setting.dataset, desde:$scope.busqueda.desde, hasta:$scope.busqueda.hasta});
        //console.log(request);
        //return;
        crud.insertar("/tramiteDocumentario",request,function(response){
            if(response.responseSta){                
                verDocumento( response.data.reporte );
            }            
        },function(data){
            console.info(data);
        });     
    };
    
    
    listarDatos();
    function listarDatos(){       
        
        request = crud.crearRequest('organizacion',1,'listarOrganizaciones');
        crud.listar("/configuracionInicial",request,function(data){
            $scope.organizaciones = data.data;
        },function(data){
            console.info(data);
        });
    };
    $scope.listarAreas = function(organizacionID){
        
        if(!organizacionID)
            return;
        //preparamos un objeto request
        var request = crud.crearRequest('area',1,'listarAreasPorOrganizacion');
        request.setData({organizacionID:organizacionID});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las areas de exito y error
        crud.listar("/configuracionInicial",request,function(data){
            $scope.areas = data.data;
        },function(data){
            console.info(data);
        });
    };
	
}]);
