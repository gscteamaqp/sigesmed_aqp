app.controller("expedientesCtrl",["$scope","NgTableParams","crud","modal", function ($scope,NgTableParams,crud,modal){
        
    var params = {count: 10};
    var setting = { counts: []};
    var setting2 = { counts: []};
    $scope.miTabla = new NgTableParams(params, setting);
    $scope.miTabla2 = new NgTableParams(params, setting2);
    
    $scope.tipoTramites = [];
    $scope.prioridades = [];
    
    $scope.desde = new Date();
    $scope.desde.setDate(1);
    $scope.hasta = new Date("dd/mm/aaaa");
    
    $scope.desde2 = new Date();
    $scope.desde2.setDate(1);
    $scope.hasta2 = new Date("dd/mm/aaaa");
    
    $scope.buscarExpedientes = function(orgID,desde,hasta){
        //preparamos un objeto request
        var request = crud.crearRequest('expediente',1,'listarExpediente');
        request.setData({organizacionID:orgID,desde:convertirFecha(desde),hasta:convertirFecha(hasta)});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/tramiteDocumentario",request,function(data){
            if(data.data){
                setting.dataset = data.data;
                iniciarPosiciones(setting.dataset);
                $scope.miTabla.settings(setting);
            }
        },function(data){
            console.info(data);
        });
    };
    
    $scope.buscarExpedientesFinzalidos = function(orgID,desde,hasta){
        //preparamos un objeto request
        var request = crud.crearRequest('expediente',1,'listarExpediente');
        request.setData({organizacionID:orgID,desde:convertirFecha(desde),hasta:convertirFecha(hasta),finalizados:true});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/tramiteDocumentario",request,function(data){
            if(data.data){
                setting2.dataset = data.data;
                iniciarPosiciones(setting2.dataset);
                $scope.miTabla2.settings(setting2);
            }
        },function(data){
            console.info(data);
        });
    };
    
    $scope.verDetalle = function(e){
        $scope.expedienteSel = e;
        var request = crud.crearRequest('expediente',1,'verHistorial');
        request.setData({expedienteID:e.expedienteID});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las usuarios de exito y error
        crud.listar("/tramiteDocumentario",request,function(data){
            $scope.expedienteSel.historial = data.data.historial;
            //$scope.expedienteSel.rutas = data.data.rutas;
            $('#modalVer').modal('show');
        },function(data){
            console.info(data);
        });
    };
    
    $scope.verDetalleConDocumentos = function(e){
        $scope.expedienteSel = e;
        var request = crud.crearRequest('expediente',1,'verHistorialYDocumentos');
        request.setData({expedienteID:e.expedienteID});
        crud.listar("/tramiteDocumentario",request,function(data){
            $scope.expedienteSel.documentos = data.data.documentos;
            $scope.expedienteSel.historial = data.data.historial;
            $('#modalVer').modal('show');
        },function(data){
            console.info(data);
        });
    };
    
    $scope.entregarExpediente = function(expediente){
        
        modal.mensajeConfirmacion2($scope,"seguro que da por entregado el expediente al solicitante",function(){
            var request = crud.crearRequest('expediente',1,'entregarExpediente');
            request.setData({expedienteID:expediente.expedienteID});
            crud.actualizar("/tramiteDocumentario",request,function(response){
                if(response.responseSta){
                    eliminarElemento(setting2.dataset,expediente.i);
                    $scope.miTabla2.reload();
                    modal.mensaje("CONFIRMACION","el tramite se entrego satisfactoriamente");
                }
                
            },function(data){
                console.info(data);
            });
            
        },"<div align='center' style='font-size:20px'><b>"+expediente.codigo+"</b></div><br><div align='center' style='font-size:16px'><b>"+expediente.persona+"</b></div><br><div align='center' style='font-size:16px'><b>DNI: "+expediente.DNI+"</b></div>");
        
    };
    
    listarDatos();
    function listarDatos(){
        //preparamos un objeto request
        var request = crud.crearRequest('tramite_datos',1,'listarEstadoExpedientes');
        crud.listar("/tramiteDocumentario",request,function(data){
            $scope.estados = data.data;
        },function(data){
            console.info(data);
        });
        request = crud.crearRequest('tramite_datos',1,'listarPrioridadExpedientes');
        crud.listar("/tramiteDocumentario",request,function(data){
            data.data.forEach(function(item){
                var o = item;
                o.id = item.prioridadExpedienteID;
                o.title = item.nombre;
                $scope.prioridades.push(o);                
            });
        },function(data){
            console.info(data);
        });        
        request = crud.crearRequest('tipoTramite',1,'listarTipoTramite');
        crud.listar("/tramiteDocumentario",request,function(data){
            data.data.forEach(function(item){
                var o = item;
                o.id = item.tipoTramiteID;
                o.title = item.nombre;
                $scope.tipoTramites.push(o);                
            });
        },function(data){
            console.info(data);
        });
    };
	
}]);
