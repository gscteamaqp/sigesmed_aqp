+app.config(["ngTableFilterConfigProvider",function(ngTableFilterConfigProvider){
    ngTableFilterConfigProvider.setConfig({
      aliasUrls: {"checkbox": "app/ng-table/filters/checkbox.html"}
    });
}]);

app.controller("bandejaExpedientesCtrl",["$rootScope","$scope","NgTableParams","crud","modal", function ($rootScope,$scope,NgTableParams,crud,modal){
        
    var params = {count: 40};
    var settingN = { counts: []};    
    $scope.miTablaN = new NgTableParams(params, settingN);
    $scope.usuarioDerivador = $rootScope.usuMaster.usuario.ID;
    $scope.campoContrasenia ;
    var settingR = { counts: []};
    $scope.miTablaR = new NgTableParams(params, settingR);
    
    var settingD = { counts: []};
    $scope.miTablaD = new NgTableParams(params, settingD);
    
    var settingF = { counts: []};
    $scope.miTablaF = new NgTableParams(params, settingF);
    
    $scope.tipoTramites = [];
    $scope.prioridades = [];
    $scope.estados = [];
    $scope.areas = [];
    $scope.documentos = [];
    $scope.fechaFinal = new Date();
    $scope.hoy = convertirFecha2( $scope.fechaFinal);
    
   //  alert( "nom certificado "+$rootScope.usuMaster.usuario.);
    /*
    $scope.desde = new Date();
    $scope.hasta = new Date("dd/mm/aaaa");
    
    $scope.desde2 = new Date();
    $scope.hasta2 = new Date("dd/mm/aaaa");
    
    $scope.desde3 = new Date();
    $scope.hasta3 = new Date("dd/mm/aaaa");*/
    //alert($rootScope.usuMaster.usuario.usuarioID);
    //alert($rootScope.usuMaster.usuario.nombre);

    $scope.buscarHistorialNuevos = function(orgID,areID){
        if(!areID ){
            modal.mensaje("ALERTA","esta funcion requiere que el usuario tenga asignado un area en la organizacion");
            return;
        }
        var request = crud.crearRequest('expediente',1,'listarHistorial');        
        request.setData({organizacionID:orgID,areaID:areID});
        //alert("area id "+ areID);
        crud.listar("/tramiteDocumentario",request,function(response){
            if(response.data){                
                response.data.forEach(function(item){
                    for(var i=0;$scope.tipoTramites.length > i;i++)
                        if( item.tipoTramiteID == $scope.tipoTramites[i].tipoTramiteID ){
                            item.nombreTramite = $scope.tipoTramites[i].nombre;
                            item.duracion = $scope.tipoTramites[i].duracion;
                            item.tupa = $scope.tipoTramites[i].tupa;
                            break;
                        }
                });             
                settingN.dataset = response.data;                
                iniciarPosiciones(settingN.dataset);
                $scope.miTablaN.settings(settingN);
            }
        },function(data){
            console.info(data);
        });
    };
    $scope.buscarHistorialRecibidos = function(orgID,areID){        
        if(!areID ){
            modal.mensaje("ALERTA","esta funcion requiere que el usuario tenga asignado un area en la organizacion");
            return;
        }
        var request = crud.crearRequest('expediente',1,'listarHistorial');        
        request.setData({organizacionID:orgID,areaID:areID,estado:"recibidos"});
        crud.listar("/tramiteDocumentario",request,function(response){
            if(response.data){
                response.data.forEach(function(item){
                    for(var i=0;$scope.tipoTramites.length > i;i++)
                        if( item.tipoTramiteID == $scope.tipoTramites[i].tipoTramiteID ){
                            item.nombreTramite = $scope.tipoTramites[i].nombre;
                            item.duracion = $scope.tipoTramites[i].duracion;
                            item.tupa = $scope.tipoTramites[i].tupa;
                            break;
                        }
                });
                settingR.dataset = response.data;
                iniciarPosiciones(settingR.dataset);
                $scope.miTablaR.settings(settingR);
            }
        },function(data){
            console.info(data);
        });
    };
    $scope.buscarHistorialDerivados = function(orgID,areID){
        if(!areID ){
            modal.mensaje("ALERTA","esta funcion requiere que el usuario tenga asignado un area en la organizacion");
            return;
        }
        var request = crud.crearRequest('expediente',1,'listarHistorial');
        request.setData({organizacionID:orgID,areaID:areID,estado:"derivados"});
        crud.listar("/tramiteDocumentario",request,function(response){
            if(response.data){
                response.data.forEach(function(item){
                    for(var i=0;$scope.tipoTramites.length > i;i++)
                        if( item.tipoTramiteID == $scope.tipoTramites[i].tipoTramiteID ){
                            item.nombreTramite = $scope.tipoTramites[i].nombre;
                            item.duracion = $scope.tipoTramites[i].duracion;
                            item.tupa = $scope.tipoTramites[i].tupa;
                            break;
                        }
                });
                settingD.dataset = response.data;
                iniciarPosiciones(settingD.dataset);
             //   $scope.registros=settingD.dataset;//delete
                $scope.miTablaD.settings(settingD);
            }
        },function(data){
            console.info(data);
        });
    };
    $scope.buscarHistorialFinalizados = function(orgID,areID){        
        if(!areID ){
            modal.mensaje("ALERTA","esta funcion requiere que el usuario tenga asignado un area en la organizacion");
            return;
        }
        var request = crud.crearRequest('expediente',1,'listarHistorial');        
        request.setData({organizacionID:orgID,areaID:areID,estado:"finalizados"});
        crud.listar("/tramiteDocumentario",request,function(response){
            if(response.data){
                response.data.forEach(function(item){
                    for(var i=0;$scope.tipoTramites.length > i;i++)
                        if( item.tipoTramiteID == $scope.tipoTramites[i].tipoTramiteID ){
                            item.nombreTramite = $scope.tipoTramites[i].nombre;
                            item.duracion = $scope.tipoTramites[i].duracion;
                            item.tupa = $scope.tipoTramites[i].tupa;
                            break;
                        }
                });
                settingF.dataset = response.data;
                iniciarPosiciones(settingF.dataset);
                $scope.miTablaF.settings(settingF);
            }
        },function(data){
            console.info(data);
        });
    };
    
    $scope.prepararRecepcion = function(){
        $scope.expedientesSel = [];
        settingN.dataset.forEach(function(item){
            if( item.selec )
                $scope.expedientesSel.push(item);
        });
        if($scope.expedientesSel.length > 0)        
            $('#modalRecepcionar').modal('show');
        else
            modal.mensaje("ALERTA","seleccione elementos para esta operacion");
    };
    
    $scope.recepcionar = function(resID){
        
        $scope.expedientesSel.forEach(function(item){
            item.responsableID = resID;
        });
        
        var request = crud.crearRequest('expediente',1,'recepcionarExpediente');
        request.setData($scope.expedientesSel);
        
        
        
        crud.actualizar("/tramiteDocumentario",request,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                var data = response.data;
                for(var i=0;i<data.length;i++){                    
                    eliminarElemento(settingN.dataset,$scope.expedientesSel[i].i);
                    $scope.expedientesSel[i].selec=false;
                    $scope.expedientesSel[i].fechaRecepcion=data[i].fechaRecepcion;
                    $scope.expedientesSel[i].estadoID=data[i].estadoID;
                    $scope.expedientesSel[i].estado=data[i].estado;
                    insertarElemento(settingR.dataset,$scope.expedientesSel[i]);
                }
                $scope.miTablaN.reload();
                $scope.miTablaR.reload();
                //$scope.expedientesSel = [];
                $('#modalRecepcionar').modal('hide');
            }            
        },function(data){
            console.info(data);
        });
    };
    
    $scope.prepararDerivar = function(o){
        
        verificarCertificado();
       
        
        
        $scope.observacion = "";
        
        $scope.seDeriva = false;
        
        var request = crud.crearRequest('tipoTramite',1,'listarRequisito');
        request.setData({tipoTramiteID:o.tipoTramiteID,tupa:o.tupa});
        crud.listar("/tramiteDocumentario",request,function(data){
            $scope.rutas = data.data.rutas;
            
            var l = $scope.rutas.length-1;
            
            var destino = buscarContenido( $scope.rutas,"areaOriID","areaDesID", o.areaID );
            
            if(destino){
                $scope.areaID = ""+destino;
                $scope.verUsuarios($scope.areaID);
                $scope.seDeriva = true;
            }
            else
                $scope.seDeriva = false;
            
            $scope.rutas.push( { areaOriID: $scope.rutas[l].areaDesID, areaOri:{areaOriID: $scope.rutas[l].areaDesID ,nombre: $scope.rutas[l].areaDes.nombre} });
        },function(data){
            console.info(data);
        });
        
        $scope.tupa = o.tupa;
        
        $scope.documentos = [];        
        $scope.expedientesSel = [];
        $scope.expedientesSel.push(o);
        if($scope.expedientesSel.length > 0)
            $('#modalDerivar').modal('show');
        else
            modal.mensaje("ALERTA","seleccione elementos para esta operacion");
    };
    
    $scope.verUsuarios = function(areaID){
        $scope.usuarios = [];
        //preparamos un objeto request
        var request = crud.crearRequest('usuarioSistema',1,'listarUsuariosPorArea');
        request.setData({areaID:areaID});
        crud.listar("/configuracionInicial",request,function(data){
            $scope.usuarios  = data.data;
        },function(data){
            console.info(data);
        });
              
    };
    $scope.derivar = function(user){
        
        var requestD = crud.crearRequest('expediente',1,'verificarExistenciaCertificado');
        requestD.setData({UserId:$rootScope.usuMaster.usuario.usuarioID});
        
        crud.listar("/tramiteDocumentario",requestD,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.data.nombreCer != null){
                
                if(!$scope.passCert){
                    modal.mensaje("ALERTA","Clave de certificado del usuario es necesaria ..!");
                    return;
                }

                var request = crud.crearRequest('expediente',1,'derivarExpediente');
               // alert( "usuario master -"+user);
                request.setData({UsuarioD:user,historiales:$scope.expedientesSel,passCert:$scope.passCert,codigo:$scope.expedientesSel[0].codigo,areaID:$scope.areaID,usuarioID:$scope.usuarioID,observacion:$scope.observacion,documentos:$scope.documentos});


                crud.insertar("/tramiteDocumentario",request,function(response){
                    modal.mensaje("CONFIRMACION",response.responseMsg);
                    if(response.responseSta){
                        $scope.expedientesSel.forEach(function (item,index){
                            eliminarElemento(settingR.dataset,item.i);
                            item.selec=false;
                            item.fechaEnvio=response.data[index].fechaEnvio;

                            item.area = buscarContenido( $scope.areas,"areaID","nombre", Number($scope.areaID));
                            insertarElemento(settingD.dataset,item);
                        });
                        $scope.miTablaR.reload();
                        $scope.miTablaD.reload();
                        $('#modalDerivar').modal('hide');

                        $scope.areaID = null;
                        $scope.usuarioID = null;

                    }            
                },function(data){
                    console.info(data);
                });

                firmarDocumentos($scope.expedientesSel[0].codigo,user,$scope.passCert);

            }   
            else{
               if(!$scope.passCert){
                    modal.mensaje("ALERTA","Clave de del Usuario es necesaria ..!");
                    return;
                }

                var request2 = crud.crearRequest('expediente',1,'derivarExpedienteSc');
               // alert( "usuario master -"+user);
                request2.setData({UsuarioD:user,historiales:$scope.expedientesSel,passCert:$scope.passCert,codigo:$scope.expedientesSel[0].codigo,areaID:$scope.areaID,usuarioID:$scope.usuarioID,observacion:$scope.observacion,documentos:$scope.documentos});


                crud.insertar("/tramiteDocumentario",request2,function(response){
                    modal.mensaje("CONFIRMACION",response.responseMsg);
                    if(response.responseSta){
                        $scope.expedientesSel.forEach(function (item,index){
                            eliminarElemento(settingR.dataset,item.i);
                            item.selec=false;
                            item.fechaEnvio=response.data[index].fechaEnvio;

                            item.area = buscarContenido( $scope.areas,"areaID","nombre", Number($scope.areaID));
                            insertarElemento(settingD.dataset,item);
                        });
                        $scope.miTablaR.reload();
                        $scope.miTablaD.reload();
                        $('#modalDerivar').modal('hide');

                        $scope.areaID = null;
                        $scope.usuarioID = null;

                    }            
                },function(data){
                    console.info(data);
                });

            }
                
        },function(data){
            console.info(data);
        });
               


        
        
        
    };
    $scope.passCertificado=function(){
        $scope.passw=$scope.passw2;
        alert("valor->"+$scope.passw)
        $('#modalContraseña').modal('hide');
        
    }
    $scope.generarCargo = function(areaOri,areaDesID){
        
        var expedientesCargo = [];
        settingD.dataset.forEach(function(item){
            if( item.areaID == areaDesID )
               expedientesCargo.push(item);
        });
        if(expedientesCargo.length > 0){
            var areaDes = buscarContenido($scope.areas,"areaID","nombre",Number(areaDesID));
            console.log(areaDes);
            var request = crud.crearRequest('expediente',1,'generarCargo');
            request.setData({areaOri:areaOri,areaDes:areaDes,derivados:expedientesCargo});
            crud.insertar("/tramiteDocumentario",request,function(response){
                if(response.responseSta){
                    verDocumento( response.data.cargo );
                }            
            },function(data){
                console.info(data);
            });
        }
        else
            modal.mensaje("ALERTA","No hay expediente derivados al area seleccionada");
    };
    $scope.generarCargoT2 = function(areaOri){
        
         var expedientesCargo = [];
        settingD.dataset.forEach(function(item){
            if( item.selec )
               expedientesCargo.push(item);
        });
      

        if(expedientesCargo.length > 0) {

            var request = crud.crearRequest('expediente',1,'generarCargoT2');
            request.setData({areaOri:areaOri,derivados:expedientesCargo});
            crud.insertar("/tramiteDocumentario",request,function(response){
                if(response.responseSta){
                    verDocumento( response.data.cargo );
                }            
            },function(data){
                console.info(data);
            });
        }
        else
            modal.mensaje("ALERTA","seleccione elementos para esta operacion");
    };
    $scope.prepararDevolver = function(o){
        
        $scope.observacion = "";
        
        var request = crud.crearRequest('expediente',1,'verHistorialAnterior');
        request.setData({expedienteID:o.expedienteID,historialID:o.historialID});
        crud.listar("/tramiteDocumentario",request,function(response){
            if(response.responseSta){
                
                $scope.historialAnterior = response.data;
            }
            else
                modal.mensaje("CONFIRMACION",response.responseMsg);
        },function(data){
            console.info(data);
        });
        
        $scope.expedientesSel = [];
        $scope.expedientesSel.push(o);
        $('#modalDevolver').modal('show');
    };
    $scope.devolver= function(){
        
        var request = crud.crearRequest('expediente',1,'devolverExpediente');
        request.setData({historiales:$scope.expedientesSel,areaID:$scope.historialAnterior.areaID,usuarioID:$scope.historialAnterior.responsableID,observacion:$scope.historialAnterior.observacion});
        
        crud.insertar("/tramiteDocumentario",request,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                
                eliminarElemento(settingR.dataset,$scope.expedientesSel[0].i);
                $scope.miTablaR.reload();
                $('#modalDevolver').modal('hide');
            }            
        },function(data){
            console.info(data);
        });
    };
    
    $scope.prepararFinalizar = function(o){
        
        $scope.observacion = "";
        
        $scope.documentos = [];
        $scope.expedientesSel = [];
        $scope.expedientesSel.push(o);
        verificarCertificado();
        
        $('#modalFinalizar').modal('show');
        
        
    };
    $scope.finalizar= function(user){
        
      var request = crud.crearRequest('expediente',1,'verificarExistenciaCertificado');
        request.setData({UserId:$rootScope.usuMaster.usuario.usuarioID});
        
        crud.listar("/tramiteDocumentario",request,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            alert(response.data.nombreCer)
            
            
            if(response.data.nombreCer != null){
                
                if(!$scope.passCert2){
                    modal.mensaje("ALERTA","Clave de certificado del usuario es necesaria ..!");
                    return;
                }
                var request3 = crud.crearRequest('expediente',1,'finalizarExpediente');
                request3.setData({UsuarioD:user,historial:$scope.expedientesSel[0],passCert:$scope.passCert2,codigo:$scope.expedientesSel[0].codigo,observacion:$scope.observacion,documentos:$scope.documentos});

                crud.insertar("/tramiteDocumentario",request3,function(response){
                    modal.mensaje("CONFIRMACION",response.responseMsg);
                    if(response.responseSta){
                        eliminarElemento(settingR.dataset,$scope.expedientesSel[0].i);
                        $scope.expedientesSel[0].selec=false;
                        //$scope.expedientesSel[0].fechaAtencion=;
                        $scope.expedientesSel[0].estadoID=5;
                        $scope.expedientesSel[0].estado="FINALIZADO";
                        insertarElemento(settingF.dataset,$scope.expedientesSel[0]);                
                        $scope.miTablaR.reload();
                        $scope.miTablaF.reload();

                        $('#modalFinalizar').modal('hide');
                    }            
                },function(data){
                    console.info(data);
                });
                firmarDocumentos($scope.expedientesSel[0].codigo,user,$scope.passCert2);
    
            }   
            else{
               

                if(!$scope.passCert2){
                    modal.mensaje("ALERTA","Clave de Usuario es necesaria ..!");
                    return;
                }
                var request2= crud.crearRequest('expediente',1,'finalizarExpedienteSc');
                request2.setData({UsuarioD:user,historial:$scope.expedientesSel[0],passCert:$scope.passCert2,codigo:$scope.expedientesSel[0].codigo,observacion:$scope.observacion,documentos:$scope.documentos});

                crud.insertar("/tramiteDocumentario",request2,function(response){
                    modal.mensaje("CONFIRMACION",response.responseMsg);
                    if(response.responseSta){
                        eliminarElemento(settingR.dataset,$scope.expedientesSel[0].i);
                        $scope.expedientesSel[0].selec=false;
                        //$scope.expedientesSel[0].fechaAtencion=;
                        $scope.expedientesSel[0].estadoID=5;
                        $scope.expedientesSel[0].estado="FINALIZADO";
                        insertarElemento(settingF.dataset,$scope.expedientesSel[0]);                
                        $scope.miTablaR.reload();
                        $scope.miTablaF.reload();

                        $('#modalFinalizar').modal('hide');
                    }            
                },function(data){
                    console.info(data);
                });
               
                
            }
                
        },function(data){
            console.info(data);
        });
        
        
       
    };
    $scope.finalizarConFirma= function(usuario){
       $scope.finalizar(usuario);
         alert($scope.expedientesSel[0].codigo);
       firmarDocumentos($scope.expedientesSel[0].codigo,usuario);
    };
    $scope.prepararRecojo = function(){
        $scope.expedientesSel = [];
        settingF.dataset.forEach(function(item){
            if( item.selec )
                $scope.expedientesSel.push(item);
        });
        if($scope.expedientesSel.length > 0)        
            $('#modalRecojo').modal('show');
        else
            modal.mensaje("ALERTA","seleccione elementos para esta operacion");
    };
    $scope.recoger = function(){
        
        var request = crud.crearRequest('expediente',1,'finalExpediente');
        request.setData($scope.expedientesSel);
        
        crud.actualizar("/tramiteDocumentario",request,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                for(var i=0;i<$scope.expedientesSel.length;i++){                    
                    eliminarElemento(settingF.dataset,$scope.expedientesSel[i].i);
                }
                $scope.miTablaF.reload();
                $('#modalRecojo').modal('hide');
            }            
        },function(data){
            console.info(data);
        });
    };
    
    $scope.verDetalleConDocumentos = function(e){
        $scope.expedienteSel = e;
        var request = crud.crearRequest('expediente',1,'verHistorialYDocumentos');
        request.setData({expedienteID:e.expedienteID});
        crud.listar("/tramiteDocumentario",request,function(data){ 
            $scope.expedienteSel.documentos = data.data.documentos;
            $scope.expedienteSel.historial = data.data.historial;
            $('#modalVer').modal('show');
        },function(data){
            console.info(data);
        });
    };
    $scope.verDetalle = function(e){
        $scope.expedienteSel = e;
        var request = crud.crearRequest('expediente',1,'verHistorial');
        request.setData({expedienteID:e.expedienteID});
        crud.listar("/tramiteDocumentario",request,function(data){
            $scope.expedienteSel.historial = data.data.historial;
            //$scope.expedienteSel.rutas = data.data.rutas;
            $('#modalVer').modal('show');
        },function(data){
            console.info(data);
        });
    };
    
    
    $scope.agregarDocumento = function(){
        if($scope.documento.tipoDocumentoID == 0 ){
            modal.mensaje("CONFIRMACION","ingrese un tipo de documento");
            return;
        }
        if($scope.documento.descripcion =="" ){
            modal.mensaje("CONFIRMACION","ingrese descripcion del documento");
            return;
        }
        var cadena1 = $scope.documento.nombreArchivo;
        var cadena2 = cadena1.substring(cadena1.length - 4, cadena1.length);
        if (cadena2 != ".pdf") {
            modal.mensaje("CONFIRMACION","Selecione solo archivos PDF");
            return;
        }
        $scope.documento.nombre = buscarContenido($scope.tipoDocumentos,"tipoDocumentoID","nombre",$scope.documento.tipoDocumentoID);
        $scope.documentos.push($scope.documento);
        $scope.documento= {tipoDocumentoID:0,descripcion:"",nombreArchivo:"",archivo:{},edi:false};
    };
    $scope.editarDocumento = function(i,d){
        //si estamso editando
        if(d.edi){
            d.copia.nombre = buscarContenido($scope.tipoDocumentos,"tipoDocumentoID","nombre",d.tipoDocumentoID);
            $scope.documentos[i] = d.copia;
            
        }
        //si queremos editar
        else{
            d.copia = JSON.parse(JSON.stringify(d));
            d.edi =true;            
        }
    };
    $scope.eliminarDocumento = function(i,d){
        //si estamso cancelando la edicion
        if(d.edi){
            d.edi = false;            
            //delete d.copia;
            d.copia = null;
        }
        //si queremos eliminar el elemento
        else{
            $scope.documentos.splice(i,1);
        }
    };
    
    
    
    $scope.seleccionarTodos = function(s,nombre){
        if(s)
            $scope[nombre].data.forEach(function(item){
                item.selec= true;               
            });
        else
            $scope[nombre].data.forEach(function(item){
                item.selec= false;
            });
    };
    
    $scope.listarAreas = function(organizacionID){
        //preparamos un objeto request
        var request = crud.crearRequest('area',1,'listarAreasPorOrganizacion');
        request.setData({organizacionID:organizacionID});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las areas de exito y error
        crud.listar("/configuracionInicial",request,function(data){
            $scope.areas = data.data;
        },function(data){
            console.info(data);
        });
    };
    
    listarDatos();
    
    
    
    function listarDatos(){
        //preparamos un objeto request
        
        var request = crud.crearRequest('tipoTramite',1,'listarTipoTramite');
        crud.listar("/tramiteDocumentario",request,function(data){
            data.data.forEach(function(item){
                var o = item;
                o.id = item.tipoTramiteID;
                o.title = item.nombre;
                $scope.tipoTramites.push(o);                
            });
        },function(data){
            console.info(data);
        });
        request = crud.crearRequest('tramite_datos',1,'listarEstadoExpedientes');
        crud.listar("/tramiteDocumentario",request,function(data){
            data.data.forEach(function(item){
                var o = item;
                o.id = item.estadoExpedienteID;
                o.title = item.nombre;
                $scope.estados.push(o);
            });
        },function(data){
            console.info(data);
        });
        request = crud.crearRequest('tramite_datos',1,'listarPrioridadExpedientes');
        crud.listar("/tramiteDocumentario",request,function(data){
            data.data.forEach(function(item){
                var o = item;
                o.id = item.prioridadExpedienteID;
                o.title = item.nombre;
                $scope.prioridades.push(o);
            });
        },function(data){
            console.info(data);
        });
        
        request = crud.crearRequest('area',1,'listarTipoAreas');
        crud.listar("/configuracionInicial",request,function(data){
            data.data.forEach(function(item){
                var o = item;
                o.id = item.tipoAreaID;
                o.title = item.nombre;
                $scope.areas.push(o);
            });
           
        },function(data){
            console.info(data);
        });
        
        request = crud.crearRequest('tramite_datos',1,'listarTipoDocumentos');
        crud.listar("/tramiteDocumentario",request,function(data){
            $scope.tipoDocumentos = data.data;
        },function(data){
            console.info(data);
        });
        
    };
    function firmarDocumentos(cod,usuario,passCert){
        alert("firmando docs"+cod);
        
        if(!cod ){
            modal.mensaje("ALERTA","Codigo Expediente necesario");
            return;
        }
        var request = crud.crearRequest('expediente',1,'firmaDocumentos');
        
        request.setData({codigoExp:cod,passCert:passCert,UsuarioD:usuario});
        
        crud.listar("/tramiteDocumentario",request,function(response){
            if(response.data){
                console.log(response.data);
               modal.mensaje("ALERTA","Los Documentos se Firmaron Correctamente");
               //alert("Firmado Correctamente");
             }
        },function(data){
            console.info(data);
        });
    };
	
   
    function verificarCertificado(){
       
        var requestV = crud.crearRequest('expediente',1,'verificarExistenciaCertificado');
        requestV.setData({UserId:$rootScope.usuMaster.usuario.usuarioID});
        
        crud.listar("/tramiteDocumentario",requestV,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.data.nombreCer != null){
                $scope.campoContrasenia="Contraseña del Certificado";
    
            }   
            else{
                //alert("No se encontro certifificado para el usuario") ; 
                $scope.campoContrasenia="Contraseña del Usuario";
            }
                
        },function(data){
            console.info(data);
        });
    }
   
}]);
