app.controller("consultaExpedienteCtrl",["$scope","NgTableParams","crud","modal", function ($scope,NgTableParams,crud,modal){
        
    var params = {count: 10};
    var setting = { counts: []};
    $scope.miTabla = new NgTableParams(params, setting);
    
    $scope.tipoTramites = [];
    $scope.prioridades = [];
    
    $scope.organizacionSel;
    $scope.dni = "";
    $scope.nombre = "";
    $scope.apellidop = "";
    $scope.apellidoM = "";
    $scope.ruc = "";
    $scope.codigo = "";
    $scope.desde = new Date();
    $scope.desde.setDate(1);
    
    $scope.hasta = new Date("dd/mm/aaaa");
    
    $scope.escribiendoDni=function(){
        $scope.ruc = "";
    };
    $scope.escribiendoNombre=function(){
        $scope.nombre = "";
    };
    $scope.escribiendoApellidoP=function(){
        $scope.apellidoP = "";
    };
     $scope.escribiendoApellidoM=function(){
        $scope.apellidoM = "";
    };
    $scope.escribiendoRuc=function(){
        $scope.dni = "";
    };
    
    $scope.buscarExpedientes = function(desde,hasta){
        
        
        //preparamos un objeto request
        var request = crud.crearRequest('expediente',1,'consultarExpediente');
        request.setData({organizacionID:$scope.organizacionSel,nombre:$scope.nombre,apellidoP:$scope.apellidoP,apellidoM:$scope.apellidoM,dni:$scope.dni,ruc:$scope.ruc,codigo:$scope.codigo,desde:convertirFecha(desde),hasta:convertirFecha(hasta)});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/tramiteDocumentario",request,function(response){            
            if(!response.responseSta){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                return;
            }
            if(response.data){
                setting.dataset = response.data;
                iniciarPosiciones(setting.dataset);
                $scope.miTabla.settings(setting);
            }
        },function(data){
            console.info(data);
        });
    };
    
    $scope.verDetalleConDocumentos = function(e){
        $scope.expedienteSel = e;
        var request = crud.crearRequest('expediente',1,'verHistorialYDocumentos');
        request.setData({expedienteID:e.expedienteID});
        crud.listar("/tramiteDocumentario",request,function(data){
            $scope.expedienteSel.documentos = data.data.documentos;
            $scope.expedienteSel.historial = data.data.historial;
            $('#modalVer').modal('show');
        },function(data){
            console.info(data);
        });
    };
    
    listarDatos();
    function listarDatos(){
        /*
        //preparamos un objeto request
        var request = crud.crearRequest('tramite_datos',1,'listarEstadoExpedientes');
        crud.listar("/tramiteDocumentario",request,function(data){
            $scope.estados = data.data;
        },function(data){
            console.info(data);
        });*/
        request = crud.crearRequest('tramite_datos',1,'listarPrioridadExpedientes');
        crud.listar("/tramiteDocumentario",request,function(data){
            data.data.forEach(function(item){
                var o = item;
                o.id = item.prioridadExpedienteID;
                o.title = item.nombre;
                $scope.prioridades.push(o);
            });
        },function(data){
            console.info(data);
        });        
        request = crud.crearRequest('tipoTramite',1,'listarTipoTramite');
        crud.listar("/tramiteDocumentario",request,function(data){
            data.data.forEach(function(item){
                var o = item;
                o.id = item.tipoTramiteID;
                o.title = item.nombre;
                $scope.tipoTramites.push(o);                
            });
        },function(data){
            console.info(data);
        });
        request = crud.crearRequest('organizacion',1,'listarOrganizaciones');
        crud.listar("/configuracionInicial",request,function(data){
            $scope.organizaciones = data.data;
        },function(data){
            console.info(data);
        });
    };
	
}]);
