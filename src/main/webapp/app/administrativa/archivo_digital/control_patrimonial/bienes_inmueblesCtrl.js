app.controller("bienes_inmueblesCtrl",["$rootScope", "$scope","NgTableParams","$location","crud","modal", "$http", "$timeout",function ($rootScope, $scope,NgTableParams,$location,crud,modal, $http, $timeout){
        
    var ORG_ID = $rootScope.usuMaster.organizacion.organizacionID
        
    /*Guardar ambientes original*/   
    $scope.AMBIENTES = [];
    
    $scope.accion_bien = "Registrar";
    $scope.actualizarBienInmueble = true; //Flag para determinar si se llama al servicio actualizar o registrar
    
    $scope.datosUbigeo = {
        departamentos: [],
        provincias: [],
        distritos: [],
        dpto: -1,
        prov: -1,
        dist: -1
    };
        
    /*Datos Catalogo*/
    $scope.item_catalogo= {
        gru_gen_id:0,
        cla_gen_id:0,
        fam_gen_id:0,
        uni_med_id:0,
        cod:0,
        den_bie:""
    };
    
    $scope.ambiente = {
        descripcion:"",
        ubicacion:"",
        estado:"",
        area:0,
        inst_cons:"",
        fec_cons:new Date(),
        user:0,
        orgid:0
    };
    
    /*Condicion*/
    $scope.condicion = [
        {cond_id:"B",cond_nom:"BUENO"},
        {cond_id:"M",cond_nom:"MALO"},
        {cond_id:"R",cond_nom:"REGULAR"}
    ];
    
    $scope.lista = {
        direccion :"",
        propiedad:"",
        tipo_terreno:"",
        um:"",
        condicion:""
    }
    
    
    /*Datos Bien Inmueble*/
    $scope.bien_inmueble = {
        /*Cabecera*/
        bien_id: 0,
        descripcion:"",
        tip_dire:"",
        direccion:"",
        nro:0,
        mz:"",
        lt:0,
        departamento:"",
        provincia:"",
        distrito:"",
        user:0,
        orgId:0,
        /*Detalle Tecnico*/
        propiedad:"",
        tipo_terreno:"",
        est_sanea:false,
        area: 0,
        um:"",
        part_elec:"",
        nro_fic_reg:"",
        reg_sinabip:"",
        /*Ambiente*/
        amb_id:0,
        actualizar_bien: false
    };
    
    
    /*INFORMACION ADICIONAL*/
    $scope.propiedad = [
        {nombre: "Estatal"}, {nombre: "Particular"}
    ];

    $scope.tipo_terreno = [
        {nombre: "Estatal"}, {nombre: "Particular"}
    ];
    $scope.unidades_metricas = [
        {nombre: "m2"}, {nombre: "km2"}
    ];

    /*Direccion*/
    $scope.direccion = [
        {nombre: "Avenida"}, {nombre: "Jiron"}, {nombre: "Calle"},
        {nombre: "Urbanizacion"}, {nombre: "Alameda"}, {nombre: "Malecon"},
        {nombre: "Ovalo"}, {nombre: "Alameda"}, {nombre: "Plaza"},
        {nombre: "Unidad Vecinal"}, {nombre: "Cooperativa"}, {nombre: "Parque"},
        {nombre: "Residencial"}, {nombre: "Carretera"}, {nombre: "Asent.Humano"}
    ];

    $scope.dire={};

    var listar_ambientes = false;
    
    $scope.revisarLocalStorage = function () {
        
        if (localStorage.getItem("bien_inmueble") !== null) {
            $scope.actualizarBienInmueble = true;
            $scope.accion_bien = "Actualizar";
            var bienInmuebleTemp = JSON.parse(localStorage.getItem("bien_inmueble"));
            console.log(bienInmuebleTemp);
            $scope.buscar_bien_inmueble(bienInmuebleTemp.bien_inm, bienInmuebleTemp.org_id);
        }        
    }
      
    $scope.loadDpto = function () {
        $http.get('../recursos/json/departamentos.json').success(function (data) {
            $scope.datosUbigeo.departamentos = data;
        });
        $scope.datosUbigeo.dpto = 01;
        $scope.datosUbigeo.prov = -1;
        $scope.datosUbigeo.dist = -1;
    };
    
    $scope.loadProv = function () {
        $http.get('../recursos/json/provincias.json').success(function (data) {
            var dep = $scope.datosUbigeo.departamentos.filter(function (obj) {
                return obj.codigo_ubigeo === $scope.datosUbigeo.dpto;
            })[0];
            $scope.datosUbigeo.provincias = data[dep.id_ubigeo];
            $scope.datosUbigeo.distritos = [];
            $scope.datosUbigeo.prov = -1;
            $scope.datosUbigeo.dist = -1;
        });
    };
    
    $scope.loadDist = function () {
        $http.get('../recursos/json/distritos.json').success(function (data) {
            var pro = $scope.datosUbigeo.provincias.filter(function (obj) {
                return obj.codigo_ubigeo === $scope.datosUbigeo.prov;
            })[0];
            $scope.datosUbigeo.distritos = data[pro.id_ubigeo];
            $scope.datosUbigeo.dist = -1;
        });
    };       
    
    /*FUNCIONES AUXILIARES*/
    $scope.listar_tipo_propiedad = function(){
        
        var request = crud.crearRequest('configuracion_patrimonial',1,'listar_tipo_propiedad');
        crud.listar("/controlPatrimonial",request,function(data){
            if(data.data){
                $scope.propiedad = data.data;
            }
            },function(data){    
            console.info(data);
        });  
    }
    
    $scope.listar_tipo_terreno = function(){
        var request = crud.crearRequest('configuracion_patrimonial',1,'listar_tipo_terreno');
        crud.listar("/controlPatrimonial",request,function(data){
            if(data.data){
                $scope.tipo_terreno = data.data;
            }
            },function(data){
                
            console.info(data);
        });
   
    }
    
    $scope.listar_unidades_metricas = function(){
        var request = crud.crearRequest('configuracion_patrimonial',1,'listar_unidades_metricas');
        crud.listar("/controlPatrimonial",request,function(data){
            if(data.data){
                $scope.unidades_metricas = data.data;
            }
            },function(data){
                
            console.info(data);
        });
    } 
    /*----------------------*/

    /*Tabla de Ambientes*/
    var params = {count: 20};
    var setting = { counts: []};
    $scope.tabla_ambientes = new NgTableParams(params, setting); 
    
    
    $scope.registrar_bien_inmueble = function(user,orgid){
        
        //Ubigeo        
        $scope.bien_inmueble.departamento = $scope.datosUbigeo.dpto;
        $scope.bien_inmueble.provincia = $scope.datosUbigeo.prov;
        $scope.bien_inmueble.distrito = $scope.datosUbigeo.dist;
        
        /*OBTENEMOS LAS LISTAS*/
        $scope.bien_inmueble.tip_dire = $scope.lista.direccion.nombre;
        $scope.bien_inmueble.propiedad = $scope.lista.propiedad.nombre;
        $scope.bien_inmueble.tipo_terreno = $scope.lista.tipo_terreno.nombre;
        $scope.bien_inmueble.um = $scope.lista.um.nombre;
        
        $scope.bien_inmueble.user = user;
        $scope.bien_inmueble.orgId = orgid;
        
        console.log($scope.bien_inmueble);    
        //$scope.borrarValoresBien();
        //return;
        
        var accept = true;
        for(var atrib in $scope.bien_inmueble){
            if($scope.bien_inmueble[atrib] == "" || $scope.bien_inmueble[atrib] == -1){
                if(atrib=="descripcion"){continue;}
                if(atrib=="mz"){continue;}
                if(atrib=="lt"){continue;}
                if(atrib=="reg_sinabip"){continue;}
                accept = false;
                break;
            }
        }
        if (accept == true) {
            
            var msgConfirmModal = "Seguro que desea registrar el bien inmueble";
            
            if ($scope.actualizarBienInmueble) {
                msgConfirmModal = "Seguro que desea actualizar el bien inmueble";
            }
            
            modal.mensajeConfirmacion($scope, msgConfirmModal, function () {

                var request = crud.crearRequest('ingresos', 1, 'registrar_bien_inmueble');
                request.setData($scope.bien_inmueble);

                crud.insertar("/controlPatrimonial", request, function (response) {
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                    if (response.responseSta) {

                        $scope.borrarValoresBien();
                        localStorage.removeItem('bien_inmueble');
                        var path = localStorage.getItem("prev_page");
                        localStorage.removeItem('prev_page');
                        $location.path(path);

                    }
                }, function (data) {
                    console.info(data);
                });
            });
                
        } else {
            modal.mensaje("ERROR!!","Verifique que todos los campos esten correctamente llenados");
        }

    }
    
    
    $scope.borrarValoresBien = function(){
        $scope.bien_inmueble = {
            bien_id: 0,
            /*Cabecera*/
            descripcion: "",
            tip_dire: "",
            direccion: "",
            nro: 0,
            mz: "",
            lt: "",
            departamento: "",
            provincia: "",
            distrito: "",
            user: 0,
            orgId: 0,
            /*Detalle Tecnico*/
            propiedad: "",
            tipo_terreno: "",
            est_sanea: false,
            area: 0,
            um: "",
            part_elec: "",
            nro_fic_reg: "",
            reg_sinabip: "",
            /*Ambiente*/
            amb_id: 0,
            actualizar_bien: false
        };
        
        $scope.lista = {
            direccion: "",
            propiedad: "",
            tipo_terreno: "",
            um: "",
            condicion: ""
        }
        
        $scope.datosUbigeo.dpto = -1;
        $scope.datosUbigeo.prov = -1;
        $scope.datosUbigeo.dist = -1;
        
        $scope.reloadAmbientes();
    }
 
    $scope.mostrarAmbientes = function(){
        var request = crud.crearRequest('ingresos',1,'listar_ambientes');
        request.setData({org_id: ORG_ID});
        crud.listar("/controlPatrimonial",request,function(data){
            if(data.data){
                setting.dataset = data.data;
                $scope.AMBIENTES = JSON.parse(JSON.stringify(data.data)); 
                iniciarPosiciones(setting.dataset);                
                $scope.tabla_ambientes.settings(setting);
                $scope.tabla_ambientes.reload();
                
                //console.log($scope.tabla_ambientes);
                
            }
        },function(data){
            console.info(data);
        });
   }
   
    $scope.registrarAmbientes = function(user,org_id){
       
    //   $scope.ambiente.estado = $scope.ambiente.estado.cond_nom;
        $scope.ambiente.estado = $scope.lista.condicion.cond_id;       
        $scope.ambiente.fec_cons = convertirFecha($scope.ambiente.fec_cons);
        $scope.ambiente.user = user;
        $scope.ambiente.orgid = org_id;
        var accept = true;
        for(var atrib in $scope.ambiente){
            if($scope.ambiente[atrib]==""){
                accept = false;
                break;
            }
        }
        
        console.log($scope.ambiente);
        if(accept==true){
            modal.mensajeConfirmacion($scope,"Seguro que desea registrar Ambiente",function(){
               
               var request = crud.crearRequest('ingresos',1,'registrar_ambientes');
               request.setData($scope.ambiente);        
               
               crud.insertar("/controlPatrimonial",request,function(response){
                   modal.mensaje("CONFIRMACION",response.responseMsg);
                   if(response.responseSta){
                    
                    $scope.borrar_valores_ambiente();   
                    
                   }
               },function(data){
                   console.info(data);
               });
            });
        }else{
             modal.mensaje("ERROR AL REGISTRAR","Verifique que todos los campos esten correctamente llenados");
        }
   }
   
   
    $scope.borrar_valores_ambiente = function(){
        
        $scope.ambiente = {
            descripcion: "",
            ubicacion: "",
            estado: "",
            area: 0,
            inst_cons: "",
            fec_cons: new Date()
        };
   }
   
    $scope.reporte_bienes_inmuebles = function(){
       
       
   }
   

   
   $scope.elegirAmbiente = function(amb){

       $scope.bien_inmueble.amb_id = amb.amb_id;
       var my_array = new Array();
       my_array[0] = amb;
       setting.dataset = my_array;
       iniciarPosiciones(setting.dataset);
       $scope.tabla_ambientes.settings(setting);
       $scope.tabla_ambientes.reload();
   }

    $scope.buscar_bien_inmueble = function(id_bien,orgId){
               
        var request = crud.crearRequest('ingresos', 1, 'obtener_bien_inmueble');
        request.setData({id_bie_inm: id_bien});
        crud.listar("/controlPatrimonial", request, function (data) {
            console.log(data);
            if (data.data) {
                $scope.bien_inmueble.bien_id = id_bien;
                $scope.bien_inmueble.actualizar_bien = true;
                $scope.bien_inmueble.direccion = data.data[0].direccion;                
                $scope.bien_inmueble.descripcion = data.data[0].descripcion;
                $scope.bien_inmueble.nro = data.data[0].nro;
                $scope.bien_inmueble.mz = data.data[0].mz;
                $scope.bien_inmueble.lt = data.data[0].lt;
                $scope.bien_inmueble.departamento = data.data[0].departamento;
                $scope.bien_inmueble.provincia = data.data[0].provincia;
                $scope.bien_inmueble.distrito = data.data[0].distrito;
                
                $scope.bien_inmueble.part_elec = data.data[0].part_elec;
                $scope.bien_inmueble.nro_fic_reg = data.data[0].nro_fic_reg;
                $scope.bien_inmueble.reg_sinabip = data.data[0].reg_sinabip;
                $scope.bien_inmueble.est_sanea = data.data[0].est_sanea;
                $scope.bien_inmueble.area = data.data[0].area;
                   
                $scope.bien_inmueble.amb_id = data.data[0].amb_id;
                
                $scope.obtenerDireccion(data.data[0].tip_dire);
                $scope.obtenerPropiedad(data.data[0].propiedad);
                $scope.obtenerTipoTerreno(data.data[0].tipo_terreno);
                $scope.obtenerUM(data.data[0].um);

                $scope.datosUbigeo.dpto = data.data[0].departamento;
                $scope.loadProv();
                
                $timeout(function () {          
                    
                    $scope.buscarAmbiente(data.data[0].amb_id);
                                            
                    $scope.datosUbigeo.prov = data.data[0].provincia;
                    $scope.loadDist();
                    
                    $timeout(function(){
                        $scope.datosUbigeo.dist = data.data[0].distrito;
                    }, 3000);
                    
                }, 3000);
            }
        }, function (data) {
            console.info(data);
        }); 
 
    }
    
    $scope.cancelarRegistro = function() {
        
        $scope.borrarValoresBien();
        localStorage.removeItem('bien_inmueble');
        localStorage.removeItem('prev_page');
    }
        
    $scope.obtenerPropiedad = function(NomProp){
       
        if($scope.propiedad[0].nombre == NomProp){
            $scope.lista.propiedad= $scope.propiedad[0];
        }else{
            $scope.lista.propiedad = $scope.propiedad[1];

        }
    }
    $scope.obtenerTipoTerreno = function(NomTipTer){
        
        if($scope.tipo_terreno[0].nombre == NomTipTer){
             $scope.lista.tipo_terreno=$scope.tipo_terreno[0];
        }else{
             $scope.lista.tipo_terreno=$scope.tipo_terreno[1];
        }

    }
    
    $scope.obtenerUM = function(NomUM){
        
        if($scope.unidades_metricas[0].nombre == NomUM){
            $scope.lista.um = $scope.unidades_metricas[0];
        }else{
            $scope.lista.um = $scope.unidades_metricas[1];
        }
    }
    
    $scope.obtenerDireccion = function(Dire){
        
        if($scope.direccion[0].nombre == Dire){
            $scope.lista.direccion = $scope.direccion[0];
        }else{
            $scope.lista.direccion = $scope.direccion[1];
        }
    }
    
    $scope.buscarAmbiente = function(ambId){
        
        $scope.selectedAmbiente = true;
        var size = setting.dataset.length;
        for (var i = 0; i < size; i++)
        {
            if (setting.dataset[i].amb_id == ambId) {
                //editar_item_catalogo = true;
                var setting2 = {counts: []};
                /*Redimensionamos la tabla de catalogo de bienes*/
                var my_array = new Array();
                my_array[0] = setting.dataset[i];
                setting2.dataset = my_array;
                setting.dataset = setting2.dataset;
                iniciarPosiciones(setting.dataset);
                $scope.tabla_ambientes.settings(setting);
                $scope.tabla_ambientes.reload();
                break;
            }
        }
    }
    
    $scope.reloadAmbientes = function () {
        
        $scope.selectedAmbiente = false;
        //console.log($scope.AMBIENTES);
        setting.dataset = $scope.AMBIENTES;
        iniciarPosiciones(setting.dataset);
        $scope.tabla_ambientes.settings(setting);
        $scope.tabla_ambientes.reload();
   
   }
    
    $scope.changeSelectedAmbiente = function(event, item) {
        
        if (event.target.checked) {       
           $scope.elegirAmbiente(item);        
        
        } else {
            $scope.reloadAmbientes();
        }
   }
   
   $scope.openModalNuevoAmbiente = function () {
       $("#modalNuevoAmbiente").modal('show');
   }
   
}]);

