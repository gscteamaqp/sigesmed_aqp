/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


app.controller("ambientesCtrl",["$rootScope", "$scope", "$filter", "NgTableParams","$location","crud","modal", function ($rootScope, $scope, $filter, NgTableParams,$location,crud,modal){
        
    console.log("ambientes");
    
    var ORG_ID = $rootScope.usuMaster.organizacion.organizacionID;
    
    // Table ambientes 
    var params = { count: 20 };
    var setting = { counts: []};
    $scope.tabla_ambientes = new NgTableParams(params, setting);
    
    $scope.flagEditarAmbiente = false;
    
    /*
     * Objeto que guarda los datos del nuevo ambiente
     */
    $scope.ambiente = {
        amb_id: null,
        descripcion:"",
        ubicacion:"",
        estado:"",
        area:0,
        inst_cons:"",
        fec_cons_dt: new Date(),
        fec_cons: "",
        user:0,
        orgid:0
    };
    
    
    /*
     * Array de Objectos que guardan la condicion del ambiente
     */
    $scope.condicion = [
        {cond_id:"B",cond_nom:"BUENO"},
        {cond_id:"M",cond_nom:"MALO"},
        {cond_id:"R",cond_nom:"REGULAR"}
    ];
    
    $scope.lista = {
        direccion :"",
        propiedad:"",
        tipo_terreno:"",
        um:"",
        condicion:""
    };
    
    /*
     * Mostrar ambientes
     * Servicio que consulta los ambientes pertenecientes a una IE
     * @returns {tabla_ambientes}
     */
    $scope.mostrarAmbientes = function(){
        var request = crud.crearRequest('ingresos',1,'listar_ambientes');
        request.setData({org_id: ORG_ID});
        crud.listar("/controlPatrimonial",request,function(response) {
            if(response.data){
                setting.dataset = response.data;                
                iniciarPosiciones(setting.dataset);                
                $scope.tabla_ambientes.settings(setting);
                $scope.tabla_ambientes.reload();                
                //console.log($scope.tabla_ambientes);                
            }
        },function(data){
            console.info(data);
        });
    }
    
    /*
     * Mostrar modal agregar Ambiente
     * Muesta el modal y limpia los campos para ingresar un nuevo ambiente  
     */
    $scope.mostrarModalAgregarAmbiente = function() {
        $scope.flagEditarAmbiente = false;
        $scope.limpiar_datos_ambiente();
        $("#modalNuevoAmbiente").modal('show');
        console.log("open modal");
    }
    
    /*
     * Agregar el nuevo ambiente
     */
    $scope.registrarAmbientes = function(user,org_id){
       
    //   $scope.ambiente.estado = $scope.ambiente.estado.cond_nom;
        $scope.ambiente.estado = $scope.lista.condicion.cond_id;
        $scope.ambiente.fec_cons = convertirFecha($scope.ambiente.fec_cons_dt);
        $scope.ambiente.user = user;
        $scope.ambiente.orgid = org_id;
       
        var accept = true;
        for(var atrib in $scope.ambiente){
            if($scope.ambiente[atrib]==""){
                accept = false;
                break;
            }
        }
        if (accept == true) {
            modal.mensajeConfirmacion($scope,"Seguro que desea registrar el nuevo ambiente",function(){
               
               var request = crud.crearRequest('ingresos',1,'registrar_ambientes');
               request.setData($scope.ambiente);        
               
               console.log(request);
               
               crud.insertar("/controlPatrimonial",request,function(response){
                   modal.mensaje("CONFIRMACION",response.responseMsg);
                   if(response.responseSta){                    
                       
                       $("#modalNuevoAmbiente").modal('hide'); 
                       $scope.mostrarAmbientes();
                   }
               },function(data){
                   console.info(data);
               });
            });
        } else {
            modal.mensaje("ERROR AL REGISTRAR","Verifique que todos los campos esten correctamente llenados");
        }
   }
    
    
    /*
     * Mostrar modal para editar el ambiente
     */
    
    $scope.cargarEditarAmbiente = function(item) {
        //console.log(item);
        $scope.flagEditarAmbiente = true;
        
        $scope.ambiente.amb_id = item.amb_id;
        $scope.ambiente.descripcion = item.amb_des;
        $scope.ambiente.ubicacion = item.amb_ubi;
        $scope.ambiente.area = item.amb_area;
        $scope.ambiente.inst_cons = item.amb_inst_cons;
        $scope.ambiente.fec_cons_dt = new Date(item.amb_fec_cons);
        
        var amb_est = $filter('filter')($scope.condicion, {cond_id: item.amb_est})[0];
        $scope.lista.condicion = amb_est;
        
        console.log(amb_est);
        
        $("#modalNuevoAmbiente").modal('show'); 
    }
    
    /*
     * Editar ambiente
     */
    
    $scope.editarAmbiente = function(user, orgId) {
        
        $scope.ambiente.estado = $scope.lista.condicion.cond_id;
        $scope.ambiente.fec_cons = convertirFecha($scope.ambiente.fec_cons_dt);
        $scope.ambiente.user = user;
        $scope.ambiente.orgid = orgId;
        
        //console.log($scope.ambiente);
        
        var accept = true;
        for(var atrib in $scope.ambiente){
            if($scope.ambiente[atrib]==""){
                accept = false;
                break;
            }
        }
        if (accept == true) {
            
            var request = crud.crearRequest('ingresos', 1, 'editar_ambiente');
            request.setData($scope.ambiente);

            crud.insertar("/controlPatrimonial", request, function (response) {

                if (response.responseSta) {

                    $scope.mostrarAmbientes();
                    $("#modalNuevoAmbiente").modal('hide');
                }

            }, function (data) {
                console.info(data);
            });
        
        } else {
            modal.mensaje("ERROR AL ACTUALIZAR","Verifique que todos los campos esten correctamente llenados");
        }
        
    }
    
    /*
     * Eliminar ambiente
     * Servicio que elimina un ambiente por amb_id
     * @returns 
     */
    $scope.eliminarAmbiente = function(item) {
        
        modal.mensajeConfirmacion($scope, "Seguro que desea eliminar el ambiente", function () {

            var request = crud.crearRequest('ingresos',1,'eliminar_ambiente');        
            var ambId = item.amb_id;
            request.setData({amb_id: ambId});

            crud.eliminar("/controlPatrimonial", request, function (response) {

                if (response.responseSta) {
                    $scope.mostrarAmbientes();
                }  

            }, function (data) {
                console.info(data);
            });
        });
        
    }
    
    $scope.limpiar_datos_ambiente = function () {
        
        $scope.ambiente = {
            amb_id: null,
            descripcion:"",
            ubicacion:"",
            estado:"",
            area:0,
            inst_cons:"",
            fec_cons_dt: new Date(),
            fec_cons: "",
            user:0,
            orgid:0
        };
        
        $scope.lista = {                
            condicion: ""
        };
    }
}]);
