app.controller("consulta_inventarioCtrl",["$scope","NgTableParams","$location","crud","modal", function ($scope,NgTableParams,$location,crud,modal){


  /*Tabla de Bienes Inmuebles*/
    
    var params = {count: 20};
    var setting = { counts: []};
    $scope.tabla_inventarios = new NgTableParams(params, setting); 


    $scope.listar_inventarios = function(orgId){
        var request = crud.crearRequest('ingresos',1,'listar_inventarios');
        request.setData({org_id:orgId});
       
        crud.listar("/controlPatrimonial",request,function(response){
            if(response.responseSta){
                setting.dataset = response.data;
                iniciarPosiciones(setting.dataset);
                $scope.tabla_inventarios.settings(setting);
                $scope.tabla_inventarios.reload();
            }
            },function(data){
                
            console.info(data);
        });  
    }
    
    $scope.verificar_inventario = function(inventario,orgId){
        
         $scope.verificar_inventario = {
            id_inv:0,
            org_id:0,
            tipo_inv:"",
            interfaz:""
          };
          $scope.verificar_inventario.id_inv = inventario.inv_ini_id;
          $scope.verificar_inventario.org_id = orgId;
          $scope.verificar_inventario.tipo_inv = inventario.tipo_inv;
          $scope.verificar_inventario.interfaz = "consulta_inventario";

          localStorage.setItem('update_inventario', window.btoa(JSON.stringify($scope.verificar_inventario)) );
          $location.path('inventario_inicial');
            
    }
    
    $scope.editarInventario = function(inv, orgId) {
        
        console.log(inv);
        
        if(inv.tipo_inv == "Inventario Inicial"){
            
            var invData = { inv_id: inv.inv_id, org_id: orgId, tipo_inv: "I"}; 
            
        } else if (inv.tipo_inv == "Inventario Fisico") {
            
            var invData = { inv_id: inv.inv_id, org_id: orgId, tipo_inv: "F"};  
        
        }        
        
        localStorage.setItem("inv_data", JSON.stringify(invData));
        $location.path( "/inventario_inicial" );
        
    }
    
    $scope.eliminarInventario = function(Inv){
        
        if(Inv.tipo_inv=="Inventario Inicial"){
            
            modal.mensajeConfirmacion($scope,"Seguro que desea eliminar este Inventario Inicial ?",function(){
            var request = crud.crearRequest('ingresos',1,'eliminar_inventario_inicial');
            request.setData({inv_ini_id:Inv.inv_ini_id});
                crud.eliminar("/controlPatrimonial",request,function(response){  

                    modal.mensaje("CONFIRMACION",response.responseMsg);        

                        if(response.responseSta){
                            eliminarElemento(setting.dataset,Inv.i);
                            iniciarPosiciones(setting.dataset);  
                            $scope.tabla_inventarios.settings(setting);
                            $scope.tabla_inventarios.reload();
                        }
                        
                    },function(data){
                        console.info(data);
                    });
            });
        }
        if(Inv.tipo_inv=="Inventario Fisico"){
            
            modal.mensajeConfirmacion($scope,"Seguro que desea eliminar este Inventario Fisico  ?",function(){
            var request = crud.crearRequest('ingresos',1,'eliminar_inventario_fisico');
            request.setData({inv_fis_id:Inv.inv_fis_id});
            crud.eliminar("/controlPatrimonial",request,function(response){   
            
                modal.mensaje("CONFIRMACION",response.responseMsg);     
                    if(response.responseSta){
                        eliminarElemento(setting.dataset,Inv.i);
                        iniciarPosiciones(setting.dataset);  
                        $scope.tabla_inventarios.settings(setting);
                        $scope.tabla_inventarios.reload();
                    }
                },function(data){
                    console.info(data);
                });
            });
            
        }
    }
    
    $scope.reporteInventarios = function (orgId) {

        var request = crud.crearRequest('ingresos', 1, 'reporte_inventarios');
        request.setData({org_id: orgId});
        crud.listar("/controlPatrimonial", request, function (data) {
            $scope.dataBase64 = data.data[0].datareporte;
            window.open($scope.dataBase64);
        }, function (data) {
            console.info(data);
        });
    }
    
    $scope.reporteInventarioDetalle = function(item, orgId) {
        
        if(item.tipo_inv=="Inventario Inicial"){
            
            var request = crud.crearRequest('ingresos', 1, 'reporte_inventario_inicial');
            
            request.setData({org_id: orgId, inv_ini_id: item.inv_ini_id});            
            
            crud.listar("/controlPatrimonial", request, function (data) {
                $scope.dataBase64 = data.data[0].datareporte;
                window.open($scope.dataBase64);
            }, function (data) {
                console.info(data);
            });
        }
        
        if(item.tipo_inv=="Inventario Fisico"){
            
            var request = crud.crearRequest('ingresos', 1, 'reporte_inventario_fisico');
            
            request.setData({org_id: orgId, inv_fis_id: item.inv_fis_id});           
            
            crud.listar("/controlPatrimonial", request, function (data) {
                $scope.dataBase64 = data.data[0].datareporte;
                window.open($scope.dataBase64);
            }, function (data) {
                console.info(data);
            });
            
        }
        
        
    }

}]);