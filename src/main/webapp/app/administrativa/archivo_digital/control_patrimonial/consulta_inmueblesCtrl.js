app.controller("consulta_inmueblesCtrl",["$scope","NgTableParams","$location","crud","modal", function ($scope,NgTableParams,$location,crud,modal){
        
    
    $scope.bienInmueble = {
        amb_id: 4,
        area: 100,
        departamento: "04",
        descripcion: "Bien inmueble de prueba",
        direccion: "aaa",
        distrito: "01",
        est_sanea: true,
        lt: 1,
        mz: "1",
        nro: 1,
        nro_fic_reg: "33",
        part_elec: "111",
        propiedad: "Particular",
        provincia: "01",
        reg_sinabip: "22",
        tip_dire: "Avenida",
        tipo_terreno: "Estatal",
        um: "m2"
    }
    
    /*Tabla de Bienes Inmuebles*/
    
    var params = {count: 20};
    var setting = { counts: []};
    $scope.tabla_bienes_inmuebles = new NgTableParams(params, setting); 


    $scope.listar_inmuebles = function(orgId){
        var request = crud.crearRequest('ingresos',1,'listar_bienes_inmuebles');
        request.setData({org_id:orgId});
        crud.listar("/controlPatrimonial",request,function(response){
            if(response.responseSta){
                setting.dataset = response.data;
                iniciarPosiciones(setting.dataset);
                $scope.tabla_bienes_inmuebles.settings(setting);
                $scope.tabla_bienes_inmuebles.reload();
            }
        },function(data){
            console.info(data);
        });
    }

    $scope.editarBienInmueble = function(bienInm,orgId){
        localStorage.setItem("bien_inmueble", JSON.stringify({bien_inm: bienInm.cod_bie_inm, org_id: orgId}));
        localStorage.setItem("prev_page", "/consulta_inmuebles");
        $location.path( "/bienes_inmuebles" );
    }

    $scope.eliminarBienInmueble = function(bien_inm){
        
        modal.mensajeConfirmacion($scope,"Seguro que desea eliminar el bien inmueble?",function(){
       
        var request = crud.crearRequest('ingresos',1,'eliminar_bien_inmueble');
           request.setData(bien_inm);
           
           crud.eliminar("/controlPatrimonial",request,
            function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    /*Eliminamos el Elemento en la Tabla y Actualizamos*/
                    eliminarElemento(setting.dataset, bien_inm.i);
                    iniciarPosiciones(setting.dataset);
                    $scope.tabla_bienes_inmuebles.settings(setting);
                    $scope.tabla_bienes_inmuebles.reload();
                }

            },function(data){
            });

        });

    }

    /*Edicion y Verificacion de Bienes*/
    $scope.verificar_registro_bien = function (bien, user) {


        $scope.verificar_bien = {
            id_bien: 0,
            org_id: 0,
            interfaz: ""
        };
        
        $scope.verificar_bien.id_bien = bien.cod_bie;
        $scope.verificar_bien.org_id = bien.org_id;
        $scope.verificar_bien.interfaz = "consulta_inmuebles";

        localStorage.setItem('update_bien_inmueble', window.btoa(JSON.stringify($scope.verificar_bien)));
        $location.path('bienes_inmuebles');

    };
    
    $scope.verBienInmueble = function(item) {

        var request = crud.crearRequest('ingresos', 1, 'obtener_bien_inmueble');
        request.setData({id_bie_inm: item.cod_bie_inm});

        crud.listar("/controlPatrimonial", request, function (response) {

            if (response.responseSta) {
                $scope.bienInmueble = response.data[0];
                $("#modalMostrarDetallesBienInmueble").modal('show');
            }
        }, function (data) {
            console.info(data);
        });
        
    }
    
    $scope.reporteBienesInmuebles = function (orgId) {

        var request = crud.crearRequest('ingresos', 1, 'reporte_bienes_inmuebles');
        request.setData({org_id: orgId});
        crud.listar("/controlPatrimonial", request, function (data) {
            $scope.dataBase64 = data.data[0].datareporte;
            window.open($scope.dataBase64);
        }, function (data) {
            console.info(data);
        });
    }

}]);