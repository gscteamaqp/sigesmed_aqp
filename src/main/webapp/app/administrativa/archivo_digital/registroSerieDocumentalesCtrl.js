app.controller("registroSerieDocumentalesCtrl",["$scope","NgTableParams","crud","modal", function ($scope,NgTableParams,crud,modal){
      
   $scope.areas = [];
   $scope.unidades_organicas = [];
   $scope.series_documentales = [];
   $scope.serie_documental = {};
   $scope.area={}; // obtiene datos del area a registrar
   $scope.area_id={};
   $scope.serie_select={};
   
   $scope.codigo_area={};
   
   //Datos de la tabla Series Documentales
   var params = {count: 20};
   var setting = { counts: []};
   $scope.tabla_series = new NgTableParams(params, setting);
   
   //Posicion de la Serie a Editar se usa en : editar_serie();
   $scope.pos_serie = {};
   
   $scope.show_modal = false;
   
   //Registramos un Area
   $scope.registrarArea = function(){
       
       modal.mensajeConfirmacion($scope,"Seguro que desea registrar una Nueva Area",function(){
               
               var request = crud.crearRequest('serie_documental',1,'insertarArea');
               request.setData($scope.area);        
               
               crud.insertar("/archivo_digital",request,function(response){
                   modal.mensaje("CONFIRMACION",response.responseMsg);
                   if(response.responseSta){
                       $scope.area = response.data;
                   }
               },function(data){
                   console.info(data);
               });
           });
       
   };
   
      
   //Listamos las Areas
   $scope.listarAreas = function(){
       
       var request = crud.crearRequest('serie_documental',1,'listarArea');
       crud.listar("/archivoDigital",request,function(data){
       $scope.areas = data.data;
       },function(data){
           console.info(data);
       });
       
   };
   $scope.listarUnidadesOrganicas = function(){
       
       $scope.series_documentales = null;
       var request = crud.crearRequest('gestion_area_archivo_central',1,'listar_unidad_organica');
     //  request.setData({tipoTramiteID:tipoTramite.tipoTramiteID,tupa:tipoTramite.tupa});
    //   request.setData({codigo_area:serie_documental.areaID});
       crud.listar("/archivoDigital",request,function(data){
            $scope.unidades_organicas = data.data;
        },function(data){
            console.info(data);
        });
   }
   
   $scope.elegirUnidadOrganica = function(AreaID){
        $scope.series_documentales = null;
       var tipoArea = null;
        for(var i=0;i<$scope.areas.length;i++ )
            if($scope.areas[i].areaID == AreaID){
                tipoArea = $scope.areas[i];
                break;
            }
        if(tipoArea){
            var request = crud.crearRequest('gestion_area_archivo_central',1,'listar_unidad_organica');
            request.setData({codigo_area:tipoArea.areaID});
            crud.listar("/archivoDigital",request,function(data){
                 $scope.unidades_organicas = data.data;
                },function(data){
                    console.info(data);
                });
            
        }
        else{
            $scope.requisitos = [];
            $scope.rutas = [];
        }
       
   }
   $scope.elegirSerieDocumental = function(UniID){
       
       var tipoUniOrg = null;
       for(var i = 0 ;i<$scope.unidades_organicas.length;i++)
           if($scope.unidades_organicas[i].uni_org_id == UniID){
               tipoUniOrg = $scope.unidades_organicas[i];
               break;
           }
       if(tipoUniOrg){
           $scope.serie_documental.uni_org= tipoUniOrg.nom_uni;
           $scope.serie_documental.uni_org_id = UniID;
            var request = crud.crearRequest('gestion_series_documentales',1,'listar_series_documentales');
            request.setData({uni_org_id:tipoUniOrg.uni_org_id});
            crud.listar("/archivoDigital",request,function(data){
                setting.dataset = data.data;
                $scope.series_documentales = data.data;
                },function(data){
                    console.info(data);
                });
       }
      
         
   }
   
   $scope.obtener_serie_actual = function(areID){
       
       
       $scope.serie_documental.cod_serie = "";
       $scope.serie_documental.nom_serie = "";
       $scope.serie_documental.val_serie = "";
       
       var tipoSerie = null;
       for(var i=0 ;i<$scope.areas.length;i++){
           if($scope.areas[i].areaID == areID){
               tipoSerie = $scope.areas[i];
               $scope.serie_documental.abr = tipoSerie.abr;
               $scope.serie_documental.nombre_area = tipoSerie.nombre_area;
               $scope.serie_documental.uni_org = $scope.serie_documental.uni_org;
               break;
           }
       }
       
   }
   $scope.registrar_retencion = function(){
        var request = crud.crearRequest('gestion_series_documentales',1,'registrar_tabla_retencion');
            request.setData($scope.serie_documental);
            crud.insertar("/archivoDigital",request,function(response){
                 modal.mensaje("CONFIRMACION",response.responseMsg);
                 if(response.responseSta){
                    
                    $('#modalTablaRetencion').modal('hide');
                    
                 }
                }
           
            );
        
   }
   
   
   $scope.registrar_serie = function(){
       
       
       var request = crud.crearRequest('gestion_series_documentales',1,'registrar_serie_documental');
       request.setData($scope.serie_documental);
       
       crud.insertar("/archivoDigital",request,function(response){
           modal.mensaje("CONFIRMACION",response.responseMsg);
           if(response.responseSta){
               
           //     $scope.serie_documental.serie_id = response.data.serie_id;
           //     $scope.serie_documental.cod_serie = response.data.ser_doc_cod;
           //     $scope.serie_documental.nom_serie = response.data.nom;
                   $scope.nueva_serie={}; 
  
                  $scope.nueva_serie.serie_id = response.data.serie_id;
                  $scope.nueva_serie.cod_serie = response.data.ser_doc_cod;
                  $scope.nueva_serie.nom_serie = response.data.nom;
                  $scope.nueva_serie.val_serie = $scope.serie_documental.val_serie;
                //insertamos el elemento a la lista
                  $scope.serie_documental.serie_id = $scope.nueva_serie.serie_id;
                  $scope.serie_documental.cod_serie =  $scope.nueva_serie.cod_serie;
                  $scope.serie_documental.nom_serie = $scope.nueva_serie.nom_serie ;
                 
                
                //PENDIENTE DE VERIFICACION (Actualizar la lista de series documentales)
                insertarElemento($scope.series_documentales,$scope.nueva_serie);
                $scope.tabla_series.reload();
                
                
             
                //Limpiamos los campos obtenidos
            //    $scope.serie_documental = {cod_serie:"",nom_serie:"",val_Serie:""};
             
              $('#modalNuevaSerie').modal('hide');
               $scope.registrar_retencion();
               
           }
           
       },function(data){
            console.info(data);
        });
        
        
       
   }
   $scope.preparar_retencion = function(SerieCod){
       var serie = null;
       
       
       for(var i = 0 ;i<$scope.series_documentales.length;i++){
           if($scope.series_documentales[i].cod_serie == SerieCod){
               serie = $scope.series_documentales[i];
               break;
           }
       }
        var request = crud.crearRequest('gestion_series_documentales',1,'obtener_tabla_retencion');
           request.setData({serie_id:serie.serie_id});
            crud.listar("/archivoDigital",request,function(response){
                //modal.mensaje("CONFIRMACION",response.responseMsg);
                 if(response.responseSta){
                     $scope.serie_documental.ag = response.data[0].ag;
                     $scope.serie_documental.ap = response.data[0].ap;
                     $scope.serie_documental.oaa = response.data[0].oaa;
                     
                    $scope.serie_documental.serie_id  = serie.serie_id;
                    $scope.serie_documental.cod_serie = serie.cod_serie;
                    $scope.serie_documental.nom_serie = serie.nom_serie;
                    $scope.serie_documental.val_serie = serie.val_serie;
                     
                     $('#modalTablaRetencion').modal('show');
                 }
                 
                     
                }
            );   
       
    //   $scope.serie_documental.serie_id  = serie.serie_id;
    //   $scope.serie_documental.cod_serie = serie.cod_serie;
    //   $scope.serie_documental.nom_serie = serie.nom_serie;
    //   $scope.serie_documental.val_serie = serie.val_serie;
    //   $('#modalTablaRetencion').modal('show');
   }
   
   $scope.actualizar_retencion = function(){
        var request = crud.crearRequest('gestion_series_documentales',1,'editar_tabla_retencion');
            request.setData($scope.serie_documental);
            crud.actualizar("/archivoDigital",request,function(response){
                 modal.mensaje("CONFIRMACION",response.responseMsg);
                 if(response.responseSta){
                    
                    $('#modalTablaRetencion').modal('hide');
                 }
                }
           
            );
            $scope.serie_documental.ag = "";
            $scope.serie_documental.ap = "";
            $scope.serie_documental.oaa ="";  
    
   }
   
   $scope.preparar_editar_serie = function(SerieCod){
       var serie = null;
       
       for(var i = 0 ;i<$scope.series_documentales.length;i++){
           if($scope.series_documentales[i].cod_serie == SerieCod){
               serie = $scope.series_documentales[i];
               $scope.pos_serie = i;
               break;
           }
       }
       $scope.serie_documental.serie_id  = serie.serie_id;
       $scope.serie_documental.cod_serie = serie.cod_serie;
       $scope.serie_documental.nom_serie = serie.nom_serie;
       $scope.serie_documental.val_serie = serie.val_serie;
       $('#modalEditarSerie').modal('show');
   }
   
   $scope.editar_serie = function(){
       
            var request = crud.crearRequest('gestion_series_documentales',1,'editar_serie_documental');
            request.setData($scope.serie_documental);
            crud.actualizar("/archivoDigital",request,function(response){
                 modal.mensaje("CONFIRMACION",response.responseMsg);
                 if(response.responseSta){
                     
                    setting.dataset[$scope.pos_serie] = $scope.serie_documental;
                    $scope.tabla_series.reload();
                    $('#modalEditarSerie').modal('hide');
                 }
                }
           
            )
    }
   
  $scope.eliminar_serie = function(SerieCod){
      
       var serie = null;
       var num_serie = 0; // numero de serie de la tabla a eliminar
       for(var i = 0 ;i<$scope.series_documentales.length;i++){
           if($scope.series_documentales[i].cod_serie == SerieCod){
               serie = $scope.series_documentales[i];
               nun_serie = i+1;
               break;
           }
       }
       if(serie){
           var id_serie = serie.serie_id;
           
           modal.mensajeConfirmacion($scope,"seguro que desea eliminar este registro",function(){
           var request = crud.crearRequest('gestion_series_documentales',1,'eliminar_serie_documental');
           request.setData({serie_id:id_serie});
           
           crud.eliminar("/archivoDigital",request,
            function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    eliminarElemento(setting.dataset,i);
                    $scope.tabla_series.reload();
                }

            },function(data){
            });
            
            request = crud.crearRequest('gestion_series_documentales',1,'eliminar_tabla_retencion');
                request.setData({serie_id:id_serie});
                crud.eliminar("/archivoDigital",request,
                function(response){
                    modal.mensaje("CONFIRMACION",response.responseMsg);
                    if(response.responseSta){
                        
                    }
                },function(data){
                    });
            });
           
       }
     
  }
  
  $scope.registrar_tabla = function(){
      
      
  }
  
  $scope.limpiar_registro = function(){
      
      $('#modalEditarInventario').modal('hide');
  }
  
  
   //Elegimos el Area Seleccionada
   function elegirArea(areaID){
       var area_temp = null;
       // OJO
   };
   
   $scope.prueba_funcion = function(){
       $scope.show_modals = true;
   }
    
        
        
        
        
}]);
