app.controller("directorioExternoCtrl", ["$rootScope", "$scope", "$http", "NgTableParams", "crud", "modal", function ($rootScope, $scope, $http, NgTableParams, crud, modal) {

        $scope.directorioExterno = [];
        $scope.nuevoDex = {dexNom: "", dexEma: "", dexDir: "", dexTel: ""};
        $scope.dexSel = {};

        //Para las obsiones de reportes, los nombres de los campos sirven para hacer la consulta 
        //a la base de datos
        $scope.checkModel = {
            'dex_nombre': false,
            'dex_ema': false,
            'dex_dir': false,
            'dex_tel': false,
        };

        $scope.checkModel2 = {
            'dex_nombre': ['Nombre', 3],
            'dex_ema': ['Email', 5],
            'dex_dir': ['Direccion', 4],
            'dex_tel': ['Telefono', 3]
        };

        //variables para el reporte
        $scope.checkResults = [];
        $scope.checkWeight = [];
        $scope.checkTitles = [];
        //

        $scope.$watchCollection('checkModel', function () {
            $scope.checkResults = [];
            $scope.checkWeight = [];
            $scope.checkTitles = [];
            angular.forEach($scope.checkModel, function (value, key) {
                if (value) {
                    $scope.checkResults.push(key);
                    $scope.checkTitles.push($scope.checkModel2[key][0]);
                    $scope.checkWeight.push($scope.checkModel2[key][1]);
                }
            });
        })

        var paramsDirExt = {count: 10};
        var settingDirExt = {counts: []};
        $scope.tablaDirExt = new NgTableParams(paramsDirExt, settingDirExt);

        $scope.listarExterno = function () {
            //preparamos un objeto request        
            var request = crud.crearRequest('directorioExterno', 1, 'listarDirectorioExterno');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las funciones de exito y error        
            crud.listar("/directorio", request, function (data) {
                settingDirExt.dataset = data.data;
                iniciarPosiciones(settingDirExt.dataset);
                $scope.tablaDirExt.settings(settingDirExt);
            }, function (data) {
                console.info(data);
            });
        };

        $scope.prepararEditarDirExt = function (dex) {
            $scope.dexSel = JSON.parse(JSON.stringify(dex));
            $('#modalEditar').modal('show');
        };

        $scope.editarDirExt = function () {

            var request = crud.crearRequest('directorioExterno', 1, 'actualizarDirectorioExterno');
            request.setData($scope.dexSel);

            crud.actualizar("/directorio", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    //actualizando
                    settingDirExt.dataset[$scope.dexSel.i] = $scope.dexSel;
                    $scope.tablaDirExt.reload();
                    $('#modalEditar').modal('hide');
                }
            }, function (data) {
                console.info(data);
            });

        };

        $scope.eliminarDirExt = function (i, dexId) {

            modal.mensajeConfirmacion($scope, "Seguro que desea eliminar este registro?", function () {

                var request = crud.crearRequest('directorioExterno', 1, 'eliminarDirectorioExterno');
                request.setData({dexId: dexId});

                crud.eliminar("/directorio", request, function (response) {

                    modal.mensaje("CONFIRMACION", response.responseMsg);
                    if (response.responseSta) {
                        eliminarElemento(settingDirExt.dataset, i);
                        $scope.tablaDirExt.reload();
                    }

                }, function (data) {
                    console.info(data);
                });

            });

        };

        $scope.agregarDirExt = function () {

            var request = crud.crearRequest('directorioExterno', 1, 'insertarDirectorioExterno');
            request.setData($scope.nuevoDex);

            crud.insertar("/directorio", request, function (response) {

                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    //recuperamos las variables que nos envio el servidor
                    $scope.nuevoDex.dexId = response.data.dexId;

                    insertarElemento(settingDirExt.dataset, $scope.nuevoDex);
                    $scope.tablaDirExt.reload();
                    //reiniciamos las variables
                    $scope.nuevoDex = {dexNom: "", dexEma: "", dexDir: "", dexTel: ""};
                    //cerramos la ventana modal
                    $('#modalNuevo').modal('hide');
                }
            }, function (data) {
                console.info(data);
            });

        };

        //Visualizar de archivos en formato PDF
        $scope.reporte = function () {
            
            if($scope.checkResults.length < 3) {
//                console.log($scope.checkResults.length);
                return;
            }
                        
            var objeto = {
                Organizacion: $rootScope.usuMaster.organizacion.nombre,
                Director: $rootScope.usuMaster.usuario.usuarioID,
                OrganizacionId: $rootScope.usuMaster.organizacion.organizacionID
//                Departamento: "Arequipa",
//                Provincia: "Arequipa",
//                Distrito: "Paucarpata"
            };
            var request = crud.crearRequest('trabajador', 1, 'reporteDirectorioExterno');
            request.setData({campos: $scope.checkResults, titulos: $scope.checkTitles, pesos: $scope.checkWeight, traTip: $scope.tipo, orgId: $rootScope.usuMaster.organizacion.organizacionID, objeto: objeto});
            crud.listar("/directorio", request, function (data) {
                //$scope.dataBase64 = data.data[0].datareporte;
                //$scope.dataBase64_2 = data.data[1].datareporte;
                window.open(data.data[0].datareporte);
//            window.open(data.data[0].datareporte2);
                var link = document.createElement("a");
                link.href = data.data[0].datareporte2;
                //set the visibility hidden so it will not effect on your web-layout
                link.style = "visibility:hidden";
                link.download = "reportes.xls";
                //this part will append the anchor tag and remove it after automatic click
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);

            }, function (data) {
                console.info(data);
            });
        };
        //fin
        $scope.modalreporte = function () {
            $scope.checkModel = {
                'dex_nombre': true,
                'dex_ema': true,
                'dex_dir': true,
                'dex_tel': true
            };
            $('#modalReportes').modal('show');
        };

    }]);
