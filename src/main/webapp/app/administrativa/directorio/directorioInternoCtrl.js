app.controller("directorioInternoCtrl",["$rootScope","$scope","$sce","NgTableParams","crud","modal", function ($rootScope,$scope,$sce,NgTableParams,crud,modal){
        
    $scope.content = "";
    $scope.dataBase64 = "";
    
    //Para las opciones de reportes, los nombres de los campos sirven para hacer la consulta 
    //a la base de datos
    $scope.checkModel = {
        'p.dni': false,
        'p.ape_mat': false,
        'p.ape_pat': false,
        'p.nom':false,
        'p.per_dir':false,
        'p.email':false,
        'p.fij':false,
        'tc.crg_tra_nom':false,
        'o.nom':false
    };
    
    $scope.checkModel2 = {        
        'p.dni': {0:'DNI',1:3},
        'p.ape_mat': {0:'Ape. Materno',1:3},
        'p.ape_pat': {0:'Ape. Paterno',1:3},
        'p.nom':{0:'Nombre',1:3},
        'p.per_dir':{0:'Direccion',1:5},
        'p.email':{0:'Email',1:4},
        'p.fij':{0:'Telefono',1:3},
        'tc.crg_tra_nom':{0:'Cargo',1:3},
        'o.nom':{0:'Institucion',1:4}
    };

    //variables para el reporte
    $scope.page = false; //horientacion de la pagina
    $scope.checkResults = [];
    $scope.checkWeight = [];
    $scope.checkTitles = [];
    //
        
    $scope.$watchCollection('checkModel', function () {
        $scope.checkResults = [];
        $scope.checkWeight = [];
        $scope.checkTitles = [];
        angular.forEach($scope.checkModel, function (value, key) {
          if (value) {
            $scope.checkResults.push(key);
            $scope.checkTitles.push($scope.checkModel2[key][0]);
          }
        });        
    })
    //fin
    
    $scope.perId = 0;
    $scope.trabajador = 0;
    
    $scope.tipoParientes = [];
        
    $scope.disabled= true;
    
    $scope.directorioInterno = [];
    $scope.directorioTipo = "";
    $scope.pariente = {
        parId:0,
        perId:0,
        tpaId:0,
        parDni:"",
        parPat:"",
        parMat:"",
        parNom:"",
        parDir:"",
        parTel:"",
        tpaDes:""
    };    
        
    var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
    
    var paramsDirInt = {count: 10};
    var settingDirInt = {counts: []};
    $scope.tablaDirInt = new NgTableParams(paramsDirInt, settingDirInt);
  
    var paramsParientes = {count: 4};
    var settingParientes = {counts: []};
    $scope.tablaParientes = new NgTableParams(paramsParientes, settingParientes);
    
    //variable del tipo de trabajador(directivo, docente,..)
    $scope.tipo = "";   
    
    $scope.listar = function(){        
        var activo = $scope.active;          
        switch(activo){
            case 0:
                $scope.listarInterno("");                   
                break;
            case 1:
                $scope.listarInterno("Di");
                break;
            case 2:
                $scope.listarInterno("Do");
                break;
            case 3:
                $scope.listarInterno("Ad");                
                break;
            case 4:
                $scope.listarPadres(); 
                $scope.tipo = "Pa" ;
                break;
            case 5:
                $scope.listarEstudiantes();
                $scope.tipo = "Es" ;
                break;
        }
        
    }
    
    $scope.listarInterno = function(traTip){
        $scope.tipo = traTip;
        //preparamos un objeto request        
        var request = crud.crearRequest('trabajador',1,'listarTrabajadoresPorOrganizacionyTipo');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        request.setData({orgId:$rootScope.usuMaster.organizacion.organizacionID, traTip:traTip});
        crud.listar("/directorio",request,function(data){
            settingDirInt.dataset = data.data;            
            iniciarPosiciones(settingDirInt.dataset);            
            $scope.tablaDirInt.settings(settingDirInt);
        },function(data){
            console.info(data);
        });
    };
        
    $scope.listarPadres = function(){
        $scope.tipo = "Pa";
        //preparamos un objeto request        
        var request = crud.crearRequest('trabajador',1,'listarPadresPorOrganizacion');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        request.setData({orgId:$rootScope.usuMaster.organizacion.organizacionID});
        crud.listar("/directorio",request,function(data){            
            settingDirInt.dataset = data.data;            
            iniciarPosiciones(settingDirInt.dataset);            
            $scope.tablaDirInt.settings(settingDirInt);
        },function(data){
            console.info(data);
        });
    };
    
    $scope.listarEstudiantes = function(){
        $scope.tipo = "Es";
        //preparamos un objeto request        
        var request = crud.crearRequest('trabajador',1,'listarEstudiantesPorOrganizacion');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        request.setData({orgId:$rootScope.usuMaster.organizacion.organizacionID});
        crud.listar("/directorio",request,function(data){            
            settingDirInt.dataset = data.data;            
            iniciarPosiciones(settingDirInt.dataset);            
            $scope.tablaDirInt.settings(settingDirInt);
        },function(data){
            console.info(data);
        });
    };    
        
    $scope.eliminarPariente = function(i,r, modo){
        
        //si estamso cancelando la edicion
        if(r.edi){
            r.edi = false;
            delete r.copia;
        }
        //si queremos eliminar el elemento
        else{

            modal.mensajeConfirmacion($scope,"Seguro que desea eliminar este registro?",function(){
            
                var request = crud.crearRequest('parientes',1,'eliminarPariente');
                request.setData({parId:r.parId, perId:$scope.trabajador});

                crud.actualizar("/directorio",request,function(response){

                    modal.mensaje("CONFIRMACION",response.responseMsg);
                    if(response.responseSta){
                        eliminarElemento(settingParientes.dataset,i);
                        $scope.tablaParientes.reload();
                    }                    
                },function(data){
                    console.info(data);
                });            
            });             
        }                
        //        
    };
    
    $scope.prepararEditar = function(i,traId, perId){
        //$scope.funcionSel = JSON.parse(JSON.stringify(traId));
        //$scope.funcionSel.i = i;             
        $scope.disabled= true;        
        $scope.trabajador = perId;
        $scope.trabajador.i = i;
        
        //preparamos un objeto request        
        var request = crud.crearRequest('parientes',1,'listarParientesPorTrabajador');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        request.setData({traId:traId});
        crud.listar("/directorio",request,function(data){
            settingParientes.dataset = data.data;
            //asignando la posicion en el arreglo a cada objeto
            iniciarPosiciones(settingParientes.dataset);            
            
//            settingParientes.dataset.forEach(function(item,index){
//                item.edi = false;
//            });            
            $scope.tablaParientes.settings(settingParientes);                        
        },function(data){
            console.info(data);
        });
        
        $scope.reiniciarModal();      
        $('#modalEditar').modal('show');     
    };    
    
    $scope.editarPariente = function(i,r,modo){
        //si estamso editando
        if(r.edi){
            //se actualiza            
            modal.mensajeConfirmacion($scope,"Seguro que desea actualizar este registro",function(){
            
                var request = crud.crearRequest('parientes',1,'actualizarPariente');
                r.copia.perId = $scope.trabajador;
                request.setData(r.copia);

                crud.actualizar("/directorio",request,function(response){

                    modal.mensaje("CONFIRMACION",response.responseMsg);
                    if(response.responseSta){
                        console.log("se actualizo");
                        //eliminarElemento(settingParientes.dataset,i);
                        //$scope.tablaParientes.reload();
                        r.i = r.copia.i;
                        r.parDni = r.copia.parDni;
                        r.parPat = r.copia.parPat;
                        r.parMat = r.copia.parMat;
                        r.parNom = r.copia.parNom;
                        r.parDir = r.copia.parDir;
                        r.parTel = r.copia.parTel;
                        r.tpaDes = r.copia.tpaDes;
                        r.tpaId = r.copia.tpaId;
                        r.parId = r.copia.parId;
                        delete r.copia;
                    }
                    
                },function(data){
                    console.info(data);
                    return;
                });            
            });                  
            r.edi = false; 
        }
        //si queremos editar
        else{
            r.copia = JSON.parse(JSON.stringify(r));            
            r.edi = true;         
        }
    };
        
    $scope.agregarPariente = function(pariente){

        var request = crud.crearRequest('parientes',1,'insertarPariente');
        
        pariente.perId = $scope.trabajador;
        request.setData(pariente);
        
        crud.insertar("/directorio",request,function(response){
            
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                //recuperamos las variables que nos envio el servidor
                objeto = {
                    parId:response.data.parId,                    
                    tpaId:$scope.pariente.tpaId,
                    parDni:$scope.pariente.parDni,
                    parPat:$scope.pariente.parPat,
                    parMat:$scope.pariente.parMat,
                    parNom:$scope.pariente.parNom,
                    parDir:$scope.pariente.parDir,
                    parTel:$scope.pariente.parTel,
                    tpaDes:$scope.tipoParientes[$scope.pariente.tpaId - 1].tpaDes
                };
                insertarElemento(settingParientes.dataset,objeto);
                $scope.tablaParientes.reload();
                //reiniciamos las variables
                $scope.pariente = {
                    parId:0,
                    perId:0,
                    tpaId:0,
                    parDni:"",
                    parPat:"",
                    parMat:"",
                    parNom:"",
                    parDir:"",
                    parTel:"",
                    tpaDes:""
                };                 
                $scope.disabled= true;                
            }            
        },function(data){
            console.info(data);
        });        
    };
    
    $scope.verificarDni = function(dni){        
        //verificar el dni ingresado, para saber si existe en la tabla persona        
        var request = crud.crearRequest('persona',1,'buscarPersonaxDni');        
        request.setData({perDni:dni});
        crud.listar("/directorio",request,function(data){            
            // si existe una persona con ese DNI en la tabla Persona
            if(Object.keys(data.data).length != 0){

                $scope.pariente.parId = data.data.parId;                    
                $scope.pariente.parDni = dni;
                $scope.pariente.parPat = data.data.parPat;
                $scope.pariente.parMat = data.data.parMat;
                $scope.pariente.parNom = data.data.parNom;
                $scope.pariente.parDir = data.data.parDir;
                $scope.pariente.parTel = data.data.parTel;                
            }                        
        },function(data){
            console.info(data);
        });        
        //
        //habilitamos los campos bloqueados        
        $scope.disabled = false;        
    };
    
    listarTipoParientes();
    
    function listarTipoParientes(){
        //preparamos un objeto request
        var request = crud.crearRequest('tipoPariente',1,'listarTipos');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/directorio",request,function(data){
            $scope.tipoParientes = data.data;
        },function(data){
            console.info(data);
        });
    };
        
    //Visualizar de archivos en formato PDF
    $scope.reporte = function () {  
        
        $scope.checkWeight = [];
        for(var k=0;k<$scope.checkResults.length;++k){
            $scope.checkWeight.push($scope.checkModel2[$scope.checkResults[k]][1]);
        }
        
        var reporte = '';
        
        //        if($scope.tipo == 'Pa') reporte = 'reporteApoderados';
        //        else if ($scope.tipo == 'Es')   reporte = 'reporteEstudiantes';
//        else  
//        console.log( $scope.checkResults);
//        console.log( $scope.checkTitles);
//        console.log( $scope.checkWeight);
//        console.log( $scope.tipo);
        if($scope.tipo === 'Pa' || $scope.tipo === 'Es') {
            var index = $scope.checkResults.indexOf('tc.crg_tra_nom');
            if (index > -1) {
                $scope.checkResults.splice(index, 1);
                $scope.checkTitles.splice(index, 1);
                $scope.checkWeight.splice(index, 1);
            }
        }
        
        if($scope.checkResults.length < 3) {
//            console.log($scope.checkResults.length);
            return;
        }
            
        reporte = 'reporte';  
        var objeto ={
            Organizacion:"II.EE Tupac Amaru II ILO",
            Director:"Jose Manuel Ramos Rondon",
            Nivel:"Primaria - Secundaria",
            Provincia:"ILO",
            Distrito:"Alto Mar"
        };

        var request = crud.crearRequest('trabajador',1,reporte);   
        request.setData({campos:$scope.checkResults, titulos:$scope.checkTitles, pesos:$scope.checkWeight, traTip:$scope.tipo, orgId:$rootScope.usuMaster.organizacion.organizacionID, objeto:objeto, page:$scope.page});
        crud.listar("/directorio",request,function(data){ 
                
            $scope.dataBase64 = data.data[0].datareporte;
            window.open($scope.dataBase64);
            var link = document.createElement("a");    
            link.href = data.data[0].datareporte2;    
            //set the visibility hidden so it will not effect on your web-layout
            link.style = "visibility:hidden";
            link.download = "reportes.xls";    
            //this part will append the anchor tag and remove it after automatic click
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        },function(data){
            console.info(data);
        });            
    };
    //fin
    $scope.modalreporte = function(){ 
        
        $scope.checkModel = {
            'p.dni': true,
            'p.ape_mat': true,
            'p.ape_pat': true,
            'p.nom':true,
            'p.per_dir':true,
            'p.email':true,
            'p.fij':true,
            'tc.crg_tra_nom':true,
            'o.nom':true
        };
        
        if($scope.tipo === "Pa" || $scope.tipo === "Es")  $scope.ocultar = true;
        else $scope.ocultar = false;

        $('#modalReportes').modal('show');        
    };
    
    $scope.reiniciarModal = function(){ 
        $scope.pariente = {
                        parId:0,
                        perId:0,
                        tpaId:0,
                        parDni:"",
                        parPat:"",
                        parMat:"",
                        parNom:"",
                        parDir:"",
                        parTel:"",
                        tpaDes:""
        };  
    }
    
    $scope.cerrarModalPariente = function(){
        $scope.listar();
        $('#modalEditar').modal('show');                
    }
    
}]);
