app.controller("reporte_bienesCtrl", ["$scope", "NgTableParams", "crud", "modal", function ($scope, NgTableParams, crud, modal) {

    $scope.reporte = {
        org_id: "",
        fec_ini: "",
        fec_fin: "",
        fec_ini_md: new Date,
        fec_cie_md: new Date,
        tip_ane: 0
    };
    
    $scope.maxDateInicio;
    $scope.minDateCierre = new Date();

    $scope.anexos = {};/*Lista de Anexos*/

    $scope.generarReporte = function (orgId) {
        
        $scope.reporte.fec_ini = convertirFecha($scope.reporte.fec_ini_md);
        $scope.reporte.fec_fin = convertirFecha($scope.reporte.fec_cie_md);

        $scope.reporte.org_id = orgId;

        /* Verificamos que todos los campos esten llenados */
        var accept = true;
        for (var atrib in $scope.reporte) {
            if ($scope.reporte[atrib] == "") {
                console.log(atrib);
                accept = false;
                break;
            }
        }
        if (accept == true) { /*Todos los campos estan llenados correctamente*/

            var request = crud.crearRequest('ingresos', 1, 'reporte_bienes');
            request.setData($scope.reporte);
            
            crud.listar("/controlPatrimonial", request, function (response) {
                
                if (response.responseSta){
                    verDocumentoPestana(response.data[0].datareporte);
                }
                
            }, function (data) {
                console.info(data);
            });
        } else {
            modal.mensaje("ERROR", "Verifique que todos los campos esten llenados");
        }

    }

    $scope.listar_anexos = function () {
        var request = crud.crearRequest('configuracion_patrimonial', 1, 'listar_anexos');
        crud.listar("/controlPatrimonial", request, function (data) {
            $scope.anexos = data.data;
        }, function (data) {
            console.info(data);
        });
    }
    
    $scope.seleccion_fecha_inicial = function () {

        $scope.minDateCierre = $scope.reporte.fec_ini_md;
    }

    $scope.seleccion_fecha_cierre = function () {

        $scope.maxDateInicio = $scope.reporte.fec_cie_md;

    }

}]);

