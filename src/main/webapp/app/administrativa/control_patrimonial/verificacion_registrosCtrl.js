app.controller("verificacion_registrosCtrl", ["$rootScope", "$scope", "NgTableParams", "$location", "crud", "modal", function ($rootScope, $scope, NgTableParams, $location, crud, modal) {

//    $scope.bienMueble = {
//        nro_mot: "",
//        obs: "OBSERVACION",
//        col: "a",
//        valor_cont: 1,
//        mod: "a",
//        cod_int: "",
//        dim: 1,
//        val_cont_id: 28,
//        cant: 1,
//        estado_bie: "B",
//        cond: 0,
//        tip_mov_ing: 1,
//        fec_res: "2017-01-01 00:00:00",
//        cod_ba_bie: "",
//        tip_mov_ing_des: "INGRESO POR COMPRA",
//        des_bie: "ABCDE",
//        act_dep: "A",
//        tip: "a",
//        ser: "a",
//        raza: "a",
//        num_res: "ASD89",
//        rut_imag_2: "archivos/scp/DOC_INV_BMUEBLE_21_761b.pdf",
//        an_id: 1,
//        rut_imag_1: "archivos/scp/MU_modulo_administrador.pdf",
//        cod_cuenta: "5",
//        fec_reg: "2017-09-29 00:00:00",
//        edad: 1,
//        id_bien: 28,
//        nro_pla: "",
//        amb_id: 1,
//        mov_ing_id: 1,
//        doc_ref: "archivos/scp/",
//        org_id: 6,
//        marc: "a",
//        nro_cha: "",
//        cat_bie_id: 5,
//        rut_doc_bie: "",
//        con_pat_id: 1,
//        rut_aut_img: "archivos/scp/Screenshot from 2017-09-26 16:59:17.png",
//        file_name: "2017-10-12 15:03:58-Trabajo Biometria.pdf",
//        file_url: "archivos/control_patrimonial/",
//        cb_cod: "04640950",
//        cb_den_bie: "TETERA ELECTRICA",
//        an_des: "MOBILIARIO"
//    };  
    
    var ORG_ID = $rootScope.usuMaster.organizacion.organizacionID;
        
    $scope.bienMueble = {
        nro_mot: "",
        obs: "",
        col: "",
        valor_cont: 0,
        mod: "",
        cod_int: "",
        dim: 0,
        val_cont_id: 0,
        cant: 0,
        estado_bie: "",
        cond: 0,
        cond_nombre: "",
        tip_mov_ing: 0,
        fec_res: "",
        cod_ba_bie: "",
        tip_mov_ing_des: "",
        des_bie: "",
        act_dep: "",
        tip: "",
        ser: "",
        raza: "",
        num_res: "",
        rut_imag_2: "",
        rut_imag_2_url: "",
        rut_imag_2_name: "",
        an_id: 0,
        rut_imag_1: "",
        rut_imag_1_url: "",
        rut_imag_1_name: "",
        cod_cuenta: "",
        fec_reg: "",
        edad: 0,
        id_bien: 0,
        nro_pla: "",
        amb_id: 0,
        mov_ing_id: 0,
        doc_ref: "",
        doc_ref_url: "",
        doc_ref_name: "",
        org_id: 0,
        marc: "",
        nro_cha: "",
        cat_bie_id: 0,
        rut_doc_bie: "",
        con_pat_id: 0,
        rut_aut_img: "",
        rut_aut_img_url: "",
        rut_aut_img_name: "",
        file_name: "",
        file_url: "",
        cb_cod: "",
        cb_den_bie: "",
        an_des: ""
    };  
    
    $scope.cant_bienes = {
        bienes_alta: 0,
        bienes_baja: 0,
        bienes_inventario: 0
    };
    
    /*Tipo de Validacion de Registro Bien (Alta,Baja,Salida)*/
    $scope.tipo_validacion = {
        id: 0,
        causal_id: 0
    };
    
    $scope.validacion = [
        {id: 1, nombre: "ALTA DE BIENES"},
        {id: 2, nombre: "BAJA DE BIENES"},
        {id: 3, nombre: "SALIDA DE BIENES"}
    ];

    /*Control Patrimonial Actual*/
    $scope.cp_actual;
    
    $scope.bienActual = [];
    
    $scope.organizaciones = [];

    /*TABLA BIENES (GENERALES,ALTA,BAJA)*/
    var params = {count: 20};
    var setting = {counts: []};
    $scope.tabla_bienes = new NgTableParams(params, setting);


    /*TABLA DE BIENES DE ALTA*/
    var params_a = {count: 10};
    var setting_a = {counts: []};
    $scope.tabla_altas = new NgTableParams(params_a, setting_a);


    /*TABLA DE BIENES DE BAJA*/
    var params_b = {count: 10};
    var setting_b = {counts: []};
    $scope.tabla_bajas = new NgTableParams(params_b, setting_b);
    
    /*TABLA DE BIENES DE BAJA*/
    var params_s = {count: 10};
    var setting_s = {counts: []};
    $scope.tabla_salidas = new NgTableParams(params_s, setting_s);


    /*Mostrar todos los bienes*/
    $scope.mostrar_bienes_generales = function (orgId) {             
        
        $rootScope.showLoading();       
        
        var request = crud.crearRequest('ingresos', 1, 'listar_bienes_muebles');
        request.setData({org_id: orgId});
        crud.listar("/controlPatrimonial", request, function (data) {
            if (data.data) {
                //console.log(data.data, "BIENES GENERALES");
                setting.dataset = data.data;
                iniciarPosiciones(setting.dataset);
                $scope.tabla_bienes.settings(setting);
                $scope.tabla_bienes.reload();
                
                $rootScope.hideLoading();                
            }
        }, function (data) {
            console.info(data);
        });
    };

    $scope.mostrar_bienes_alta = function (orgId) {
        var request = crud.crearRequest('ingresos', 1, 'listar_altas');
        request.setData({org_id: orgId});
        crud.listar("/controlPatrimonial", request, function (data) {
            if (data.data) {
                $scope.cant_bienes.bienes_alta = data.data.length;
                setting_a.dataset = data.data;
                //console.log(data.data, "altas");
                iniciarPosiciones(setting_a.dataset);
                $scope.tabla_altas.settings(setting_a);
                $scope.tabla_altas.reload();
            }
        }, function (data) {
            console.info(data);
        });

    };

    $scope.mostrar_bienes_baja = function (orgId) {
        var request = crud.crearRequest('ingresos', 1, 'listar_bajas');
        request.setData({org_id: orgId});
        //console.log(request);
        crud.listar("/controlPatrimonial", request, function (data) {
            if (data.data) {
                $scope.cant_bienes.bienes_baja = data.data.length;
                setting_b.dataset = data.data;
                //console.log(data.data, "bajas");
                iniciarPosiciones(setting_b.dataset);
                $scope.tabla_bajas.settings(setting_b);
                $scope.tabla_bajas.reload();
            }
        }, function (data) {
            console.info(data);
        });

    };
    
    $scope.mostrar_bienes_salida = function (orgId) {
        var request = crud.crearRequest('salidas', 1, 'listar_salidas');
        request.setData({org_id: orgId});
        crud.listar("/controlPatrimonial", request, function (response) {
            if (response.responseSta) {
                
                $scope.cant_bienes.bienes_salida = response.data.length;
                setting_s.dataset = response.data;                
                iniciarPosiciones(setting_s.dataset);
                $scope.tabla_salidas.settings(setting_s);
                $scope.tabla_salidas.reload();                
            }
        }, function (data) {
            console.info(data);
        });

    };
     
    $scope.listar_causal = function () {
        
        if ($scope.tipo_validacion.id == 1) { /*Altas*/

            var request = crud.crearRequest('ingresos', 1, 'listar_causal_alta');
            crud.listar("/controlPatrimonial", request, function (data) {
                
                if (data.responseSta) {
                    $scope.tipo_causal = data.data;
                }

            }, function (data) {
                console.info(data);
            });

        } else if ($scope.tipo_validacion.id == 2) { /*Bajas*/

            var request = crud.crearRequest('ingresos', 1, 'listar_causal_baja');
            crud.listar("/controlPatrimonial", request, function (data) {
                
                if (data.responseSta) {
                    $scope.tipo_causal = data.data;
                }

            }, function (data) {
                console.info(data);
            });


        } else if ($scope.tipo_validacion.id == 3) { /*Salidas*/
            
            var request = crud.crearRequest('salidas', 1, 'listar_causal_salida');
            crud.listar("/controlPatrimonial", request, function (data) {
                if (data.responseSta) {
                    $scope.tipo_causal = data.data;
                }                

            }, function (data) {
                console.info(data);
            });
            
            var request = crud.crearRequest('salidas', 1, 'listar_ie');
            request.setData({org_id: ORG_ID});            
            crud.listar("/controlPatrimonial", request, function (data) {
                if (data.responseSta) {
                    
                    $scope.organizaciones = data.data;
                    
                }
            }, function (data) {
                console.info(data);
            });
            
        }
    }
    
    $scope.bien_mueble = {
        id_bien: 0,
        org_id: 0,
        org_id_des: 0,
        usu_mod: 0,
        causal_id: 0,
        mov_ing_id: 0,
        verificar: ""
    }
    /* Valida la alta o baja del bien*/
    $scope.validarBienMueble = function (orgId, user) {
        
        $scope.bien_mueble.org_id = orgId;
        $scope.bien_mueble.usu_mod = user;
        $scope.bien_mueble.causal_id = $scope.tipo_validacion.causal_id;
        $scope.bien_mueble.usu_mod = user;
        
        //console.log($scope.bienActual);
        //return;

        if ($scope.tipo_validacion.causal_id == 0 || $scope.tipo_validacion.id == 0) {
            modal.mensaje("ERROR", "Debe seleccionar una opcion");
            return;
        } else {
            if ($scope.tipo_validacion.id == 1) { /*REGISTRAMOS LAS ALTAS*/                              
                
                modal.mensajeConfirmacion($scope, "Seguro que desea dar de Alta al Bien Mueble?", function () {
                    var request = crud.crearRequest('ingresos', 1, 'registrar_altas');
                    request.setData($scope.bien_mueble);
                    
                    crud.insertar("/controlPatrimonial", request, function (response) {
                        modal.mensaje("CONFIRMACION", response.responseMsg);
                        if (response.responseSta) {
                            $scope.bienActual.verificar = 'A';
                            $scope.mostrar_bienes_alta(orgId);
                            $scope.mostrar_bienes_baja(orgId);
                            $("#modalValidarBien").modal('hide');
                        }
                    }, function (data) {
                        console.info(data);
                    });
                });
            }
            if ($scope.tipo_validacion.id == 2) { /*REGISTRAMOS LAS BAJAS*/               
                                
                modal.mensajeConfirmacion($scope, "Seguro que desea dar de Baja el Bien Mueble?", function () {
                    var request = crud.crearRequest('salidas', 1, 'registrar_baja');
                    request.setData($scope.bien_mueble);
                    crud.insertar("/controlPatrimonial", request, function (response) {
                        modal.mensaje("CONFIRMACION", response.responseMsg);
                        if (response.responseSta) {
                            $scope.bienActual.verificar = 'B';
                            $scope.mostrar_bienes_baja(orgId);
                            $scope.mostrar_bienes_alta(orgId);
                            $("#modalValidarBien").modal('hide');
                        }
                    }, function (data) {
                        console.info(data);
                    });
                });

            }
            if ($scope.tipo_validacion.id == 3) { /*REGISTRAMOS LAS SALIDAS*/
                
                if ($scope.bien_mueble.org_id_des == 0) {
                    modal.mensaje("ERROR", "Debe seleccionar la organización de destino");
                    return;
                }
                
                modal.mensajeConfirmacion($scope, "Seguro que desea dar de Salida el Bien Mueble?", function () {
                    var request = crud.crearRequest('salidas', 1, 'registrar_salida');
                    
                    request.setData($scope.bien_mueble);
                    //return;
                    crud.insertar("/controlPatrimonial", request, function (response) {
                        modal.mensaje("CONFIRMACION", response.responseMsg);
                        if (response.responseSta) {
                            var index = $scope.bienActual.i;
                            eliminarElemento(setting.dataset, index);
                            iniciarPosiciones(setting.dataset);
                            $scope.tabla_bienes.settings(setting);
                            $scope.tabla_bienes.reload();
                            $scope.mostrar_bienes_salida(orgId);
                            $scope.mostrar_bienes_alta(orgId);
                            $("#modalValidarBien").modal('hide');
                        }
                    }, function (data) {
                        console.info(data);
                    });
                });
                
            }
            
        }
    }    
    
    $scope.editarBienMueble = function(bien, userId) {
        
        //localStorage.setItem("tabla_inventario", JSON.stringify($scope.tabla_inventario_inicial.data));
        localStorage.setItem("bien_mueble", JSON.stringify(bien));
        localStorage.setItem("prev_page", "/verificacion_registros");
        $location.path( "/registro_bienes" );
    }

    $scope.elimimarBienMueble = function (Bien) {

        modal.mensajeConfirmacionAuth($scope, "Seguro que desea eliminar este Bien Mueble?", function () {
            var request = crud.crearRequest('ingresos', 1, 'eliminar_bien_mueble');
            request.setData({cod_bie: Bien.cod_bie});
            crud.eliminar("/controlPatrimonial", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    eliminarElemento(setting.dataset, Bien.i);
                    iniciarPosiciones(setting.dataset);
                    $scope.tabla_bienes.settings(setting);
                    $scope.tabla_bienes.reload();
                }
            }, function (data) {
                console.info(data);
            });
        });
    }


    $scope.verBienMueble = function (item) {

        var request = crud.crearRequest('ingresos', 1, 'obtener_bien');
        request.setData({id_bien: item.cod_bie});

        crud.listar("/controlPatrimonial", request, function (response) {
            
            if (response.responseSta) {
                //console.log(response.data[0]);
                $scope.bienMueble = response.data[0];
                $("#modalMostrarDetallesBien").modal('show');
            }
        }, function (data) {
            console.info(data);
        });
    }


    $scope.mostrarModalValidarBienMueble = function (item) {
        
        $scope.bienActual = item; //Para almacenar todo el item de la fila de la tabla
        
        $scope.bien_mueble.id_bien = item.cod_bie;
        $scope.bien_mueble.mov_ing_id = item.mov_ing_id;
        $scope.bien_mueble.verificar = item.verificar;
        
        if (item.verificar == 'A') {
            $scope.validacion = [
                {id: 2, nombre: "BAJA DE BIENES"},
                {id: 3, nombre: "SALIDA DE BIENES"}
            ];
       
        } else if (item.verificar == 'B') {
            $scope.validacion = [
                {id: 1, nombre: "ALTA DE BIENES"}                
            ];
        
        } else {
            $scope.validacion = [
                {id: 1, nombre: "ALTA DE BIENES"},
                {id: 2, nombre: "BAJA DE BIENES"}
                //{id: 3, nombre: "SALIDA DE BIENES"}
            ];
        }
        
        $scope.tipo_validacion = {
            id: "",
            causal_id: ""
        };
        
        $scope.tipo_causal = [];
        
        $scope.bien_mueble.org_id_des = "";
        
        $("#modalValidarBien").modal('show');
    }


}]);
