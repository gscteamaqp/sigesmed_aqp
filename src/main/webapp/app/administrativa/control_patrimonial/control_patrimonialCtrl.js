app.controller("control_patrimonialCtrl", ["$scope", "$filter", "NgTableParams", "crud", "modal",  function ($scope, $filter, NgTableParams, crud, modal) {

    $scope.searchText;
    $scope.selectedWork;
    // $scope.trabajadores = $scope.listaTrabajadores();

    /*Tabla Control Patrimonial para una IE*/
    var params = {count: 20};
    var setting = {counts: []};
    $scope.tabla_control_patrimonial = new NgTableParams(params, setting);

    $scope.estados = [
        {
            id: 73,
            nom: 'Inactivo'
        },
        {
            id: 65,
            nom: 'Activo'
        }  
    ];
    $scope.minDateInicio = new Date();
    $scope.maxDateInicio;
    $scope.minDateCierre = new Date();

    $scope.nueva_configuracion = {
        id_cp: 0,
        org_id: 0,
        per_id: null,
        per_res: "",
        fec_ini: "",
        fec_cie: "",
        fec_ini_md: new Date,
        fec_cie_md: new Date,
        obs: "",
        documentoAdj: {nombreArchivo: "", archivo: {}, url: ""},
        usu_mod: 0,
        estado: 0
    };

    $scope.listaTrabajadores = function (orgId) {

        var request = crud.crearRequest('configuracionControl', 1, 'listarTrabajadorUsuario');
        request.setData({organizacionID: orgId});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
        crud.listar("/controlPersonal", request, function (response) {
            if (response.responseSta)
            {
                $scope.trabajadores = [];
                angular.forEach(response.data, function (value, key) {
                    $scope.trabajadores.push({
                        per_id: value.personaID,
                        display: value.datos,
                        value: angular.lowercase(value.datos)
                    });
                });
            }

        }, function (data) {
            console.info(data);
        });
    }


    $scope.mostrar_control_patrimonial = function (orgId) {
        var request = crud.crearRequest('configuracion_patrimonial', 1, 'listar_control_patrimonial');
        request.setData({org_id: orgId});
        crud.listar("/controlPatrimonial", request, function (data) {
            if (data.data) {
                setting.dataset = data.data;
                iniciarPosiciones(setting.dataset);
                $scope.tabla_control_patrimonial.settings(setting);
                $scope.tabla_control_patrimonial.reload();

            }
        }, function (data) {
            console.info(data);
        });
    }


    $scope.nuevo_control_patrimonial = function (orgId, user) {

        $scope.nueva_configuracion.org_id = orgId;
        $scope.nueva_configuracion.usu_mod = user.ID;
        $scope.nueva_configuracion.fec_ini = convertirFecha($scope.nueva_configuracion.fec_ini_md);
        $scope.nueva_configuracion.fec_cie = convertirFecha($scope.nueva_configuracion.fec_cie_md);

        /*Verificamos los campos llenados correctamente*/
        var accept = true;

        for (var atrib in $scope.nueva_configuracion) {

            if ($scope.nueva_configuracion[atrib] == "" || $scope.nueva_configuracion[atrib] == null) {

                if (atrib == "cp_id") {
                    continue;
                }
                accept = false;
                break;
            }
        }
        if (accept == true) { /*Todos los campos estan llenados correctamente*/

            if ($scope.nueva_configuracion.cp_id != 0) {                   

                $scope.nueva_configuracion.estado = $scope.nueva_configuracion.estado.id;

                modal.mensajeConfirmacion($scope, "Seguro que desea Actualizar el Control Patrimonial  ?", function () {

                    var request = crud.crearRequest('configuracion_patrimonial', 1, 'editar_control_patrimonial');
                    request.setData($scope.nueva_configuracion);
                    console.log(request);
                    //return;
                    crud.actualizar("/controlPatrimonial", request, function (response) {
                        modal.mensaje("CONFIRMACION", response.responseMsg);
                        if (response.responseSta) {                               

                            $scope.mostrar_control_patrimonial(orgId);

                            $scope.limpiar_campos_control();
                            $('#modalnuevocontrol').modal('hide');
                        }
                    }, function (data) {
                        console.info(data);
                    });
                });
            } else {
                modal.mensajeConfirmacion($scope, "Seguro que desea Registrar el Control Patrimonial  ?", function () {

                    var request = crud.crearRequest('configuracion_patrimonial', 1, 'registrar_control_patrimonial');
                    request.setData($scope.nueva_configuracion);
                    console.log(request);
                    //return;
                    crud.insertar("/controlPatrimonial", request, function (response) {
                        modal.mensaje("CONFIRMACION", response.responseMsg);
                        if (response.responseSta) {
                            $('#modalnuevocontrol').modal('hide');
                            /*var nuevo_control = $scope.nueva_configuracion;
                            nuevo_control.cp_id = response.data.cp_id;
                            nuevo_control.fec_ini = $scope.date_to_string($scope.nueva_configuracion.fec_ini_md);
                            nuevo_control.fec_cie = $scope.date_to_string($scope.nueva_configuracion.fec_cie_md);
                            nuevo_control.estado = 69; //Por defecto inactivo
                            insertarElemento(setting.dataset, nuevo_control);
                            console.log(setting.dataset);
                            $scope.tabla_control_patrimonial.reload();*/
                            $scope.mostrar_control_patrimonial(orgId);
                            $scope.limpiar_campos_control();

                        }
                    }, function (data) {
                        console.info(data);
                    });
                });

            }
        } else {
            modal.mensaje("ERROR!!", "Verifique que todos los campos esten llenados");
        }

    }

    $scope.editarControlPatrimonial = function (control) {
        //console.log($scope.tabla_control_patrimonial.data);            
        $scope.nueva_configuracion.cp_id = control.cp_id;
        $scope.nueva_configuracion.per_res = control.per_res;
        fec_ini = new Date(control.fec_ini.replace(/-/g, '\/'));
        fec_cie = new Date(control.fec_cie.replace(/-/g, '\/'));
        $scope.nueva_configuracion.fec_ini_md = fec_ini;
        $scope.nueva_configuracion.fec_cie_md = fec_cie;
        $scope.nueva_configuracion.obs = control.obs;
        $scope.nueva_configuracion.id_cp = control.cp_id;
        $scope.nueva_configuracion.i = control.i;
        $scope.nueva_configuracion.documentoAdj.nombreArchivo = control.file_name;

        var est = $filter('filter')($scope.estados, { id: control.estado })[0];

        $scope.nueva_configuracion.estado = est;

        $scope.selectedWork = { per_id: control.cp_id, value: control.per_res, display: control.per_res };

        $('#modalnuevocontrol').modal('show');

    };

    $scope.eliminarControlPatrimonial = function (control) {            

        modal.mensajeConfirmacionAuth($scope, "Esta seguro que desea eliminar?", function () {

            var request = crud.crearRequest('configuracion_patrimonial', 1, 'eliminar_control_patrimonial');
            request.setData(control);

            crud.eliminar("/controlPatrimonial", request, function (data) {
                modal.mensaje("CONFIRMACION", data.responseMsg);
                if (data.responseSta) {
                    eliminarElemento(setting.dataset, control.i);
                    $scope.tabla_control_patrimonial.reload();
                }
            }, function (data) {
                console.info(data);
            });
        });
        // console.log(control);
    }

    $scope.limpiar_campos_control = function () {

        $scope.selectedWork = null;

        $scope.nueva_configuracion = {
            cp_id: 0,
            org_id: 0,
            per_id: null,
            per_res: "",
            fec_ini: "",
            fec_cie: "",
            fec_ini_md: new Date,
            fec_cie_md: new Date,
            obs: "",
            documentoAdj: {nombreArchivo: "", archivo: {}, url: ""},
            usu_mod: 0,
            estado: 0
        };

        $scope.nueva_configuracion.estado = $scope.estados[1];
    }

    $scope.seleccion_fecha_inicial = function () {

        $scope.minDateCierre = $scope.nueva_configuracion.fec_ini_md;
        $scope.nueva_configuracion.fec_cie_md = $scope.nueva_configuracion.fec_ini_md;
    }

    $scope.seleccion_fecha_cierre = function () {

        $scope.maxDateInicio = $scope.nueva_configuracion.fec_cie_md;

    }

    $scope.date_to_string = function (myDate) {
        // var today = myDate;
        var dd = myDate.getDate();
        var mm = myDate.getMonth() + 1; //January is 0!
        var yyyy = myDate.getFullYear();

        if (dd < 10) {
            dd = '0' + dd;
        }

        if (mm < 10) {
            mm = '0' + mm;
        }

        myDate = yyyy + '-' + mm + '-' + dd;

        return myDate;
        //console.log("fecha inicial cambio a: ", myDate);
    }

    $scope.querySearch = function (query) {
        var results = query ? $scope.trabajadores.filter($scope.createFilterFor(query)) : $scope.trabajadores,
                deferred;
        return results;
    }

    $scope.createFilterFor = function (query) {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(state) {
            // console.log(state);
            // console.log(state.value.indexOf(query));
            return (state.value.indexOf(lowercaseQuery) !== -1)
        };

    }

    $scope.seleccionTrabajador = function () {

        if ($scope.selectedWork == null) {
            $scope.nueva_configuracion.per_res = "";

        } else {

            $scope.nueva_configuracion.per_id = $scope.selectedWork.per_id;
            $scope.nueva_configuracion.per_res = $scope.selectedWork.display;
            //console.log($scope.selectedWork);
            //console.log($scope.nueva_configuracion);
        }

    }
}]);
