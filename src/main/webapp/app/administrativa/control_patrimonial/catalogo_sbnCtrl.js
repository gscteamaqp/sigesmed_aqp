app.controller("catalogo_sbnCtrl", ["$rootScope", "$scope", "$filter","NgTableParams", "crud", "modal", "servicioLogin", "urls", function ($rootScope, $scope, $filter, NgTableParams, crud, modal, servicioLogin, urls) {

    /*Datos Catalogo*/
    $scope.item_catalogo = {
        cat_bie_id: -1,
        gru_gen_id: 0,
        cla_gen_id: 0,
        fam_gen_id: 0,
        uni_med_id: 0,
        gru_gen: {},
        cla_gen: {},
        fam_gen: {},
        uni_med: {},
        cod: "",
        den_bie: ""
    };
    
    $scope.modalTitle = "";
    $scope.modalAction = "";
    $scope.editItem = false;
    
    /*Lista Grupos GenericmostrarCatalogoos*/
    $scope.grupos_genericos = {};

    /*Lista Clases Genericas*/
    $scope.clases_genericas = {};

    /*Lista Familia Genericas*/
    $scope.familias_genericas = {};

    /*Lista de Unidades de Medida*/
    $scope.unidades_medida = {};

    /*Tabla del Catalogo de Bienes*/
    var params = {count: 20};
    var setting = {counts: []};
    $scope.tabla_catalogo = new NgTableParams(params, setting);

    $scope.tabla_catalogo_reporte;
   
    /*Catalogo de Bienes Importados*/
    $scope.catalogo_bienes = {nombre: "", archivo: ""};
    
    $scope.agregarItem = function () {
        
        $scope.modalTitle = "Agregar Nuevo Item";
        $scope.modalAction = "add";
        
        reiniciarNuevoItem();
        $("#modalnuevoitem").modal('show');
    }
    
    $scope.editarItemCatalogo = function (item) {
        
        $scope.modalTitle = "Editar Item";
        $scope.modalAction = "edit";
        
        $scope.editItem = true;       
        
        $scope.item_catalogo = JSON.parse(JSON.stringify(item));
        
        $scope.listarClasesGenericas({gru_gen_id: $scope.item_catalogo.gru_gen_id});
                
        var uni_med = $filter('filter')($scope.unidades_medida, {uni_med_id: $scope.item_catalogo.uni_med_id})[0];
        var gru_gen = $filter('filter')($scope.grupos_genericos, {gru_gen_id: $scope.item_catalogo.gru_gen_id})[0];
              
        $scope.item_catalogo.uni_med = uni_med;
        $scope.item_catalogo.gru_gen = gru_gen;
                
        $("#modalnuevoitem").modal('show');
    }
    
    reiniciarNuevoItem = function () {
        
        $scope.editItem = false; 
        
        $scope.item_catalogo = {
            cat_bie_id: -1,
            gru_gen_id: 0,
            cla_gen_id: 0,
            fam_gen_id: 0,
            uni_med_id: 0,
            gru_gen: {},
            cla_gen: {},
            fam_gen: {},
            uni_med: {},
            cod: "",
            den_bie: ""
        };
    }
    
    $scope.seleccionarUnidadMed = function (uniMed) {
        $scope.item_catalogo.uni_med_id = uniMed.uni_med_id;
    }
    
    $scope.listarGruposGenericos = function () {

        var request = crud.crearRequest('catalogo_bienes', 1, 'listar_grupos');
        crud.listar("/controlPatrimonial", request, function (data) {
            $scope.grupos_genericos = data.data;            
        }, function (data) {
            console.info(data);
        });
    }

    $scope.listarClasesGenericas = function (grupo) {
        
        $scope.item_catalogo.gru_gen_id = grupo.gru_gen_id;
        
        var request = crud.crearRequest('catalogo_bienes', 1, 'listar_clases');
        request.setData({gru_gen_id: grupo.gru_gen_id});
        
        crud.listar("/controlPatrimonial", request, function (data) {
            $scope.clases_genericas = data.data;   
            $scope.familias_genericas = null;                
            
            //Cuando editamos un itme
            if ($scope.item_catalogo.cat_bie_id != -1) {
                
                var cla_gen = $filter('filter')($scope.clases_genericas, {cla_gen_id: $scope.item_catalogo.cla_gen_id})[0];
                $scope.item_catalogo.cla_gen = cla_gen;
                $scope.listarFamiliasGenericas(cla_gen);
            }
        }, function (data) {
            console.info(data);
        });

    }

    $scope.listarFamiliasGenericas = function (clase) {
        // Autmaticamente se llama cuando cambias el grupo
        if (clase == null) {
            return;            
        }
        
        $scope.item_catalogo.cla_gen_id = clase.cla_gen_id;
        
        var request = crud.crearRequest('catalogo_bienes', 1, 'listar_familias');
        request.setData({cla_gen_id: clase.cla_gen_id});
        crud.listar("/controlPatrimonial", request, function (data) {
            $scope.familias_genericas = data.data;
            
            //Cuando editamos un itme
            if ($scope.item_catalogo.cat_bie_id != -1) {
                var fam_gen = $filter('filter')($scope.familias_genericas, {fam_gen_id: $scope.item_catalogo.fam_gen_id})[0];
                $scope.item_catalogo.fam_gen = fam_gen;
            }
            
        }, function (data) {
            console.info(data);
        });

    }


    $scope.listarUnidadesMedida = function () {
        var request = crud.crearRequest('catalogo_bienes', 1, 'listar_unidades_medida');
        crud.listar("/controlPatrimonial", request, function (data) {
            $scope.unidades_medida = data.data;
        }, function (data) {
            console.info(data);
        }
        );
    }

    $scope.agregarItemCatalogo = function (itemCatalogo, usu_mod) {

        $scope.nuevo_item = itemCatalogo;                
        
        // console.log(itemCatalogo);
        //VERIFICAMOS SI TODOS LOS CAMPOS SE LLENARON CORRECTAMENTE
        var accept = true;
        for (var atrib in itemCatalogo) {
            if (atrib != 'i' && atrib != 'num_res' && itemCatalogo[atrib] == "") {
                accept = false;
            }
        }
        var codigo_sbn = $scope.nuevo_item.cod;
        if (!(codigo_sbn.length == 6)) {
            aceept = false;
        }

        if (accept == true) { /*Todos los campos estan llenados correctamente*/
            
            // Nombre de la unidad
            $scope.nuevo_item.uni_med_nom = $scope.item_catalogo.uni_med.nom;
            
            $scope.nuevo_item.num_res = "";
            $scope.nuevo_item.usu_mod = usu_mod.ID;
           
            $scope.registrarItemsCatalogo($scope.nuevo_item);
            
            /*Finalmente cerramos la ventana modal*/
            $('#modalnuevoitem').modal('hide');
        } else {

            modal.mensaje("ERROR!!", "Verifique que todos los campos requeridos esten llenados");
        }
    }

    $scope.registrarItemsCatalogo = function (itemCatalogo) {
        
        $scope.item = itemCatalogo;
               
        // delete $scope.item_catalogo.gru_gen;
       
        // console.log(itemCatalogo);
        
        if ($scope.item.cat_bie_id == -1) { //Agregar nuevo item
            
            modal.mensajeConfirmacion($scope, "Seguro que desea registrar Items al Catalogo de Bienes ?", function () {

                var request = crud.crearRequest('catalogo_bienes', 1, 'registrar_catalogo_bienes');
                request.setData($scope.nuevo_item);
                crud.insertar("/controlPatrimonial", request, function (response) {
                    modal.mensaje("CONFIRMACION", response.responseMsg);

                    if (response.responseSta) {
                        //$scope.area = response.data;
                        $scope.item.cat_bie_id = response.data.cat_bie_id;
                        insertarElemento(setting.dataset, $scope.item);
                        $scope.tabla_catalogo.reload();                                        
                    }
                }, function (data) {
                    console.info(data);
                });
            });
        
        } else { // Editar item             
            
            var request = crud.crearRequest('catalogo_bienes', 1, 'editar_item_catalogo');
            request.setData(itemCatalogo);
            crud.actualizar("/controlPatrimonial", request, function (response) {

                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    //  Manually hide the modal.
                    var index = itemCatalogo.i;
                    setting.dataset[index] = itemCatalogo;
                    $scope.tabla_catalogo.reload(); 
                }

            }, function (data) {
                console.info(data);
            });
        }       
        
    }

    $scope.mostrarCatalogo = function () {
        
        $rootScope.showLoading();
        
        var request = crud.crearRequest('catalogo_bienes', 1, 'listar_catalogo_bienes');
        crud.listar("/controlPatrimonial", request, function (response) {
            if (response.responseSta) {
                setting.dataset = response.data;
                iniciarPosiciones(setting.dataset);
                $scope.tabla_catalogo.settings(setting);
                $scope.tabla_catalogo.reload();
                
                $rootScope.hideLoading();
            }
        }, function (data) {
            console.info(data);
        });

    }

    $scope.eliminarItemCatalogo = function (item) {

        modal.mensajeConfirmacionAuth($scope, "Seguro que desea eliminar este Catalogo?", function () {
            var request = crud.crearRequest('catalogo_bienes', 1, 'eliminar_item_catalogo');
            request.setData(item);
            crud.eliminar("/controlPatrimonial", request, function (response) {
                
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                    if (response.responseSta) {
                        eliminarElemento(setting.dataset, item.i);
                        $scope.tabla_catalogo.reload();
                    }

                }, function (data) {
            });
        });
    }
    
    $scope.verItemCatalogo = function (item) {
        console.log(item);
        $scope.item_catalogo = item;
        $("#modalVerItem").modal("show");
    }

    $scope.reporteCatalogo = function () {
        
        var request = crud.crearRequest('catalogo_bienes', 1, 'reporte_catalogo');
        request.setData($scope.tabla_catalogo.data);        
        crud.listar("/controlPatrimonial", request, function (response) {
            
            if (response.responseSta){
                verDocumentoPestana(response.data[0].datareporte);
            }            
            
        }, function (data) {
            console.info(data);
        });
    }

    $scope.cargar_codigo = function () {
        
        if ($scope.item_catalogo.fam_gen == null) {
            //$scope.item_catalogo.cod = "";
            return;
        }
        
        $scope.item_catalogo.fam_gen_id = $scope.item_catalogo.fam_gen.fam_gen_id;
        
        var grupo;
        var clase;
        var familia;
        
       grupo = $scope.item_catalogo.gru_gen.cod_gru;
       clase = $scope.item_catalogo.cla_gen.cod_cla;
       familia = $scope.item_catalogo.fam_gen.cod_fa;

        var codigo_sbn = (grupo.concat(clase)).concat(familia);
        if (codigo_sbn.length == 8) {
            $scope.item_catalogo.cod = codigo_sbn;
        } else {
            $scope.item_catalogo.cod = "";
        }
    }

}]);


