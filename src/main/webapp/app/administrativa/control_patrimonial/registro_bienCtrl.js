
app.controller("registro_bienCtrl", ["$rootScope", "$scope", "NgTableParams", "$location", "crud", "modal", "ModalService", "$timeout", function ($scope, $rootScope, NgTableParams, $location, crud, modal, ModalService, $timeout) {

    var ORG_ID = $rootScope.usuMaster.organizacion.organizacionID;

    $scope.bienesCookies = false;  
    $scope.actualizarBien = false;

    /*Codigo de Barras del Bien*/
    $scope.codigo_bar = "";

    /*Guardar catalogo de bienes original*/
    $scope.CATALAGO = [];

    $scope.selectedCatalogo = false;        

    /*Datos Catalogo*/
    $scope.item_catalogo = {
        gru_gen_id: 0,
        cla_gen_id: 0,
        fam_gen_id: 0,
        uni_med_id: 0,
        cod: 0,
        den_bie: ""
    };
    /*Datos Movmiento Ingresos*/
    $scope.movimiento = {
        tip_mov_ing: 0,
        con_pat_id: 0,
        num_res: "",
        fec_res_dt: new Date(),
        fec_res: "",
        obs: ""
    };

    /*Listado de Controles Patrimoniales*/
    $scope.controles_patrimoniales = {};

    /*Datos Detalle Tecnico*/
    $scope.detalle_tecnico = {};

    /*Correlativo del Nuevo Bien*/
    $scope.nro_correlativo;

    /*Listado de Tipo de Movimientos de Ingreso*/
    $scope.movimientos_ingreso = {};

    /*ALTAS*/
    $scope.altas = {};
    /*BAJAS*/
    $scope.bajas = {};
    /*SALIDAS*/
    $scope.salidas = {};

    //** Variable para la Actualizacion de un Bien
    $scope.bien_mueble_update;

    /*Accion del Bien*///(Registrar,Actualizar)
    $scope.accion_bien = "Crear Registro Individual";

    $scope.revisarLocalStorage = function () {

        if (localStorage.getItem("bien_mueble") !== null) {
            $scope.actualizarBien = true;               
            $scope.accion_bien = "Actualizar Bien";
            var bienMuebleTemp = JSON.parse(localStorage.getItem("bien_mueble"));
            $scope.buscar_bien(bienMuebleTemp.cod_bie, bienMuebleTemp.org_id);

        } else if (localStorage.getItem("tabla_inventario") !== null) {
            $scope.bienesCookies = true; //Cuando tienes que agregar un bien y regresar a otra interfaz
            $scope.accion_bien = "Guardar";

        } else {
            localStorage.removeItem('bien_mueble');
            localStorage.removeItem('prev_page');
        }           

    }        

    /*Lista del Tipo de Causal*/
    $scope.tipo_causal = {};

    /*Lista Seleccionada*/

    $scope.lista = {
        movimiento: "",
        anexo: "",
        cont_pat: "",
        ambiente: "",
        condicion: ""
    };

    /*Documentos Adjuntos*/
    $scope.doc_referencia = {nombre: "", archivo: "", nombre_archivo: "", doc_ref_url:""};
    $scope.imag_1 = {nombre: "", archivo: "", nombre_archivo: "", rut_imag_1_url:""};
    $scope.imag_2 = {nombre: "", archivo: "", nombre_archivo: "", rut_imag_2_url:""};
    $scope.autopartes = {nombre: "", archivo: "", nombre_archivo: "", rut_aut_img_url:""};

    /*Datos del Bien Mueble*/
    $scope.bien_mueble = {
        /*Id Bien*/    // OJO: Solo para actualizar registro
        id_bien: 0,
        /*Id Movimiento Ingresos */ // OJO: Solo para actualizar registro
        mov_ing_id: 0,
        /*Id Valor Contable*/ //OJO: Solo para actualizar registro
        val_cont_id: 0,
        /*Lista de Archivos*/ //OJO:Solo para actualizar Registro 
        doc_referencia: "",
        imag_1: "",
        imag_2: "",
        autopartes: "",
        /***************************/

        /*Cabecera*/
        amb_id: 0,
        cat_bie_id: "",
        an_id: "",
        des_bie: "",
        cant: 0,
        fec_reg: "",
        cod_int: "1",
        rut_doc_bie: "",
        cod_ba_bie: "",
        usu_mod: 0,
        org_id: 0,
        verificar: "N",
        /*Cuenta Contable*/
        valor_cont: 0,
        act_dep: false,
        cod_cuenta: "",
        /*Detalle Tecnico*/
        marc: "",
        mod: "",
        cond: "",
        dim: 0,
        ser: "",
        col: "",
        tip: "",
        nro_mot: "",
        nro_pla: "",
        nro_cha: "",
        raza: "",
        edad: 0,
        rut_imag_1: "",
        rut_imag_2: "",
        rut_aut_img: "",
        movimiento: {},
        actualizar_bien: false
    };
    /*Lista de Tipo de Anexo*/
    $scope.tipo_anexo = {
        ane_id: "",
        ane_nom: ""
    }

    /*Lista de Ambientes*/
    $scope.ambientes = {
        amb_id: "",
        amb_des: ""
    }
    /*Condicion*/
    $scope.condicion = [
        {cond_id: 'B', cond_nom: 'BUENO'},
        {cond_id: 'M', cond_nom: 'MALO'},
        {cond_id: 'R', cond_nom: 'REGULAR'}
    ];

    $scope.bien_mueble.cond = $scope.condicion[2];


    /*...................*/
    /*Variable bandera para saber si es un registro de insercion o actualizacion */
    var es_actualizar = false;


    /*Variable Bandera para elegir el item de catalogo de bien*/
    var editar_item_catalogo = false;

    /*Tabla del Catalogo de Bienes*/
    var params = {count: 5};
    var setting = {counts: []};
    $scope.tabla_catalogo = new NgTableParams(params, setting);

    /*Variable Bandera para el Control del Listado*/
    var flag_listas = 1;


    $scope.mostrarCatalogo = function () {

        var request = crud.crearRequest('catalogo_bienes', 1, 'listar_catalogo_bienes');
        crud.listar("/controlPatrimonial", request, function (data) {
            if (data.data) {

                flag_listas = flag_listas + 1; //4
                setting.dataset = data.data;
                $scope.CATALAGO = JSON.parse(JSON.stringify(data.data)); //Copia para restaurar los valores por defecto
                iniciarPosiciones(setting.dataset);
                $scope.tabla_catalogo.settings(setting);
                $scope.tabla_catalogo.reload();
                $scope.selectedCatalogo = false;
                /*$scope.listar_anexos(); */
            }
        }, function (data) {
            console.info(data);
        });

    }

    $scope.registrar_bien = function (usuario, orgId) {

        /*Seleccionamos las Listas*/
        $scope.movimiento.tip_mov_ing = $scope.lista.movimiento.tip_mov_id;
        $scope.bien_mueble.an_id = $scope.lista.anexo.ane_id;
        $scope.movimiento.con_pat_id = $scope.lista.cont_pat.cp_id;
        $scope.bien_mueble.amb_id = $scope.lista.ambiente.amb_id;
        $scope.bien_mueble.cond = $scope.lista.condicion.cond_id;

        /*Asignamos Valores de los Documentos Adjuntos*/
        $scope.bien_mueble.rut_doc_bie = $scope.doc_referencia.archivo;
        $scope.bien_mueble.rut_imag_1 = $scope.imag_1.archivo;
        $scope.bien_mueble.rut_imag_2 = $scope.imag_2.archivo;
        $scope.bien_mueble.rut_aut_img = $scope.autopartes.archivo;
        $scope.bien_mueble.org_id = orgId;
        $scope.bien_mueble.movimiento = $scope.movimiento;

        //$scope.bien_mueble.cod_cuenta = parseInt($scope.bien_mueble.cod_cuenta);

        /*Asignamos los Nombres de los Archivos*/ //OJO: Solo para el caso de Actualizar el Registro
        $scope.bien_mueble.doc_referencia = $scope.doc_referencia.nombre_archivo;
        $scope.bien_mueble.imag_1 = $scope.imag_1.nombre_archivo;
        $scope.bien_mueble.imag_2 = $scope.imag_2.nombre_archivo;
        $scope.bien_mueble.autopartes = $scope.autopartes.nombre_archivo;

//            console.log($scope.bien_mueble);

        /*Datos del Usuario*/
        $scope.bien_mueble.usu_mod = usuario.ID;

        /*if ($scope.bien_mueble.actualizar_bien == false) {
            $scope.movimiento.fec_res = convertirFecha($scope.movimiento.fec_res_dt);
        }*/

        $scope.movimiento.fec_res = convertirFecha($scope.movimiento.fec_res_dt);

        /*Validamos que solo suban imagenes*/
        var accept = true;

        if ($scope.bien_mueble.rut_imag_1 != "" && !$scope.validateUploadImage($scope.bien_mueble.rut_imag_1)) {
            accept = false;
            modal.mensaje("FORMATO INVALIDO", "Solo se aceptan imagenes, campo 1");
        }

        if ($scope.bien_mueble.rut_imag_2 != "" && !$scope.validateUploadImage($scope.bien_mueble.rut_imag_2)) {
            accept = false;
            modal.mensaje("FORMATO INVALIDO", "Solo se aceptan imagenes, campo 2");
        }

        if ($scope.bien_mueble.rut_aut_img != "" && !$scope.validateUploadImage($scope.bien_mueble.rut_aut_img)) {
            accept = false;
            modal.mensaje("FORMATO INVALIDO", "Solo se aceptan imagenes, autopartes");
        }

        /*Verificamos que todos lo campos esten llenados*/

        for (var atrib in $scope.bien_mueble) {
            if ($scope.bien_mueble[atrib] == "") {
                if (atrib == "cod_ba_bie") {
                    continue;
                }
                if (atrib == "id_bien") {
                    continue;
                }
                if (atrib == "mov_ing_id") {
                    continue;
                }
                if (atrib == "val_cont_id") {
                    continue;
                }
                if (atrib == "nro_mot") {
                    continue;
                }
                if (atrib == "nro_pla") {
                    continue;
                }
                if (atrib == "nro_cha") {
                    continue;
                }
                if (atrib == "raza") {
                    continue;
                }
                if (atrib == "rut_imag_1") {
                    continue;
                }
                if (atrib == "rut_imag_2") {
                    continue;
                }
                if (atrib == "rut_aut_img") {
                    continue;
                }
                if (atrib == "act_dep") {
                    continue;
                }
                if (atrib == "actualizar_bien") {
                    continue;
                }
                if (atrib == "rut_doc_bie") {
                    continue;
                }
                if (atrib == "rut_imag_1") {
                    continue;
                }
                if (atrib == "rut_imag_2") {
                    continue;
                }
                if (atrib == "rut_aut_img") {
                    continue;
                }
                if (atrib == "doc_referencia") {
                    continue;
                }
                if (atrib == "imag_1") {
                    continue;
                }
                if (atrib == "imag_2") {
                    continue;
                }
                if (atrib == "autopartes") {
                    continue;
                }
                if (atrib == "fec_reg") {
                    continue;
                }
                if (atrib == "cod_ba_bie") {
                    continue;
                }
                if (atrib == "cod_int") {
                    continue;
                }                    
                //console.log(atrib);
                accept = false;
            }
        }

        if (accept == true) {

            if ($scope.actualizarBien == false) {
                modal.mensajeConfirmacion($scope, "Seguro que desea registrar el bien mueble", function () {
                    var request = crud.crearRequest('ingresos', 1, 'registrar_bien_mueble');
                    request.setData($scope.bien_mueble);
//                        console.log(request);
                    //return;
                    crud.insertar("/controlPatrimonial", request, function (response) {
                        modal.mensaje("CONFIRMACION", response.responseMsg);

                        if (response.responseSta) {
                            //   $scope.area = response.data;}

                            //  $location.path(localStorage.getItem('origen'));
                            var cod_bie = response.data.cod_bie;

                            if (localStorage.getItem("prev_page") !== null && localStorage.getItem("tabla_inventario") !== null) {

                                var objStr = localStorage.getItem("tabla_inventario");
                                var tablaInventario = JSON.parse(objStr); 
                                var numBienes = tablaInventario.length;

                                $scope.itemTemporal = {
                                    an_bien: $scope.lista.anexo.ane_nom,
                                    cant_bie: $scope.bien_mueble.cant,
                                    cat_bie_id: $scope.bien_mueble.cat_bie_id,
                                    cod_bie: cod_bie,
                                    cod_pat_bie: $scope.lista.cont_pat.cp_id,
                                    des_bie: $scope.bien_mueble.des_bie,
                                    est_bien: $scope.lista.condicion.cond_id,
                                    fec_reg: convertirFecha2(new Date()),
                                    i: numBienes,
                                    mov_ing_id: $scope.lista.movimiento.tip_mov_id,
                                    org_id: orgId,
                                    tip_bie: true,
                                    ubi_bien: $scope.lista.ambiente.amb_des,
                                    verificar: "N"
                                }

                                tablaInventario.push($scope.itemTemporal);

//                                    console.log(tablaInventario, "tabla_inventario");

                                localStorage.setItem("tabla_inventario", JSON.stringify(tablaInventario));

                                var path = localStorage.getItem("prev_page");
                                $location.path(path);                                    
                            }

                            $scope.borrar_valores_bien();

                        }
                    }, function (data) {
                        console.info(data);
                    });

                });

            } else {
                modal.mensajeConfirmacion($scope, "Seguro que desea actualizar el Bien Mueble", function () {
                    var request = crud.crearRequest('ingresos', 1, 'registrar_bien_mueble');
                    request.setData($scope.bien_mueble);
//                        console.log(request);
                    //return;
                    crud.insertar("/controlPatrimonial", request, function (response) {
                        modal.mensaje("CONFIRMACION", response.responseMsg);
                        if (response.responseSta) {

                            if (localStorage.getItem("prev_page") !== null && localStorage.getItem("tabla_inventario") !== null) {

                                var objStr = localStorage.getItem("tabla_inventario");
                                var tablaInventario = JSON.parse(objStr); 

                                if (localStorage.getItem("bien_mueble") !== null) {
                                    var bienMuebleTemp = localStorage.getItem("bien_mueble");

                                    bienMuebleTemp = JSON.parse(bienMuebleTemp); 

                                    var itemTemporal = tablaInventario[bienMuebleTemp.i];

//                                        console.log(itemTemporal);

                                    itemTemporal.an_bien = $scope.lista.anexo.ane_nom;
                                    itemTemporal.cant_bie = $scope.bien_mueble.cant;
                                    itemTemporal.cat_bie_id = $scope.bien_mueble.cat_bie_id;
                                    itemTemporal.cod_pat_bie = $scope.lista.cont_pat.cp_id;
                                    itemTemporal.des_bie = $scope.bien_mueble.des_bie;
                                    itemTemporal.est_bien = $scope.lista.condicion.cond_id;                                            
                                    itemTemporal.ubi_bien = $scope.lista.ambiente.amb_des;                                         

                                    localStorage.setItem("tabla_inventario", JSON.stringify(tablaInventario));

                                }   
                            }

                            localStorage.removeItem('bien_mueble');
                            var path = localStorage.getItem("prev_page");
                            localStorage.removeItem('prev_page');
                            $location.path(path);
                        }
                    }, function (data) {
                        console.info(data);
                    });

                });

            }


        } else {
            modal.mensaje("ERROR AL REGISTRAR", "Verifique que todos los campos esten llenados");
        }

    }

    $scope.validateUploadImage = function (file) {

        var ext = file.name.match(/\.(.+)$/)[1];
        if (angular.lowercase(ext) === 'jpg' || angular.lowercase(ext) === 'jpeg' || angular.lowercase(ext) === 'png') {
            return true;
        } else {
            return false;
        }
    }

    $scope.listar_anexos = function () {

        var request = crud.crearRequest('configuracion_patrimonial', 1, 'listar_anexos');
        crud.listar("/controlPatrimonial", request, function (data) {
            if (data.data) {

                $scope.tipo_anexo = data.data;
                flag_listas = flag_listas + 1; //5

                //$scope.listar_tipos_movimiento();
            }
        }, function (data) {
            console.info(data);
        });
    }

    $scope.listar_ambientes = function (orgId) {
        var request = crud.crearRequest('ingresos', 1, 'listar_ambientes');
        request.setData({org_id: $rootScope.usuMaster.organizacion.organizacionID});
        crud.listar("/controlPatrimonial", request, function (data) {
            if (data.data) {
                $scope.ambientes = data.data;
                flag_listas = flag_listas + 1; //3
                $scope.obtener_correlativo($rootScope.usuMaster.organizacion.organizacionID);
                //$scope.mostrarCatalogo();
            }
        }, function (data) {
            console.info(data);
        });

    }

    $scope.elegirCatalogo = function (cat) {

        $scope.bien_mueble.cat_bie_id = cat.id_cat;
        editar_item_catalogo = true;

        $scope.selectedCatalogo = true;

        var setting2 = {counts: []};
        /*Redimensionamos la tabla de catalogo de bienes*/
        var my_array = new Array();
        my_array[0] = cat;

        setting2.dataset = my_array;
        setting.dataset = setting2.dataset;
        iniciarPosiciones(setting.dataset);
        $scope.tabla_catalogo.settings(setting);
        $scope.tabla_catalogo.reload();

    }

    $scope.changeSelectedCatalogo = function (event, item) {

        if (event.target.checked) {
            $scope.elegirCatalogo(item);

        } else {
            $scope.reloadCatalogo(); //Mostrar todos los items del catalogo
        }
    }

    $scope.reloadCatalogo = function () {

        $scope.selectedCatalogo = false;

        setting.dataset = $scope.CATALAGO;
        iniciarPosiciones(setting.dataset);
        $scope.tabla_catalogo.settings(setting);
        $scope.tabla_catalogo.reload();

    }

    $scope.buscarCatalogo = function (IdCat) {
        $scope.selectedCatalogo = true;
        var size = setting.dataset.length;
        for (var i = 0; i < size; i++)
        {
            if (setting.dataset[i].cat_bie_id == IdCat) {
                editar_item_catalogo = true;
                var setting2 = {counts: []};
                /*Redimensionamos la tabla de catalogo de bienes*/
                var my_array = new Array();
                my_array[0] = setting.dataset[i];
                setting2.dataset = my_array;
                setting.dataset = setting2.dataset;
                iniciarPosiciones(setting.dataset);
                $scope.tabla_catalogo.settings(setting);
                $scope.tabla_catalogo.reload();
                break;
            }
        }
    }

    $scope.borrar_valores_bien = function () {

        $scope.imag_1 = {nombre: "", archivo: "", nombre_archivo: ""};
        $scope.imag_2 = {nombre: "", archivo: "", nombre_archivo: ""};
        $scope.autopartes = {nombre: "", archivo: "", nombre_archivo: ""};

        $scope.bien_mueble = {
            //Cabecera
            cod_bie: 0,
            amb_id: 0,
            cat_bie_id: "",
            an_id: "",
            des_bie: "",
            cant: 0,
            fec_reg: "",
            obs: "",
            cod_int: 0,
            estado_bie: "",
            rut_doc_bie: "",
            cod_ba_bie: "",
            usu_mod: 0,
            //Cuenta Contable
            valor_cont: 0,
            act_dep: false,
            cod_cuenta: "",
            //Detalle Tecnico
            marc: "",
            mod: "",
            cond: "",
            dim: 0,
            ser: "",
            col: "",
            tip: "",
            nro_mot: "",
            nro_pla: "",
            nro_cha: "",
            raza: "",
            edad: 0,
            rut_imag_1: "",
            rut_imag_2: "",
            rut_aut_img: ""
        };

        $scope.mostrarCatalogo();
        $scope.listar_tipos_movimiento();
        $scope.listar_anexos();
        $scope.listar_ambientes();
    }

    $scope.listar_tipo_movimiento = function () {

    }

    $scope.listar_controles_patrimoniales = function (orgId) {

        var request = crud.crearRequest('configuracion_patrimonial', 1, 'listar_control_patrimonial');
        request.setData({org_id: orgId});
        crud.listar("/controlPatrimonial", request, function (data) {
            if (data.data) {

                $scope.controles_patrimoniales = data.data;
                flag_listas = flag_listas + 1; //2
                //$scope.listar_ambientes(orgId);

            }
        }, function (data) {
            console.info(data);
        });

    }

    $scope.validar_bien = function () {
        $('#modalvalidarbien').modal('show');
    }

    $scope.buscar_bien = function (id_bien, orgId) {
        
        $rootScope.showLoading();

        flag_listas = 1;
        var request = crud.crearRequest('ingresos', 1, 'obtener_bien');
        request.setData({id_bien: id_bien});
        crud.listar("/controlPatrimonial", request, function (data) {
//                console.log(data.data, "detalles bien mueble");
            if (data.data) {

                $scope.bien_mueble.cod_bie = id_bien;
                /*Cabecera*/
                /*Obtenemos el Ambiente*/
                //$scope.obtenerAmbiente(data.data[0].amb_id);
                //$scope.bien_mueble.amb_id=data.data[0].amb_id;

                /*Redimensionamos la Tabla de Catalogo de Bienes*/
                $scope.bien_mueble.cat_bie_id = (data.data)[0].cat_bie_id;
//                    $scope.buscarCatalogo((data.data)[0].cat_bie_id);


                /****************************/

                $scope.bien_mueble.des_bie = (data.data)[0].des_bie;
                $scope.bien_mueble.cant = data.data[0].cant;

                /*Obtenemos el formato de la Fecha */
                fec_reg = new Date(data.data[0].fec_reg);
                $scope.bien_mueble.fec_reg = data.data[0].fec_reg;
                /***********************************/

                $scope.bien_mueble.cod_int = data.data[0].cod_int;
                //    $scope.bien_mueble.rut_doc_bie=data.data[0].rut_doc_bie;
                $scope.doc_referencia.nombre_archivo = data.data[0].doc_ref_name;
                $scope.doc_referencia.doc_ref_url = data.data[0].doc_ref_url;

                $scope.bien_mueble.cod_ba_bie = data.data[0].cod_ba_bie;
                $scope.bien_mueble.usu_mod = data.data[0].usu_mod;
                /*Cuenta Contable*/
                $scope.bien_mueble.valor_cont = data.data[0].valor_cont;
                if (data.data[0].act_dep == 'A') {
                    $scope.bien_mueble.act_dep = true;
                } else {
                    $scope.bien_mueble.act_dep = false;
                }

                $scope.bien_mueble.cod_cuenta = data.data[0].cod_cuenta;
                /*Detalle Tecnico*/
                $scope.bien_mueble.marc = data.data[0].marc;
                $scope.bien_mueble.mod = data.data[0].mod;
                /*Seleccionamos la Condicion*/
                $scope.lista.condicion = $scope.condicion[data.data[0].cond];

                $scope.bien_mueble.dim = data.data[0].dim;
                $scope.bien_mueble.ser = data.data[0].ser;
                $scope.bien_mueble.col = data.data[0].col;
                $scope.bien_mueble.tip = data.data[0].tip;
                $scope.bien_mueble.nro_mot = data.data[0].nro_mot;
                $scope.bien_mueble.nro_pla = data.data[0].nro_pla;
                $scope.bien_mueble.nro_cha = data.data[0].nro_cha;
                $scope.bien_mueble.raza = data.data[0].raza;
                $scope.bien_mueble.edad = data.data[0].edad;

                //    $scope.bien_mueble.rut_imag_1=data.data[0].rut_imag_1;
                $scope.imag_1.nombre_archivo = data.data[0].rut_imag_1_name;
                $scope.imag_1.rut_imag_1_url = data.data[0].rut_imag_1_url;

                //    $scope.bien_mueble.rut_imag_2=data.data[0].rut_imag_2;
                $scope.imag_2.nombre_archivo = data.data[0].rut_imag_2_name;
                $scope.imag_2.rut_imag_2_url = data.data[0].rut_imag_2_url;
                //    $scope.bien_mueble.rut_aut_img=data.data[0].rut_aut_img;
                $scope.autopartes.nombre_archivo = data.data[0].rut_aut_img_name;
                $scope.autopartes.rut_aut_img_url = data.data[0].rut_aut_img_url;
                /*Movimiento*/
//                    $scope.obtenerTipMovIng(data.data[0].tip_mov_ing);
//                    /*Obtener el Control Patrimonial*/
//                    $scope.obtenerControl(data.data[0].con_pat_id);
//                    
//                    $scope.obtenerAnexo(data.data[0].an_id);

                $scope.movimiento.num_res = data.data[0].num_res;
                /*Obtenemos el Formato de la Fecha de Resolucion*/
                fec_res = new Date(data.data[0].fec_res.replace(/-/g, '\/'));
                $scope.movimiento.fec_res_dt = fec_res;
                /******************************************/

                $scope.movimiento.obs = data.data[0].obs;
                $scope.bien_mueble.id_bien = data.data[0].id_bien;
                $scope.bien_mueble.mov_ing_id = data.data[0].mov_ing_id;
                $scope.bien_mueble.val_cont_id = data.data[0].val_cont_id;

                $scope.bien_mueble.actualizar_bien = true;

                $scope.actualizarBien = true;

                $scope.accion_bien = "Actualizar Bien";

                $timeout(function(){
                    $scope.obtenerAmbiente(data.data[0].amb_id);
                    $scope.buscarCatalogo((data.data)[0].cat_bie_id);
                    $scope.obtenerTipMovIng(data.data[0].tip_mov_ing);
                    $scope.obtenerControl(data.data[0].con_pat_id);
                    $scope.obtenerAnexo(data.data[0].an_id);     
                    $rootScope.hideLoading();
                    
                }, 3000);
            }
        }, function (data) {
            console.info(data);
        });

    }


    $scope.update_bien = function () {

        $scope.bien_mueble_update = JSON.parse(window.atob(localStorage.getItem('update_bien_mueble')));
        if (flag_listas == 6)
        {

            $scope.buscar_bien($scope.bien_mueble_update.id_bien, $scope.bien_mueble_update.org_id);
            localStorage.removeItem('update_bien_mueble');
        }
    }

    $scope.cancelarRegistroBien = function() {

        if (localStorage.getItem('prev_page') != null) {

            var prev = localStorage.getItem('prev_page');
            $location.path(prev);
            localStorage.removeItem('prev_page');
            localStorage.removeItem('bien_mueble');

        } else {
            
            localStorage.removeItem('bien_mueble');
            localStorage.removeItem('prev_page');
            location.reload(); 
        }            
    }

    $scope.listar_tipos_movimiento = function () {
        var request = crud.crearRequest('ingresos', 1, 'listar_tipo_movimiento');
        crud.listar("/controlPatrimonial", request, function (data) {
            if (data.data) {

                $scope.movimientos_ingreso = data.data;
                flag_listas = flag_listas + 1; //6
                if (flag_listas == 6 && $scope.bien_mueble_update.interfaz == "verificacion_registros")
                {
                    $scope.buscar_bien($scope.bien_mueble_update.id_bien, $scope.bien_mueble_update.org_id);
                    localStorage.removeItem('update_bien_mueble');
                }
            }
        }, function (data) {
            console.info(data);
        });
    }

    $scope.obtenerTipMovIng = function (TipMovId) {

        var size = $scope.movimientos_ingreso.length;
        for (var i = 0; i < size; i++) {
            if ($scope.movimientos_ingreso[i].tip_mov_id == TipMovId) {
                $scope.lista.movimiento = $scope.movimientos_ingreso[i];
                break;
            }
        }
    }

    $scope.obtenerAnexo = function (AnId) {

        var size = $scope.tipo_anexo.length;
        for (var i = 0; i < size; i++) {
            if ($scope.tipo_anexo[i].ane_id == AnId) {
                $scope.lista.anexo = $scope.tipo_anexo[i];
                break;
            }
        }
    }

    $scope.obtenerAmbiente = function (AmbId) {

        var size = $scope.ambientes.length;
        for (var i = 0; i < size; i++) {
            if ($scope.ambientes[i].amb_id == AmbId) {
                $scope.lista.ambiente = $scope.ambientes[i];
                break;
            }
        }

    }

    $scope.obtenerControl = function (cpId) {

        var size = $scope.controles_patrimoniales.length;
        for (var i = 0; i < size; i++) {
            if ($scope.controles_patrimoniales[i].cp_id == cpId) {
                $scope.lista.cont_pat = $scope.controles_patrimoniales[i];
                break;
            }
        }
    }


    $scope.buscarBienCodigoBarras = function(){

        if (this.codigo_bar.length > 10) {

            this.codigo_bar = "";

        } else if (this.codigo_bar.length == 10) {                

            //console.log(this.codigo_bar);
            var request = crud.crearRequest('ingresos', 1, 'buscar_bien_codigo_barras');
            request.setData({cod_ba_bie: this.codigo_bar});
            crud.listar("/controlPatrimonial", request, function (data) {

                if (data.responseSta) {
                    $scope.obtenerAmbiente(data.data[0].amb_id);
                    //$scope.bien_mueble.amb_id=data.data[0].amb_id;

                    /*Redimensionamos la Tabla de Catalogo de Bienes*/
                    $scope.bien_mueble.cat_bie_id = (data.data)[0].cat_bie_id;
                    $scope.buscarCatalogo((data.data)[0].cat_bie_id);

                    /****************************/

                    $scope.bien_mueble.des_bie = (data.data)[0].des_bie;
                    $scope.bien_mueble.cant = data.data[0].cant;

                    /*Obtenemos el formato de la Fecha */
                    fec_reg = new Date(data.data[0].fec_reg);
                    $scope.bien_mueble.fec_reg = data.data[0].fec_reg;
                    /***********************************/

                    $scope.bien_mueble.cod_int = data.data[0].cod_int;
                    //    $scope.bien_mueble.rut_doc_bie=data.data[0].rut_doc_bie;
                    $scope.doc_referencia.nombre_archivo = data.data[0].doc_ref_name;
                    $scope.doc_referencia.doc_ref_url = data.data[0].doc_ref_url;

                    $scope.bien_mueble.cod_ba_bie = data.data[0].cod_ba_bie;
                    $scope.bien_mueble.usu_mod = data.data[0].usu_mod;
                    /*Cuenta Contable*/
                    $scope.bien_mueble.valor_cont = data.data[0].valor_cont;
                    if (data.data[0].act_dep == 'A') {
                        $scope.bien_mueble.act_dep = true;
                    } else {
                        $scope.bien_mueble.act_dep = false;
                    }

                    $scope.bien_mueble.cod_cuenta = data.data[0].cod_cuenta;
                    /*Detalle Tecnico*/
                    $scope.bien_mueble.marc = data.data[0].marc;
                    $scope.bien_mueble.mod = data.data[0].mod;
                    /*Seleccionamos la Condicion*/
                    $scope.lista.condicion = $scope.condicion[data.data[0].cond];

                    $scope.bien_mueble.dim = data.data[0].dim;
                    $scope.bien_mueble.ser = data.data[0].ser;
                    $scope.bien_mueble.col = data.data[0].col;
                    $scope.bien_mueble.tip = data.data[0].tip;
                    $scope.bien_mueble.nro_mot = data.data[0].nro_mot;
                    $scope.bien_mueble.nro_pla = data.data[0].nro_pla;
                    $scope.bien_mueble.nro_cha = data.data[0].nro_cha;
                    $scope.bien_mueble.raza = data.data[0].raza;
                    $scope.bien_mueble.edad = data.data[0].edad;

                    //    $scope.bien_mueble.rut_imag_1=data.data[0].rut_imag_1;
                    $scope.imag_1.nombre_archivo = data.data[0].rut_imag_1_name;
                    $scope.imag_1.rut_imag_1_url = data.data[0].rut_imag_1_url;

                    //    $scope.bien_mueble.rut_imag_2=data.data[0].rut_imag_2;
                    $scope.imag_2.nombre_archivo = data.data[0].rut_imag_2_name;
                    $scope.imag_2.rut_imag_2_url = data.data[0].rut_imag_2_url;
                    //    $scope.bien_mueble.rut_aut_img=data.data[0].rut_aut_img;
                    $scope.autopartes.nombre_archivo = data.data[0].rut_aut_img_name;
                    $scope.autopartes.rut_aut_img_url = data.data[0].rut_aut_img_url;
                    /*Movimiento*/
                    $scope.obtenerTipMovIng(data.data[0].tip_mov_ing);
                    /*Obtener el Control Patrimonial*/
                    $scope.obtenerControl(data.data[0].con_pat_id);

                    $scope.obtenerAnexo(data.data[0].an_id);

                    $scope.movimiento.num_res = data.data[0].num_res;
                    /*Obtenemos el Formato de la Fecha de Resolucion*/
                    fec_res = new Date(data.data[0].fec_res.replace(/-/g, '\/'));
                    $scope.movimiento.fec_res_dt = fec_res;
                    /******************************************/

                    $scope.movimiento.obs = data.data[0].obs;
                    $scope.bien_mueble.id_bien = data.data[0].id_bien;
                    $scope.bien_mueble.mov_ing_id = data.data[0].mov_ing_id;
                    $scope.bien_mueble.val_cont_id = data.data[0].val_cont_id;

                    $scope.bien_mueble.actualizar_bien = true;

                    $scope.actualizarBien = true;
                    $scope.accion_bien = "Actualizar Bien";

                } else {
                    modal.mensaje("CONFIRMACION", data.responseMsg);
                }

            }, function (data) {
                console.info(data);
            });

        }

    }

    $scope.generarCodigoBarras = function() {
        var request = crud.crearRequest('ingresos', 1, 'generar_codigo_barras');
        var codBie =  $scope.bien_mueble.cod_bie;
        request.setData({cod_bie: codBie});

        crud.listar("/controlPatrimonial", request, function (response) {
            //console.log("generarCodigoBarras", codBie);
            if (response.responseSta) {
                $scope.dataBase64 = response.data[0].datareporte;
                window.open($scope.dataBase64);
            }
        }, function (data) {
            console.info(data);
        });
    }

    $scope.obtener_correlativo = function (org_id) {
        var request = crud.crearRequest('ingresos', 1, 'obtener_correlativo');
        request.setData({org_id: org_id});

        crud.listar("/controlPatrimonial", request, function (data) {
            if (data.data) {
                $scope.registrar_correlativo(data.data.num_corr);
            }
        }, function (data) {
            console.info(data);
        });
    }         

    $scope.registrar_correlativo = function (num_corr) {
        var nuevo_corr = num_corr + 1;

        var str_corr = "";
        var size_corr = str_corr.length;

        for (var i = 0; i < 6 - size_corr; i++) {
            str_corr = str_corr + "0";
        }
        str_corr = str_corr + nuevo_corr.toString();
        $scope.bien_mueble.cod_int = str_corr;

    }

    $scope.showBuscarCuentaContable = function () {

        //Variables para manejo de la tabla
        var paramsCuentasContables = {count: 10};
        var settingCuentasContables = {counts: []};
        $scope.tablaCuentasContables = new NgTableParams(paramsCuentasContables, settingCuentasContables);

        listarTablaCuentasContables();

        function  listarTablaCuentasContables() {
            //preparamos un objeto request
            var request = crud.crearRequest('cuentaContable', 1, 'listarCuentaContable');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistemaContable", request, function (response) {

                if (response.responseSta) {

                    settingCuentasContables.dataset = response.data;
                    //asignando la posicion en el arreglo a cada objeto
                    iniciarPosiciones(settingCuentasContables.dataset);
                    $scope.tablaCuentasContables.settings(settingCuentasContables);
                    $scope.tablaCuentasContables.reload();
                    //console.log($scope.tablaCuentasContables);

                    /*Recien cuando obtenemos la data de las cuentas, mostramos el modal*/

                    ModalService.showModal({
                        templateUrl: "administrativa/control_patrimonial/buscarCuentasContables.html",
                        controller: "buscarCuentasContableCtrl",
                        inputs: {
                            title: "Buscar Cuentas Contables",
                            tabla: $scope.tablaCuentasContables
                        }
                    }).then(function (modal) {
                        modal.element.modal();
                        modal.close.then(function (result) {
                            if (result.flag) {
                                $scope.bien_mueble.cod_cuenta = result.data.cuentaContableID;
//                                    console.log(result);
                            }
                        });
                    })
                }
            }, function (data) {
                console.info(data);
            });
        }
        ;
    };
}]);


app.controller('buscarCuentasContableCtrl', [
'$scope', '$element', 'title', 'tabla', 'close', 'crud', 'modal', 'NgTableParams',
function ($scope, $element, title, tabla, close, crud, modal, NgTableParams) {

    $scope.title = title;
    //$scope.tablaTrabajadores = tabla;
    //$scope.search = true;
    $scope.setClickedRow = function (d) {

        //  Manually hide the modal.
        $element.modal('hide');

        //  Now call close, returning control to the caller.
        close({
            data: d,
            flag: true
        }, 500); // close, but give 500ms for bootstrap to animate

        //console.log(d);
    };

    $scope.close = function () {
        close({
            flag: false
        }, 500); // close, but give 500ms for bootstrap to animate
    };

    //  This cancel function must use the bootstrap, 'modal' function because
    //  the doesn't have the 'data-dismiss' attribute.
    $scope.cancel = function () {

        //  Manually hide the modal.
        $element.modal('hide');

        //  Now call close, returning control to the caller.
        close({
            flag: false
        }, 500); // close, but give 500ms for bootstrap to animate
    };

}]);
