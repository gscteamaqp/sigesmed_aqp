app.controller("informes_iieeCtrl", ["$scope", "NgTableParams", "crud", "modal", function ($scope, NgTableParams, crud, modal) {

    /*Tabla del Catalogo de Bienes*/
    var params = {count: 20};
    var setting = {counts: []};
    $scope.tabla_patrimonio_institucional = new NgTableParams(params, setting);
       
    $scope.organizaciones = [];

    $scope.mostrar_informes_patrimonial = function (orgId) {

        var request = crud.crearRequest('configuracion_patrimonial', 1, 'listar_control_ugel');
        request.setData({org_id: orgId});
        crud.listar("/controlPatrimonial", request, function (data) {            
            
            if (data.data) {
                setting.dataset = data.data;
                iniciarPosiciones(setting.dataset);
                $scope.tabla_patrimonio_institucional.settings(setting);
                $scope.tabla_patrimonio_institucional.reload();
            }
        }, function (data) {
            console.info(data);
        });

    }

    $scope.reporte_ie = function (ie) {

        var orgId = ie.org_id;

        var request = crud.crearRequest('configuracion_patrimonial', 1, 'reporte_bienes_ie');
        request.setData({org_id: orgId});
        crud.listar("/controlPatrimonial", request, function (data) {
            $scope.dataBase64 = data.data[0].datareporte;
            window.open($scope.dataBase64);
        }, function (data) {
            console.info(data);
        });


    }
    
    $scope.obtenerIE = function (orgId) {

        var request = crud.crearRequest('reportes', 1, 'listarIE');
        request.setData({organizacionID: orgId});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las usuarios de exito y error
        
        crud.listar("/controlPersonal", request, function (data) {
            if (data.responseSta) {
                
                // Se necesita id, title
                for (var i = 0; i < data.data.length; i++) {
                    var IE = {id: data.data[i].id, title: data.data[i].nom};
                    $scope.organizaciones.push(IE);
                }
            }
            //modal.mensaje("CONFIRMACION",data.responseMsg);
        }, function (data) {
            console.info(data);
        });
    }

}]);

