app.controller("inventario_inicialCtrl", ["$rootScope", "$scope", "NgTableParams", "$location", "crud", "modal", "$timeout", function ($rootScope, $scope, NgTableParams, $location, crud, modal, $timeout) {

    var ORG_ID = $rootScope.usuMaster.organizacion.organizacionID;   
    
    /*Tipo de Inventario*/ //(Identifica el Tipo de Inventario a Registrar)
    $scope.tipo_inventario_inicial = {val: false};
    $scope.tipo_inventario_fisico = {val: true};
    /********************/
    
    /*Tabla del Inventario Inicial*/
    var params = {count: 15};
    var setting = {counts: []};
    $scope.tabla_inventario_inicial = new NgTableParams(params, setting);
    setting.dataset = new Array();


    /*Tabla de Bienes Muebles*/
    var params2 = {count: 7};
    var setting2 = {counts: []};
    $scope.tabla_bienes_muebles = new NgTableParams(params2, setting2);
    
    $scope.accion_inventario = "Registrar Inventario";
    $scope.editarInventario = false;

    /*Variable para la Actualizacion de un Inventario*/
    $scope.inventario_update = {};

    /*Datos Movmiento Ingresos*/
    $scope.movimiento = {
        mov_ing_id: 0, /*OJO : Solo para Actualizar */

        tip_mov_ing: 0,
        cont_pat_id: 0,
        num_res: "",
        fec_res: new Date(),
        obs: ""
    };

    $scope.lista = {
        movimiento: "",
        cont_pat: ""
    };

    $scope.movimientos_ingreso = {};

    /*Variable Bandera para el Control del Listado*/
    var flag_listas = 1;

    $scope.inventario_inicial_cab = {
        inv_ini_id: 0, //OJO: Solo para Edicion de Inventario
        actualizar_inventario: false,
        con_pat_id: 0,
        fla_cie: "A",
        usu_mod: 0,
        org_id: 0
    };

    $scope.bienes_inv_ini = {
        cod_bie: ""
    };

    $scope.inventario_inicial = {
        movimiento: {},
        inventario_inicial_cab: {},
        bienes_inv_ini: []
    };

    $scope.tabla_inventario_reporte = {};

    /*Lista de Ambientes*/
    $scope.ambientes = {};

    /*Listar Controles Patrimoniales*/
    $scope.controles_patrimoniales = {};

    /*Bienes por Registrar al Inventario Inicial*/
    var array_bienes = new Array();

    /*Controles Patrimoniales para una Institucion*/
    $scope.controles_patrimoniales = {};
    
    $scope.verificarStorageEditarInv = function () {
        
        if (localStorage.getItem("inv_data") !== null) {
            
            var invData = JSON.parse(localStorage.getItem("inv_data"));
            
            $scope.editarInventario = true;
        
            if (invData.tipo_inv == "I"){
                
                $scope.buscarInventarioInicial(invData.inv_id, invData.org_id);
                
            } else if (invData.tipo_inv == "F") {
                
                $scope.buscarInventarioFisico(invData.inv_id, invData.org_id);
            }
            
            localStorage.removeItem("inv_data");
            
        }
        
        if (localStorage.getItem("inv_data_mov") !== null) {
            
            var invMov = JSON.parse(localStorage.getItem("inv_data_mov"));
                      
            if (invMov.cont_pat_id != 0) {
                $scope.movimiento.cont_pat_id = invMov.cont_pat_id;
            }
            
            if (invMov.num_res != "") {
                $scope.movimiento.num_res = invMov.num_res;
            }
            
            if (invMov.obs != "") {
                $scope.movimiento.obs = invMov.obs;
            }
            
            if (invMov.mov_ing_id != 0) {
                $scope.movimiento.mov_ing_id = invMov.mov_ing_id;
            }                        
            
            localStorage.removeItem("inv_data_mov");
        }
    }   
        
    $scope.listarAmbientes = function () {
        var request = crud.crearRequest('catalogo_bienes', 1, 'listar_grupos');
        crud.listar("/controlPatrimonial", request, function (data) {
            $scope.ambientes = data.data;
        }, function (data) {
            console.info(data);
        });
    }

    $scope.registro_bienes = function () {

        localStorage.setItem('origen', "inventario_inicial");
        $location.path('registro_bienes');
    }


    $scope.mostrar_bienes_muebles = function (orgId) {
        var request = crud.crearRequest('ingresos', 1, 'listar_bienes_muebles');
        request.setData({org_id: orgId});
        crud.listar("/controlPatrimonial", request, function (data) {
            if (data.data) {
                setting2.dataset = data.data;                
                iniciarPosiciones(setting2.dataset);
                $scope.tabla_bienes_muebles.settings(setting2);
                $scope.tabla_bienes_muebles.reload();

            }
        }, function (data) {
            console.info(data);
        });
    }

    $scope.elegir_bienes = function (bien_mue) {

        /*Desabilitamos la opcion de registro*/
        bien_mue.tip_bie = true;
        insertarElemento(array_bienes, bien_mue);
    }

    $scope.registrar_bienes = function () {

        var listaBienesSize = array_bienes.length;       
       
        for (var i = 0; i < listaBienesSize; i++) {
            insertarElemento(setting.dataset, array_bienes[i]);
        }
//        /*Marcamos los bienes Registrados (Solo para el caso de Actualizar)*/
        if ($scope.inventario_inicial_cab.actualizar_inventario == true) {
            for (var j = 0; j < listaBienesSize; j++) {
                for (var k = 0; k < setting2.dataset.length; k++) {
                    if (setting2.dataset[k].cod_bie == array_bienes[j].cod_bie) {
                        setting2.dataset[k].tip_bie = true;
                        break;
                    }
                }
            }
        }
        array_bienes = new Array();
//
        iniciarPosiciones(setting.dataset);
        $scope.tabla_inventario_inicial.settings(setting);
        $scope.tabla_inventario_inicial.reload();

        $('#modalListarBienes').modal('hide');
    }

    $scope.registrar_bienes_inventario = function (user, orgId) {

        /*Asignamos las listas*/
        $scope.movimiento.cont_pat_id = $scope.lista.cont_pat.cp_id;
        $scope.movimiento.tip_mov_ing = $scope.lista.movimiento.tip_mov_id;


        /*Registramos la Data Obtenida a un solo Objeto*/
        $scope.inventario_inicial_cab.usu_mod = user.ID;
        //$scope.inventario_inicial_cab.fec_res = convertirFecha($scope.inventario_inicial_cab.fec_res);
        $scope.inventario_inicial_cab.org_id = orgId;
        $scope.inventario_inicial.inventario_inicial_cab = $scope.inventario_inicial_cab;
        $scope.inventario_inicial.bienes_inv_ini = setting.dataset;
        $scope.inventario_inicial.movimiento = $scope.movimiento;
        /*
        if ($scope.inventario_inicial_cab.actualizar_inventario == false) {
            $scope.movimiento.fec_res = convertirFecha($scope.movimiento.fec_res_dt);
        }*/
        
        $scope.inventario_inicial.movimiento = $scope.movimiento;
        $scope.inventario_inicial_cab.org_id = orgId;
      
        /*REGISTRO INVENTARIO INICIAL*/
        if ($scope.tipo_inventario_inicial.val == true) {
            
            modal.mensajeConfirmacion($scope, "Seguro que desea registrar los Bienes al Inventario Inicial ?", function () {
                var request = crud.crearRequest('ingresos', 1, 'registrar_inventario_incial');
                request.setData($scope.inventario_inicial);
                //console.log(request, "inv inicial");
                //return;
                crud.insertar("/controlPatrimonial", request, function (response) {

                    if (response.responseSta) {
                        modal.mensaje("CONFIRMACION", response.responseMsg);
                        $scope.limpiar_inventario();
                    }
                }, function (data) {
                    console.info(data);
                });
            });
        }
        /*REGISTRO INVENTARIO FISICO*/
        if ($scope.tipo_inventario_fisico.val == true) {
                        
            modal.mensajeConfirmacion($scope, "Seguro que desea registrar los Bienes al Inventario Fisico ?", function () {
                var request = crud.crearRequest('ingresos', 1, 'registrar_inventario_fisico');
                request.setData($scope.inventario_inicial);
                //console.log(request, "inv fisico");
                //return;
                crud.insertar("/controlPatrimonial", request, function (response) {
                    
                    if (response.responseSta) {
                        modal.mensaje("CONFIRMACION", response.responseMsg);
                        $scope.limpiar_inventario();
                    }
                }, function (data) {
                    console.info(data);
                });
            });
        }

    }

    $scope.listar_bienes_inventario = function () {
        var request = crud.crearRequest('ingresos', 1, 'listar_inventario_inicial');
        console.log(request);
        return;
        crud.listar("/controlPatrimonial", request, function (data) {
            console.log(data);
            if (data.data) {
                $scope.tabla_inventario_reporte = data.data;
                setting.dataset = data.data;
                iniciarPosiciones(setting.dataset);
                $scope.tabla_inventario_inicial.settings(setting);
                $scope.tabla_inventario_inicial.reload();
            }
        }, function (data) {
            console.info(data);
        });
    }


    $scope.reporte_inventario = function () {

        $scope.listar_bienes_inventario();
        return;
        var size = $scope.tabla_inventario_reporte.length;
        for (var i = 0; i < size; i++) {
            var fecha = $scope.tabla_inventario_reporte[i].fec_reg;

            var año = parseInt(fecha.substr(0, 4));
            var mes = parseInt((fecha.substr(4, 4)).substr(1, 2));
            var dia = parseInt((fecha.substr(7, 8)).substr(1, 2));

            var nueva_fecha = dia + "/" + (mes) + "/" + año;
            $scope.tabla_inventario_reporte[i].fec_reg = nueva_fecha;

        }
        var request = crud.crearRequest('ingresos', 1, 'reporte_inventario_inicial');
        request.setData($scope.tabla_inventario_reporte);
        console.log(request);
        return;
        crud.listar("/controlPatrimonial", request, function (data) {
            $scope.dataBase64 = data.data[0].datareporte;
            window.open($scope.dataBase64);
        }, function (data) {
            console.info(data);
        });

    }

    $scope.listar_tipos_movimiento = function () {
        var request = crud.crearRequest('ingresos', 1, 'listar_tipo_movimiento');
        crud.listar("/controlPatrimonial", request, function (data) {
            if (data.data) {
                $scope.movimientos_ingreso = data.data;
                flag_listas = 3;
                if (flag_listas == 3 && ($scope.inventario_update.tipo_inv == "Inventario Inicial" || $scope.inventario_update.tipo_inv == "Inventario Fisico"))
                {
                    if ($scope.inventario_update.tipo_inv == "Inventario Inicial") {
                        $scope.buscar_inventario_inicial($scope.inventario_update.id_inv, $scope.inventario_update.org_id);
                        localStorage.removeItem('update_inventario');
                    }

                    if ($scope.inventario_update.tipo_inv == "Inventario Fisico") {

                        $scope.buscar_inventario_fisico($scope.inventario_update.id_inv, $scope.inventario_update.org_id);
                        localStorage.removeItem('update_inventario');

                    }

                }
            }

        }, function (data) {
            console.info(data);
        });

    }


    $scope.listar_controles_patrimoniales = function (orgId) {

        var request = crud.crearRequest('configuracion_patrimonial', 1, 'listar_control_patrimonial');
        request.setData({org_id: orgId});
        crud.listar("/controlPatrimonial", request, function (data) {
            if (data.data) {
                flag_listas = 2;
                $scope.controles_patrimoniales = data.data;
                $scope.listar_tipos_movimiento();

            }
        }, function (data) {
            console.info(data);
        });

    }

    $scope.elegir_inv_inicial = function () {
        $scope.tipo_inventario_inicial.val = true;
        $scope.tipo_inventario_fisico.val = false;
    }

    $scope.elegir_inv_fisico = function () {
        $scope.tipo_inventario_fisico.val = true;
        $scope.tipo_inventario_inicial.val = false;
    }

    $scope.cargarDatosInventario = function (data, tip_inv) {
//        /*MOVIMIENTO INGRESOS*/
        
        $rootScope.showLoading();
                
        $scope.movimiento.cont_pat_id = data.dat_mov.cont_pat_id;
        $scope.movimiento.num_res = data.dat_mov.num_res;
//
        fec_reg = new Date(data.dat_mov.fec_res);
        $scope.movimiento.fec_res = fec_reg;
        $scope.movimiento.obs = data.dat_mov.obs;
        $scope.movimiento.mov_ing_id = data.dat_mov.mov_ing_id;
//        /*********************/
//
        /*CABECERA INVENTARIO INICIAL*/
        
        $scope.inventario_inicial_cab.inv_ini_id = data.dat_inv.inv_id;
        $scope.inventario_inicial_cab.actualizar_inventario = true;
        if (tip_inv == 'I') {
            $scope.tipo_inventario_inicial.val = true;
            $scope.tipo_inventario_fisico.val = false;
        }
        if (tip_inv == 'F') {
            $scope.tipo_inventario_inicial.val = false;
            $scope.tipo_inventario_fisico.val = true;
        }
        $scope.accion_inventario = "ACTUALIZAR INVENTARIO";
        /****************************/
        /*DETALLE INVENTARIO INICIAL*/
        console.log(data.det_inv);
        var size = data.det_inv.length;
        for (var i = 0; i < size; i++) {
            $scope.nuevo_bien = data.det_inv[i];
            insertarElemento(array_bienes, $scope.nuevo_bien);
        }
        
        $timeout(function () {
            $scope.obtenerTipMovIng(data.dat_mov.tip_mov_ing);
            $scope.obtenerControl(data.dat_mov.cont_pat_id);
            $scope.registrar_bienes();
            
            $rootScope.hideLoading();
        }, 3000);
        
        /****************************/
    }

    $scope.buscarInventarioInicial = function (id_inv_inicial, orgId) {
        var request = crud.crearRequest('ingresos', 1, 'obtener_inventario_inicial');
        request.setData({id_inv: id_inv_inicial, org_id: orgId});
        var tip_inv = 'I';
        crud.listar("/controlPatrimonial", request, function (response) {
            
            if (response.responseSta) {
                $scope.cargarDatosInventario(response.data, tip_inv);
            }

        }, function (data) {
            console.info(data);
        }); 

    }
    $scope.buscarInventarioFisico = function (id_inv_fis, orgId) {
        var request = crud.crearRequest('ingresos', 1, 'obtener_inventario_fisico');
        request.setData({id_inv: id_inv_fis, org_id: orgId});
        var tip_inv = 'F';
        crud.listar("/controlPatrimonial", request, function (response) {
            console.log(response);
            if (response.responseSta) {
                $scope.cargarDatosInventario(response.data, tip_inv);
            }
        }, function (data) {
            console.info(data);
        }); 
    }
    
    

    $scope.obtenerTipMovIng = function (TipMovId) {

        var size = $scope.movimientos_ingreso.length;
        for (var i = 0; i < size; i++) {
            if ($scope.movimientos_ingreso[i].tip_mov_id == TipMovId) {
                $scope.lista.movimiento = $scope.movimientos_ingreso[i];
                $scope.movimiento.tip_mov_ing = TipMovId;
                break;
            }
        }
    }

    $scope.obtenerControl = function (cpId) {

        var size = $scope.controles_patrimoniales.length;
        for (var i = 0; i < size; i++) {
            if ($scope.controles_patrimoniales[i].cp_id == cpId) {
                $scope.lista.cont_pat = $scope.controles_patrimoniales[i];
                $scope.movimiento.cont_pat_id = cpId;

                break;
            }
        }
    }


    $scope.eliminarBienInventario = function (ItemInv) {

        modal.mensajeConfirmacion($scope, "Seguro que desea eliminar el Bien Mueble del Inventario ? ", function () {
            eliminarElemento(setting.dataset, ItemInv.i);
            iniciarPosiciones(setting.dataset);
            $scope.tabla_inventario_inicial.settings(setting);
            $scope.tabla_inventario_inicial.reload();
            $scope.actualizar_bienes(ItemInv);
        });
    }
    
    $scope.editarBienInventario = function(itemInv) {
        
        console.log($scope.movimiento);
        //return;
        
        console.log($scope.tabla_inventario_inicial.data); 
        localStorage.setItem("tabla_inventario", JSON.stringify($scope.tabla_inventario_inicial.data));
        localStorage.setItem("inv_data_mov", JSON.stringify($scope.movimiento));
        localStorage.setItem("bien_mueble", JSON.stringify(itemInv));
        localStorage.setItem("prev_page", "/inventario_inicial");
        $location.path( "/registro_bienes" );
    }
    
    /*Funcion que Actualiza Bienes(deselecciona de la tabla)*/
    $scope.actualizar_bienes = function (ItemBie) {
        var size = setting2.dataset.length;
        for (var i = 0; i < size; i++) {

            if (setting2.dataset[i].cod_bie == ItemBie.cod_bie) {
                setting.dataset
                setting2.dataset[i].tip_bie = false;
                break;
            }

        }
    }   

    $scope.registrarNuevoBien = function () {
        console.log($scope.tabla_inventario_inicial.data, "new item");
        //return;
        localStorage.setItem("tabla_inventario", JSON.stringify($scope.tabla_inventario_inicial.data));
        localStorage.setItem("prev_page", "/inventario_inicial");
        $location.path( "/registro_bienes" );
    }
    
    $scope.verificarStorageInv = function () {
         
        var tabla_inventario = JSON.parse(localStorage.getItem("tabla_inventario"));
        
        if (tabla_inventario != null && tabla_inventario.length != 0) {
            //console.log(tabla_inventario);
            setting.dataset = tabla_inventario;
            iniciarPosiciones(setting.dataset);
            $scope.tabla_inventario_inicial.settings(setting);
            $scope.tabla_inventario_inicial.reload();
        }
    }
    
    $scope.limpiar_inventario = function() {               
        
        /*Datos Movmiento Ingresos*/
        $scope.movimiento = {
            mov_ing_id: 0, /*OJO : Solo para Actualizar */
            tip_mov_ing: 0,
            cont_pat_id: 0,
            num_res: "",
            fec_res: new Date(),
            obs: ""
        };
        
        $scope.lista = {
            movimiento: "",
            cont_pat: ""
        };
        
        $scope.tabla_inventario_inicial.settings().dataset = [];
        $scope.tabla_inventario_inicial.reload();
        
        $scope.mostrar_bienes_muebles(ORG_ID);
    
        console.log($scope.inventario_inicial); 
        
        localStorage.removeItem("tabla_inventario");
        localStorage.removeItem("prev_page");
    }
    
    $scope.editarInventario = false;
    $scope.verificarStorageEditarInv(); //Verifica si queremos editar un inventario
    $scope.verificarStorageInv();    //Verifica si tenemos un detalle de inventario guardado
    
 }]);


