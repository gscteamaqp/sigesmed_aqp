app.controller("reporte_cpCtrl", ["$scope", "NgTableParams", "crud", "modal", function ($scope, NgTableParams, crud, modal) {

    $scope.minDateInicio = new Date();
    $scope.maxDateInicio;
    $scope.minDateCierre = new Date();

    /*Anexos*/
    $scope.anexos = {};

    /*TABLA DE AMBIENTES*/
    var params = {count: 10};
    var setting = {counts: []};
    $scope.tabla_ambientes = new NgTableParams(params, setting);

    /*TABLA DE CONTROLES PATRIMONIALES*/
    var params2 = {count: 10};
    var setting2 = {counts: []};
    $scope.tabla_control_patrimonial = new NgTableParams(params2, setting2);

    /*TABLA DE ARCHIVOS*/
    var params3 = {count: 10};
    var setting3 = {counts: []};
    $scope.tabla_archivos = new NgTableParams(params3, setting3);


    /*Datos del Reporte*/
    $scope.reporte = {
        fec_ini_md: new Date(),
        fec_fin_md: new Date(),
        fec_ini: "",
        fec_fin: "",
        ane_id: "",
        cod_amb: "",
        cod_pat: "",
        desc_amb: "",
        desc_pat: "",
        est: "",
        org_id: 0
    };

    /*Condicion*/
    $scope.condicion = [
        {cond_id: "B", cond_nom: "BUENO"},
        {cond_id: "M", cond_nom: "MALO"},
        {cond_id: "R", cond_nom: "REGULAR"}
    ];

    /*FUNCIONES ADICIONALES*/
    $scope.listar_anexos = function () {
        var request = crud.crearRequest('configuracion_patrimonial', 1, 'listar_anexos');

        crud.listar("/controlPatrimonial", request, function (response) {
            if (response.responseSta) {
                $scope.anexos = response.data;
            }
        }, function (data) {
            console.info(data);
        });
    };

    $scope.mostrarAmbientes = function (orgId) {
        var request = crud.crearRequest('ingresos', 1, 'listar_ambientes');
        request.setData({org_id: orgId});
        crud.listar("/controlPatrimonial", request, function (response) {
            if (response.responseSta) {
                setting.dataset = response.data;
                iniciarPosiciones(setting.dataset);
                $scope.tabla_ambientes.settings(setting);
                $scope.tabla_ambientes.reload();
            }
        }, function (data) {
            console.info(data);
        });
    }

    $scope.mostrarControlPatrimonial = function (orgId) {
        var request = crud.crearRequest('configuracion_patrimonial', 1, 'listar_control_patrimonial');
        request.setData({org_id: orgId});
        crud.listar("/controlPatrimonial", request, function (response) {
            if (response.responseSta) {
                setting.dataset = response.data;
                iniciarPosiciones(setting.dataset);
                $scope.tabla_ambientes.settings(setting);
                $scope.tabla_ambientes.reload();
            }
        }, function (data) {
            console.info(data);
        });
    }

    $scope.ver_reporte = function (orgId) {

        $scope.reporte.fec_ini = convertirFecha($scope.reporte.fec_ini_md);
        $scope.reporte.fec_fin = convertirFecha($scope.reporte.fec_fin_md);
        $scope.reporte.org_id = orgId;

        /* Verificamos que todos los campos esten llenados */
        var accept = true;
        for (var atrib in $scope.reporte) {

            if ($scope.reporte[atrib] == "") {

                accept = false;
                //break;
            }
        }
        if (accept == true) { /*Todos los campos estan llenados correctamente*/

            var request = crud.crearRequest('ingresos', 1, 'reporte_bienes_cp');
            request.setData($scope.reporte);
            crud.listar("/controlPatrimonial", request, function (data) {
                $scope.dataBase64 = data.data[0].datareporte;
                window.open($scope.dataBase64);
            }, function (data) {
                console.info(data);
            });
        } else {
            modal.mensaje("ERROR", "Verifique que todos los campos esten llenados");
        }

    }

    $scope.mostar_ambientes = function (orgId) {
        var request = crud.crearRequest('ingresos', 1, 'listar_ambientes');
        request.setData({org_id: orgId});
        crud.listar("/controlPatrimonial", request, function (response) {
            if (response.responseSta) {
                setting.dataset = response.data;
                iniciarPosiciones(setting.dataset);
                $scope.tabla_ambientes.settings(setting);
                $scope.tabla_ambientes.reload();
            }
        }, function (data) {
            console.info(data);
        });
    }

    $scope.mostrar_control = function (orgId) {

        var request = crud.crearRequest('configuracion_patrimonial', 1, 'listar_control_patrimonial');
        request.setData({org_id: orgId});
        crud.listar("/controlPatrimonial", request, function (response) {
            if (response.responseSta) {
                setting2.dataset = response.data;
                iniciarPosiciones(setting2.dataset);
                $scope.tabla_control_patrimonial.settings(setting2);
                $scope.tabla_control_patrimonial.reload();
            }
        }, function (data) {
            console.info(data);
        });
    }

    $scope.elegir_ambiente = function (amb) {

        $scope.reporte.cod_amb = amb.amb_id;
        $scope.reporte.desc_amb = amb.amb_des;
        $('#modalListarAmbientes').modal('hide');

    }

    $scope.elegir_control = function (cp) {
        $scope.reporte.cod_pat = cp.cp_id;
        $scope.reporte.desc_pat = cp.obs;
        $('#modalListarControl').modal('hide');
    }

    $scope.mostrar_archivos = function (orgId) {
        var request = crud.crearRequest('ingresos', 1, 'listar_archivos_bienes');
        request.setData({org_id: orgId});
        crud.listar("/controlPatrimonial", request, function (response) {
            if (response.responseSta) {
                console.log(response);
                setting3.dataset = response.data;
                iniciarPosiciones(setting3.dataset);
                $scope.tabla_archivos.settings(setting3);
                $scope.tabla_archivos.reload();
            }
        }, function (data) {
            console.info(data);
        });

    }

    $scope.seleccion_fecha_inicial = function () {

        $scope.minDateCierre = $scope.reporte.fec_ini_md;
        $scope.reporte.fec_fin_md = $scope.reporte.fec_ini_md;
    }

    $scope.seleccion_fecha_cierre = function () {

        $scope.maxDateInicio = $scope.reporte.fec_fin_md;

    }

}]);