/* global modal */

app.controller("documentosGeneradosCtrl", ["$rootScope", "$scope", "NgTableParams", "crud", "modal", function ($rootScope, $scope, NgTableParams, crud, modal) {



        //Implenetacion del controlador


        var params = {count: 10};
        var setting = {counts: []};
        $scope.miTabla = new NgTableParams(params, setting);

        $scope.alineacion = [{id: 1, title: 'Centrado'}, {id: 2, title: 'Justificado'}, {id: 3, title: 'Izquierda'}, {id: 4, title: 'Derecha'}];
        $scope.dataBase64 = "";
        $scope.nombreTipoDocumento = {};
        $scope.tipoDocumentos = [];
        $scope.documentoSeleccionado = {
            docId: 0,
            plaId:null,
            fechaRegistro:"",
            tipoFir: null,
            descripcion: "",
            tipoDocumentoID: 0,
            contenidoPlantilla: [{nombreLetra: null, size: 0, contenido: "", alineacion: {}, isBold: false, isCursiva: false,isSubrayado:false}],
            personaID: $rootScope.usuMaster.usuario.ID,
            marcaAgua:false,
            verImagen:false,
            isSaved:false,
            documento:{nombreArchivo:"",archivo:{},url:""}
        };
        $scope.contenidoDocumento=[{}];
        $scope.contenidoPlantilla=[{}];

        $scope.listarDocumentos = function () {

            //preparamos un objeto request
            var request = crud.crearRequest('ver_documento', 1, 'listarDocumentos');
            request.setData({organizacionID:$rootScope.usuMaster.organizacion.organizacionID});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las funciones de exito y error
            crud.listar("/documentosComunicacion", request, function (data) {
                setting.dataset = data.data;
                iniciarPosiciones(setting.dataset);
                $scope.miTabla.settings(setting); 
                
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.listarNombresLetra = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('generarPlantilla', 1, 'listarPropLetras');

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las organizaciones de exito y error
            crud.listar("/documentosComunicacion", request, function (data) {
                $scope.letras = data.data;
                $scope.documentoSeleccionado.contenidoPlantilla[0].nombreLetra = $scope.letras[0];
                $scope.documentoSeleccionado.contenidoPlantilla[1].nombreLetra = $scope.letras[0];
                $scope.documentoSeleccionado.contenidoPlantilla[2].nombreLetra = $scope.letras[0];
//                $scope.iniciarDatos();
            }, function (data) {
                console.info(data);
            });

        };
        
        $scope.listarTipoDocumentos = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('tramite_datos', 1, 'listarTipoDocumentos');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las organizaciones de exito y error
            crud.listar("/tramiteDocumentario", request, function (data) {
               
                data.data.forEach(function(item){
                    var o = item;
                    o.id = item.tipoDocumentoID;
                    o.title = item.nombre;
                    $scope.tipoDocumentos.push(o);
                    $scope.iniciarDatos();
                });
              //  $scope.listarDocumentos();
            }, function (data) {
                console.info(data);
            });
        };

        $scope.listarTipoDocumentos();

        $scope.generar = function (t)
        {
            
            $scope.documentoSeleccionado.docId=t.documentoId;    
            
            var request = crud.crearRequest('ver_documento', 1, 'traerContenidoDocumento');
            request.setData({documentoId:$scope.documentoSeleccionado.docId});
            crud.listar("/documentosComunicacion", request, function (data) {
               $scope.contenidoDocumento=data.data;
               for(var i=0;i<$scope.contenidoDocumento.length;i++)
               {
                   $scope.documentoSeleccionado.contenidoPlantilla[i].contenido=$scope.contenidoDocumento[i].contenido;
               }
               $scope.generarFormatos(t);
            }, function (data) {
                console.info(data);
            });

        };
        
        $scope.generarFormatos = function (t)
        {
            $scope.documentoSeleccionado.plaId=t.plantillaId;           
            var request = crud.crearRequest('generarDocumento', 1, 'traerPlantilla2Editar');
            request.setData({plantillaID:$scope.documentoSeleccionado.plaId});
            crud.listar("/documentosComunicacion", request, function (data) {
               $scope.contenidoPlantilla=data.data;
               for(var i=0;i<$scope.contenidoPlantilla.length;i++)
               {
                   $scope.documentoSeleccionado.contenidoPlantilla[i].nombreLetra=$scope.letras[$scope.contenidoPlantilla[i].letraId-1];
                   $scope.documentoSeleccionado.contenidoPlantilla[i].alineacion=$scope.alineacion[$scope.contenidoPlantilla[i].alineacion-1];
                   $scope.documentoSeleccionado.contenidoPlantilla[i].isBold=$scope.contenidoPlantilla[i].isBold;
                   $scope.documentoSeleccionado.contenidoPlantilla[i].isCursiva=$scope.contenidoPlantilla[i].isCursiva;
                   $scope.documentoSeleccionado.contenidoPlantilla[i].isSubrayado=$scope.contenidoPlantilla[i].isSubrayado;
                   $scope.documentoSeleccionado.contenidoPlantilla[i].size=$scope.contenidoPlantilla[i].tamanho;
                       
               }
                if (typeof t.nombreArchivo == "undefined") {
                    $scope.documentoSeleccionado.verImagen = false;
                }
                else
                {
                    $scope.documentoSeleccionado.verImagen = true;
                    $scope.documentoSeleccionado.isSaved = false;
                    $scope.documentoSeleccionado.nombreArchivo = t.nombreArchivo;
                }
                $scope.vistaPreliminar();
            }, function (data) {
                console.info(data);
            });
        };

        $scope.vistaPreliminar = function ()
        {
    
            
            var request = crud.crearRequest('generarPlantilla', 1, 'vistaPreliminar');
            request.setData($scope.documentoSeleccionado);

            crud.listar("/documentosComunicacion", request, function (data) {
               
                $scope.dataBase64 = data.data[0].datareporte;
                window.open($scope.dataBase64);
            }, function (data) {
                console.info(data);
            });

        };
//        
        
        $scope.iniciarDatos = function () {
            $scope.listarNombresLetra();

            $scope.documentoSeleccionado.contenidoPlantilla = [{}, {}, {}];
            $scope.documentoSeleccionado.contenidoPlantilla[0].contenido="";
            $scope.documentoSeleccionado.contenidoPlantilla[0].alineacion = $scope.alineacion[2];
            $scope.documentoSeleccionado.contenidoPlantilla[0].isBold = false;
            $scope.documentoSeleccionado.contenidoPlantilla[0].isCursiva = false;
            $scope.documentoSeleccionado.contenidoPlantilla[0].isSubrayado = false;
            $scope.documentoSeleccionado.contenidoPlantilla[0].size=11;
            
            $scope.documentoSeleccionado.contenidoPlantilla[1].contenido="";
            $scope.documentoSeleccionado.contenidoPlantilla[1].alineacion = $scope.alineacion[2];
            $scope.documentoSeleccionado.contenidoPlantilla[1].isBold = false;
            $scope.documentoSeleccionado.contenidoPlantilla[1].isCursiva = false;
            $scope.documentoSeleccionado.contenidoPlantilla[1].isSubrayado = false;
            $scope.documentoSeleccionado.contenidoPlantilla[1].size=11;
            
            $scope.documentoSeleccionado.contenidoPlantilla[2].contenido="";
            $scope.documentoSeleccionado.contenidoPlantilla[2].alineacion = $scope.alineacion[2];
            $scope.documentoSeleccionado.contenidoPlantilla[2].isBold = false;
            $scope.documentoSeleccionado.contenidoPlantilla[2].isCursiva = false;
            $scope.documentoSeleccionado.contenidoPlantilla[2].isSubrayado = false;
            $scope.documentoSeleccionado.contenidoPlantilla[2].size=11;
        };
    }]);
