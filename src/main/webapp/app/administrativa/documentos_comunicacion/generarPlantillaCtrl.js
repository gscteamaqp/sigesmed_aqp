app.controller("generarPlantillaCtrl", ["$rootScope","$scope", "NgTableParams", '$route', "crud", "modal", function ($rootScope,$scope, NgTableParams, $route, crud, modal) {

        $scope.tipoDocumentos = [{tipoDocumentoID:0,nombre:"",descripcion:"",fecha:"",estado:""}];
        
        $scope.isNegrita={};
        $scope.isCursiva={};
        $scope.alineacion=[{id:1, title:'Centrado'},{id:2, title:'Justificado'},{id:3, title:'Izquierda'},{id:4, title:'Derecha'}];
        $scope.letras = [{idLetra:0,nomLetra:""}];
        $scope.tamanhos = [{tamanho: ""}];
        $scope.verificado = {};
        $scope.contenidos = [];
        $scope.cabeceraPlantilla = {};
        $scope.nuevaPlantilla = {
            tipoDoc: null,
            descripcion: "",
            contenidoPlantilla: [{nombreLetra:null,size:0, contenido:"", alineacion: null, isBold:false,isCursiva:false,isSubrayado:false}],
            personaID:$rootScope.usuMaster.usuario.ID,
            marcaAgua:true,
            verImagen:false,
            imagenes :[],
            isSaved:true
        };
        $scope.imagenes = [];
        $scope.verImagen={nombreArchivo:"",archivo:{}};
        $scope.dataBase64 = "";
        $scope.imagen = {descripcion:"",nombreArchivo:"",archivo:{},edi:false};
        $scope.listarNombresLetra = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('generarPlantilla', 1, 'listarPropLetras');
           
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las organizaciones de exito y error
            crud.listar("/documentosComunicacion", request, function (data) {
                $scope.letras = data.data;
                $scope.nuevaPlantilla.contenidoPlantilla[0].nombreLetra = $scope.letras[0];
                $scope.nuevaPlantilla.contenidoPlantilla[1].nombreLetra = $scope.letras[0];
                $scope.nuevaPlantilla.contenidoPlantilla[2].nombreLetra = $scope.letras[0];
//                $scope.iniciarDatos();
            }, function (data) {
                console.info(data);
            });
            
        };
        
//        $scope.listarNombresLetra();
        
        $scope.listarTamLetra = function (l) {
            //preparamos un objeto request
            var request = crud.crearRequest('generarPlantilla', 1, 'listarPropLetras');
            request.setData(l);
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las organizaciones de exito y error
            crud.listar("/documentosComunicacion", request, function (data) {
                $scope.tamanhos = data.data;
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.listarTipoDocumentos = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('tramite_datos', 1, 'listarTipoDocumentos');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las organizaciones de exito y error
            crud.listar("/tramiteDocumentario", request, function (data) {
                $scope.tipoDocumentos = data.data;
            }, function (data) {
                console.info(data);
            });
        };
   
        $scope.continuarCabecera = function () {
            if ($scope.nuevaPlantilla.tipoDoc != null  && $scope.nuevaPlantilla.descripcion != "")
            {

                $scope.cabeceraPlantilla = false;
                $scope.contenidos[0] = true;
            }
            else
            {
                modal.mensaje("ERROR", "Ingrese todos los datos del Paso 1");
            }
        };

        $scope.continuarEncabezado = function () {

            if ($scope.nuevaPlantilla.contenidoPlantilla[0].contenido == "")
            {
                modal.mensaje("ADVERTENCIA", "No se registro texto");
                $scope.nuevaPlantilla.contenidoPlantilla[0].contenido = "";
            }

            $scope.contenidos[0] = false;
            $scope.contenidos[1] = true;


        };

        $scope.cancelarEncabezado = function ()
        {
            $scope.nuevaPlantilla.contenidoPlantilla[0].contenido = "";
        };

        $scope.continuarCuerpo = function () {
            if ($scope.nuevaPlantilla.contenidoPlantilla[1].contenido == "")
            {
                modal.mensaje("ADVERTENCIA", "No se registro texto");
                $scope.nuevaPlantilla.contenidoPlantilla[1].contenido = "";
            }


            $scope.contenidos[1] = false;
            $scope.contenidos[2] = true;

        };

        $scope.cancelarCuerpo = function ()
        {
            $scope.nuevaPlantilla.contenidoPlantilla[1].contenido = "";
        };

        $scope.continuarDespedida = function () {
            if ($scope.nuevaPlantilla.contenidoPlantilla[2].contenido == "")
            {
                modal.mensaje("ADVERTENCIA", "No se registro texto");
                $scope.nuevaPlantilla.contenidoPlantilla[2].contenido = "";
            }
            $scope.contenidos[2] = false;
            $scope.verificado = true;

        };

        $scope.cancelarDespedida = function ()
        {
            $scope.nuevaPlantilla.contenidoPlantilla[2].contenido = "";
        };

        $scope.cancelarEdicion = function ()
        {
            $route.reload();
        };

        $scope.guardar = function ()
        {
            
            modal.mensajeConfirmacion($scope, "Esta seguro que desea generar la plantilla?", function () {
                $scope.nuevaPlantilla.imagenes=$scope.imagenes;
                var request = crud.crearRequest('generarPlantilla', 1, 'nuevaPlantilla');
                request.setData($scope.nuevaPlantilla);
                
                console.log(request);

                crud.insertar("/documentosComunicacion", request, function (data) {
                    modal.mensaje("CONFIRMACION", data.responseMsg);
                    $route.reload();
                }, function (data) {
                    console.info(data);
                });
            });
        }
        
        $scope.ver = function ()
        {
            if (!angular.equals($scope.verImagen, {nombreArchivo: "", archivo: {}}))
            {
                var request = crud.crearRequest('generarPlantilla', 1, 'ingresarImagenTemporal');
                request.setData($scope.verImagen);

                crud.insertar("/documentosComunicacion", request, function (data) {
                    modal.mensaje("CONFIRMACION", data.responseMsg);
                    $scope.nuevaPlantilla.verImagen = true;
                    $scope.vistaPreliminar();
                    }, function (data) {
                        console.info(data);
                    });
                 }
                 else
                 {
                     $scope.vistaPreliminar();
                 }
                
        };
        
   
        $scope.vistaPreliminar = function ()
        {
            var request = crud.crearRequest('generarPlantilla', 1, 'vistaPreliminar');
            request.setData($scope.nuevaPlantilla);

            crud.listar("/documentosComunicacion", request, function (data) {
                modal.mensaje("CONFIRMACION", data.responseMsg);
                $scope.dataBase64 = data.data[0].datareporte;
                window.open($scope.dataBase64);
            }, function (data) {
                console.info(data);
            });
        };

        $scope.editarImagen = function (i, d) {
            if (d.edi) {
                $scope.imagenes[i] = d.copia;

            }
            else {
                d.copia = JSON.parse(JSON.stringify(d));
                d.edi = true;
            }
        };
        
        $scope.agregarImagen = function () {
           
            if ($scope.imagen.descripcion == "") {
                modal.mensaje("CONFIRMACION", "Ingrese descripcion de la Imagen");
                return;
            }
            if ($scope.imagen.nombreArchivo=="") {
                modal.mensaje("CONFIRMACION", "Ingrese Imagen");
                return;
            }
            $scope.imagenes.push($scope.imagen);
            $scope.imagen = {descripcion: "", nombreArchivo: "", archivo: {}, edi: false};
        };
        
        $scope.eliminarImagen = function (i, d) {
            //si estamso cancelando la edicion
            if (d.edi) {
                d.edi = false;
                //delete d.copia;
                d.copia = null;
            }
            //si queremos eliminar el elemento
            else {
                $scope.imagenes.splice(i, 1);
            }
        };
        
        $scope.iniciarDatos = function () {
            $scope.listarTipoDocumentos();
            $scope.listarNombresLetra();
            $scope.cabeceraPlantilla = true;
            $scope.verificado = false;
            $scope.nuevaPlantilla.contenidoPlantilla=[{},{},{}];
            $scope.nuevaPlantilla.contenidoPlantilla[0].contenido = "";
            $scope.nuevaPlantilla.contenidoPlantilla[1].contenido = "";
            $scope.nuevaPlantilla.contenidoPlantilla[2].contenido = "";
            $scope.nuevaPlantilla.contenidoPlantilla[0].isBold = false;
            $scope.nuevaPlantilla.contenidoPlantilla[1].isBold = false;
            $scope.nuevaPlantilla.contenidoPlantilla[2].isBold = false;
            $scope.nuevaPlantilla.contenidoPlantilla[0].isCursiva = false;
            $scope.nuevaPlantilla.contenidoPlantilla[1].isCursiva = false;
            $scope.nuevaPlantilla.contenidoPlantilla[2].isCursiva = false;
            $scope.nuevaPlantilla.contenidoPlantilla[0].isSubrayado = false;
            $scope.nuevaPlantilla.contenidoPlantilla[1].isSubrayado = false;
            $scope.nuevaPlantilla.contenidoPlantilla[2].isSubrayado = false;
            $scope.nuevaPlantilla.contenidoPlantilla[0].alineacion = $scope.alineacion[2];
            $scope.nuevaPlantilla.contenidoPlantilla[1].alineacion = $scope.alineacion[2];
            $scope.nuevaPlantilla.contenidoPlantilla[2].alineacion = $scope.alineacion[2];
            $scope.nuevaPlantilla.contenidoPlantilla[0].size = 10;
            $scope.nuevaPlantilla.contenidoPlantilla[1].size = 10;
            $scope.nuevaPlantilla.contenidoPlantilla[2].size = 10;
            $scope.nuevaPlantilla.marcaAgua=true;
            
        };
        //Encabezado
        $scope.botonAlineacionIzqEnc = function () {
            $scope.nuevaPlantilla.contenidoPlantilla[0].alineacion = $scope.alineacion[2];
        };
        $scope.botonAlineacionCenEnc = function () {
            $scope.nuevaPlantilla.contenidoPlantilla[0].alineacion = $scope.alineacion[0];
        };
        $scope.botonAlineacionDerEnc = function () {
            $scope.nuevaPlantilla.contenidoPlantilla[0].alineacion = $scope.alineacion[3];
        };
        $scope.botonAlineacionJusEnc = function () {
            $scope.nuevaPlantilla.contenidoPlantilla[0].alineacion = $scope.alineacion[1];
        };

        $scope.botonBoldEnc = function () {
            if(!$scope.nuevaPlantilla.contenidoPlantilla[0].isBold)
            $scope.nuevaPlantilla.contenidoPlantilla[0].isBold = true;
            else
                $scope.nuevaPlantilla.contenidoPlantilla[0].isBold = false;
        };
        $scope.botonCursivaEnc = function () {
            if(!$scope.nuevaPlantilla.contenidoPlantilla[0].isCursiva)
            $scope.nuevaPlantilla.contenidoPlantilla[0].isCursiva = true;
            else
                $scope.nuevaPlantilla.contenidoPlantilla[0].isCursiva = false;
        };
        $scope.botonSubrayadoEnc = function () {
            if(!$scope.nuevaPlantilla.contenidoPlantilla[0].isSubrayado)
            $scope.nuevaPlantilla.contenidoPlantilla[0].isSubrayado = true;
            else
                $scope.nuevaPlantilla.contenidoPlantilla[0].isSubrayado = false;
        };
        //Cuerpo
        $scope.botonAlineacionIzqCue = function () {
            $scope.nuevaPlantilla.contenidoPlantilla[1].alineacion = $scope.alineacion[2];
        };
        $scope.botonAlineacionCenCue = function () {
            $scope.nuevaPlantilla.contenidoPlantilla[1].alineacion = $scope.alineacion[0];
        };
        $scope.botonAlineacionDerCue = function () {
            $scope.nuevaPlantilla.contenidoPlantilla[1].alineacion = $scope.alineacion[3];
        };
        $scope.botonAlineacionJusCue = function () {
            $scope.nuevaPlantilla.contenidoPlantilla[1].alineacion = $scope.alineacion[1];
        };

        $scope.botonBoldCue = function () {
            if(!$scope.nuevaPlantilla.contenidoPlantilla[1].isBold)
            $scope.nuevaPlantilla.contenidoPlantilla[1].isBold = true;
            else
                $scope.nuevaPlantilla.contenidoPlantilla[1].isBold = false;
        };
        $scope.botonCursivaCue = function () {
            if(!$scope.nuevaPlantilla.contenidoPlantilla[1].isCursiva)
            $scope.nuevaPlantilla.contenidoPlantilla[1].isCursiva = true;
            else
                $scope.nuevaPlantilla.contenidoPlantilla[1].isCursiva = false;
        };
        $scope.botonSubrayadoCue = function () {
            if(!$scope.nuevaPlantilla.contenidoPlantilla[1].isSubrayado)
            $scope.nuevaPlantilla.contenidoPlantilla[1].isSubrayado = true;
            else
                $scope.nuevaPlantilla.contenidoPlantilla[1].isSubrayado = false;
        };
        //Despedida
        $scope.botonAlineacionIzqDes = function () {
            $scope.nuevaPlantilla.contenidoPlantilla[2].alineacion = $scope.alineacion[2];
        };
        $scope.botonAlineacionCenDes = function () {
            $scope.nuevaPlantilla.contenidoPlantilla[2].alineacion = $scope.alineacion[0];
        };
        $scope.botonAlineacionDerDes = function () {
            $scope.nuevaPlantilla.contenidoPlantilla[2].alineacion = $scope.alineacion[3];
        };
        $scope.botonAlineacionJusDes = function () {
            $scope.nuevaPlantilla.contenidoPlantilla[2].alineacion = $scope.alineacion[1];
        };

        $scope.botonBoldDes = function () {
            if(!$scope.nuevaPlantilla.contenidoPlantilla[2].isBold)
            $scope.nuevaPlantilla.contenidoPlantilla[2].isBold = true;
            else
                $scope.nuevaPlantilla.contenidoPlantilla[2].isBold = false;
        };
        $scope.botonCursivaDes = function () {
            if(!$scope.nuevaPlantilla.contenidoPlantilla[2].isCursiva)
            $scope.nuevaPlantilla.contenidoPlantilla[2].isCursiva = true;
            else
                $scope.nuevaPlantilla.contenidoPlantilla[2].isCursiva = false;
        };
        $scope.botonSubrayadoDes = function () {
            if(!$scope.nuevaPlantilla.contenidoPlantilla[2].isSubrayado)
            $scope.nuevaPlantilla.contenidoPlantilla[2].isSubrayado = true;
            else
                $scope.nuevaPlantilla.contenidoPlantilla[2].isSubrayado = false;
        };
    }]);
