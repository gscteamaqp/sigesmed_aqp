var seApp = angular.module('app');
seApp.config(['$routeProvider',function($routeProvider){
    $routeProvider.when('/mi_legajo_personal/:ficha',{
            templateUrl:'administrativa/sistema_escalafon/verMiLegajoPersonal.html',
            controller:'verMiLegajoPersonalCtrl',
            controllerAs:'verMiLegajoCtrl'
        });
}]);
app.controller('verMiInformacionEscalafonariaCtrl', ["$location", '$scope', '$rootScope', '$http', 'NgTableParams', 'crud', 'modal', function ($location, $scope, $rootScope, $http, NgTableParams, crud, modal) {
        //Variables y funciones para controlar la vista
        $scope.optionsDatosPersonales = [true, false, false, false, false];
        $scope.optionsDatosAcademicos = [true, false, false, false, false, false];
        $scope.optionsExperienciaLaboral = [true, false, false];

        $scope.updateOptions = function (numOption, optionsSet) {
            for (var i = 0; i < optionsSet.length; i += 1) {
                optionsSet[i] = false;
            }
            optionsSet[numOption] = true;
        };

        $scope.status_1 = {close: true, statusGD: false, statusLD: false};
        $scope.changeDiscapacidadSi = function () {
            $scope.fichaEscalafonariaSel.perDis = true;
        };
        $scope.changeDiscapacidadNo = function () {
            $scope.fichaEscalafonariaSel.perDis = false;
        };

        $scope.statusDA = false;
        $scope.changeDASi = function () {
            $scope.statusDA = false;
        };
        $scope.changeDANo = function () {
            $scope.statusDA = true;
        };

        function changeAseguradoState(codAutEss) {
            if (codAutEss !== "               ")
                $scope.asegurado = "SI";
            else
                $scope.asegurado = "NO";
        }
        ;

        function changeSistemaPensionesState(sisPen) {
            if (sisPen === "ONP")
                $scope.optionsSP = "NO";
            else
                $scope.optionsSP = "SI";
        }
        ;

        function changeDishabilityState(perDis) {
            if (perDis === true)
                $scope.discapacidad = "SI";
            else
                $scope.discapacidad = "NO";
        }
        ;

        $scope.parsearBooleano = function (i) {
            var o = "";
            if (i) {
                o = "Si";
            } else {
                o = "No";
            }
            return o;
        };

        $scope.parsearBooleano2 = function (i) {
            var o = "";
            if (i) {
                o = "Habilitado";
            } else {
                o = "No Habilitado";
            }
            return o;
        };

        $scope.sexo = [{id: "F", title: "Femenino"}, {id: "M", title: "Masculino"}];
        $scope.estadoCivil = [{id: "S", title: "Soltero(a)"}, {id: "C", title: "Casado(a)"}];
        $scope.tipoTrabajador = [{id: "N", title: "Nombrado"}, {id: "V", title: "Contratado"}];
        $scope.grupoOcupacional = [{id: "A", title: "Administrativo"}, {id: "D", title: "Docente"}];

        $rootScope.paramsMisParientes = {count: 10};
        $rootScope.settingMisParientes = {counts: []};
        $rootScope.tablaMisParientes = new NgTableParams($rootScope.paramsMisParientes, $rootScope.settingMisParientes);

        $rootScope.paramsMisForEdu = {count: 10};
        $rootScope.settingMisForEdu = {counts: []};
        $rootScope.tablaMisForEdu = new NgTableParams($rootScope.paramsMisForEdu, $rootScope.settingMisForEdu);

        $rootScope.paramsMisEstCom = {count: 10};
        $rootScope.settingMisEstCom = {counts: []};
        $rootScope.tablaMisEstCom = new NgTableParams($rootScope.paramsMisEstCom, $rootScope.settingMisEstCom);

        $rootScope.paramsMisExpPon = {count: 10};
        $rootScope.settingMisExpPon = {counts: []};
        $rootScope.tablaMisExpPon = new NgTableParams($rootScope.paramsMisExpPon, $rootScope.settingMisExpPon);

        $rootScope.paramsMisPublicaciones = {count: 10};
        $rootScope.settingMisPublicaciones = {counts: []};
        $rootScope.tablaMisPublicaciones = new NgTableParams($rootScope.paramsMisPublicaciones, $rootScope.settingMisPublicaciones);

        $rootScope.paramsMisDesplazamientos = {count: 10};
        $rootScope.settingMisDesplazamientos = {counts: []};
        $rootScope.tablaMisDesplazamientos = new NgTableParams($rootScope.paramsMisDesplazamientos, $rootScope.settingMisDesplazamientos);

        $rootScope.paramsMisColegiaturas = {count: 10};
        $rootScope.settingMisColegiaturas = {counts: []};
        $rootScope.tablaMisColegiaturas = new NgTableParams($rootScope.paramsMisColegiaturas, $rootScope.settingMisColegiaturas);

        $rootScope.paramsMisAscensos = {count: 10};
        $rootScope.settingMisAscensos = {counts: []};
        $rootScope.tablaMisAscensos = new NgTableParams($rootScope.paramMisAscensos, $rootScope.settingMisAscensos);

        $rootScope.paramsMisCapacitaciones = {count: 10};
        $rootScope.settingMisCapacitaciones = {counts: []};
        $rootScope.tablaMisCapacitaciones = new NgTableParams($rootScope.paramsMisCapacitaciones, $rootScope.settingMisCapacitaciones);

        $rootScope.paramsMisReconocimientos = {count: 10};
        $rootScope.settingMisReconocimientos = {counts: []};
        $rootScope.tablaMisReconocimientos = new NgTableParams($rootScope.paramsMisReconocimientos, $rootScope.settingMisReconocimientos);

        $rootScope.paramsMisEstPos = {count: 10};
        $rootScope.settingMisEstPos = {counts: []};
        $rootScope.tablaMisEstPos = new NgTableParams($rootScope.paramsMisEstPos, $rootScope.settingMisEstPos);

        $rootScope.paramsMisDemeritos = {count: 10};
        $rootScope.settingMisDemeritos = {counts: []};
        $rootScope.tablaMisDemeritos = new NgTableParams($rootScope.paramsMisDemeritos, $rootScope.settingMisDemeritos);
        
        var tiposFormacion = [
            {id: "1", title: "Estudios Basicos Regulares"},
            {id: "2", title: "Estudios Tecnicos"},
            {id: "3", title: "Bachiller"},
            {id: "4", title: "Universitario"},
            {id: "5", title: "Licenciatura"},
            {id: "6", title: "Maestria"},
            {id: "7", title: "Doctorado"},
            {id: "8", title: "Otros"}
        ];

        var tiposEstCom = [
            {id: "1", title: "Informatica"},
            {id: "2", title: "Idiomas"},
            {id: "3", title: "Certificación"},
            {id: "4", title: "Diplomado"},
            {id: "5", title: "Especialización"},
            {id: "6", title: "Otros"}
        ];

        var niveles = [
            {id: "N", title: "Ninguno"},
            {id: "B", title: "Básico"},
            {id: "I", title: "Intermedio"},
            {id: "A", title: "Avanzado"}
        ];

        var tiposEstPos = [{id: "1", title: "Maestria"}, {id: "2", title: "Doctorado"}];

        var tiposDesplazamientos = [
            {id: "1", title: "Designación"},
            {id: "2", title: "Rotación"},
            {id: "3", title: "Reasignación"},
            {id: "4", title: "Destaque"},
            {id: "5", title: "Permuta"},
            {id: "6", title: "Encargo"},
            {id: "7", title: "Comisión de servicio"},
            {id: "8", title: "Transferencia"},
            {id: "9", title: "Otros"}
        ];

        var motivos = [
            {id: "1", title: "Mérito"},
            {id: "2", title: "Felicitación"},
            {id: "3", title: "25 años de servicio"},
            {id: "4", title: "30 años de servicio"},
            {id: "5", title: "Luto y sepelio"},
            {id: "6", title: "Otros"}
        ];
        
        var direcciones = [];
        
        function listarDirecciones() {
            var request = crud.crearRequest('datos_personales', 1, 'listarDirecciones');
            request.setData({perId: $scope.personaSel.perId});
            crud.listar('/sistema_escalafon', request, function (response) {
                direcciones = response.data;
                changeOptionsDirectionsState();
            }, function (data) {
                console.info(data);
            });
        }
        function changeOptionsDirectionsState() {
            switch (direcciones.length) {
                case 1:
                    $scope.optionsDirections = "SI";
                    $scope.status_1.statusGD = true;
                    $scope.direccionDNISel.dirId = direcciones[0].dirId;
                    $scope.direccionDNISel.dirId = direcciones[0].dirId;
                    $scope.direccionDNISel.tipDir = direcciones[0].tipDir;
                    $scope.direccionDNISel.nomDir = direcciones[0].nomDir;
                    $scope.direccionDNISel.depDirDes = direcciones[0].depDirDes;
                    $scope.direccionDNISel.proDirDes = direcciones[0].proDirDes;
                    $scope.direccionDNISel.disDirDes = direcciones[0].disDirDes;
                    $scope.direccionDNISel.nomZon = direcciones[0].nomZon;
                    $scope.direccionDNISel.desRef = direcciones[0].desRef;
                    break;

                case 2:
                    $scope.optionsDirections = "NO";
                    $scope.status_1.statusGD = true;
                    $scope.status_1.statusLD = true;
                    $scope.direccionDNISel.dirId = direcciones[0].dirId;
                    $scope.direccionDNISel.dirId = direcciones[0].dirId;
                    $scope.direccionDNISel.tipDir = direcciones[0].tipDir;
                    $scope.direccionDNISel.nomDir = direcciones[0].nomDir;
                    $scope.direccionDNISel.depDirDes = direcciones[0].depDirDes;
                    $scope.direccionDNISel.proDirDes = direcciones[0].proDirDes;
                    $scope.direccionDNISel.disDirDes = direcciones[0].disDirDes;
                    $scope.direccionDNISel.nomZon = direcciones[0].nomZon;
                    $scope.direccionDNISel.desRef = direcciones[0].desRef;

                    $scope.direccionDASel.dirId = direcciones[1].dirId;
                    $scope.direccionDASel.dirId = direcciones[1].dirId;
                    $scope.direccionDASel.tipDir = direcciones[1].tipDir;
                    $scope.direccionDASel.nomDir = direcciones[1].nomDir;
                    $scope.direccionDASel.depDirDes = direcciones[1].depDirDes;
                    $scope.direccionDASel.proDirDes = direcciones[1].proDirDes;
                    $scope.direccionDASel.disDirDes = direcciones[1].disDirDes;
                    $scope.direccionDASel.nomZon = direcciones[1].nomZon;
                    $scope.direccionDASel.desRef = direcciones[1].desRef;
                    break;
            }
        }

        $scope.personaSel = {
            perId: 0,
            apePat: "",
            apeMat: "",
            nom: "",
            dni: "",
            fecNac: "",
            num1: "",
            num2: "",
            fij: "",
            email: "",
            sex: "",
            estCiv: "",
            depNac: "",
            proNac: "",
            disNac: "",
            estado: 'A'
        };

        $scope.direccionDNISel = {
            tipDir: "R",
            nomDir: "",
            depDir: "",
            proDir: "",
            disDir: "",
            nomZon: "",
            desRef: ""
        };
        $scope.direccionDASel = {
            tipDir: "A",
            nomDir: "",
            depDir: "",
            proDir: "",
            disDir: "",
            nomZon: "",
            desRef: ""
        };

        $scope.trabajadorSel = {
            traId: "",
            traCon: ""
        };

        $scope.fichaEscalafonariaSel = {
            ficEscId: 0,
            autEss: "",
            sisPen: "",
            nomAfp: "",
            codCuspp: "",
            fecIngAfp: "",
            perDis: false,
            regCon: "",
            gruOcu: ""
        };
        
        function obtenerInformacionEscalafonaria() {
            var request = crud.crearRequest('datos_personales', 1, 'buscarFichaPorUsuId');
            request.setData({usuId: $scope.usuMaster.usuario.usuarioID, opcion: 1});
            crud.listar('/sistema_escalafon', request, function (response) {
                if (response.responseSta) {
                    $http.get('../recursos/json/departamentos.json').success(function (data) {
                        $scope.departamentosLugNac = data;
                        $scope.departamentosDirDNI = data;
                        $scope.departamentosDirDA = data;
                    });
        
                    var datosPersonales = response.data;
                    $scope.personaSel.perId = datosPersonales.persona.perId;
                    listarDirecciones();
                    $scope.personaSel.apePat = datosPersonales.persona.apePat;
                    $scope.personaSel.apeMat = datosPersonales.persona.apeMat;
                    $scope.personaSel.nom = datosPersonales.persona.nom;
                    $scope.personaSel.dni = datosPersonales.persona.dni;
                    $scope.personaSel.fecNac = new Date(datosPersonales.persona.fecNac);
                    $scope.personaSel.num1 = datosPersonales.persona.num1;
                    $scope.personaSel.num2 = datosPersonales.persona.num2;
                    $scope.personaSel.fij = datosPersonales.persona.fij;
                    $scope.personaSel.email = datosPersonales.persona.email;
                    $scope.personaSel.sex = datosPersonales.persona.sex;
                    $scope.personaSel.estCiv = datosPersonales.persona.estCiv;
                    $scope.personaSel.depNacDes = datosPersonales.persona.depNacDes;
                    $scope.personaSel.proNacDes = datosPersonales.persona.proNacDes; 
                    $scope.personaSel.disNacDes = datosPersonales.persona.disNacDes;

                    $scope.trabajadorSel.traId = datosPersonales.trabajador.traId;
                    $scope.trabajadorSel.traCon = datosPersonales.trabajador.traCon;
                    
                    $scope.fichaEscalafonariaSel.ficEscId = datosPersonales.ficha.ficEscId;
                    $scope.fichaEscalafonariaSel.autEss = datosPersonales.ficha.autEss;
                    $scope.fichaEscalafonariaSel.sisPen = datosPersonales.ficha.sisPen;
                    $scope.fichaEscalafonariaSel.nomAfp = datosPersonales.ficha.nomAfp;
                    $scope.fichaEscalafonariaSel.codCuspp = datosPersonales.ficha.codCuspp;
                    $scope.fichaEscalafonariaSel.fecIngAfp = new Date(datosPersonales.ficha.fecIngAfp);
                    $scope.fichaEscalafonariaSel.perDis = datosPersonales.ficha.perDis;
                    $scope.fichaEscalafonariaSel.regCon = datosPersonales.ficha.regCon;
                    $scope.fichaEscalafonariaSel.gruOcu = datosPersonales.ficha.gruOcu;
                    
                    changeAseguradoState($scope.fichaEscalafonariaSel.autEss);
                    changeSistemaPensionesState($scope.fichaEscalafonariaSel.sisPen);
                    changeDishabilityState($scope.fichaEscalafonariaSel.perDis);
                    
                    listarParientes();
                    listarFormacionesEducativas();
                    listarColegiaturas();
                    listarEstudiosComplementarios();
                    listarEstudiosPostgrado();
                    listarExposiciones();
                    listarPublicaciones();
                    listarDesplazamientos();
                    listarAscensos();
                    listarCapacitaciones();
                    listarReconocimientos();
                    listarDemeritos();
                    
                } else {
                    modal.mensaje('ERROR', response.responseMsg);
                }
            }, function (data) {
                console.info(data);
            });
        };
        obtenerInformacionEscalafonaria();
        
        
        

        //Funciones para listas los datos de tablas
        function listarParientes() {
            //preparamos un objeto request
            var request = crud.crearRequest('familiares', 1, 'listarParientes');
            request.setData($scope.trabajadorSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingMisParientes.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingMisParientes.dataset);
                $rootScope.tablaMisParientes.settings($rootScope.settingMisParientes);
            }, function (data) {
                console.info(data);
            });
        };

        function listarFormacionesEducativas() {
            //preparamos un objeto request
            var request = crud.crearRequest('formacion_educativa', 1, 'listarFormacionesEducativas');
            request.setData($scope.fichaEscalafonariaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item) {
                    item.estConDes = item.estCon ? "Si" : "No";
                    item.tipDes = buscarContenido(tiposFormacion, 'id', 'title', item.tip);
                });
                $rootScope.settingMisForEdu.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingMisForEdu.dataset);
                $rootScope.tablaMisForEdu.settings($rootScope.settingMisForEdu);
            }, function (data) {
                console.info(data);
            });
        };

        function listarColegiaturas() {
            //preparamos un objeto request
            var request = crud.crearRequest('colegiatura', 1, 'listarColegiaturas');
            request.setData($scope.fichaEscalafonariaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item) {
                    item.conRegDes = item.conReg ? "Habilitado" : "No habilitado";
                });
                $rootScope.settingMisColegiaturas.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingMisColegiaturas.dataset);
                $rootScope.tablaMisColegiaturas.settings($rootScope.settingMisColegiaturas);
            }, function (data) {
                console.info(data);
            });
        };

        function listarEstudiosComplementarios () {
            //preparamos un objeto request
            var request = crud.crearRequest('estudio_complementario', 1, 'listarEstudiosComplementarios');
            request.setData($scope.fichaEscalafonariaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item) {
                    item.tipDes = buscarContenido(tiposEstCom, 'id', 'title', item.tip);
                    item.nivDes = buscarContenido(niveles, 'id', 'title', item.niv);
                });
                $rootScope.settingMisEstCom.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingMisEstCom.dataset);
                $rootScope.tablaMisEstCom.settings($rootScope.settingMisEstCom);
            }, function (data) {
                console.info(data);
            });
        };

        function listarEstudiosPostgrado() {
            //preparamos un objeto request
            var request = crud.crearRequest('estudio_postgrado', 1, 'listarEstudiosPostgrado');
            request.setData($scope.fichaEscalafonariaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item) {
                    item.tipDes = buscarContenido(tiposEstPos, 'id', 'title', item.tip);
                });
                $rootScope.settingMisEstPos.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingMisEstPos.dataset);
                $rootScope.tablaMisEstPos.settings($rootScope.settingMisEstPos);
            }, function (data) {
                console.info(data);
            });
        };

        function listarExposiciones() {
            //preparamos un objeto request
            var request = crud.crearRequest('exposicion_ponencia', 1, 'listarExposiciones');
            request.setData($scope.fichaEscalafonariaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingMisExpPon.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingMisExpPon.dataset);
                $rootScope.tablaMisExpPon.settings($rootScope.settingMisExpPon);
            }, function (data) {
                console.info(data);
            });
        };

        function listarPublicaciones() {
            //preparamos un objeto request
            var request = crud.crearRequest('publicacion', 1, 'listarPublicaciones');
            request.setData($scope.fichaEscalafonariaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingMisPublicaciones.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingMisPublicaciones.dataset);
                $rootScope.tablaMisPublicaciones.settings($rootScope.settingMisPublicaciones);
            }, function (data) {
                console.info(data);
            });
        };

        function listarDesplazamientos() {
            //preparamos un objeto request
            var request = crud.crearRequest('desplazamiento', 1, 'listarDesplazamientos');
            request.setData($scope.fichaEscalafonariaSel);
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item) {
                    item.tipDes = buscarContenido(tiposDesplazamientos, 'id', 'title', item.tip);
                });
                $rootScope.settingMisDesplazamientos.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingMisDesplazamientos.dataset);
                $rootScope.tablaMisDesplazamientos.settings($rootScope.settingMisDesplazamientos);
            }, function (data) {
                console.info(data);
            });
        };

        function listarAscensos() {
            //preparamos un objeto request
            var request = crud.crearRequest('ascenso', 1, 'listarAscensos');
            request.setData($scope.fichaEscalafonariaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingMisAscensos.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingMisAscensos.dataset);
                $rootScope.tablaMisAscensos.settings($rootScope.settingMisAscensos);
            }, function (data) {
                console.info(data);
            });
        };

        function listarCapacitaciones() {
            //preparamos un objeto request
            var request = crud.crearRequest('capacitacion', 1, 'listarCapacitaciones');
            request.setData($scope.fichaEscalafonariaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingMisCapacitaciones.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingMisCapacitaciones.dataset);
                $rootScope.tablaMisCapacitaciones.settings($rootScope.settingMisCapacitaciones);
            }, function (data) {
                console.info(data);
            });
        };

        function listarReconocimientos() {
            //preparamos un objeto request
            var request = crud.crearRequest('reconocimientos', 1, 'listarReconocimientos');
            request.setData($scope.fichaEscalafonariaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item) {
                    item.motDes = buscarContenido(motivos, 'id', 'title', item.mot);
                });
                $rootScope.settingMisReconocimientos.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingMisReconocimientos.dataset);
                $rootScope.tablaMisReconocimientos.settings($rootScope.settingMisReconocimientos);
            }, function (data) {
                console.info(data);
            });
        };

        function listarDemeritos() {
            //preparamos un objeto request
            var request = crud.crearRequest('demeritos', 1, 'listarDemeritos');
            request.setData($scope.fichaEscalafonariaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item) {
                    item.sepDes = item.sep ? "Si" : "No";
                });
                $rootScope.settingMisDemeritos.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingMisDemeritos.dataset);
                $rootScope.tablaMisDemeritos.settings($rootScope.settingMisDemeritos);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.verFichaEscalafonaria = function(){                
            var request = crud.crearRequest('reportes',1,'reporteFichaEscalafonaria');
            request.setData({
                ficEscId:$scope.fichaEscalafonariaSel.ficEscId, 
                traId:$scope.trabajadorSel.traId, 
                perDni:$scope.personaSel.dni
            });
            crud.insertar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    verDocumento(response.data.file);
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde');
            }); 
        };
        
        $scope.prepararVerMiLegajoPersonal = function () {

            var request = crud.crearRequest('datos_personales',1,'buscarFichaPorDNI');
            request.setData({perDni:$scope.personaSel.dni, opcion:200});
            crud.listar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    $location.url('/mi_legajo_personal/'+btoa(JSON.stringify(response.data)));
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function (data) {
                console.info(data);
            });
        };
    }]);
app.controller('verMiLegajoPersonalCtrl', ['$routeParams', '$rootScope', '$scope', 'crud', 'NgTableParams', 'modal', function ($routeParams, $rootScope, $scope, crud, NgTableParams, modal) {
        var datos = JSON.parse(atob($routeParams.ficha));
        

        var categorias = [
            {idCat: "1", idSubCat: "1", subCatDes: "Datos personales"},
            {idCat: "1", idSubCat: "2", subCatDes: "Nacimiento"},
            {idCat: "1", idSubCat: "3", subCatDes: "Salud"},
            {idCat: "1", idSubCat: "4", subCatDes: "Direccion"},
            {idCat: "2", idSubCat: "1", subCatDes: "Datos familiares"},
            {idCat: "3", idSubCat: "1", subCatDes: "Formacion educativa"},
            {idCat: "3", idSubCat: "2", subCatDes: "Colegiatura"},
            {idCat: "3", idSubCat: "3", subCatDes: "Estudios complementarios"},
            {idCat: "3", idSubCat: "4", subCatDes: "Estudios postgrado"},
            {idCat: "3", idSubCat: "5", subCatDes: "Exposicion y/o Ponencias"},
            {idCat: "3", idSubCat: "6", subCatDes: "Publicaciones"},
            {idCat: "4", idSubCat: "1", subCatDes: "Desplazamientos"},
            {idCat: "4", idSubCat: "2", subCatDes: "Ascensos"},
            {idCat: "4", idSubCat: "3", subCatDes: "Capacitaciones"},
            {idCat: "5", idSubCat: "1", subCatDes: "Reconocimientos"},
            {idCat: "6", idSubCat: "1", subCatDes: "Demeritos"},
            {idCat: "7", idSubCat: "1", subCatDes: "Otros"}
        ];

        $rootScope.paramsMiLegDatosPersonales = {count: 10};
        $rootScope.settingMiLegDatosPersonales = {counts: []};
        $rootScope.tablaMiLegDatosPersonales = new NgTableParams($rootScope.paramsMiLegDatosPersonales, $rootScope.settingMiLegDatosPersonales);

        $rootScope.paramsMiLegDatosFamiliares = {count: 10};
        $rootScope.settingMiLegDatosFamiliares = {counts: []};
        $rootScope.tablaMiLegDatosFamiliares = new NgTableParams($rootScope.paramsMiLegDatosFamiliares, $rootScope.settingMiLegDatosFamiliares);

        $rootScope.paramsMiLegDatosAcademicos = {count: 10};
        $rootScope.settingMiLegDatosAcademicos = {counts: []};
        $rootScope.tablaMiLegDatosAcademicos = new NgTableParams($rootScope.paramsMiLegDatosAcademicos, $rootScope.settingMiLegDatosAcademicos);

        $rootScope.paramsMiLegExperienciaLaboral = {count: 10};
        $rootScope.settingMiLegExperienciaLaboral = {counts: []};
        $rootScope.tablaMiLegExperienciaLaboral = new NgTableParams($rootScope.paramsMiLegExperienciaLaboral, $rootScope.settingMiLegExperienciaLaboral);

        $rootScope.paramsMiLegReconocimientos = {count: 10};
        $rootScope.settingMiLegReconocimientos = {counts: []};
        $rootScope.tablaMiLegReconocimientos = new NgTableParams($rootScope.paramsMiLegReconocimientos, $rootScope.settingMiLegReconocimientos);

        $rootScope.paramsMiLegDemeritos = {count: 10};
        $rootScope.settingMiLegDemeritos = {counts: []};
        $rootScope.tablaMiLegDemeritos = new NgTableParams($rootScope.paramsMiLegDemeritos, $rootScope.settingMiLegDemeritos);

        $rootScope.paramsMiLegOtros = {count: 10};
        $rootScope.settingMiLegOtros = {counts: []};
        $rootScope.tablaMiLegOtros = new NgTableParams($rootScope.paramsMiLegOtros, $rootScope.settingMiLegOtros);

        var legDatosPersonales = [];
        var legDatosFamiliares = [];
        var legDatosAcademicos = [];
        var legExperienciaLaboral = [];
        var legReconocimientos = [];
        var legDemeritos = [];
        var legOtros = [];

        listarLegajos();
        function listarLegajos() {
            //preparamos un objeto request
            var request = crud.crearRequest('legajo_personal', 1, 'listarLegajos');
            request.setData(datos);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item) {
                    var subCategorias = buscarObjetos(categorias, 'idCat', item.catLeg);
                    item.subCatDes = buscarContenido(subCategorias, 'idSubCat', 'subCatDes', item.subCat);

                    switch (item.catLeg) {
                        case '1':
                            legDatosPersonales.push(item);
                            break;
                        case '2':
                            legDatosFamiliares.push(item);
                            break;
                        case '3':
                            legDatosAcademicos.push(item);
                            break;
                        case '4':
                            legExperienciaLaboral.push(item);
                            break;
                        case '5':
                            legReconocimientos.push(item);
                            break;
                        case '6':
                            legDemeritos.push(item);
                            break;
                        default:
                            legOtros.push(item);
                    }
                });

                $rootScope.settingMiLegDatosPersonales.dataset = legDatosPersonales;
                iniciarPosiciones($rootScope.settingMiLegDatosPersonales.dataset);
                $rootScope.tablaMiLegDatosPersonales.settings($rootScope.settingMiLegDatosPersonales);

                $rootScope.settingMiLegDatosFamiliares.dataset = legDatosFamiliares;
                iniciarPosiciones($rootScope.settingMiLegDatosFamiliares.dataset);
                $rootScope.tablaMiLegDatosFamiliares.settings($rootScope.settingMiLegDatosFamiliares);

                $rootScope.settingMiLegDatosAcademicos.dataset = legDatosAcademicos;
                iniciarPosiciones($scope.settingMiLegDatosAcademicos.dataset);
                $rootScope.tablaMiLegDatosAcademicos.settings($rootScope.settingMiLegDatosAcademicos);

                $scope.settingMiLegExperienciaLaboral.dataset = legExperienciaLaboral;
                iniciarPosiciones($rootScope.settingMiLegExperienciaLaboral.dataset);
                $rootScope.tablaMiLegExperienciaLaboral.settings($rootScope.settingMiLegExperienciaLaboral);

                $rootScope.settingMiLegReconocimientos.dataset = legReconocimientos;
                iniciarPosiciones($rootScope.settingMiLegReconocimientos.dataset);
                $rootScope.tablaMiLegReconocimientos.settings($rootScope.settingMiLegReconocimientos);

                $rootScope.settingMiLegDemeritos.dataset = legDemeritos;
                iniciarPosiciones($rootScope.settingMiLegDemeritos.dataset);
                $rootScope.tablaMiLegDemeritos.settings($rootScope.settingMiLegDemeritos);

                $rootScope.settingMiLegOtros.dataset = legOtros;
                iniciarPosiciones($rootScope.settingMiLegOtros.dataset);
                $rootScope.tablaMiLegOtros.settings($rootScope.settingMiLegOtros);


            }, function (data) {
                console.info(data);
            });

        };
    }]);


