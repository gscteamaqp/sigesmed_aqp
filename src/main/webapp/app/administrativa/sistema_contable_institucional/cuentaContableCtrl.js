app.requires.push('angularModalService');
app.requires.push('ngAnimate');



app.controller("cuentaContableCtrl", ["$scope", "$rootScope", "NgTableParams", "crud", "modal", "ModalService", function ($scope, $rootScope, NgTableParams, crud, modal, ModalService) {

        //arreglo donde estan todas las cuentas contables
        $scope.cuentasContables = [];
        //variable que servira para crear una nueva cuenta contable
        $scope.cuentaContable = {cuentaContableID: 0, numero: "", tipo: "", subClase: "", clasificacion: "0", nombre: "", saldo: "0.0", encabezado: false, efectivo: false, impuesto: false, iva: false, operacionID: 0, estado: 'A'};
        //variable temporal, para la seleccion de una cuenta contable
        $scope.cuentaContableSel = {};
        $scope.documento = {nombreArchivo: "", archivo: {}};
        $scope.archivoExcel = [];
        //Variables para manejo de la tabla
        var paramsCuentas = {count: 10, sorting: {numero: "asc"}};
        var settingCuentas = {counts: [], filterOptions: {filterComparator: _.startsWith}};
        $scope.tablaCuentas = new NgTableParams(paramsCuentas, settingCuentas);


        $scope.showNuevaCuenta = function () {


            ModalService.showModal({
                templateUrl: "administrativa/sistema_contable_institucional/agregarCuentaContable.html",
                controller: "agregarNuevaCuentaCtrl",
                inputs: {
                    title: "Nueva Cuenta Contable"
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {

                        $scope.cuentaContable = result.cuenta;
                        //insertamos el elemento a la lista
                        insertarElemento(settingCuentas.dataset, $scope.cuentaContable);
                        $scope.tablaCuentas.reload();

                    }


                });
            });

        };

        $scope.listarCuentas = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('cuentaContable', 1, 'listarCuentaContable');

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistemaContable", request, function (response) {
                console.log(response.toString());
                settingCuentas.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones(settingCuentas.dataset);
                $scope.tablaCuentas.settings(settingCuentas);
                console.log($scope.tablaCuentas);

            }, function (data) {
                console.info(data);
            });
        };

        $scope.prepararEditar = function (t) {
            $scope.cuentaContableSel = JSON.parse(JSON.stringify(t));

            ModalService.showModal({
                templateUrl: "administrativa/sistema_contable_institucional/agregarCuentaContable.html",
                controller: "editarCuentaCtrl",
                inputs: {
                    cuentaContable: $scope.cuentaContableSel,
                    title: "Editar Cuenta Contable"
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                        $scope.cuentaContable = result.cuenta;
                        //insertamos el elemento a la lista
                        settingCuentas.dataset[$scope.cuentaContableSel.i] = $scope.cuentaContable;
                        $scope.tablaCuentas.reload();
                        console.log($scope.cuentaContable);
                    }

                });
            });

        };


        $scope.eliminarCuenta = function (i, idDato) {

            modal.mensajeConfirmacion($scope, "Seguro que desea eliminar este registro", function () {

                var request = crud.crearRequest('cuentaContable', 1, 'eliminarCuentaContable');
                request.setData({cuentaContableID: idDato});

                crud.eliminar("/sistemaContable", request, function (response) {

                    modal.mensaje("CONFIRMACION", response.responseMsg);
                    if (response.responseSta) {
                        eliminarElemento(settingCuentas.dataset, i);
                        $scope.tablaCuentas.reload();
                    }

                }, function (data) {
                    console.info(data);
                });

            });



        };

        $scope.flag = function (t) {
            if (t)
                return "Si";
            return "No";
        };

        $scope.applyGlobalSearch = function (term) {
            $scope.tablaCuentas.filter({$: term});
        };

        $scope.importarCuentas = function () {


            $scope.archivoExcel.push($scope.documento);
            var cadena1 = $scope.documento.nombreArchivo;
            var cadena2 = cadena1.substring(cadena1.length - 5, cadena1.length);
            if (cadena2 != ".xlsx") {
                modal.mensaje("ERROR TIPO DE DOCUMENTO", "SOLO EXTENCION .xlsx ");
                return;
            }
            /*
             console.log("nombre de archivo "+cadena1);
             console.log("nombre de extenciopn "+cadena2);
             console.log($scope.documento.archivo);
             */

            var request = crud.crearRequest('cuentaContable', 1, 'insertarCuentaContableExcel');

            request.setData({documento: $scope.documento});

            crud.insertar("/sistemaContable", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {

                    //recuperamos las variables que nos envio el servidor
                    $('#modalNuevo').modal('hide');

                    $scope.tablaCuentas.reload();
                    $scope.archivoExcel = [];
                    $scope.documento = {nombreArchivo: "", archivo: {}};
                    alert("Importacion correcta");
                }

            }, function (data) {
                console.info(data);
            });

        };


    }]);


app.controller('agregarNuevaCuentaCtrl', [
    '$scope', "$rootScope", '$element', 'title', 'close', 'crud', 'modal', function ($scope, $rootScope, $element, title, close, crud, modal) {
        //variable que servira para crear una nueva cuenta contable
        $scope.cuentaContable = {cuentaContableID: 0, numero: "", tipo: "", subClase: "", clasificacion: "0", nombre: "", saldo: "0.0", encabezado: false, efectivo: false, impuesto: false, iva: false, operacionID: 0, estado: 'A'};

        $scope.title = title;
        //Variables parala clasificacion de Caja  
        $scope.tipoClasificacion = {tipo: '0', descripcion: ""}
        $scope.listaClasificacion = [{tipo: '1', descripcion: "operativo"}, {tipo: '2', descripcion: "inversion"}, {tipo: '3', descripcion: "financiamiento"}];

        //variable para las opciones de la subclase
        $scope.tipoSubClase = {tipo: '0', descripcion: ""}
        $scope.listaTipos = [];

        $scope.agregarCuentaContable = function () {

            var request = crud.crearRequest('cuentaContable', 1, 'insertarCuentaContable');
            $scope.cuentaContable.organizacion = $rootScope.usuMaster.organizacion.organizacionID;
            request.setData($scope.cuentaContable);


            if ($scope.cuentaContable.nombre === "") {
                modal.mensaje("CONFIRMACION", "Ingrese nombre de la Cuenta");
                return;
            }

            if (!$scope.cuentaContable.tipo || !$scope.cuentaContable.subClase) {
                modal.mensaje("CONFIRMACION", "Seleccione el tipo de la Cuenta y su clase");
                return;
            }
            if ($scope.cuentaContable.numero === "") {
                modal.mensaje("CONFIRMACION", "Ingrese numero de la Cuenta");
                return;
            }

            crud.insertar("/sistemaContable", request, function (response) {
                organizacionID:$rootScope.usuMaster.organizacion.organizacionID
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {

                    if (response.data.cuentaContableID) {
                        //recuperamos las variables que nos envio el servidor
                        $scope.cuentaContable = response.data;



                        //  Manually hide the modal.
                        $element.modal('hide');

                        //  Now call close, returning control to the caller.
                        close({
                            cuenta: $scope.cuentaContable,
                            flag: true
                        }, 500); // close, but give 500ms for bootstrap to animate
                    }
                }


            }, function (data) {
                console.info(data);
            });

        };





        //  This close function doesn't need to use jQuery or bootstrap, because
        //  the button has the 'data-dismiss' attribute.
        $scope.close = function () {
            $element.modal('hide');
            close({

                flag: false
            }, 500); // close, but give 500ms for bootstrap to animate
        };

        //  This cancel function must use the bootstrap, 'modal' function because
        //  the doesn't have the 'data-dismiss' attribute.
        $scope.cancel = function () {

            //  Manually hide the modal.
            $element.modal('hide');

            //  Now call close, returning control to the caller.
            close({

                flag: false
            }, 500); // close, but give 500ms for bootstrap to animate
        };


        $scope.listar = function (r) {

            $scope.listaTipos = [];
            if (r === "1") {

                $scope.listaTipos = [
                    {tipo: '1', descripcion: "activo cuenta bancaria"},
                    {tipo: '2', descripcion: "otro activo"}
                ];
                document.getElementById("select1").disabled = false;
                document.getElementById("select4").disabled = true;
                document.getElementById("select6").disabled = true;
                document.getElementById("select7").disabled = true;



            } else if (r === '4') {
                $scope.listaTipos = [
                    {tipo: '1', descripcion: "tarjeta de credito"},
                    {tipo: '2', descripcion: "cuenta de prestamo"},
                    {tipo: '3', descripcion: "otro pasivo"}
                ];

                document.getElementById("select1").disabled = true;
                document.getElementById("select4").disabled = false;
                document.getElementById("select6").disabled = true;
                document.getElementById("select7").disabled = true;

            } else if (r === '6') {
                $scope.listaTipos = [
                    {tipo: '1', descripcion: "gastos operativos"},
                    {tipo: '2', descripcion: "gastos no operativos"},
                    {tipo: '3', descripcion: "costo de ventas"}
                ];

                document.getElementById("select1").disabled = true;
                document.getElementById("select4").disabled = true;
                document.getElementById("select6").disabled = false;
                document.getElementById("select7").disabled = true;

            } else if (r === '7') {
                $scope.listaTipos = [
                    {tipo: '1', descripcion: "ingresos operativos(ventas)"},
                    {tipo: '2', descripcion: "ingresos no operativos"}
                ];

                document.getElementById("select1").disabled = true;
                document.getElementById("select4").disabled = true;
                document.getElementById("select6").disabled = true;
                document.getElementById("select7").disabled = false;

            } else {
                document.getElementById("select1").disabled = true;
                document.getElementById("select4").disabled = true;
                document.getElementById("select6").disabled = true;
                document.getElementById("select7").disabled = true;

            }

            console.log($scope.listaTipos);
        };

        $scope.seleccionar = function (s) {

            if (s === '1') {
                document.getElementById("select4").value = null;
                document.getElementById("select6").value = null;
                document.getElementById("select7").value = null;


            } else if (s === '4') {
                document.getElementById("select1").value = null;
                document.getElementById("select6").value = null;
                document.getElementById("select7").value = null;

            } else if (s === '6') {
                document.getElementById("select1").value = null;
                document.getElementById("select4").value = null;
                document.getElementById("select7").value = null;

            } else if (s === '7') {
                document.getElementById("select1").value = null;
                document.getElementById("select6").value = null;
                document.getElementById("select4").value = null;

            } else {
                document.getElementById("select1").value = null;
                document.getElementById("select4").value = null;
                document.getElementById("select6").value = null;
                document.getElementById("select7").value = null;



            }

        };


    }]);



app.controller('editarCuentaCtrl', [
    '$scope', '$element', 'title', 'cuentaContable', 'close', 'crud', 'modal',
    function ($scope, $element, title, cuentaContable, close, crud, modal) {
        //variable que servira para crear una nueva cuenta contable
        //  $scope.cuentaContable = {cuentaContableID:"",tipo:"0",subClase:"0",clasificacion:"0",nombre:"",saldo:"0.0",encabezado:false,efectivo:false,impuesto:false,iva:false,operacionID:0,estado:'A'};
        $scope.title = title;
        //Variables parala clasificacion de Caja  
        $scope.tipoClasificacion = {tipo: '0', descripcion: ""}
        $scope.listaClasificacion = [{tipo: '1', descripcion: "operativo"}, {tipo: '2', descripcion: "inversion"}, {tipo: '3', descripcion: "financiamiento"}];

        //variable para las opciones de la subclase
        $scope.tipoSubClase = {tipo: '0', descripcion: ""}
        $scope.listaTipos = [];

        $scope.cuentaContable = cuentaContable;



        $scope.actualizarCuentaContable = function () {

            var request = crud.crearRequest('cuentaContable', 1, 'actualizarCuentaContable');

            request.setData($scope.cuentaContable);
            console.log($scope.cuentaContable)
            if ($scope.cuentaContable.nombre === "") {
                modal.mensaje("CONFIRMACION", "Ingrese nombre de la Cuenta");
                return;
            }

            if (!$scope.cuentaContable.tipo || !$scope.cuentaContable.subClase) {
                modal.mensaje("CONFIRMACION", "Seleccione el tipo de la Cuenta y su clase");
                return;
            }
            if ($scope.cuentaContable.numero === "") {
                modal.mensaje("CONFIRMACION", "Ingrese numero de la Cuenta");
                return;
            }


            crud.actualizar("/sistemaContable", request, function (response) {

                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {

                    console.log($scope.cuentaContable)
                    //  Manually hide the modal.
                    $element.modal('hide');

                    //  Now call close, returning control to the caller.
                    close({
                        cuenta: $scope.cuentaContable,
                        flag: true
                    }, 500); // close, but give 500ms for bootstrap to animate
                }

            }, function (data) {
                console.info(data);
            });

        };


        //  This close function doesn't need to use jQuery or bootstrap, because
        //  the button has the 'data-dismiss' attribute.
        $scope.close = function () {
            $element.modal('hide');
            close({

                flag: false
            }, 500); // close, but give 500ms for bootstrap to animate
        };

        //  This cancel function must use the bootstrap, 'modal' function because
        //  the doesn't have the 'data-dismiss' attribute.
        $scope.cancel = function () {

            //  Manually hide the modal.
            $element.modal('hide');

            //  Now call close, returning control to the caller.
            close({

                flag: false
            }, 500); // close, but give 500ms for bootstrap to animate
        };



        $scope.listar = function (r) {
            $scope.cuentaContable.subClase = '';
            $scope.listaTipos = [];
            if (r === '1') {

                $scope.listaTipos = [
                    {tipo: '1', descripcion: "activo cuenta bancaria"},
                    {tipo: '2', descripcion: "otro activo"}
                ];
                document.getElementById("select1").disabled = false;
                document.getElementById("select4").disabled = true;
                document.getElementById("select6").disabled = true;
                document.getElementById("select7").disabled = true;



            } else if (r === '4') {
                $scope.listaTipos = [
                    {tipo: '1', descripcion: "tarjeta de credito"},
                    {tipo: '2', descripcion: "cuenta de prestamo"},
                    {tipo: '3', descripcion: "otro pasivo"}
                ];

                document.getElementById("select1").disabled = true;
                document.getElementById("select4").disabled = false;
                document.getElementById("select6").disabled = true;
                document.getElementById("select7").disabled = true;

            } else if (r === '6') {
                $scope.listaTipos = [
                    {tipo: '1', descripcion: "gastos operativos"},
                    {tipo: '2', descripcion: "gastos no operativos"},
                    {tipo: '3', descripcion: "costo de ventas"}
                ];

                document.getElementById("select1").disabled = true;
                document.getElementById("select4").disabled = true;
                document.getElementById("select6").disabled = false;
                document.getElementById("select7").disabled = true;

            } else if (r === '7') {
                $scope.listaTipos = [
                    {tipo: '1', descripcion: "ingresos operativos(ventas)"},
                    {tipo: '2', descripcion: "ingresos no operativos"}
                ];

                document.getElementById("select1").disabled = true;
                document.getElementById("select4").disabled = true;
                document.getElementById("select6").disabled = true;
                document.getElementById("select7").disabled = false;

            } else {
                document.getElementById("select1").disabled = true;
                document.getElementById("select4").disabled = true;
                document.getElementById("select6").disabled = true;
                document.getElementById("select7").disabled = true;

            }

            console.log($scope.listaTipos);
        };

        $scope.seleccionar = function (s) {

            if (s === '1') {
                document.getElementById("select4").value = null;
                document.getElementById("select6").value = null;
                document.getElementById("select7").value = null;


            } else if (s === '4') {
                document.getElementById("select1").value = null;
                document.getElementById("select6").value = null;
                document.getElementById("select7").value = null;

            } else if (s === '6') {
                document.getElementById("select1").value = null;
                document.getElementById("select4").value = null;
                document.getElementById("select7").value = null;

            } else if (s === '7') {
                document.getElementById("select1").value = null;
                document.getElementById("select6").value = null;
                document.getElementById("select4").value = null;

            } else {
                document.getElementById("select1").value = null;
                document.getElementById("select4").value = null;
                document.getElementById("select6").value = null;
                document.getElementById("select7").value = null;



            }
            console.log($scope.cuentaContable.subClase);
        };

    }]);

