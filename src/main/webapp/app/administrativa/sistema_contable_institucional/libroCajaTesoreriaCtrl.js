app.requires.push('angularModalService');
app.requires.push('ngAnimate');
app.requires.push('ui.bootstrap');


app.controller("libroCajaTesoreriaCtrl", ["$scope", "$filter", "$rootScope", "NgTableParams", "crud", "modal", "ModalService", function ($scope, $filter, $rootScope, NgTableParams, crud, modal, ModalService) {

        //dato del libro  
        $scope.libroEstado = {debe: 0.00, haber: 0.00, saldoMesAnterior: 0.00, saldoMes: 0.00, estado: "", mes: "", mesSel: "", libro: "", enunciado: ""};

        $scope.libroEstado.libro = {libroID: 0, nombre: "", observacion: "", fechaApertura: "", fechaCierre: "", saldoApertura: 0.00, saldoActual: 0.00, organizacionID: 0, personaID: 0, estado: ''};
        $scope.libroEstado.mes = new Date();
        // $scope.libro.mesSel= $scope.libro.mes.getMonth().toString();
        $scope.nombreBusqueda;

        //Variables para manejo de la Operacion del Libro Caja
        var paramsLibroCaja = {count: 10, sorting: {fecha: "asc"}};
        var settingLibroCaja = {counts: []};
        $scope.tablaLibroCaja = new NgTableParams(paramsLibroCaja, settingLibroCaja);
        // $scope.tablaLibroCaja.filter().fecha=$scope.libro.mesSel;

        //Variables para manejo de la tabla
        var paramsOperaciones = {count: 10};
        var settingOperaciones = {counts: []};
        $scope.tablaOperaciones = new NgTableParams(paramsOperaciones, settingOperaciones);


        //Variables para manejo de la tabla
        var paramsClienteProveedor = {count: 10};
        var settingClienteProveedor = {counts: []};
        $scope.tablaClienteProveedor = new NgTableParams(paramsClienteProveedor, settingClienteProveedor);

        //variable para list tipo  de pago
        $scope.tipoPago = [];
        //variable para list tipo  de Area -> centro de costo
        $scope.tipoArea = [];
        //variable para list cuenta efectivo
        $scope.cuentasEfectivo = [];

        //variable para list hechos libro
        $scope.hechosLibroCaja = [];

        //variable para agregar Transaccion a la Tabla del Libro Caja
        $scope.asiento = {operacionID: "", libro: "", glosa: "", importe: "", tipoPago: "", area: "", numeroD: "", fecha: "", clienteProveedor: "", debe: "", Haber: "", observacion: "", estado: ""};

        $scope.asiento.tipoPago = {tipoPagoID: "", nombre: ""};
        $scope.asiento.area = {areaID: "", nombre: ""};
        $scope.asiento.clienteProveedor = {clienteProveedorID: "", datos: ""};
        $scope.asiento.debe = {cuentaContableID: "", nombre: "", importe: ""};
        $scope.asiento.haber = {cuentaContableID: "", nombre: "", importe: ""};

        var Base64 = {_keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", encode: function (e) {
                var t = "";
                var n, r, i, s, o, u, a;
                var f = 0;
                e = Base64._utf8_encode(e);
                while (f < e.length) {
                    n = e.charCodeAt(f++);
                    r = e.charCodeAt(f++);
                    i = e.charCodeAt(f++);
                    s = n >> 2;
                    o = (n & 3) << 4 | r >> 4;
                    u = (r & 15) << 2 | i >> 6;
                    a = i & 63;
                    if (isNaN(r)) {
                        u = a = 64
                    } else if (isNaN(i)) {
                        a = 64
                    }
                    t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
                }
                return t
            }, decode: function (e) {
                var t = "";
                var n, r, i;
                var s, o, u, a;
                var f = 0;
                e = e.replace(/[^A-Za-z0-9+/=]/g, "");
                while (f < e.length) {
                    s = this._keyStr.indexOf(e.charAt(f++));
                    o = this._keyStr.indexOf(e.charAt(f++));
                    u = this._keyStr.indexOf(e.charAt(f++));
                    a = this._keyStr.indexOf(e.charAt(f++));
                    n = s << 2 | o >> 4;
                    r = (o & 15) << 4 | u >> 2;
                    i = (u & 3) << 6 | a;
                    t = t + String.fromCharCode(n);
                    if (u != 64) {
                        t = t + String.fromCharCode(r)
                    }
                    if (a != 64) {
                        t = t + String.fromCharCode(i)
                    }
                }
                t = Base64._utf8_decode(t);
                return t
            }, _utf8_encode: function (e) {
                e = e.replace(/rn/g, "n");
                var t = "";
                for (var n = 0; n < e.length; n++) {
                    var r = e.charCodeAt(n);
                    if (r < 128) {
                        t += String.fromCharCode(r)
                    } else if (r > 127 && r < 2048) {
                        t += String.fromCharCode(r >> 6 | 192);
                        t += String.fromCharCode(r & 63 | 128)
                    } else {
                        t += String.fromCharCode(r >> 12 | 224);
                        t += String.fromCharCode(r >> 6 & 63 | 128);
                        t += String.fromCharCode(r & 63 | 128)
                    }
                }
                return t
            }, _utf8_decode: function (e) {
                var t = "";
                var n = 0;
                var r = c1 = c2 = 0;
                while (n < e.length) {
                    r = e.charCodeAt(n);
                    if (r < 128) {
                        t += String.fromCharCode(r);
                        n++
                    } else if (r > 191 && r < 224) {
                        c2 = e.charCodeAt(n + 1);
                        t += String.fromCharCode((r & 31) << 6 | c2 & 63);
                        n += 2
                    } else {
                        c2 = e.charCodeAt(n + 1);
                        c3 = e.charCodeAt(n + 2);
                        t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
                        n += 3
                    }
                }
                return t
            }}
        //alert($rootScope.usuMaster.usuario.ID..nombre);
        verificaSaldo();
        //alert("org"+$rootScope.usuMaster.organizacion.organizacionID);

        //listarLibroCaja();               



        function  verificaSaldo() {

            //preparamos un objeto request               
            var request = crud.crearRequest('libroCaja', 1, 'saldosDelLibro');
            request.setData({organizacionID: $rootScope.usuMaster.organizacion.organizacionID, personaID: $rootScope.usuMaster.usuario.usuarioID});
            //llamamos al servicio          
            crud.listar("/sistemaContable", request, function (response) {
                //  modal.mensaje("CONFIRMACION",response.responseMsg);
                if (response.responseSta) {
                    if (response.data.estado === 'I' || response.data.estado === 'C' || response.data.estado === 'N') {
                        $scope.libroEstado.estado = response.data.estado;
                        modal.mensaje("CONFIRMACION", response.responseMsg);
                    } else {
                        modal.mensaje("CONFIRMACION", response.responseMsg);
                        $scope.libroEstado = response.data;
                        $scope.libroEstado.mes = new Date();
                        $scope.libroEstado.mesSel = $scope.libroEstado.mes.getMonth().toString();
                        if ($scope.libroEstado.mes.getMonth() < 9)
                            $scope.tablaLibroCaja.filter().fecha = "/0" + (parseFloat($scope.libroEstado.mesSel) + 1) + "/";
                        else
                            $scope.tablaLibroCaja.filter().fecha = "/" + (parseFloat($scope.libroEstado.mesSel) + 1) + "/";



                        settingLibroCaja.dataset = response.data.asientos;
                        $scope.tablaLibroCaja.settings(settingLibroCaja);
                        $scope.cuentasEfectivo = response.data.cuentasEfectivo;
                        $scope.hechosLibroCaja = response.data.hechosLibroCaja;


                        var fechaApertura = new Date($scope.libroEstado.libro.fechaApertura.toString());

                        if (fechaApertura.getMonth() === $scope.libroEstado.mes.getMonth()) {
                            var saldoApertura = 0.00;
                            for (var i = 0; i < $scope.cuentasEfectivo.length; i++)
                                saldoApertura += $scope.cuentasEfectivo[i].saldoApertura;

                            $scope.libroEstado.saldoMesAnterior = saldoApertura.toFixed(2);
                            $scope.libroEstado.enunciado = "SALDO DE APERTURA INICIAL";
                        } else {
                            $scope.libroEstado.enunciado = "SALDO DE MES ANTERIOR";
                        }
                        console.log(response.data);
                        console.log($scope.pages);
                    }

                }

            }, function (data) {
                console.info(data);
            });


        }
        ;


        function listarLibroCaja() {
            //preparamos un objeto request
            var request = crud.crearRequest('libroCaja', 1, 'listarTransaccionesConDocumentosPorOrganizacion');
            request.setData({libroID: $scope.libroEstado.libroID, fecha: $scope.libro.mes});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request             
            crud.listar("/sistemaContable", request, function (response) {

                settingLibroCaja.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                $scope.libro.saldos = iniciarPosicionesYSumar(settingLibroCaja.dataset);
                $scope.libro.saldos.saldo = parseFloat($scope.libro.saldos.saldo) + parseFloat($scope.libroEstado.saldoApertura);
                $scope.libro.saldos.saldo = $scope.libro.saldos.saldo.toFixed(2);
                $scope.tablaLibroCaja.settings(settingLibroCaja);

                console.log($scope.tablaLibroCaja);
                console.log($scope.libro.saldos);
            }, function (data) {
                console.info(data);
            });



        }
        ;

        /*Funcion que busca dentro de un array*/
        function buscarHechos(lista, labelClave, idBuscado, labelClaveF, idBuscadoF) {
            var h = [];
            for (var i = 0; i < lista.length; i++) {
                if (lista[i][labelClave] === idBuscado && lista[i][labelClaveF] >= idBuscadoF)
                    h.push(lista[i]);
            }

            return h;
        }
        /*Funcion que busca dentro de un array*/
        function buscarCuenta(lista, labelClave, idBuscado) {
            for (var i = 0; i < lista.length; i++) {
                if (lista[i][labelClave] === idBuscado)
                    return lista[i];
            }
        }

        /*Funcion que busca dentro de un array y cambia contenido*/
        function buscarCuentaActualizar(lista, labelClave, idBuscado, labelContenido, contenido) {
            for (var i = 0; i < lista.length; i++) {
                if (lista[i][labelClave] === idBuscado)
                    lista[i][labelContenido] = contenido;
            }
        }


        function actualizarLibro(asiento) {
            var cuentaEfectivo;
            var hechoLibro = [];
            var datos = {importe: asiento.importe, tipo: asiento.libro, libro: "", cuentaE: "", hechoL: "", debe: 0.00, haber: 0.00};


            if (asiento.libro === 'C') {
                cuentaEfectivo = buscarCuenta($scope.cuentasEfectivo, 'cuentaContableID', asiento.haber.cuentaContableID);
                hechoLibro = buscarHechos($scope.hechosLibroCaja, 'cuentaContableID', asiento.haber.cuentaContableID, 'fechaMes', $scope.libroEstado.mesSel);

            } else if (asiento.libro === 'V') {
                cuentaEfectivo = buscarCuenta($scope.cuentasEfectivo, 'cuentaContableID', asiento.debe.cuentaContableID);
                hechoLibro = buscarHechos($scope.hechosLibroCaja, 'cuentaContableID', asiento.debe.cuentaContableID, 'fechaMes', $scope.libroEstado.mesSel);

            }

            datos.libro = $scope.libroEstado.libro;
            datos.cuentaE = cuentaEfectivo;
            datos.hechoL = hechoLibro;
            datos.debe = $scope.libroEstado.debe;
            datos.haber = $scope.libroEstado.haber;


            var request = crud.crearRequest('libroCaja', 1, 'actualizarLibroYSaldos');
            request.setData(datos);
            crud.actualizar("/sistemaContable", request, function (response) {

                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    //recuperamos las variables que nos envio el servidor
                    $scope.libroEstado.libro.saldoActual = response.data.saldoActual;
                    $scope.libroEstado.saldoMes = response.data.saldoMes;
                    $scope.libroEstado.debe = response.data.debe;
                    $scope.libroEstado.haber = response.data.haber;

                    buscarCuentaActualizar($scope.cuentasEfectivo, 'cuentaEfectivoID', response.data.cuentaEfectivoID, 'saldoActual', response.data.importeCE);

                    var hechosL = [];
                    hechosL = response.data.hechosL;
                    console.log(hechosL);
                    if (response.data.tipo === 'C') {
                        for (var i = 0; i < hechosL.length; i++)
                            buscarCuentaActualizar($scope.hechosLibroCaja, 'hechosID', hechosL[i]['hechosID'], 'importeH', hechosL[i]['importeH']);
                    } else if (response.data.tipo === 'V') {
                        for (var i = 0; i < hechosL.length; i++)
                            buscarCuentaActualizar($scope.hechosLibroCaja, 'hechosID', hechosL[i]['hechosID'], 'importeD', hechosL[i]['importeD']);
                    }
                }
                console.log($scope.libroEstado);

            }, function (data) {
                console.info(data);
            });

        }
        ;

        $scope.cambiarMes = function (mesSel) {

            var hechos = [];
            var debeM = 0.00, haberM = 0.00, debeMA = 0.00, haberMA = 0.00, saldoMes = 0.00, saldoMesA = 0.00;
            hechos = $scope.hechosLibroCaja;

            var mes = parseInt(mesSel), mesA = mes - 1;

            for (var i = 0; i < hechos.length; i++) {


                if (hechos[i].fechaMes === mes) {
                    debeM += hechos[i].importeD;
                    haberM += hechos[i].importeH;
                } else if (hechos[i].fechaMes === mesA) {
                    debeMA += hechos[i].importeD;
                    haberMA += hechos[i].importeH;
                }
            }
            saldoMes = debeM - haberM;
            saldoMesA = debeMA - haberMA;

            $scope.libroEstado.debe = debeM.toFixed(2);
            $scope.libroEstado.haber = haberM.toFixed(2);
            $scope.libroEstado.saldoMesAnterior = saldoMesA.toFixed(2);
            $scope.libroEstado.saldoMes = saldoMes.toFixed(2);

            if (mesSel < 9) {
                $scope.tablaLibroCaja.filter().fecha = "/0" + (parseFloat($scope.libroEstado.mesSel) + 1) + "/";
                $scope.libroEstado.mesSel = mesSel;
                $scope.libroEstado.mes.setMonth(mesSel);
            } else {
                $scope.tablaLibroCaja.filter().fecha = "/" + (parseFloat($scope.libroEstado.mesSel) + 1) + "/";
                $scope.libroEstado.mesSel = mesSel;
                $scope.libroEstado.mes.setMonth(mesSel);
            }

            var fechaApertura = new Date($scope.libroEstado.libro.fechaApertura.toString());

            if (fechaApertura.getMonth() === $scope.libroEstado.mes.getMonth()) {
                var saldoApertura = 0.00;
                for (var i = 0; i < $scope.cuentasEfectivo.length; i++)
                    saldoApertura += $scope.cuentasEfectivo[i].saldoApertura;

                $scope.libroEstado.saldoMesAnterior = saldoApertura.toFixed(2);
                $scope.libroEstado.enunciado = "SALDO DE APERTURA INICIAL";
            } else {
                $scope.libroEstado.enunciado = "SALDO DE MES ANTERIOR";
            }




        };



        $scope.listarOpe = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('operacion', 1, 'listarOperacion');

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request             
            crud.listar("/sistemaContable", request, function (response) {

                settingOperaciones.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones(settingOperaciones.dataset);
                $scope.tablaOperaciones.settings(settingOperaciones);

            }, function (data) {
                console.info(data);
            });

            console.log($scope.tablaOperaciones);
        };

        $scope.listarClienteProveedor = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('libroCaja', 1, 'listarClienteProveedor');

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request             
            crud.listar("/sistemaContable", request, function (response) {

                settingClienteProveedor.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones(settingClienteProveedor.dataset);
                $scope.tablaClienteProveedor.settings(settingClienteProveedor);

            }, function (data) {
                console.info(data);
            });

            console.log($scope.tablaClienteProveedor);
        };

        listarTipoPago();
        listarAreas();
        function listarTipoPago() {
            //preparamos un objeto request
            var request = crud.crearRequest('libroCaja', 1, 'listarTipoPago');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las funciones de exito y error
            crud.listar("/sistemaContable", request, function (data) {
                $scope.tipoPago = data.data;
                console.info(data.data);
            }, function (data) {
                console.info(data);
            });
        }
        ;

        function listarAreas() {
            //preparamos un objeto request
            var request = crud.crearRequest('area', 1, 'listarAreas');
            crud.listar("/configuracionInicial", request, function (data) {
                $scope.tipoArea = data.data;
                //console.info(data.data);
            }, function (data) {
                console.info(data);
            });
        }
        ;

        // listarCuentasEfectivo();
        function listarCuentasEfectivo() {
            //preparamos un objeto request
            var request = crud.crearRequest('libroCaja', 1, 'listarCuentasEfectivo');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las funciones de exito y error
            crud.listar("/sistemaContable", request, function (data) {
                $scope.cuentasEfectivo = data.data;
            }, function (data) {
                console.info(data);
            });
        }
        ;
        $scope.eliminarCuenta = function (i, t) {
            $scope.asiento = JSON.parse(JSON.stringify(t));
            alert("asiID : " + $scope.asiento.asiId);
            alert("CodUniOpID : " + $scope.asiento.codUniOpeId);

            modal.mensajeConfirmacion($scope, "seguro que desea eliminar el registro de la operacion", function () {
                var request = crud.crearRequest('libroCaja', 1, 'eliminarRegistroTransaccion');


                request.setData({AsientoID: $scope.asiento.asiId, CodUniOpID: $scope.asiento.codUniOpeId});
                crud.eliminar("/sistemaContable", request, function (response) {
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                    if (response.responseSta) {
                        /*console.log("el valor de i es : "+i);
                         console.log($scope.tablaLibroCaja.data);
                         $scope.tablaLibroCaja.data.splice(i,1);
                         $scope.tablaLibroCaja.reload();*/
                        eliminarElemento(settingLibroCaja.dataset, i);
                        iniciarPosiciones(settingLibroCaja.dataset);
                        $scope.tablaLibroCaja.settings(settingLibroCaja);
                        $scope.tablaLibroCaja.reload();
                    }
                }, function (data) {
                    console.info(data);
                });
            });
        };

        $scope.showNuevaTransaccion2 = function () {

            ModalService.showModal({
                templateUrl: "administrativa/sistema_contable_institucional/agregarTransaccion.html",
                controller: "agregarTransaccionCtrl",
                inputs: {
                    title: "Seleccionar Operación",
                    tablaOperacion: $scope.tablaOperaciones,
                    tablaClienteProveedor: $scope.tablaClienteProveedor,
                    listTipoPago: $scope.tipoPago,
                    listArea: $scope.tipoArea,
                    listCuentasE: $scope.cuentasEfectivo,
                    libroID: $scope.libroEstado.libro.libroID,
                    mesSel: $scope.libroEstado.mesSel
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {

                        $scope.asiento = result.data;
                        //$scope.asiento.fecha=new Date($scope.asiento.fecha);             
                        //insertamos el elemento a la lista
                        insertarElemento(settingLibroCaja.dataset, $scope.asiento);
                        $scope.tablaLibroCaja.reload();

                        //funcion que permite actualiza salods de las tablas LibroCaj ,cuentasefectivo y hechos 
                        actualizarLibro($scope.asiento);
                        // Ver contnido del libro y el asiento registrado
                        //     console.log($scope.asiento);
                        //      console.log($scope.libroEstado);
                    }


                });
            });

        };

        $scope.prepararEditar = function (i,t) {

            $scope.asiento = JSON.parse(JSON.stringify(t));
            // alert("asiID : "+ $scope.asiento.asiId);
            //alert("operacioncompraoventaID : "+ $scope.asiento.codUniOpeId);
            if($scope.asiento.tipoPago.tipoPagoID == 1){
                 modal.mensaje("ALERTA","No es Posible la edicion de Recibos Auxiliares");
                 return;
            }
            ModalService.showModal({
                templateUrl: "administrativa/sistema_contable_institucional/editarTransaccion.html",
                controller: "editarTransaccionCtrl",
                inputs: {
                    title: "Datos de la Operación",
                    tablaOperacion: $scope.tablaOperaciones,
                    tablaClienteProveedor: $scope.tablaClienteProveedor,
                    listTipoPago: $scope.tipoPago,
                    listCuentasE: $scope.cuentasEfectivo,
                    libroID: $scope.libroEstado.libro.libroID,
                    asiento: $scope.asiento

                            //   listtipoArea:$scope.tipoArea
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {



                        $scope.asiento = result.data;

                        //insertamos el elemento a la lista
                        //eliminarElemento(settingLibroCaja.dataset, i);
                        //  iniciarPosiciones(settingLibroCaja.dataset);
                     //  insertarElemento(settingLibroCaja.dataset, $scope.asiento);
                      // eliminarElemento(settingLibroCaja.dataset,  $scope.asiento);
                       
                        //iniciarPosiciones(settingLibroCaja.dataset);
                       // $scope.tablaLibroCaja.settings(settingLibroCaja);
                        $scope.tablaLibroCaja.reload();
                        console.log($scope.asiento);

                    }

                });
            });

        };
        $scope.verTransaccion = function (t) {

            $scope.asiento = JSON.parse(JSON.stringify(t));

            ModalService.showModal({
                templateUrl: "administrativa/sistema_contable_institucional/verTransaccion.html",
                controller: "verTransaccionCtrl",
                inputs: {
                    title: "Datos de la Operación",
                    tablaOperacion: $scope.tablaOperaciones,
                    tablaClienteProveedor: $scope.tablaClienteProveedor,
                    listTipoPago: $scope.tipoPago,
                    listCuentasE: $scope.cuentasEfectivo,
                    asiento: $scope.asiento
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {



                        $scope.asiento = result.data;

                        //insertamos el elemento a la lista
                        insertarElemento(settingLibroCaja.dataset, $scope.asiento);
                        $scope.tablaLibroCaja.reload();
                        console.log($scope.asiento);

                    }

                });
            });

        };

        $scope.fecha = function (d) {
            var i = new Date(d.y, d.m, d.d);
            return i;
        };

        $scope.getFilterMonth = function (n) {
            if (n === '0') {
                $scope.tablaLibroCaja.filter().fecha = "Jan";
            } else if (n === '1') {
                $scope.tablaLibroCaja.filter().fecha = "Feb";
            } else if (n === '2') {
                $scope.tablaLibroCaja.filter().fecha = "Mar";
            } else if (n === '3') {
                $scope.tablaLibroCaja.filter().fecha = "Apr";
            } else if (n === '4') {
                $scope.tablaLibroCaja.filter().fecha = "May";
            } else if (n === '5') {
                $scope.tablaLibroCaja.filter().fecha = "Jun";
            } else if (n === '6') {
                $scope.tablaLibroCaja.filter().fecha = "Jul";
            } else if (n === '7') {
                $scope.tablaLibroCaja.filter().fecha = "Agu";
            } else if (n === '8') {
                $scope.tablaLibroCaja.filter().fecha = "Sep";
            } else if (n === '9') {
                $scope.tablaLibroCaja.filter().fecha = "Oct";
            } else if (n === '10') {
                $scope.tablaLibroCaja.filter().fecha = "Nov";
            } else if (n === '11') {
                $scope.tablaLibroCaja.filter().fecha = "Dec";
            } else {
                $scope.tablaLibroCaja.filter().fecha = "";
            }


        };

        //Visualizar de archivos en formato PDF
        $scope.reporte = function () {
            var request = crud.crearRequest('libroCaja', 1, 'reporteLibroCaja');

            request.setData($scope.tablaLibroCaja.settings().dataset);
            crud.insertar("/sistemaContable", request, function (data) {
                $scope.dataBase64 = data.data[0].datareporte;
                window.open($scope.dataBase64);
            }, function (data) {
                console.info(data);
            });
        };



    }]);


app.controller('agregarTransaccionCtrl', [
    '$scope', "$rootScope", '$element', 'libroID', 'mesSel', 'tablaOperacion', 'tablaClienteProveedor', 'listTipoPago', 'listArea', 'listCuentasE', 'title', 'NgTableParams', 'close', 'crud', 'modal', "ModalService",
    function ($scope, $rootScope, $element, libroID, mesSel, tablaOperacion, tablaClienteProveedor, listTipoPago, listArea, listCuentasE, title, NgTableParams, close, crud, modal, ModalService) {
        //variable de la fecha actual para el calendaario
        var fecha = new Date();
        fecha.setMonth(mesSel);

        //variable para agregar Transaccion a la Tabla del Libro Caja
        $scope.asientoE = {operacionID: "", libro: "C", glosa: "", importe: "", tipoPago: "", area: "", numeroD: "", fecha: "", clienteProveedor: "", debe: "", haber: "", doc: "", observacion: "", estado: "", codUniOpeID: "", libroID: libroID};
        $scope.asientoE.tipoPago = {tipoPagoID: "", nombre: ""};
        $scope.asientoE.area = {areaID: "", nombre: ""};
        $scope.asientoE.clienteProveedor = {clienteProveedorID: "", datos: ""};
        $scope.asientoE.debe = {cuentaContableID: "", nombre: ""};
        $scope.asientoE.haber = {cuentaContableID: "", nombre: ""};

        $scope.asientoE.doc = {archivo: {}, edi: false};

        $scope.asientoI = {operacionID: "", libro: "V", glosa: "", importe: "", tipoPago: "", numeroD: "", fecha: "", clienteProveedor: "", debe: "", haber: "", doc: "", observacion: "", estado: "", codUniOpeID: "", libroID: libroID};
        $scope.asientoI.tipoPago = {tipoPagoID: "", nombre: ""};
        $scope.asientoI.clienteProveedor = {clienteProveedorID: "", datos: ""};
        $scope.asientoI.debe = {cuentaContableID: "", nombre: ""};
        $scope.asientoI.haber = {cuentaContableID: "", nombre: ""};

        $scope.asientoI.doc = {archivo: {}, edi: false};


        //variable para agregar Compra 
        $scope.registroC = {fechaR: "", tipoPagoID: "", areaID: "", orgID: "", numeroD: "", importe: "", clienteProveedorID: "", doc: ""};
        $scope.registroC.doc = {archivo: {}, edi: false};

        //variable para agregar Venta 
        $scope.registroV = {fechaR: "", tipoPagoID: "", numeroD: "", importe: "", clienteProveedorID: "", doc: ""};
        $scope.registroV.doc = {archivo: {}, edi: false};


        $scope.tipoPago = listTipoPago;
        $scope.tipoArea = listArea;
        $scope.cuentasEfectivo = listCuentasE;

        $scope.showBuscarOperacion = function (flag) {
            if (flag === 1)
                tablaOperacion.filter().tipo = "E";        //cambiar por C compras     
            else
                tablaOperacion.filter().tipo = "I";        //cambiar por V ventas

            ModalService.showModal({
                templateUrl: "administrativa/sistema_contable_institucional/buscarOperacion.html",
                controller: "buscarOperacionCtrl",
                inputs: {
                    title: "Seleccionar Operación",
                    tabla: tablaOperacion,
                    tablaClienteProveedor: tablaClienteProveedor
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                        if (result.data.tipo === "E") {     //cambiar por C compras  
                            $scope.asientoE.operacionID = result.data.operacionID;
                            $scope.asientoE.glosa = result.data.descripcion;
                            $scope.asientoE.debe = result.data.cuentaOperaciones[0];
                        } else if (result.data.tipo === "I") {

                            $scope.asientoI.operacionID = result.data.operacionID;
                            $scope.asientoI.glosa = result.data.descripcion;
                            $scope.asientoI.haber = result.data.cuentaOperaciones[0];
                        }



                    }


                });
            });

        };

        $scope.showBuscarClienteProveedor = function (flag) {
            if (flag === 1)
                tablaClienteProveedor.filter().tipo = "P";
            else
                tablaClienteProveedor.filter().tipo = "C";

            ModalService.showModal({
                templateUrl: "administrativa/sistema_contable_institucional/buscarClienteProveedor.html",
                controller: "buscarClienteProveedorCtrl",
                inputs: {
                    title: "Seleccionar Datos",
                    tablaClienteProveedor: tablaClienteProveedor
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                        if (result.data.tipo === "P")
                            $scope.asientoE.clienteProveedor = result.data;
                        else if (result.data.tipo === "C")
                            $scope.asientoI.clienteProveedor = result.data;
                    }
                });
            });

        };
        $scope.showReciboAuxiliar = function () {


            ModalService.showModal({
                templateUrl: "administrativa/sistema_contable_institucional/reciboAuxiliar.html",
                controller: "generarReciboAuxiliarCtrl",
                inputs: {
                    title: "Recibo Auxiliar",
                    tablaOperacion: tablaOperacion,
                    asientoE: $scope.asientoE,
                    listCuentasE: $scope.cuentasEfectivo,
                    libroID: libroID,
                    tablaClienteProveedor: tablaClienteProveedor
                }
            }).then(function (modal) {
                modal.element.modal();

            });

        };

        $scope.agregarTransaccionE = function () {

            $scope.registroC.tipoPagoID = $scope.asientoE.tipoPago.tipoPagoID;
            $scope.registroC.areaID = $scope.asientoE.area.areaID;
            $scope.registroC.numeroD = $scope.asientoE.numeroD;
            $scope.registroC.importe = $scope.asientoE.importe;
            $scope.registroC.clienteProveedorID = $scope.asientoE.clienteProveedor.clienteProveedorID;
            $scope.registroC.datos = $scope.asientoE.clienteProveedor.datos;
            $scope.registroC.fechaR = $scope.asientoE.fecha.toString();
            $scope.registroC.orgID = $rootScope.usuMaster.organizacion.organizacionID;
            $scope.registroC.doc = $scope.asientoE.doc;

            var request = crud.crearRequest('libroCaja', 1, 'insertarCompra');
            request.setData($scope.registroC);
            crud.insertar("/sistemaContable", request, function (response) {

                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {

                    $scope.asientoE.codUniOpeID = response.data.codUniOpeID;
                    $scope.asientoE.nomDocAdj = response.data.nomDocAdj;

                    request = crud.crearRequest('libroCaja', 1, 'insertarAsiento');
                    $scope.asientoE.fecha = $scope.registroC.fechaR;
                    console.log($scope.asientoE);
                    request.setData($scope.asientoE);

                    crud.insertar("/sistemaContable", request, function (response) {

                        modal.mensaje("CONFIRMACION", response.responseMsg);
                        if (response.responseSta) {

                            $scope.asiento = response.data;
                            $scope.asiento.clienteProveedor = $scope.asientoE.clienteProveedor;


                        }
                        //  Manually hide the modal.
                        $element.modal('hide');

                        //  Now call close, returning control to the caller.
                        close({
                            data: $scope.asiento,
                            flag: true
                        }, 500); // close, but give 500ms for bootstrap to animate

                    }, function (data) {
                        console.info(data);
                    });

                }

            }, function (data) {
                console.info(data);
            });
            console.log($scope.asiento);
        };

        $scope.agregarTransaccionI = function () {

            $scope.registroV.tipoPagoID = $scope.asientoI.tipoPago.tipoPagoID;
            $scope.registroV.numeroD = $scope.asientoI.numeroD;
            $scope.registroV.importe = $scope.asientoI.importe;
            $scope.registroV.clienteProveedorID = $scope.asientoI.clienteProveedor.clienteProveedorID;
            $scope.registroV.datos = $scope.asientoI.clienteProveedor.datos;
            $scope.registroV.fechaR = $scope.asientoI.fecha.toString();
            $scope.registroV.doc = $scope.asientoI.doc;

            var request = crud.crearRequest('libroCaja', 1, 'insertarVenta');

            request.setData($scope.registroV);

            crud.insertar("/sistemaContable", request, function (response) {

                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {

                    $scope.asientoI.codUniOpeID = response.data.codUniOpeID;
                    $scope.asientoI.nomDocAdj = response.data.nomDocAdj;

                    request = crud.crearRequest('libroCaja', 1, 'insertarAsiento');
                    $scope.asientoI.fecha = $scope.registroV.fechaR;
                    console.log($scope.asientoE);
                    request.setData($scope.asientoI);

                    crud.insertar("/sistemaContable", request, function (response) {

                        modal.mensaje("CONFIRMACION", response.responseMsg);
                        if (response.responseSta) {

                            $scope.asiento = response.data;
                            $scope.asiento.clienteProveedor = $scope.asientoI.clienteProveedor;


                        }
                        //  Manually hide the modal.
                        $element.modal('hide');

                        //  Now call close, returning control to the caller.
                        close({
                            data: $scope.asiento,
                            flag: true
                        }, 500); // close, but give 500ms for bootstrap to animate

                    }, function (data) {
                        console.info(data);
                    });


                }


            }, function (data) {
                console.info(data);
            });



        };

        //  This close function doesn't need to use jQuery or bootstrap, because
        //  the button has the 'data-dismiss' attribute.
        $scope.close = function () {
            //  Manually hide the modal.
            $element.modal('hide');

            close({
                cuenta: $scope.cuentaContable,
                flag: false
            }, 500); // close, but give 500ms for bootstrap to animate
        };

        //  This cancel function must use the bootstrap, 'modal' function because
        //  the doesn't have the 'data-dismiss' attribute.
        $scope.cancel = function () {

            //  Manually hide the modal.
            $element.modal('hide');

            //  Now call close, returning control to the caller.
            close({
                cuenta: $scope.cuentaContable,
                flag: false
            }, 500); // close, but give 500ms for bootstrap to animate
        };

        $scope.dateOptions = {
            maxDate: new Date(fecha.getFullYear(), fecha.getMonth(), 31),
            minDate: new Date(fecha.getFullYear(), fecha.getMonth(), 1),
            startingDay: 1

        };


        $scope.open2 = function () {
            $scope.popup2.opened = true;
        };

        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };

        $scope.popup2 = {
            opened: false
        };

        $scope.popup1 = {
            opened: false
        };

        $scope.getFecha = function (fecha, dato) {
            var f = dato;
            if (f) {
                fecha.d = f.getDate();
                fecha.m = f.getMonth();
                fecha.y = f.getFullYear();
            }

        };

        $scope.agregarDocE = function () {
            $scope.asientoE.doc.edi = true;
        }
        $scope.borrarDocE = function () {
            $scope.asientoE.doc.edi = false;

        }

        $scope.agregarDocI = function () {
            $scope.asientoI.doc.edi = true;
        }
        $scope.borrarDocI = function () {
            $scope.asientoI.doc.edi = false;

        }

    }]);




app.controller('buscarOperacionCtrl', [
    '$scope', '$element', 'tabla', 'title', 'close', 'crud', 'modal', 'NgTableParams',
    function ($scope, $element, tabla, title, close, crud, modal, NgTableParams) {


        $scope.title = title;
        $scope.tablaOperaciones = tabla;


        $scope.setClickedRow = function (d) {

            //  Manually hide the modal.
            $element.modal('hide');

            //  Now call close, returning control to the caller.
            close({
                data: d,
                flag: true
            }, 500); // close, but give 500ms for bootstrap to animate


        }

        $scope.close = function () {
            close({

                flag: false
            }, 500); // close, but give 500ms for bootstrap to animate
        };

        //  This cancel function must use the bootstrap, 'modal' function because
        //  the doesn't have the 'data-dismiss' attribute.
        $scope.cancel = function () {

            //  Manually hide the modal.
            $element.modal('hide');

            //  Now call close, returning control to the caller.
            close({

                flag: false
            }, 500); // close, but give 500ms for bootstrap to animate
        };



    }]);

app.controller('buscarClienteProveedorCtrl', [
    '$scope', '$element', 'tablaClienteProveedor', 'title', 'close', 'crud', 'modal', 'NgTableParams',
    function ($scope, $element, tablaClienteProveedor, title, close, crud, modal, NgTableParams) {


        $scope.title = title;
        $scope.tablaClienteProveedor = tablaClienteProveedor;



        $scope.setClickedRow = function (d) {

            //  Manually hide the modal.
            $element.modal('hide');

            //  Now call close, returning control to the caller.
            close({
                data: d,
                flag: true
            }, 500); // close, but give 500ms for bootstrap to animate


        }

        $scope.close = function () {
            close({

                flag: false
            }, 500); // close, but give 500ms for bootstrap to animate
        };

        //  This cancel function must use the bootstrap, 'modal' function because
        //  the doesn't have the 'data-dismiss' attribute.
        $scope.cancel = function () {

            //  Manually hide the modal.
            $element.modal('hide');

            //  Now call close, returning control to the caller.
            close({

                flag: false
            }, 500); // close, but give 500ms for bootstrap to animate
        };



    }]);

app.controller('generarReciboAuxiliarCtrl', [
    '$scope', 'listCuentasE', '$rootScope', 'libroID', '$element', 'title', 'tablaOperacion', 'tablaClienteProveedor', 'close', 'crud', 'modal', 'NgTableParams', 'ModalService', 'asientoE',
    function ($scope, listCuentasE, $rootScope, libroID, $element, title, tablaOperacion, tablaClienteProveedor, close, crud, modal, NgTableParams, ModalService, asientoE) {
        $scope.title = title;
        $scope.cuentasEfectivo = listCuentasE;

        $scope.asientoR = {operacionID: "", libro: "C", glosa: "", importe: "", tipoPago: "", area: "", numeroD: "", fecha: "", clienteProveedor: "", debe: "", haber: "", doc: "", observacion: "", estado: "", codUniOpeID: "", libroID: libroID};
        $scope.asientoR.tipoPago = {tipoPagoID: "", nombre: ""};
        $scope.asientoR.area = {areaID: "", nombre: ""};
        $scope.asientoR.clienteProveedor = {clienteProveedorID: "", datos: ""};
        $scope.asientoR.debe = {cuentaContableID: "", nombre: ""};
        $scope.asientoR.haber = {cuentaContableID: "", nombre: ""};

        $scope.asientoR.doc = {archivo: {}, edi: false};

        $scope.asientoR.persona = {dni: "", plazo: "", existe: true};

        $scope.registroR = {fechaR: "", tipoPagoID: "", areaID: "", orgID: "", numeroD: "", importe: "", clienteProveedorID: "", doc: ""};
        $scope.registroR.doc = {archivo: {}, edi: false};

        $scope.agregarTransaccionRB = function () {

            $scope.registroR.tipoPagoID = 1;//debe estar definido previamente en la BD como "Recibo Auxiliar"
            //$scope.registroR.areaID = $scope.asientoR.area.areaID;
            $scope.registroR.numeroD = $scope.asientoR.numeroD;
            $scope.registroR.importe = $scope.asientoR.importe;
            $scope.registroR.clienteProveedorID = 1;//debe estas definido en el BD como 'Auxiliar'
            //$scope.registroR.datos = $scope.asientoR.clienteProveedor.datos;
            $scope.registroR.fechaR = $scope.asientoR.fecha.toString();
            $scope.registroR.orgID = $rootScope.usuMaster.organizacion.organizacionID;
            $scope.registroR.doc = $scope.asientoR.doc;
            $scope.registroR.dni = $scope.asientoR.persona.dni;
            $scope.registroR.plazo = $scope.asientoR.persona.plazo;

            var request = crud.crearRequest('libroCaja', 1, 'insertarCompraReciboAuxiliar');
            request.setData($scope.registroR);
            crud.insertar("/sistemaContable", request, function (response) {

                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {

                    $scope.asientoR.codUniOpeID = response.data.codUniOpeID;
                    $scope.asientoR.nomDocAdj = response.data.nomDocAdj;
                    $scope.asientoR.fecha = $scope.registroR.fechaR;
                    $scope.asientoR.tipoPago.tipoPagoID = 1;
                    $scope.asientoR.tipoPago.nombre = "Recibo Auxiliar";

                    console.log($scope.asientoR);

                    request = crud.crearRequest('libroCaja', 1, 'insertarAsiento');
                    request.setData($scope.asientoR);

                    crud.insertar("/sistemaContable", request, function (response) {

                        modal.mensaje("CONFIRMACION", response.responseMsg);
                        if (response.responseSta) {

                            $scope.asiento = response.data;
                            $scope.asiento.clienteProveedor = $scope.asientoR.clienteProveedor;


                        }
                        //  Manually hide the modal.
                        $element.modal('hide');

                        //  Now call close, returning control to the caller.
                        close({
                            data: $scope.asiento,
                            flag: true
                        }, 500); // close, but give 500ms for bootstrap to animate

                    }, function (data) {
                        console.info(data);
                    });

                }

            }, function (data) {
                console.info(data);
            });
            console.log($scope.asiento);
        };


        $scope.imprimirReciboAuxiliar = function () {

            //$scope.registroR.tipoPagoID = 1;//debe estar definido previamente en la BD como "Recibo Auxiliar"
            //$scope.registroR.areaID = $scope.asientoR.area.areaID;
            $scope.registroR.Descripcion = $scope.asientoR.glosa;
            $scope.registroR.numeroD = $scope.asientoR.numeroD;
            $scope.registroR.tesoreEncargado = $rootScope.usuMaster.usuario.nombre;
            $scope.registroR.importe = $scope.asientoR.importe;
            //$scope.registroR.clienteProveedorID = 1;//debe estas definido en el BD como 'Auxiliar'
            //$scope.registroR.datos = $scope.asientoR.clienteProveedor.datos;
            $scope.registroR.fechaR = $scope.asientoR.fecha.toString();
            $scope.registroR.nombreOrg = $rootScope.usuMaster.organizacion.nombre;
            //$scope.registroR.doc = $scope.asientoR.doc;
            var nomcom = $scope.asientoR.persona.nombre + $scope.asientoR.persona.paterno + $scope.asientoR.persona.materno;
            $scope.registroR.nombrePerCom = nomcom;
            $scope.registroR.dni = $scope.asientoR.persona.dni;
            $scope.registroR.plazo = $scope.asientoR.persona.plazo;

            var request = crud.crearRequest('libroCaja', 1, 'impReciboAuxiliar');

            request.setData($scope.registroR);

            crud.insertar("/sistemaContable", request, function (response) {
                if (response.responseSta) {
                    verDocumento(response.data.datareporte);
                }
            }, function (data) {
                console.info(data);
            });
        };
        $scope.buscarPersona = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('usuarioSistema', 1, 'buscarPersona');

            //        if(!$scope.tipoPersona)
            //            request.setData({dni:0,usuarioID:$scope.persona.dni});
            //        else
            request.setData({dni: $scope.asientoR.persona.dni});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/configuracionInicial", request, function (data) {
                modal.mensaje("CONFIRMACION", data.responseMsg);
                if (data.responseSta) {
                    $scope.asientoR.persona = data.data.persona;
                    console.log(data.data.persona);
                } else {
                    $scope.asientoR.persona.existe = false;
                    modal.mensaje("No se encontro el DNI");
                }
            }, function (data) {
                console.info(data);
            });
        };
        $scope.setClickedRow = function (d) {

            //  Manually hide the modal.
            $element.modal('hide');

            //  Now call close, returning control to the caller.
            close({
                data: d,
                flag: true
            }, 500); // close, but give 500ms for bootstrap to animate


        }

        $scope.close = function () {
            close({

                flag: false
            }, 500); // close, but give 500ms for bootstrap to animate
        };
        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };
        $scope.popup1 = {
            opened: false
        };
        //  This cancel function must use the bootstrap, 'modal' function because
        //  the doesn't have the 'data-dismiss' attribute.
        $scope.cancel = function () {

            //  Manually hide the modal.
            $element.modal('hide');

            //  Now call close, returning control to the caller.
            close({

                flag: false
            }, 500); // close, but give 500ms for bootstrap to animate
        };

        $scope.showBuscarOperacion = function (flag) {
            if (flag === 1)
                tablaOperacion.filter().tipo = "E";        //cambiar por C compras     
            else
                tablaOperacion.filter().tipo = "I";        //cambiar por V ventas

            ModalService.showModal({
                templateUrl: "administrativa/sistema_contable_institucional/buscarOperacion.html",
                controller: "buscarOperacionCtrl",
                inputs: {
                    title: "Seleccionar Operación",
                    tabla: tablaOperacion,
                    tablaClienteProveedor: tablaClienteProveedor
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                        if (result.data.tipo === "E") {     //cambiar por C compras  
                            $scope.asientoR.operacionID = result.data.operacionID;
                            $scope.asientoR.glosa = result.data.descripcion;
                            console.log($scope.asientoR.glosa);
                            $scope.asientoR.debe = result.data.cuentaOperaciones[0];

                        } else if (result.data.tipo === "I") {

                            $scope.asientoI.operacionID = result.data.operacionID;
                            $scope.asientoI.glosa = result.data.descripcion;
                            $scope.asientoI.haber = result.data.cuentaOperaciones[0];
                        }



                    }


                });
            });
        }


    }]);

app.controller('editarTransaccionCtrl', [
    '$scope', '$element', 'libroID', 'listTipoPago', 'listCuentasE', 'asiento', 'title', 'tablaOperacion', 'tablaClienteProveedor', 'NgTableParams', 'close', 'crud', 'modal', "ModalService",
    function ($scope, $element, libroID, listTipoPago, listCuentasE, asiento, title, tablaOperacion, tablaClienteProveedor, NgTableParams, close, crud, modal, ModalService) {

       // alert("faltqante " + libroID);
        $scope.title = title;
        $scope.asientoE = asiento;
     
        //asiento.
        //alert (asiento.libro);
        var day = $scope.asientoE.fecha.toString().substr(0, 2);
        var month = $scope.asientoE.fecha.toString().substr(3, 2);
        var year = $scope.asientoE.fecha.toString().substr(6, 4);
        $scope.asientoE.fecha = new Date(year, month - 1, day);
        $scope.tipoPago = listTipoPago;
        // $scope.tipoArea = listtipoArea;
        $scope.cuentasEfectivo = listCuentasE;
        $scope.asientoE.libroID = libroID;

        function fecha(d) {
            var i = new Date(d.y, d.m, d.d);
            return i;
        }
        ;
        //variable para agregar Compra 
        $scope.registroCE = {fechaR: "", tipoPagoID: "", areaID: "", orgID: "", numeroD: "", importe: "", clienteProveedorID: "", doc: ""};
        $scope.registroCE.doc = {archivo: {}, edi: false};

        //variable para agregar Venta 
        //$scope.registroVE = {fechaR: "", tipoPagoID: "", numeroD: "", importe: "", clienteProveedorID: "", doc: ""};
        //$scope.registroVE.doc = {archivo: {}, edi: false};

        $scope.editarTransaccion = function () {

            $scope.registroCE.tipoPagoID = $scope.asientoE.tipoPago.tipoPagoID;
            //$scope.registroCE.areaID = $scope.asientoE.area.areaID;
            $scope.registroCE.numeroD = $scope.asientoE.numeroD;
            $scope.registroCE.importe = $scope.asientoE.importe;
            $scope.registroCE.clienteProveedorID = $scope.asientoE.clienteProveedor.clienteProveedorID;
            $scope.registroCE.datos = $scope.asientoE.clienteProveedor.datos;
            $scope.registroCE.fechaR = $scope.asientoE.fecha.toString();
            //$scope.registroCE.orgID = $rootScope.usuMaster.organizacion.organizacionID;
            $scope.registroCE.doc = $scope.asientoE.doc;
            $scope.registroCE.idOperacion = $scope.asientoE.codUniOpeId;
            if (asiento.libro == 'C') {
                var request = crud.crearRequest('libroCaja', 1, 'actualizarCompras');

                request.setData($scope.registroCE);
                crud.actualizar("/sistemaContable", request, function (response) {

                    modal.mensaje("CONFIRMACION", response.responseMsg);
                    if (response.responseSta) {

                        $scope.asientoE.codUniOpeID = response.data.codUniOpeID;
                        $scope.asientoE.nomDocAdj = response.data.nomDocAdj;

                        request = crud.crearRequest('libroCaja', 1, 'actualizarAsiento');
                        $scope.asientoE.fecha = $scope.registroCE.fechaR;


                        console.log($scope.asientoE);
                        request.setData($scope.asientoE);

                        crud.actualizar("/sistemaContable", request, function (response) {

                            modal.mensaje("CONFIRMACION", response.responseMsg);
                            if (response.responseSta) {

                                $scope.asiento = response.data;
                                $scope.asiento.clienteProveedor = $scope.asientoE.clienteProveedor;


                            }
                            //  Manually hide the modal.
                            $element.modal('hide');

                            //  Now call close, returning control to the caller.
                            close({
                                data: $scope.asiento,
                                flag: true
                            }, 500); // close, but give 500ms for bootstrap to animate

                        }, function (data) {
                            console.info(data);
                        });

                    }

                }, function (data) {
                    console.info(data);
                });
            } else if (asiento.libro == 'V') {
                var request = crud.crearRequest('libroCaja', 1, 'actualizarVentas');

                request.setData($scope.registroCE);
                crud.actualizar("/sistemaContable", request, function (response) {

                    modal.mensaje("CONFIRMACION", response.responseMsg);
                    if (response.responseSta) {

                        $scope.asientoE.codUniOpeID = response.data.codUniOpeID;
                        $scope.asientoE.nomDocAdj = response.data.nomDocAdj;

                        request = crud.crearRequest('libroCaja', 1, 'actualizarAsiento');
                        $scope.asientoE.fecha = $scope.registroCE.fechaR;


                        console.log($scope.asientoE);
                        request.setData($scope.asientoE);

                        crud.actualizar("/sistemaContable", request, function (response) {

                            modal.mensaje("CONFIRMACION", response.responseMsg);
                            if (response.responseSta) {

                                $scope.asiento = response.data;
                                $scope.asiento.clienteProveedor = $scope.asientoE.clienteProveedor;


                            }
                            //  Manually hide the modal.
                            $element.modal('hide');

                            //  Now call close, returning control to the caller.
                            close({
                                data: $scope.asiento,
                                flag: true
                            }, 500); // close, but give 500ms for bootstrap to animate

                        }, function (data) {
                            console.info(data);
                        });

                    }

                }, function (data) {
                    console.info(data);
                });
            }


            console.log($scope.asiento);
        };



        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };

        $scope.popup1 = {
            opened: false
        };

        $scope.getFecha = function (fecha, dato) {
            var f = dato;
            if (f) {
                fecha.d = f.getDate();
                fecha.m = f.getMonth();
                fecha.y = f.getFullYear();
            }

        };


        $scope.showBuscarOperacion2 = function (flag) {
            if ($scope.asientoE.libro === 'C')
                tablaOperacion.filter().tipo = "E";        //cambiar por C compras     
            else
                tablaOperacion.filter().tipo = "I";        //cambiar por V ventas

            ModalService.showModal({
                templateUrl: "administrativa/sistema_contable_institucional/buscarOperacion.html",
                controller: "buscarOperacionCtrl",
                inputs: {
                    title: "Seleccionar Operación",
                    tabla: tablaOperacion,
                    tablaClienteProveedor: tablaClienteProveedor
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                        //cambiar por C compras  
                        $scope.asientoE.operacionID = result.data.operacionID;
                        $scope.asientoE.glosa = result.data.descripcion;
                        $scope.asientoE.debe = result.data.cuentaOperaciones[0];





                    }


                });
            });

        };

        $scope.showBuscarClienteProveedor2 = function (flag) {
            if ($scope.asientoE.libro === 'C')
                tablaClienteProveedor.filter().tipo = "P";
            else
                tablaClienteProveedor.filter().tipo = "C";

            ModalService.showModal({
                templateUrl: "administrativa/sistema_contable_institucional/buscarClienteProveedor.html",
                controller: "buscarClienteProveedorCtrl",
                inputs: {
                    title: "Seleccionar Datos",
                    tablaClienteProveedor: tablaClienteProveedor
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {

                        $scope.asientoE.clienteProveedor = result.data;





                    }


                });
            });

        };
    }]);

app.controller('verTransaccionCtrl', [
    '$scope', '$element', 'listTipoPago', 'listCuentasE', 'asiento', 'title', 'tablaOperacion', 'tablaClienteProveedor', 'NgTableParams', 'close', 'crud', 'modal', "ModalService",
    function ($scope, $element, listTipoPago, listCuentasE, asiento, title, tablaOperacion, tablaClienteProveedor, NgTableParams, close, crud, modal, ModalService) {


        $scope.title = title;
        $scope.asiento = asiento;
        var day = $scope.asiento.fecha.toString().substr(0, 2);
        var month = $scope.asiento.fecha.toString().substr(3, 2);
        var year = $scope.asiento.fecha.toString().substr(6, 4);

        $scope.asiento.fecha = new Date(year, month - 1, day);
        $scope.tipoPago = listTipoPago;
        $scope.cuentasEfectivo = listCuentasE;

        function fecha(d) {
            var i = new Date(d.y, d.m, d.d);
            return i;
        }
        ;


        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };

        $scope.popup1 = {
            opened: false
        };

        $scope.getFecha = function (fecha, dato) {
            var f = dato;
            if (f) {
                fecha.d = f.getDate();
                fecha.m = f.getMonth();
                fecha.y = f.getFullYear();
            }

        };


        $scope.showBuscarOperacion = function (flag) {
            if ($scope.asiento.libro === 'C')
                tablaOperacion.filter().tipo = "E";        //cambiar por C compras     
            else
                tablaOperacion.filter().tipo = "I";        //cambiar por V ventas

            ModalService.showModal({
                templateUrl: "administrativa/sistema_contable_institucional/buscarOperacion.html",
                controller: "buscarOperacionCtrl",
                inputs: {
                    title: "Seleccionar Operación",
                    tabla: tablaOperacion,
                    tablaClienteProveedor: tablaClienteProveedor
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                        //cambiar por C compras  
                        $scope.asiento.operacionID = result.data.operacionID;
                        $scope.asiento.glosa = result.data.descripcion;
                        $scope.asiento.debe = result.data.cuentaOperaciones[0];





                    }


                });
            });

        };

        $scope.showBuscarClienteProveedor = function (flag) {
            if ($scope.asiento.libro === 'C')
                tablaClienteProveedor.filter().tipo = "P";
            else
                tablaClienteProveedor.filter().tipo = "C";

            ModalService.showModal({
                templateUrl: "administrativa/sistema_contable_institucional/buscarClienteProveedor.html",
                controller: "buscarClienteProveedorCtrl",
                inputs: {
                    title: "Seleccionar Datos",
                    tablaClienteProveedor: tablaClienteProveedor
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {

                        $scope.asiento.clienteProveedor = result.data;





                    }


                });
            });

        };
    }]);


