
app.requires.push('angularModalService');
app.requires.push('ngAnimate');
app.requires.push('ui.bootstrap');           
     

app.controller("configuracionLibroCtrl",["$scope","$rootScope","NgTableParams","crud","modal","ModalService", function ($scope,$rootScope,NgTableParams,crud,modal,ModalService){
   var i=0; var f=new Date();
   //estados del libro  I=INACTIVO A=ACTIVO C=CERRADO E=ELIMINADO   
   $scope.estado=true;  //flag para datos del libro
   $scope.estadoLibro=true; //flag para datos del estado del libro
   $scope.estadoT=true; //flag para datos Tesorero 
   $scope.estadoA=true; //flag para agregar un nuevo libro
   //fecha formato
   formato={d:"",m:"",y:""};
   
   //variables que almacena los datos de la configuración
   $scope.libro= {libroID:0,nombre:"",observacion:"",fechaApertura:"",fechaCierre:"",saldoApertura:"",saldoActual:"",organizacionID:$rootScope.usuMaster.organizacion.organizacionID,personaID:$rootScope.usuMaster.usuario.usuarioID,estado:'N'};
   
   //variables que almacena los datos del libro anterior
    $scope.libroA=[];    $scope.contenido=false;
    
   //arreglo donde estan todos los libros caja de la institución
    var paramsLibros= {count: 10};
    var settingLibros = { counts: []};
    $scope.tablaLibros = new NgTableParams(paramsLibros, settingLibros);
    
   //variable para almacenar tesoreros;
     $scope.listaTesoreros=[];
    
    //Variables para agregar nuevo tesorero ,trabajador 
    $scope.tesorero={personaID:"",libroID:"",datos:"",fechaInicio:"",fechaCierre:"",rd:"",doc:""}; 
    $scope.tesorero.doc = {archivo:{},edi:false};
   //Variable para almacenar la lista de bancos
     $scope.listaBancos =[];       
   //Variable para almacenar las cuentas de efectivo 
     $scope.tablaCuentaEfectivo =[];
   //Variable para almacenar las cuentas de efectivo seleccionadas  
     $scope.tablaCuentaEfectivoSel =[];
   //Variable para almacenar las cuentas corrientes del libro
     $scope.tablaCuentasCorrientes =[];  
    //Variable para almacena una cuenta corriente
      $scope.cuentaBancaria={cuentaCorrienteID:"",entidad:"",numeroCuenta:""};
      
  //funcion inicial verifica el estado  del libro   
     estadoLibro();

   function estadoLibro(){
         //preparamos un objeto request
        var request = crud.crearRequest('libroCaja',1,'estadoLibroCaja');        
        
        //vamos a verificar estado del libro 
        request.setData({organizacionID:$rootScope.usuMaster.organizacion.organizacionID }); 
        //llamamos al servicio          
        crud.listar("/sistemaContable",request,function(response){
            //  modal.mensaje("CONFIRMACION",response.responseMsg);
            console.log(response.data);
            if(response.responseSta){
               
                var e=response.data.estado;
                
                if(e==='N'){
                    $scope.estadoA=false;                   
                    listarCuentasEfectivo();  
                       
                }
                else if(e==='I'){    
                    $scope.estadoLibro=false;
                    $scope.estadoT=false; 
                    $scope.libro=response.data;
                    $scope.tablaCuentaEfectivo=response.data.cuentasEfectivo;
                    $scope.listaTesoreros=response.data.tesoreros;
                    $scope.libro.fechaApertura=new Date(response.data.fechaA);
                    $scope.libro.fechaCierre=new Date(response.data.fechaC);
                }
                else if(e==='A'){   
                    $scope.estadoLibro=false;
                    $scope.estadoT=false;
                    $scope.libro=response.data;
                    $scope.tablaCuentaEfectivo=response.data.cuentasEfectivo;
                    $scope.listaTesoreros=response.data.tesoreros;
                    $scope.libro.fechaApertura=new Date(response.data.fechaA);
                    $scope.libro.fechaCierre=new Date(response.data.fechaC);
                }
                else if(e==='C'){
                  
                  $scope.estadoA=false;   
                  $scope.libroA=response.data.cuentasEfectivo;  
                  $scope.libro.saldoActual=response.data.saldo;
                  $scope.libro.estado='C';
                  $scope.contenido=true;                 
                  listarCuentasEfectivo();    
                }
               
            }
      
        
        },function(data){
            console.info(data);
        });
       
   };   
    
    $scope.agregarLibro =function() {
        $scope.estado=false;
        $scope.estadoLibro=false;
    };
 
    $scope.listarLibroCaja=function (){

          //preparamos un objeto request
    var request = crud.crearRequest('libroCaja',1,'listarLibroCajaConTesoreros');        
    request.setData({organizacionID:$rootScope.usuMaster.organizacion.organizacionID});
    //llamamos al servicio listar,  
    crud.listar("/sistemaContable",request,function(response){
        console.log(response.toString());
        settingLibros.dataset = response.data;
    //asignando la posicion en el arreglo a cada objeto
        iniciarPosiciones(settingLibros.dataset);
        $scope.tablaLibros.settings(settingLibros);
        console.log( $scope.tablaLibros);
    },function(data){
        console.info(data);
    });
    };  
               
             
     
        $scope.getFecha=function(fecha,dato){
            var f=dato;               
            if(f){
            fecha.d=f.getDate();
            fecha.m=f.getMonth();
            fecha.y=f.getFullYear();
            }
            
        };
        
      
        
       $scope.fecha=function(d){
          var i=new Date(d.y,d.m,d.d);
            return i;
        };
        
         
  $scope.registrarLibro = function(){
     
       var request = crud.crearRequest('libroCaja',1,'insertarLibroCaja');                            
       $scope.libro.fechaA=$scope.libro.fechaApertura.toString();
       $scope.libro.fechaC=$scope.libro.fechaCierre.toString();
       $scope.libro.cuentasEfectivo=$scope.tablaCuentaEfectivoSel;
       
       request.setData($scope.libro);
       console.log($scope.libro);
       crud.insertar("/sistemaContable",request,function(response){
           
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                //recuperamos las variables que nos envio el servidor
                $scope.libro = response.data;
                $scope.libro.fechaApertura=new Date(response.data.fechaApertura);
                $scope.libro.fechaCierre=new Date(response.data.fechaCierre);
                //$scope.libro.cuentasEfectivo=[];
                $scope.estadoA=true;
                $scope.estadoT=false;
                                                        
            }  
            console.log($scope.libro);
        },function(data){
            console.info(data);
        });               
             
    };  
    
    $scope.agregarTesorero=function (){
    
        var request = crud.crearRequest('libroCaja',1,'insertarTesorero');                            
        $scope.tesorero.libroID=$scope.libro.libroID;
        $scope.tesorero.fechaInicio=$scope.tesorero.fechaInicio.toString();
        $scope.tesorero.fechaCierre=$scope.tesorero.fechaCierre.toString();
        request.setData($scope.tesorero);
        console.log($scope.tesorero);
        crud.insertar("/sistemaContable",request,function(response){
           
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
             $scope.listaTesoreros.push(response.data);  
             $scope.tesorero={personaID:"",libroID:"",datos:"",fechaInicio:"",fechaCierre:"",rd:"",doc:""}; 

     
            }  
           
        },function(data){
            console.info(data);
        });               
             
    };  
  
  $scope.dateOptions = {  
    
    maxDate: new Date(f.getFullYear(),11,31),
    minDate: new Date(f.getFullYear(),1,1),
    startingDay: 1
        
  };


  $scope.open2 = function() {
    $scope.popup2.opened = true;
  };

  $scope.open1 = function() {
    $scope.popup1.opened = true;
  };
  
  $scope.popup2 = {
    opened: false
  };
  
   $scope.popup1 = {
    opened: false
  };
   $scope.open3 = function() {
    $scope.popup3.opened = true;
  };

  $scope.open4 = function() {
    $scope.popup4.opened = true;
  };
  
  $scope.popup3 = {
    opened: false
  };
  
   $scope.popup4 = {
    opened: false
  };

 

 $scope.showBuscarTrabajador = function() {
   
   //Variables para manejo de la tabla
    var paramsTrabajadores= {count: 10};
    var settingTrabajadores = { counts: []};
    $scope.tablaTrabajadores = new NgTableParams(paramsTrabajadores, settingTrabajadores);
    
        listarTablaTrabajadores();

     function  listarTablaTrabajadores (){
        //preparamos un objeto request
        var request = crud.crearRequest('libroCaja',1,'listarTrabajador');        
         request.setData({organizacionID:$rootScope.usuMaster.organizacion.organizacionID});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
        crud.listar("/sistemaContable",request,function(response){
            console.log(response.toString());
            settingTrabajadores.dataset = response.data;
        //asignando la posicion en el arreglo a cada objeto
            iniciarPosiciones(settingTrabajadores.dataset);
            $scope.tablaTrabajadores.settings(settingTrabajadores);
            console.log( $scope.tablaTrabajadores);
        },function(data){
            console.info(data);
        });
    };  
 
    ModalService.showModal({
      templateUrl: "administrativa/sistema_contable_institucional/buscarTrabajador.html",
      controller: "buscarTrabajadorCtrl",
      inputs: {
        title: "Seleccionar Trabajador",
        tabla: $scope.tablaTrabajadores
    }                 
    }).then(function(modal) {
      modal.element.modal();
      modal.close.then(function(result) {
          if(result.flag){
              $scope.tesorero.personaID=result.data.personaID;
              $scope.tesorero.datos=result.data.datos;
            
          }
        
      });
    });
    console.log($scope.tesorero);
  };


    function listarCuentasEfectivo(){
        //preparamos un objeto request
        var request = crud.crearRequest('libroCaja',1,'listarCuentasEfectivo');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/sistemaContable",request,function(data){
            $scope.tablaCuentaEfectivo = data.data;
            
        },function(data){
            console.info(data);
        });
    };
    
    listarBancos();    
       function listarBancos(){
        //preparamos un objeto request
        var request = crud.crearRequest('libroCaja',1,'listarBancos');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/sistemaContable",request,function(data){
            $scope.listaBancos = data.data;
            
        },function(data){
            console.info(data);
        });
    };
    
$scope.agregarCuentasEfectivo=function (miTabla){
    for (var i =0,l = miTabla.length; i < l; i++){
         if (miTabla[i].flag ===true && !isFinite(miTabla[i].importe))  {
             
            modal.mensaje("CONFIRMACION","Ingrese el importe de la cuenta seleccionada");
            return;
        
         }
     }
    var saldoTotal={saldo:0.00};
     for (var i =0,l = miTabla.length; i < l; i++){
         if (miTabla[i].flag ===true) {
             $scope.tablaCuentaEfectivoSel.push(miTabla[i]);
            saldoTotal.saldo+= parseFloat(miTabla[i].importe);
         }
     }
      saldoTotal.saldo= saldoTotal.saldo.toFixed(2);
     $scope.libro.saldoActual= saldoTotal.saldo;
     console.log($scope.tablaCuentaEfectivoSel);
     console.log(saldoTotal);
      $('#modalCuentasEfectivo').modal('hide');
};

$scope.agregarBanco= function(tabla){
    $scope.cuentaBancaria.cuentaCorrienteID=tabla.cuentaContableID;    
    i=tabla.i;
    
    $scope.cuentaBancaria.entidadID=tabla.banco;
    $scope.cuentaBancaria.numeroCuenta=tabla.num;

    $('#modalNuevoBanco').modal('show');
      
        
        
    };
$scope.registrarBanco= function(){
     $scope.tablaCuentaEfectivo[i].banco=$scope.cuentaBancaria.entidadID;
     $scope.tablaCuentaEfectivo[i].num=$scope.cuentaBancaria.numeroCuenta;
     $scope.tablaCuentaEfectivo[i].f=true;
   //   $scope.cuentaBancaria={entidadID:"",numero:"",cuentaCorrienteID:""};
      $('#modalNuevoBanco').modal('hide');
       
};

$scope.agregarDoc = function (){
     $scope.tesorero.doc.edi=true;
 }
 $scope.borrarDoc = function (){
     $scope.tesorero.doc.edi=false;
     
    
 }

}]);



app.controller('buscarTrabajadorCtrl', [
  '$scope', '$element', 'title','tabla', 'close','crud','modal','NgTableParams',
  function($scope, $element, title,tabla, close,crud,modal,NgTableParams) {
   
    
  $scope.title=title;
  $scope.tablaTrabajadores=tabla;
    
  $scope.setClickedRow=function (d){
        
       //  Manually hide the modal.
             $element.modal('hide');
    
                 //  Now call close, returning control to the caller.
                 close({
                     data: d,
                     flag:true
                }, 500); // close, but give 500ms for bootstrap to animate
                
                console.log(d);
  };
 
  $scope.close = function() {
 	  close({
     
      flag:false
    }, 500); // close, but give 500ms for bootstrap to animate
  };

  //  This cancel function must use the bootstrap, 'modal' function because
  //  the doesn't have the 'data-dismiss' attribute.
  $scope.cancel = function() {

    //  Manually hide the modal.
    $element.modal('hide');
    
    //  Now call close, returning control to the caller.
    close({
    
      flag:false
    }, 500); // close, but give 500ms for bootstrap to animate
  };
  
  }]);

