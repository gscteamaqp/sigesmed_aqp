
app.requires.push('angularModalService');
app.requires.push('ngAnimate');
app.requires.push('ui.bootstrap');
      

app.controller("supervisionLibroCajaCtrl",["$scope","$filter","$rootScope","NgTableParams","crud","modal","ModalService", function ($scope,$filter,$rootScope,NgTableParams,crud,modal,ModalService){
  
 
  //variable para agregar un nuevo control
  $scope.control= {fecha:"",controlID:0,descripcion:"",fechaPreCierre:'',fechaCierre:'',fechaReApertura:'',organizacionID:""};
  
  //variable para manejo de la fecha del control
  $scope.control.fecha = new Date();   
 
  //Variable almacena organizacion y fecha de control para consulta de sus libros a sus cargo
  $scope.organizacionControl={organizacionID:$rootScope.usuMaster.organizacion.organizacionID,fechaControl:""};
  
  //Variables para manejo de la tabla del los Libros Caja
  var paramsLibrosCaja= {count: 10};
  var paramsControl= {count: 6};
  var settingLibrosCaja = { counts: [],filterOptions: { filterComparator:_.startsWith }};
  var settingControl = { counts: []};
  
  $scope.tablaLibrosCaja = new NgTableParams(paramsLibrosCaja, settingLibrosCaja);
  $scope.tablaControlLibro = new NgTableParams(paramsControl, settingControl);
   
  //Variable para listar registros de controles del libro
  $scope.controles=[];
  
  listarControl();
  
  $scope.listarLibros = function ()   {
     
       $scope.organizacionControl.fechaControl=  $scope.control.fecha.getFullYear();
       //preparamos un objeto request
        var request = crud.crearRequest('libroCaja',1,'listarLibrosCajaPorUGEL');        
        request.setData( $scope.organizacionControl);
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request             
        crud.listar("/sistemaContable",request,function(response){
           
           if(response.data===null){ $scope.tablaLibrosCaja=[];}
           else{
            settingLibrosCaja.dataset =response.data;
        //asignando la posicion en el arreglo a cada objeto
            iniciarPosiciones(settingLibrosCaja.dataset);
            $scope.tablaLibrosCaja.settings(settingLibrosCaja);
            
            console.log($scope.tablaLibrosCaja);}
         
        },function(data){
            console.info(data);
        });
       
  };
  
  $scope.agregarControlLibro = function(){
     
       var request = crud.crearRequest('controlLibro',1,'insertarControl');                            
       

        if($scope.control.descripcion==="" ){
            modal.mensaje("CONFIRMACION","Ingrese la descripción del control");
            return;
        }
        if( ($scope.control.fechaPreCierre==="" )){
            modal.mensaje("CONFIRMACION","La fecha Pre-cierre no es correcta");
            return;
        }
        if(($scope.control.fechaCierre==="" )){
            modal.mensaje("CONFIRMACION","La fecha Cierre no es correcta");
            return;
        }
        if(($scope.control.fechaReApertura==="" )){
            modal.mensaje("CONFIRMACION","La fecha Re-Apertura no es correcta");
            return;
        }
        
       $scope.control.fechaPreCierre=$scope.control.fechaPreCierre.toString();
       $scope.control.fechaCierre=$scope.control.fechaCierre.toString();
       $scope.control.fechaReApertura=$scope.control.fechaReApertura.toString();
       $scope.control.organizacionID=$rootScope.usuMaster.organizacion.organizacionID;
       request.setData($scope.control);
       
       crud.insertar("/sistemaContable",request,function(response){
           
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                  listarControl();
                //recuperamos las variables que nos envio el servidor
               $scope.controles.push(response.data);
               $scope.control={controlID:0,descripcion:"",fechaPreCierre:'',fechaCierre:'',fechaReApertura:''};                                                        
            }  
            
        },function(data){
            console.info(data);
        });               
             
    }; 
    
  function listarControl(){
      var request= crud.crearRequest('controlLibro',1,'listarControl') ;
      request.setData({year:$scope.control.fecha.getFullYear(),organizacionID:$rootScope.usuMaster.organizacion.organizacionID});
      
      crud.listar("/sistemaContable",request,function (data){
          modal.mensaje("CONFIRMACION",data.responseMsg);
          if(data.data){
             // $scope.controles = response.data;
              settingControl.dataset =data.data;
        //asignando la posicion en el arreglo a cada objeto
            iniciarPosiciones(settingControl.dataset);
            $scope.tablaControlLibro.settings(settingControl);
          }
         
      
          },function(data){
            console.info(data);
        });  
      
      
    };
  $scope.eliminarControl = function(i,idDato){
        
        modal.mensajeConfirmacion($scope,"seguro que desea eliminar el control de libro",function(){
            var request = crud.crearRequest('controlLibro',1,'eliminarControl');
            request.setData({controlD:idDato});
            crud.eliminar("/sistemaContable",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta)
                    listarControl();
            },function(data){
                console.info(data);
            });
        });
    };
    $scope.editarControl = function(i,r,modo){
        //si estamso editando
        if(r.edi){
            if(modo)
                $scope.tipoTramiteSel.requisitos[i] = r.copia;                
            else
                $scope.requisitos[i] = r.copia;
        }
        //si queremos editar
        else{
            
            var dd =  r.fechaPreCierre.toString().substr(0,2);
            var mm = r.fechaPreCierre.toString().substr(3,2);
            var yy =  r.fechaPreCierre.toString().substr(6,4);
            
            var d =  r.fechaCierre.toString().substr(0,2);
            var m = r.fechaCierre.toString().substr(3,2);
            var y =  r.fechaCierre.toString().substr(6,4);
            r.copia = JSON.parse(JSON.stringify(r));
            r.copia.fechaPreCierre= new Date(yy,mm-1,dd);
            r.copia.fechaCierre= new Date(y,m-1,d);

            r.edi =true;            
        }
    };
  
  $scope.dateOptions = {      
    datepickerMode: "year",
        formatYear: "yyyy" ,
        minMode: "year",
        minDate:new Date("2010,1,1"),
        maxDate:new Date("2100/10/10")
      
  };

     
  $scope.open1 = function() {
    $scope.popup1.opened = true;
  };
  $scope.popup1 = {
    opened: false
  };

var f=new Date();
$scope.dateOptions1 = {  
    
    maxDate: new Date(f.getFullYear(),11,31),
    minDate: new Date(f.getFullYear(),0,1),
    startingDay: 1
        
  };

     
  $scope.open2 = function() {
    $scope.popup2.opened = true;
  };
  $scope.popup2 = {
    opened: false
  };
  $scope.open3 = function() {
    $scope.popup3.opened = true;
  };
  $scope.popup3 = {
    opened: false
  };
  $scope.open4 = function() {
    $scope.popup4.opened = true;
  };
  $scope.popup4 = {
    opened: false
  };
  $scope.open5 = function() {
    $scope.popup5.opened = true;
  };
  $scope.popup5 = {
    opened: false
  };    
        
   $scope.applyGlobalSearch = function(term) {
    $scope.tablaLibrosCaja.filter({ $: term });
};

$scope.isCollapsed=false;
$scope.verLibro=function (libro){
    $scope.isCollapsed=true;
    $scope.consulta.organizacionID=libro.organizacionID;
    $scope.consulta.personaID=libro.personaID;
    
     settingLibroCaja.dataset =[];
                  $scope.tablaLibroCaja.settings(settingLibroCaja);
                  settingCuentasIngreso.dataset =[];
                  $scope.tablaCuentasIngreso.settings(settingCuentasIngreso);
                  $scope.totalIngreso = [];
                  settingCuentasEgreso.dataset =[];
                  $scope.tablaCuentasEgreso.settings(settingCuentasEgreso);
                  $scope.totalEgreso = [];
               
                  $scope.libro = [];
                  $scope.resultados = [];
                  $scope.saldoDesde= [];
                  $scope.saldoInicial = [];
                  $scope.totalI= [];
                  $scope.saldoA= [];
                    $scope.libroEstado={};
                    $scope.saldoApertura ="";
    
};

$scope.regresarSupervision = function (){
                  settingLibroCaja.dataset =[];
                  $scope.tablaLibroCaja.settings(settingLibroCaja);
                  settingCuentasIngreso.dataset =[];
                  $scope.tablaCuentasIngreso.settings(settingCuentasIngreso);
                  $scope.totalIngreso = [];
                  settingCuentasEgreso.dataset =[];
                  $scope.tablaCuentasEgreso.settings(settingCuentasEgreso);
                  $scope.totalEgreso = [];
               
                  $scope.libro = [];
                  $scope.resultados = [];
                  $scope.saldoDesde= [];
                  $scope.saldoInicial = [];
                  $scope.totalI= [];
                  $scope.saldoA= [];
                    $scope.libroEstado={};
                    $scope.saldoApertura ="";
                      
                    
 $scope.isCollapsed=false;
     
}
    
     $scope.seleccionar ={flagReporteL:false,flagReporteI:false,flagReporteE:false,flagReporteB:false};                       
 
    //variables de consulta BD
    $scope.consulta ={desde:"",hasta:"",organizacionID:"",personaID:""};
    
   
    //Variables para manejo de la Operacion del Libro Caja
    var paramsCuentasIngreso= {count: 10 };
    var settingCuentasIngreso= { counts: []};
    $scope.tablaCuentasIngreso = new NgTableParams(paramsCuentasIngreso, settingCuentasIngreso);
     
    //Variables para manejo de la Operacion del Libro Caja
    var paramsCuentasEgreso= {count: 10 };
    var settingCuentasEgreso= { counts: []};
    $scope.tablaCuentasEgreso = new NgTableParams(paramsCuentasEgreso, settingCuentasEgreso);
     
    $scope.resultados=[] ;
    var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
   
  // verificaSaldo();
  // listarLibroCaja();               
        
  $scope.verReporte =function (){
     
        //preparamos un objeto request               
      var request = crud.crearRequest('libroCaja',1,'reporteLibro');        
        $scope.consulta.fechaD=$scope.consulta.desde.toString();
        $scope.consulta.fechaH=$scope.consulta.hasta.toString();
        request.setData($scope.consulta); 
        //llamamos al servicio          
        crud.listar("/sistemaContable",request,function(response){
            //  modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                console.log(response.data);
             if (response.data.estado==='I'|| response.data.estado==='C' || response.data.estado==='N'){
                  $scope.libroEstado.estado=response.data.estado;
                 modal.mensaje("CONFIRMACION",response.responseMsg);
             }  
             else{
                modal.mensaje("CONFIRMACION",response.responseMsg);  
               //   $scope.libroEstado=response.data;
              //    $scope.libroEstado.mes= new Date();
             //     $scope.libroEstado.mesSel=$scope.libroEstado.mes.getMonth().toString();                  
            //      if($scope.libroEstado.mes.getMonth()<9)
           //           $scope.tablaLibroCaja.filter().fecha="/0"+ (parseFloat($scope.libroEstado.mesSel)+ 1) +"/";
          //        else
         //             $scope.tablaLibroCaja.filter().fecha="/"+ (parseFloat($scope.libroEstado.mesSel)+ 1) +"/";



               //   settingLibroCaja.dataset =response.data.asientos;
               //   $scope.tablaLibroCaja.settings(settingLibroCaja);
                  settingCuentasIngreso.dataset =response.data.cuentasIngreso;
                  $scope.tablaCuentasIngreso.settings(settingCuentasIngreso);
                  $scope.totalIngreso = response.data.totalIngreso;
                  settingCuentasEgreso.dataset =response.data.cuentasEgreso;
                  $scope.tablaCuentasEgreso.settings(settingCuentasEgreso);
                  $scope.totalEgreso = response.data.totalEgreso;
               
                  $scope.libro = response.data.libro;
                  $scope.resultados = response.data.resultados;
                  $scope.saldoDesde= response.data.saldoDesde;
                  $scope.saldoInicial = saldoInicial($scope.libro.saldoApertura,$scope.resultados[0],$scope.saldoDesde);
                  $scope.totalI= suma( $scope.saldoInicial,$scope.totalIngreso.totalIngresos);
                  $scope.saldoA= resta( $scope.totalI,$scope.totalEgreso.totalEgresos);
        //          $scope.cuentasEfectivo = response.data.cuentasEfectivo;
        //          $scope.hechosLibroCaja = response.data.hechosLibroCaja;
                  
                  verificaSaldo();
        //          var fechaApertura = new Date($scope.libroEstado.libro.fechaApertura.toString());

       //           if(fechaApertura.getMonth()=== $scope.libroEstado.mes.getMonth()){
         //             var saldoApertura =0.00;
         //             for(var i=0;i<$scope.cuentasEfectivo.length;i++)
         //                 saldoApertura += $scope.cuentasEfectivo[i].saldoApertura;                      
                      
         //           $scope.libroEstado.saldoMesAnterior=saldoApertura.toFixed(2);
         //           $scope.libroEstado.enunciado = "SALDO DE APERTURA INICIAL";
         //         }
         //         else{
         //           $scope.libroEstado.enunciado = "SALDO DE MES ANTERIOR";
         //         }
         //         console.log(response.data);             
             }
               
            }
        
        },function(data){
            console.info(data);
        });
     
        
    }; 
      
      function saldoInicial (a,re,sd){
          var saldoInicial,mesAnterior, saldoDesde =0.00;
        
           if(re.saldo && re.mes!==$scope.consulta.desde.getMonth()) {mesAnterior = re.saldo;}
           else { mesAnterior=a;}
           
           if(sd.debe) {saldoDesde=sd.saldo;}
           
           saldoInicial = mesAnterior + saldoDesde;
           saldoInicial = saldoInicial.toFixed(2);
           return saldoInicial;
      };
      
      function suma (a,b){
          var result=0.00;
                               
           result = parseFloat(a) + parseFloat(b); 
           result = result.toFixed(2);
           return result;
      };
       function resta (a,b){
          var result=0.00;
                               
           result = parseFloat(a) - parseFloat(b); 
           result = result.toFixed(2);
           return result;
      };
      


        
        //Visualizar de archivos en formato PDF
    $scope.reporte = function () {        
        var request = crud.crearRequest('libroCaja',1,'reporteLibroCaja');       
     
        request.setData($scope.tablaLibroCaja.settings().dataset);        
        crud.insertar("/sistemaContable",request,function(data){            
            $scope.dataBase64 = data.data[0].datareporte;
            window.open($scope.dataBase64);
        },function(data){
            console.info(data);
        });            
    };
    
      $scope.reporteIngresos = function () {        
        var request = crud.crearRequest('libroCaja',1,'reporteIngresos');       
        var cabeceraReporte={saldoInicial:$scope.saldoInicial,totalIngreso:$scope.totalIngreso.totalIngresos,totalI:$scope.totalI,totalEgreso:$scope.totalEgreso.totalEgresos,saldoA:$scope.saldoA};
        request.setData({cabecera:cabeceraReporte,tablaIngresos:$scope.tablaCuentasIngreso.settings().dataset,totales:$scope.totalIngreso});        
        crud.insertar("/sistemaContable",request,function(data){            
            $scope.dataBase64 = data.data[0].datareporte;
            window.open($scope.dataBase64);
        },function(data){
            console.info(data);
        });            
    };
    
     $scope.reporteEgresos = function () {        
        var request = crud.crearRequest('libroCaja',1,'reporteEgresos');       
        request.setData({tablaEgresos:$scope.tablaCuentasEgreso.settings().dataset,totales:$scope.totalEgreso});        
        crud.insertar("/sistemaContable",request,function(data){            
            $scope.dataBase64 = data.data[0].datareporte;
            window.open($scope.dataBase64);
        },function(data){
            console.info(data);
        });            
    };
    
     $scope.reporteBalanceGeneral = function () {        
       var request = crud.crearRequest('libroCaja',1,'reporteBalanceGeneral');       
        var cabeceraReporte={saldoInicial:$scope.saldoInicial,totalIngreso:$scope.totalIngreso.totalIngresos,totalI:$scope.totalI,totalEgreso:$scope.totalEgreso.totalEgresos,saldoA:$scope.saldoA};
        request.setData({cabecera:cabeceraReporte,tablaIngresos:$scope.tablaCuentasIngreso.settings().dataset,totalesIngreso:$scope.totalIngreso,tablaEgresos:$scope.tablaCuentasEgreso.settings().dataset,totalesEgreso:$scope.totalEgreso});        
        crud.insertar("/sistemaContable",request,function(data){            
            $scope.dataBase64 = data.data[0].datareporte;
            window.open($scope.dataBase64);
        },function(data){
            console.info(data);
        });     
    };
        
   
       
    
 $scope.seleccionar = function (v) {        
        if(v ==="1"){
           
           $scope.seleccionar.flagReporteL=true;         
           $scope.seleccionar.flagReporteI = $scope.seleccionar.flagReporteE= $scope.seleccionar.flagReporteB =false;
        }
        else if(v==="2"){
            
           $scope.seleccionar.flagReporteI=true;           
           $scope.seleccionar.flagReporteL = $scope.seleccionar.flagReporteE= $scope.seleccionar.flagReporteB =false;
        }
         else if(v==="3"){
            $scope.seleccionar.flagReporteE=true;           
           $scope.seleccionar.flagReporteL = $scope.seleccionar.flagReporteI= $scope.seleccionar.flagReporteB =false;
        }
         else if(v==="4"){
            $scope.seleccionar.flagReporteB=true;           
           $scope.seleccionar.flagReporteL = $scope.seleccionar.flagReporteI= $scope.seleccionar.flagReporteE =false;
        }
        else{                    
          $scope.seleccionar.flagReporteL= $scope.seleccionar.flagReporteI = $scope.seleccionar.flagReporteE= $scope.seleccionar.flagReporteB =false;
        }
        
    };


    //dato del libro  
    $scope.libroEstado= {debe:0.00,haber:0.00,saldoMesAnterior:0.00,saldoMes:0.00,estado:"",mes:"",mesSel:"",libro:"",enunciado:""};
    
        $scope.libroEstado.libro={libroID:0,nombre:"",observacion:"",fechaApertura:"",fechaCierre:"",saldoApertura:0.00,saldoActual:0.00,organizacionID:0,personaID:0,estado:''};             
        $scope.libroEstado.mes=new Date();
  // $scope.libro.mesSel= $scope.libro.mes.getMonth().toString();
    
     
    //Variables para manejo de la Operacion del Libro Caja
    var paramsLibroCaja= {count: 10, sorting: { fecha: "asc" }};
    var settingLibroCaja = { counts: []};
    $scope.tablaLibroCaja = new NgTableParams(paramsLibroCaja, settingLibroCaja);
    // $scope.tablaLibroCaja.filter().fecha=$scope.libro.mesSel;
  
    //Variables para manejo de la tabla
    var paramsOperaciones= {count: 10};
    var settingOperaciones = { counts: []};
    $scope.tablaOperaciones = new NgTableParams(paramsOperaciones, settingOperaciones);
    
    
    //Variables para manejo de la tabla
    var paramsClienteProveedor= {count: 10};
    var settingClienteProveedor = { counts: []};
    $scope.tablaClienteProveedor = new NgTableParams(paramsClienteProveedor, settingClienteProveedor);
   
   //variable para list tipo  de pago
   $scope.tipoPago=[];
   //variable para list cuenta efectivo
   $scope.cuentasEfectivo=[];
   
   //variable para list hechos libro
   $scope.hechosLibroCaja=[];
   
   //variable para agregar Transaccion a la Tabla del Libro Caja
   $scope.asiento = {operacionID:"",libro:"",glosa:"",importe:"",tipoPago:"",numeroD:"",fecha:"",clienteProveedor:"",debe:"",Haber:"",observacion:"",estado:""};
   
       $scope.asiento.tipoPago = {tipoPagoID:"",nombre:""};
       $scope.asiento.clienteProveedor = {clienteProveedorID:"",datos:""};
       $scope.asiento.debe = {cuentaContableID:"",nombre:"",importe:""};
       $scope.asiento.haber = {cuentaContableID:"",nombre:"",importe:""};
     
    var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
   
   
  // listarLibroCaja();               
      
        
        
  function  verificaSaldo(){
     
        //preparamos un objeto request               
      var request = crud.crearRequest('libroCaja',1,'saldoLibro');        
        request.setData({organizacionID:$scope.consulta.organizacionID,personaID:$scope.consulta.personaID}); 
        //llamamos al servicio          
        crud.listar("/sistemaContable",request,function(response){
            //  modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
             if (response.data.estado==='I'|| response.data.estado==='C' || response.data.estado==='N'){
                  $scope.libroEstado.estado=response.data.estado;
                 modal.mensaje("CONFIRMACION",response.responseMsg);
             }  
             else{
                modal.mensaje("CONFIRMACION",response.responseMsg);  
                  $scope.libroEstado=response.data;
                  $scope.libroEstado.mes= new Date();
                  $scope.libroEstado.mesSel=$scope.libroEstado.mes.getMonth().toString();                  
                  if($scope.libroEstado.mes.getMonth()<9)
                      $scope.tablaLibroCaja.filter().fecha="/0"+ (parseFloat($scope.libroEstado.mesSel)+ 1) +"/";
                  else
                      $scope.tablaLibroCaja.filter().fecha="/"+ (parseFloat($scope.libroEstado.mesSel)+ 1) +"/";



                  settingLibroCaja.dataset =response.data.asientos;
                  $scope.tablaLibroCaja.settings(settingLibroCaja);
                  $scope.cuentasEfectivo = response.data.cuentasEfectivo;
                  $scope.hechosLibroCaja = response.data.hechosLibroCaja;
                  
                  
                  var fechaApertura = new Date($scope.libroEstado.libro.fechaApertura.toString());

                  if(fechaApertura.getMonth()=== $scope.libroEstado.mes.getMonth()){
                      var saldoApertura =0.00;
                      for(var i=0;i<$scope.cuentasEfectivo.length;i++)
                          saldoApertura += $scope.cuentasEfectivo[i].saldoApertura;                      
                      
                    $scope.libroEstado.saldoMesAnterior=saldoApertura.toFixed(2);
                    $scope.libroEstado.enunciado = "SALDO DE APERTURA INICIAL";
                  }
                  else{
                    $scope.libroEstado.enunciado = "SALDO DE MES ANTERIOR";
                  }
                  console.log(response.data);  
                  console.log($scope.pages);
             }
               
            }
        
        },function(data){
            console.info(data);
        });
     
        
    }; 
    
      
  function listarLibroCaja(){
        //preparamos un objeto request
        var request = crud.crearRequest('libroCaja',1,'listarTransaccionesConDocumentosPorOrganizacion');        
        request.setData({libroID:$scope.libroEstado.libroID, fecha:$scope.libro.mes});
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request             
        crud.listar("/sistemaContable",request,function(response){
           
            settingLibroCaja.dataset =response.data;
        //asignando la posicion en el arreglo a cada objeto
            $scope.libro.saldos=iniciarPosicionesYSumar(settingLibroCaja.dataset);
            $scope.libro.saldos.saldo=parseFloat($scope.libro.saldos.saldo)+ parseFloat($scope.libroEstado.saldoApertura);
            $scope.libro.saldos.saldo= $scope.libro.saldos.saldo.toFixed(2);
            $scope.tablaLibroCaja.settings(settingLibroCaja);
            
            console.log($scope.tablaLibroCaja);
            console.log($scope.libro.saldos);
        },function(data){
            console.info(data);
        });
        
      
    
  };
    
  /*Funcion que busca dentro de un array*/
    function buscarHechos(lista, labelClave, idBuscado,labelClaveF,idBuscadoF){
      var h=[];
        for(var i=0;i<lista.length;i++ ){
            if(lista[i][labelClave] === idBuscado && lista[i][labelClaveF] >= idBuscadoF )
                h.push(lista[i]);
          }

          return h;
    }   
/*Funcion que busca dentro de un array*/
    function buscarCuenta(lista, labelClave, idBuscado){
        for(var i=0;i<lista.length;i++ ){
            if(lista[i][labelClave] === idBuscado)
                return lista[i];
          }
    }
     
/*Funcion que busca dentro de un array y cambia contenido*/
    function buscarCuentaActualizar(lista, labelClave, idBuscado,labelContenido,contenido){
        for(var i=0;i<lista.length;i++ ){
            if(lista[i][labelClave] === idBuscado)
                lista[i][labelContenido]=contenido;                   
        }
    }
    
    
function actualizarLibro (asiento){
      var cuentaEfectivo;
      var hechoLibro=[];
      var datos={importe:asiento.importe,tipo:asiento.libro,libro:"",cuentaE:"",hechoL:"",debe:0.00,haber:0.00}; 

      
      if(asiento.libro==='C'){
          cuentaEfectivo=buscarCuenta($scope.cuentasEfectivo,'cuentaContableID',asiento.haber.cuentaContableID);
          hechoLibro= buscarHechos($scope.hechosLibroCaja,'cuentaContableID',asiento.haber.cuentaContableID,'fechaMes',$scope.libroEstado.mesSel);
            
         }
      else if(asiento.libro==='V'){
          cuentaEfectivo=buscarCuenta($scope.cuentasEfectivo,'cuentaContableID',asiento.debe.cuentaContableID);
          hechoLibro= buscarHechos($scope.hechosLibroCaja,'cuentaContableID',asiento.debe.cuentaContableID,'fechaMes',$scope.libroEstado.mesSel);
            
      }
          
          datos.libro=$scope.libroEstado.libro;
          datos.cuentaE=cuentaEfectivo;
          datos.hechoL=hechoLibro;
          datos.debe=$scope.libroEstado.debe;
          datos.haber=$scope.libroEstado.haber;

          
       var request = crud.crearRequest('libroCaja',1,'actualizarLibroYSaldos');                                   
       request.setData(datos);
       crud.actualizar("/sistemaContable",request,function(response){
           
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                //recuperamos las variables que nos envio el servidor
                $scope.libroEstado.libro.saldoActual = response.data.saldoActual;
                $scope.libroEstado.saldoMes = response.data.saldoMes;
                $scope.libroEstado.debe = response.data.debe;
                $scope.libroEstado.haber = response.data.haber;

                buscarCuentaActualizar($scope.cuentasEfectivo,'cuentaEfectivoID',response.data.cuentaEfectivoID,'saldoActual',response.data.importeCE);                                                 
                
                var hechosL=[];
                hechosL=response.data.hechosL;
                console.log(hechosL);
                if(response.data.tipo==='C'){
                    for(var i=0;i<hechosL.length;i++)
                        buscarCuentaActualizar($scope.hechosLibroCaja,'hechosID',hechosL[i]['hechosID'],'importeH',hechosL[i]['importeH']); 
                    }
                else if(response.data.tipo==='V'){
                     for(var i=0;i<hechosL.length;i++)
                        buscarCuentaActualizar($scope.hechosLibroCaja,'hechosID',hechosL[i]['hechosID'],'importeD',hechosL[i]['importeD']); 
                    }  
        }
            console.log( $scope.libroEstado);
           
        },function(data){
            console.info(data);
        });               
             
    }; 
    
  $scope.cambiarMes=function (mesSel)  {
      
      var hechos=[]; var debeM=0.00 , haberM=0.00 , debeMA=0.00 , haberMA=0.00, saldoMes=0.00 ,saldoMesA=0.00; 
      hechos=$scope.hechosLibroCaja;
      
      var mes= parseInt(mesSel), mesA=mes-1; 
      
      for(var i=0;i<hechos.length;i++){
          
         
           if(hechos[i].fechaMes === mes){                           
                debeM+= hechos[i].importeD ; 
                haberM+= hechos[i].importeH ; 
            }
            else if(hechos[i].fechaMes === mesA){
                debeMA+= hechos[i].importeD ; 
                haberMA+= hechos[i].importeH ; 
            }                                                                                                                                                             
      }
      saldoMes= debeM - haberM;
      saldoMesA = debeMA - haberMA;
      
      $scope.libroEstado.debe= debeM.toFixed(2);
      $scope.libroEstado.haber= haberM.toFixed(2);
      $scope.libroEstado.saldoMesAnterior= saldoMesA.toFixed(2);
      $scope.libroEstado.saldoMes=saldoMes.toFixed(2);
      
       if(mesSel<9){
           $scope.tablaLibroCaja.filter().fecha="/0"+ (parseFloat($scope.libroEstado.mesSel)+ 1) +"/";
           $scope.libroEstado.mesSel=mesSel;
           $scope.libroEstado.mes.setMonth(mesSel);
       }
       else{
           $scope.tablaLibroCaja.filter().fecha="/"+ (parseFloat($scope.libroEstado.mesSel)+ 1) +"/";
           $scope.libroEstado.mesSel=mesSel;
           $scope.libroEstado.mes.setMonth(mesSel);
       }
       
         var fechaApertura = new Date($scope.libroEstado.libro.fechaApertura.toString());

                  if(fechaApertura.getMonth()=== $scope.libroEstado.mes.getMonth()){
                      var saldoApertura =0.00;
                      for(var i=0;i<$scope.cuentasEfectivo.length;i++)
                          saldoApertura += $scope.cuentasEfectivo[i].saldoApertura;                      
                      
                    $scope.libroEstado.saldoMesAnterior=saldoApertura.toFixed(2);
                    $scope.libroEstado.enunciado = "SALDO DE APERTURA INICIAL";
                  }
                  else{
                    $scope.libroEstado.enunciado = "SALDO DE MES ANTERIOR";
                  }
       
      


    };


    
  $scope.listarOpe = function(){
        //preparamos un objeto request
        var request = crud.crearRequest('operacion',1,'listarOperacion');        
        
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request             
        crud.listar("/sistemaContable",request,function(response){
           
            settingOperaciones.dataset = response.data;
        //asignando la posicion en el arreglo a cada objeto
            iniciarPosiciones(settingOperaciones.dataset);
            $scope.tablaOperaciones.settings(settingOperaciones);
           
        },function(data){
            console.info(data);
        });
        
       console.log($scope.tablaOperaciones);
    };    
    
    $scope.listarClienteProveedor = function(){
        //preparamos un objeto request
        var request = crud.crearRequest('libroCaja',1,'listarClienteProveedor');        
        
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request             
        crud.listar("/sistemaContable",request,function(response){
           
            settingClienteProveedor.dataset = response.data;
        //asignando la posicion en el arreglo a cada objeto
            iniciarPosiciones(settingClienteProveedor.dataset);
            $scope.tablaClienteProveedor.settings(settingClienteProveedor);
           
        },function(data){
            console.info(data);
        });
        
       console.log($scope.tablaClienteProveedor);
    }; 
    
    listarTipoPago();
    
    function listarTipoPago(){
        //preparamos un objeto request
        var request = crud.crearRequest('libroCaja',1,'listarTipoPago');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/sistemaContable",request,function(data){
            $scope.tipoPago = data.data;
        },function(data){
            console.info(data);
        });
    };
    
   // listarCuentasEfectivo();
       function listarCuentasEfectivo(){
        //preparamos un objeto request
        var request = crud.crearRequest('libroCaja',1,'listarCuentasEfectivo');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/sistemaContable",request,function(data){
            $scope.cuentasEfectivo = data.data;
        },function(data){
            console.info(data);
        });
    };
    
    $scope.showNuevaTransaccion = function() {

    ModalService.showModal({
      templateUrl: "administrativa/sistema_contable_institucional/agregarTransaccion.html",
      controller: "agregarTransaccionCtrl",
      inputs: {
        title: "Seleccionar Operación",
        tablaOperacion:$scope.tablaOperaciones,
        tablaClienteProveedor:$scope.tablaClienteProveedor,
        listTipoPago:$scope.tipoPago,
        listCuentasE:$scope.cuentasEfectivo,
        libroID:$scope.libroEstado.libro.libroID,
        mesSel:$scope.libroEstado.mesSel
    }                 
    }).then(function(modal) {
      modal.element.modal();
      modal.close.then(function(result) {
          if(result.flag){                                          
                
                $scope.asiento  = result.data;
              //$scope.asiento.fecha=new Date($scope.asiento.fecha);             
                //insertamos el elemento a la lista
                insertarElemento(settingLibroCaja.dataset,$scope.asiento);
                $scope.tablaLibroCaja.reload();  
                
                //funcion que permite actualiza salods de las tablas LibroCaj ,cuentasefectivo y hechos 
                actualizarLibro($scope.asiento);
          // Ver contnido del libro y el asiento registrado
           //     console.log($scope.asiento);
          //      console.log($scope.libroEstado);
          }
         

      });
    });
   
  };
  
    $scope.prepararEditar = function(t){
     
      $scope.asiento = JSON.parse(JSON.stringify(t));
      
    ModalService.showModal({
      templateUrl: "administrativa/sistema_contable_institucional/editarTransaccion.html",
      controller: "editarTransaccionCtrl",
      inputs: {
        title: "Datos de la Operación",
        tablaOperacion:$scope.tablaOperaciones,
        tablaClienteProveedor:$scope.tablaClienteProveedor,
        listTipoPago:$scope.tipoPago,
        listCuentasE:$scope.cuentasEfectivo,
        asiento:$scope.asiento 
    }                 
    }).then(function(modal) {
      modal.element.modal();
      modal.close.then(function(result) {
          if(result.flag){
              
              
              
                $scope.asiento  = result.data;
               
                //insertamos el elemento a la lista
                insertarElemento(settingLibroCaja.dataset,$scope.asiento);
                $scope.tablaLibroCaja.reload();
                console.log($scope.asiento);
               
          }
        
      });
    });
   
  };
  
    
         $scope.fecha=function(d){
          var i=new Date(d.y,d.m,d.d);
            return i;
        };
        
         $scope.getFilterMonth= function (n){
            if(n==='0'){$scope.tablaLibroCaja.filter().fecha="Jan";}         
            else if(n==='1'){$scope.tablaLibroCaja.filter().fecha="Feb";}
            else if(n==='2'){$scope.tablaLibroCaja.filter().fecha="Mar";}
            else if(n==='3'){$scope.tablaLibroCaja.filter().fecha="Apr";}
            else if(n==='4'){$scope.tablaLibroCaja.filter().fecha="May";}
            else if(n==='5'){$scope.tablaLibroCaja.filter().fecha="Jun";}
            else if(n==='6'){$scope.tablaLibroCaja.filter().fecha="Jul";}
            else if(n==='7'){$scope.tablaLibroCaja.filter().fecha="Agu";}
            else if(n==='8'){$scope.tablaLibroCaja.filter().fecha="Sep";}
            else if(n==='9'){$scope.tablaLibroCaja.filter().fecha="Oct";}
            else if(n==='10'){$scope.tablaLibroCaja.filter().fecha="Nov";}
            else if(n==='11'){$scope.tablaLibroCaja.filter().fecha="Dec";}
          else {$scope.tablaLibroCaja.filter().fecha="";}
                     
                
        };
        
        //Visualizar de archivos en formato PDF
    $scope.reporte = function () {        
        var request = crud.crearRequest('libroCaja',1,'reporteLibroCaja');       
   
        request.setData($scope.tablaLibroCaja.settings().dataset);        
        crud.insertar("/sistemaContable",request,function(data){            
            $scope.dataBase64 = data.data[0].datareporte;
            window.open($scope.dataBase64);
        },function(data){
            console.info(data);
        });            
    };
        
       
 }]);


app.controller('editarTransaccionCtrl', [
  '$scope', '$element', 'listTipoPago','listCuentasE','asiento','title','tablaOperacion','tablaClienteProveedor','NgTableParams', 'close','crud','modal',"ModalService",
  function($scope, $element,listTipoPago,listCuentasE,asiento,title,  tablaOperacion,tablaClienteProveedor,NgTableParams, close,crud,modal,ModalService) {
         
     
    $scope.title=title;
    $scope.asiento=asiento;   
    var day = $scope.asiento.fecha.toString().substr(0,2);
    var month = $scope.asiento.fecha.toString().substr(3,2);
    var year = $scope.asiento.fecha.toString().substr(6,4);
    
    $scope.asiento.fecha= new Date(year,month-1,day);
    $scope.tipoPago=listTipoPago;
    $scope.cuentasEfectivo= listCuentasE;
 
 function fecha(d){
          var i=new Date(d.y,d.m,d.d);
            return i;
        };
        
        
  

  $scope.open1 = function() {
    $scope.popup1.opened = true;
  };
  
   $scope.popup1 = {
    opened: false
  };
   
   $scope.getFecha=function(fecha,dato){
            var f=dato;               
            if(f){
            fecha.d=f.getDate();
            fecha.m=f.getMonth();
            fecha.y=f.getFullYear();
            }
            
        };
         
         
   $scope.showBuscarOperacion = function(flag) {
   if($scope.asiento.libro==='C')
       tablaOperacion.filter().tipo="E";        //cambiar por C compras     
   else
       tablaOperacion.filter().tipo="I";        //cambiar por V ventas
    
    ModalService.showModal({
      templateUrl: "administrativa/sistema_contable_institucional/buscarOperacion.html",
      controller: "buscarOperacionCtrl",
      inputs: {
        title: "Seleccionar Operación",
        tabla: tablaOperacion,
        tablaClienteProveedor:tablaClienteProveedor
    }                 
    }).then(function(modal) {
      modal.element.modal();
      modal.close.then(function(result) {
          if(result.flag){
             //cambiar por C compras  
                  $scope.asiento.operacionID=result.data.operacionID;
                  $scope.asiento.glosa=result.data.descripcion;
                  $scope.asiento.debe=result.data.cuentaOperaciones[0];
              
            
              
             
            
          }
      
        
      });
    });
   
  };
  
   $scope.showBuscarClienteProveedor = function(flag) {
   if($scope.asiento.libro==='C')
       tablaClienteProveedor.filter().tipo="P";             
   else
       tablaClienteProveedor.filter().tipo="C"; 
   
    ModalService.showModal({
      templateUrl: "administrativa/sistema_contable_institucional/buscarClienteProveedor.html",
      controller: "buscarClienteProveedorCtrl",
      inputs: {
        title: "Seleccionar Datos",      
        tablaClienteProveedor:tablaClienteProveedor
    }                 
    }).then(function(modal) {
      modal.element.modal();
      modal.close.then(function(result) {
          if(result.flag){
                              
                  $scope.asiento.clienteProveedor=result.data;
                 
              
              
             
            
          }
      
        
      });
    });
   
  };
}]);


        
