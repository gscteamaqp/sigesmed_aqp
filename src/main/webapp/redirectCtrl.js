/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var colores = ["#fa8564","#1fb5ad","#9466b5","#58C9F3","#CE93D8","#90CAF9","#4DB6AC","#1DE9B6","#DCE775","#EEFF41","#FF9800","#BDBDBD","#F48FB1","#3F51B5","#006064","#FF6F00","#8D6E63","#B71C1C","#FFEE58","#607D8B","#FFF176","#43A047","#FF5722"];
app.factory('servicioLogin', ['$http','urls', function($http,urls){
    return{
        buscarUsuario: function(data,sucess,error){
            $http({
                method: 'POST',
                url: urls.BASE + "/login",
                headers: {'Content-Type': 'text/plain'},
                data: data
            }).success( sucess ).error( error);
        },
        recuperarPassword: function(data,sucess,error){
            $http({
                method: 'PUT',
                url: urls.BASE + "/login",
                headers: {'Content-Type': 'text/plain'},
                data: data
            }).success( sucess ).error( error);
        },
        mensaje: function(titulo,mensaje){
            $.gritter.add({
                // (string | mandatory) the heading of the notification
                title: titulo,
                // (string | mandatory) the text inside the notification
                text: mensaje,
                // (string | optional) the image to display on the left
                //image: 'assets/img/ui-sam.jpg',
                // (bool | optional) if you want it to fade out on its own or just sit there
                sticky: false,
                // (int | optional) the time you want it to be alive for before fading out
                time: '2500',
                // (string | optional) the class name you want to apply to that specific message
                class_name: 'my-sticky-class'
            });
        }
    };
}]);
app.controller("redirectCtrl",["$scope", "$location", "$rootScope", "servicioLogin", "urls", function ($scope,$location,$rootScope,servicioLogin, urls){
    var user = $location.$$search['user'];
    var password = $location.$$search['pass'];
    var linkredirect = $location.$$search['link'];
    
    if(!user || user==""){
        servicioLogin.mensaje("MENSAJE","ingrese nombre de usuario");
        return;
    }
    var request = new Request(user,'web');
    request.setCmd('login',1,'search');
    var usuario = {};
    usuario.nombre = user;
    $rootScope.usuario = usuario;
    request.setData($rootScope.usuario);
    $rootScope.bloquear=true;
    servicioLogin.buscarUsuario(request,function(response){
        $rootScope.session = {};
        if( response.responseSta ){                
            $rootScope.organizaciones = response.data;
            $rootScope.session.organizacion = $rootScope.organizaciones[0];
            $rootScope.session.rolID = $rootScope.session.organizacion.roles[0].rolID;
            $rootScope.bloquear=false;
            
            
            /*lpgin pass*/
            if(!password || password==""){
                servicioLogin.mensaje("MENSAJE","ingrese su password");
                return;
            }
            if(!$rootScope.session.organizacion || $rootScope.session.organizacion==null){
                servicioLogin.mensaje("MENSAJE","Seleccine una Organizacion");
                return;
            }
            if(!$rootScope.session.rolID || $rootScope.session.rolID==null){
                servicioLogin.mensaje("MENSAJE","Seleccine un Rol");
                return;
            }

            $rootScope.usuario.organizacionID = $rootScope.session.organizacion.organizacionID;
            $rootScope.usuario.rolID = $rootScope.session.rolID;
            $rootScope.usuario.nombre = user;
            $rootScope.usuario.password = password;

            var request = new Request($rootScope.usuario.nombre,'web');
            request.setCmd('login',1,'signin');
            //request.setMetadataValue('user.password','sh5');
            request.setData($rootScope.usuario);
            $rootScope.bloquear=true;
            servicioLogin.buscarUsuario(request,succesIniciarSession,errorIniciarSession);
            /*en login pass*/
            return;                
        }
        $rootScope.bloquear=false;
    },function(){
        $rootScope.bloquear=false;
        console.log(response);
    });
    
    function succesIniciarSession(response){
        $rootScope.bloquear=false;
        if( response.responseSta ){
            var objResponse = response.data;
            localStorage.setItem('jwt', objResponse.jwt);
            localStorage.setItem('usuario', window.btoa(JSON.stringify(objResponse.usuario)) );//JSON.stringify({usuarioID:objResponse.usuarioID,nombre:$rootScope.usuario.nombre}) );
            localStorage.setItem('rol', window.btoa(JSON.stringify(objResponse.rol)) );//JSON.stringify({roldID:$rootScope.rolSel.rolID,nombre:$rootScope.rolSel.nombre}));
            localStorage.setItem('organizacion', window.btoa(JSON.stringify(objResponse.organizacion)) );// JSON.stringify({organizacionID:$rootScope.organizacionSel.organizacionID,nombre:$rootScope.organizacionSel.nombre}));
            if(objResponse.personalizacion)
                localStorage.setItem('personalizacion', window.btoa(JSON.stringify(objResponse.personalizacion)));
            if(objResponse.area)
                localStorage.setItem('area', window.btoa(JSON.stringify(objResponse.area)) );
            
            var funciones = [[],[],[],[],[],[]];
            objResponse.modulos.forEach(function(item){
                item.color = colores[item.moduloID-1];
                item.subModulos.forEach(function(sub){
                    sub.color = colores[sub.subModuloID-1];
                    sub.funciones.forEach(function(fun){
                        fun.color = colores[sub.subModuloID-1];
                        funciones[fun.tipo].push(fun);
                    });
                });                
            });
            localStorage.setItem('modulos', JSON.stringify(objResponse.modulos));
            localStorage.setItem('funciones', JSON.stringify(funciones));
            localStorage.setItem('events', JSON.stringify(objResponse.events));
            
            //Modulo cod PED  SUBModulo codigo MMANT funcion clave ayuBas
            var modulo = null;
            var subModulo = null;
            var vista = null;
            for(var i = 0; i < objResponse.modulos.length && vista === null; i++) {
                modulo = objResponse.modulos[i];
                for(var j = 0; j < objResponse.modulos[i].subModulos.length && vista === null; j++) {
                    subModulo = objResponse.modulos[i].subModulos[j];
                    vista = buscarObjeto( objResponse.modulos[i].subModulos[j].funciones, "clave", linkredirect);
                }
            }
            
            localStorage.setItem('menu', JSON.stringify(subModulo));
            localStorage.setItem('modNom', JSON.stringify(modulo.nombre));
            localStorage.setItem('subModNom', JSON.stringify(subModulo.nombre));
            localStorage.setItem('visNom', JSON.stringify(vista.nombre));
            location.replace( urls.BASECONTEXTO + objResponse.url+"#"+linkredirect );
            return;
        }
        servicioLogin.mensaje("MENSAJE",response.responseMsg);
    };
    function errorIniciarSession(response){
        $rootScope.bloquear=false;
        console.log(response);
    };
    function buscarObjeto(lista, labelClave,idBuscado){
        if(!lista)
            return null;
        for(var i=0;i<lista.length;i++ ){
            if(lista[i][labelClave] === idBuscado)
                return lista[i];
        }
        return null;
    };
}]);

