/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.estudiante.datos_estudiante.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mpf.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Seccion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Grado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.ListaArticulo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.ListaUtiles;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;


/**
 *
 * @author carlos
 */
public class ListarUtilesByAreaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
       
        Grado grado=null;
        Seccion seccion=null;
        AreaCurricular area=null;
        Organizacion organizacion=null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            Integer gradoId = requestData.getInt("grado");
            int seccionId = requestData.getInt("seccion");
            Integer areaId = requestData.getInt("area");
            Integer orgId = requestData.getInt("organizacionID");
            
            grado=new Grado(gradoId);
            seccion=new Seccion((char)seccionId);
            area=new AreaCurricular(areaId);
            organizacion=new Organizacion(orgId);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo verificar los datos", e.getMessage() );
        }

        ListaUtiles lista = null;
        EstudianteDao estudianteDao = (EstudianteDao)FactoryDao.buildDao("mpf.EstudianteDao");
        try{
            lista=estudianteDao.listarUtilesByArea(grado,seccion,area,organizacion);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar los Estudiantes del Pariente ", e.getMessage() );
        }
        
         if(lista==null || lista.getListas().size()==0)
        {
             return WebResponse.crearWebResponseError("No hay lista de Articulos" );
        }
        
        JSONArray miArray = new JSONArray();
        if(lista==null)
        {
            return WebResponse.crearWebResponseExito("No se encuentra una Lista de Utiles para el Area Seleccionada");        
        }
        
        for(ListaArticulo la:lista.getListas() ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("cant",la.getCanArt());
            oResponse.put("des",la.getDesArt());
            oResponse.put("precio",la.getPreArt());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}

