/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_comunicacion.configuracion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sdc.PlantillaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.Plantilla;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class ListarPlantillasTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        Integer organizacoinId;
        Organizacion _user =null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            organizacoinId = requestData.getInt("organizacionID");        
            _user = new Organizacion(organizacoinId);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar las Plantillas", e.getMessage() );
        }

        List<Plantilla> plantillas = new ArrayList<>();
        PlantillaDao plantillaDao = (PlantillaDao)FactoryDao.buildDao("sdc.PlantillaDao");
        try{
            plantillas =plantillaDao.listarPlantillas(_user);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar las Plantillas ", e.getMessage() );
        }

        JSONArray miArray = new JSONArray();
       
        DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        for(Plantilla plantilla:plantillas ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("plantillaId",plantilla.getPlaIde());
            oResponse.put("descripcion",plantilla.getPlaDes());
            oResponse.put("version",plantilla.getPlaVer());
            String fecha=sdf.format(plantilla.getFecRegistro());
            oResponse.put("fechaRegistro",fecha);
            oResponse.put("estado", plantilla.getPlaEst());
            oResponse.put("usuario",plantilla.getPlaUsu().getUsuSesId());
            oResponse.put("tipoDocumentoID",plantilla.getTipoDocumento().getTipDocId());
            oResponse.put("archivo",plantilla.getArchivo());
            oResponse.put("url",plantilla.getUrl());
//            oResponse.put("_descripcion",plantilla.getTipoDocumento().getDes());
//            oResponse.put("_fecha",plantilla.getTipoDocumento().getFecMod().toString());
//            oResponse.put("_estado",plantilla.getTipoDocumento().getEstReg());
//            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}

