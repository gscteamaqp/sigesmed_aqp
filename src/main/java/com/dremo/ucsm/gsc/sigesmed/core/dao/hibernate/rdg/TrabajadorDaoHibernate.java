/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.rdg;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.TrabajadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TrabajadorCargo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author christian
 */
public class TrabajadorDaoHibernate extends GenericDaoHibernate<Trabajador> implements TrabajadorDao {

    @Override
    public Trabajador buscarPorPersonayCargo(Long idePersona, TrabajadorCargo ideCargo) {
        Trabajador trabajador = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar ultimo codigo de tramite
            String hql = "SELECT t FROM Trabajador t WHERE t.perId=:p1 and t.traCar=:p2";
            Query query = session.createQuery(hql);
            query.setParameter("p1", idePersona);
            query.setParameter("p2", ideCargo);
            //puede haber mas resultados pero por el cargo solo cogeremos el trabajdor
            //que tenga dicho cargo para asignarlo a la comparticion
            query.setMaxResults(1); 
            trabajador = ((Trabajador)query.uniqueResult());
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar al trabajador \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar al trabajador \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return trabajador;
    }
  @Override
    public List<UsuarioSession> buscarConRolPorOrganizacionYNombre(int orgID, String nom) {
        List<UsuarioSession> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar Usuarios
            String hql = "SELECT us FROM UsuarioSession us JOIN FETCH us.usuario u JOIN FETCH us.persona p JOIN FETCH us.rol r JOIN FETCH us.organizacion LEFT JOIN FETCH us.area WHERE u.estReg!='E' and us.estReg!='E' "
                    + "and us.organizacion.orgId=:p1 and (p.nom LIKE :nom or p.apeMat LIKE :nom or p.apePat LIKE :nom) ORDER BY us.fecCre" ;
            Query query = session.createQuery(hql);
            query.setParameter("p1", orgID);
            query.setString("nom", '%'+nom+'%');
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los Usuarios \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los Usuarios \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }

    @Override
    public UsuarioSession getByID(int id) {
        Session session=HibernateUtil.getSessionFactory().openSession();
        UsuarioSession us=null;        
        try{
            String hql = "SELECT us FROM UsuarioSession us JOIN FETCH us.usuario u JOIN FETCH us.persona p JOIN FETCH us.rol r JOIN FETCH us.organizacion LEFT JOIN FETCH us.area WHERE u.estReg!='E' and us.estReg!='E' "
                        + "and p.perId=:p1" ;
            Query q=session.createQuery(hql);
            q.setParameter("p1",id);
            //ESTA LINEA ESTE MAL, CORREGIR deberia ser asi: us=(UsuarioSession)q.uniqueResult();
            //us=(UsuarioSession)q.setMaxResults(1).uniqueResult();
            us=(UsuarioSession)q.uniqueResult();
            //FIN
        }catch(Exception e){
            System.out.println("No se pudo listar los datos de la persona "+e.getMessage());
            throw new UnsupportedOperationException("No se pudo istar los datos de la persona \\n "+ e.getMessage());           
        }finally{
            session.close();
        }
        return us;
    }
    @Override
    public UsuarioSession getByUsuarioSesionID(int id) {
        Session session=HibernateUtil.getSessionFactory().openSession();
        UsuarioSession us=null;        
        try{
            String hql = "SELECT us FROM UsuarioSession us JOIN FETCH us.usuario u JOIN FETCH us.persona p JOIN FETCH us.rol r JOIN FETCH us.organizacion LEFT JOIN FETCH us.area WHERE u.estReg!='E' and us.estReg!='E' "
                        + "and us.usuSesId=:p1" ;
            Query q=session.createQuery(hql);
            q.setParameter("p1",id);
            //ESTA LINEA ESTE MAL, CORREGIR deberia ser asi: us=(UsuarioSession)q.uniqueResult();
            //us=(UsuarioSession)q.setMaxResults(1).uniqueResult();
            us=(UsuarioSession)q.uniqueResult();
            //FIN
        }catch(Exception e){
            System.out.println("No se pudo listar los datos de la persona "+e.getMessage());
            throw new UnsupportedOperationException("No se pudo istar los datos de la persona \\n "+ e.getMessage());           
        }finally{
            session.close();
        }
        return us;
    }
 
}
