/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.ItemFileDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.ItemFile;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;

/**
 *
 * @author ucsm
 */
public class ListarPapeleraTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        /*Solo interactuamos con la base de datos */
        
        ItemFileDao iteFilDao = (ItemFileDao)FactoryDao.buildDao("rdg.ItemFileDao");
        List<ItemFile> listAllDocs = iteFilDao.buscarEliminados();
        
        //Retornar respuesta con lista de documentos
        JSONArray listDocs = new JSONArray();
        listDocs.put(listAllDocs);
        
        return WebResponse.crearWebResponseExito("Fue exitoso la insercion de archivos",listDocs);
    }
    
}
