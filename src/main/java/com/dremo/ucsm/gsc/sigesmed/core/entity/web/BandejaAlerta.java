package com.dremo.ucsm.gsc.sigesmed.core.entity.web;

import com.dremo.ucsm.gsc.sigesmed.core.entity.AlertaSistema;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="pedagogico.bandeja_alerta" )
public class BandejaAlerta  implements java.io.Serializable {

    @Id
    @Column(name="ban_ale_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_bandejaalerta", sequenceName="pedagogico.bandeja_alerta_ban_ale_id_seq" )
    @GeneratedValue(generator="secuencia_bandejaalerta")
    private int banAleId;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_env", nullable=false, length=29)
    private Date fecEnv;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_vis", nullable=false, length=29)
    private Date fecVis;    
    
    @Column(name="ale_sis_id")
    private int aleId;    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ale_sis_id",insertable = false,updatable = false)
    private AlertaSistema alerta;
    
    @Column(name="usu_ses_id")
    private int usuSesId;

    public BandejaAlerta() {
    }
    public BandejaAlerta(int banAleId) {
        this.banAleId = banAleId;
    }
    public BandejaAlerta(int banAleId, Date fecEnv,Date fecVis,int aleId,int usuarioId) {
       this.banAleId = banAleId;
       this.fecVis = fecVis;
       this.fecEnv = fecEnv;
       this.aleId = aleId;
       this.usuSesId = usuarioId;       
    }
   
     
    public int getBanAleId() {
        return this.banAleId;
    }    
    public void setBanAleId(int banAleId) {
        this.banAleId = banAleId;
    }
    
    public Date getFecVis() {
        return this.fecVis;
    }
    public void setFecVis(Date fecVis) {
        this.fecVis = fecVis;
    }
    
    public Date getFecEnv() {
        return this.fecEnv;
    }
    public void setFecEnv(Date fecEnv) {
        this.fecEnv = fecEnv;
    }
    
    public int getAleId() {
        return this.aleId;
    }    
    public void setAleId(int aleId) {
        this.aleId = aleId;
    }
    
    public AlertaSistema getAlerta() {
        return this.alerta;
    }
    public void setAlerta(AlertaSistema alerta) {
        this.alerta = alerta;
    }
    
    public int getUsuSesId() {
        return this.usuSesId;
    }    
    public void setUsuSesId(int usuSesId) {
        this.usuSesId = usuSesId;
    }
}


