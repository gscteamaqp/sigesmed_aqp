package com.dremo.ucsm.gsc.sigesmed.core.dao.scec;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Rol;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.IntegranteComision;

import java.util.List;

/**
 * Created by geank on 28/09/16.
 */
public interface IntegranteComisionDao extends GenericDao<IntegranteComision> {
    List<IntegranteComision> buscarPorComision(int idCom);
    IntegranteComision buscarPorComision(int idPersona,int idComision);
    IntegranteComision buscarPresidenteComision(int idComision);
    IntegranteComision buscarPorComisionConAsistencias(int idPersona,int idComision);
    void cambiarCargoComision(int nuevoCargo,int anteriorCargo);
    Rol buscarRolPorNombre(String nombreRol);
}
