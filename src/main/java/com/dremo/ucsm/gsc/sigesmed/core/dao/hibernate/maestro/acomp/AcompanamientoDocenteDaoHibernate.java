package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.maestro.acomp;


import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.acomp.AcompanamientoDocenteDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.acomp.AcompanamientoDocente;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import java.util.List;

/**
 * Created by Administrador on 05/01/2017.
 */
public class AcompanamientoDocenteDaoHibernate extends GenericDaoHibernate<AcompanamientoDocente> implements AcompanamientoDocenteDao {
    @Override
    public List<Organizacion> listarOrganizcionesHijas(int idPadre) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT o FROM Organizacion o INNER JOIN o.organizacionPadre op WHERE op.orgId =:id";
            Query query = session.createQuery(hql);
            query.setInteger("id",idPadre);
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public AcompanamientoDocente buscarAcompanamiento(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria criteria = session.createCriteria(AcompanamientoDocente.class)
                    .add(Restrictions.eq("acoId", id))
                    .setFetchMode("compromisos", FetchMode.JOIN)
                    .createAlias("area", "a", JoinType.INNER_JOIN)
                    .createAlias("nivel", "n", JoinType.INNER_JOIN)
                    .createAlias("grado", "g", JoinType.INNER_JOIN)
                    .createAlias("seccion", "s", JoinType.INNER_JOIN);
            return (AcompanamientoDocente)criteria.uniqueResult();

        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<AcompanamientoDocente> buscarAcompanamientosDocente(int idDoc) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria criteria = session.createCriteria(AcompanamientoDocente.class)
                    .add(Restrictions.eq("estReg", 'A'))
                    .setFetchMode("compromisos", FetchMode.JOIN)
                    //.createAlias("compromisos", "c", JoinType.INNER_JOIN)
                    .createAlias("area", "a", JoinType.INNER_JOIN)
                    .createAlias("nivel", "n", JoinType.INNER_JOIN)
                    .createAlias("grado", "g", JoinType.INNER_JOIN)
                    .createAlias("seccion", "s", JoinType.INNER_JOIN)
                    .createCriteria("docente", "d", JoinType.INNER_JOIN).add(Restrictions.eq("doc_id",idDoc))
                    .setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);;
            return criteria.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
}
