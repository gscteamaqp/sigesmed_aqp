package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.ParticipanteCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.ParticipanteCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.ParticipanteCapacitacionId;
import java.math.BigInteger;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class ParticipanteCapacitacionDaoHibernate extends GenericDaoHibernate<ParticipanteCapacitacion> implements ParticipanteCapacitacionDao {
    
    private static final Logger logger = Logger.getLogger(ParticipanteCapacitacionDaoHibernate.class.getName());
    
    @Override
    public boolean verificarParticipante(String dniDoc, int codSed) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Criteria query = session.createCriteria(ParticipanteCapacitacion.class)
                    .createAlias("sedCap", "sed")
                    .createAlias("persona", "per")
                    .add(Restrictions.eq("per.dni", dniDoc))
                    .add(Restrictions.eq("sed.sedCapId", codSed))
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            
            return (query.uniqueResult() == null);
        } catch (Exception e){
            logger.log(Level.SEVERE,"buscarPorDni",e);
            throw e;
        } finally {
            session.close();
        }      
    }    
    
    @Override
    public ParticipanteCapacitacion buscarPorId(int codPar, int codSed) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            ParticipanteCapacitacionId id = new ParticipanteCapacitacionId(codSed,codPar);
            ParticipanteCapacitacion participante = (ParticipanteCapacitacion)session.get(ParticipanteCapacitacion.class,id);

            return participante;
        } catch (Exception e){
            logger.log(Level.SEVERE,"buscarPorId",e);
            throw e;
        } finally {
            session.close();
        }   
    }
    
    @Override
    public List<Integer> listarMensaje(int codSed) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            
            String queryStr = "SELECT usuario_session.usu_ses_id FROM public.usuario_session, pedagogico.participantes_capacitacion " +
                                "WHERE participantes_capacitacion.per_id = usuario_session.usu_id AND participantes_capacitacion.sed_cap_id = " + codSed + " AND participantes_capacitacion.est_reg = 'A'";
            
            return (List<Integer>) session.createSQLQuery(queryStr).list();
        } catch (Exception e){
            logger.log(Level.SEVERE,"listarMensaje",e);
            throw e;
        } finally {
            session.close();
        }   
    }
    
    @Override
    public List<BigInteger> listarParticipantes(int codSed) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            
            String queryStr = "SELECT participantes_capacitacion.per_id FROM pedagogico.participantes_capacitacion WHERE "
                                + "participantes_capacitacion.sed_cap_id = " + codSed + " AND participantes_capacitacion.est_reg = 'A'";
            
            return (List<BigInteger>) session.createSQLQuery(queryStr).list();
        } catch (Exception e){
            logger.log(Level.SEVERE, "listarDesarrollo",e);
            throw e;
        } finally {
            session.close();
        }   
    }
    
    @Override
    public List<ParticipanteCapacitacion> listarPorSede(int codSed) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Criteria query = session.createCriteria(ParticipanteCapacitacion.class)
                    .add(Restrictions.eq("estReg", 'A'))                    
                    .createCriteria("sedCap")
                    .add(Restrictions.eq("sedCapId",codSed));
            
            return query.list();
        } catch (Exception e){
            logger.log(Level.SEVERE,"listarPorSede",e);
            throw e;
        } finally {
            session.close();
        }      
    }
    
    @Override
    public ParticipanteCapacitacion buscarPorSedeDni(String dni, int codSed) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Criteria query = session.createCriteria(ParticipanteCapacitacion.class)
                    .createAlias("sedCap", "sed")
                    .createAlias("persona", "per")
                    .add(Restrictions.eq("per.dni", dni))
                    .add(Restrictions.eq("sed.sedCapId", codSed))
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            
            return (ParticipanteCapacitacion) query.uniqueResult();
        } catch (Exception e){
            logger.log(Level.SEVERE,"buscarPorSedeDni",e);
            throw e;
        } finally {
            session.close();
        }    
    }  
}
