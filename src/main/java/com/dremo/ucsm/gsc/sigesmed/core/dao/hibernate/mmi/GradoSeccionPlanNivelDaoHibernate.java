package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi;

import com.dremo.ucsm.gsc.sigesmed.util.DateUtil;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GradoSeccionPlanNivelDaoHibernate extends GenericQueryDaoHibernate {

    //Suponiendo que un estudiante solo tiene una matricula activa
    public List<Object[]> get4OrgAndYear(long orgId, int year) {
        List<Object[]> result = new ArrayList<>();
        try {
            String sql = "SELECT gspn.gra_sec_pla_est_id AS ele0, CONCAT(je.abr,CONCAT(': ', pn.des)) AS ele1, \n"
                    + "gr.gra_id AS ele2, gspn.sec_id AS ele3,  gspn.num_alu_max AS ele4, gspn.num_alu_mat AS ele5, gspn.pla_niv_id AS ele6 \n"
                    + "FROM grado_seccion_plan_nivel AS gspn\n"
                    + "INNER JOIN institucional.plan_nivel AS pn ON pn.pla_niv_id = gspn.pla_niv_id\n"
                    + "INNER JOIN institucional.plan_estudios AS pe ON pe.pla_est_id = pn.pla_est_id\n"
                    + "INNER JOIN grado AS gr ON gr.gra_id = gspn.gra_id\n"
                    + "INNER JOIN jornada_escolar AS je ON je.jor_esc_id = pn.jor_esc_id\n"
                    + "WHERE gspn.est_reg = 'A' AND pe.\"org_id\" = " + orgId + " AND pe.\"ano_esc\" = '" + year + "-01-01'";

            result = sqlQuery(sql);

        } catch (Exception e) {
            throw e;
        }
        return result;
    }

    //Suponiendo que un estudiante solo tiene una matricula activa
    public List<Object[]> getGradosySecciones(long orgId, int year) {
        List<Object[]> result = new ArrayList<>();
        try {
            String sql = "SELECT \n"
                    + "gspn.gra_sec_pla_est_id AS gradoSecId, gspn.num_alu_max AS gradoSecNumMax, gspn.num_alu_mat AS gradoSecMat, gspn.pla_niv_id AS planNivelId,\n"
                    + "nv.niv_id AS nivelId, nv.nom AS nivelNom, nv.abr AS nivelAbr,\n"
                    + "je.jor_esc_id AS jornadaId, je.nom AS jornadaNom, je.abr AS jornadaAbr, \n"
                    + "tr.tur_id AS turnoId, tr.nom AS turnoNom,\n"
                    + "gr.gra_id AS gradoId, gr.nom AS gradoNom, gr.abr AS gradoAbr,\n"
                    + "se.sec_id AS seccId, se.nom AS seccNom \n"
                    + "FROM grado_seccion_plan_nivel AS gspn\n"
                    + "INNER JOIN institucional.plan_nivel AS pn ON pn.pla_niv_id = gspn.pla_niv_id\n"
                    + "INNER JOIN institucional.plan_estudios AS pe ON pe.pla_est_id = pn.pla_est_id\n"
                    + "INNER JOIN grado AS gr ON gr.gra_id = gspn.gra_id\n"
                    + "INNER JOIN jornada_escolar AS je ON je.jor_esc_id = pn.jor_esc_id\n"
                    + "INNER JOIN nivel AS nv ON nv.niv_id = je.niv_id\n"
                    + "INNER JOIN institucional.turno AS tr ON tr.tur_id = pn.tur_id\n"
                    + "INNER JOIN institucional.seccion AS se ON se.sec_id = gspn.sec_id\n"
                    + "WHERE gspn.est_reg = 'A' AND pe.\"org_id\" = " + orgId + " AND pe.\"ano_esc\" = '" + year + "-01-01'\n"
                    + "ORDER BY nivelId, jornadaId, turnoId, gradoId, seccId;";

            result = sqlQuery(sql);

        } catch (Exception e) {
            throw e;
        }
        return result;
    }

    //Suponiendo que un estudiante solo tiene una matricula activa
    public List<Object[]> getGradosySeccionesDet(long orgId, int year, int nivId, int jorEscId, char turId, int graId) {
        List<Object[]> result = new ArrayList<>();
        try {
            String sql = "SELECT \n"
                    + "gspn.gra_sec_pla_est_id AS gradoSecId, gspn.num_alu_max AS gradoSecNumMax, gspn.num_alu_mat AS gradoSecMat, gspn.pla_niv_id AS planNivelId,\n"
                    + "nv.niv_id AS nivelId, nv.nom AS nivelNom, nv.abr AS nivelAbr,\n"
                    + "je.jor_esc_id AS jornadaId, je.nom AS jornadaNom, je.abr AS jornadaAbr, \n"
                    + "tr.tur_id AS turnoId, tr.nom AS turnoNom,\n"
                    + "gr.gra_id AS gradoId, gr.nom AS gradoNom, gr.abr AS gradoAbr,\n"
                    + "se.sec_id AS seccId, se.nom AS seccNom \n"
                    + "FROM grado_seccion_plan_nivel AS gspn\n"
                    + "INNER JOIN institucional.plan_nivel AS pn ON pn.pla_niv_id = gspn.pla_niv_id\n"
                    + "INNER JOIN institucional.plan_estudios AS pe ON pe.pla_est_id = pn.pla_est_id\n"
                    + "INNER JOIN grado AS gr ON gr.gra_id = gspn.gra_id\n"
                    + "INNER JOIN jornada_escolar AS je ON je.jor_esc_id = pn.jor_esc_id\n"
                    + "INNER JOIN nivel AS nv ON nv.niv_id = je.niv_id\n"
                    + "INNER JOIN institucional.turno AS tr ON tr.tur_id = pn.tur_id\n"
                    + "INNER JOIN institucional.seccion AS se ON se.sec_id = gspn.sec_id\n"
                    + "WHERE gspn.est_reg = 'A' AND pe.\"org_id\" = " + orgId + " AND pe.\"ano_esc\" = '" + year + "-01-01'\n"
                    + "AND je.niv_id = " + nivId + " AND je.jor_esc_id = " + jorEscId + " AND pn.tur_id = '" + turId + "' AND gspn.gra_id = " + graId + "\n"
                    + "ORDER BY nivelId, jornadaId, turnoId, gradoId, seccId;";

            result = sqlQuery(sql);

        } catch (Exception e) {
            throw e;
        }
        return result;
    }

    public void addNumMatriculasXGradoySeccion(int graSecPlaEstId, int numMat) {
        try {
            String sql = "UPDATE grado_seccion_plan_nivel\n"
                    + " SET num_alu_mat= num_alu_mat + " + numMat + "\n"
                    + " WHERE gra_sec_pla_est_id = " + graSecPlaEstId + ";";
            ExecuteSqlQuery(sql);
        } catch (Exception e) {
            throw e;
        }
    }

    public void lessNumMatriculasXGradoySeccion(int graSecPlaEstId, int numMat) {
        try {
            String sql = "UPDATE grado_seccion_plan_nivel\n"
                    + " SET num_alu_mat= num_alu_mat - " + numMat + "\n"
                    + " WHERE gra_sec_pla_est_id = " + graSecPlaEstId + ";";
            ExecuteSqlQuery(sql);
        } catch (Exception e) {
            throw e;
        }
    }

    public void ActivateEstudianteMatriculado(long perId, int orgId) {
        try {
            String sql = "UPDATE pedagogico.estudiante\n"
                    + " SET est_mat= true, org_id= " + orgId + "\n"
                    + " WHERE per_id = " + perId + ";";
            ExecuteSqlQuery(sql);
        } catch (Exception e) {
            throw e;
        }
    }

    public void disableEstudianteMatriculado(int estId, char secId, int graId, int plaNivId, int matId) {
        //desactiva la matricula activa
        //al estudiante le indica q no esta matriculado
        //se libera la plaza
        //desactiva grado_ie_estudiante
        try {
            String sql = "UPDATE pedagogico.matricula\n"
                    + " SET act = false \n"
                    + " WHERE est_id = " + estId + " and act = true; \n"
                    + "\n"
                    + "UPDATE pedagogico.estudiante \n"
                    + " SET est_mat = false \n"
                    + " WHERE per_id = " + estId + "; \n"
                    + "\n"
                    + "UPDATE grado_seccion_plan_nivel\n"
                    + " SET num_alu_mat = num_alu_mat -1\n"
                    + " WHERE sec_id = '" + secId + "' and gra_id = " + graId + " and est_reg = 'A' and pla_niv_id = " + plaNivId + ";\n"
                    + "\n"
                    + "UPDATE pedagogico.grado_ie_estudiante\n"
                    + " SET act = false \n"
                    + " WHERE sec_id = '" + secId + "' and gra_id = " + graId + " and est_reg = 'A' and act = true and mat_id = " + matId + ";";
            ExecuteSqlQuery(sql);
        } catch (Exception e) {
            throw e;
        }
    }

    public int lastPariente(long estId) {
        List<Object[]> result = new ArrayList<>();
        try {
            String sql = "SELECT par_id, per_id\n"
                    + " FROM parientes\n"
                    + " WHERE per_id = " + estId + " AND est_reg != 'E' AND par_viv = true\n"
                    + " ORDER BY fec_mod DESC;";
            result = sqlQuery(sql);
        } catch (Exception e) {
            throw e;
        }

        if (result.size() <= 0) {
            return -1;
        } else {
            return (int) ((BigInteger) result.get(0)[0]).intValue();
        }
    }

    public void disableMatriculas(int orgId) {
        try {
            String sql = "UPDATE pedagogico.matricula\n"
                    + " SET act = false\n"
                    + " WHERE  org_des_id= " + orgId + " and act = true;\n"
                    + " \n"
                    + "UPDATE pedagogico.estudiante\n"
                    + " SET est_mat = false\n"
                    + " WHERE  org_id = " + orgId + " and est_mat = true;\n"
                    + "\n"
                    + "UPDATE pedagogico.grado_ie_estudiante AS ie\n"
                    + " SET act = false\n"
                    + " FROM pedagogico.matricula AS mat\n"
                    + " WHERE  ie.act = true and mat.mat_id = ie.mat_id and mat.org_des_id = " + orgId + ";";
            ExecuteSqlQuery(sql);
        } catch (Exception e) {
            throw e;
        }
    }

    public String getOrganizacionNombre(int orgId) {
        List<Object[]> result = new ArrayList<>();
        try {
            String sql = "SELECT org_id, nom\n"
                    + "  FROM organizacion\n"
                    + "  WHERE org_id = " + orgId + ";";
            result = sqlQuery(sql);
        } catch (Exception e) {
            throw e;
        }

        if (result.size() <= 0) {
            return "I.E. Desconocida";
        } else {
            return (result.get(0)[1]).toString();
        }
    }

    public String getOrganizacionCod(int orgId) {
        List<Object[]> result = new ArrayList<>();
        try {
            String sql = "SELECT org_id, cod\n"
                    + "  FROM organizacion\n"
                    + "  WHERE org_id = " + orgId + ";";
            result = sqlQuery(sql);
        } catch (Exception e) {
            throw e;
        }

        if (result.size() <= 0) {
            return "I.E. Desconocida";
        } else {
            return (result.get(0)[1]).toString();
        }
    }

    public Object[] getTrasladoInternoByEstudianteId(long estId) {
        List<Object[]> result = new ArrayList<>();
        try {
            String sql = "SELECT \n"
                    + "tra.tra_ing_id AS id, tra.tra_ing_tip AS tip, tra.tra_ing_est AS estate, tra.tra_ing_ano_ori AS anoOri, \n"
                    + "tra.tra_ing_ano_des AS anoDes, tra.tra_ing_fec AS fecha, tra.org_ori_id AS orgOri, org_ori.nom AS orgOriNom, \n"
                    + "tra.org_des_id AS orgDes, org_des.nom AS orgDesId,\n"
                    + "tra.gra_id AS graId, tra.pla_niv_id AS plnNiv,tra.obs AS obs, tra.tra_ing_res AS resolucion, \n"
                    + "tra.per_id AS estId, est.nom AS estNom, est.ape_pat AS estApePat, est.ape_mat AS estApeMat, \n"
                    + "tra.est_reg AS estReg"
                    + " FROM pedagogico.traslado_ingreso AS tra \n"
                    + " INNER JOIN organizacion AS org_ori ON org_ori.org_id = tra.org_ori_id \n"
                    + " INNER JOIN organizacion AS org_des ON org_des.org_id = tra.org_des_id \n"
                    + " INNER JOIN pedagogico.persona AS est ON est.per_id = tra.per_id \n"
                    + " WHERE tra.per_id = " + estId + " AND tra.est_reg = 'A' AND tra.tra_ing_est = 'P'; ";
            result = sqlQuery(sql);
        } catch (Exception e) {
            throw e;
        }
        Object[] msj;
        //true = existe un traslado, false = no existe traslado activo
        if (result == null) {
            msj = new Object[2];
            msj[0] = false;
            msj[1] = "No se encuentra traslados registrados";
            return msj;
        } else if (result.size() <= 0) {
            msj = new Object[2];
            msj[0] = false;
            msj[1] = "No se encuentra traslados registrados";
        } else {
            msj = new Object[result.get(0).length + 1];
            msj[0] = true;
            for (int i = 0; i < result.get(0).length; i++) {
                msj[i + 1] = result.get(0)[i];
            }
        }
        return msj;
    }

    public void restarVacantes(int usuMod, int cantidad, int plaNivId, char secId, int graId) {
        try {
            String myDate = DateUtil.getString2Date(new Date());
            String sql = "UPDATE grado_seccion_plan_nivel\n"
                    + " SET  num_alu_mat= num_alu_mat -" + cantidad + ",  fec_mod= '" + myDate + "', usu_mod= " + usuMod + "\n"
                    + " WHERE gra_id = " + graId + " AND sec_id = '" + secId + "' AND pla_niv_id = " + plaNivId + " AND est_reg = 'A';";
            ExecuteSqlQuery(sql);
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean checkUsuario(long perId) {
        List<Object[]> result = new ArrayList<>();
        try {
            String sql = "SELECT usu_id FROM usuario WHERE usu_id = " + perId + ";";
            result = sqlQuery(sql);

        } catch (Exception e) {
            throw e;
        }
        return result.size() > 0;
    }

    public void createUsuario(long hqlUsuId, String hqlDNIEstudiante, int hqlUsuMod, int hqlOrgId) {
        try {
            String hqlFecAct = DateUtil.getTimestampToString(DateUtil.getCurrentDate());
            String sql = "INSERT INTO usuario \n"
                    + "(usu_id, nom, pas, fec_cre, fec_mod, usu_mod, est_reg) \n"
                    + "VALUES(" + hqlUsuId + ", 'EST" + hqlDNIEstudiante + "', '"
                    + hqlDNIEstudiante + "'  , '" + hqlFecAct + "'  , '" + hqlFecAct
                    + "'  , " + hqlUsuMod + " , 'A'); \n"
                    + "INSERT INTO usuario_session \n"
                    + "(fec_cre, fec_mod, usu_mod, est_reg, usu_id, rol_id, org_id, are_id) \n"
                    + "VALUES('" + hqlFecAct + "'  , '" + hqlFecAct + "'  , " + hqlUsuMod
                    + "  , 'A', " + hqlUsuId + "  , 9, " + hqlOrgId + "  , NULL);";
            ExecuteSqlQuery(sql);
        } catch (Exception e) {
            throw e;
        }
    }

    public void setOrganizacionUsuarioSession(long hqlUsuId, int hqlUsuMod, int hqlOrgOriId, int hqlOrgDesId) {
        try {
            String hqlFecAct = DateUtil.getTimestampToString(DateUtil.getCurrentDate());
            String sql = "UPDATE usuario_session\n"
                    + "SET fec_mod='" + hqlFecAct + "', usu_mod= " + hqlUsuMod + " , org_id = " + hqlOrgDesId + " \n"
                    + "WHERE rol_id = 9 AND org_id = " + hqlOrgOriId + " AND usu_id = " + hqlUsuId + ";";
            ExecuteSqlQuery(sql);
        } catch (Exception e) {
            throw e;
        }
    }

}
