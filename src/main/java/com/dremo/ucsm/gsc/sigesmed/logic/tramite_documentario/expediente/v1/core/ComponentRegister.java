/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.RecepcionarExpedienteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.InsertarExpedienteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.ListarExpedienteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.EliminarExpedienteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.ActualizarExpedienteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.ConsultarExpedienteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.DerivarExpedienteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.DerivarExpedienteScTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.DevolverExpedienteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.EntregarExpedienteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.ReporteEstadisticaExpedienteRutaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.ReporteEstadisticaExpedienteEnAreaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.ReporteEstadisticaTipoTramiteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.ReporteEstadisticaExpedienteTrabajadorPorAreaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.EstadisticaExpedienteEnAreaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.EstadisticaExpedienteTrabajadorPorAreaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.EstadisticaResumenTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.EstadisticaTipoTramiteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.FinalExpedienteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.FinalizarExpedienteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.FinalizarExpedienteScTx;

import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.GenerarCargoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.GenerarCargoT2Tx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.ListarHistorialTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.ReporteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.ReporteAreaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.ReporteTipoTramiteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.ReporteTrabajadorTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.ReporteRutaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.ReporteGraficoTablaTrabajadorTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.VerDocumentosDeExpedienteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.VerHistorialAnteriorTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.VerHistorialDeExpedienteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.VerHistorialYDocumentosDeExpedienteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.FirmaDocumentosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.VerificarExistenciaCertificadoTx;
/**
 *
 * @author abel
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_TRAMITE_DOCUMENTARIO);        
        
        //Registrnado el Nombre del componente
        component.setName("expediente");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionPOST("insertarExpediente", InsertarExpedienteTx.class);
        component.addTransactionGET("listarExpediente", ListarExpedienteTx.class);
        component.addTransactionGET("consultarExpediente", ConsultarExpedienteTx.class);
        component.addTransactionGET("listarHistorial", ListarHistorialTx.class);
        component.addTransactionGET("verHistorial", VerHistorialDeExpedienteTx.class);
        component.addTransactionGET("verDocumentos", VerDocumentosDeExpedienteTx.class);
        component.addTransactionGET("verHistorialYDocumentos", VerHistorialYDocumentosDeExpedienteTx.class);
        
        
        component.addTransactionPUT("finalExpediente", FinalExpedienteTx.class);
        component.addTransactionPUT("entregarExpediente", EntregarExpedienteTx.class);
        component.addTransactionPUT("actualizarExpediente", ActualizarExpedienteTx.class);
        component.addTransactionPUT("recepcionarExpediente", RecepcionarExpedienteTx.class);
        component.addTransactionPOST("devolverExpediente", DevolverExpedienteTx.class);
        component.addTransactionPOST("derivarExpediente", DerivarExpedienteTx.class);
        component.addTransactionPOST("derivarExpedienteSc",DerivarExpedienteScTx.class);
        component.addTransactionPOST("finalizarExpediente", FinalizarExpedienteTx.class);
        component.addTransactionPOST("finalizarExpedienteSc", FinalizarExpedienteScTx.class);

        component.addTransactionGET("verHistorialAnterior", VerHistorialAnteriorTx.class);
        
        component.addTransactionGET("estadisticaExpedienteArea", EstadisticaExpedienteEnAreaTx.class);
        component.addTransactionGET("estadisticaExpedienteTrabajador", EstadisticaExpedienteTrabajadorPorAreaTx.class);
        component.addTransactionGET("estadisticaTipoTramite", EstadisticaTipoTramiteTx.class);
        component.addTransactionGET("estadisticaResumen", EstadisticaResumenTx.class);
        component.addTransactionGET("reporteEstadisticaExpedienteRuta", ReporteEstadisticaExpedienteRutaTx.class);
        component.addTransactionGET("reporteEstadisticaExpedienteArea", ReporteEstadisticaExpedienteEnAreaTx.class);
        component.addTransactionGET("reporteEstadisticaTipoTramite",ReporteEstadisticaTipoTramiteTx.class); 
        component.addTransactionGET("reporteEstadisticaExpedienteTrabajadorPorArea",ReporteEstadisticaExpedienteTrabajadorPorAreaTx.class);
        component.addTransactionPOST("reporte", ReporteTx.class);
        component.addTransactionPOST("reporteArea", ReporteAreaTx.class);
        component.addTransactionPOST("reporteTipoTramite", ReporteTipoTramiteTx.class);
        component.addTransactionPOST("reporteTrabajor", ReporteTrabajadorTx.class);
        component.addTransactionPOST("reporteRuta", ReporteRutaTx.class);
        component.addTransactionPOST("reporteGraficoTablaTrabajador", ReporteGraficoTablaTrabajadorTx.class);
        component.addTransactionPOST("generarCargo", GenerarCargoTx.class);        
        component.addTransactionPOST("generarCargoT2", GenerarCargoT2Tx.class);
        component.addTransactionGET("firmaDocumentos", FirmaDocumentosTx.class);
        component.addTransactionGET("verificarExistenciaCertificado", VerificarExistenciaCertificadoTx.class);

        
        return component;
    }
}
