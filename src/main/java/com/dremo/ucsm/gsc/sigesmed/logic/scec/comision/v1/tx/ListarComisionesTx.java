/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.scec.comision.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scec.ComisionDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.ComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.Comision;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.dremo.ucsm.gsc.sigesmed.logic.scec.SCEC;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class ListarComisionesTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ListarComisionesTx.class.getName());
       
    @Override
    public WebResponse execute(WebRequest wr) {
        String idOrgStr = wr.getMetadataValue("org");
        return listarComisiones(Integer.parseInt(idOrgStr));
    }
    private WebResponse listarComisiones(int idOrg){
        try{
            ComisionDao comisionDao = (ComisionDao) FactoryDao.buildDao("scec.ComisionDao");
            List<Comision> comisiones = comisionDao.listarComisionesPorOrganizacion(idOrg);
            JSONArray jsonComisiones = new JSONArray();
            for(Comision comision : comisiones){
                String a = EntityUtil.objectToJSONString(
                        new String[]{"comId","nomCom","desCom","fecConCo"},
                        new String[]{"cod","nom","des","fec"},comision);
                logger.log(Level.INFO," Json: {0}",a);
                JSONObject jsonComision = new JSONObject(a);
                JSONObject jsonOrg = new JSONObject(
                        EntityUtil.objectToJSONString(new String[]{"orgId","nom"}, new String[]{"cod","nom"},comision.getOrganizacion()));
                jsonComision.put("org",jsonOrg);

                jsonComisiones.put(jsonComision);
            }
            return WebResponse.crearWebResponseExito("Se obtuvieron con exito",jsonComisiones);
        }catch(Exception e){
            logger.log(Level.SEVERE,"listarComisiones",e);
            return WebResponse.crearWebResponseError("Error en listar los datos",WebResponse.BAD_RESPONSE);
        }

    }
   
}

