/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.std;

import com.dremo.ucsm.gsc.sigesmed.core.dao.*;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.DocumentoExpediente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.EntidadCantidadModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.Expediente;
import java.util.Date;
import java.util.List;
/**
 *
 * @author abel
 */
public interface ExpedienteDao extends GenericDao<Expediente>{
    
    public String buscarUltimoCodigo();    
    public List<Expediente> buscarPorOrganizacion(int tipoOrganizacionID);
    public List<Expediente> buscarPorCodigo(String codigo);
    public List<Expediente> buscarPorNombreApellido(String nombre, String apellidoP, String apellidoM);
    public List<Expediente> buscarPorFecha(Date desde,Date hasta);
    public List<Expediente> buscarPorOrganizacionYFecha(int organizacionID,Date desde,Date hasta);
    public List<Expediente> buscarPorOrganizacionYFechaYDNI(int organizacionID,Date desde,Date hasta,String dni);    
    public List<Expediente> buscarPorOrganizacionYFechaYRUC(int organizacionID,Date desde,Date hasta,String ruc); 
    public List<Expediente> buscarEnTramitePorOrganizacionYFecha(int organizacionID,Date desde,Date hasta);
    public List<Expediente> buscarFinalizadosPorOrganizacionYFecha(int organizacionID,Date desde,Date hasta);
    public List<Expediente> buscarEntregadosPorOrganizacionYFecha(int organizacionID,Date desde,Date hasta);
    
    public void finalizarExpediente(int expedienteID, int usuarioID);
    public void finalizarExpedientes(List<Integer> expedientesID, int usuarioID);
    
    public void entregarExpediente(int expedienteID, int usuarioID);
    
    public List<DocumentoExpediente> buscarDocumentosPorExpediente(int expedienteID);
    public List<String> buscarDocumentosPorExpedienteC(String expedienteCod);

    public List<EntidadCantidadModel> cantidadTipoExpedientesPorOrganizacionYFecha(int organizacionID, Date desde, Date hasta);
    public List<EntidadCantidadModel> cantidadTipoExpedientesFinalizadosPorOrganizacionYFecha(int organizacionID, Date desde, Date hasta);
    public List<EntidadCantidadModel> cantidadTipoExpedientesEntregadosPorOrganizacionYFecha(int organizacionID, Date desde, Date hasta);
    
    public List<EntidadCantidadModel> cantidadExpedientesPorFecha(Date desde, Date hasta);
}
