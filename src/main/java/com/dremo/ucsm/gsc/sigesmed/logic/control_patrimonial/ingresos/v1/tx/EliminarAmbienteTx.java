/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.AmbientesDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.Ambientes;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author harold
 */
public class EliminarAmbienteTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        
        JSONObject requestData = (JSONObject)wr.getData();
        
        int amb_id = requestData.getInt("amb_id");
        
        AmbientesDAO ambDao = (AmbientesDAO)FactoryDao.buildDao("scp.AmbientesDAO");               
        
        try {
            ambDao.eliminarAmbiente(amb_id);
            return WebResponse.crearWebResponseExito("Ambiente eliminado");
        
        } catch (Exception e) {
            
           return WebResponse.crearWebResponseExito("Ambiente no puede ser eliminado", e);
        }        
         
    }
    
}
