/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.scp;

/**
 *
 * @author Administrador
 */
import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import java.util.Date;
import java.util.List;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.CatalogoBienes;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.ClaseGenerica;

public interface ClaseGenericaDAO extends GenericDao<ClaseGenerica>{
    public List<ClaseGenerica> listarClases(int gru_gen_id);
    public ClaseGenerica mostrarDetalleClase(int cla_gem_id);

}
