/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.sma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.CompromisosGestion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.Items;
import java.util.List;

/**
 *
 * @author Administrador
 */
public interface CompromisosGestionDao extends GenericDao<CompromisosGestion>{
    public String buscarUltimoCodigo();
    public CompromisosGestion obtenerPlantillaCompromisos(int gruId);        
    public List<Items> obtenerItems(int gruId);    
}
