package com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.AdjuntoComentarioTema;

public interface AdjuntoComentarioTemaDao extends  GenericDao<AdjuntoComentarioTema>{
    AdjuntoComentarioTema buscarPorId(int adjCod);
}
