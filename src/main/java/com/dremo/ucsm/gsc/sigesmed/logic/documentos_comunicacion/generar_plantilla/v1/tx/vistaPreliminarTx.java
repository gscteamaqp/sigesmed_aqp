
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_comunicacion.generar_plantilla.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class vistaPreliminarTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
       

        try {

            JSONObject requestData = (JSONObject) wr.getData();
            JSONArray contenidos = (JSONArray) requestData.getJSONArray("contenidoPlantilla");
            Boolean isMarcaAgua=requestData.getBoolean("marcaAgua");
            Boolean verImagen=requestData.getBoolean("verImagen");
            Boolean isSaved=requestData.getBoolean("isSaved");
            FileJsonObject miF=null;
            String path=ServicioREST.PATH_SIGESMED+"/archivos/documentos_comunicacion_imagenes/";
           
            
            
            
             Mitext m = new Mitext(Boolean.FALSE);
            for (int i = 0; i < 3; i++) {

                JSONObject  ctn = (JSONObject) contenidos.get(i);
                String ctn_=ctn.getString("contenido");
                Integer tam=ctn.getInt("size");
                JSONObject  nl=contenidos.getJSONObject(i);
                JSONObject letra=nl.getJSONObject("nombreLetra");
                Integer idLetra=letra.getInt("idLetra");
                JSONObject alineacion=nl.getJSONObject("alineacion");
                Integer idAlineacion=alineacion.getInt("id");
                         
                Boolean isBold=ctn.getBoolean("isBold");
                Boolean isCursiva=ctn.getBoolean("isCursiva");
                Boolean isSubrayado=ctn.getBoolean("isSubrayado");
                String tip = String.valueOf(i + 1);

                m.setStyle(idLetra, tam, isBold, isCursiva,isSubrayado);
                m.agregarParrafoMyEstilo(ctn_,idAlineacion);
                
                

            }
 
            if(verImagen)
            {
                if(isSaved)
                {
                    m.agregarImagen(path+"temporalSDC.png");
                }
               else
                {
                    String nombreArchivo=requestData.getString("nombreArchivo");
                     m.agregarImagen(path+nombreArchivo);
                }
                    
            }
            
            if(isMarcaAgua)
                m.agregarMarcaDeAgua("Documento No Valido");
            
            m.cerrarDocumento();
            JSONArray miArray = new JSONArray();
            JSONObject oResponse = new JSONObject();
            oResponse.put("datareporte", m.encodeToBase64());
            miArray.put(oResponse);

            return WebResponse.crearWebResponseExito("Se genero el reporte de la plantilla correctamente", miArray);

        } catch (Exception ex) {
            Logger.getLogger(vistaPreliminarTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

}
