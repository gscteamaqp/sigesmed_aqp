package com.dremo.ucsm.gsc.sigesmed.core.entity.web;

import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.*;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="pedagogico.evaluacion_escolar" )
public class EvaluacionEscolar  implements java.io.Serializable {

    @Id
    @Column(name="eva_esc_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_evaluacionescolar", sequenceName="pedagogico.evaluacion_escolar_eva_esc_id_seq" )
    @GeneratedValue(generator="secuencia_evaluacionescolar")
    private int evaEscId;
    @Column(name="nom",length=32)
    private String nom;
    @Column(name="des", length=512)
    private String des;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_env", nullable=false, length=29)
    private Date fecEnv; 
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_ini", nullable=false, length=29)
    private Date fecIni;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_fin", nullable=false, length=29)
    private Date fecFin;
    
    @Column(name="sis_cal")
    private boolean sisCal;
    
    @Column(name="aut")
    private boolean aut;
    
    @Column(name="ord_ale")
    private boolean ordAle;
    
    @Column(name="num_int")
    private int numInt;
    
    @Column(name="est_eva")
    private char estado;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_mod", nullable=false, length=29)
    private Date fecMod;    
    @Column(name="usu_mod")
    private int usuMod;
    @Column(name="est_reg")
    private char estReg;
    
    @Column(name="pla_est_id")
    private int plaEstId;    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="pla_est_id",insertable = false,updatable = false)
    private PlanEstudios planEstudios;
    
    @Column(name="gra_id")
    private int graId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="gra_id",updatable = false,insertable = false)
    private Grado grado;
    
    
    @Column(name="eva_maes_id")
    private int eva_maes_id;
    
    @Column(name="sec_id")
    private char secId;
    
    @Column(name="are_cur_id")
    private int areCurId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="are_cur_id",updatable = false,insertable = false)
    private AreaCurricular area;
    
    @OneToMany(mappedBy="evaluacion",cascade=CascadeType.ALL)
    private List<PreguntaEvaluacion> preguntas;
    
    @OneToMany(mappedBy="evaluacion",cascade=CascadeType.ALL)
    private List<MensajeCalificacion> mensajes;

    @OneToMany(mappedBy="evaluacionEscolar" , cascade= CascadeType.ALL)
    private List<BandejaEvaluacion> bandeja_evaluacion;
    
    
    
    public EvaluacionEscolar() {
    }
    public EvaluacionEscolar(int evaEscId) {
        this.evaEscId = evaEscId;
    }
    public EvaluacionEscolar(int evaEscId,String nom, String des,Date fecEnv, Date fecIni,Date fecFin,boolean sisCal,boolean aut, boolean ordAle,int numInt, char estado,int plaEstId,int graId,char secId,int areCurId, Date fecMod, int usuMod, char estReg) {
       this.evaEscId = evaEscId;
       this.nom = nom;
       this.des = des;
       this.fecEnv = fecEnv;
       this.fecIni = fecIni;
       this.fecFin = fecFin;
       this.sisCal = sisCal;
       this.aut = aut;
       this.ordAle = ordAle;
       this.numInt = numInt;
       this.estado = estado;
       this.plaEstId = plaEstId;
       this.graId = graId;
       this.secId = secId;
       this.areCurId = areCurId;
               
       this.fecMod = fecMod;
       this.usuMod = usuMod;
       this.estReg = estReg;
    }

    public void setBandeja_evaluacion(List<BandejaEvaluacion> bandeja_evaluacion) {
        this.bandeja_evaluacion = bandeja_evaluacion;
    }

    public List<BandejaEvaluacion> getBandeja_evaluacion() {
        return bandeja_evaluacion;
    }
    
    
    

    public void setEva_maes_id(int eva_maes_id) {
        this.eva_maes_id = eva_maes_id;
    }

    public int getEva_maes_id() {
        return eva_maes_id;
    }
   
    public int getEvaEscId() {
        return this.evaEscId;
    }    
    public void setEvaEscId(int evaEscId) {
        this.evaEscId = evaEscId;
    }
    
    public String getNom() {
        return this.nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }    
    public String getDes() {
        return this.des;
    }
    public void setDes(String des) {
        this.des = des;
    }
    
    public Date getFecEnv() {
        return this.fecEnv;
    }
    public void setFecEnv(Date fecEnv) {
        this.fecEnv = fecEnv;
    }
    
    public Date getFecIni() {
        return this.fecIni;
    }
    public void setFecIni(Date fecIni) {
        this.fecIni = fecIni;
    }
    
    public Date getFecFin() {
        return this.fecFin;
    }
    public void setFecFin(Date fecFin) {
        this.fecFin = fecFin;
    }
    
    public boolean getSisCal() {
        return this.sisCal;
    }
    public void setSisCal(boolean sisCal) {
        this.sisCal = sisCal;
    }
    
    public boolean getAut() {
        return this.aut;
    }
    public void setAut(boolean aut) {
        this.aut = aut;
    }
    
    public boolean getOrdAle() {
        return this.ordAle;
    }
    public void setOrdAlel(boolean ordAle) {
        this.ordAle = ordAle;
    }
    
    public int getNumInt() {
        return this.numInt;
    }
    public void setNumInt(int numInt) {
        this.numInt = numInt;
    }
    
    public char getEstado(){
        return this.estado;
    }
    public void setEstado(char estado){
        this.estado = estado;
    }
    
    public Date getFecMod() {
        return this.fecMod;
    }
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }
    
    public int getUsuMod() {
        return this.usuMod;
    }
    public void setUsuMod(int usuMod) {
        this.usuMod = usuMod;
    }
    
    public char getEstReg() {
        return this.estReg;
    }
    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }
    
    public int getPlaEstId() {
        return this.plaEstId;
    }    
    public void setPlaEstId(int plaEstId) {
        this.plaEstId = plaEstId;
    }
    
    public PlanEstudios getPlanEstudios() {
        return this.planEstudios;
    }
    public void setPlanEstudios(PlanEstudios planEstudios) {
        this.planEstudios = planEstudios;
    }
    
    public int getGraId() {
        return this.graId;
    }    
    public void setGraId(int graId) {
        this.graId = graId;
    }
    
    public Grado getGrado() {
        return this.grado;
    }
    public void setGrado(Grado grado) {
        this.grado = grado;
    }
    
    
    public int getAreCurId() {
        return this.areCurId;
    }    
    public void setAreCurId(int areCurId) {
        this.areCurId = areCurId;
    }
    
    public AreaCurricular getArea() {
        return this.area;
    }
    public void setArea(AreaCurricular area) {
        this.area = area;
    }
    
    public char getSecId() {
        return this.secId;
    }    
    public void setSecId(char secId) {
        this.secId = secId;
    }
    
    public List<PreguntaEvaluacion> getPreguntas(){
        return this.preguntas;
    }
    public void setPreguntas(List<PreguntaEvaluacion> preguntas){
        this.preguntas = preguntas;
    }
    
    public List<MensajeCalificacion> getMensajes(){
        return this.mensajes;
    }
    public void setMensajes(List<MensajeCalificacion> mensajes){
        this.mensajes = mensajes;
    }
}


