/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.EstudioComplementarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.EstudioComplementario;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author gscadmin
 */
public class EstudioComplementarioDaoHibernate extends GenericDaoHibernate<EstudioComplementario> implements EstudioComplementarioDao{

    @Override
    public List<EstudioComplementario> listarxFichaEscalafonaria(int ficEscId) {
        List<EstudioComplementario> estCom = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT estCom from EstudioComplementario as estCom "
                    + "join fetch estCom.fichaEscalafonaria as fe "
                    + "WHERE fe.ficEscId=" + ficEscId + " AND estCom.estReg='A'";
            Query query = session.createQuery(hql);
            estCom = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los estudios complementarios \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los estudios complementarios \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return estCom;
    }

    @Override
    public EstudioComplementario buscarPorId(Integer estComId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        EstudioComplementario f = (EstudioComplementario)session.get(EstudioComplementario.class, estComId);
        session.close();
        return f;
    }
    
}
