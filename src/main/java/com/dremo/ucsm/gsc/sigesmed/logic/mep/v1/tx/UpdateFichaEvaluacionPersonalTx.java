package com.dremo.ucsm.gsc.sigesmed.logic.mep.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mep.FichaEvaPerslDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mep.FichaEvaluacionPersonalDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.ElementoFicha;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.EscalaValoracionFicha;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.FichaEvaluacionPersonal;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.IndicadorFichaEvaluacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Jerson Herrera on 15/09/2017.
 */
public class UpdateFichaEvaluacionPersonalTx extends MepGeneralTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {

        FichaEvaluacionPersonal ficEvaPer = null;
        JSONObject requestData = null;
//        FichaEvaluacionPersonalDao fepDao = (FichaEvaluacionPersonalDao) FactoryDao.buildDao("mep.FichaEvaluacionPersonalDao");
        FichaEvaluacionPersonalDao fepDao = (FichaEvaluacionPersonalDao) FactoryDao.buildDao("mep.FichaEvaPerslDao");

        try {
            requestData = (JSONObject) wr.getData();
            int fichaId = requestData.getInt("id");
            ficEvaPer = fepDao.getFichaById(fichaId);

        } catch (Exception e) {
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        }

        try {
            String strVigencia = requestData.getString("vig");
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
            Date vigencia = sf.parse(strVigencia);
            ficEvaPer.setDes(requestData.optString("des"));
            ficEvaPer.setVigencia(vigencia);
            fepDao.update(ficEvaPer);
        } catch (ParseException ex) {
            Logger.getLogger(UpdateFichaEvaluacionPersonalTx.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e) {
            System.out.println("No se pudo actualizar la ficha de evaluacion\n" + e);
            return WebResponse.crearWebResponseError("No se pudo actualizar la ficha de evaluacion", e.getMessage());
        }

        return WebResponse.crearWebResponseExito("La ficha de evaluacion se actualizo correctamente");

    }
}
