package com.dremo.ucsm.gsc.sigesmed.core.entity.scec;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by admin on 15/09/16.
 */
@Entity
@Table(name = "asistencia_reunion", schema = "pedagogico")
public class AsistenciaReunionComision implements  java.io.Serializable{
    @Embeddable
    public static class AsistenciaReunionId implements java.io.Serializable{
        @Column(name = "reu_id")
        protected int reuId;

        @Column(name = "per_id")
        protected int perId;

        @Column(name = "com_id")
        protected int comId;

        public AsistenciaReunionId(){}
        public AsistenciaReunionId(int reuId, int perId, int comId){
            this.reuId = reuId;
            this.perId = perId;
            this.comId = comId;
        }
        @Override
        public boolean equals(Object o){
            if(o != null && o instanceof AsistenciaReunionId){
                AsistenciaReunionId ar = (AsistenciaReunionId) o;
                return ar.perId == this.perId
                        && ar.reuId == this.reuId && ar.comId == this.comId;
            }
            return false;
        }
        @Override
        public int hashCode(){
            return reuId * perId * comId + 1;
        }
    }

    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "reuId", column = @Column(name = "reu_id")),
            @AttributeOverride(name = "perId", column = @Column(name = "per_id")),
            @AttributeOverride(name = "comId", column = @Column(name = "com_id"))})
    private AsistenciaReunionId id = new AsistenciaReunionId();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "reu_id", insertable = false, updatable = false)
    private ReunionComision reunionComision;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumns( {
            @JoinColumn(name = "per_id", referencedColumnName = "per_id",insertable = false, updatable = false),
            @JoinColumn(name = "com_id", referencedColumnName = "com_id",insertable = false, updatable = false) })
    private IntegranteComision integranteComision;

    @Column(name = "est")
    private Boolean est;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_asi")
    private Date fecAsi;

    @Column(name = "usu_mod")
    private Integer usuMod;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length=29)
    private Date fecMod;

    @Column(name = "est_reg",length = 1)
    private Character estReg;

    public AsistenciaReunionComision() {
    }

    public AsistenciaReunionComision(ReunionComision reunionComision, IntegranteComision integranteComision) {
        this.reunionComision = reunionComision;
        this.integranteComision = integranteComision;

        this.id.reuId = reunionComision.getReuId();
        this.id.perId = integranteComision.getPersona().getPerId();
        this.id.comId = integranteComision.getComision().getComId();

        this.reunionComision.getAsistenciasReunion().add(this);
        this.integranteComision.getAsistenciaReuniones().add(this);
    }

    public AsistenciaReunionComision(ReunionComision reunionComision, IntegranteComision integranteComision, Boolean est, Date fecAsi) {
        this.reunionComision = reunionComision;
        this.id.reuId = reunionComision.getReuId();
        this.id.perId = integranteComision.getPersona().getPerId();
        this.id.comId = integranteComision.getComision().getComId();
        this.est = est;
        this.fecAsi = fecAsi;

        this.id.reuId = reunionComision.getReuId();
        this.id.perId = integranteComision.getPersona().getPerId();
        this.id.comId = integranteComision.getComision().getComId();

        this.reunionComision.getAsistenciasReunion().add(this);
        this.integranteComision.getAsistenciaReuniones().add(this);
    }

    public AsistenciaReunionComision(int reuId, int perId,int comId, Date fecAsi, boolean est){
        this.fecAsi = fecAsi;
        this.est = est;

        this.id.reuId = reuId;
        this.id.perId = perId;
        this.id.comId = comId;

    }
    public boolean getEst() {
        return est;
    }


    public Date getFecAsi() {
        return fecAsi;
    }

    public void setFecAsi(Date fecAsi) {
        this.fecAsi = fecAsi;
    }

    public int getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(int usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public char getEstReg() {
        return estReg;
    }

    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }

    public ReunionComision getReunionComision() {
        return reunionComision;
    }

    public void setReunionComision(ReunionComision reunionComision) {
        this.reunionComision = reunionComision;
    }

    public IntegranteComision getIntegranteComision() {
        return integranteComision;
    }

    public void setIntegranteComision(IntegranteComision integranteComision) {
        this.integranteComision = integranteComision;
    }

    public AsistenciaReunionId getId() {
        return id;
    }

    public void setId(AsistenciaReunionId id) {
        this.id = id;
    }

    public void setEst(Boolean est) {
        this.est = est;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }
}
