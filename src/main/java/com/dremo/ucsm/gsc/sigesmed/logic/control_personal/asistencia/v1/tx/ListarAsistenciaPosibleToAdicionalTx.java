/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.asistencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.AsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.DiaSemana;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.HorarioCab;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.HorarioDet;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.RegistroAsistencia;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.DateUtil;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author carlos
 */
public class ListarAsistenciaPosibleToAdicionalTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        Integer trabId = 0;
        Date fechaAsistencia;
        Trabajador trabajador;
        AsistenciaDao asistenciaDao = (AsistenciaDao) FactoryDao.buildDao("cpe.AsistenciaDao");
        List<RegistroAsistencia> registros=null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            trabId = requestData.getInt("idTrab");      
            String fecha = requestData.getString("fecha");
            
            trabajador = new Trabajador(trabId);
            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");//dd/MM/yyyy
            fechaAsistencia = sdfDate.parse(fecha);
            Calendar cin = Calendar.getInstance();
            cin.setTime(fechaAsistencia);
            DiaSemana dia=null;
            int tin = cin.get(Calendar.DAY_OF_WEEK);
            switch (tin) {
                case 1:
                    dia=new DiaSemana('D');
                    break;
                case 2:
                    dia=new DiaSemana('L');
                    break;
                case 3:
                    dia=new DiaSemana('M');
                    break;
                case 4:
                    dia=new DiaSemana('W');
                    break;
                case 5:
                    dia=new DiaSemana('J');
                    break;
                case 6:
                    dia=new DiaSemana('V');
                    break;
                case 7:
                    dia=new DiaSemana('S');
                    break;
            }
            
            System.out.println("Dia " + tin);
            registros=asistenciaDao.listarAsistenciasPosiblesToAdicional(trabajador,fechaAsistencia,dia);
            System.out.println("Registros " + registros.size());
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("Error al validar los datos" );
        }
       
        if (registros.size()==0) {
            return WebResponse.crearWebResponseError("No se encuentra Asistencias disponibles en la fecha indicada" );
        }
        
        JSONObject oRes = new JSONObject();
        
        System.out.println("Horario: " + registros.get(0).getHorario().getHorCabDes());
        
        try {

            SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm a");//dd/MM/yyyy           
                        
            List<HorarioDet> horario= registros.get(0).getHorario().getHorarioDetalle();
            
            System.out.println("Horario Detalle: " + horario.size());
            
            JSONArray miArray = new JSONArray();
            for (RegistroAsistencia reg : registros) {
                
                JSONObject oRegistro = new JSONObject();
                
                HorarioDet turnoActual = null;
                
                for (HorarioDet h : horario) {
                    if (DateUtil.compararHora1yHora2(reg.getHoraSalida(),h.getFecSalida())) {
                        turnoActual = h;
                    }
                    
                }
                if(turnoActual!=null)
                {
                    Integer minutosAdicional=DateUtil.diferenciaFechas(turnoActual.getFecSalida(),reg.getHoraSalida() , 4);
                
                    oRegistro.put("id", reg.getRegAsiId());
                    if(reg.getHoraIngreso()==null)
                    {
                        oRegistro.put("ingreso", "");
                    }
                    else
                    {
                        oRegistro.put("ingreso", sdfDate.format(reg.getHoraIngreso()));
                    }


                    oRegistro.put("salida", sdfDate.format(reg.getHoraSalida()));
                    oRegistro.put("adicional", minutosAdicional);
                    oRegistro.put("maximo", minutosAdicional);
                    oRegistro.put("estado", reg.getEstAsi());
                    oRegistro.put("edi", false);

                    miArray.put(oRegistro);
                }
                
            }
            oRes.put("registros", miArray);

               
            

        } catch (Exception e) {
            System.out.println("\n" + e);
            return WebResponse.crearWebResponseError("No se pudo cargar las asistencias", e.getMessage());
        }
        
        return WebResponse.crearWebResponseExito("Consulta correcta", oRes);
        //validar el usuario
    
    }
}
