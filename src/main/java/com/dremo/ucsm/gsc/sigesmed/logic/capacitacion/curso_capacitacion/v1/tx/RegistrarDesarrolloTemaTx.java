package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.AdjuntoTemaCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.DesarrolloTemaCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.EvaluacionDesarrolloDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.ParticipanteCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.AdjuntoTemaCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.DesarrolloTemaCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.EvaluacionDesarrollo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.EvaluacionDesarrolloId;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import com.google.gson.JsonObject;
import java.math.BigInteger;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.DatatypeConverter;
import org.json.JSONArray;
import org.json.JSONObject;

public class RegistrarDesarrolloTemaTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(RegistrarDesarrolloTemaTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();

            DesarrolloTemaCapacitacionDao desarrolloTemaCapacitacionDao = (DesarrolloTemaCapacitacionDao) FactoryDao.buildDao("capacitacion.DesarrolloTemaCapacitacionDao");
            DesarrolloTemaCapacitacion desarrollo = new DesarrolloTemaCapacitacion(data.getInt("tem"), 
                    data.getInt("sed"),
                    data.getJSONObject("cont").getString("nom"),
                    data.getJSONObject("cont").getString("tip").charAt(0));
            
            if (desarrollo.getTip() != 'C' && desarrollo.getTip()!='V')
                desarrollo.setFecEnt(DatatypeConverter.parseDateTime(data.getJSONObject("cont").getString("ent")).getTime());
            
            desarrolloTemaCapacitacionDao.insert(desarrollo);
            
            if (desarrollo.getTip() != 'C' && desarrollo.getTip()!='V') {
                ParticipanteCapacitacionDao participanteCapacitacionDao = (ParticipanteCapacitacionDao) FactoryDao.buildDao("capacitacion.ParticipanteCapacitacionDao");
                EvaluacionDesarrolloDao evaluacionDesarrolloDao = (EvaluacionDesarrolloDao) FactoryDao.buildDao("capacitacion.EvaluacionDesarrolloDao");
                
                for(BigInteger idDoc: participanteCapacitacionDao.listarParticipantes(data.getInt("sed"))) {
                    EvaluacionDesarrollo evaDes = new EvaluacionDesarrollo(new EvaluacionDesarrolloId(desarrollo.getDesTemCapId(), idDoc.intValue()),
                                                                            new Date(), 'A', idDoc.intValue(), 0);
                    
                    evaluacionDesarrolloDao.insert(evaDes);
                }
            }
            
            
                JSONArray adjArray = data.getJSONObject("cont").getJSONArray("adjuntos");
                AdjuntoTemaCapacitacionDao adjuntoTemaCapacitacionDao = (AdjuntoTemaCapacitacionDao) FactoryDao.buildDao("capacitacion.AdjuntoTemaCapacitacionDao");

                if (adjArray.length() > 0) {
                    
                    for (int i = 0; i < adjArray.length(); i++) {
                        AdjuntoTemaCapacitacion adjunto = new AdjuntoTemaCapacitacion("Nombre_archivo",data.getInt("usuMod"),desarrollo);
                        adjunto.setTipo('A');
                        adjuntoTemaCapacitacionDao.insert(adjunto);

                        FileJsonObject file = new FileJsonObject(adjArray.getJSONObject(i).optJSONObject("arc"),
                                "tem_" + data.getInt("tem") + "_des_" + desarrollo.getDesTemCapId() + "_adj_" + adjunto.getAdjTemCapId() + "_" + adjArray.getJSONObject(i).getString("nom"));
                        BuildFile.buildFromBase64(HelpTraining.attachments_Development, file.getName(), file.getData());
                        adjunto.setNom(file.getName());
                        adjuntoTemaCapacitacionDao.update(adjunto);
                    }
                }
            
                /*Para el Caso de Suba de Links*/
                JSONArray linkArray = data.getJSONObject("cont").getJSONArray("links");
                if(linkArray.length() >0){
                    for (int i = 0; i < linkArray.length(); i++) {
                        AdjuntoTemaCapacitacion adjunto = new AdjuntoTemaCapacitacion("Nombre_archivo",data.getInt("usuMod"),desarrollo);
                        adjunto.setTipo('L');
                        String nombre = linkArray.getJSONObject(i).getString("nom");
                        adjunto.setNom(nombre);
                        adjuntoTemaCapacitacionDao.insert(adjunto);
                    }
                }
                
            

            return WebResponse.crearWebResponseExito("El desarrollo del tema del curso de capacitación fue creado correctamente", WebResponse.OK_RESPONSE);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "registrarDesarrollo", e);
            return WebResponse.crearWebResponseError("No se pudo registrar rl desarrollo del tema del curso de capacitación", WebResponse.BAD_RESPONSE);
        }
    }
}
