/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.scp;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author harold
 */

@Entity
@Table(name="salidas", schema="administrativo")
public class Salidas {
    
    @Id
    @Column(name="salidas_id" , unique=true, nullable=false)
    @SequenceGenerator(name="secuencia_inv_tra",sequenceName="administrativo.salidas_salidas_id_seq")
    @GeneratedValue(generator="secuencia_inv_tra")
    private int salidas_id;
    
    @Column(name="cau_sal_id")
    private int cau_sal_id;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cau_sal_id" , insertable=false , updatable=false)
    private CausalSalida causal_salida;
    
    @Column(name="org_id_ori")
    private int org_id_ori;
    
    @Column(name="org_id_des")
    private int org_id_des;
    
    @Column(name="usu_mod")
    private int usu_mod;    
    
    @Column(name="mov_ing_id")
    private int mov_ing_id;
    
    @Column(name="fec_reg")
    private Date fec_reg;
    
    @Column(name="est_reg")
    private char estReg;
    
    @OneToMany(fetch=FetchType.LAZY)
    @JoinColumn(name="salidas_id" , insertable=false , updatable=false)
    private List<SalidasDetalle> salDet;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="org_id_des" , insertable=false , updatable=false)
    private Organizacion orgDes;

    public Salidas() {
    }   

    public Salidas(int salidas_id, int cau_sal_id, int org_id_ori, int org_id_des, int usu_mod, int mov_ing_id, Date fec_reg, char estReg) {
        this.salidas_id = salidas_id;
        this.cau_sal_id = cau_sal_id;
        this.org_id_ori = org_id_ori;
        this.org_id_des = org_id_des;
        this.usu_mod = usu_mod;
        this.mov_ing_id = mov_ing_id;
        this.fec_reg = fec_reg;
        this.estReg = estReg;
    }              
    
    public void setSalidas_id(int salidas_id) {
        this.salidas_id = salidas_id;
    }

    public void setCausal_salida(CausalSalida causal_salida) {
        this.causal_salida = causal_salida;
    }

    public void setOrg_id_ori(int org_id_ori) {
        this.org_id_ori = org_id_ori;
    }

    public void setOrg_id_des(int org_id_des) {
        this.org_id_des = org_id_des;
    }

    public void setUsu_mod(int usu_mod) {
        this.usu_mod = usu_mod;
    }

    public void setMov_ing_id(int mov_ing_id) {
        this.mov_ing_id = mov_ing_id;
    }

    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }

    public void setCau_sal_id(int cau_sal_id) {
        this.cau_sal_id = cau_sal_id;
    }

    public void setFec_reg(Date fec_reg) {
        this.fec_reg = fec_reg;
    }

    public void setSalDet(List<SalidasDetalle> salDet) {
        this.salDet = salDet;
    }   

    public void setOrgDes(Organizacion orgDes) {
        this.orgDes = orgDes;
    }       
    
    public int getSalidas_id() {
        return salidas_id;
    }

    public CausalSalida getCausal_salida() {
        return causal_salida;
    }

    public int getOrg_id_ori() {
        return org_id_ori;
    }

    public int getOrg_id_des() {
        return org_id_des;
    }

    public int getUsu_mod() {
        return usu_mod;
    }

    public int getMov_ing_id() {
        return mov_ing_id;
    }

    public char getEstReg() {
        return estReg;
    }    

    public int getCau_sal_id() {
        return cau_sal_id;
    }

    public Date getFec_reg() {
        return fec_reg;
    }
       
    public List<SalidasDetalle> getSalDet() {
        return salDet;
    }   

    public Organizacion getOrgDes() {
        return orgDes;
    }   
    
}
