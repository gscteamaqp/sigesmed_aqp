/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.rdg;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.ItemFileDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.ItemFile;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.ItemFileDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.TipoEspecializado;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author ucsm
 */
public class ItemFileDaoHibernate extends GenericDaoHibernate<ItemFile> implements ItemFileDao {

    @Override
    public ItemFile buscarPorID(int clave) {
        ItemFile itefil = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Query query = session.getNamedQuery("ItemFile.findByIteIde").setInteger("iteIde", clave);
            query.setMaxResults(1);
            itefil = ((ItemFile)query.uniqueResult());
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar el itemFile \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar por codigo el itemFile \\n "+ e.getMessage());
        }
        finally{
            session.close();
        }
        return itefil;
    }
    
    
    //Este metodo debe buscar todos los itemfiles ya que usamos los codigos incluso de los no activos
    @Override
    public String buscarUltimoCodigo() {
        String codigo = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar ultimo codigo de tramite
            String hql = "SELECT ifile.iteAltIde FROM ItemFile ifile ORDER BY ifile.iteIde DESC ";
            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            codigo = ((String)query.uniqueResult());
            
            //solo cuando no hay ningun registro aun
            if(codigo==null)
                codigo = "0000";
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return codigo;
    }

    //Este metodo debe recuperar solo los que esten activos ya que es usado para mostrar el contenido del directorio
    @Override
    public List<ItemFile> buscarHijosPorIdPadre(ItemFile padre, int usuCod) {
        
        List<ItemFile> documentos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar requisitos
            //String hql = "SELECT MAX(itemFil.iteVer),itemFil.iteNom FROM ItemFile itemFil WHERE itemFil.itePadIde.iteIde=:p1 AND itemFil.estReg='A'";
            String hql="SELECT itemFil FROM ItemFile itemFil WHERE itemFil.itePadIde.iteIde=:p1 AND itemFil.estReg='A' AND itemFil.iteMod='N' AND itemFil.iteCodCat!='D' AND itemFil.iteUsuIde=:p2";
            Query query = session.createQuery(hql);
            query.setParameter("p1", padre.getIteIde());
            query.setParameter("p2", usuCod);
            documentos = query.list();        
        }catch(Exception e){
            System.out.println("No se pudo Listar los documento \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los documentos \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return documentos;
    }
    
    @Override
    public List<ItemFile> buscarAllHijosPorIdPadre(ItemFile padre) {
        List<ItemFile> documentos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar requisitos
            String hql = "SELECT itemFil FROM ItemFile itemFil WHERE itemFil.itePadIde=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", padre);
            documentos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los documento \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los documentos \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return documentos;
    }

    @Override
    public int eliminarHijosByPadre(ItemFile padre) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        int result = 0;
        try{
            //eliminando los hijos o cambiando su estado a "E" de eliminado
            String hql = "UPDATE ItemFile iteFil SET iteFil.estReg='E' WHERE iteFil.itePadIde  =:p1";            
            Query query = session.createQuery(hql);
            query.setParameter("p1", padre);
            result = query.executeUpdate();    
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo eliminar los hijos de : " + padre.getIteNom() + "\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
        return result;
    }

    @Override
    public List<ItemFile> buscarEliminados() {
        List<ItemFile> documentos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar requisitos
            String hql = "SELECT itemFil FROM ItemFile itemFil WHERE itemFil.estReg='E'";
            Query query = session.createQuery(hql);
            documentos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los documento \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los documentos \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return documentos;
    }

    @Override
    public int eliminarTotalHijosByPadre(ItemFile padre) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        int result = 0;
        try{
            //eliminando los hijos o cambiando su estado a "E" de eliminado
            String hql = "DELETE FROM ItemFile iteFil WHERE iteFil.itePadIde  =:p1";            
            Query query = session.createQuery(hql);
            query.setParameter("p1", padre);
            result = query.executeUpdate();    
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo eliminar Totalmente los hijos de : " + padre.getIteNom() + "\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
        return result;
    }

    @Override
    public List<ItemFile> listPorTipoEsp(TipoEspecializado tes) {
        List<ItemFile> documentos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar requisitos
            String hql = "SELECT itemFil FROM ItemFile itemFil WHERE itemFil.iteTifIde.tifTesIde=:p1 and itemFil.estReg!='E'";
            Query query = session.createQuery(hql);
            query.setParameter("p1", tes);
            documentos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los documento \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los documentos \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return documentos;
    }

    @Override
    public boolean buscarNombreRepetido(ItemFile item) {
        Session session=HibernateUtil.getSessionFactory().openSession();
        boolean flag=false;
        try{
            String hql="SELECT count(*) FROM ItemFile item WHERE item.itePadIde =:p1 and item.iteNom =:p2 and item.estReg='A'";
            Query q=session.createQuery(hql);
            q.setParameter("p1", item.getItePadIde());

            q.setParameter("p2", item.getIteNom());

            Long temp=(Long)q.uniqueResult();
            

            if(temp>0)
                flag=true;
            else
                flag=false;
            
        }catch(Exception e){
            System.out.println("Nombre repetido, no se pudo guardar  \\n" + e.getMessage() );
            throw new UnsupportedOperationException( "Nombre repetido, no se pudo guardar \\n"+e.getMessage());
        }finally{
            session.close();
        }
        return flag;        
    }
    @Override
    public Integer buscarIdPadre(ItemFile item) {
        Session session=HibernateUtil.getSessionFactory().openSession();
        Integer idAbuelo=0;
        try{
            String hql="SELECT item.itePadIde FROM ItemFile item WHERE item.iteIde=:p1 and item.estReg!='E'"; 
            Query q=session.createQuery(hql);
            q.setParameter("p1",item.getIteIde());
            idAbuelo=((ItemFile) q.uniqueResult()).getIteIde(); 
            
        }catch(Exception e){
            System.out.println(e.getMessage());
            throw new UnsupportedOperationException("No se pudo obtener la carpeta padre");
        }finally{
            session.close();
            return idAbuelo;
        }
        
    }
     @Override
    public int obtenerNumDocPorCarpetaYTipo(ItemFile carpeta, ItemFile item) {
        Session session=HibernateUtil.getSessionFactory().openSession();
        int  ultimaVersion=0;
        try{
            String hql="SELECT item FROM ItemFile item WHERE item.tesIde.tesIde=:p1 "
                    + "and item.itePadIde=:p2 and item.iteOrgIde=:p3 and item.estReg='A' and item.iteMod='N'"
                    + "ORDER BY item.iteVer DESC";
            Query q=session.createQuery(hql);
            q.setParameter("p1",item.getTesIde().getTesIde());
            q.setParameter("p2", carpeta);
            q.setParameter("p3",item.getIteOrgIde());
      
            q.setMaxResults(1);
            //ultimaVersion=(Short)q.list().get(0);
            ultimaVersion=((ItemFile)q.uniqueResult()).getIteVer();
    
        }catch(Exception e){
            System.out.println("No se pudo obtener la última versión" + e.getMessage());
            throw new UnsupportedOperationException("No se pudo obtener la última versión"); 
        }finally{
            session.close();
            return ultimaVersion;//Devuelve la última versión, o en caso de que no exista devuelve 0
        }         
    }
    @Override
    public ItemFile obtenerUltimaVersion(ItemFile item, ItemFile padre) {
        Session session=HibernateUtil.getSessionFactory().openSession();
        ItemFile  ultimaVersion=null;
        try{
            String hql="SELECT item FROM ItemFile item WHERE item.tesIde.tesIde=:p1 "
                    + "and item.itePadIde=:p2 and item.iteOrgIde=:p3 and item.estReg='A' and item.iteMod='N'"
                    + "ORDER BY item.iteVer DESC";
            Query q=session.createQuery(hql);
            q.setParameter("p1",item.getTesIde().getTesIde());
            q.setParameter("p2", padre);
            q.setParameter("p3",item.getIteOrgIde());
      
            q.setMaxResults(1);
            //ultimaVersion=(Short)q.list().get(0);
            ultimaVersion=(ItemFile)q.uniqueResult();
    
        }catch(Exception e){
            System.out.println("No se pudo obtener la última versión" + e.getMessage());
            throw new UnsupportedOperationException("No se pudo obtener la última versión"); 
        }finally{
            session.close();
            return ultimaVersion;//Devuelve la última versión, o en caso de que no exista devuelve 0
        }        
    }

    @Override
    public boolean desactivarVersionesAnteriores(ItemFile item,ItemFile padre) {
        Session session=HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{         
            String hql="UPDATE ItemFile item SET item.iteMod='S', item.estReg='E' WHERE item.tesIde.tesIde=:p1 AND item.iteVer=:p2 "
                    + "AND item.itePadIde=:p3 and item.iteOrgIde=:p4";
            Query q=session.createQuery(hql);
            q.setParameter("p1",item.getTesIde().getTesIde());
            q.setParameter("p2",((Integer)(item.getIteVer().intValue()-1)).shortValue());
            q.setParameter("p3",padre);
            q.setParameter("p4",item.getIteOrgIde());
            
            q.executeUpdate();
            miTx.commit();
            return true;
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo desactivar las versiones anteriores "+e.getMessage());
            throw new UnsupportedOperationException("No se pudo desactivar las versiones anteriores.");
        }finally{
            session.close();
        } 
    }

    @Override
    public List<ItemFile> buscarCarpetas() {
            Session session=HibernateUtil.getSessionFactory().openSession();
        List<ItemFile> dirs=new ArrayList<ItemFile>();
        try{
            String hql="SELECT item FROM ItemFile item WHERE item.iteCodCat='D' and item.estReg='A'";
            Query q=session.createQuery(hql);
            dirs=q.list();
        }catch(Exception e){
            System.out.println("No se pudo listar las carpetas para el arbol jerarquico "+e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar las carpetas para el arbol jerarquico"); //To change body of generated methods, choose Tools | Templates.
        }finally{
            session.close();
        }
        return dirs;
        
    }

    @Override
    public boolean esPropio(int usuID, ItemFile item) {
        Session session=HibernateUtil.getSessionFactory().openSession();
        boolean flag=false;
        try{
           String hql="SELECT count(*) FROM ItemFile item WHERE item.iteIde =:p1 and item.iteUsuIde =:p2";
            Query q=session.createQuery(hql);
            q.setParameter("p1", item.getIteIde());
            q.setParameter("p2", usuID);
            Long temp=(Long)q.uniqueResult();            
            if(temp>0)
                flag=true;//Es propio
            else
                flag=false;
        }catch(Exception e){
            System.out.println("No se pudo determinar si el archivo es propio o compartido "+e.getMessage());
            throw new UnsupportedOperationException("No se pudo determinar si el archivo es propio o compartido"); //To change body of generated methods, choose Tools | Templates.
        }finally{
            session.close();
        }        
        return flag;
    }    

    @Override
    public ItemFile buscarCompartido(ItemFileDetalle det) {
        Session session=HibernateUtil.getSessionFactory().openSession();
        ItemFile item=null;
        try{
            String hql="SELECT item FROM ItemFile item WHERE item.iteIde=:p1";
            Query q=session.createQuery(hql);
            q.setParameter("p1", det.getIfdIteIde().getIteIde());
            item=(ItemFile)q.uniqueResult();
        }catch(Exception e){
            System.out.println("No se pudo obtener los datos del documento compartido "+e.getMessage());
            throw new UnsupportedOperationException("No se pudo obtener los datos del documento compartido");
        }finally{
            session.close();
        }
        return item;
    }

    @Override
    public void eliminarPlazoDeCarpeta(ItemFile item) {
        Session session=HibernateUtil.getSessionFactory().openSession();
        try{
            String hql="UPDATE ItemFile item SET item.itePlaIde.plzIde=null WHERE item.iteIde=:p1 AND item.estReg='A'";
            Query q=session.createQuery(hql);
            q.setParameter("p1",item.getIteIde());
            q.executeUpdate();
        }catch(Exception e){
            System.out.println("No se pudo eliminar el plazo de la carpeta "+e.getMessage());
            throw new UnsupportedOperationException("No se pudo eliminar el plazo de la carpeta"); 
        }finally{
            session.close();
        }
    }

    @Override
    public void desactivarDocPorTipoDocumento(TipoEspecializado tipo) {
        Session session=HibernateUtil.getSessionFactory().openSession();
         Transaction miTx = session.beginTransaction();
        try{
            String hql="UPDATE ItemFile item SET item.estReg='E' WHERE item.tesIde=:p1";
            Query q=session.createQuery(hql);
            q.setParameter("p1",tipo);
            q.executeUpdate();
            miTx.commit();
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se puede actualizar el tipo de documento en los archivos "+e.getMessage());
            throw new UnsupportedOperationException("No se puede actualizar el tipo de documento en los archivos "); 
        }finally{
            session.close();
        }
        
    }

    @Override
    public List<ItemFile> buscarHistorialPorDocumento(ItemFile item) {
        Session session=HibernateUtil.getSessionFactory().openSession();
        List<ItemFile> historial=null;
        try{
            String hql="SELECT item FROM ItemFile item WHERE item.tesIde.tesIde=:p1 AND item.itePadIde=:p2 and item.iteOrgIde=:p3";
            Query q=session.createQuery(hql);
            q.setParameter("p1",item.getTesIde().getTesIde());
            q.setParameter("p2",item.getItePadIde());
            q.setParameter("p3",item.getIteOrgIde());
            historial=q.list();
            
        }catch(Exception e){
            System.out.println("No se pudo listar el historial "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar el historial "+ e.getMessage()); 
        }finally{
            session.close();
        }
        return historial;
    }

    @Override
    public List<ItemFile> buscarHijosPorIdPadreUgel(ItemFile padre, int orgID, int usuCod) {
        List<ItemFile> documentos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql="SELECT itemFil FROM ItemFile itemFil INNER JOIN FETCH itemFil.iteOrgIde org WHERE "
                    + "(org.organizacionPadre.orgId=:p2 OR itemFil.iteUsuIde=:p3)  AND itemFil.itePadIde.iteIde=:p1 AND itemFil.estReg='A' "
                    + "AND itemFil.iteMod='N' AND itemFil.iteCodCat!='D'";
            Query query = session.createQuery(hql);
            query.setParameter("p1", padre.getIteIde());
            query.setParameter("p2", orgID);
            query.setParameter("p3", usuCod);
            documentos = query.list();        
        }catch(Exception e){
            System.out.println("No se pudo Listar los documentos \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los documentos \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return documentos;
    }

    @Override
    public List<String> buscarTiposDocumentos(int usuCod, int orgID) {
        Session session=HibernateUtil.getSessionFactory().openSession();
        List<String> tipos=null;
        try{       
            String hql="SELECT DISTINCT tip.tesAli FROM ItemFile item INNER JOIN item.tesIde tip "
                    + "WHERE item.iteOrgIde.orgId=:p1 and item.iteUsuIde=:p2 and item.estReg='A' and item.iteMod='N'";
            Query q=session.createQuery(hql);
            q.setParameter("p1", orgID);
            q.setParameter("p2", usuCod);
            
            
            tipos=q.list();
        }catch(Exception e){
            System.out.println("No se pudo obtener los tipos de documentos "+e.getMessage());
            throw new UnsupportedOperationException("No se pudo obtener los tipos de documentos "+e.getMessage());
        }finally{
                session.close();
        }
        return tipos;
    }

    @Override
    public int buscarValorPorTipo(TipoEspecializado tipo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ItemFile> obtenerTodasCarpetas() {
        Session session=HibernateUtil.getSessionFactory().openSession();
        List<ItemFile> carpetas=null;
        try{       
            String hql="SELECT item FROM ItemFile WHERE item.iteCodCat='D'";
            Query q=session.createQuery(hql);            
            carpetas=q.list();
        }catch(Exception e){
            System.out.println("No se pudo obtener todas las carpetas "+e.getMessage());
            throw new UnsupportedOperationException("No se pudo obtener todas las carpetas "+e.getMessage());
        }finally{
                session.close();
        }
        return carpetas;
    }

  
}
