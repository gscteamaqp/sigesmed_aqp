/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_personales.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FichaEscalafonariaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class BuscarFichaEscalafonariaPorDniTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(BuscarFichaEscalafonariaPorDniTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {

        JSONObject requestData = (JSONObject)wr.getData();
        
        String perDNI = requestData.getString("perDni");
        Integer opcion = requestData.getInt("opcion");
                
        FichaEscalafonaria fichaEscalafonaria = new FichaEscalafonaria();
        FichaEscalafonariaDao fichaEscalafonariaDao = (FichaEscalafonariaDao)FactoryDao.buildDao("se.FichaEscalafonariaDao");
        
        try{
            fichaEscalafonaria = fichaEscalafonariaDao.buscarPorDNI(perDNI);

        }catch(Exception e){
            System.out.println("No se encontro la ficha escalafonaria \n"+e);
            logger.log(Level.SEVERE,"Buscar ficha escalafonaria",e);
            return WebResponse.crearWebResponseError("No se encontro la ficha escalafonaria ", e.getMessage() );
        }
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONObject data = new JSONObject();        
        if (opcion == 1){
             
            JSONObject persona = new JSONObject();
            JSONObject trabajador = new JSONObject();
            JSONObject ficha = new JSONObject();
            
            persona.put("perId", fichaEscalafonaria.getTrabajador().getPersona().getPerId());
            persona.put("apePat", fichaEscalafonaria.getTrabajador().getPersona().getApePat());
            persona.put("apeMat", fichaEscalafonaria.getTrabajador().getPersona().getApeMat());
            persona.put("nom", fichaEscalafonaria.getTrabajador().getPersona().getNom());
            persona.put("dni", fichaEscalafonaria.getTrabajador().getPersona().getDni());
            persona.put("fecNac", fichaEscalafonaria.getTrabajador().getPersona().getFecNac());
            persona.put("num1", fichaEscalafonaria.getTrabajador().getPersona().getNum1());
            persona.put("num2", fichaEscalafonaria.getTrabajador().getPersona().getNum2());
            persona.put("fij", fichaEscalafonaria.getTrabajador().getPersona().getFij());
            persona.put("email", fichaEscalafonaria.getTrabajador().getPersona().getEmail());
            persona.put("sex", fichaEscalafonaria.getTrabajador().getPersona().getSex());
            persona.put("estCiv", fichaEscalafonaria.getTrabajador().getPersona().getEstCiv());
            persona.put("depNac", fichaEscalafonaria.getTrabajador().getPersona().getDepNac());
            persona.put("proNac", fichaEscalafonaria.getTrabajador().getPersona().getProNac());
            persona.put("disNac", fichaEscalafonaria.getTrabajador().getPersona().getDisNac());
            
            trabajador.put("traId", fichaEscalafonaria.getTrabajador().getTraId());
            trabajador.put("traCon", fichaEscalafonaria.getTrabajador().getTraCon());
            
            ficha.put("ficEscId", fichaEscalafonaria.getFicEscId());
            ficha.put("autEss", fichaEscalafonaria.getAutEss());
            ficha.put("sisPen", fichaEscalafonaria.getSisPen());
            ficha.put("nomAfp", fichaEscalafonaria.getNomAfp());
            ficha.put("codCuspp", fichaEscalafonaria.getCodCuspp());
            ficha.put("fecIngAfp", fichaEscalafonaria.getFecIngAfp());
            ficha.put("perDis", fichaEscalafonaria.getPerDis());
            ficha.put("regCon", fichaEscalafonaria.getRegCon());
            ficha.put("gruOcu", fichaEscalafonaria.getGruOcu());

            data.put("persona", persona);
            data.put("trabajador", trabajador);
            data.put("ficha", ficha);
 
        }else{
            data.put("ficEscId", fichaEscalafonaria.getFicEscId());
        }
        
        return WebResponse.crearWebResponseExito("Se listo correctamente", data);
    }
    
}
