/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.area.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.AreaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;

/**
 *
 * @author abel
 */
public class ActualizarResponsableAreaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int areaID =0;
        Integer responsableID = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            areaID = requestData.getInt("areaID");
            responsableID = requestData.optInt("responsableID");
            if(responsableID == 0)
                responsableID = null;
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo actualizar al responsable del area, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        AreaDao areaDao = (AreaDao)FactoryDao.buildDao("AreaDao");
        try{
            areaDao.cambiarResponsable(responsableID,new Date(),wr.getIdUsuario(),areaID);
        
        }catch(Exception e){
            System.out.println("No se pudo actualizar al responsable del area\n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar al responsable del area", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El responsable de area se actualizo correctamente");
        //Fin
    }
    
}
