/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.mep.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mep.FichaEvaluacionPersonalDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mep.TrabajadorCargoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.TrabajadorCargo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mep.FichaEvaPerslDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.FichaEvaluacionPersonal;
import java.text.ParseException;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;
/**
 *
 * @author geank
 */
public class RegistrarFichaEvaluacionTx implements ITransaction{
    private final static Logger logger = Logger.getLogger(RegistrarFichaEvaluacionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject dataRequest = (JSONObject)wr.getData();
        String nomFicha = dataRequest.getString("nombre");  
        String descripcion = dataRequest.getString("descrip");
        String strVigencia = dataRequest.getString("vigencia");
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        Date vigencia = null;
        try {
            vigencia = sf.parse(strVigencia);
        } catch (ParseException ex) {
            Logger.getLogger(RegistrarFichaEvaluacionTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        int idRol = dataRequest.getInt("tipo");
//        FichaEvaluacionPersonal fep = new FichaEvaluacionPersonal(nomFicha, new Date(), descripcion);
        FichaEvaluacionPersonal fep = new FichaEvaluacionPersonal(nomFicha, new Date(), descripcion, vigencia);
        return registrarFicha(fep,idRol);
    }
    private WebResponse registrarFicha(FichaEvaluacionPersonal fep, int idRol){
        try{
            TrabajadorCargoDao cargoDao = (TrabajadorCargoDao) FactoryDao.buildDao("mep.TrabajadorCargoDao");
            FichaEvaluacionPersonalDao fichaDao = (FichaEvaluacionPersonalDao) FactoryDao.buildDao("mep.FichaEvaluacionPersonalDao");
            List<FichaEvaluacionPersonal> fichas = fichaDao.buscarTodos(FichaEvaluacionPersonal.class);
            TrabajadorCargo cargo = cargoDao.buscarPorId(idRol);
            for(FichaEvaluacionPersonal ficha : fichas ){
                if(ficha.getTipTra().getCarNom().equals(cargo.getCarNom())){
                    return WebResponse.crearWebResponseError("La ficha ya existe",WebResponse.BAD_RESPONSE);
                }
            }
            fep.setTipTra(cargo);
            fichaDao.insertFichaEvaluacion(fep);
            JSONObject jsonFep = new JSONObject(EntityUtil.objectToJSONString(
                    new String[]{"ficEvaPerId","nom","des","fecCre","vigencia"},
                    new String[]{"id","nom","des","fcre","vig"},
                    fep
            ));
            jsonFep.put("tip",cargo.getCarNom()).put("tipcod",cargo.getTraCarId());
            return WebResponse.crearWebResponseError("Se creo la ficha de evaluacion",WebResponse.OK_RESPONSE).setData(jsonFep);
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarFicha",e);
            return WebResponse.crearWebResponseError("error al crear la ficha de evaluacion",WebResponse.BAD_RESPONSE);
        }
    }
    private WebResponse registrarFicha(FichaEvaluacionPersonal fep){
        WebResponse dataResponse = new WebResponse();
        dataResponse.setScope("web");
        dataResponse.setResponseSta(true);
        try{
            FichaEvaPerslDaoHibernate fichaEva = new FichaEvaPerslDaoHibernate();
            fichaEva.insertFichaEvaluacion(fep);
            if(fep != null){
                if(fep.getFicEvaPerId() != -1){
                    dataResponse.setResponse(MEP.SUCC_RESPONSE_COD);
                    dataResponse.setResponseMsg(MEP.SUCC_RESPONSE_MESS_INS);
                    dataResponse.setData(new JSONObject(fep.toString()));
                }else{
                    dataResponse.setResponse(MEP.ERR_RESPONSE_COD);
                    dataResponse.setResponseMsg("Ya existe una ficha con el rol indicado");
                }
            }else{
                dataResponse.setResponse(MEP.ERR_RESPONSE_COD);
                dataResponse.setResponseMsg(MEP.ERR_RESPONSE_MESS_INS);
            }
            
        }catch(Exception e){
            logger.log(Level.SEVERE,logger.getName()+":insertFichaEvaluacion",e);
            dataResponse.setResponse(MEP.ERR_RESPONSE_COD);
            dataResponse.setResponseMsg(MEP.ERR_RESPONSE_MESS_INS);
        }
        return dataResponse;
    }
}
