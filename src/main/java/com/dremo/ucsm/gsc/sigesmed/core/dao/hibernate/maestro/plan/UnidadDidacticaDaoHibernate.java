package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.maestro.plan;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.UnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.PeriodosPlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.*;
import java.util.ArrayList;
import org.hibernate.*;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;

import java.util.List;

/**
 * Created by Administrador on 26/10/2016.
 */
public class UnidadDidacticaDaoHibernate extends GenericDaoHibernate<UnidadDidactica> implements UnidadDidacticaDao{
    @Override
    public UnidadDidactica buscarUnidadDidactica(int idUnidad, String tip) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            UnidadDidactica unidad = null;
            switch (tip.toUpperCase()){
                case "M" :unidad = (ModuloAprendizaje) session.get(UnidadDidactica.class,idUnidad); break;
                case "P" :unidad = (ProyectoAprendizaje) session.get(UnidadDidactica.class,idUnidad); break;
                case "U" :unidad = (UnidadAprendizaje)session.get(UnidadDidactica.class,idUnidad); break;
            }
            return unidad;
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public UnidadDidactica buscarUnidadDidactica(int idUnidad) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria criteria = session.createCriteria(UnidadDidactica.class)
                    .add(Restrictions.eq("uniDidId",idUnidad));

            return (UnidadDidactica)criteria.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
    
    

    @Override
    public void eliminarUnidadDidactica(int idUnidad) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            String hql="UPDATE UnidadDidactica SET estReg = 'E' WHERE uniDidId =:idUnidad";
            Query query = session.createQuery(hql);
            query.setInteger("idUnidad",idUnidad);
            query.executeUpdate();
            tx.commit();
        }catch (Exception e){
            tx.rollback();
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public void editarUnidadDidactica(UnidadDidactica newUnidad) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            String hql="UPDATE UnidadDidactica SET tit=:newTit,sitSig=:newSit,fecIni=:newfecIni,fecFin=:newFecFin WHERE uniDidId =:idUnidad";
            Query query = session.createQuery(hql);

            query.setInteger("idUnidad",newUnidad.getUniDidId());
            query.setString("newTit",newUnidad.getTit());
            query.setString("newSit",newUnidad.getSitSig());
            query.setDate("newfecIni",newUnidad.getFecIni());
            query.setDate("newFecFin",newUnidad.getFecFin());
            query.executeUpdate();
            tx.commit();
        }catch (Exception e){
            tx.rollback();
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<UnidadDidactica> buscarUnidadesPorPlan(int idPlan) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(UnidadDidactica.class)
                    .add(Restrictions.eq("estReg",'A'))
                    .createAlias("periodo","pe",JoinType.INNER_JOIN)
                    .createCriteria("programacionAnual", JoinType.INNER_JOIN).add(Restrictions.eq("proAnuId",idPlan));
            return query.list();
        }catch (Exception e){
            throw  e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<UnidadDidactica> buscarUnidadesPorPlanConDatos(int idPlan) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(UnidadDidactica.class)
                    .add(Restrictions.eq("estReg", 'A'))
                    .createAlias("periodo", "pe", JoinType.INNER_JOIN)
                    .addOrder(Order.asc("uniDidId"))
                    .createCriteria("programacionAnual", JoinType.INNER_JOIN).add(Restrictions.eq("proAnuId", idPlan));
            List<UnidadDidactica> unidades = query.list();
            for(UnidadDidactica unidad : unidades){
                unidad.getEvaluaciones().size();
                unidad.getProductos().size();
                unidad.getMateriales().size();
                List<CompetenciasUnidadDidactica> aprendizajes = unidad.getCompetenciasUnidad();
                for(CompetenciasUnidadDidactica competenciaUnidad : aprendizajes){
                    competenciaUnidad.getIndicadores().size();
                }
            }
            return unidades;
        }catch (Exception e){
            throw  e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<UnidadDidactica> listarUnidadesPorDocenteYOrganizacion(int idOrg, int idDoc) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(UnidadDidactica.class)
                    .createAlias("programacionAnual", "pa", JoinType.INNER_JOIN)
                    .createAlias("pa.are", "ar", JoinType.INNER_JOIN)
                    .createAlias("pa.gra", "grad", JoinType.INNER_JOIN);
                    //.createAlias("pa.org", "org", JoinType.INNER_JOIN);
            query.createCriteria("pa.doc").add(Restrictions.eq("doc_id", idDoc));
            query.createCriteria("pa.org",JoinType.INNER_JOIN).add(Restrictions.eq("orgId", idOrg));
            query.add(Restrictions.eq("pa.estReg",'A'));
            query.add(Restrictions.eq("estReg",'A'));

            /*String hql = "SELECT u FROM UnidadDidactica u INNER JOIN FETCH u.programacionAnual pa INNER JOIN FETCH pa.are ar INNER JOIN FETCH ";
            Query query = session.createQuery(hql);*/
            return query.list();
        }catch (Exception e){
            throw  e;
        }finally {
            session.close();
        }
    }

    @Override
    public void registrarCapacidades(CompetenciasUnidadDidactica competencia) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            session.saveOrUpdate(competencia);
            tx.commit();
        }catch (Exception e){
            tx.rollback();
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<CompetenciasUnidadDidactica> listarCompetenciasUnidad(int idUnidad) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(CompetenciasUnidadDidactica.class)
                    .createAlias("indicadores", "selinds", JoinType.LEFT_OUTER_JOIN)
                    .add(Restrictions.eq("id.uniDidId", idUnidad))
                    .add(Restrictions.eq("estReg",'A'))
                    .setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
            query.createCriteria("competenciaCapacidad")
                    .createCriteria("cap").createCriteria("indicadores")
                    .addOrder(Order.asc("indAprId"))
                    .setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<IndicadorAprendizaje> listarIndicadoresUnidadPorCapacidad(int idUnidad, int idComp, int idCap) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String sql = "SELECT i.* FROM pedagogico.indicador_aprendizaje i" +
                    " INNER JOIN  pedagogico.indicadores_unidad_aprendizaje iu ON iu.ind_apr_id = i.ind_apr_id" +
                    " WHERE iu.uni_did_id =:uniId AND iu.com_id =:comId AND iu.cap_id =:capId";
            SQLQuery query = session.createSQLQuery(sql);
            query.setInteger("uniId",idUnidad);
            query.setInteger("comId",idComp);
            query.setInteger("capId",idCap);

            query.addEntity(IndicadorAprendizaje.class);
            //query.setResultTransformer(Transformers.aliasToBean(IndicadorAprendizaje.class));
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public void eliminarCompetenciasUnidad(int idUnidad, int idComp) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            String hql = "UPDATE CompetenciasUnidadDidactica  SET estReg = 'E'  WHERE  id.uniDidId =:idUnidad AND id.comId =:idComp";
            Query query = session.createQuery(hql);
            query.setInteger("idUnidad",idUnidad);
            query.setInteger("idComp",idComp);
            query.executeUpdate();
            tx.commit();
        }catch (Exception e){
            tx.rollback();
            throw e;
        }finally {
            session.close();
        }
    }
    @Override
    public void eliminarCompetenciasUnidad(int idUnidad, int idComp, int idCap) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            String hql = "UPDATE CompetenciasUnidadDidactica SET estReg = 'E' WHERE id.uniDidId =:idUnidad AND id.comId =:idComp AND id.capId =:idCap";
            Query query = session.createQuery(hql);
            query.setInteger("idUnidad",idUnidad);
            query.setInteger("idComp",idComp);
            query.setInteger("idCap",idCap);
            query.executeUpdate();
            tx.commit();
        }catch (Exception e){
            tx.rollback();
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public void eliminarIndicadorUnidadDidactica(int unidadId,int compId,int capId,int idIndicador) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            String hql = "DELETE FROM CompetenciasUnidadDidactica where id.uniDidId =:uId AND id.comId =:comId AND id.capId =:capId";
            Query query = session.createQuery(hql);
        }catch (Exception e){
            tx.rollback();
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public void updateCompetenciaUnidadDidactica(CompetenciasUnidadDidactica comUnid) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            session.update(comUnid);
            tx.commit();
        }catch (Exception e){
            tx.rollback();
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public CompetenciasUnidadDidactica buscarCompetenciaUnidad(int idUnidad, int idComp, int idCap) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT comUni FROM CompetenciasUnidadDidactica comUni LEFT JOIN FETCH comUni.indicadores  WHERE comUni.id.uniDidId =:uId AND comUni.id.comId =:comId AND comUni.id.capId =:capId";
            Query query = session.createQuery(hql);
            query.setInteger("uId",idUnidad);
            query.setInteger("comId",idComp);
            query.setInteger("capId",idCap);
            query.setMaxResults(1);
            return (CompetenciasUnidadDidactica)query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
    @Override
    public List<CompetenciasUnidadDidactica> buscarCompetenciasUnidad(int idUnidad) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT comUni FROM CompetenciasUnidadDidactica comUni LEFT JOIN FETCH comUni.indicadores  WHERE comUni.id.uniDidId =:uId";
            Query query = session.createQuery(hql);
            query.setInteger("uId",idUnidad);
            query.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
            query.setMaxResults(1);
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public MaterialRecursoProgramacion buscarMaterialPorNom(String nom) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(MaterialRecursoProgramacion.class)
                    .add(Restrictions.eq("nom", nom))
                    .setMaxResults(1);
            return (MaterialRecursoProgramacion) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<MaterialRecursoProgramacion> listarMateriales(int idUnidad) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria criteria = session.createCriteria(MaterialRecursoProgramacion.class)
                    .add(Restrictions.eq("estReg",'A'))
                    .createCriteria("unidadDidactica").add(Restrictions.eq("uniDidId",idUnidad));
            return criteria.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public MaterialRecursoProgramacion buscarMaterial(int idMaterial) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(MaterialRecursoProgramacion.class)
                    .add(Restrictions.eq("matId", idMaterial))
                    .setMaxResults(1);
            return (MaterialRecursoProgramacion) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public MaterialRecursoProgramacion registrarMaterial(MaterialRecursoProgramacion material) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            session.persist(material);
            tx.commit();
            return material;
        }catch (Exception e){
            tx.rollback();
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public void eliminarMaterial(int idMaterial) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            String hql = "DELETE FROM MaterialRecursoProgramacion WHERE id =:idMaterial";
            Query query = session.createQuery(hql);
            query.setInteger("idMaterial",idMaterial);
            query.executeUpdate();
            tx.commit();
        }catch (Exception e){
            tx.rollback();
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public MaterialRecursoProgramacion editarMaterial(MaterialRecursoProgramacion material) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            session.update(material);
            tx.commit();
            return material;
        }catch (Exception e){
            tx.rollback();
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<ProductosUnidadAprendizaje> listarProductosUnidad(int idUnidad) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(ProductosUnidadAprendizaje.class)
                    .add(Restrictions.eq("estReg",'A'))
                    .createCriteria("unidad").add(Restrictions.eq("uniDidId",idUnidad));
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public ProductosUnidadAprendizaje registrarProductoUnidad(ProductosUnidadAprendizaje producto) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            session.persist(producto);
            tx.commit();
            return producto;
        }catch (Exception e){
            tx.rollback();
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public void eliminarProductoUnidad(int idProducto) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            String hql = "DELETE FROM ProductosUnidadAprendizaje WHERE proUniAprId =:idProd";
            Query query = session.createQuery(hql);
            query.setInteger("idProd",idProducto);
            query.executeUpdate();
            tx.commit();
        }catch (Exception e){
            tx.rollback();
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public void editarProductoUnidad(ProductosUnidadAprendizaje producto) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            session.update(producto);
            tx.commit();
        }catch (Exception e){
            tx.rollback();
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<UnidadDidactica> listarUnidadesArea(int idPlan, int idDoc, int idArea, int idGrado) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT u FROM UnidadDidactica u INNER JOIN u.programacionAnual p" +
                    " WHERE p.planEstudios.plaEstId =:planId AND p.doc.doc_id =:docId AND p.are.areCurId =:areaId AND p.gra.graId =:graId";
            Query query = session.createQuery(hql);
            query.setInteger("planId",idPlan);
            query.setInteger("docId",idDoc);
            query.setInteger("areaId",idArea);
            query.setInteger("graId",idGrado);
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<PeriodosPlanEstudios> listarPeridosPLanEstudios(int idPlan, int idNivel, char tipPer) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT p FROM PeriodosPlanEstudios p INNER JOIN FETCH p.periodo per" +
                    " WHERE p.planEstudios.plaEstId =:planId AND p.nivel.nivId =:nivId" +
                    " AND per.perId =:tipPer";
            Query query = session.createQuery(hql);
            query.setInteger("planId",idPlan);
            query.setInteger("nivId",idNivel);
            query.setCharacter("tipPer", tipPer);
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public PeriodosPlanEstudios buscarPeriodoEvaluacion(int idPeriodo) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT p FROM PeriodosPlanEstudios p WHERE p.perPlaEstId =:perId";
            Query query = session.createQuery(hql);
            query.setInteger("perId",idPeriodo);
            return (PeriodosPlanEstudios) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<IndicadorAprendizaje> listarIndicadoresUnidad(int idUnidad) {
        
        /*
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<IndicadoresUnidadAprendizaje> ind_uni;
        List<IndicadorAprendizaje> indicadores = new ArrayList<>();
        try{
           String hql = "SELECT iu FROM IndicadoresUnidadAprendizaje iu INNER JOIN FETCH iu.indicador  WHERE iu.uni_did_id =:p1";
           Query query = session.createQuery(hql);
           query.setInteger("p1",idUnidad);
           ind_uni = query.list();
           for(IndicadoresUnidadAprendizaje ind : ind_uni){
               indicadores.add(ind.getIndicador());
           }
            return indicadores;
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
        */
        
          Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(IndicadorAprendizaje.class)
                    .createCriteria("indicadoresUnidad",JoinType.INNER_JOIN)
                    .createCriteria("unidad",JoinType.INNER_JOIN)
                    .add(Restrictions.eq("uniDidId",idUnidad));
            return  query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }

      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public UnidadDidactica buscarUnidadDidacticaPeriodo(int idUnidad) {
        Session session = HibernateUtil.getSessionFactory().openSession();
         try{
                
           String hql = "SELECT ud FROM UnidadDidactica ud INNER JOIN FETCH ud.periodo  WHERE ud.uniDidId =:p1";
           Query query = session.createQuery(hql);
           return (UnidadDidactica) query.uniqueResult();
           
         }
         catch(Exception e){
              throw e;
         }finally {
            session.close();
        }
        
        
        
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
