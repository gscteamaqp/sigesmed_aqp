/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.estudio_complementario.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.EstudioComplementarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.EstudioComplementario;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class AgregarEstudioComplementarioTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(AgregarEstudioComplementarioTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
         /*
        *   Parte para la lectura, verificacion y validacion de datos
         */
        EstudioComplementario estCom = null;
        
        Integer ficEscId = 0;

        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat sdo = new SimpleDateFormat("dd/MM/yyyy");

        try {
            JSONObject requestData = (JSONObject) wr.getData();

            ficEscId = requestData.getInt("ficEscId");
            Character tip = requestData.getString("tip").charAt(0);
            String des = requestData.getString("des");
            Character niv = requestData.getString("niv").charAt(0);
            String insCer = requestData.getString("insCer");
            String tipPar = requestData.getString("tipPar");
            Date fecIni = sdi.parse(requestData.getString("fecIni").substring(0, 10));
            Date fecTer = sdi.parse(requestData.getString("fecTer").substring(0, 10));
            Integer horLec = requestData.getInt("horLec");
            
            estCom = new EstudioComplementario(new FichaEscalafonaria(ficEscId), tip, des, niv, insCer, tipPar, fecIni, fecTer, horLec, wr.getIdUsuario(), new Date(), 'A');

        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE,"Datos nuevo estudio complementario",e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }
        //Fin

        /*
        *  Parte para la operacion en la Base de Datos
         */
        //si el pariente no existe en la tabla persona
        EstudioComplementarioDao estComDao = (EstudioComplementarioDao) FactoryDao.buildDao("se.EstudioComplementarioDao");
        try {
            estComDao.insert(estCom);
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar nuevo estudio complementario",e);
            System.out.println(e);
        }
        
        //Fin       

        /*
        *  Repuesta Correcta
         */
        JSONObject oResponse = new JSONObject();
        oResponse.put("estComId", estCom.getEstComId());
        oResponse.put("tip", estCom.getTip());
        oResponse.put("des", estCom.getDes());
        oResponse.put("niv", estCom.getNiv());
        oResponse.put("insCer", estCom.getInsCer());
        oResponse.put("tipPar", estCom.getTipPar());
        oResponse.put("fecIni", sdo.format(estCom.getFecIni()));
        oResponse.put("fecTer", sdo.format(estCom.getFecTer()));
        oResponse.put("horLec", estCom.getHorLec());
                
        return WebResponse.crearWebResponseExito("El registro del estudio complementario se realizo correctamente", oResponse);
        //Fin
        
    }
    
}
