package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.maestro.plan;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.ObjetivoProgramacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.ObjetivoProgramacion;
import org.hibernate.Session;

/**
 * Created by Administrador on 24/10/2016.
 */
public class ObjetivoProgramacionDaoHibernate extends GenericDaoHibernate<ObjetivoProgramacion> implements ObjetivoProgramacionDao {
    @Override
    public ObjetivoProgramacion buscarObjetivoPorId(int idObj) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            ObjetivoProgramacion obj = (ObjetivoProgramacion)session.get(ObjetivoProgramacion.class, idObj);
            return obj;
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
}
