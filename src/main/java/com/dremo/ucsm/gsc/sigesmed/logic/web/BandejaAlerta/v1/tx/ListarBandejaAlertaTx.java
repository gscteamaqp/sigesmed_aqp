/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.BandejaAlerta.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.BandejaAlertaDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaAlerta;
import java.util.List;

/**
 *
 * @author abel
 */
public class ListarBandejaAlertaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {   
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<BandejaAlerta> bandejas = null;
        BandejaAlertaDao bandejaDao = (BandejaAlertaDao)FactoryDao.buildDao("web.BandejaAlertaDao");
        try{
            bandejas = bandejaDao.listarTodosPorUsuario(wr.getIdUsuario());
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las bandejas de alertas\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar las bandejas de alertas", e.getMessage() );
        }
        //Fin        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        int i = 0;
        for(BandejaAlerta bandeja:bandejas ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("bandejaAlertaID",bandeja.getBanAleId() );
            
            oResponse.put("alertaID",bandeja.getAleId());
            oResponse.put("nombre",bandeja.getAlerta().getNom());
            oResponse.put("codigo",bandeja.getAlerta().getCod());
            oResponse.put("accion",bandeja.getAlerta().getAcc());
            oResponse.put("descripcion",bandeja.getAlerta().getDes());            
            oResponse.put("tipo",""+bandeja.getAlerta().getTipAle());                    
            switch(bandeja.getAlerta().getTipAle()){
                case 'I':oResponse.put("tipoIcono","alert-info");break;
                case 'A':oResponse.put("tipoIcono","alert-warning");break;
                case 'E':oResponse.put("tipoIcono","alert-danger");break;
            }
            
            oResponse.put("fechaEnvio",bandeja.getFecEnv());
            oResponse.put("fechaVisto",bandeja.getFecVis());
            
            oResponse.put("i",i++);
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente la bandeja de alertas",miArray);        
        //Fin
    }
    
}

