/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.estudiante.datos_estudiante.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.AsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.Inasistencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.Justificacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.JustificacionInasistenciaTrabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.RegistroAsistencia;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.DateUtil;
import com.dremo.ucsm.gsc.sigesmed.util.GCell;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Carlos
 */
public class ReporteFichaEstudianteTx implements ITransaction{
    
    @Override
    public WebResponse execute(WebRequest wr) {        
        
        
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Integer org;
        JSONObject requestData = (JSONObject)wr.getData();
        String nombre="";
        String nacionalidad="";
        String dni="";
        String nacimiento="";
        String sexo="";
        String peso="";
        String talla="";
        String alergia="";
        String sangre="";
        String email="";
        String numero1="";
        String apoderadoNombre="";
        String apoderadoNumero="";
        String nivel="";
        String grado="";
        String seccion="";
        String turno="";
        String orden="";
        
        try{
           org=requestData.getInt("orgID");
           JSONObject ficha=requestData.getJSONObject("ficha");
           nombre=ficha.getString("nombres");
           nacionalidad=ficha.getString("nacionalidad");
           dni=ficha.getString("dni");
           nacimiento=ficha.getString("nacimiento");
           sexo=ficha.getString("sexo");
           peso=ficha.getString("peso");
           talla=ficha.getString("talla");
           alergia=ficha.getString("alergia");
           sangre=ficha.getString("sangre");
           email=ficha.getString("email");
           numero1=ficha.getString("numero1");
           apoderadoNombre=ficha.getString("apoderadoNombre");
           apoderadoNumero=ficha.getString("apoderadoNumero");
           nivel=ficha.getString("nivel");
           grado=ficha.getString("grado");
           seccion=ficha.getString("seccion");
           turno=ficha.getString("turno");
           orden=ficha.getString("orden");
           
        }catch(Exception e){
            System.out.println("No se pudo verificar los datos \n"+e);
            return WebResponse.crearWebResponseError("No se pudo verificar los datos ", e.getMessage() );
        }
       
        OrganizacionDao organizacionDao= (OrganizacionDao)FactoryDao.buildDao("OrganizacionDao");
        Organizacion organizacion=null;
        try{
           
            organizacion=organizacionDao.load(Organizacion.class, org);
            
        }catch(Exception e){
            System.out.println("No se pudo verificar la organizacion \n"+e);
            return WebResponse.crearWebResponseError("No se pudo verificar la orgnaizacion ", e.getMessage() );
        }
        
        
        
        //Creando el reporte....        
        Mitext m = null;        
        try {
            m = new Mitext(false);
            m.agregarTitulo("FICHA DEL ESTUDIANTE");
        } catch (Exception ex) {
            System.out.println("No se pudo crear el documento \n"+ex);
            Logger.getLogger(ReporteFichaEstudianteTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        float[] columnWidthsD={6,6,3};
        Table tabla = new Table(columnWidthsD);
        tabla.setWidthPercent(100);
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("INSTITUCION EDUCATIVA").setFontSize(10).setTextAlignment(TextAlignment.LEFT)));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+organizacion.getNom()).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));

        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("COD").setFontSize(10).setTextAlignment(TextAlignment.LEFT)));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+organizacion.getCod()).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));

        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("DIRECCION").setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+organizacion.getDir()).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));

        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("NIVEL").setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+nivel).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,5).setBorder(Border.NO_BORDER));
        
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("GRADO").setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+grado+" "+seccion).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,5).setBorder(Border.NO_BORDER));
        
        //titulo datos personales
        float[] columnWidthsTitPer={1};
        Table tablaTitPer = new Table(columnWidthsTitPer);
        tablaTitPer.setWidthPercent(100);
        tablaTitPer.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).setBackgroundColor(Color.LIGHT_GRAY).add(new Paragraph("Datos Personales").setFontSize(11).setTextAlignment(TextAlignment.LEFT)));
        
        float[] columnWidthsPer={4,6,1,4,6,1};
        Table tablaPer = new Table(columnWidthsPer);
        tablaPer.setWidthPercent(100);
        
        tablaPer.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("Nombre").setFontSize(10).setTextAlignment(TextAlignment.LEFT)));
        tablaPer.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+nombre).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tablaPer.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
        tablaPer.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
        tablaPer.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
        tablaPer.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
        
        tablaPer.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("Nacionalidad").setFontSize(10).setTextAlignment(TextAlignment.LEFT)));
        tablaPer.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+nacionalidad).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tablaPer.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
        tablaPer.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("DNI").setFontSize(10).setTextAlignment(TextAlignment.LEFT)));
        tablaPer.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+dni).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tablaPer.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
        
        tablaPer.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("Fecha de Nacimiento").setFontSize(10).setTextAlignment(TextAlignment.LEFT)));
        tablaPer.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+nacimiento).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tablaPer.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
        tablaPer.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("Sexo").setFontSize(10).setTextAlignment(TextAlignment.LEFT)));
        tablaPer.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+sexo).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tablaPer.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
        
        tablaPer.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("Email").setFontSize(10).setTextAlignment(TextAlignment.LEFT)));
        tablaPer.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+email).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tablaPer.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
        tablaPer.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("Telefono").setFontSize(10).setTextAlignment(TextAlignment.LEFT)));
        tablaPer.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+numero1).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tablaPer.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
        
        tablaPer.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("Nombre Apoderado").setFontSize(10).setTextAlignment(TextAlignment.LEFT)));
        tablaPer.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+apoderadoNombre).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tablaPer.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
        tablaPer.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("Telefono Contacto").setFontSize(10).setTextAlignment(TextAlignment.LEFT)));
        tablaPer.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+apoderadoNumero).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tablaPer.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
        
        //titulo datos escolares
        float[] columnWidthsTitEsc={1};
        Table tablaTitEsco = new Table(columnWidthsTitEsc);
        tablaTitEsco.setWidthPercent(100);
        tablaTitEsco.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).setBackgroundColor(Color.LIGHT_GRAY).add(new Paragraph("Datos Escolares").setFontSize(11).setTextAlignment(TextAlignment.LEFT)));
        
        
        float[] columnWidthsEsc={4,6,1,4,6,1,4,6,1};
        Table tablaEsco = new Table(columnWidthsEsc);
        tablaEsco.setWidthPercent(100);
        
        tablaEsco.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("Nivel").setFontSize(10).setTextAlignment(TextAlignment.LEFT)));
        tablaEsco.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+nivel).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tablaEsco.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
        tablaEsco.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("Grado").setFontSize(10).setTextAlignment(TextAlignment.LEFT)));
        tablaEsco.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+grado).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tablaEsco.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
        tablaEsco.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("Seccion").setFontSize(10).setTextAlignment(TextAlignment.LEFT)));
        tablaEsco.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+seccion).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tablaEsco.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
        
        tablaEsco.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("Turno").setFontSize(10).setTextAlignment(TextAlignment.LEFT)));
        tablaEsco.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+turno).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tablaEsco.addCell(new Cell(1,7).setBorder(Border.NO_BORDER));
        
        //titulo datos clinicos
        float[] columnWidthsTitCli={1};
        Table tablaTitCli = new Table(columnWidthsTitCli);
        tablaTitCli.setWidthPercent(100);
        tablaTitCli.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).setBackgroundColor(Color.LIGHT_GRAY).add(new Paragraph("Datos Clinicos").setFontSize(11).setTextAlignment(TextAlignment.LEFT)));
        
        
        float[] columnWidthsCli={4,6,1,4,6,1,4,6,1};
        Table tablaCli = new Table(columnWidthsCli);
        tablaCli.setWidthPercent(100);
        
        tablaCli.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("Talla").setFontSize(10).setTextAlignment(TextAlignment.LEFT)));
        tablaCli.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+talla).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tablaCli.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
        tablaCli.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("Peso").setFontSize(10).setTextAlignment(TextAlignment.LEFT)));
        tablaCli.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+peso).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tablaCli.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
        tablaCli.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("Sangre").setFontSize(10).setTextAlignment(TextAlignment.LEFT)));
        tablaCli.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+sangre).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tablaCli.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
        
         tablaCli.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("Alergias").setFontSize(10).setTextAlignment(TextAlignment.LEFT)));
        tablaCli.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : "+alergia).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tablaCli.addCell(new Cell(1,7).setBorder(Border.NO_BORDER));
        
        m.agregarTabla(tabla);
        m.agregarParrafo("");
        m.agregarParrafo("");
        m.agregarTabla(tablaTitPer);
        m.agregarTabla(tablaPer);
        m.agregarParrafo("");
        m.agregarParrafo("");
        m.agregarTabla(tablaTitEsco);
        m.agregarTabla(tablaEsco);
        m.agregarParrafo("");
        m.agregarParrafo("");
        m.agregarTabla(tablaTitCli);
        m.agregarTabla(tablaCli);
        m.cerrarDocumento();
       
                 
        JSONArray miArray = new JSONArray();
        JSONObject oResponse = new JSONObject();        
        oResponse.put("datareporte",m.encodeToBase64());
        miArray.put(oResponse);                       
        
        return WebResponse.crearWebResponseExito("Se genero el reporte correctamente",miArray);        
        
    }
    
    
}