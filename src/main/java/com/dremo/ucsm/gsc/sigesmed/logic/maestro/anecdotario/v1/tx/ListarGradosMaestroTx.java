package com.dremo.ucsm.gsc.sigesmed.logic.maestro.anecdotario.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.anecdotario.AnecdotarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.ProgramacionAnualDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.PlanEstudiosDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Grado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Calendar;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 21/12/2016.
 */
public class ListarGradosMaestroTx implements ITransaction{
    private static Logger logger = Logger.getLogger(ListarGradosMaestroTx.class.getName());
    private static final int CURRICULA = 1;
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject)wr.getData();
        int org = data.getInt("org");
        int idDoc = data.getInt("doc");
        int idPlan = data.optInt("plane",-1);
        return listarGrados(idDoc,org,idPlan);

    }
    private WebResponse listarGrados(int idDoc,int idOrg,int idPlan){
        try{
            AnecdotarioDao anecDao = (AnecdotarioDao) FactoryDao.buildDao("maestro.anecdotario.AnecdotarioDao");
            ProgramacionAnualDao progDao = (ProgramacionAnualDao) FactoryDao.buildDao("maestro.plan.ProgramacionAnualDao");
            PlanEstudiosDao planDao = (PlanEstudiosDao) FactoryDao.buildDao("mech.PlanEstudiosDao");
            PlanEstudios plan = idPlan == -1 ?planDao.buscarVigentePorOrganizacion(idOrg) : progDao.buscarPlanEstudios(idPlan);
            Calendar now = Calendar.getInstance();
            int year = now.get(Calendar.YEAR);
            String yearInString = String.valueOf(year);
            if(plan==null){
                return WebResponse.crearWebResponseError("No se Encontro un Plan de Estudios Vigente para el "+yearInString); 
            }
            List<Grado> grados = anecDao.listarGradosDocente(idOrg,plan.getPlaEstId(), idDoc);
            if(grados ==null || grados.size()==0){
                return WebResponse.crearWebResponseError("No se encontro Grados asociados al Plan de Estudio "+yearInString + " Verifique que el Docente tenga asignado una Plaza Magisterial");
            }
            
            if(grados==null){
                return WebResponse.crearWebResponseError("No se Encontro un Plan de Estudios Vigente"); 
            }
            JSONArray jsonNiveles = new JSONArray();
            for(Grado grado : grados){
                JSONObject jsonNivel  = buscarElementoJSON(jsonNiveles, "id", new Integer(grado.getNivId()));
                if(jsonNivel == null){
                    jsonNivel = new JSONObject();
                    jsonNivel.put("id",grado.getNivId());
                    jsonNivel.put("nom",grado.getNivnom());
                    jsonNivel.put("grados",new JSONArray());
                    jsonNiveles.put(jsonNivel);
                }
            }
            JSONArray jsonGrados = new JSONArray();
            for(Grado grado : grados){
                JSONObject jsonNivel  = buscarElementoJSON(jsonNiveles, "id", new Integer(grado.getNivId()));
                JSONObject jsonGrado  = buscarElementoJSON(jsonGrados, "id", new Integer(grado.getGraId()));
                if(jsonGrado == null){
                    jsonGrado = new JSONObject();
                    jsonGrado.put("id",grado.getGraId());
                    jsonGrado.put("nom",grado.getNom());
                    JSONArray secciones = new JSONArray();
                    secciones.put(grado.getSec());
                    jsonGrado.put("secciones",secciones);
                    jsonGrados.put(jsonGrado);
                    jsonNivel.getJSONArray("grados").put(jsonGrado);
                }else{
                    JSONArray secciones = jsonGrado.getJSONArray("secciones");
                    secciones.put(grado.getSec());
                }
            }
            List<AreaCurricular> areas = progDao.buscarAreaCurricularCurricula(CURRICULA);
            JSONArray areasJSON = new JSONArray(EntityUtil.listToJSONString(
                    new String[]{"areCurId","nom"},
                    new String[]{"cod","nom"},
                    areas
            )) ;
            JSONObject result = new JSONObject();
            result.put("niveles",jsonNiveles);
            result.put("areas",areasJSON);
            return WebResponse.crearWebResponseExito("Se realizo con exito el registro de los grados",result);
        }catch (Exception e){
            logger.log(Level.SEVERE,"listarGrados",e);
            return WebResponse.crearWebResponseError("No se pudieron los datos de los grados");
        }
    }
    private JSONObject buscarElementoJSON(JSONArray elementosJSON,String key,Object value){
        if(elementosJSON.length() == 0){
            return null;
        }
        else{
            for (int i = 0; i < elementosJSON.length(); i++){
                JSONObject objJSON = elementosJSON.getJSONObject(i);
                if(objJSON.get(key).equals(value)){
                    return objJSON;
                }
            }
            return null;
        }
    }

}
