/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.util;

import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.io.font.FontProgramFactory;
import com.itextpdf.layout.border.Border;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.io.source.ByteArrayOutputStream;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfResources;
import com.itextpdf.layout.element.Image;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.kernel.pdf.extgstate.PdfExtGState;
import com.itextpdf.layout.Canvas;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.Style;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.HorizontalAlignment;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.VerticalAlignment;
import com.itextpdf.text.Rectangle;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import javax.imageio.ImageIO;
import org.apache.commons.codec.binary.Base64;
import org.jfree.chart.JFreeChart;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class Mitext {

//    public static final String DEST = "ReportesTemp/"; 
    public static final String DEST = ServicioREST.PATH_SIGESMED + "/ReportesTemp/";
    private PdfWriter writer;
    private PdfDocument pdf;
    private Document document;
    private String nombre;
    ByteArrayOutputStream baos = null;

    //diferentes estilos
    private PdfFont fontTimes = PdfFontFactory.createFont(FontConstants.TIMES_ROMAN);

    private Style titulo = new Style().setFont(fontTimes).setFontSize(15);
    private Style date = new Style().setFont(fontTimes).setFontSize(8);
    private Style general = new Style().setFont(fontTimes).setFontSize(10);
    private Style generalBold = new Style().setFont(fontTimes).setFontSize(10).setBold();

    //CONSTANTES DE TIPO DE LETRA//
    public static final Integer COURIER = 3;
    public static final Integer TIMES_NEW_ROMAN = 2;
    public static final Integer ARIAL = 1;
    //CONSTANTES DE FORMATO DE ALINEACION//
    public static final Integer CENTRADO = 1;
    public static final Integer JUSTIFICADO = 2;
    public static final Integer IZQUIERDA = 3;
    public static final Integer DERECHA = 4;

    private PdfFont myFont = null;// PdfFontFactory.createFont(FontConstants.HELVETICA);
    private Style myEstilo = null;

    //formatoFont: tipo de Letra
    //size: tamanho de la letra
    //isNegrita , true si es negrita
    //isCursiva, true si es cursiva
    public void setStyle(Integer formatoFont, Integer size, boolean isNegrita, boolean isCursiva, boolean isSubrayado) throws IOException {

        Integer _n = 0;

        if (formatoFont == TIMES_NEW_ROMAN) {
            myFont = PdfFontFactory.createFont(FontConstants.TIMES_ROMAN);
        }
        if (formatoFont == ARIAL) {
            myFont = PdfFontFactory.createFont(FontConstants.HELVETICA);
        }
        if (formatoFont == COURIER) {
            myFont = PdfFontFactory.createFont(FontConstants.COURIER);
        }

        myEstilo = new Style().setFont(myFont).setFontSize(size);

        if (isNegrita) {
            myEstilo.setBold();
        }
        if (isCursiva) {
            myEstilo.setItalic();
        }
        if (isSubrayado) {
            myEstilo.setUnderline();
        }

    }

    //fin   
//    inicializa el documento itext
//    nombreDoc: nombre del documento pdf(ejemplo.pdf), puede ser una ruta.
//               pero el documento siempre estara dentro de la carpeta "/ReportesTemp".
    public Mitext(String nombreDoc) throws FileNotFoundException, Exception {
        nombre = DEST + nombreDoc;
        //Initialize PDF writer
        writer = new PdfWriter(nombre);
        //Initialize PDF document
        pdf = new PdfDocument(writer);
        //Se agrega el encabezado
        pdf.addEventHandler(PdfDocumentEvent.END_PAGE, new MyEventHandler());
        // Initialize document
        document = new Document(pdf);
        document.add(new Paragraph(""));

    }
    
    
//    inicializa el documento itext
//    nombreDoc: nombre del documento pdf(ejemplo.pdf), puede ser una ruta, 
//               pero el documento siempre estara dentro de la carpeta "/ReportesTemp".
//    margenes: arreglo con los margenes del documento
//    rotacion: si es true la horienzacion del documento sera horizontal 
    public Mitext(String nombreDoc, int[] margenes, Boolean rotacion, String titulo_encabezado) throws FileNotFoundException, Exception {
        nombre = DEST + nombreDoc;
        //Initialize PDF writer
        writer = new PdfWriter(nombre);
        //Initialize PDF document
        pdf = new PdfDocument(writer);
        //Se agrega el encabezado
        pdf.addEventHandler(PdfDocumentEvent.END_PAGE, new MyEventHandler(titulo_encabezado));
        // Initialize document
        document = new Document(pdf);
        document.setMargins(margenes[0], margenes[1], margenes[2], margenes[3]);
        if (rotacion) {
            document = new Document(pdf, PageSize.A4.rotate());
        } else {
            document = new Document(pdf);
        }
        document.add(new Paragraph(""));
    }

    //este metodo crea el pdf en memoria
    public Mitext(Boolean rotacion, String titulo_encabezado) throws FileNotFoundException, Exception {

        baos = new ByteArrayOutputStream();
        //Initialize PDF writer
        writer = new PdfWriter(baos);
        //Initialize PDF document
        pdf = new PdfDocument(writer);

        //Se agrega el encabezado
        pdf.addEventHandler(PdfDocumentEvent.END_PAGE, new MyEventHandler());
        // Initialize document
        // Initialize document
        document = new Document(pdf);

        if (rotacion) {
            document = new Document(pdf, PageSize.A4.rotate());
        } else {
            document = new Document(pdf);
        }
        document.add(new Paragraph(""));

    }

    //este metodo crea el pdf en memoria
    public Mitext() throws FileNotFoundException, Exception {

        baos = new ByteArrayOutputStream();
        //Initialize PDF writer
        writer = new PdfWriter(baos);
        //Initialize PDF document
        pdf = new PdfDocument(writer);

        //Se agrega el encabezado
        pdf.addEventHandler(PdfDocumentEvent.END_PAGE, new MyEventHandler());
        // Initialize document
        document = new Document(pdf, PageSize.A4.rotate());

        document.add(new Paragraph(""));

    }

    public Mitext(Boolean rotacion) throws FileNotFoundException, Exception {

        baos = new ByteArrayOutputStream();
        //Initialize PDF writer
        writer = new PdfWriter(baos);
        //Initialize PDF document
        pdf = new PdfDocument(writer);

        //Se agrega el encabezado
        pdf.addEventHandler(PdfDocumentEvent.END_PAGE, new MyEventHandler());
        // Initialize document
        if (rotacion) {
            document = new Document(pdf, PageSize.A4.rotate());
        } else {
            document = new Document(pdf);
        }

        document.add(new Paragraph(""));

    }

    public ByteArrayOutputStream getBaos() {
        return baos;
    }

    public void setBaos(ByteArrayOutputStream baos) {
        this.baos = baos;
    }

    public void agregarParrafo(String parrafo) {
        document.add(new Paragraph(parrafo));
    }

    public void agregarParrafoMyEstilo(String texto, Integer direccion) throws IOException {

        Table table = new Table(1);
        switch (direccion) {
            case 1:
                table.addCell(getCell(texto, TextAlignment.CENTER, myEstilo));
                break;
            case 2:
                table.addCell(getCell(texto, TextAlignment.JUSTIFIED, myEstilo));
                break;
            case 3:
                table.addCell(getCell(texto, TextAlignment.LEFT, myEstilo));
                break;
            case 4:
                table.addCell(getCell(texto, TextAlignment.RIGHT, myEstilo));
                break;

        }

        document.add(table);
        document.add(new Paragraph(""));
        document.add(new Paragraph(""));

    }

    public void agregarMarcaDeAgua(String texto) throws IOException {
        int n = pdf.getNumberOfPages();

        PdfFont font = PdfFontFactory.createFont(FontProgramFactory.createFont(FontConstants.HELVETICA));
        Paragraph p = new Paragraph(texto).setFont(font).setFontSize(15);
//        new Canvas(under, pdf, pdf.getDefaultPageSize())
//                .showTextAligned(p, 297, 550, 1, TextAlignment.CENTER, VerticalAlignment.TOP, 0);
        for (int i = 1; i <= n; i++) {
            PdfCanvas over = new PdfCanvas(pdf.getPage(i));
//        over.setFillColor(Color.BLACK);
//        p = new Paragraph("This watermark is added ON TOP OF the existing content")
//                .setFont(font).setFontSize(15);
//        new Canvas(over, pdf, pdf.getDefaultPageSize()).showTextAligned(p, 297, 500, 1, TextAlignment.CENTER, VerticalAlignment.TOP, 0);

            over.saveState();
            PdfExtGState gs1 = new PdfExtGState();
            gs1.setFillOpacity(0.5f);
            over.setExtGState(gs1);
            new Canvas(over, pdf, pdf.getDefaultPageSize()).showTextAligned(p, 297, 450, 1, TextAlignment.CENTER, VerticalAlignment.TOP, 0);

            over.restoreState();
        }

    }

    public void agregarTitulo(String parrafo) throws IOException {

        //formato de la fecha...
        SimpleDateFormat formato
                = new SimpleDateFormat("dd/MM/yyyy", new Locale("es", "ES"));
        String fecha = formato.format(new Date());
        //fin

        Table table = new Table(1);
        table.addCell(getCell(parrafo, TextAlignment.CENTER, titulo));
        table.addCell(getCell("Fecha: " + fecha, TextAlignment.RIGHT, date));

        document.add(table);

    }

    public Cell getCell(String text, TextAlignment alignment, Style style) {
        Cell cell = new Cell().add(new Paragraph(text).addStyle(style));
        cell.setPadding(0);
        cell.setTextAlignment(alignment);
        cell.setBorder(Border.NO_BORDER);
        return cell;
    }

    public void agregarSubtitulos(JSONObject o) {

        Table table = new Table(4);

        Iterator iter = o.keys();
        String key = "";
        String value = "";
        while (iter.hasNext()) {
            key = (String) iter.next();
            value = o.getString(key);
            table.addCell(getCell(key, TextAlignment.LEFT, generalBold));
            table.addCell(getCell(value, TextAlignment.LEFT, general));
        }

        document.add(table);

    }

    //i: numero de lineas a insertar
    public void newLine(int i) {
        while (i-- > 0) {
            document.add(new Paragraph(""));
        }
    }

    public void setParagraph(Paragraph p) {
        document.add(p);
    }

    //metodo para agregar una imagen
    public void agregarImagen(String rutaImagen, float x, float y) throws MalformedURLException {
        Image imagen = new Image(ImageDataFactory.create(rutaImagen), x, y);
        document.add(imagen);
    }

    public void agregarImagen(String rutaImagen) throws MalformedURLException {
        Image imagen = new Image(ImageDataFactory.create(rutaImagen));
        document.add(imagen);
    }

    //metodo para agregar una imagen en base 64
    public void agregarImagen64(String fileBase64, float x, float y) throws MalformedURLException {
        Image imagen = new Image(ImageDataFactory.create(Base64.decodeBase64(fileBase64)), x, y);
        document.add(imagen);
    }

    public void agregarImagen64(String fileBase64) throws MalformedURLException {
        Image imagen = new Image(ImageDataFactory.create(Base64.decodeBase64(fileBase64)));
        document.add(imagen);
    }
    
    //metodo para agregar una imagen en base 64 centrada con un tamaño ajustable
    public void agregarImagen64Centrada(String fileBase64, float x, float y) throws MalformedURLException{        
        Image imagen = new Image(ImageDataFactory.create(Base64.decodeBase64(fileBase64)));
        imagen.scaleAbsolute(x,y);
        imagen.setHorizontalAlignment(HorizontalAlignment.CENTER);      
        document.add(imagen);
    }
    
    //metodo para agregar una tabla

    public void agregarTabla(Table tabla) {
        document.add(tabla);
    }

    //metodo para agregar un grafico
    public void agregarGrafico(JFreeChart chart, int width, int height) throws IOException {

        ByteArrayOutputStream _baos = new ByteArrayOutputStream();
        ImageIO.write(chart.createBufferedImage(width, height), "png", _baos);
        document.add(new Image(ImageDataFactory.create(_baos.toByteArray())));

    }

    //siempre se debe usar este metodo el terminar de crear un documento
    public void cerrarDocumento() {
        document.close();
    }

    public PdfDocument getPdf() {
        return pdf;
    }

    public void setPdf(PdfDocument pdf) {
        this.pdf = pdf;
    }

    public PdfWriter getWriter() {
        return writer;
    }

    public void setWriter(PdfWriter writer) {
        this.writer = writer;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    //solo usar cuando se crea el pdf en memoria
    public String encodeToBase64() {
        return "data:application/pdf;base64," + Base64.encodeBase64String(baos.toByteArray());
    }

}
