/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.desplazamiento.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.desplazamiento.v1.tx.*;
/**
 *
 * @author Yemi
 */
public class ComponentRegister implements IComponentRegister{

    @Override
    public WebComponent createComponent() {
        WebComponent seComponent = new WebComponent(Sigesmed.MODULO_ESCALAFON);

        seComponent.setName("desplazamiento");
        seComponent.setVersion(1);
        
        seComponent.addTransactionGET("listarDesplazamientos", ListarDesplazamientosTx.class);
	seComponent.addTransactionPOST("agregarDesplazamiento", AgregarDesplazamientoTx.class);
        seComponent.addTransactionPOST("actualizarDesplazamiento", ActualizarDesplazamientoTx.class);
        seComponent.addTransactionGET("listarPorOrganizacion", ListarPorOrganizacionTx.class);
        
        return seComponent;
    }
    
}
