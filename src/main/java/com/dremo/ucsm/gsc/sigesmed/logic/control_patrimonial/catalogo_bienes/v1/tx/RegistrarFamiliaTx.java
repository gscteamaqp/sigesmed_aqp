/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.catalogo_bienes.v1.tx;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.FamiliaGenericaDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.FamiliaGenerica;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
 *
 * @author Administrador
 */
public class RegistrarFamiliaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        FamiliaGenerica fam_gen = null;
        
        JSONObject requestData = (JSONObject)wr.getData();
        JSONArray familias = requestData.getJSONArray("familias");
        for(int i = 0; i < familias.length();i++){
             JSONObject familia =familias.getJSONObject(i);
             int cla_gen_id = familia.getInt("cla_gen_id");
             int gru_gen_id = familia.getInt("gru_gen_id");
             String cod = familia.getString("cod");
             String nom = familia.getString("nom");
             Date fec_mod = new Date();
             int usu_mod = familia.getInt("usu_mod");
             char est_reg = 'A';
             
             fam_gen = new FamiliaGenerica(0,cla_gen_id,gru_gen_id,cod,nom,fec_mod,usu_mod,est_reg);
             
             FamiliaGenericaDAO fam_gen_dao = (FamiliaGenericaDAO)FactoryDao.buildDao("scp.FamiliaGenericaDAO");
             
             fam_gen_dao.insert(fam_gen);
          
        }
        
        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
