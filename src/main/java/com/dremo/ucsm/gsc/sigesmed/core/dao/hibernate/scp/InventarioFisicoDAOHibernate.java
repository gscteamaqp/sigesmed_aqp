/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scp;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import java.util.List;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.InventarioInicialDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.InventarioFisicoDAO;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.InventarioFisico;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.InventarioFisicoDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.InventarioInicial;
/**
 *
 * @author Administrador
 */
public class InventarioFisicoDAOHibernate extends GenericDaoHibernate<InventarioFisico> implements InventarioFisicoDAO {

    @Override
    public List<InventarioFisico> listarInventarioFisico(int org_id) {
        
        List<InventarioFisico> inventario_fisico = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
        //    String hql = "SELECT DISTINCT ii FROM InventarioInicial ii JOIN FETCH ii.inv_ini_det detalle JOIN FETCH detalle.cod_bie WHERE ii.est_reg!='E'";
        //       String hql = "SELECT ii FROM InventarioInicial ii JOIN FETCH ii.inv_ini_det WHERE ii.est_reg!='E'";
            String hql = "SELECT DISTINCT ifi FROM InventarioFisico ifi   WHERE ifi.est_reg!='E' and ifi.org_id=:p1";
      
            Query query = session.createQuery(hql); 
            query.setParameter("p1",org_id);
            inventario_fisico = query.list();
            
        }
        catch(Exception e){
         System.out.println("No se pudo Mostrar el Inventario Fisico \\n "+ e.getMessage());
             throw new UnsupportedOperationException("No se pudo Mostrar el Inventario Fisico \\n "+ e.getMessage());
        }
        finally{
            session.close();
        }
        return inventario_fisico;  
        
        
        
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public InventarioFisico obtenerInventarioFisico(int inv_id, int org_id) {
        
        InventarioFisico ifi = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        
        try{
            
            String hql = "SELECT DISTINCT ifi FROM InventarioFisico ifi   WHERE ifi.inv_fis_id=:p1 and ifi.org_id=:p2";
            Query query = session.createQuery(hql); 
            query.setParameter("p1",inv_id);
            query.setParameter("p2",org_id);
            ifi = (InventarioFisico)(query.uniqueResult());
            
            
        }catch(Exception e){
            System.out.println("No se pudo Mostrar el Inventario Fisico \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Mostrar el Inventario Fisico\\n "+ e.getMessage());
        }
          finally{
            session.close();
        }
        return ifi;  

      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<InventarioFisicoDetalle> obtenerDetalleInventarioFisico(int inv_id, int org_id) {
        
        List<InventarioFisicoDetalle> ifid = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        
        try{
            
            String hql = "SELECT DISTINCT ifid FROM InventarioFisicoDetalle ifid JOIN FETCH ifid.cod_bie bienes JOIN FETCH bienes.ambiente JOIN FETCH bienes.anexo WHERE ifid.inv_ini_id=:p1";
            Query query = session.createQuery(hql); 
            query.setParameter("p1",inv_id);
           
            ifid = query.list();
            
        }catch(Exception e){
            System.out.println("No se pudo Mostrar el Inventario Fisico Detalle \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Mostrar el Inventario Fisico Detalle\\n "+ e.getMessage());
 
        }
        finally{
          session.close();
        }
        return ifid;  
        
        
        
     //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void eliminar_detalle(int inv_fis_id) {
        
         Session session = HibernateUtil.getSessionFactory().openSession();
         Transaction miTx = session.beginTransaction();
        try{
            String hql = "DELETE FROM InventarioFisicoDetalle WHERE inv_ini_det= :p1";
            Query query = session.createQuery(hql); 
            query.setParameter("p1",inv_fis_id);
            query.executeUpdate();
             miTx.commit();
        }catch(Exception e){
              miTx.rollback();
            System.out.println("No se pudo Eliminar el Detalle del Inventario Inicial \\n "+ e.getMessage());
        }
        
        
        
     //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void eliminar_inventario_fisico(int inv_fis_id) {
         Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            String hql = "UPDATE InventarioFisico  set est_reg = 'E'  WHERE inv_fis_id = :p1";
             
            Query query = session.createQuery(hql);
            query.setParameter("p1",inv_fis_id);
            query.executeUpdate();
            miTx.commit();
        
        }
        catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo Eliminar el Inventario Fisico  \\n "+ e.getMessage());
             //  throw new UnsupportedOperationException("No se pudo Mostrar la lista de Bienes Muebles por Fecha \\n "+ e.getMessage());
            e.printStackTrace();
        }
        
        
        
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<InventarioFisico> reporteInventario(int org_id) {
        
        List<InventarioFisico> invFis = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try {
            
            String hql = "SELECT if FROM InventarioFisico if JOIN FETCH if.mov_ingresos mvi JOIN FETCH mvi.tipo_movimiento WHERE if.est_reg!='E' and if.org_id=:p1";

            Query query = session.createQuery(hql);
            query.setParameter("p1", org_id);
            invFis = query.list();
            
        } catch (Exception e) {
            t.rollback();            
            throw new UnsupportedOperationException("No se pudo Mostrar el Inventario Inicial \\n " + e.getMessage());
        } finally {
            session.close();
        }
        
        return invFis;
    }
    
    
}
