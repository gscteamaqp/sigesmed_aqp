/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.configuracion_patrimonial.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;


import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.ControlPatrimonial;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.ControlPatrimonialDAO;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;

import org.json.JSONArray;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class ListarControlPatrimonialUGELTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        
                JSONArray miArray = new JSONArray();
        try{
            
            List<ControlPatrimonial> control_patrimonial = null;
            JSONObject requestData = (JSONObject)wr.getData();
            int orgId = requestData.getInt("org_id");
            ControlPatrimonialDAO cp_dao = (ControlPatrimonialDAO)FactoryDao.buildDao("scp.ControlPatrimonialDAO");
            control_patrimonial = cp_dao.listarPatrimonioUGEL(orgId);
            
            SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
            
            for(ControlPatrimonial cp : control_patrimonial){                

                JSONObject oResponse = new JSONObject();
                oResponse.put("cod_ie", cp.getOrg().getCod());
                oResponse.put("des_ie", cp.getOrg().getDes());
                oResponse.put("dir_ie", cp.getOrg().getDir());
                oResponse.put("cp_obs", cp.getObs());
                oResponse.put("fec_ini", DATE_FORMAT.format(cp.getFec_ini()));
                oResponse.put("fec_cie", DATE_FORMAT.format(cp.getFec_cie()));
                oResponse.put("org_id", cp.getOrg().getOrgId());
                oResponse.put("org_nom", cp.getOrg().getNom());
          
               miArray.put(oResponse);
           }            
        }catch(Exception e){
            System.out.println("No se pudo Listar los Grupos Genericos\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los Grupos Genericos", e.getMessage() );        
        }
          return WebResponse.crearWebResponseExito("Se Listo Los Grupos Genericos Correctamente",miArray); 
        
     }
    
    
}
