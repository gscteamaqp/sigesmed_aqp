/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.std;

import java.io.Serializable;

/**
 *
 * @author abel
 */
public final class DocumentoExpedienteId implements Serializable {
    
    private static final long serialVersionUID = 1562260205094677677L;
    private Integer expediente;
    private int docExpId;

    public DocumentoExpedienteId() {
    }

    public DocumentoExpedienteId(Integer expediente, int docExpId) {
        this.setExpediente(expediente);
        this.setDocExpId(docExpId);
    }

    @Override
    public int hashCode() {
        return ((this.getExpediente() == null
                ? 0 : this.getExpediente().hashCode())
                ^ ((int) this.getDocExpId()));
    }

    @Override
    public boolean equals(Object otherOb) {

        if (this == otherOb) {
            return true;
        }
        if (!(otherOb instanceof DocumentoExpedienteId)) {
            return false;
        }
        DocumentoExpedienteId other = (DocumentoExpedienteId) otherOb;
        return ((this.getExpediente() == null
                ? other.getExpediente() == null : this.getExpediente()
                .equals(other.getExpediente()))
                && (this.getDocExpId() == other.getDocExpId()));
    }

    @Override
    public String toString() {
        return "" + getExpediente() + "-" + getDocExpId();
    }

    public Integer getExpediente() {
        return expediente;
    }
    public void setExpediente(Integer expediente) {
        this.expediente = expediente;
    }

    public int getDocExpId() {
        return docExpId;
    }
    public void setDocExpId(int docExpId) {
        this.docExpId = docExpId;
    }
    
}
