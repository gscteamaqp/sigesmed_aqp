/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.familiares.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ParientesDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TipoParienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Parientes;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TipoPariente;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Yemi
 */
public class ActualizarFamiliarTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ActualizarFamiliarTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try{
     
            DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
            JSONObject requestData = (JSONObject)wr.getData();
        
            Integer perId = requestData.getInt("perId");
            Integer parId = requestData.getInt("parId");
            Integer tipoParienteId = requestData.getInt("tipoParienteId");
            String parApeMat = requestData.optString("parApeMat");
            String parApePat = requestData.optString("parApePat");
            String parNom = requestData.optString("parNom");
            String parDni = requestData.optString("parDni");
            Date parFecNac = sdi.parse(requestData.optString("parFecNac").substring(0, 10));
            String parFij = requestData.optString("parFij");
            String parNum1 = requestData.optString("parNum1");
            String parNum2 = requestData.optString("parNum2");
            String parEmail = requestData.optString("parEmail");
            String parSex = requestData.optString("parSex");         
            
            return actualizarFamiliar(perId, parId, tipoParienteId, parApePat, parApeMat, parNom, 
            parDni, parFecNac, parFij, parNum1, parNum2, parEmail, parSex);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar pariente",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    
    private WebResponse actualizarFamiliar(Integer perId, Integer parId, Integer tipoParienteId, String parApePat, String parApeMat, String parNom, 
            String parDni, Date parFecNac, String parFij, String parNum1, String parNum2, String parEmail, String parSex) {
        try{
            PersonaDao personaDao = (PersonaDao)FactoryDao.buildDao("se.PersonaDao");        
            Persona pariente = personaDao.buscarPersonaPorId(parId);

            pariente.setApeMat(parApeMat);
            pariente.setApePat(parApePat);
            pariente.setNom(parNom);
            pariente.setDni(parDni);
            pariente.setFecNac(parFecNac);
            pariente.setFij(parFij);
            pariente.setNum1(parNum1);
            pariente.setNum2(parNum2);
            pariente.setEmail(parEmail);
            pariente.setSex(parSex.charAt(0));
            
            personaDao.update(pariente);

            ParientesDao parientesDao = (ParientesDao)FactoryDao.buildDao("se.ParientesDao");        
            Parientes parientes = new Parientes(parId, perId, new TipoPariente(tipoParienteId));
            parientes.setPariente(pariente);
            
            parientesDao.update(parientes);
            
            DateFormat sdo = new SimpleDateFormat("yyyy-MM-dd");
            JSONObject oResponse = new JSONObject();
            oResponse.put("parId", parientes.getParId());
            oResponse.put("perId", parientes.getPerId());
            oResponse.put("parApePat", parientes.getPariente().getApePat());
            oResponse.put("parApeMat", parientes.getPariente().getApeMat());
            oResponse.put("parNom", parientes.getPariente().getNom());
            oResponse.put("parDni", parientes.getPariente().getDni());
            oResponse.put("parFecNac", sdo.format(parientes.getPariente().getFecNac()));
            oResponse.put("parFij", parientes.getPariente().getFij());
            oResponse.put("parNum1", parientes.getPariente().getNum1());
            oResponse.put("parNum2", parientes.getPariente().getNum2());
            oResponse.put("parEmail", parientes.getPariente().getEmail());
            oResponse.put("parSex", parientes.getPariente().getSex());
            oResponse.put("tipoParienteId", parientes.getParentesco().getTpaId());
            oResponse.put("parentesco", "");
            
            
            return WebResponse.crearWebResponseExito("Pariente actualizado exitosamente",oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizarPariente",e);
            return WebResponse.crearWebResponseError("Error, el pariente no fue actualizado");
        }
    } 
}
