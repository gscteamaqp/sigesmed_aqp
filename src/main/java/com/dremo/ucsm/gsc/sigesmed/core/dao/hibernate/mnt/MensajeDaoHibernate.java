/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mnt;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mnt.MensajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mnt.Mensaje;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Administrador
 */
public class MensajeDaoHibernate extends GenericDaoHibernate<Mensaje> implements MensajeDao{ 

    @Override
    public int buscarSessionPorIdUsuario(int usuID) {
         int id;
        Session session=HibernateUtil.getSessionFactory().openSession();
        Transaction t=session.beginTransaction();
        try{
            //Listar ayudas por funcionalidad
            String hql="SELECT S.usuSesId FROM UsuarioSession S WHERE S.usuario.usuId=:p1";
            Query query=session.createQuery(hql);
            query.setParameter("p1",usuID);           
            id=((int)query.uniqueResult());
        }catch(Exception e){
            t.rollback();
            System.err.println("No se pudo obtener el código de session\\n "+e.getMessage());
            throw new UnsupportedOperationException("No se pudo obtener el código de session\\n" +e.getMessage());
        }finally{
            session.close();
        }
        
        return id;
    }

    @Override
    public List<Mensaje> buscarMensajesEnviadosPorSession(int sessionID) {
        List<Mensaje> enviados=new ArrayList<Mensaje>();
        Session session=HibernateUtil.getSessionFactory().openSession();
        Transaction t=session.beginTransaction();
        try{
//            String hql="SELECT m FROM Mensaje m LEFT JOIN FETCH m.usuAut aut LEFT JOIN FETCH m.usuDest dest LEFT JOIN FETCH aut.persona LEFT JOIN FETCH m.usuDest.persona WHERE m.usuAut.usuSesId =:p1 ";
            String hql="SELECT m FROM Mensaje m LEFT JOIN FETCH m.usuAut aut LEFT JOIN FETCH aut.persona LEFT JOIN FETCH m.usuDest dest LEFT JOIN FETCH dest.persona WHERE m.usuAut.usuSesId =:p1 and m.estRegAut='A'";
            Query query=session.createQuery(hql);
            query.setParameter("p1",sessionID);
            enviados=query.list();
     
        }catch(Exception e){
          
            System.err.println("No se puede obtener los mensajes\\n "+e.getMessage());
            throw new UnsupportedOperationException("No se pudo obtener los mensajes\\n" +e.getMessage());
        }finally{
            session.close();
        }
        
        return enviados;
    }
    @Override
    public List<Mensaje> buscarMensajesRecibidosPorSession(int sessionID) {
        List<Mensaje> enviados=new ArrayList<Mensaje>();
        Session session=HibernateUtil.getSessionFactory().openSession();
        Transaction t=session.beginTransaction();
        try{
            String hql="SELECT m FROM Mensaje m LEFT JOIN FETCH m.usuAut aut LEFT JOIN FETCH aut.persona LEFT JOIN FETCH m.usuDest dest LEFT JOIN FETCH dest.persona WHERE m.usuDest.usuSesId =:p1 and m.estRegDest='A'";
            Query query=session.createQuery(hql);
            query.setParameter("p1",sessionID);
            enviados=query.list();
     
        }catch(Exception e){          
            System.err.println("No se puede obtener los mensajes\\n "+e.getMessage());
            throw new UnsupportedOperationException("No se pudo obtener los mensajes\\n" +e.getMessage());
        }finally{
            session.close();
        }
        
        return enviados;
    }

    @Override
    public void archivarMensajeAutor(int menID) {
        Session session=HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            String hql="UPDATE Mensaje m SET m.menEstAut='A' WHERE m.mensajeID=:p1 and m.estRegAut='A'";
            Query q=session.createQuery(hql);
            q.setParameter("p1",menID);
            q.executeUpdate();
            miTx.commit();
        }catch(Exception e){
            miTx.rollback();
            System.err.println("No se puede archivar el mensaje\\n "+e.getMessage());
            throw new UnsupportedOperationException("No se puede archivar el mensaje\\n" +e.getMessage());
        }finally{
            session.close();
        }                        
    }

    @Override
    public void archivarMensajeDestinatario(int menID) {
        Session session=HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            String hql="UPDATE Mensaje m SET m.menEstDest='A' WHERE m.mensajeID=:p1 and m.estRegDest='A'";
            Query q=session.createQuery(hql);
            q.setParameter("p1",menID);
            q.executeUpdate();
            miTx.commit();
        }catch(Exception e){
            miTx.rollback();
            System.err.println("No se puede archivar el mensaje\\n "+e.getMessage());
            throw new UnsupportedOperationException("No se puede archivar el mensaje\\n" +e.getMessage());
        }finally{
            session.close();
        }                
    }

    @Override
    public void eliminarMensajeAutor(int menID) {
        Session session=HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            String hql="UPDATE Mensaje m SET m.estRegAut='E' WHERE m.mensajeID=:p1";
            Query q=session.createQuery(hql);
            q.setParameter("p1",menID);
            q.executeUpdate();
            miTx.commit();
        }catch(Exception e){
            miTx.rollback();
            System.err.println("No se pudo eliminar el mensaje\\n "+e.getMessage());
            throw new UnsupportedOperationException("No se pudo eliminar el mensaje\\n" +e.getMessage());
        }finally{
            session.close();
        }      
    }

    @Override
    public void eliminarMensajeDestinatario(int menID) {
        Session session=HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            String hql="UPDATE Mensaje m SET m.estRegDest='E' WHERE m.mensajeID=:p1";
            Query q=session.createQuery(hql);
            q.setParameter("p1",menID);
            q.executeUpdate();
            miTx.commit();
        }catch(Exception e){
            miTx.rollback();
            System.err.println("No se pudo eliminar el mensaje\\n "+e.getMessage());
            throw new UnsupportedOperationException("No se pudo eliminar el mensaje\\n" +e.getMessage());
        }finally{
            session.close();
        }   
    }
    
}