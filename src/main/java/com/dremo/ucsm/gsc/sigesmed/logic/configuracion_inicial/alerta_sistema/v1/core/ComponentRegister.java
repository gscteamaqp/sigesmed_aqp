/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.alerta_sistema.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.alerta_sistema.v1.tx.ActualizarAlertaSistemaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.alerta_sistema.v1.tx.EliminarAlertaSistemaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.alerta_sistema.v1.tx.InsertarAlertaSistemaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.alerta_sistema.v1.tx.ListarAlertaSistemaTx;

/**
 *
 * @author abel
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_CONFIGURACION);        
        
        //Registrando el Nombre del componente
        component.setName("alertaSistema");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionPOST("insertarAlerta", InsertarAlertaSistemaTx.class);
        component.addTransactionGET("listarAlertas", ListarAlertaSistemaTx.class);
        component.addTransactionDELETE("eliminarAlerta", EliminarAlertaSistemaTx.class);
        component.addTransactionPUT("actualizarAlerta", ActualizarAlertaSistemaTx.class);
        
        return component;
    }
}
