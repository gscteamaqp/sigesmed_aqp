/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.calendario.v1.core;

import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.horarios.v1.core.*;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.calendario.v1.tx.EditarDiaEspecialTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.calendario.v1.tx.EliminarDiaEspecialTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.calendario.v1.tx.NuevoDiaEspecialTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.horarios.v1.tx.ListarCalendarioTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.horarios.v1.tx.ListarHorariosTx;





/**
 *
 * @author carlos
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_CONTROL_PERSONAL);        
        
        //Registrnado el Nombre del componente
        component.setName("registroCalendario");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionPUT("editarDiaEspecial", EditarDiaEspecialTx.class);
 //       component.addTransactionGET("listarHorarios", ListarHorariosTx.class);
    //    component.addTransactionGET("listarCalendario", ListarCalendarioTx.class);
        component.addTransactionPOST("nuevoDiaEspecial", NuevoDiaEspecialTx.class);
        component.addTransactionDELETE("eliminarDiaEspecial", EliminarDiaEspecialTx.class);
//        component.addTransactionGET("buscarPersona", BuscarPersonaTx.class);
      
        
        return component;
    }
}
