/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.salidas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.AltasDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.BajasDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.BienesMueblesDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.DetalleAltasDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.DetalleBajasDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.DetalleSalidasDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.SalidasDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.Altas;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.AltasDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BajasDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BienesMuebles;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.Salidas;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.SalidasDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import org.json.JSONObject;

/**
 *
 * @author harold
 */
public class RegistrarSalidasTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        JSONObject requestData = (JSONObject) wr.getData();
        Salidas salida = null;
        SalidasDetalle sal_det = null;
        BienesMuebles bie_mue = null;
        
        try {
            
            int id_bien = requestData.getInt("id_bien");
            String verificar = requestData.getString("verificar");
            int cau_sal_id = requestData.getInt("causal_id");
            int org_id = requestData.getInt("org_id");
            int org_id_des = requestData.getInt("org_id_des");
            int usu_mod = requestData.getInt("usu_mod");
            int mov_ing_id = requestData.getInt("mov_ing_id");
            
            BienesMueblesDAO bie_dao = (BienesMueblesDAO)FactoryDao.buildDao("scp.BienesMueblesDAO");
            SalidasDAO sal_dao = (SalidasDAO)FactoryDao.buildDao("scp.SalidasDAO");
            BajasDAO baj_dao = (BajasDAO)FactoryDao.buildDao("scp.BajasDAO");
            AltasDAO alt_dao = (AltasDAO)FactoryDao.buildDao("scp.AltasDAO");
            DetalleAltasDAO det_alt_dao = (DetalleAltasDAO)FactoryDao.buildDao("scp.DetalleAltasDAO");
            
            salida = new Salidas(0,cau_sal_id, org_id, org_id_des, usu_mod, mov_ing_id, new Date(), 'A');
            
            switch (verificar) {
                case "A":
                    Altas alt = null;
                    AltasDetalle alt_det = null;
                    int alt_id;
                    /*Eliminamos la Alta (Cabecera+Detalle)*/
                    alt_det = baj_dao.alta_baja(id_bien);
                    alt_id = alt_det.getAltas_id();
                    
                    alt = new Altas();
                    alt.setAltas_id(alt_id);
                    det_alt_dao.deleteAbsolute(alt_det);
                    alt_dao.deleteAbsolute(alt);

                    // return WebResponse.crearWebResponseExito("ERROR: El Bien Mueble ya ha sido dado de Alta");
                    break;
                case "B":
                    return WebResponse.crearWebResponseExito("ERROR: El Bien Mueble ya ha sido dado de Baja");

            }
            
            /*INSERTAMOS EL BIEN DADO DE BAJA*/ 
            sal_dao.insert(salida);
            
            System.out.println("bien: "+ id_bien+" " + salida.getSalidas_id());
            
            sal_det = new SalidasDetalle(0,id_bien,salida.getSalidas_id());
            DetalleSalidasDAO det_sal_dao = (DetalleSalidasDAO)FactoryDao.buildDao("scp.DetalleSalidasDAO");
            det_sal_dao.insert(sal_det);                      
            
            bie_dao.updateBienPorSalida(id_bien, org_id_des, "N");
            
        } catch (Exception e) {
            
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar la salida del bien, datos incorrectos", e.getMessage());
            
        }
        
        JSONObject oResponse = new JSONObject();
        oResponse.put("salida_id", salida.getSalidas_id());
        return WebResponse.crearWebResponseExito("El registro de la salida del bien  se realizo correctamente", oResponse);

        
    }       
}
