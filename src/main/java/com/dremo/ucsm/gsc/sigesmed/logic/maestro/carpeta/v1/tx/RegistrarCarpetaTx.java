package com.dremo.ucsm.gsc.sigesmed.logic.maestro.carpeta.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.carpeta.CarpetaPedagogicaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.CarpetaPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 10/10/2016.
 */
public class RegistrarCarpetaTx implements ITransaction {
    private static final Logger logger = Logger.getLogger(RegistrarCarpetaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        CarpetaPedagogica carpeta = new CarpetaPedagogica(data.getInt("eta"),data.optString("des","descripcion de la carpeta"),
                new Date());
        return registrarCarpeta(carpeta);
    }

    private WebResponse registrarCarpeta(CarpetaPedagogica carpeta) {
        try{
            CarpetaPedagogicaDao carpetaDao = (CarpetaPedagogicaDao) FactoryDao.buildDao("maestro.carpeta.CarpetaPedagogicaDao");
            if(carpetaDao.buscarCarpetaPorEtapa(carpeta.getEta()) != null){
                return WebResponse.crearWebResponseError("Ya existe la carpeta para el año indicado",WebResponse.BAD_RESPONSE);
            }
            else {
                carpetaDao.insert(carpeta);
                return WebResponse.crearWebResponseExito("Se realizo el registro correctamen",WebResponse.OK_RESPONSE).setData(
                        new JSONObject(
                                EntityUtil.objectToJSONString(
                                        new String[]{"carDigId","fecCre"},
                                        new String[]{"cod","cre"},
                                        carpeta
                                )
                        )
                );
            }

        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarCarpeta",e);
            return WebResponse.crearWebResponseError("Error al registrar la carpeta",WebResponse.BAD_RESPONSE);
        }

    }
}
