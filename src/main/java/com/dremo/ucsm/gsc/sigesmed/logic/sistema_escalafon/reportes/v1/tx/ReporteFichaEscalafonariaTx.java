/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ParientesDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.AscensoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ColegiaturaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DemeritoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DesplazamientoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.EstudioComplementarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.EstudioPostgradoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ExposicionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FichaEscalafonariaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FormacionEducativaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.PublicacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ReconocimientoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Ascenso;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Colegiatura;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Demerito;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Desplazamiento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.EstudioComplementario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.EstudioPostgrado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Exposicion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FormacionEducativa;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Parientes;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Publicacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Reconocimiento;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.layout.element.Paragraph;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class ReporteFichaEscalafonariaTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ReporteFichaEscalafonariaTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {

        JSONObject data = (JSONObject) wr.getData();
        Integer ficEscId = data.optInt("ficEscId");
        Integer traId = data.optInt("traId");
        String perDni = data.optString("perDni");
        
        return crearReporte(ficEscId, traId, perDni);
    }

    private WebResponse crearReporte(Integer ficEscId, Integer traId, String perDni) {
        try{
            String data = crearArchivo(ficEscId, traId, perDni);
            return WebResponse.crearWebResponseExito("", new JSONObject().put("file",data));
        }catch (Exception e){
            logger.log(Level.SEVERE,"crearReporte",e);
            return WebResponse.crearWebResponseError("No se puede crear el reporte");
        }
    }
    private String  crearArchivo(Integer ficEscId, Integer traId, String perDni) throws Exception{
        FichaEscalafonariaDao  fichaEscalafonariaDao = (FichaEscalafonariaDao) FactoryDao.buildDao("se.FichaEscalafonariaDao");
        
        ParientesDao parientesDao = (ParientesDao) FactoryDao.buildDao("se.ParientesDao");
        FormacionEducativaDao formacionEducativaDao = (FormacionEducativaDao) FactoryDao.buildDao("se.FormacionEducativaDao");
        ColegiaturaDao colegiaturaDao = (ColegiaturaDao) FactoryDao.buildDao("se.ColegiaturaDao");
        EstudioComplementarioDao estudioComplementarioDao = (EstudioComplementarioDao) FactoryDao.buildDao("se.EstudioComplementarioDao");
        ExposicionDao exposicionDao = (ExposicionDao) FactoryDao.buildDao("se.ExposicionDao");
        PublicacionDao publicacionDao = (PublicacionDao) FactoryDao.buildDao("se.PublicacionDao");
        DesplazamientoDao desplazamientoDao = (DesplazamientoDao) FactoryDao.buildDao("se.DesplazamientoDao");
        ReconocimientoDao reconocimientoDao = (ReconocimientoDao) FactoryDao.buildDao("se.ReconocimientoDao");
        DemeritoDao demeritoDao = (DemeritoDao) FactoryDao.buildDao("se.DemeritoDao");
        EstudioPostgradoDao estudioPostgradoDao = (EstudioPostgradoDao) FactoryDao.buildDao("se.EstudioPostgradoDao");
        AscensoDao  ascensoDao = (AscensoDao) FactoryDao.buildDao("se.AscensoDao");
        
        
        FichaEscalafonaria datosGenerales = null;
        
        List<Parientes> parientes = null;
        List<FormacionEducativa> formacionesEducativas = null;
        List<Colegiatura> colegiaturas = null;
        List<EstudioComplementario> estudiosComplementarios = null;
        List<EstudioComplementario> estudiosEspecializacion = null;
        List<EstudioComplementario> conocimientosInformaticos = null;
        List<EstudioComplementario> idiomas = null;
        List<Exposicion> exposiciones = null;
        List<Publicacion> publicaciones = null;
        List<Desplazamiento> desplazamientos = null;
        List<Reconocimiento> reconocimientos = null;
        List<Reconocimiento> meritos = null;
        List<Reconocimiento> bonificaciones = null;
        List<Demerito> demeritos = null;
        List<EstudioPostgrado> estudiosPostgrado = null;
        List<Ascenso> ascensos = null;
        
        datosGenerales = fichaEscalafonariaDao.buscarPorDNI(perDni);
        
        parientes = parientesDao.listarxTrabajador(traId);
        formacionesEducativas = formacionEducativaDao.listarxFichaEscalafonaria(ficEscId);
        colegiaturas = colegiaturaDao.listarxFichaEscalafonaria(ficEscId);
        estudiosComplementarios = estudioComplementarioDao.listarxFichaEscalafonaria(ficEscId);
        exposiciones = exposicionDao.listarxFichaEscalafonaria(ficEscId);
        publicaciones = publicacionDao.listarxFichaEscalafonaria(ficEscId);
        desplazamientos = desplazamientoDao.listarxFichaEscalafonaria(ficEscId);
        reconocimientos = reconocimientoDao.listarxFichaEscalafonaria(ficEscId);
        demeritos = demeritoDao.listarxFichaEscalafonaria(ficEscId);
        estudiosPostgrado = estudioPostgradoDao.listarxFichaEscalafonaria(ficEscId);
        ascensos = ascensoDao.listarxFichaEscalafonaria(ficEscId);
        
        
        Mitext mitext = new Mitext(true,"SIGESMED");
        mitext.agregarTitulo("Ficha escalafonaria");

        PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
        
        //Datos personales
        Paragraph p = new Paragraph("I. DATOS PERSONALES");
        p.setKeepTogether(true);
        p.setFont(bold).setFontSize(12);
        mitext.getDocument().add(p);

        GTabla gtabla = new GTabla(new float[]{5.0f, 5.0f, 5.0f});
        gtabla.build(new String[]{"APELLIDO PATERNO","APELLIDO MATERNO","NOMBRES"});
        gtabla.processLine(new String[]{datosGenerales.getTrabajador().getPersona().getApePat(), datosGenerales.getTrabajador().getPersona().getApeMat(), 
            datosGenerales.getTrabajador().getPersona().getNom() });
        mitext.agregarTabla(gtabla);
        
        //Datos familiares
        p = new Paragraph("II. DATOS FAMILIARES");
        p.setKeepTogether(true);
        p.setFont(bold).setFontSize(12);
        mitext.getDocument().add(p);

        gtabla = new GTabla(new float[]{5.0f, 2.0f, 1.0f, 1.0f, 1.0f, 2.0f});
        gtabla.build(new String[]{"APELLIDOS Y NOMBRES", "PARENTESCO", "FECHA DE NACIMIENTO", "EDAD", "SEXO", "Nº DNI"});

        if (parientes != null){
            for(Parientes pa:parientes){
                String apellidosYNombres = pa.getPariente().getApePat() + " " + pa.getPariente().getApeMat() + " " +pa.getPariente().getNom();
                String edad = calcularEdad(pa.getPariente().getFecNac()).toString();
                Character sexo = pa.getPariente().getSex();
                if(sexo==null)
                    sexo = ' ';
                gtabla.processLine(new String[]{apellidosYNombres, pa.getParentesco().getTpaDes(), pa.getPariente().getFecNac().toString(), edad, 
                    sexo.toString(), pa.getPariente().getDni()});
            }
        }
        mitext.agregarTabla(gtabla);
        
        //Formaciones educativas
        p = new Paragraph("III. FORMACION EDUCATIVA");
        p.setKeepTogether(true);
        p.setFont(bold).setFontSize(12);
        mitext.getDocument().add(p);

        gtabla = new GTabla(new float[]{1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f});
        gtabla.build(new String[]{"TIPO FORMACION", "NIVEL ACADEMICO", "Nº DE TITULO", "ESPECIALIDAD O PROG. ACADEMICO", "ESTUDIOS CONCLUIDOS", "FECHA DE EXPEDICION", "CENTRO DE ESTUDIOS/LUGAR"});
        
        if (formacionesEducativas != null){
           for(FormacionEducativa fe:formacionesEducativas){
                gtabla.processLine(new String[]{mostrarTipoFormacionEducativa(fe.getTipFor()), fe.getNivAca(), fe.getNumTit(), fe.getEspAca(), fe.getEstCon()?"SI": "NO", fe.getFecExp().toString(), fe.getCenEst()});
            }
        }
        mitext.agregarTabla(gtabla);
        
        //Colegiaturas
        p = new Paragraph("INFORMACION RESPECTO A COLEGIATURA");
        p.setKeepTogether(true);
        p.setFont(bold).setFontSize(12);
        mitext.getDocument().add(p);

        gtabla = new GTabla(new float[]{5.0f, 3.0f, 3.0f});
        gtabla.build(new String[]{"COLEGIO PROFESIONAL", "REG. Nº COLEGIATURA", "CONDICION A LA FECHA"});
        
        if (colegiaturas != null){
           for(Colegiatura c:colegiaturas){
                gtabla.processLine(new String[]{c.getNomColPro(), c.getNumRegCol(), c.getConReg()? "Habilitado": "No Habilitado"});
            } 
        }
        mitext.agregarTabla(gtabla);
        
        /*//Reconocimientos
        for(EstudioComplementario ec:estudiosComplementarios){
            switch (ec.getTip()) {
                case '1':
                    conocimientosInformaticos.add(ec);
                    break;
                case '2':
                    idiomas.add(ec);
                    break;
                default:
                    estudiosEspecializacion.add(ec);
                    break;
            }
        }
        
        //Estudios de especializacion
        p = new Paragraph("ESTUDIOS DE ESPECIALIZACION");
        p.setKeepTogether(true);
        p.setFont(bold).setFontSize(12);
        mitext.getDocument().add(p);

        gtabla = new GTabla(new float[]{5.0f, 5.0f, 2.0f, 1.0f, 1.0f, 1.0f});
        gtabla.build(new String[]{"DESCRIPCION", "INSTITUCION CERTIFICADORA", "TIPO DE PARTICIPACION", "FECHA DE INICIO", "FECHA DE TERMINO", "HORAS LECTIVAS"});
       
        if (estudiosEspecializacion != null){
            for(EstudioComplementario ec:estudiosEspecializacion){
                gtabla.processLine(new String[]{ec.getDes(), ec.getInsCer(), ec.getTipPar(), ec.getFecIni().toString(), ec.getFecTer().toString(), 
                    ec.getHorLec().toString()});
            }
        }
        mitext.agregarTabla(gtabla);

        //Conocimientos informaticos
        p = new Paragraph("CONOCIMIENTOS INFORMATICOS");
        p.setKeepTogether(true);
        p.setFont(bold).setFontSize(12);
        mitext.getDocument().add(p);

        gtabla = new GTabla(new float[]{5.0f, 3.0f});
        gtabla.build(new String[]{"CONOCIMIENTOS", "NIVEL"});
        
        if (conocimientosInformaticos != null){
            for(EstudioComplementario ec:conocimientosInformaticos){
                gtabla.processLine(new String[]{ec.getDes(), mostrarNivel(ec.getNiv())});
            }
        }
        mitext.agregarTabla(gtabla);

        //Idiomas
        p = new Paragraph("IDIOMAS");
        p.setKeepTogether(true);
        p.setFont(bold).setFontSize(12);
        mitext.getDocument().add(p);

        gtabla = new GTabla(new float[]{5.0f, 3.0f});
        gtabla.build(new String[]{"IDIOMAS", "NIVEL"});
        
        if (idiomas != null){
            for(EstudioComplementario ec:idiomas){
                gtabla.processLine(new String[]{ec.getDes(), mostrarNivel(ec.getNiv())});
            }
        }
        mitext.agregarTabla(gtabla);
*/

        //Exposiciones y/o Ponencias
        p = new Paragraph("EXPOSICIONES Y/O PONENCIAS");
        p.setKeepTogether(true);
        p.setFont(bold).setFontSize(12);
        mitext.getDocument().add(p);

        gtabla = new GTabla(new float[]{5.0f, 5.0f, 3.0f, 1.5f, 1.5f, 1.5f});
        gtabla.build(new String[]{"DESCRIPCION", "INSTITUCION ORGANIZADORA", "TIPO DE PARTICIPACION", "FECHA DE INICIO","FECHA DE TERMINO", "HORAS LECTIVAS"});
        
        if(exposiciones != null){
            for(Exposicion eyp:exposiciones){
                gtabla.processLine(new String[]{eyp.getDes(), eyp.getInsOrg(), eyp.getTipPar(), eyp.getFecIni().toString(), eyp.getFecTer().toString(), eyp.getHorLec().toString()});
            }
        }
        mitext.agregarTabla(gtabla);

        /*//Publicaciones
        p = new Paragraph("PUBLICACIONES");
        p.setKeepTogether(true);
        p.setFont(bold).setFontSize(12);
        mitext.getDocument().add(p);

        gtabla = new GTabla(new float[]{5.0f, 3.0f, 5.0f, 3.0f, 3.0f, 1.0f, 2.0f});
        gtabla.build(new String[]{"NOMBRE DE LA EDITORIAL, REVISTA O MEDIO DE DIFUSION", "TIPO DE PUBLICACION", "TITULO DE PUBLICACION", "GRADO DE PARTICIPACION", 
            "LUGAR", "FECHA DE PUBLICACION", "Nº DE REGISTRO"});
        
        if (publicaciones != null){
            for(Publicacion pu:publicaciones){
                gtabla.processLine(new String[]{ pu.getNomEdi(), pu.getTipPub(), pu.getTitPub(), pu.getGraPar(), pu.getLug(), pu.getFecPub().toString(), pu.getNumReg()});
            }
        }
        mitext.agregarTabla(gtabla);
*/
        //Cargos desempeñados
        p = new Paragraph("IV. CARGOS DESEMPEÑADOS");
        p.setKeepTogether(true);
        p.setFont(bold).setFontSize(12);
        mitext.getDocument().add(p);

        float[] colWidthsTitle1 = {5, 5, 5, 5, 5, 5, 5, 5};
        gtabla = new GTabla(colWidthsTitle1);
        gtabla.setWidthPercent(100);
        gtabla.build(new String[]{"CARGO, DESPLAZAMIENTO O TIPO DE SERVIDOR", "Nº RESOLUCION", "FECHA DE RESOLUCION", "INSTITUCION EDUCATIVA", "CARGO",
        "HORAS", "FECHA INICIO", "FECHA TERMINO"});
        
        if (desplazamientos != null){
            for(Desplazamiento d:desplazamientos){
                gtabla.processLine(new String[]{mostrarTipoDesplazamiento(d.getTip()), d.getNumRes(), d.getFecRes().toString(), d.getInsEdu(), d.getCar(), 
                    d.getJorLab(), d.getFecIni().toString(), d.getFecTer().toString()});
            }
        }
        mitext.agregarTabla(gtabla);

        /*
        for(Reconocimiento r:reconocimientos){
            if (r.getMot() == '3' || r.getMot() == '4' || r.getMot() == '5'){
                bonificaciones.add(r);
            }else{
                meritos.add(r);
            }
        }
        
        //Meritos, felicitaciones y reconocimientos
        p = new Paragraph("V. MERITOS, FELICITACIONES Y RECONOCIMIENTOS");
        p.setKeepTogether(true);
        p.setFont(bold).setFontSize(12);
        mitext.getDocument().add(p);

        gtabla = new GTabla(new float[]{3.0f, 3.0f, 5.0f, 5.0f});
        gtabla.build(new String[]{"Nº RESOLUCION", "FECHA DE RESOLUCION", "MOTIVO DEL MERITO, FELICITACION Y/O RECONOCIMIENTO", "ENTIDAD QUE EMITE RESOLUCION"});

        if (meritos != null){
            for(Reconocimiento m:meritos){
                gtabla.processLine(new String[]{m.getNumRes(), m.getFecRes().toString(), mostrarMotivo(m.getMot()), m.getEntEmi()});
            }
        }
        mitext.agregarTabla(gtabla);
        
        //BONIFICACIONES POR CUMPLIR 25 Y 30 AÑOS DE SERVICIOS OFICIALES Y BONIFICIACIONES POR LUTO Y SEPELIO
        p = new Paragraph("VI. BONIFICACIONES POR CUMPLIR 25 Y 30 AÑOS DE SERVICIOS OFICIALES Y BONIFICIACIONES POR LUTO Y SEPELIO");
        p.setKeepTogether(true);
        p.setFont(bold).setFontSize(12);
        mitext.getDocument().add(p);

        gtabla = new GTabla(new float[]{3.0f, 3.0f, 5.0f, 5.0f});
        gtabla.build(new String[]{"Nº RESOLUCION", "FECHA DE RESOLUCION", "25, 30 AÑOS DE SRVICIOS / LUTO Y SEPELIO", "ENTIDAD QUE EMITE RESOLUCION"});
        
        if (bonificaciones != null){
            for(Reconocimiento b:bonificaciones){
                gtabla.processLine(new String[]{b.getNumRes(), b.getFecRes().toString(), mostrarMotivo(b.getMot()), b.getEntEmi()});
            } 
        }
        mitext.agregarTabla(gtabla);
        */
        //Demeritos
        p = new Paragraph("VII. DEMERITOS");
        p.setKeepTogether(true);
        p.setFont(bold).setFontSize(12);
        mitext.getDocument().add(p);
        
        float[] colWidthsTitle = {10, 5, 5, 5, 5, 5, 5};
        gtabla = new GTabla(colWidthsTitle);
        gtabla.setWidthPercent(100);
        gtabla.build(new String[]{"ENTIDAD QUE EMITE LA RESOLUCION", "Nº RESOLUCION", "FECHA DE R.D", 
            "RESUELVE SEPARACION DEFINITIVA Y/O TEMPORAL DEL CARGO","FECHA INICIO", "FECHA FIN", "MOTIVO DE SEPARACION"});
        
        if (demeritos != null){
            for(Demerito d:demeritos){
                gtabla.processLine(new String[]{d.getEntEmi(), d.getNumRes(), d.getFecRes().toString(), d.getSep()?"SI":"NO", d.getFecIni().toString(), 
                    d.getFecFin().toString(), d.getMot()});
            }
        }
        mitext.agregarTabla(gtabla);
        
        //Acumulacion de años de estudios y reconocimientos por estudios de maestrias
        p = new Paragraph("IX. ACUMULACION DE AÑOS DE ESTUIDIOS Y RECONOCIMIENTOS POR ESTUDIOS DE MAESTRIAS");
        p.setKeepTogether(true);
        p.setFont(bold).setFontSize(12);
        mitext.getDocument().add(p);

        gtabla = new GTabla(new float[]{3.0f, 1.0f, 3.0f, 4.0f});
        gtabla.build(new String[]{"Nº RESOLUCION", "FECHA DE RESOLUCION", "FECHA DE INICIO DE ESTUDIOS Y TERMINO DE ESTUDIOS", "INSTITUO Y/O UNIVERSIDAD"});
        
        if (estudiosPostgrado != null){
            for(EstudioPostgrado ep:estudiosPostgrado){
                gtabla.processLine(new String[]{ep.getNumRes(), ep.getFecRes().toString(), ep.getFecIniEst().toString() + " a " + ep.getFecTerEst().toString(), 
                    ep.getIns()});
            }
        }
        mitext.agregarTabla(gtabla);
        
        //Ascensos
        p = new Paragraph("IX. REGISTRO DE ASCENSOS");
        p.setKeepTogether(true);
        p.setFont(bold).setFontSize(12);
        mitext.getDocument().add(p);

        gtabla = new GTabla(new float[]{3.0f, 3.0f, 3.0f, 3.0f});
        gtabla.build(new String[]{"Nº RESOLUCION","FECHA DE RESOLUCION","FECHA EN LA QUE SURTE EFECTO LA RESOLUCION", "NIVEL, ESCALA AL QUE ASCIENDE"});
        
        if (ascensos != null){
            for(Ascenso a:ascensos){
                gtabla.processLine(new String[]{a.getNumRes(), a.getFecRes().toString(), a.getFecEfe().toString(), a.getEsc()});
            }
        }
        mitext.agregarTabla(gtabla);

        mitext.cerrarDocumento();
        return mitext.encodeToBase64();
    }
    private String mostrarMotivo(Character m){
        switch (m){
            case '1': return "Mérito";
            case '2': return "Felicitación";
            case '3': return "25 años de servicios";
            case '4': return "30 años de servicios";
            case '5': return "Luto y sepelio";
            default: return "Otros";
        }
    }
    private String mostrarTipoDesplazamiento(Character t){
        switch (t){
            case '1': return "Designación";
            case '2': return "Rotación";
            case '3': return "Reasignación";
            case '4': return "Destaque";
            case '5': return "Permuta";
            case '6': return "Encargo";
            case '7': return "Comisión de servicio";
            case '8': return "Transferencia";
            default: return "Otros";
        }
    }
    
    private String mostrarTipoEstudioComplementario(Character ec){
        switch (ec){
            case '1': return "Informatica";
            case '2': return "Idiomas";
            case '3': return "Certificación";
            case '4': return "Diplomado";
            case '5': return "Especialización";
            default: return "Otros";
        }
    }
    
    private String mostrarNivel(Character n){
        switch (n){
            case 'B': return "Básico";
            case 'I': return "Intermedio";
            case 'A': return "Avanzado";
            default: return "Ninguno";
        }
    }  
    
    private String mostrarTipoFormacionEducativa(Character t){
        switch (t){
            case '1': return "Estudios Basicos Regulares";
            case '2': return "Estudios Tecnicos";
            case '3': return "Bachiller";
            case '4': return "Universitario";
            case '5': return "Licenciatura";
            case '6': return "Maestria";
            case '7': return "Doctorado";
            default: return "Otros";
        }
    }
    
    private Integer calcularEdad(Date fecNac) {
        Date fechaActual = new Date();
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        String fecha_nac = formato.format(fecNac);
        String hoy = formato.format(fechaActual);
        String[] dat1 = fecha_nac.split("/");
        String[] dat2 = hoy.split("/");
        int anos = Integer.parseInt(dat2[2]) - Integer.parseInt(dat1[2]);
        int mes = Integer.parseInt(dat2[1]) - Integer.parseInt(dat1[1]);
        if (mes < 0) {
            anos = anos - 1;
        } else if (mes == 0) {
            int dia = Integer.parseInt(dat2[0]) - Integer.parseInt(dat1[0]);
            if (dia > 0) {
                anos = anos - 1;
            }
        }
        return anos;
    }

}
