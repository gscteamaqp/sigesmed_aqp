/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.legajo_personal.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.LegajoPersonalDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.LegajoPersonal;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class AgregarLegajosTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(AgregarLegajosTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        
        LegajoPersonal legajoPersonal = null;
        List<LegajoPersonal> legajos = new ArrayList<LegajoPersonal>();
        List<FileJsonObject> archivos = new ArrayList<FileJsonObject>();
        Integer ficEscId = 0;
        Character catLeg = '0';
        Character subCat = '0';
        String nomLeg = "";
        JSONObject arcJSON = null;
        
        
        try {
            
            JSONObject requestData = (JSONObject) wr.getData();
            
            JSONArray rLegajos = requestData.getJSONArray("legajos");
            FileJsonObject file = null;
            
            for(int i = 0;i < rLegajos.length();i++) {
                ficEscId = rLegajos.getJSONObject(i).getInt("ficEscId");
                catLeg = rLegajos.getJSONObject(i).getString("catLeg").charAt(0);
                subCat = rLegajos.getJSONObject(i).getString("subCat").charAt(0);
                String des = rLegajos.getJSONObject(i).getString("des");
                Integer codAspOri = rLegajos.getJSONObject(i).getInt("codAspOri");
                
                //Datos del archivo
                nomLeg = rLegajos.getJSONObject(i).optString("nom");
                arcJSON = rLegajos.getJSONObject(i).optJSONObject("archivo");
                
                if(arcJSON != null && arcJSON.length() > 0){
                    file = new FileJsonObject(arcJSON, ficEscId + "_legajo_");
                }else{
                    return WebResponse.crearWebResponseError("No se pudo leer el archivo, datos incorrectos");  
                }

                legajoPersonal = new LegajoPersonal(new FichaEscalafonaria(ficEscId), nomLeg, new Date(), Legajo.LEGAJO_PATH, des, catLeg, subCat, codAspOri);
                legajos.add(legajoPersonal);
                archivos.add(file);
            }
            
        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE,"Datos nuevo legajo personal",e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }
        /*
        *  Parte para la operacion en la Base de Datos
         */
        LegajoPersonalDao legajoPersonalDao = (LegajoPersonalDao) FactoryDao.buildDao("se.LegajoPersonalDao");
        try {
            Integer centinela = 0;
            for(LegajoPersonal leg:legajos) {
                legajoPersonalDao.insert(leg);
                if( archivos.get(centinela) != null){
                    leg.setNom(archivos.get(centinela).concatName("" + leg.getLegPerId()) );
                    BuildFile.buildFromBase64(Legajo.LEGAJO_PATH, leg.getNom(), archivos.get(centinela).getData());
                }
                legajoPersonalDao.update(leg);
                centinela++;
            }
            
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar legajos",e);
            System.out.println(e);
        }
        
        //Fin       

        /*
        *  Repuesta Correcta
         */
        JSONArray myArray = new JSONArray();
        for(LegajoPersonal leg:legajos) {
            JSONObject oResponse = new JSONObject();
            oResponse.put("nom", leg.getNom());
            oResponse.put("url", leg.getUrl());
            oResponse.put("fecIng", leg.getFecIng());
            oResponse.put("catLeg", leg.getCatLeg());
            oResponse.put("subCat", leg.getSubCat());
            
            myArray.put(oResponse);
        }
        
                
        return WebResponse.crearWebResponseExito("El registro de los legajos se realizo correctamente", myArray);
        //Fin
        
    }
    
}
