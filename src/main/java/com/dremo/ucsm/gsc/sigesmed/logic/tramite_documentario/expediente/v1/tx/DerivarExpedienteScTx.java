/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.UsuarioDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.std.HistorialExpedienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.DocumentoExpediente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.Expediente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.HistorialExpediente;
import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.BuildCodigo;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import com.dremo.ucsm.gsc.sigesmed.util.FirmaDigital;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author G-16
 */
public class DerivarExpedienteScTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        List<FileJsonObject> listaArchivos = new ArrayList<FileJsonObject>();
        
        List<HistorialExpediente> historiales = new ArrayList<HistorialExpediente>();
        List<HistorialExpediente> historialesNuevos = new ArrayList<HistorialExpediente>();
        
        HistorialExpediente actual = null;
        
        HistorialExpedienteDao historialDao = (HistorialExpedienteDao)FactoryDao.buildDao("std.HistorialExpedienteDao");
      
        UsuarioDaoHibernate userdao = new UsuarioDaoHibernate(); 
        Usuario user;
        String passCert;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            JSONArray historialData = requestData.getJSONArray("historiales");
            int areaID = requestData.getInt("areaID");
            int usuarioID = requestData.getInt("usuarioID");
            String codigo = requestData.getString("codigo");
            String observacion = requestData.optString("observacion");
            JSONArray listaDocumentos = requestData.optJSONArray("documentos");
            passCert = requestData.getString("passCert");
            
            int responsableId = requestData.getInt("UsuarioD");
            user = userdao.buscarPorId(responsableId);
            
            if( !(user.getPasCert().equals(passCert )))
               return WebResponse.crearWebResponseError("ERROR nose se pudo deribar , contraseña incorrecta");
            
            Date hoy = new Date();
            
            for( int j = 0 ; j < historialData.length(); j++){
                JSONObject bo = historialData.getJSONObject(j);
                HistorialExpediente actual2 = new HistorialExpediente(bo.getInt("historialID"),bo.getInt("expedienteID"));
                actual2.setFecAte(hoy);
                actual2.setObservacion(observacion);
                actual2.setEstadoId(EstadoExpediente.DERIVADO);
                historiales.add(actual2);                
                
                historialesNuevos.add( new HistorialExpediente(bo.getInt("historialID")+1,new Expediente(bo.getInt("expedienteID")),"",EstadoExpediente.NUEVO, areaID, usuarioID,hoy ) );
            }
            
            actual = historiales.get(0);
            
            if(listaDocumentos.length() > 0){
                int numDoc = historialDao.numeroDocumentos(actual.getExpediente().getExpId()) + 1;
                actual.setDocumentos(new ArrayList<DocumentoExpediente>());
                for(int i = 0; i < listaDocumentos.length();i++){
                    JSONObject bo =listaDocumentos.getJSONObject(i);

                    String nombreArchivo = "";
                    String documentoDescripcion = bo.getString("descripcion");
                    int tipoDocumentoId = bo.getInt("tipoDocumentoID");
                    
                    //verificamos si existe un archivo adjunto al requisito
                    JSONObject jsonArchivo = bo.optJSONObject("archivo");
                    if( jsonArchivo !=null && jsonArchivo.length() > 0 ){
                        FileJsonObject miF = new FileJsonObject( jsonArchivo ,codigo+"_doc_int_"+BuildCodigo.cerosIzquierda(numDoc + i,2));
                        nombreArchivo = miF.getName();
                        listaArchivos.add(miF);
                    }
                    actual.getDocumentos().add( new DocumentoExpediente(numDoc + i, actual.getExpediente(),documentoDescripcion,nombreArchivo,tipoDocumentoId,actual.getHisExpId()) );
                }
            }
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo derviar los historiales, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        try{
            historialDao.actualizarEstadoVarios(historiales,EstadoExpediente.DERIVADO);
            historialDao.insertarEstadoVarios(historialesNuevos);
            historialDao.insertarDocumentos(actual.getDocumentos());
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo derivar los historiales", e.getMessage() );
        }
        
        
         //si ya se registro el tipo de tramite 
        //ahora creamos los archivos que se desean subir
        
       
           
        for(FileJsonObject archivo : listaArchivos){
            String clave="";
            BuildFile.buildFromBase64("expediente/salientes", archivo.getName(), archivo.getData());
           
        }
    
        /*
        *  Repuesta Correcta
        */    
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        
        JSONArray aHistoriales = new JSONArray();
        for(HistorialExpediente h: historialesNuevos){
            JSONObject oHistorial = new JSONObject();
            oHistorial.put("historialID",h.getHisExpId());
            
            //oHistorial.put("tipoTramiteID",h.getExpediente().getTipoTramiteId());
            
            oHistorial.put("fechaEnvio",sdf.format(h.getFecEnv()));
            
            //oHistorial.put("responsableID",h.getResId());
            aHistoriales.put(oHistorial);
        }
        return WebResponse.crearWebResponseExito("Se derivo correctamente el estado de los historias",aHistoriales);
        //Fin
    }
    
}
