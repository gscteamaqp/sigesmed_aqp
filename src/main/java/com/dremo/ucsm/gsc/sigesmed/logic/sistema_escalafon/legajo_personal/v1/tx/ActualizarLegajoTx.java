/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.legajo_personal.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.LegajoPersonalDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.LegajoPersonal;
import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class ActualizarLegajoTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ActualizarLegajoTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        Integer usuarioId = wr.getIdUsuario();
        return editarLegajo(data, usuarioId);
    }

    private WebResponse editarLegajo(JSONObject data, int usuarioId) {
        try{
            Integer legId = data.getInt("legId");
            String des = data.getString("des");

            LegajoPersonalDao legajoDao = (LegajoPersonalDao) FactoryDao.buildDao("se.LegajoPersonalDao");
            LegajoPersonal legajo = legajoDao.buscarPorId(legId);

            legajo.setDes(des);
            legajo.setFecIng(new Date());
            
            legajo.setFecMod(new Date());
            legajo.setUsuMod(usuarioId);
            
            String nomArchivo = data.optString("nomArchivo");
            JSONObject fileJSON = data.optJSONObject("archivo");

            if(fileJSON != null && fileJSON.length() > 0){
                if(legajo.getUrl() != null && legajo.getUrl().length() > 0){
                    File file = new File(ServicioREST.PATH_SIGESMED + File.separator + Sigesmed.UBI_ARCHIVOS + File.separator + Legajo.LEGAJO_PATH + File.separator + legajo.getNom());
                    file.delete();
                }
                
                String nomFile = legajo.getNom();
                nomFile = nomFile.substring(0, nomFile.indexOf("."));
                FileJsonObject miF = new FileJsonObject(fileJSON, nomFile);
                BuildFile.buildFromBase64(Legajo.LEGAJO_PATH, miF.getName(), miF.getData());
                legajo.setNom(miF.getName());
            }
            legajoDao.update(legajo);
            
            
            DateFormat sdo = new SimpleDateFormat("dd-MM-yyyy");
            
            JSONObject oResponse = new JSONObject();
            oResponse.put("legId", legajo.getLegPerId());
            oResponse.put("url", Sigesmed.UBI_ARCHIVOS + File.separator  + legajo.getUrl() + File.separator );
            oResponse.put("nomDoc", legajo.getNom());
            oResponse.put("existeArchivo", true);
            oResponse.put("mensaje", "El archivo existe");
            oResponse.put("des", legajo.getDes());
            oResponse.put("fecReg", sdo.format(legajo.getFecIng()));
            oResponse.put("catLeg", legajo.getCatLeg());
            oResponse.put("subCat", legajo.getSubCat());
            oResponse.put("subCatDes", "");
            oResponse.put("codAspOrg", legajo.getCodAspOri());

            return WebResponse.crearWebResponseExito("Se actualizo correctamente el legago", oResponse);
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizarLegajo",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar el legajo");
        }
    }
    
}
