/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.plan_estudios.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.PlanEstudiosDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.MetaAtencionModel;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
/**
 *
 * @author abel
 */
public class ListarMetasAtencionPorOrganizacionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        int orgId = 0;        
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            
            orgId = requestData.getInt("organizacionID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo encontrar las metas de atencion, datos incorrectos", e.getMessage() );
        }
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<MetaAtencionModel> metas = null;
        try{
            PlanEstudiosDao planDao = (PlanEstudiosDao)FactoryDao.buildDao("mech.PlanEstudiosDao");
            
            metas = planDao.listarMetasAtencionPorOrganizacion(orgId);
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo encontrar las metas de atencion", e.getMessage() );
        }
        
        JSONArray años = new JSONArray();
        int nivel = 0;
        
        JSONArray aMet = null;
        for(MetaAtencionModel m : metas){
            
            if(nivel != m.nivelID){
                aMet = new JSONArray();
                JSONObject oAño = new JSONObject(); 
                oAño.put("nivelID", m.nivelID);
                oAño.put("metas", aMet);                
                años.put(oAño);
                
                nivel = m.nivelID;
            }
            
            JSONObject oMet = new JSONObject();            
            oMet.put("ID", m.ID );
            oMet.put("año", (""+m.año).substring(0,4) );
            oMet.put("gradoID", m.gradoID );
            oMet.put("meta", m.num1 );
            oMet.put("meta2", m.num2 );
            aMet.put(oMet);            
        }
        return WebResponse.crearWebResponseExito("Se encontro las metas de atencion",años);
    }    
    
    
}
