/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scp;

/**
 *
 * @author Administrador
 */

import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.TipoPropiedadDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.TipoPropiedad;



public class TipoPropiedadDAOHibernate  extends GenericDaoHibernate<TipoPropiedad> implements TipoPropiedadDAO {

    @Override
    public List<TipoPropiedad> listarTiposPropiedad() {
        
        List<TipoPropiedad> tipos_propiedades = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT prop FROM TipoPropiedad prop WHERE prop.est_reg!='E'";

            Query query = session.createQuery(hql); 
            tipos_propiedades = query.list();
            
        }
        catch(Exception e){
         System.out.println("No se pudo Mostrar el Catalogo de lista de Propiedades \\n "+ e.getMessage());
             throw new UnsupportedOperationException("No se pudo Mostrar el Catalogo de Lista de Propiedades \\n "+ e.getMessage());
        }
        finally{
            session.close();
        }
        return tipos_propiedades;  
        
     //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
