package com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.CronogramaSedeCapacitacion;

public interface CronogramaSedeCapacitacionDao extends GenericDao<CronogramaSedeCapacitacion> {
    CronogramaSedeCapacitacion buscarCronogramaPorSede(int sedId);
}
