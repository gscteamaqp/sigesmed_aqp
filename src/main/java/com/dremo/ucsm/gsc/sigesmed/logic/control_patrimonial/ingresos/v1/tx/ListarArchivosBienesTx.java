/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import java.util.List;

import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BienesMuebles;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.BienesMueblesDAO;
import java.text.SimpleDateFormat;
/**
 *
 * @author Administrador
 */
public class ListarArchivosBienesTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        JSONArray miArray = new JSONArray();
        
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        
        try{
            List<BienesMuebles> bie_mue = null;
            JSONObject requestData = (JSONObject)wr.getData();
            int org_id = requestData.getInt("org_id");
            BienesMueblesDAO bie_mue_dao = (BienesMueblesDAO)FactoryDao.buildDao("scp.BienesMueblesDAO");
            bie_mue = bie_mue_dao.listarBienesMuebles(org_id);
            
            for(BienesMuebles bm : bie_mue){
                JSONObject oResponse = new JSONObject();
                oResponse.put("des_bie",bm.getDes_bie());
                oResponse.put("fec_reg_bie",formatter.format(bm.getFec_reg()));
                oResponse.put("nom_arch_bie",bm.getRut_doc_bie());
                oResponse.put("nombre_archivo","archivos/"+"control_patrimonial"+"/"+bm.getRut_doc_bie());
                miArray.put(oResponse);
            
            }
        }catch(Exception e){
               System.out.println("No se pudo Mostrar los Archivos Adjuntos\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Mostrar los Archivos Adjuntos ", e.getMessage() ); 
       
        }
              return WebResponse.crearWebResponseExito("Se Listo los Archivos Correctamente",miArray); 
 
    //    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
}
