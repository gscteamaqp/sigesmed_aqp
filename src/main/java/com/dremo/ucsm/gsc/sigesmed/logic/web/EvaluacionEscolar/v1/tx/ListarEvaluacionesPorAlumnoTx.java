/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.EvaluacionEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaEvaluacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.RespuestaEvaluacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static org.hibernate.Hibernate.isInitialized;

/**
 *
 * @author abel
 */
public class ListarEvaluacionesPorAlumnoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int planID = 0;
        int alumnoID = 0;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            
            planID = requestData.getInt("planID");
            alumnoID = requestData.getInt("alumnoID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listar las evaluaciones por alumno, datos incorrectos", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<BandejaEvaluacion> evaluaciones = null;
        EvaluacionEscolarDao evaluacionDao = (EvaluacionEscolarDao)FactoryDao.buildDao("web.EvaluacionEscolarDao");
        try{
            evaluaciones = evaluacionDao.buscarEvaluacionesPorAlumno(planID,alumnoID);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las evaluaciones por alumno"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar las evaluaciones por alumno", e.getMessage() );
        }
        //Fin        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        List<Integer> evaluacionesFueraTiempo = new ArrayList<Integer>();
        int i = 0;
        
        Date hoy = new Date();//fecha y hora actual
        for(BandejaEvaluacion evaluacion:evaluaciones ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("bandejaEvaluacionID",evaluacion.getBanEvaId() );
            oResponse.put("nota",evaluacion.getNota());
            
            oResponse.put("planID",evaluacion.getEvaluacionEscolar().getPlaEstId() );
            oResponse.put("evaluacionID",evaluacion.getEvaluacionEscolar().getEvaEscId() );
            oResponse.put("nombre",evaluacion.getEvaluacionEscolar().getNom());
            oResponse.put("descripcion",evaluacion.getEvaluacionEscolar().getDes());
            oResponse.put("calificacion",evaluacion.getEvaluacionEscolar().getSisCal());
            oResponse.put("automatico",evaluacion.getEvaluacionEscolar().getAut());
            oResponse.put("aleatorio",evaluacion.getEvaluacionEscolar().getOrdAle());
            oResponse.put("intentos",evaluacion.getEvaluacionEscolar().getNumInt());
            oResponse.put("docenteID",evaluacion.getEvaluacionEscolar().getUsuMod());
            oResponse.put("areaID",evaluacion.getEvaluacionEscolar().getAreCurId());
            oResponse.put("fechaEnvio",sf.format(evaluacion.getEvaluacionEscolar().getFecEnv()) );
            oResponse.put("fechaInicio",sf.format(evaluacion.getEvaluacionEscolar().getFecIni()) );
            oResponse.put("fechaFin",sf.format(evaluacion.getEvaluacionEscolar().getFecFin()) );
            
            JSONArray aRespuestas = new JSONArray();
            if(isInitialized(evaluacion.getRespuestas())!=false){
                for(RespuestaEvaluacion respuesta : evaluacion.getRespuestas()){
                    JSONObject oRespuesta = new JSONObject();
                    oRespuesta.put("respuestaID",respuesta.getResEvaId() );
                    oRespuesta.put("respuesta",respuesta.getRespuesta());
                    oRespuesta.put("puntos",respuesta.getPuntos());
                
                    aRespuestas.put(oRespuesta);
                }            
                oResponse.put("respuestas", aRespuestas);  
            }
            else{
                oResponse.put("respuestas","");
            }

            if(evaluacion.getFecEnt()!=null)
                oResponse.put("fechaEntrega",sf.format(evaluacion.getFecEnt()) );
            if(evaluacion.getFecVis()!=null)
                oResponse.put("fechaRevicion",sf.format(evaluacion.getFecVis()) );
            
            //verificamos que las evaluaciones esten en el tiempo indicado para ser resueltas
            if(evaluacion.getEstado()==Evaluacion.ESTADO_NUEVO && hoy.compareTo(evaluacion.getEvaluacionEscolar().getFecFin()) > 0 ){
                evaluacionesFueraTiempo.add(evaluacion.getBanEvaId());
                oResponse.put("estado",""+Evaluacion.ESTADO_FUERA_TIEMPO);
            }
            else
                oResponse.put("estado",""+evaluacion.getEstado());
            
            oResponse.put("i",i++);
            
            miArray.put(oResponse);
        }
        
        //actualizando el estado de las evaluaciones a finalizado
        if(evaluacionesFueraTiempo.size()>0)
            evaluacionDao.evaluacionesFueraDeTiempo(evaluacionesFueraTiempo);
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente las evaluaciones por alumno",miArray);        
        //Fin
    }
    
}

