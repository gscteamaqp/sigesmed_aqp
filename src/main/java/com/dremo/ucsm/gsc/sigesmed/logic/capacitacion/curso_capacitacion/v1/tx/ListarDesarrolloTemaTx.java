package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.DesarrolloTemaCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.AdjuntoTemaCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.DesarrolloTemaCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class ListarDesarrolloTemaTx implements ITransaction {
    
    private static final Logger logger = Logger.getLogger(ListarDesarrolloTemaTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();
            DesarrolloTemaCapacitacionDao desarrolloTemaCapacitacionDao = (DesarrolloTemaCapacitacionDao) FactoryDao.buildDao("capacitacion.DesarrolloTemaCapacitacionDao");
            List<DesarrolloTemaCapacitacion> desarrollos = desarrolloTemaCapacitacionDao.buscarPorTema(data.getInt("cod"));
            JSONObject desObject = new JSONObject();
            
            JSONArray contenidos = new JSONArray();
            JSONArray tareas = new JSONArray();
            JSONArray evaluaciones = new JSONArray();
            JSONArray videos = new JSONArray();
            
            for(DesarrolloTemaCapacitacion desarrollo: desarrollos) {
                JSONObject object = new JSONObject();
                object.put("cod", desarrollo.getDesTemCapId());
                object.put("nom", desarrollo.getNom());
                object.put("tip", desarrollo.getTip());
                
                JSONArray arrayAttachment = new JSONArray();

                for(AdjuntoTemaCapacitacion attachment: desarrollo.getAdjuntos()) {
                    JSONObject objectAttachment = new JSONObject();
                    objectAttachment.put("cod", attachment.getAdjTemCapId());
                    String nomFile = attachment.getNom();
                    if(attachment.getTipo()!='L'){
                        
                        nomFile = nomFile.substring(nomFile.lastIndexOf("_adj_") + 6);
                        nomFile = nomFile.substring(nomFile.indexOf("_") + 1);
                        nomFile = nomFile.substring(0, nomFile.lastIndexOf(".")); 
                        objectAttachment.put("nom", nomFile);
                        objectAttachment.put("url", data.getString("url") + Sigesmed.UBI_ARCHIVOS + HelpTraining.attachments_D_Address + attachment.getNom());
                    }
                    else{
                        objectAttachment.put("nom", nomFile);
                        objectAttachment.put("url", nomFile);
                    }
                     
                    objectAttachment.put("tipo",Character.toString(attachment.getTipo()));

                    arrayAttachment.put(objectAttachment);
                }

                object.put("adj", arrayAttachment);
                
                switch(desarrollo.getTip()) {
                    case 'C':   contenidos.put(object);
                                break;
                        
                    case 'E':   evaluaciones.put(object);
                                break;
                        
                    case 'T':   tareas.put(object);
                                break;
                        
                    case 'V':   videos.put(object);
                                break;
                }
            }
            
            desObject.put("cont", contenidos);
            desObject.put("eval", evaluaciones);
            desObject.put("tare", tareas);
            desObject.put("vide", videos);
            
            return WebResponse.crearWebResponseExito("Se listo correctamente",WebResponse.OK_RESPONSE).setData(desObject);
        } catch (Exception e) {
            logger.log(Level.SEVERE,"ListarDesarrolloTemaTx",e);
            return WebResponse.crearWebResponseError("Error al listar el desarrollo del tema",WebResponse.BAD_RESPONSE);
        }
    }
}
