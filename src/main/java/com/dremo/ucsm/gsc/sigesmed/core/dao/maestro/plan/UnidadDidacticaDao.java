package com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.PeriodosPlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.*;

import java.util.List;

/**
 * Created by Administrador on 26/10/2016.
 */
public interface UnidadDidacticaDao extends GenericDao<UnidadDidactica> {
    UnidadDidactica buscarUnidadDidactica(int idUnidad, String tip);
    UnidadDidactica buscarUnidadDidactica(int idUnidad);
    UnidadDidactica buscarUnidadDidacticaPeriodo(int idUnidad);
    void eliminarUnidadDidactica(int idUnidad);
    void editarUnidadDidactica(UnidadDidactica newUnidad);
    List<UnidadDidactica> buscarUnidadesPorPlan(int idPlan);
    List<UnidadDidactica> buscarUnidadesPorPlanConDatos(int idPlan);
    List<UnidadDidactica> listarUnidadesPorDocenteYOrganizacion(int idOrg,int idDoc);
    void registrarCapacidades(CompetenciasUnidadDidactica competencia);
    List<CompetenciasUnidadDidactica> listarCompetenciasUnidad(int idUnidad);
    List<IndicadorAprendizaje> listarIndicadoresUnidadPorCapacidad(int idUnidad,int idComp,int idCap);
    List<IndicadorAprendizaje> listarIndicadoresUnidad(int idUnidad);
    void eliminarCompetenciasUnidad(int idUnidad,int idComp);
    void eliminarCompetenciasUnidad(int idUnidad,int idComp,int idCap);
    void eliminarIndicadorUnidadDidactica(int unidadId,int compId,int capId,int idIndicador);
    void updateCompetenciaUnidadDidactica(CompetenciasUnidadDidactica comUnid);
    CompetenciasUnidadDidactica buscarCompetenciaUnidad(int idUnidad, int idComp, int idCap);
    List<CompetenciasUnidadDidactica> buscarCompetenciasUnidad(int idUnidad);
    MaterialRecursoProgramacion buscarMaterialPorNom(String nom);

    List<MaterialRecursoProgramacion> listarMateriales(int idUnidad);
    MaterialRecursoProgramacion buscarMaterial(int idMaterial);
    MaterialRecursoProgramacion registrarMaterial(MaterialRecursoProgramacion material);
    void eliminarMaterial(int idMaterial);
    MaterialRecursoProgramacion editarMaterial(MaterialRecursoProgramacion material);
    List<ProductosUnidadAprendizaje> listarProductosUnidad(int idUnidad);
    ProductosUnidadAprendizaje registrarProductoUnidad(ProductosUnidadAprendizaje producto);
    void eliminarProductoUnidad(int idProducto);
    void editarProductoUnidad(ProductosUnidadAprendizaje producto);

    List<UnidadDidactica> listarUnidadesArea(int idPlan,int idDoc, int idArea,int idGrado);
    List<PeriodosPlanEstudios> listarPeridosPLanEstudios(int idPlan,int idNivel,char tipPer);
    PeriodosPlanEstudios buscarPeriodoEvaluacion(int idPeriodo);
}
