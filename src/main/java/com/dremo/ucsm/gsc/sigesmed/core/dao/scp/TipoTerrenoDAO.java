/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.scp;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import java.util.Date;
import java.util.List;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.TipoTerreno;


/**
 *
 * @author Administrador
 */
public interface TipoTerrenoDAO extends GenericDao<TipoTerreno> {
    
    public List<TipoTerreno> listarTiposTerreno(); 
}
