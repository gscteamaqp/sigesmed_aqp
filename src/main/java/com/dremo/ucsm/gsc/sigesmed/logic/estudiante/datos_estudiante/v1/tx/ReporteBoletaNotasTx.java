/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.estudiante.datos_estudiante.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mpf.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.AreaExonerada;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.AreaModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.GradoIeEstudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.Matricula;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.RegistroAuxiliarCompetencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.RegistroAuxiliarCompetenciaModel;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.DateUtil;
import com.dremo.ucsm.gsc.sigesmed.util.GCell;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Carlos
 */
public class ReporteBoletaNotasTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {

        Integer org;
        Long matriculaId;
        String alumno="";
        JSONObject requestData = (JSONObject) wr.getData();

        try {
            org = requestData.getInt("organizacionID");
            matriculaId = requestData.getLong("matriculaID");
            alumno=requestData.getString("persona");

        } catch (Exception e) {
            System.out.println("No se pudo verificar los datos \n" + e);
            return WebResponse.crearWebResponseError("No se pudo verificar los datos ", e.getMessage());
        }

        OrganizacionDao organizacionDao = (OrganizacionDao) FactoryDao.buildDao("OrganizacionDao");
        Organizacion organizacion = null;
        EstudianteDao estudianteDao = (EstudianteDao) FactoryDao.buildDao("mpf.EstudianteDao");
        List<AreaExonerada> exoneraciones=null;
        List<AreaModel> areas = null;
        GradoIeEstudiante gradoIee = null;
        List<RegistroAuxiliarCompetencia> registroNotas = null;
        Matricula estudiante = null;
        try {
            organizacion = organizacionDao.load(Organizacion.class, org);
            gradoIee = estudianteDao.getGradoIeeActual(new Matricula(matriculaId));
            registroNotas = estudianteDao.listarNotasByCompetencias(gradoIee);
            
            estudiante=estudianteDao.getDatosMatriculaAndPlan(matriculaId);
            exoneraciones=estudianteDao.listarCursosExonerados(estudiante.getEstudiante());
            List<Integer> exoIds = new ArrayList<>();
            if(exoneraciones!=null && exoneraciones.size()>0)
            {
                for (AreaExonerada exo : exoneraciones) {
                    exoIds.add(exo.getId());
                }
           
                areas = estudianteDao.buscarAreasByPlanEstudios(estudiante.getPlanNivel().getPlaEstId(), estudiante.getGradoId(), estudiante.getSeccionId(),exoIds);
            } 
            else
            {
                areas = estudianteDao.buscarAreasByPlanEstudios(estudiante.getPlanNivel().getPlaEstId(), estudiante.getGradoId(), estudiante.getSeccionId());
            }
            
        } catch (Exception e) {
            System.out.println("No se pudo listar las Notas \n" + e);
            return WebResponse.crearWebResponseError("No se pudo listar las Notas ", e.getMessage());
        }

        List<RegistroAuxiliarCompetenciaModel> report = new ArrayList<>();
        String[] notas = new String[]{"", "", "", ""};//4 periodos de plan de estudios

        RegistroAuxiliarCompetenciaModel nuevo = new RegistroAuxiliarCompetenciaModel(registroNotas.get(0).getComp().getNom(), notas, registroNotas.get(0).getAreaCurricular().getNom(),registroNotas.get(0).getComp().getComId(),registroNotas.get(0).getAreaCurricular().getAreCurId());
        report.add(nuevo);
        for (RegistroAuxiliarCompetencia a : registroNotas) {
            int competencia = a.getComp().getComId();
            
            boolean flag = true;
            for (RegistroAuxiliarCompetenciaModel b : report) {
                if (competencia==b.getCompetenciaId()) {
                    b.setNota(a.getNota(), a.getPeriodoPlanEstudios().getEta() - 1);
                    flag = false;
                } else if (flag && report.get(report.size() - 1) == b) {
                    RegistroAuxiliarCompetenciaModel nuevo_ = new RegistroAuxiliarCompetenciaModel();//(a.getComp().getNom(), a.getNota(), a.getPeriodoPlanEstudios().getEta(), a.getAreaCurricular().getNom());
                    nuevo_.setArea(a.getAreaCurricular().getNom());
                    nuevo_.setCompetencia(a.getComp().getNom());
                    nuevo_.setNota(new String[]{"", "", "", ""});
                    nuevo_.setNota(a.getNota(), a.getPeriodoPlanEstudios().getEta() - 1);
                    nuevo_.setAreaId(a.getAreaCurricular().getAreCurId());
                    nuevo_.setCompetenciaId(a.getComp().getComId());
                    report.add(nuevo_);
                    break;
                }
            }

        }

        List<List<RegistroAuxiliarCompetenciaModel>> libreta=new ArrayList<List<RegistroAuxiliarCompetenciaModel>>();
        List<RegistroAuxiliarCompetenciaModel> curso= new ArrayList<>();
        int cursoId=report.get(0).getAreaId();
        for(RegistroAuxiliarCompetenciaModel rg:report)
        {
            if(cursoId==rg.getAreaId())
            {
                curso.add(rg);
                cursoId=rg.getAreaId();
            }
            else
            {
                libreta.add(curso);
                curso=new ArrayList<>();
                curso.add(rg);
            }
        }
        libreta.add(curso);
        

        
        
        //Creando el reporte....        
        Mitext m = null;
        try {
            m = new Mitext(false);
            m.agregarTitulo("BOLETA DE NOTAS "+DateUtil.obtenerAnioActual());
        } catch (Exception ex) {
            System.out.println("No se pudo crear el documento \n" + ex);
            Logger.getLogger(ReporteBoletaNotasTx.class.getName()).log(Level.SEVERE, null, ex);
        }

        float[] columnWidthsD = {6, 6, 3};
        Table tabla = new Table(columnWidthsD);
        tabla.setWidthPercent(100);
        tabla.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph("INSTITUCION EDUCATIVA").setFontSize(10).setTextAlignment(TextAlignment.LEFT)));
        tabla.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph(" : " + organizacion.getNom()).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER));

        tabla.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph("COD").setFontSize(10).setTextAlignment(TextAlignment.LEFT)));
        tabla.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph(" : " + organizacion.getCod()).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER));

        tabla.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph("DIRECCION").setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph(" : " + organizacion.getDir()).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER));

        tabla.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph("GRADO").setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph(" : " + gradoIee.getGrado().getNom()).setFontSize(11)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER));

        tabla.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph("SECCION").setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph(" : " + gradoIee.getSeccion().getNom()).setFontSize(11)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER));
        
        tabla.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph("ALUMNO").setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph(" : " + alumno).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER));

        float[] titlesBlack = new float[]{3, 6, 1, 1, 1, 1, 1, 1};//15
        GTabla t = new GTabla(titlesBlack);
        
        try {
            
            t.build("AREA", 8, 2, 1);
            t.build("LOGROS DE APRENDIZAJE", 8, 2, 1);
            t.build("PERIODO", 8, 1, 4);
            t.build("Cal.Prom.de Area", 8, 2, 1);
            t.build("Eva.de Recuperacion", 8, 2, 1);
            t.build("1", 8, 1, 1);
            t.build("2", 8, 1, 1);
            t.build("3", 8, 1, 1);
            t.build("4", 8, 1, 1);

            t.setWidthPercent(100);
        } catch (IOException ex) {
            Logger.getLogger(ReporteBoletaNotasTx.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.print("libreta total de cursos: " + libreta.size());
        for (List<RegistroAuxiliarCompetenciaModel> cursoModel : libreta) {
            GCell[] cell = new GCell[3+(5*(cursoModel.size()+1))];
            cell[0]=t.createCellCenter(cursoModel.size()+1, 1);//area
            cell[1]= t.createCellLeft(1, 1,true);//competencias
            cell[2]= t.createCellCenter(1, 1);//nota1
            cell[3]= t.createCellCenter(1, 1);//nota2
            cell[4]= t.createCellCenter(1, 1);//nota3
            cell[5]= t.createCellCenter(1, 1);//nota4
            cell[6]=t.createCellCenter(cursoModel.size()+1, 1,true);//promedio gen
            cell[7]=t.createCellCenter(cursoModel.size()+1, 1,true);//recuperacion
            int lm=0;
            int j=0;
            for(j=8;j<cell.length-5;j++)
            {
                if(lm%5==0)
                    cell[j]= t.createCellLeft(1, 1);
                else
                    cell[j]= t.createCellCenter(1, 1);
                lm++;
            }
            cell[j++]= t.createCellLeft(1, 1,true);
            cell[j++]= t.createCellCenter(1, 1,true);
            cell[j++]= t.createCellCenter(1, 1,true);
            cell[j++]= t.createCellCenter(1, 1,true);
            cell[j]= t.createCellCenter(1, 1,true);
            
            
            String fila_data[] = new String[3+(5*(cursoModel.size()+1))];
            fila_data[0] = cursoModel.get(0).getArea();
            
            fila_data[1] = cursoModel.get(0).getCompetencia();
            fila_data[2] = cursoModel.get(0).getNota(0);
            fila_data[3] = cursoModel.get(0).getNota(1);
            fila_data[4] = cursoModel.get(0).getNota(2);
            fila_data[5] = cursoModel.get(0).getNota(3);
            fila_data[6] = "";//nota evaluacion final
            fila_data[7] = "";//nota recuperacion
            int k=8;
            for (int l=1;l<cursoModel.size();l++) {
                fila_data[k++] = cursoModel.get(l).getCompetencia();
                fila_data[k++] = cursoModel.get(l).getNota(0);
                fila_data[k++] = cursoModel.get(l).getNota(1);
                fila_data[k++] = cursoModel.get(l).getNota(2);
                fila_data[k++] = cursoModel.get(l).getNota(3);
                
            }
            fila_data[k++] = "CALIFIC. DE PROMEDIO DE AREA";
            String notaProm1=estudianteDao.getNotaAreaByPeriodo(gradoIee.getGraOrgEstId(), cursoModel.get(0).getAreaId(), 1);
            fila_data[k++] = notaProm1!=null?notaProm1:"";
            String notaProm2=estudianteDao.getNotaAreaByPeriodo(gradoIee.getGraOrgEstId(), cursoModel.get(0).getAreaId(), 2);
            fila_data[k++] = notaProm2!=null?notaProm2:"";
            String notaProm3=estudianteDao.getNotaAreaByPeriodo(gradoIee.getGraOrgEstId(), cursoModel.get(0).getAreaId(), 3);
            fila_data[k++] = notaProm3!=null?notaProm3:"";
            String notaProm4=estudianteDao.getNotaAreaByPeriodo(gradoIee.getGraOrgEstId(), cursoModel.get(0).getAreaId(), 4);
            fila_data[k] = notaProm4!=null?notaProm4:"";
            
            t.processLineCell(fila_data, cell, 8);
        }
        
        m.agregarTabla(tabla);
        m.agregarParrafo("");
        m.agregarTabla(t);
        m.cerrarDocumento();

        JSONArray miArray = new JSONArray();
        JSONObject oResponse = new JSONObject();
        oResponse.put("datareporte", m.encodeToBase64());
        miArray.put(oResponse);

        return WebResponse.crearWebResponseExito("Se genero el reporte correctamente", miArray);

    }

}
