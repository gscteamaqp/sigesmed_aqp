/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.sad;

import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.ArchivosInventarioTransferencia;
import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jeferson
 */
public interface ArchivoInventarioDAO extends GenericDao<ArchivosInventarioTransferencia>{
    
    public List<ArchivosInventarioTransferencia> buscarPorCodigo(int codigo);
    public List<ArchivosInventarioTransferencia> listarArchivos(int codigo);
    public List<ArchivosInventarioTransferencia> listarArchivosSerie(int codSerie);
}
