/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.UsuarioDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.std.ExpedienteDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.FirmaDigital;
import java.io.File;

import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author G-16
 */
public class FirmaDocumentosTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {

        /*
        *   Parte para la lectura, verificacion y validacion de datos
         */


        List<String> docs = null;
        ExpedienteDaoHibernate expediente = new ExpedienteDaoHibernate();

        UsuarioDaoHibernate userdao = new UsuarioDaoHibernate();
        Usuario user;
        String passCert;
        String codigo;
        String rutacertificado;
        try {
            JSONObject requestData = (JSONObject) wr.getData();

            codigo = requestData.getString("codigoExp");
            passCert = requestData.getString("passCert");
            int responsableId = requestData.getInt("UsuarioD");
            user = userdao.buscarPorId(responsableId);
            
           //verificando q exista un certificado para el responsable
            if (user.getNomCert()== null){
                return WebResponse.crearWebResponseError("NO AY UN CERTIFICADO PARA ESTE USUARIO" );
            }
             //verificando ruta del certificado
           rutacertificado = ServicioREST.PATH_SIGESMED+ File.separator +"archivos"+File.separator+"expediente"+File.separator+user.getNomCert() ;
           if(FirmaDigital.verificar(rutacertificado) == false){
                    return WebResponse.crearWebResponseError("CERTIFICADO NO ENCONTRADO" );
           }
            //leendo los documentos           
        } catch (Exception e) {
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo finalizar el expediente, datos incorrectos", e.getMessage());
        }

        docs = expediente.buscarDocumentosPorExpedienteC(codigo);
        System.out.println(docs);
        JSONArray miArray = new JSONArray();
        
        for (String documento : docs) {
         
            String clave="";
            
            miArray.put(documento);
          // String rutacertificado = ServicioREST.PATH_SIGESMED+ File.separator +"archivos"+File.separator+"expediente"+File.separator+user.getNomCert() ;
           
           String rutapdf = ServicioREST.PATH_SIGESMED+ File.separator +"archivos"+File.separator+"expediente"+File.separator +File.separator + documento;
           File af = new File(rutapdf);
        //   System.out.println(user.getPasCert());
           if (af.exists()) { 
            //clave = user.getPasCert();
            clave = passCert;
            String pathPDFM = FirmaDigital.sign(rutapdf, rutacertificado,clave);
            FirmaDigital.limpiar(pathPDFM,rutapdf);
            FirmaDigital.reenombrar(pathPDFM,rutapdf);
           }
           else {
              rutapdf = ServicioREST.PATH_SIGESMED+ File.separator +"archivos"+File.separator+"expediente"+File.separator+"salientes"+File.separator + documento;
//           clave = user.getPasCert();
             clave = passCert;
            String pathPDFM = FirmaDigital.sign(rutapdf, rutacertificado,clave);
            if (pathPDFM != "ERROR") {
                FirmaDigital.limpiar(pathPDFM,rutapdf);
                FirmaDigital.reenombrar(pathPDFM,rutapdf);
            }else {
                WebResponse.crearWebResponseExito("ERRORDOCUMENTO NO ENCONTRADO");
            }
           }
           
        }
        /*
        *  Repuesta Correcta
         */
        return WebResponse.crearWebResponseExito("Se finalizo correctamente la lista de documentos", miArray);
        //Fin
    }

}
