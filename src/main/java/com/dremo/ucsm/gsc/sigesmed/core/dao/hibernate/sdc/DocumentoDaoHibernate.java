    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sdc;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sdc.DocumentoDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.ContenidoDocumento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.ContenidoPlantilla;

import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.Documento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.Plantilla;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Carlos
 */
public class DocumentoDaoHibernate extends GenericDaoHibernate<Documento> implements DocumentoDao {

       

    
    @Override
    public List<ContenidoPlantilla> listarContenidosPlantilla(Plantilla plantillaId) {
        
        List<ContenidoPlantilla> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar Modulos con sus Submodulos
            String hql = "SELECT p FROM ContenidoPlantilla p WHERE p.plaId=:p1 ORDER BY p.conplaId" ;    

            Query query = session.createQuery(hql);
            query.setParameter("p1", plantillaId);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Obtener contenido de la  Plantilla \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Obtener contenido de la Plantilla \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
        
    }
    
    @Override
    public List<Documento> listarDocumentos(Organizacion organizacion) {
        
        List<Documento> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar Modulos con sus Submodulos
            String hql = "SELECT p FROM Documento p JOIN FETCH p.docPlaId pl JOIN FETCH pl.plaUsu u JOIN FETCH pl.tipoDocumento tc WHERE u.organizacion=:p1 ORDER BY p.docId DESC" ;    

            Query query = session.createQuery(hql);
            query.setParameter("p1", organizacion);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Obtener contenido de la  Plantilla \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Obtener contenido de la Plantilla \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
        
    }

    @Override
    public List<ContenidoDocumento> listarContenidosDocumento(Documento documentoId) {
        
        List<ContenidoDocumento> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar Modulos con sus Submodulos
            String hql = "SELECT p FROM ContenidoDocumento p WHERE p.docId=:p1 ORDER BY p.conDocId";    

            Query query = session.createQuery(hql);
            query.setParameter("p1", documentoId);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Obtener contenido del Documento \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Obtener contenido del Documento \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
        
    }

}
