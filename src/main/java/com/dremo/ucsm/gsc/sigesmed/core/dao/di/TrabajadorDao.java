/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.di;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Trabajador;
import java.util.List;

/**
 *
 * @author Administrador
 */
public interface TrabajadorDao extends GenericDao<Trabajador>{
    public String buscarUltimoCodigo();
    public List<Trabajador> ListarxOrganizacion(int orgId);
    public List<Trabajador> ListarxOrganizacionxTipo(int orgId, String tipo);
    public List<String[]> ListarxOrganizacionxTipo(String s, int orgId, String tipo);
    public Trabajador obtenerDatosPersonales(int orgId, int perId);
    //public List<Area> buscarConOrganizacionYAreaPadre();
    //public List<Area> buscarPorOrganizacion(int organizacionID);   
}
