/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.scp;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;



/**
 *
 * @author Jeferson
 */
@Entity
@Table(name="unidad_medida", schema="administrativo")
public class UnidadMedida {
    
    @Id
    @Column(name="uni_med_id", unique= true , nullable=false)
    @SequenceGenerator(name="secuencia_det_inv_tra",sequenceName="administrativo.unidad_medida_uni_med_id_seq")
    @GeneratedValue(generator="secuencia_det_inv_tra")
    private int uni_med_id;
    
    @Column(name="nom")
    private String nom;
    
    @Column(name="fec_mod")
    private Date fec_mod;
    
    @Column(name="usu_mod")
    private int usu_mod;
    
    @Column(name="est_reg")
    private char est_reg;

    public void setUni_med_id(int uni_med_id) {
        this.uni_med_id = uni_med_id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setFec_mod(Date fec_mod) {
        this.fec_mod = fec_mod;
    }

    public void setUsu_mod(int usu_mod) {
        this.usu_mod = usu_mod;
    }

    public void setEst_reg(char est_reg) {
        this.est_reg = est_reg;
    }

    public int getUni_med_id() {
        return uni_med_id;
    }

    public String getNom() {
        return nom;
    }

    public Date getFec_mod() {
        return fec_mod;
    }

    public int getUsu_mod() {
        return usu_mod;
    }

    public char getEst_reg() {
        return est_reg;
    }

    public UnidadMedida(int uni_med_id, String nom, Date fec_mod, int usu_mod, char est_reg) {
        this.uni_med_id = uni_med_id;
        this.nom = nom;
        this.fec_mod = fec_mod;
        this.usu_mod = usu_mod;
        this.est_reg = est_reg;
    }

    public UnidadMedida() {
    }
    
    
}
