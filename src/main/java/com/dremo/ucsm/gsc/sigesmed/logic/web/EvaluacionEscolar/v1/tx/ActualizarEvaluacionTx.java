/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.EvaluacionEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.EvaluacionEscolar;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 *
 * @author abel
 */
public class ActualizarEvaluacionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */        
        
        EvaluacionEscolar nuevaEvaluacion = null;
        try{
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            JSONObject requestData = (JSONObject)wr.getData();
            int evaluacionID = requestData.getInt("evaluacionID");
            String nombre = requestData.getString("nombre");
            String descripcion = requestData.optString("descripcion");
            boolean calificacion = requestData.getBoolean("calificacion");
            boolean automatico = requestData.getBoolean("automatico");
            boolean aleatorio = requestData.getBoolean("aleatorio");
            int intentos = requestData.getInt("intentos");
            int eva_maes_id = requestData.getInt("eva_maes_id");
            int planID = requestData.getInt("planID");
            int gradoID = requestData.getInt("gradoID");
            char seccionID = requestData.getString("seccionID").charAt(0);
            int areaID = requestData.getInt("areaID");            
            Date fec_ini =  sf.parse(requestData.getString("fechaInicio")) ;
            Date fec_fin = sf.parse(requestData.getString("fechaFin"));
            Date fec_envio = sf.parse(requestData.getString("fechaEnvio"));
         
            int docenteID = requestData.getInt("docenteID");
            
            
            nuevaEvaluacion = new EvaluacionEscolar(evaluacionID,nombre,descripcion,null,null,null,calificacion,automatico,aleatorio,intentos,Evaluacion.ESTADO_NUEVO,planID,gradoID,seccionID,areaID,new Date(),docenteID,'A');
            nuevaEvaluacion.setEva_maes_id(eva_maes_id);
            nuevaEvaluacion.setFecIni(fec_ini);
            nuevaEvaluacion.setFecFin(fec_fin);
            nuevaEvaluacion.setFecEnv(fec_envio);
            
            
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo actualizar la evaluacion, datos incorrectos", e.getMessage() );
        }
        //Fin       
        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        EvaluacionEscolarDao grupoDao = (EvaluacionEscolarDao)FactoryDao.buildDao("web.EvaluacionEscolarDao");
        try{
            grupoDao.update(nuevaEvaluacion);
        }catch(Exception e){
            System.out.println("No se pudo actualizar la evaluacion\n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar la evaluacion", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("evaluacionID",nuevaEvaluacion.getEvaEscId());
        return WebResponse.crearWebResponseExito("La actualizacion de la evaluacion se realizo correctamente", oResponse);
        //Fin
    }    
    
    
}
