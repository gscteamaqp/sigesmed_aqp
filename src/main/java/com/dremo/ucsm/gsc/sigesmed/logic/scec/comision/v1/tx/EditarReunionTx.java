package com.dremo.ucsm.gsc.sigesmed.logic.scec.comision.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.ReunionComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.ReunionComision;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 05/10/2016.
 */
public class EditarReunionTx implements ITransaction {
    private static final Logger logger = Logger.getLogger(EditarReunionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        return editarReunion(data);
    }

    private WebResponse editarReunion(JSONObject data) {
        try{
            ReunionComisionDao reunionComisionDao = (ReunionComisionDao) FactoryDao.buildDao("scec.ReunionComisionDao");
            ReunionComision reunionComision = reunionComisionDao.buscarReunionPorId(data.getInt("cod"));

            String loc = data.optString("lug",reunionComision.getLugReu());
            String res = data.optString("res",reunionComision.getResConReu());
            Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            calendar.setTimeInMillis(data.optLong("fec",reunionComision.getFecReu().getTime()));
            Date fec = calendar.getTime();

            reunionComision.setLugReu(loc);
            reunionComision.setResConReu(res);
            reunionComision.setFecReu(fec);
            reunionComision.setFecMod(new Date());

            reunionComisionDao.update(reunionComision);

            return  WebResponse.crearWebResponseExito("Exito al editar la reunion",WebResponse.OK_RESPONSE);
        }catch (Exception e){
            logger.log(Level.SEVERE,"editarReunion",e);
            return WebResponse.crearWebResponseError("error al editar los datos",WebResponse.BAD_RESPONSE);
        }
    }
}
