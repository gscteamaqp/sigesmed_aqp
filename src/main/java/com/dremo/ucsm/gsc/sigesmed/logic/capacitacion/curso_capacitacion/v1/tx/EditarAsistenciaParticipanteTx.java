package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.AsistenciaParticipanteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.AsistenciaParticipante;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class EditarAsistenciaParticipanteTx implements ITransaction {
    
    private static final Logger logger = Logger.getLogger(EditarAsistenciaParticipanteTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int opt = data.getInt("opt");
        WebResponse response = null;

        switch (opt) {
            case 0:
                response = editar(data.getJSONArray("attendance"), data.getInt("usuMod"));
                break;

            case 1:
                response = justificar(data.getInt("codAsi"), data.getInt("usuMod"));
                break;
        }

        return response;
    }
    
    private WebResponse editar(JSONArray asistencias, int usuMod) {
        try {
            
            AsistenciaParticipanteDao asistenciaParticipanteDao = (AsistenciaParticipanteDao) FactoryDao.buildDao("capacitacion.AsistenciaParticipanteDao");
            JSONArray array = asistencias;

            for(int i = 0;i < array.length();i++) {
                JSONObject object = array.getJSONObject(i);
                AsistenciaParticipante attendance = asistenciaParticipanteDao.buscarPorId(object.getInt("cod"));
                
                attendance.setEstReg(object.getString("est").charAt(0));
                attendance.setFecMod(new Date());
                attendance.setUsuMod(usuMod);
                asistenciaParticipanteDao.update(attendance);
            }

            return WebResponse.crearWebResponseExito("Las asistencias de los participantes se modificaron correctamente", WebResponse.OK_RESPONSE);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "editar", e);
            return WebResponse.crearWebResponseError("No se pudo editar las asistencias de los participantes", WebResponse.BAD_RESPONSE);
        }
    }
    
    private WebResponse justificar(int codAsi, int usuMod) {
        try {
            AsistenciaParticipanteDao asistenciaParticipanteDao = (AsistenciaParticipanteDao) FactoryDao.buildDao("capacitacion.AsistenciaParticipanteDao");
            AsistenciaParticipante asistencia = asistenciaParticipanteDao.buscarPorId(codAsi);
            
            asistencia.setEstReg('J');
            asistencia.setFecMod(new Date());
            asistencia.setUsuMod(usuMod);
            asistenciaParticipanteDao.update(asistencia);
            
            return WebResponse.crearWebResponseExito("Las asistencias de los participantes se modificaron correctamente", WebResponse.OK_RESPONSE);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "justificar", e);
            return WebResponse.crearWebResponseError("No se pudo editar las asistencias de los participantes", WebResponse.BAD_RESPONSE);
        }
    }
}
