/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.matricula_individual.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.MatriculaMMIDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class GenerarFichaMatriculaExcelTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        MatriculaMMIDaoHibernate matDao = new MatriculaMMIDaoHibernate();
        try {
            JSONObject requestData = (JSONObject) wr.getData();
            long estId = requestData.getLong("estId");
            matDao.generarFichaMatriculaByEstId(estId);
            return WebResponse.crearWebResponseExito("El Reporte se Genero Correctamente");
        } catch (Exception e) {
            return WebResponse.crearWebResponseError("Error al Generar El Reporte Excel");
        }
    }
}
