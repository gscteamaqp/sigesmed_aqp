/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.horarios.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.HorarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;

import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.DiaSemana;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.HorarioCab;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.HorarioDet;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.DateUtil;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author carlos
 */
public class EliminarHorarioTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
       
        HorarioCab horarioToDelete=null;
        
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
      
            Integer hor=requestData.getInt("horarioID");
            horarioToDelete=new HorarioCab(hor);

           
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo verificar los datos", e.getMessage() );
        }
        
       
        HorarioDao horarioDao = (HorarioDao)FactoryDao.buildDao("cpe.HorarioDao");
        
        try{
            horarioDao.delete(horarioToDelete);
            
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Eliminar el Horario", e.getMessage() );
        }

        return WebResponse.crearWebResponseExito("Se Elimino correctamente");        
        //Fin
    }
    
}

