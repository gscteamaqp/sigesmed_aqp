package com.dremo.ucsm.gsc.sigesmed.logic.ma.asistencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.AsistenciaEstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.anecdotario.AnecdotarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.EstudianteAsistencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Estudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Matricula;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 11/01/2017.
 */
public class ListarAsistenciaEstudianteTx implements ITransaction {
    private static Logger logger = Logger.getLogger(ListarAsistenciaEstudianteTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idOrg = data.getInt("org");
        int idGrado = data.getInt("gra");
        char idSeccion = data.getString("secc").charAt(0);
        long fec = data.getLong("fec");
        int idArea = data.optInt("are", -1);
        return listarAsistenciaEstudiante(idOrg,idGrado,idSeccion,fec,idArea);
    }

    private WebResponse listarAsistenciaEstudiante(int idOrg, int idGrado, char idSeccion, long fec,int idArea) {
        try{
            AsistenciaEstudianteDao asisDao = (AsistenciaEstudianteDao) FactoryDao.buildDao("ma.AsistenciaEstudianteDao");
            List<EstudianteAsistencia> asistencias = idArea == -1 ?asisDao.listarAsistenciasEstudiantes(idOrg,idGrado,idSeccion,new Date(fec)) :
                    asisDao.listarAsistenciasEstudiantesArea(idOrg, idGrado, idSeccion, idArea, new Date(fec));
            JSONArray asisJSON = new JSONArray();
            if(asistencias!= null && asistencias.size() > 0 ){
                asisJSON = new JSONArray(EntityUtil.listToJSONString(
                        new String[]{"id","dni","perId","apePat","apeMat","nom","fecAsi","tipAsi","estAsi","asiArea","asiId","desJus","docJus"},
                        new String[]{"id","dni","perid","pat","mat","nom","fec","tip","est","are","asid","desjus","docjus"},
                        asistencias
                ));
            }else{
                AnecdotarioDao anecDao = (AnecdotarioDao) FactoryDao.buildDao("maestro.anecdotario.AnecdotarioDao");
                List<Estudiante> estudiantes = anecDao.listarEstudiantes(idOrg, idGrado, idSeccion);
                for(Estudiante est :estudiantes){
                    JSONObject jsonEstudiante = new JSONObject(EntityUtil.objectToJSONString(
                            new String[]{"perId","dni","nom","apePat","apeMat"},
                            new String[]{"perid","dni","nom","pat","mat"},
                            est.getPersona()
                    ));
                    Matricula matricula = anecDao.buscarMatriculaActual(est.getPersona().getPerId(),idOrg);
                    jsonEstudiante.put("id",matricula.getId());
                    asisJSON.put(jsonEstudiante);
                }

            }


            return WebResponse.crearWebResponseExito("Se listo exitosamente",asisJSON);
        }catch (Exception e){
            logger.log(Level.SEVERE,"listarAsistencia",e);
            return WebResponse.crearWebResponseError("No se puede listar");
        }
    }
}
