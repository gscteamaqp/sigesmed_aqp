/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.catalogo_bienes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.dremo.ucsm.gsc.sigesmed.util.GCell;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.border.SolidBorder;
import com.itextpdf.layout.element.Paragraph;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.SerieDocumentalDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.SerieDocumental;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;

import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.UnidadOrganicaDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.UnidadOrganica;

/**
 *
 * @author Administrador
 */
public class ReporteCatalogoTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {

        try {

            JSONArray requestData = (JSONArray) wr.getData();

            /*Cabecera del Reporte*/
            String cab1[] = {"Codigo", "Descripcion", "Unidad Medida", "Grupo", "Clase", "Familia"};
            float[] columnWidths = {2, 2, 2, 2, 2, 2};

            /*Construimos la Tabla*/
            int size = requestData.length();
            Mitext m = null;
            /*LLENAMOS LA CABECERA*/
            m = new Mitext(true, "REPORTE DE CATALOGO DE BIENES - SBN");
            GTabla t_general = new GTabla(columnWidths);
            t_general.build(cab1);
            GCell[] cell = {t_general.createCellCenter(1, 1), t_general.createCellCenter(1, 1), t_general.createCellCenter(1, 1), t_general.createCellCenter(1, 1), t_general.createCellCenter(1, 1), t_general.createCellCenter(1, 1)};

            /*.............*/
            for (int i = 0; i < size; i++) {
                JSONObject det = requestData.getJSONObject(i);
                String[] archivos_data = new String[6];
                archivos_data[0] = Integer.toString(det.getInt("cat_bie_id"));
                archivos_data[1] = det.getString("den_bie");
                archivos_data[2] = det.getString("uni_med_nom");
                archivos_data[3] = det.getString("gru_gen_nom");
                archivos_data[4] = det.getString("cla_gen_nom");
                archivos_data[5] = det.getString("fam_gen_nom");
                t_general.processLineCell(archivos_data, cell);
            }
            m.agregarTabla(t_general);

            /*Mostramos el reporte*/
            JSONArray miArray = new JSONArray();

            m.cerrarDocumento();
            JSONObject oResponse = new JSONObject();
            oResponse.put("datareporte", m.encodeToBase64());
            miArray.put(oResponse);

            return WebResponse.crearWebResponseExito("Se genero el Reporte con exito ...", miArray);

        } catch (Exception e) {
            System.out.println(e);
            return WebResponse.crearWebResponseError("ERROR AL GENERAR REPORTE ", e.getMessage());
        }

        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
