package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FuncionSistemaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.FuncionSistema;
import com.dremo.ucsm.gsc.sigesmed.core.entity.RolFuncionModel;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

public class FuncionSistemaDaoHibernate extends GenericDaoHibernate<FuncionSistema> implements FuncionSistemaDao {

    private static final Logger logger = Logger.getLogger(FuncionSistemaDaoHibernate.class.getName());

    @Override
    public List<FuncionSistema> buscarConSubModulo() {
        List<FuncionSistema> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar FuncionSistemas
            String hql = "SELECT f FROM FuncionSistema f JOIN FETCH f.subModuloSistema WHERE f.estReg!='E' ORDER BY f.funSisId" ;
            Query query = session.createQuery(hql);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las FuncionSistemas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las FuncionSistemas \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    
    @Override
    public List<FuncionSistema> buscarPorRol(int rolId) {
        List<FuncionSistema> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar FuncionSistemas
            String hql = "SELECT rf.claveRolFuncion.funcionSistema FROM RolFuncion rf JOIN FETCH rf.claveRolFuncion.funcionSistema.subModuloSistema sm JOIN FETCH sm.moduloSistema m WHERE rf.claveRolFuncion.rol.rolId=:p1 and rf.claveRolFuncion.funcionSistema.estReg='A' ORDER BY m,rf.num ";
                
            Query query = session.createQuery(hql);
            query.setParameter("p1", rolId);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las FuncionSistemas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las FuncionSistemas \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    
    @Override
    public List<RolFuncionModel> buscarFuncionesPersonalizadasPorRol(int rolId) {
        List<RolFuncionModel> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar RolFunciones
            String hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.RolFuncionModel(rf.claveRolFuncion.funcionSistema.funSisId,"
                        + "rf.claveRolFuncion.funcionSistema.subModuloSistema.subModSisId,rf.claveRolFuncion.funcionSistema.subModuloSistema.nom,rf.claveRolFuncion.funcionSistema.subModuloSistema.cod,rf.claveRolFuncion.funcionSistema.subModuloSistema.ico,"
                        + "rf.claveRolFuncion.funcionSistema.subModuloSistema.moduloSistema.modSisId,rf.claveRolFuncion.funcionSistema.subModuloSistema.moduloSistema.nom,rf.claveRolFuncion.funcionSistema.subModuloSistema.moduloSistema.cod,rf.claveRolFuncion.funcionSistema.subModuloSistema.moduloSistema.ico,"
                        + "rf.claveRolFuncion.funcionSistema.nom,rf.claveRolFuncion.funcionSistema.url,rf.claveRolFuncion.funcionSistema.claNav,rf.claveRolFuncion.funcionSistema.nomCon,rf.claveRolFuncion.funcionSistema.nomInt,rf.claveRolFuncion.funcionSistema.ico,rf.tip,rf.dep,rf.num) "
                        + "FROM RolFuncion rf WHERE rf.claveRolFuncion.rol.rolId=:p1 and rf.claveRolFuncion.funcionSistema.estReg='A' ORDER BY rf.num ";
                
            Query query = session.createQuery(hql);
            query.setParameter("p1", rolId);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los RolFunciones \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los RolFunciones \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    
    @Override
    public List<FuncionSistema> listarFunciones(int subModCod, int rolCod) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            String hql = "SELECT r.claveRolFuncion.funcionSistema FROM RolFuncion r "
                        + "WHERE r.claveRolFuncion.rol.rolId = :rolId AND CAST(r.tip AS string) = :tip AND "
                        + "r.claveRolFuncion.funcionSistema.subModuloSistema.subModSisId = :subModSisId AND "
                        + "r.claveRolFuncion.funcionSistema.estReg = :estReg "
                    + "ORDER BY r.claveRolFuncion.funcionSistema.nom";

            Query query = session.createQuery(hql);
            query.setParameter("rolId", rolCod);
            query.setParameter("tip", "2");
            query.setParameter("subModSisId", subModCod);
            query.setParameter("estReg", 'A');

            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarFunciones", e);
            throw e;
        } finally {
            session.close();
        }
    }
}
