/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.modulo_sistema.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ModuloSistemaDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ModuloSistema;
import com.dremo.ucsm.gsc.sigesmed.core.entity.SubModuloSistema;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class ListarModuloSistemaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        boolean listarConSubmodulos = false;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            
            if( requestData!= null && requestData.length()> 0 ){
                listarConSubmodulos = requestData.getBoolean("listar");
            }
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listar los Modulos del sistema", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<ModuloSistema> modulos = null;
        ModuloSistemaDao moduloDao = (ModuloSistemaDao)FactoryDao.buildDao("ModuloSistemaDao");
        try{
            if(listarConSubmodulos)
                modulos = moduloDao.listarConSubModulos();
            else
                modulos = moduloDao.buscarTodos(ModuloSistema.class);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los Modulos del Sistema \n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los Modulos del Sistema", e.getMessage() );
        }
        //Fin        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(ModuloSistema modulo:modulos ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("moduloID",modulo.getModSisId() );
            oResponse.put("codigo",modulo.getCod());
            oResponse.put("nombre",modulo.getNom());
            oResponse.put("descripcion",modulo.getDes());
            oResponse.put("icono",modulo.getIco());
            oResponse.put("fecha",modulo.getFecMod().toString());
            oResponse.put("estado",""+modulo.getEstReg());
            
            //System.out.println("--->"+modulo.getSubModuloSistemas());
            //System.out.println("--->"+subModulos.isEmpty());
            
            List<SubModuloSistema> subModulos = modulo.getSubModuloSistemas();            
            if( listarConSubmodulos && subModulos.size() > 0 ){
                JSONArray aSubModulos = new JSONArray();
                for( SubModuloSistema sm:subModulos ){
                    JSONObject oSubModulo = new JSONObject();
                    oSubModulo.put("codigo",sm.getCod() );
                    oSubModulo.put("nombre",sm.getNom() );
                    oSubModulo.put("descripcion",sm.getDes() );
                    oSubModulo.put("estado",""+sm.getEstReg() );
                    aSubModulos.put(oSubModulo);
                }
                oResponse.put("subModulos",aSubModulos);
            }
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}