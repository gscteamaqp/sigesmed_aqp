/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.plantilla_grupo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.PlantillaGrupoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaFichaInstitucional;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaGrupo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.TipoGrupo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class InsertarGrupoTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        PlantillaGrupo grupo = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            int plaId = requestData.getInt("plaId");
            String gruNom = requestData.optString("gruNom");
            int gruTip = requestData.getInt("Tipo");
                        
            grupo = new PlantillaGrupo(gruNom, new TipoGrupo(gruTip), new PlantillaFichaInstitucional(plaId));
            
        
        }catch(Exception e){            
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage() );
        }
        //Fin
                
        /*
        *  Parte para la operacion en la Base de Datos
        */
        PlantillaGrupoDao grupoDao = (PlantillaGrupoDao)FactoryDao.buildDao("smdg.PlantillaGrupoDao");
        try{
            grupoDao.insert(grupo);
        
        }catch(Exception e){
            System.out.println("No se pudo registrar\n"+e);
            return WebResponse.crearWebResponseError("No se pudo registrar", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("gruId",grupo.getPgrId());
        //oResponse.put("fecha",grupo.getFecMod().toString());
        return WebResponse.crearWebResponseExito("El registro del grupo se realizo correctamente", oResponse);
        //Fin
    }
    
}
