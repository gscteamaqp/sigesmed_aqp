/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.asistencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.JustificacionAsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.JustificacionInasistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.PersonalDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.Inasistencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.Justificacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.JustificacionInasistenciaTrabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.RegistroAsistencia;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONObject;


/**
 *
 * @author carlos
 */
public class GuardarJustificacionAsistenciaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
          
        Justificacion justificacion=null;
        //String dni="";
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            Integer idReg = requestData.getInt("idReg");  
            Integer tipo = requestData.getInt("tipo");  
            String tramite=requestData.getString("tramite");
            String obs=requestData.getString("obs");       
            //dni=requestData.getString("dni");
            justificacion=new Justificacion(tramite, obs, tipo+"", new RegistroAsistencia(idReg),new Date());
          
            
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo verificar los datos ", e.getMessage() );
        }

        JustificacionAsistenciaDao justificacionAsistenciaDao = (JustificacionAsistenciaDao)FactoryDao.buildDao("cpe.JustificacionAsistenciaDao");
        //PersonalDao personalDao=(PersonalDao)FactoryDao.buildDao("cpe.JustificacionAsistenciaDao");
        try{
            justificacionAsistenciaDao.insert(justificacion);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo ingresar la justificacion ", e.getMessage() );
        }

        return WebResponse.crearWebResponseExito("Se Inserto correctamente");        
        //Fin
    }
    
}

