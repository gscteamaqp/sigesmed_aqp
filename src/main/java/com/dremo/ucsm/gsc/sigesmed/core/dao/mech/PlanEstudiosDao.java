/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.mech;


import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.AreaModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.GradoModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.MetaAtencion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.MetaAtencionModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.NivelModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Periodo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanHoraArea;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanNivel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Turno;
import java.util.List;

/**
 *
 * @author abel
 */

public interface PlanEstudiosDao extends GenericDao<PlanEstudios>{
    
    public PlanEstudios buscarVigentePorOrganizacion(int orgId);
    public List<PlanEstudios> buscarConOrganizacion();
    
    public List<NivelModel> buscarNiveles(int planEstudiosID);
    public List<GradoModel> buscarGrados(int planEstudiosID);
    public List<AreaModel> buscarAreas(int disCurID);
    public List<AreaModel> buscarTalleres(int disCurID);
    public List<AreaModel> buscarAreasByGrado(int disCurID,Integer gradoId);
    public List<AreaModel> buscarTalleresByGrado(int disCurID,Integer gradoId);
    
    public void mergeTurno(Turno turno);
    public void mergePeriodo(Periodo periodo);
    public void mergeNivel(PlanNivel nivel);
    public void mergeMetaAtencion(MetaAtencion meta);
    public void mergeHoraDisponible(PlanHoraArea area);
    public List<Turno> listarTurnos();
    public List<Periodo> listarPeriodos();
    public List<PlanNivel> listarNivelesPorPlanEstudios(int planID);
    public List<MetaAtencionModel> listarMetasAtencionPorOrganizacion(int organizacionID);
    
    public void eliminarTurno(char turnoID);
    public void eliminarPeriodo(char periodoID);
    public void eliminarNivel(int nivelID);
    public void eliminarHoraDisponible(PlanHoraArea horaArea);
    
    public void reiniciarPlazasMagisteriales(int orgId);
}