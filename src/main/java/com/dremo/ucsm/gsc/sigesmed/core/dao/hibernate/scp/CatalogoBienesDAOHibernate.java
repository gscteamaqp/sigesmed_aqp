/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scp;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.CatalogoBienesDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.CatalogoBienes;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BienesMuebles;
/**
 *
 * @author Administrador
 */
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.CatalogoBienes;

public class CatalogoBienesDAOHibernate extends GenericDaoHibernate<CatalogoBienes> implements CatalogoBienesDAO{

    @Override
    public List<CatalogoBienes> listarCatalogo() {
        
        List<CatalogoBienes> cat_bienes = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            
            String hql = "SELECT DISTINCT cb FROM CatalogoBienes cb JOIN FETCH cb.unidad_medida JOIN FETCH cb.familia familia  JOIN FETCH familia.clase_generica clase JOIN FETCH clase.grupo  WHERE cb.est_reg!='E'";
          //    String hql = "SELECT DISTINCT ii FROM InventarioInicial ii JOIN FETCH ii.inv_ini_det detalle JOIN FETCH detalle.cod_bie WHERE ii.est_reg!='E'";

         //   String hql = "SELECT cb FROM CatalogoBienes cb  JOIN FETCH cb.familia  JOIN FETCH cb.unidad_medida WHERE cb.est_reg!='E'";

            Query query = session.createQuery(hql); 
            cat_bienes = query.list();
            
        }catch(Exception e){
             System.out.println("No se pudo Mostrar el Catalogo de Bienes \\n "+ e.getMessage());
             throw new UnsupportedOperationException("No se pudo Mostrar el Catalogo de Bienes \\n "+ e.getMessage());
        }
        finally{
            session.close();
        }
        return cat_bienes;  

    }

    @Override
    public CatalogoBienes mostrarDetalleCatalogo(int cat_bie_id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    

    
    
    /**
     * Eliminar Iten del catalago sbn
     * Cambia el est_reg = B     
     * @return void
     */
    @Override
    public void eliminarCatalagoItem(int cat_bie_id) {
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        
        try{           
            String hql = "UPDATE CatalogoBienes cb SET cb.est_reg='E' WHERE cb.cat_bie_id =:p1";            
            Query query = session.createQuery(hql);
            query.setParameter("p1", cat_bie_id);
            query.executeUpdate();    
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo eliminar : el item del catalogo del bien " +  "\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }        
    }

    public void CatalogoBienes() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CatalogoBienes obtenerCatalogo(int cat_bie_id) {
        
        CatalogoBienes catBn = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            
            String hql = "SELECT cb FROM CatalogoBienes cb WHERE cb.cat_bie_id=:p1 ";
            Query query = session.createQuery(hql); 
            query.setParameter("p1",cat_bie_id);
            catBn = (CatalogoBienes)(query.uniqueResult());
 
       }
        catch(Exception e){
            System.out.println("No se pudo el detalle del catalogo de bienes\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Mostrar la lista de Bienes Muebles por Fecha \\n "+ e.getMessage());

        }
        finally{
            session.close();
        }
        return catBn; 
    }
    
}
