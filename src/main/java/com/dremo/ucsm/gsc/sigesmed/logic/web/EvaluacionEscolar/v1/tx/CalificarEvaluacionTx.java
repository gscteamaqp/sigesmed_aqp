/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx.*;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.BandejaEvaluacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.EvaluacionEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaEvaluacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaTarea;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.RespuestaEvaluacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Alertas;
import org.json.JSONObject;
import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.AlertManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
 *
 * @author abel
 */
public class CalificarEvaluacionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            EvaluacionEscolarDao eva_res = (EvaluacionEscolarDao)FactoryDao.buildDao("web.EvaluacionEscolarDao");
            JSONArray preguntas = requestData.getJSONArray("preguntas");
            JSONArray respuestas = requestData.getJSONArray("resp_alu");
            List<RespuestaEvaluacion> respuestas_alu = new ArrayList<RespuestaEvaluacion>();
            RespuestaEvaluacion respuesta_alumno=null;
            int band_eva_id = requestData.getInt("band_id");
            
            int puntos = 0;
            
            for(int i=0;i<preguntas.length();i++){
                JSONObject pregunta = preguntas.getJSONObject(i);
                for(int j=0;j<respuestas.length();j++){
                    JSONObject respuesta = respuestas.getJSONObject(j);
                    if(pregunta.getInt("preguntaID") == respuesta.getInt("nro")){ //Encuentra la pregunta
                        String resp_correcta = pregunta.getString("resp");
                        String resp_alu = respuesta.getString("nombre");
                        if(resp_correcta.equals(resp_alu)){ // Compara las respuestas
                             respuesta_alumno = new RespuestaEvaluacion(pregunta.getInt("preguntaID"),band_eva_id,resp_alu,pregunta.getInt("punt"),new Date());
                             puntos += pregunta.getInt("punt");
                        }
                        else{ // Respuesta incorrecta
                            respuesta_alumno = new RespuestaEvaluacion(pregunta.getInt("preguntaID"),band_eva_id,resp_alu,0,new Date());
                        }
                        respuestas_alu.add(respuesta_alumno);
                        break;
                    }
                }
            }
            eva_res.insertarRespuestasAlumno(respuestas_alu);
            
            BandejaEvaluacionDao band_eva = (BandejaEvaluacionDao)FactoryDao.buildDao("web.BandejaEvaluacionDao");
            BandejaEvaluacion bandeja = band_eva.buscar_bandeja_alumno(band_eva_id);
            bandeja.setNota(puntos);
            bandeja.setEstado('E');
            bandeja.setFecEnt(new Date());
            band_eva.update(bandeja);
            
            JSONObject oResponse = new JSONObject();
            oResponse.put("puntaje",puntos);
           
            return WebResponse.crearWebResponseExito("El Registro de la Evaluacion se Realizo Correctamente", oResponse);
    
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage() );
        }
        
      
    }    
    
    
}
