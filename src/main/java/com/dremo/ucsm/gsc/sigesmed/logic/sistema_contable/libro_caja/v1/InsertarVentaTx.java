/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.LibroCajaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.ClienteProveedor;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.RegistroCompras;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.RegistroVentas;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.TipoPago;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.math.BigDecimal;
import java.util.Date;
import org.json.JSONObject;

/**
 *
 * @author RE
 */
public class InsertarVentaTx implements ITransaction{   
    
    @Override    
    public WebResponse execute(WebRequest wr)  {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        
        Object obj=null;
        FileJsonObject docVenta= null;
        String nomDocAdj="";
        
        RegistroVentas nuevaVenta = null;
        LibroCajaDao ventaDao = (LibroCajaDao)FactoryDao.buildDao("sci.LibroCajaDao");

        try{            
             
            JSONObject requestData = (JSONObject)wr.getData();           
            int clienteProveedorID = requestData.getInt("clienteProveedorID");                       
            short tipoPagoID = (short)requestData.getInt("tipoPagoID");
            String datos= requestData.getString("datos");
            
            String fecha= requestData.getString("fechaR");           
          
            double importe = requestData.getDouble("importe");
            String numeroD = requestData.optString("numeroD","");
             
             //verificamos si existe un archivo adjunto
            JSONObject jDoc = (JSONObject)requestData.opt("doc");        
            
            JSONObject jsonArchivo = jDoc.optJSONObject("archivo");
            
            
            if( jsonArchivo !=null && jsonArchivo.length() > 0 ){
                obj = ventaDao.llave(RegistroVentas.class,"codUniOpeId");                
                docVenta = new FileJsonObject( jsonArchivo ,((int)obj+1)+"_venta"); 
                nomDocAdj = docVenta.getName();
            }         
                        
            nuevaVenta = new RegistroVentas(0, new ClienteProveedor(clienteProveedorID,datos),new TipoPago(tipoPagoID),new Date(fecha), numeroD,new BigDecimal(importe),new Date(), wr.getIdUsuario(), 'A',nomDocAdj);
                           
        }catch(Exception e){
          
            return WebResponse.crearWebResponseError("No se pudo registrar Compra, datos incorrectos", e.getMessage() );
        }
         
  

        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        try{
            ventaDao.insertarVenta(nuevaVenta);        
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo registrar la cuenta contable ", e.getMessage() );
        }
        //Fin
        
      
        /*
        *  Repuesta Correcta
        */
        
          if(docVenta !=null){
            BuildFile.buildFromBase64("contable", docVenta.getName(), docVenta.getData());
        }
          
        JSONObject oResponse = new JSONObject();
        oResponse.put("codUniOpeID",nuevaVenta.getCodUniOpeId());
        oResponse.put("nomDocAdj",nuevaVenta.getNomDocAdj());

        return WebResponse.crearWebResponseExito("El registro de la Compra se realizo correctamente", oResponse);
        //Fin
    }    
    
    
}
