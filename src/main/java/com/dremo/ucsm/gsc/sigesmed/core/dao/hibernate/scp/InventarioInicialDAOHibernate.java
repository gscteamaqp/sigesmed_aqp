/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scp;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import java.util.List;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.InventarioInicialDAO;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.InventarioInicial;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.InventarioInicialDetalle;
import org.hibernate.SQLQuery;

/**
 *
 * @author Administrador
 */
public class InventarioInicialDAOHibernate extends GenericDaoHibernate<InventarioInicial> implements InventarioInicialDAO {

    @Override
    public List<InventarioInicial> listarInventarioInicial(int org_id) {

        List<InventarioInicial> inventario_inicial = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            //    String hql = "SELECT DISTINCT ii FROM InventarioInicial ii JOIN FETCH ii.inv_ini_det detalle JOIN FETCH detalle.cod_bie WHERE ii.est_reg!='E'";
            //       String hql = "SELECT ii FROM InventarioInicial ii JOIN FETCH ii.inv_ini_det WHERE ii.est_reg!='E'";
            String hql = "SELECT DISTINCT ii FROM InventarioInicial ii   WHERE ii.est_reg!='E' and ii.org_id=:p1";

            Query query = session.createQuery(hql);
            query.setParameter("p1", org_id);
            inventario_inicial = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo Mostrar el Inventario Inicial \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Mostrar el Inventario Inicial \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return inventario_inicial;

    }

    @Override
    public InventarioInicial obtenerInventarioInicial(int inv_id, int org_id) {

        InventarioInicial ii = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {

            String hql = "SELECT DISTINCT ii FROM InventarioInicial ii   WHERE ii.inv_ini_id=:p1 and ii.org_id=:p2";
            Query query = session.createQuery(hql);
            query.setParameter("p1", inv_id);
            query.setParameter("p2", org_id);
            ii = (InventarioInicial) (query.uniqueResult());

        } catch (Exception e) {
            System.out.println("No se pudo Mostrar el Inventario Inicial \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Mostrar el Inventario Inicial \\n " + e.getMessage());

        } finally {
            session.close();
        }
        return ii;

        //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<InventarioInicialDetalle> obtenerDetalleInventarioInicial(int inv_id, int org_id) {

        List<InventarioInicialDetalle> iid = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {

            String hql = "SELECT DISTINCT iid FROM InventarioInicialDetalle iid JOIN FETCH iid.cod_bie bienes JOIN FETCH bienes.ambiente JOIN FETCH bienes.anexo WHERE iid.inv_ini_id=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", inv_id);

            iid = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo Mostrar el Inventario Inicial Detalle \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Mostrar el Inventario Inicial Detalle\\n " + e.getMessage());

        } finally {
            session.close();
        }
        return iid;

        //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void eliminar_detalle(int inv_ini_id) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try {
            String hql = "DELETE FROM InventarioInicialDetalle WHERE inv_ini_id= :p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", inv_ini_id);
            query.executeUpdate();
            miTx.commit();
        } catch (Exception e) {
            miTx.rollback();
            System.out.println("No se pudo Eliminar el Detalle del Inventario Inicial \\n " + e.getMessage());
        }

        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void eliminar_inventario(int id_inv) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try {
            String hql = "UPDATE InventarioInicial  set est_reg = 'E'  WHERE inv_ini_id = :p1";

            Query query = session.createQuery(hql);
            query.setParameter("p1", id_inv);
            query.executeUpdate();
            miTx.commit();

        } catch (Exception e) {
            miTx.rollback();
            System.out.println("No se pudo Eliminar el Inventario Inicial  \\n " + e.getMessage());
            //  throw new UnsupportedOperationException("No se pudo Mostrar la lista de Bienes Muebles por Fecha \\n "+ e.getMessage());
            e.printStackTrace();
        }

        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<InventarioInicial> reporteInventario(int org_id) {

        List<InventarioInicial> invIni = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try {
            //listar ultimo codigo de tramite
//            SQLQuery query = session.createSQLQuery("SELECT ii.*, mi.* FROM administrativo.inventario_inicial as ii INNER JOIN administrativo.movimiento_ingresos as mi ON ii.mov_ing_id = mi.mov_ing_id WHERE " 
//                    + "ii.org_id = "+ org_id +" ii.est_reg != 'E'");
//
//            invIni = query.list();
//            t.commit();
            
            String hql = "SELECT ii FROM InventarioInicial ii JOIN FETCH ii.mov_ingresos mvi JOIN FETCH mvi.tipo_movimiento WHERE ii.est_reg!='E' and ii.org_id=:p1";

            Query query = session.createQuery(hql);
            query.setParameter("p1", org_id);
            invIni = query.list();
            
        } catch (Exception e) {
            t.rollback();            
            throw new UnsupportedOperationException("No se pudo Mostrar el Inventario Inicial \\n " + e.getMessage());
        } finally {
            session.close();
        }
        
        return invIni;

    }

}
