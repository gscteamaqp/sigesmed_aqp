/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.organizacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class ListarOrganizacionTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {

        /*
         *  Parte para la operacion en la Base de Datos
         */
        List<Organizacion> organizaciones = null;
        OrganizacionDao moduloDao = (OrganizacionDao) FactoryDao.buildDao("OrganizacionDao");
        try {
            organizaciones = moduloDao.buscarConTipoOrganizacionYPadre();

        } catch (Exception e) {
            System.out.println("No se pudo Listar las Organizaciones\n" + e);
            return WebResponse.crearWebResponseError("No se pudo Listar las Organizaciones", e.getMessage());
        }
        //Fin

        /*
         *  Repuesta Correcta
         */
        JSONArray miArray = new JSONArray();
        for (Organizacion organizacion : organizaciones) {
            JSONObject oResponse = new JSONObject();
            oResponse.put("organizacionID", organizacion.getOrgId());
            oResponse.put("tipoOrganizacionID", organizacion.getTipoOrganizacion().getTipOrgId());
            oResponse.put("tipoOrganizacion", organizacion.getTipoOrganizacion().getNom());
            if (organizacion.getOrganizacionPadre() != null) {
                oResponse.put("organizacionPadreID", organizacion.getOrganizacionPadre().getOrgId());
            } else {
                oResponse.put("organizacionPadreID", 0);
            }
            oResponse.put("codigo", organizacion.getCod());
            oResponse.put("nombre", organizacion.getNom());
            oResponse.put("alias", organizacion.getAli());
            oResponse.put("descripcion", organizacion.getDes());
            oResponse.put("direccion", organizacion.getDir());
            oResponse.put("fecha", organizacion.getFecMod().toString());
            oResponse.put("estado", "" + organizacion.getEstReg());

            oResponse.put("ubigeo", "" + organizacion.getUbiCod());
            oResponse.put("organizacionGestion", "" + organizacion.getOrgGes());
            oResponse.put("organizacionCaracteristica", "" + organizacion.getOrgCar());
            oResponse.put("organizacionPrograma", "" + organizacion.getOrgPro());
            oResponse.put("organizacionForma", "" + organizacion.getOrgFor());
            oResponse.put("organizacionVariante", "" + organizacion.getOrgVar());
            oResponse.put("organizacionModalidad", "" + organizacion.getOrgMod());
            
            miArray.put(oResponse);
        }

        return WebResponse.crearWebResponseExito("Se Listo correctamente", miArray);
        //Fin
    }

}
