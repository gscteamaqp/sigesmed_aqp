/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.catalogo_bienes.v1.tx;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.CatalogoBienes;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.CatalogoBienesDAO;
import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 *
 * @author Jeferson
 */
public class RegistrarCatalogoBienesTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        
        CatalogoBienes catalogo = null;
        JSONObject requestData = (JSONObject)wr.getData();
        
        JSONObject oRes = new JSONObject();
        
            try{
        
            int gru_gen_id = requestData.getInt("gru_gen_id");
            int cla_gen_id = requestData.getInt("cla_gen_id");
            int fam_gen_id = requestData.getInt("fam_gen_id");
            short uni_med_id =(short) requestData.getInt("uni_med_id");
            String cod  = requestData.getString("cod");
            
            String den_bie = requestData.getString("den_bie");
            Date fec_cre = new Date();
            String num_res = requestData.getString("num_res");
            boolean fla_sbn = false;
            Date fec_mod = new Date();
            int usu_mod = requestData.getInt("usu_mod");
            char est_reg = 'A';
            catalogo = new CatalogoBienes(0,cod,den_bie,fec_cre,num_res,uni_med_id,fla_sbn,fec_mod,usu_mod,est_reg,gru_gen_id,cla_gen_id,fam_gen_id);
            
            CatalogoBienesDAO cat_bie_dao = (CatalogoBienesDAO)FactoryDao.buildDao("scp.CatalogoBienesDAO");
            cat_bie_dao.insert(catalogo);
            
            JSONObject catBienes = new JSONObject();
            
            oRes.put("cat_bie_id", catalogo.getCat_bie_id());
            
            System.out.println(catalogo.getCat_bie_id());
            
            }
            catch(Exception e){
                System.out.println("No se pudo Registrar el Catalogo de Bien\n"+e);
                return WebResponse.crearWebResponseError("No se pudo Registrar el Catalogo de Bien ", e.getMessage() );        

            }
            return WebResponse.crearWebResponseExito("Se Registro el Nuevo Catalogo de Bien correctamente", oRes); 


}
}
