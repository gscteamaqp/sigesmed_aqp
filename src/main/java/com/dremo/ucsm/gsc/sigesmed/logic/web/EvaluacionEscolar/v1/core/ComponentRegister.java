/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx.ActualizarBandejasAlumnosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx.ActualizarEvaluacionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx.CalificarEvaluacionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx.EliminarEvaluacionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx.EnviarEvaluacionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx.FinalizarEvaluacionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx.InsertarEvaluacionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx.InsertarMensajesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx.InsertarPreguntasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx.ListarAlumnosQueCumplieronEvaluacionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx.ListarRespuestasDeEvaluacionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx.ListarEvaluacionesPorAlumnoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx.ListarEvaluacionesPorDocenteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx.ListarEvaluacionesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx.ListarMensajesCalificacionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx.ListarPreguntasEvaluacionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx.ObtenerEvaluacionPreguntasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx.ObtenerUnidadOrganicaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx.ResolverEvaluacionTx;

/**
 *
 * @author abel
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_WEB);        
        
        //Registrnado el Nombre del componente
        component.setName("evaluacion");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionPOST("insertarEvaluacion", InsertarEvaluacionTx.class);
        component.addTransactionPOST("insertarPreguntas", InsertarPreguntasTx.class);
        component.addTransactionPOST("insertarMensajes", InsertarMensajesTx.class);
        component.addTransactionPOST("enviarEvaluacion", EnviarEvaluacionTx.class);
        component.addTransactionPUT("actualizarEvaluacion", ActualizarEvaluacionTx.class);
        component.addTransactionDELETE("eliminarEvaluacion", EliminarEvaluacionTx.class);
        
        component.addTransactionPOST("resolverEvaluacion", ResolverEvaluacionTx.class);
        component.addTransactionPOST("calificarEvaluacion", CalificarEvaluacionTx.class);
        component.addTransactionPUT("finalizarEvaluacion", FinalizarEvaluacionTx.class);
        
        component.addTransactionGET("listarEvaluacionesPorDocente", ListarEvaluacionesPorDocenteTx.class);
        component.addTransactionGET("listarEvaluaciones", ListarEvaluacionesTx.class);
        component.addTransactionGET("listarAlumnosCumplieronEvaluacion", ListarAlumnosQueCumplieronEvaluacionTx.class);
        component.addTransactionGET("verRespuestasDeEvaluacion", ListarRespuestasDeEvaluacionTx.class);
        component.addTransactionGET("listarEvaluacionesPorAlumno", ListarEvaluacionesPorAlumnoTx.class);
        component.addTransactionGET("listarMensajes", ListarMensajesCalificacionTx.class);
        component.addTransactionGET("listarPreguntasEvaluacion",ListarPreguntasEvaluacionTx.class);
        component.addTransactionGET("obtenerEvaluacionPreguntas",ObtenerEvaluacionPreguntasTx.class);
        component.addTransactionGET("obtenerUnidadDidactica",ObtenerUnidadOrganicaTx.class);
        component.addTransactionPUT("actualizarbandeja",ActualizarBandejasAlumnosTx.class);
        return component;
    }
}
