package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.acomp;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Seccion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.SesionAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.UnidadDidactica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Grado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Nivel;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrador on 05/01/2017.
 */
@Entity
@Table(name = "acompanamientos",schema = "pedagogico")
public class AcompanamientoDocente implements java.io.Serializable {
    @Id
    @Column(name = "aco_id", nullable = false,unique = true)
    @SequenceGenerator(name = "acompanamientos_aco_id_seq",sequenceName = "pedagogico.acompanamientos_aco_id_seq")
    @GeneratedValue(generator = "acompanamientos_aco_id_seq")
    private int acoId;

    @OneToMany(mappedBy = "acomp",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<CompromisosAcompanamiento> compromisos = new ArrayList<>();
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "doc_id")
    private Docente docente;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "org_id")
    private Organizacion organizacion;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "aut_id")
    private Usuario autor;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "niv_id")
    private Nivel nivel;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gra_id")
    private Grado grado;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sec_id")
    private Seccion seccion;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "are_cur_id")
    private AreaCurricular area;
    @Column(name = "res")
    private Integer res;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec",length = 29)
    private Date fec;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod",length = 29)
    private Date fecMod;
    @Column(name = "est_reg",length = 1)
    private Character estReg;

    public AcompanamientoDocente() {
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public AcompanamientoDocente(Integer res, Date fec) {
        this.res = res;
        this.fec = fec;
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public int getAcoId() {
        return acoId;
    }

    public List<CompromisosAcompanamiento> getCompromisos() {
        return compromisos;
    }

    public void setCompromisos(List<CompromisosAcompanamiento> compromisos) {
        this.compromisos = compromisos;
    }

    public void setAcoId(int acoId) {
        this.acoId = acoId;
    }

    public Docente getDocente() {
        return docente;
    }

    public void setDocente(Docente docente) {
        this.docente = docente;
    }

    public Organizacion getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(Organizacion organizacion) {
        this.organizacion = organizacion;
    }

    public Usuario getAutor() {
        return autor;
    }

    public void setAutor(Usuario autor) {
        this.autor = autor;
    }

    public AreaCurricular getArea() {
        return area;
    }

    public void setArea(AreaCurricular area) {
        this.area = area;
    }

    public Integer getRes() {
        return res;
    }

    public void setRes(Integer res) {
        this.res = res;
    }

    public Date getFec() {
        return fec;
    }

    public void setFec(Date fec) {
        this.fec = fec;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public Nivel getNivel() {
        return nivel;
    }

    public void setNivel(Nivel nivel) {
        this.nivel = nivel;
    }

    public Grado getGrado() {
        return grado;
    }

    public void setGrado(Grado grado) {
        this.grado = grado;
    }

    public Seccion getSeccion() {
        return seccion;
    }

    public void setSeccion(Seccion seccion) {
        this.seccion = seccion;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
}
