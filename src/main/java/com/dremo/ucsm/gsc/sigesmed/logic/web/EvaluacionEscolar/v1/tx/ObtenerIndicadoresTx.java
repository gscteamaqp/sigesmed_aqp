/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.EvaluacionesUnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.IndicadorAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.TareaEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.TareaEscolar;
import java.util.List;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.TareaIndicador;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.TareaIndicadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.EvaluacionesUnidadDidactica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.TareaSesionAprendizaje;
/**
 *
 * @author abel
 */
public class ObtenerIndicadoresTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        int eva_maes_id;
        
        JSONObject requestData = (JSONObject)wr.getData();
        
        eva_maes_id = requestData.getInt("eva_maes_id");
        
        EvaluacionesUnidadDidacticaDao eva_unidad = (EvaluacionesUnidadDidacticaDao)FactoryDao.buildDao("maestro.plan.EvaluacionesUnidadDidacticaDao");
        
        EvaluacionesUnidadDidactica evaluacion = eva_unidad.buscarEvaluacionId(eva_maes_id);
        int uni_did_id = evaluacion.getEvaUniDidId();
        
        IndicadorAprendizajeDao indi_uni = (IndicadorAprendizajeDao)FactoryDao.buildDao("maestro.plan.IndicadorAprendizajeDao");
       
        
        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
