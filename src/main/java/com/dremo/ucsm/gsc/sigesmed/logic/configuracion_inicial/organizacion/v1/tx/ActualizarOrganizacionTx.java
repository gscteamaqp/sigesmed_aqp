/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.organizacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TipoOrganizacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import com.dremo.ucsm.gsc.sigesmed.util.ImageUtil;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Administrador
 */
public class ActualizarOrganizacionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        Organizacion organizacionAct = null;
        FileJsonObject orgImgInstFile = null;
        String orgNameImgInst = "";
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            int tipoOrganizacionID = requestData.getInt("tipoOrganizacionID");
            int organizacionPadreID = requestData.getInt("organizacionPadreID");
            int organizacionID = requestData.getInt("organizacionID");
            String codigo = requestData.getString("codigo");
            String nombre = requestData.getString("nombre");
            String alias = requestData.getString("alias");
            String descripcion = requestData.getString("descripcion");
            String direccion = requestData.getString("direccion");
            String estado = requestData.getString("estado");
            
            JSONObject orgImgInst = requestData.optJSONObject("orgImgInstFile");
            
            if(organizacionPadreID == 0)
                organizacionAct = new Organizacion(organizacionID,new TipoOrganizacion(tipoOrganizacionID), codigo, nombre, alias, descripcion,direccion, new Date(), 1, estado.charAt(0));
            else            
                organizacionAct = new Organizacion(organizacionID,new TipoOrganizacion(tipoOrganizacionID),new Organizacion(organizacionPadreID), codigo, nombre, alias, descripcion, direccion, new Date(), 1, estado.charAt(0), null);
            
            if (orgImgInst != null) {
                orgImgInstFile = new FileJsonObject(orgImgInst);
                orgNameImgInst = Integer.toString(organizacionID) + "-" + orgImgInstFile.getName();
                
                if (orgImgInstFile != null) {
                    BuildFile.buildFromBase64("organizaciones", orgNameImgInst, orgImgInstFile.getData());
                    organizacionAct.setOrgImgInst(orgNameImgInst);
                    
                    ImageUtil imgUtil = new ImageUtil();
                    
                    imgUtil.resize("organizaciones", orgNameImgInst, orgNameImgInst, 80, 80);
                }                
            }
        
            
        }catch(Exception e){
            System.err.println(e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        OrganizacionDao moduloDao = (OrganizacionDao)FactoryDao.buildDao("OrganizacionDao");
        try{
            moduloDao.update(organizacionAct);
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar la Organizacion\n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar la Organizacion", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("La Organizacion se actualizo correctamente");
        //Fin
    }
    
}
