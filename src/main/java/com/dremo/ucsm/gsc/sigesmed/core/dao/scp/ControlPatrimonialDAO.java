/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.scp;
import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import java.util.Date;
import java.util.List;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.ControlPatrimonial;
/**
 *
 * @author Administrador
 */
public interface ControlPatrimonialDAO  extends GenericDao<ControlPatrimonial>{
    
    public List<ControlPatrimonial> listarPatrimonioUGEL(int orgID);
    public List<ControlPatrimonial> listarPatrimonio(int orgId);
    public void eliminarControlPatrimonial(int con_pat_id);
}
