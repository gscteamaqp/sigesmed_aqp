/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.ItemFileDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.PlazoDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.ItemFile;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.Plazo;
import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import java.io.File;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONObject;
/**
 *
 * @author ucsm
 */
public class InsertarDirectorioGestionTx implements ITransaction{
  
    
    int codDir = 0;
    String nomDir = "";
    Integer verDir = 0;
    Integer tamDir = 0;
    Long ntamDir;
    String strfeciniPlazo="";
    String strfecfinPlazo="";
    String strfecCre="";
    String urlDir="";
    SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
    Boolean existePlazos = false;
    boolean existeProteccion=false;
    Date fecDirIniPlazo = null, fecDirFinPlazo = null, fecCre=null;
    int usuarioID=0;
    String proteccion="0";
    @Override
    public WebResponse execute(WebRequest wr) {      
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        ItemFile nuevoDir = null;
        //TipoItemFile tipIteFil = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
    
            codDir = requestData.getInt("codDir");
            nomDir = requestData.getString("nomDir");
            //verDir = requestData.getInt("verDir"); 
            verDir = 1; 
            tamDir = 0; //Al crearse una nueva carpeta esta tendra tamaño cero
            ntamDir = (long)tamDir;
            strfecCre = requestData.getString("fecCre");
            usuarioID=requestData.getInt("usuarioID");
            try {
                fecCre = formatoDelTexto.parse(strfecCre);
            } catch (ParseException ex) {
                ex.printStackTrace();
            }
            existePlazos = requestData.getBoolean("flagPlazo");
            if(existePlazos){
                strfeciniPlazo = requestData.getString("fecIniP");
                strfecfinPlazo = requestData.getString("fecFinP");
                //Construyendo la fecha de un string
                try {
                    fecDirIniPlazo = formatoDelTexto.parse(strfeciniPlazo);
                    fecDirFinPlazo = formatoDelTexto.parse(strfecfinPlazo);
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
            }
            existeProteccion=requestData.getBoolean("flagProteccion");
            if(existeProteccion){
                proteccion=requestData.getString("proteccion");
            }
            //Sabemos que url es vacio y tendra una ruta por defecto 
            //(TODO: Deberas coger la ruta de la maquina actual y no solo de tu maquina)
            urlDir = (ServicioREST.PATH_SIGESMED+"/archivos").replace('\\','/');
        //para este caso debemos colocar el id de padre en forma de objeto asi podremos insertarlo
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage() );
        }

        if(codDir != -1){            
            
            ItemFileDao itemFileDao = (ItemFileDao)FactoryDao.buildDao("rdg.ItemFileDao");
            //Buscamos el itemfile con el codigo del padre
            ItemFile filePadre = itemFileDao.buscarPorID(codDir);

            //El url debe componerse de la url del padre y del nombre del padre
//            nuevoDir = new ItemFile(nomDir, verDir.shortValue(), "D", ntamDir.doubleValue(), fecCre, urlDir +"/"+ filePadre.getIteNom(), usuarioID, "A",filePadre);
            String nameUrl=filePadre.getIteNom();
            if(filePadre.getItePadIde()!=null){
                
                ItemFile fileAbuelo=itemFileDao.buscarPorID(filePadre.getItePadIde().getIteIde());
                nameUrl=fileAbuelo.getIteNom()+"/"+nameUrl;
            }
            
            nuevoDir = new ItemFile(nomDir, verDir.shortValue(), "D", ntamDir.doubleValue(), fecCre, nameUrl, usuarioID, "A",filePadre);
            nuevoDir.setIteMod('N');
           
            //Insertamos un objeto Plazo ahora si se podra insertar una fecha de plazos controlada
            if(existePlazos){
                PlazoDao plazoDao = (PlazoDao)FactoryDao.buildDao("rdg.PlazoDao");
                Plazo plazoDir = new Plazo();
                plazoDir.setPlzFecIniSub(fecDirIniPlazo);
                plazoDir.setPlzFecFinSub(fecDirFinPlazo);
                try{
                    //haciendo solo la persistencia del tipo plazo para control de plazos del directorio
                    plazoDao.insert(plazoDir);
                }catch(Exception e){
                    return WebResponse.crearWebResponseError("No se pudo registrar, ", e.getMessage() );
                }
                nuevoDir.setItePlaIde(plazoDir);
            }
          
            String ultimoCodigo = itemFileDao.buscarUltimoCodigo();
            Integer nextCode = Integer.parseInt(ultimoCodigo)+1;
            nuevoDir.setIteIde(nextCode);
            nuevoDir.setIteAltIde(cerosIzquierda(nextCode,4));           
            nuevoDir.setIteProt(proteccion);
            
            
            
            if(itemFileDao.buscarNombreRepetido(nuevoDir)){//Nombre repetido en un mismo contexto
                return WebResponse.crearWebResponseError("Nombre de carpeta repetido");                
            }else {
                if(itemFileDao.buscarIdPadre(filePadre) == 1 || filePadre.getIteIde()== 1){//Limitando niveles de profundidad que se alcanzará a solo 2.
                    try{
                          File newdirectorio = new File(urlDir +"/"+ nameUrl+"/" + nomDir);
                            newdirectorio.mkdir();
                         itemFileDao.insert(nuevoDir);                       
                         
                    }catch(Exception e){
                        return WebResponse.crearWebResponseError("No se pudo registrar, ", e.getMessage() );
                    }                
                }else{
                   return WebResponse.crearWebResponseError("No se puede crear la carpeta en ese nivel");
                }            
            }
            
        }else{
            //creamos un nuevo directorio pero sin el padre (sin la clave foranea :D )
            //Al no ser hijo de nadie la url ira solo sin acompañar con el nombre del padre
            ItemFileDao itemFileDao = (ItemFileDao)FactoryDao.buildDao("rdg.ItemFileDao");
            
            try{
            File newdirectorio = new File(urlDir +"/"+ nomDir);
            newdirectorio.mkdir();
            }catch(Exception e){
                return WebResponse.crearWebResponseError("No se pudo crear el directorio fisico, ", e.getMessage() );
            }
            
            nuevoDir = new ItemFile(nomDir, verDir.shortValue(),"D",ntamDir.doubleValue(),fecCre, nomDir, usuarioID, "A");
            nuevoDir.setIteMod('N');
            if(existePlazos){
                PlazoDao plazoDao = (PlazoDao)FactoryDao.buildDao("rdg.PlazoDao");
                Plazo plazoDir = new Plazo();
                plazoDir.setPlzFecIniSub(fecDirIniPlazo);
                plazoDir.setPlzFecFinSub(fecDirFinPlazo);
                try{
                    //haciendo solo la persistencia del tipo plazo para control de plazos del directorio
                    plazoDao.insert(plazoDir);
                }catch(Exception e){
                    return WebResponse.crearWebResponseError("No se pudo registrar los plazos, ", e.getMessage() );
                }
                nuevoDir.setItePlaIde(plazoDir);
            }else{
                nuevoDir.setItePlaIde(new Plazo(0));
            }
            
            String ultimoCodigo = itemFileDao.buscarUltimoCodigo();
            Integer nextCode = Integer.parseInt(ultimoCodigo)+1;
            nuevoDir.setIteIde(nextCode);
            nuevoDir.setIteAltIde(cerosIzquierda(nextCode,4));
            //int idDir = (int) session.save(nuevoDir);
            
            System.out.println(nuevoDir.toString());
            try{
                 itemFileDao.insert(nuevoDir);      
            }catch(Exception e){
                return WebResponse.crearWebResponseError("No se pudo registrar, ", e.getMessage() );
            }
        }
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("ideFil",nuevoDir.getIteIde());
        oResponse.put("urlFil",nuevoDir.getIteUrlDes());
        if(nuevoDir.getItePadIde()!=null)
            oResponse.put("padFil",nuevoDir.getItePadIde().getIteIde());
        else
            oResponse.put("padFil",0);
        
        return WebResponse.crearWebResponseExito("El registro del Documento se realizo correctamente", oResponse);
        //Fin
    }
    
    public static String cerosIzquierda(long codigo,int numDigitos){
		
        String nuevoCodigo = String.valueOf( codigo );

        int numCeros = numDigitos - nuevoCodigo.length();

        for (int i=0;i < numCeros;i++)
            nuevoCodigo="0"+nuevoCodigo;

        return nuevoCodigo;
    }
}
