/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;

import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.PlantillaFichaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.PlantillaFicha;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;


public class PlantillaFichaDaoHibernate extends GenericDaoHibernate<PlantillaFicha> implements PlantillaFichaDao {

    @Override
    public String buscarUltimoCodigo() {
        String codigo = null;

        return codigo;
    }
    @Override
    public List<Object[]> listarPlantillas(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Object[]> plantillas = null;
        Transaction t = session.beginTransaction();
        Query query = null;
        try{
            
            query = session.createSQLQuery("SELECT p.plf_id, p.plf_cod, p.plf_nom, p.plf_des ,p.fec_mod\n" +
                "  FROM pedagogico.plantilla_ficha p\n" +
                "  ORDER BY p.plf_id;");  
                        
            plantillas = query.list();
            t.commit();
            
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar las plantillas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar las plantillas \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return plantillas;
    }
    
    @Override
    public PlantillaFicha obtenerPlantilla(int plaId){
        Session session = HibernateUtil.getSessionFactory().openSession();
        PlantillaFicha plantilla = null;
        Transaction t = session.beginTransaction();
        
        session.enableFilter("filtroplantilla");
        session.enableFilter("filtrocompromiso");
        
        try{                                        
            String hql = "SELECT p FROM PlantillaFicha p " 
                    + "join fetch p.compromisos gru "
                    + "WHERE p.plfId = " + plaId + " AND p.estReg = 'A' " 
                    + "ORDER BY p.plfId";
            
            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            plantilla = (PlantillaFicha)query.uniqueResult();            
            t.commit();
                      
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo obtener la plantilla \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo obtener la plantilla \\n "+ e.getMessage());            
        }
        finally{
            session.disableFilter("filtroplantilla");
            session.disableFilter("filtrocompromiso");
            session.close();
        }
        
        return plantilla;
    }
    
    @Override
    public PlantillaFicha getPlantilla(int plaId){
        Session session = HibernateUtil.getSessionFactory().openSession();
        PlantillaFicha plantilla = null;
        Transaction t = session.beginTransaction();
        try{                                        
            plantilla = (PlantillaFicha)session.get(PlantillaFicha.class, plaId);
            t.commit();
                      
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo obtener la plantilla \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo obtener la plantilla \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return plantilla;
    }
}
