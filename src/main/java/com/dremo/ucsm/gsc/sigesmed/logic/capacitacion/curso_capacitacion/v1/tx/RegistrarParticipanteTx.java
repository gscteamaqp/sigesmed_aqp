package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.ParticipanteCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.PersonaCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.SedeCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.HorarioSedeCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.ParticipanteCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.ParticipanteCapacitacionId;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.SedeCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.json.JSONArray;
import org.json.JSONObject;

public class RegistrarParticipanteTx implements ITransaction {
    
    private static final Logger logger = Logger.getLogger(RegistrarParticipanteTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data =  (JSONObject) wr.getData();
            int sedCod = data.getInt("sedCod");
            int usuMod = data.getInt("usu");
            
            PersonaCapacitacionDao personaCapacitacionDao = (PersonaCapacitacionDao) FactoryDao.buildDao("capacitacion.PersonaCapacitacionDao");
            Persona persona = personaCapacitacionDao.buscarPorId(usuMod);
            
            String usuNom = persona.getApePat() + " " + persona.getApeMat() + " " + persona.getNom();
            String usuCor = persona.getEmail();
            
            SedeCapacitacionDao sedeCapacitacionDao = (SedeCapacitacionDao) FactoryDao.buildDao("capacitacion.SedeCapacitacionDao");
            SedeCapacitacion sede = sedeCapacitacionDao.buscarSedeRegPar(sedCod);
            
            String curNom = sede.getCursoCapacitacion().getNom();
            
            DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            String curFecIni = format.format(sede.getCronograma().getFecIni());
            String curFecFin = format.format(sede.getCronograma().getFecFin());
            
            String curHors = "";
            for (HorarioSedeCapacitacion horario : sede.getHorarios())
                curHors += getDate(horario.getDia(), horario.getTurIni(), horario.getTurFin()) + ", ";
            
            curHors = curHors.substring(0, curHors.length()-2);
            
            Properties proMen = System.getProperties();
            proMen.put("mail.smtp.auth", "true");
            proMen.put("mail.smtp.starttls.enable", "true");
            proMen.put("mail.smtp.host", "smtp.gmail.com");
            proMen.put("mail.smtp.port", "587");
            
            Session sesion = Session.getInstance(proMen,
                                                new javax.mail.Authenticator() {
                                                    @Override
                                                    protected PasswordAuthentication getPasswordAuthentication() {
                                                        return new PasswordAuthentication("consultingglobalsystem@gmail.com", "@globalsystem.4321");
                                                    }
                                                });
            
            ParticipanteCapacitacionDao participanteCapacitacionDao = (ParticipanteCapacitacionDao) FactoryDao.buildDao("capacitacion.ParticipanteCapacitacionDao");

            switch(data.getString("opt")) {
                case "Selección":
                    JSONArray array = data.getJSONArray("parCur");
                    for(int i = 0;i < array.length();i++) {
                        int codPar = array.getJSONObject(i).getInt("cod");
                        String corPar = array.getJSONObject(i).getString("cor");
                        
                        MimeMessage mensaje = new MimeMessage(sesion);
                        mensaje.addRecipient(Message.RecipientType.TO, new InternetAddress(corPar));
                        mensaje.setSubject("TIGER: Invitación Capacitación");
                        
                        String urlGen = data.getString("pro") + "://" + data.getString("ip") + data.getString("url") + "#/confirm?i=" + sedCod + "?¿" + codPar + "?¿" + "0";
                        mensaje.setContent(mensajeSeleccion(usuNom, usuCor, curNom, curFecIni, curFecFin, urlGen, curHors),"text/html");
                        Transport.send(mensaje);
                        
                        ParticipanteCapacitacionId id = new ParticipanteCapacitacionId(sedCod,codPar);
                        ParticipanteCapacitacion participante = new ParticipanteCapacitacion(id, new Date(), new Date(), 'E', usuMod);
                        participanteCapacitacionDao.insert(participante); 
                    }
                    
                    break;
                    
                case "Público":
                    if(data.getBoolean("tipMod")) {
                        int codPar = data.getInt("parCod");
                        String codGen = generarCodigo(6);
                        String urlGen = data.getString("pro") + "://" + data.getString("ip") + data.getString("url") + "#/confirm?i=";
                        urlGen += codGen.substring(0,3) + "?¿" + sedCod + "?¿" + codPar + "?¿" + codGen.substring(3) + "?¿" + "1S";
                        
                        MimeMessage mensaje = new MimeMessage(sesion);
                        mensaje.addRecipient(Message.RecipientType.TO, new InternetAddress(data.getString("parCor")));
                        mensaje.setSubject("TIGER: Invitación Capacitación");
                        mensaje.setContent(mensajePublico(usuNom, usuCor, curNom, curFecIni, curFecFin, curHors, urlGen, codGen),"text/html");
                        Transport.send(mensaje);
                        
                        ParticipanteCapacitacionId id = new ParticipanteCapacitacionId(sedCod, codPar);
                        ParticipanteCapacitacion participante = new ParticipanteCapacitacion(id, new Date(), new Date(), 'E', usuMod);
                        //Agregamos el Rol que tendra este Participante
                        int rol_id = data.getInt("rolId");
                        participante.setRol_id(rol_id);
                        participanteCapacitacionDao.insert(participante); 
                    } else {
                        // Le asignamos un Rol
                        String rol_id = Integer.toString(data.getInt("rolId"));
                        String codGen = generarCodigo(6);
                        String urlGen = data.getString("pro") + "://" + data.getString("ip") + data.getString("url") + "#/confirm?i=";
                        urlGen += codGen.substring(0,3) + "?¿" + sedCod + "?¿" + 0 + "?¿" + codGen.substring(3) + "?¿" + "1N?¿" + data.getString("parCor")+"?¿"+ rol_id;

                        MimeMessage mensaje = new MimeMessage(sesion);
                        mensaje.addRecipient(Message.RecipientType.TO, new InternetAddress(data.getString("parCor")));
                        mensaje.setSubject("TIGER: Invitación Capacitación");
                        mensaje.setContent(mensajePublico(usuNom, usuCor, curNom, curFecIni, curFecFin, curHors, urlGen, codGen),"text/html");
                        Transport.send(mensaje);
                    }
                    
                    break;
            }
            
            return WebResponse.crearWebResponseExito("Los participantes fueron registrados correctamente", WebResponse.OK_RESPONSE);
        } catch (Exception e){
            logger.log(Level.SEVERE,"registrarParticipante",e);
            return WebResponse.crearWebResponseError("No se pudo registrar a los participantes", WebResponse.BAD_RESPONSE);
        } 
    }    
        
    private String mensajePublico(String usuNom, String usuCor, String curNom, String curFecIni, String curFecFin, String curHors, String urlGen, String codGen) {
      return 
            "<div>"
                + "<div><img src=\"http://www.minedu.gob.pe/minedu/img/logo_ministerio_educacion.png\" alt=\"interactive connection\" width=\"250\"/></div>"
                + "<div>"
                    + "<h2>Datos del Contacto</h2>"
                        + "<table>"
                            + "<tbody>"
                                + "<tr>"
                                    + "<td lang=\"es\"><strong>Nombres y Apellidos: </strong></td>"
                                    + "<td lang=\"es\">" + usuNom + "</td>"
                                + "</tr>"
                                + "<tr>"
                                    + "<td lang=\"es\"><strong>Correo Electrónico: </strong></td>"
                                    + "<td lang=\"es\">" + usuCor + "</td>"
                                + "</tr>"
                            + "</tbody>"
                        + "</table>"
                + "</div>"
                + "<div>"
                    + "<h2 lang=\"es\">Datos de la Capacitación</h2>"
                    + "<table>"
                        + "<tbody>"
                            + "<tr>"
                                + "<td lang=\"es\"><strong>Nombre de la Capacitación: </strong></td>"
                                + "<td lang=\"es\">" + curNom + "</td>"
                            + "</tr>"
                            + "<tr>"
                                + "<td lang=\"es\"><strong>Fecha de Inicio: </strong></td>"
                                + "<td lang=\"es\">" + curFecIni + "</td>"
                            + "</tr>"
                            + "<tr>"
                                + "<td lang=\"es\"><strong>Fecha de Fin: </strong></td>"
                                + "<td lang=\"es\">" + curFecFin + "</td>"
                            + "</tr>"
                        + "</tbody>"
                    + "</table>"
                + "</div>"
                + "<div>"
                    + "<p lang=\"es\">Usted queda cordialmente invitado a participar en esta capacitación pública. El/Los horario(s) es/son el/los siguiente(s): " + curHors + ". Si usted desea participar deberá; acceder a la url que se adjunta y posteriormente completar los campos que se mostrarán a continuación:<br/><br/> "
                        + "Su código es: " + codGen + "<br><br> URL: <a href=\"" + urlGen + "\">Click aquí</a></p>"
                + "</div>"
            + "</div>";
    }
    
    private String mensajeSeleccion(String usuNom, String usuCor, String curNom, String curFecIni, String curFecFin, String urlGen, String curHors) {
      return 
            "<div>"
                + "<div><img src=\"http://www.minedu.gob.pe/minedu/img/logo_ministerio_educacion.png\" alt=\"interactive connection\" width=\"250\"/></div>"
                + "<div>"
                    + "<h2>Datos del Contacto</h2>"
                        + "<table>"
                            + "<tbody>"
                                + "<tr>"
                                    + "<td lang=\"es\"><strong>Nombres y Apellidos: </strong></td>"
                                    + "<td lang=\"es\">" + usuNom + "</td>"
                                + "</tr>"
                                + "<tr>"
                                    + "<td lang=\"es\"><strong>Correo Electrónico: </strong></td>"
                                    + "<td lang=\"es\">" + usuCor + "</td>"
                                + "</tr>"
                            + "</tbody>"
                        + "</table>"
                + "</div>"
                + "<div>"
                    + "<h2 lang=\"es\">Datos de la Capacitación</h2>"
                    + "<table>"
                        + "<tbody>"
                            + "<tr>"
                                + "<td lang=\"es\"><strong>Nombre de la Capacitación: </strong></td>"
                                + "<td lang=\"es\">" + curNom + "</td>"
                            + "</tr>"
                            + "<tr>"
                                + "<td lang=\"es\"><strong>Fecha de Inicio: </strong></td>"
                                + "<td lang=\"es\">" + curFecIni + "</td>"
                            + "</tr>"
                            + "<tr>"
                                + "<td lang=\"es\"><strong>Fecha de Fin: </strong></td>"
                                + "<td lang=\"es\">" + curFecFin + "</td>"
                            + "</tr>"
                        + "</tbody>"
                    + "</table>"
                + "</div>"
                + "<div>"
                    + "<p lang=\"es\">Usted fue seleccionado para participar en esta capacitación, con un tiempo de duración (" + curFecIni + " - " + curFecFin + ") bajo el/los siguiente(s) horario(s): " + curHors + ". <br><br> "
                        + "Si usted desea participar en dicha capacitación deberá acceder a la URL a acontinuación, no olvidarse de leer los términos y condiciones.<br/><br/> URL: <a href=\"" + urlGen + "\">Click aquí</a></p>"
                + "</div>"
            + "</div>";
    }
        
    private String generarCodigo(int longitud) {
        String resultado = "";
        for(int i = 0;i < longitud;i++)
            resultado += String.valueOf((int)(Math.random()*10));

        return resultado;
    }
    
    private String getDate(int day, Date ini, Date fin) {
        String name = "";
        DateFormat format = new SimpleDateFormat("HH:mm");

        switch (day) {
            case 1:
                name = "Domingo " + format.format(ini) + " - " + format.format(fin);
                break;
            case 2:
                name = "Lunes " + format.format(ini) + " - " + format.format(fin);
                break;
            case 3:
                name = "Martes " + format.format(ini) + " - " + format.format(fin);
                break;
            case 4:
                name = "Miércoles " + format.format(ini) + " - " + format.format(fin);
                break;
            case 5:
                name = "Jueves " + format.format(ini) + " - " + format.format(fin);
                break;
            case 6:
                name = "Viernes " + format.format(ini) + " - " + format.format(fin);
                break;
            case 7:
                name = "Sábado " + format.format(ini) + " - " + format.format(fin);
                break;
        }

        return name;
    }
}
