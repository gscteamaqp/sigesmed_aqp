package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.ma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.NotaEvaluacionIndicadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.NotaEvaluacionIndicador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.NotaIndicador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.GradoIEEstudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.IndicadorAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.IndicadoresSesionAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Periodo;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import java.util.List;
import org.hibernate.criterion.CriteriaSpecification;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Administrador on 16/01/2017.
 */
public class NotaEvaluacionIndicadorDaoHibernate extends GenericDaoHibernate<NotaEvaluacionIndicador> implements NotaEvaluacionIndicadorDao{
    @Override
    public List<NotaIndicador> listarNotasIndicadorAlumno(int idSes, int idGradAlumno) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String sql = "SELECT nei.notEvaIndId as notevaindid,ind.indAprId as indaprid,ind.nomInd as nomind, nei.gradoEstudiante.graOrgEstId as graieestid, nei.tipNot as tipnot, nei.notaEva as notaeva" +
                    " FROM IndicadorAprendizaje ind" +
                    " INNER JOIN ind.indicadoresSesion isa" +
                    " INNER JOIN isa.sesion sa" +
                    " LEFT OUTER JOIN ind.notasEvaluacion nei" +
                    " WHERE sa.sesAprId =:idSesion and (nei.sesion.sesAprId =:idSesion or nei.sesion.sesAprId is null) and (nei.gradoEstudiante.graOrgEstId =:idGraEst or nei.gradoEstudiante.graOrgEstId is null)";
            Query query = session.createQuery(sql);
            query.setInteger("idSesion",idSes);
            query.setInteger("idGraEst",idGradAlumno);
            query.setResultTransformer(Transformers.aliasToBean(NotaIndicador.class));
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<GradoIEEstudiante> listarEstudiantesGradoActual(int idOrg, int idGrad, Character idSecc) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT gie FROM GradoIEEstudiante gie" +
                    " INNER JOIN FETCH gie.matriculaEstudiante m INNER JOIN  m.estudiante e" +
                    " INNER JOIN e.persona p" +
                    " WHERE m.ie.orgId =:orgId AND m.act=:mAct AND gie.act =:graMatAct AND gie.grado.graId =:graId AND gie.seccion.sedId =:idSecc ORDER BY p.apePat ASC";
            Query query = session.createQuery(hql);
            query.setInteger("orgId",idOrg);
            query.setBoolean("mAct",true);
            query.setBoolean("graMatAct",true);
            query.setInteger("graId", idGrad);
            query.setCharacter("idSecc",idSecc);
            query.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public NotaEvaluacionIndicador buscarNotaIndicadorEstudiante(int idSesion, int idInd,int idGraOrEst,char idPerido,int numPer) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql;Query query;
            if(idSesion == 0){
                  hql = "SELECT nei FROM NotaEvaluacionIndicador nei" +
                    " WHERE nei.indicador.indAprId =:indId" +
                    " AND nei.gradoEstudiante.graOrgEstId =:graId AND nei.periodo.perId =:idPer AND nei.numPer =:perNum";
                    query = session.createQuery(hql);
                    
            }else{
                  hql = "SELECT nei FROM NotaEvaluacionIndicador nei" +
                    " WHERE nei.sesion.sesAprId =:sesId AND nei.indicador.indAprId =:indId" +
                    " AND nei.gradoEstudiante.graOrgEstId =:graId AND nei.periodo.perId =:idPer AND nei.numPer =:perNum";
                   query = session.createQuery(hql);
                   query.setInteger("sesId",idSesion);
            }
          
            query.setInteger("indId",idInd);
            query.setInteger("graId",idGraOrEst);
            query.setCharacter("idPer", idPerido);
            query.setInteger("perNum",numPer);

            query.setMaxResults(1);
            return (NotaEvaluacionIndicador)query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
    
    

    @Override
    public NotaEvaluacionIndicador buscarNotaPorId(int idNota) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT nei FROM NotaEvaluacionIndicador nei" +
                    " WHERE nei.notEvaIndId =:notaId";
            Query query = session.createQuery(hql);
            query.setInteger("notaId",idNota);
            query.setMaxResults(1);
            return (NotaEvaluacionIndicador)query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<IndicadorAprendizaje> listarIndicadoresPeriodo(int idPer, int idorg, int idDoc, int idPLan,int idGra,int idAre) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT DISTINCT i.* FROM pedagogico.indicador_aprendizaje i\n" +
                    "  INNER JOIN pedagogico.indicadores_unidad_aprendizaje iu ON i.ind_apr_id = iu.ind_apr_id\n" +
                    "  INNER JOIN pedagogico.unidad_didactica u ON iu.uni_did_id = u.uni_did_id\n" +
                    "  INNER JOIN pedagogico.programacion_anual pa ON u.pro_anu_id = pa.pro_anu_id\n" +
                    "WHERE u.per_pla_est_id =:idPer AND pa.pla_est_id =:plaEst AND pa.doc_id =:idDoc AND pa.org_id =:idOrg AND pa.gra_id =:idGra AND pa.are_cur_id =:idArea";
            SQLQuery query = session.createSQLQuery(hql);
            query.addEntity("i",IndicadorAprendizaje.class);
            query.addFetch("c","i","cap");
            query.setInteger("idPer",idPer);
            query.setInteger("plaEst",idPLan);
            query.setInteger("idDoc",idDoc);
            query.setInteger("idOrg",idorg);
            query.setInteger("idGra",idGra);
            query.setInteger("idArea",idAre);
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public Periodo buscarPeriodoId(char periodo) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT p FROM Periodo p" +
                    " WHERE p.perId =:perId";
            Query query = session.createQuery(hql);
            query.setCharacter("perId", periodo);

            query.setMaxResults(1);
            return (Periodo)query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public JSONArray buscarNotasxIndicadorxEstudiante(int idInd, int idGraOrEst, char idPeriodo, int numPer) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT nei.indicador.indAprId, nei.notaEva FROM NotaEvaluacionIndicador nei" +
                    " WHERE nei.indicador.indAprId =:indId" + 
                    " AND nei.gradoEstudiante.graOrgEstId =:graId" +
                    " AND nei.periodo.perId =:idPer AND nei.numPer =:perNum";
            Query query = session.createQuery(hql);
            query.setInteger("indId",idInd);
            query.setInteger("graId",idGraOrEst);
            query.setCharacter("idPer", idPeriodo);
            query.setInteger("perNum",numPer);
            
            List<Object[]> resultado = query.list();
            JSONArray notas = new JSONArray();
            
            for(Object[] r:resultado){
                JSONObject aux = new JSONObject();
                aux.put("indId", Integer.parseInt(r[0].toString()));
                aux.put("nota", r[1].toString());
                
                notas.put(aux);
            }

            return notas;
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public JSONArray buscarIndicadoresxSesionxEstudiante(int idSesion, int idGraOrEst, char idPeriodo, int numPer) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT nei.indicador.indAprId FROM NotaEvaluacionIndicador nei" +
                    " WHERE nei.sesion.sesAprId =:sesId AND nei.gradoEstudiante.graOrgEstId =:graId" +
                    " AND nei.periodo.perId =:idPer AND nei.numPer =:perNum";
            Query query = session.createQuery(hql);
            query.setInteger("sesId",idSesion);
            query.setInteger("graId",idGraOrEst);
            query.setCharacter("idPer", idPeriodo);
            query.setInteger("perNum",numPer);
            
            List<Integer> resultado = query.list();
            JSONArray indicadores = new JSONArray();
            /*
            for(Integer r:resultado){
                JSONObject aux = new JSONObject();
                aux.put("indId", Integer.parseInt(r[0].toString()));
                indicadores.put(aux);
            }*/
            
            for(int i=0 ; i<resultado.size();i++){
                JSONObject aux = new JSONObject();
                aux.put("indId", Integer.parseInt(resultado.get(i).toString()));
                indicadores.put(aux);
            }

            return indicadores;
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public Boolean buscarNota(int idSesion, int idInd, int idGraOrEst, char idPerido, int numPer) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Query query;String hql;
            if(idSesion == 0){
                     hql = "SELECT nei.notEvaIndId FROM NotaEvaluacionIndicador nei" +
                    " WHERE  nei.indicador.indAprId =:indId" +
                    " AND nei.gradoEstudiante.graOrgEstId =:graId AND nei.periodo.perId =:idPer AND nei.numPer =:perNum";
                    query = session.createQuery(hql);
            }else{
                    hql = "SELECT nei.notEvaIndId FROM NotaEvaluacionIndicador nei" +
                    " WHERE nei.sesion.sesAprId =:sesId AND nei.indicador.indAprId =:indId" +
                    " AND nei.gradoEstudiante.graOrgEstId =:graId AND nei.periodo.perId =:idPer AND nei.numPer =:perNum";
                    query = session.createQuery(hql);
                    query.setInteger("sesId",idSesion);
            }
            query.setInteger("indId",idInd);
            query.setInteger("graId",idGraOrEst);
            query.setCharacter("idPer", idPerido);
            query.setInteger("perNum",numPer);

            query.setMaxResults(1);
            
            Boolean resultado = false;
            
            if (query.uniqueResult() != null)
                resultado = true;
            
            return resultado;
            
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
    
    @Override
    public IndicadoresSesionAprendizaje obtenerCompetenciaDeIndicador(int indId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String sql = "SELECT indSesApr FROM IndicadoresSesionAprendizaje as indSesApr" +
                    " join fetch indSesApr.indicador as ind" + 
                    " join fetch indSesApr.competencia as com" + 
                    " WHERE ind.indAprId=:indId AND ind.estReg = 'A'";
            Query query = session.createQuery(sql);
            query.setInteger("indId",indId);
            query.setMaxResults(1);
            IndicadoresSesionAprendizaje indicadorSesApr = (IndicadoresSesionAprendizaje)query.uniqueResult();
            System.out.println(indicadorSesApr);
            return indicadorSesApr;
            
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<IndicadoresSesionAprendizaje> obtenerIndicadoresDeCompetencia(int comId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String sql = "SELECT indSesApr FROM IndicadoresSesionAprendizaje as indSesApr" +
                    " join fetch indSesApr.indicador as ind" + 
                    " join fetch indSesApr.competencia as com" + 
                    " WHERE com.comId=:comId AND com.estReg = 'A'";
            Query query = session.createQuery(sql);
            query.setInteger("comId",comId);
            
            List<IndicadoresSesionAprendizaje> indicadorSesApr = query.list();
            System.out.println(indicadorSesApr);
            return indicadorSesApr;
            
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<NotaEvaluacionIndicador> buscarNotaIndicadoresEstudiante(int idInd, int idGraOrEst, char idPerido, int numPer) {
           // Lista las Notas sin considerar la Sesion de Aprendizaje
        
         Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql;Query query;
            
                  hql = "SELECT nei FROM NotaEvaluacionIndicador nei" +
                    " WHERE nei.indicador.indAprId =:indId" +
                    " AND nei.gradoEstudiante.graOrgEstId =:graId AND nei.periodo.perId =:idPer AND nei.numPer =:perNum";
                    query = session.createQuery(hql);
                    query.setInteger("indId",idInd);
                    query.setInteger("graId",idGraOrEst);
                    query.setCharacter("idPer", idPerido);
                    query.setInteger("perNum",numPer);

            
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
}
