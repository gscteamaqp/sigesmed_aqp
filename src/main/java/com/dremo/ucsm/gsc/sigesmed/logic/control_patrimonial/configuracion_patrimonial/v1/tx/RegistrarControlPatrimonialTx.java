/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.configuracion_patrimonial.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.ControlPatrimonial;

import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.ControlPatrimonialDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.GrupoGenericoDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.GrupoGenerico;
import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class RegistrarControlPatrimonialTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {

        JSONObject requestData = (JSONObject) wr.getData();
        ControlPatrimonial cp = null;
        FileJsonObject newFile = null;
        String fileName = "";
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {

            int org_id = requestData.getInt("org_id");
            int per_id = requestData.getInt("per_id");
            String per_res = requestData.getString("per_res");
            Date fec_ini = formatter.parse(requestData.getString("fec_ini"));
            Date fec_cie = formatter.parse(requestData.getString("fec_cie"));
            String obs = requestData.getString("obs");
            Date fec_mod = new Date();
            int usu_mod = requestData.getInt("usu_mod");
            char est_reg = 'A';

            JSONObject doc = requestData.getJSONObject("documentoAdj");
            JSONObject jsonArchivo = doc.optJSONObject("archivo");

            cp = new ControlPatrimonial(0, org_id, per_id, per_res, fec_ini, fec_cie, obs, fec_mod, usu_mod, est_reg);

            if (jsonArchivo != null && jsonArchivo.length() > 0) {
                newFile = new FileJsonObject(jsonArchivo);
                SimpleDateFormat fileFormatDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String newFileDate = fileFormatDate.format(new Date());
                fileName = newFileDate + "-" + newFile.getName();
                BuildFile.buildFromBase64("control_patrimonial", fileName, newFile.getData());
                cp.setFile_name(fileName);
            }

            ControlPatrimonialDAO cp_dao = (ControlPatrimonialDAO) FactoryDao.buildDao("scp.ControlPatrimonialDAO");

            cp_dao.insert(cp);

        } catch (Exception e) {
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar el Control Patrimonial , datos incorrectos", e.getMessage());
        }

        JSONObject oResponse = new JSONObject();
        int id_cp = cp.getCon_pat_id();
        oResponse.put("cp_id", id_cp);

        return WebResponse.crearWebResponseExito("El registro  del Control Patrimonial , se realizo correctamente", oResponse);

        //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
