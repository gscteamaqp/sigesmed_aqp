/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.ma.notas.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.ma.notas.v1.tx.*;

/**
 *
 * @author ucsm
 */
public class ComponentRegister implements IComponentRegister{
    @Override

    public WebComponent createComponent() {
        WebComponent maComponent = new WebComponent(Sigesmed.SUBMODULO_ACADEMICO);
        maComponent.setName("notas_estudiante");
        maComponent.setVersion(1);
        maComponent.addTransactionGET("listarCompetenciasPeriodo",ListarCompetenciasPeriodoTx.class);
        maComponent.addTransactionGET("listarNotasRegistroAuxiliar",ListarNotasRegistroAuxiliarTx.class);
        maComponent.addTransactionGET("listarUnidadesDidacticas",ListarUnidadesDidacticasTx.class);
        maComponent.addTransactionGET("listarPeridosPlan",ListarPeriodoPlanTx.class);
        maComponent.addTransactionGET("listarNotasIndicadoresAlumnos",ListarNotasIndicadoresAlumnsTx.class);
        maComponent.addTransactionGET("listarIndicadoresUnidad",ListarIndicadoresUnidadTx.class);
        maComponent.addTransactionGET("reporteNotasArea",ReporteNotasAreaTx.class);
        maComponent.addTransactionGET("reporteNotasTutor",ReporteNotasTutorTx.class);
        maComponent.addTransactionPOST("registrarNotasIndicador", RegistrarNotasIndicadorTx.class);
        maComponent.addTransactionPOST("registrarNotasCompetencia", RegistrarNotasRegistroAuxiliarTx.class);
        maComponent.addTransactionPOST("registrarNotasArea", RegistrarNotasAreaTx.class);
        maComponent.addTransactionGET("listarNotasCompetencias", ListarNotasCompetenciaTx.class);
        maComponent.addTransactionGET("listarConfiguracionNota",ListarConfiguracionNotaTx.class);
        maComponent.addTransactionPOST("registrarPromedioIndicadores", RegistrarPromedioIndicadoresTx.class);
        maComponent.addTransactionPOST("registrarPromedioCompetencias", RegistrarPromedioCompetenciaTx.class);
        maComponent.addTransactionPOST("registrarPromedioAreaPorPeriodo", RegistrarPromedioAreaPorPeriodoTx.class);
        maComponent.addTransactionPOST("crudConfiguracionNota", CrudConfiguracionNotaTx.class);
        return maComponent;
    }
}
