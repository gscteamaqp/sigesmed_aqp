package com.dremo.ucsm.gsc.sigesmed.core.entity;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="usuario" , uniqueConstraints = @UniqueConstraint(columnNames="nom") )
public class Usuario  implements java.io.Serializable {

    @Id 
    @Column(name="usu_id", unique=true, nullable=false)
    private int usuId;
    @Column(name="nom", unique=true, nullable=false, length=32)
    private String nom;
    @Column(name="pas", length=16)
    private String pas;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_cre", nullable=false, length=29,updatable = false)
    private Date fecCre;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_mod", length=29)
    private Date fecMod;
    @Column(name="usu_mod", nullable=false)
    private int usuMod;
    @Column(name="est_reg", nullable=false, length=1)
    private char estReg;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="usu_id", nullable=false,updatable = false,insertable = false)
    private Persona persona;
    
    @OneToMany(mappedBy="usuario",cascade=CascadeType.ALL)
    private List<UsuarioSession> sessiones;
    
    @Column(name="cer_nom", length=50)
    private String nomCert;
    @Column(name="cer_pass",length=4)
    private String pasCert;
    
    public Usuario() {
    }
    public Usuario(int usuId) {
        this.usuId = usuId;
    }

	
    public Usuario(int usuId, String nom, Date fecCre, int usuMod, char estReg) {
        this.usuId = usuId;
        this.nom = nom;
        this.fecCre = fecCre;
        this.usuMod = usuMod;
        this.estReg = estReg;
    }
    public Usuario(int usuId, String nom, String pas, Date fecCre, Date fecMod, int usuMod, char estReg) {
       this.usuId = usuId;
       this.nom = nom;
       this.pas = pas;
       this.fecCre = fecCre;
       this.fecMod = fecMod;
       this.usuMod = usuMod;
       this.estReg = estReg;
    }
   
     
    public int getUsuId() {
        return this.usuId;
    }
    
    public void setUsuId(int usuId) {
        this.usuId = usuId;
    }
    
    public String getNom() {
        return this.nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }

    
    public String getPas() {
        return this.pas;
    }
    
    public void setPas(String pas) {
        this.pas = pas;
    }

    public Date getFecCre() {
        return this.fecCre;
    }
    
    public void setFecCre(Date fecCre) {
        this.fecCre = fecCre;
    }

    public Date getFecMod() {
        return this.fecMod;
    }
    
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    
    public int getUsuMod() {
        return this.usuMod;
    }
    
    public void setUsuMod(int usuMod) {
        this.usuMod = usuMod;
    }

    
    public char getEstReg() {
        return this.estReg;
    }    
    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }
    public String getPasCert() {
        return this.pasCert;
    }
    
    public void setPasCert(String pasCert) {
        this.pasCert = pasCert;
    }
     public String getNomCert() {
        return this.nomCert;
    }
    
    public void setNomCert(String nomCert) {
        this.nomCert = nomCert;
    }
    
    public Persona getPersona() {
        return this.persona;
    }
    public void setPersona(Persona persona) {
        this.persona = persona;
    }
    
    public List<UsuarioSession> getSessiones() {
        return this.sessiones;
    }
    public void setSessiones(List<UsuarioSession> sessiones) {
        this.sessiones = sessiones;
    }

}


