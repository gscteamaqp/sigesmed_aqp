/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.util;

import com.itextpdf.io.source.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Base64;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFHeader;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFPicture;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Header;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.IOUtils;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Matricula;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Jerson Herrera
 */
public class ReportXls {

    private static final String URI_EXCEL;
    private static final HSSFWorkbook BOOK_MODEL;
    private static final Font FONT_TITLE_1;
    private static final CellStyle STYLE_TITLE_1;
    private static final Font FONT_TITLE_2;
    private static final CellStyle STYLE_TITLE_2;
    private static final Font FONT_SUBTITLE;
    private static final CellStyle STYLE_SUBTITLE;
    private static final Font FONT_SUBTITLE_BOLD;
    private static final CellStyle STYLE_SUBTITLE_BOLD;
    private static final Font FONT_DATE;
    private static final CellStyle STYLE_DATE;
    private static final HSSFCellStyle STYLE_HEADER_COLUMN;
    private static final HSSFFont FONT_HEADER_COLUMN;
    private static final HSSFCellStyle STYLE_CONTENT_COLUMN;
    private static final HSSFFont FONT_CONTENT_COLUMN;

    private HSSFWorkbook book;
    private HSSFSheet sheet;
    private Header header;
    private HSSFRow row1;
    private HSSFRow row4;

    static {

        URI_EXCEL = "data:application/vnd.ms-excel;base64,";
        //create book as a model of fonts and styles        
        BOOK_MODEL = new HSSFWorkbook();
        //create font title1
        FONT_TITLE_1 = BOOK_MODEL.createFont();
        FONT_TITLE_1.setFontHeightInPoints((short) 9);
        FONT_TITLE_1.setFontName("Helvetica");
        FONT_TITLE_1.setBold(true);
        STYLE_TITLE_1 = BOOK_MODEL.createCellStyle();
        STYLE_TITLE_1.setFont(FONT_TITLE_1);
        //create font title2 "REPORTE DE DIRECTORIO EXTERNO"
        FONT_TITLE_2 = BOOK_MODEL.createFont();
        FONT_TITLE_2.setFontHeightInPoints((short) 15);
        FONT_TITLE_2.setFontName("fontTimes");
        FONT_TITLE_2.setBold(true);
        STYLE_TITLE_2 = BOOK_MODEL.createCellStyle();
        STYLE_TITLE_2.setFont(FONT_TITLE_2);
        //create font subtitle 
        FONT_SUBTITLE = BOOK_MODEL.createFont();
        FONT_SUBTITLE.setFontHeightInPoints((short) 10);
        FONT_SUBTITLE.setFontName("fontTimes");
        STYLE_SUBTITLE = BOOK_MODEL.createCellStyle();
        STYLE_SUBTITLE.setFont(FONT_SUBTITLE);
        //create font subtitle bold
        FONT_SUBTITLE_BOLD = BOOK_MODEL.createFont();
        FONT_SUBTITLE_BOLD.setFontHeightInPoints((short) 10);
        FONT_SUBTITLE_BOLD.setFontName("fontTimes");
        FONT_SUBTITLE_BOLD.setBold(true);
        STYLE_SUBTITLE_BOLD = BOOK_MODEL.createCellStyle();
        STYLE_SUBTITLE_BOLD.setFont(FONT_SUBTITLE_BOLD);
        //create font date 
        FONT_DATE = BOOK_MODEL.createFont();
        FONT_DATE.setFontHeightInPoints((short) 8);
        FONT_DATE.setFontName("fontTimes");
        STYLE_DATE = BOOK_MODEL.createCellStyle();
        STYLE_DATE.setFont(FONT_DATE);
        //create font header column
        STYLE_HEADER_COLUMN = BOOK_MODEL.createCellStyle();
        STYLE_HEADER_COLUMN.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
        STYLE_HEADER_COLUMN.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        STYLE_HEADER_COLUMN.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        STYLE_HEADER_COLUMN.setBorderTop(HSSFCellStyle.BORDER_THIN);
        STYLE_HEADER_COLUMN.setBorderRight(HSSFCellStyle.BORDER_THIN);
        STYLE_HEADER_COLUMN.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        STYLE_HEADER_COLUMN.setAlignment(CellStyle.ALIGN_CENTER);
        STYLE_HEADER_COLUMN.setWrapText(true);
        FONT_HEADER_COLUMN = BOOK_MODEL.createFont();
        FONT_HEADER_COLUMN.setColor(HSSFColor.BLACK.index);
        STYLE_HEADER_COLUMN.setFont(FONT_HEADER_COLUMN);
        //create font content column
        STYLE_CONTENT_COLUMN = BOOK_MODEL.createCellStyle();
        STYLE_CONTENT_COLUMN.setFillForegroundColor(HSSFColor.WHITE.index);
        STYLE_CONTENT_COLUMN.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        STYLE_CONTENT_COLUMN.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        STYLE_CONTENT_COLUMN.setBorderTop(HSSFCellStyle.BORDER_THIN);
        STYLE_CONTENT_COLUMN.setBorderRight(HSSFCellStyle.BORDER_THIN);
        STYLE_CONTENT_COLUMN.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        STYLE_CONTENT_COLUMN.setAlignment(CellStyle.ALIGN_CENTER);
        STYLE_CONTENT_COLUMN.setWrapText(true);
        FONT_CONTENT_COLUMN = BOOK_MODEL.createFont();
        FONT_HEADER_COLUMN.setColor(HSSFColor.BLACK.index);
        STYLE_CONTENT_COLUMN.setFont(FONT_CONTENT_COLUMN);
    }

    public ReportXls() {
        book = new HSSFWorkbook();
        sheet = book.createSheet();
        row1 = sheet.createRow(0);
        row4 = sheet.createRow(3);

    }

    public void addHeader(String StrHeader) {
        header = sheet.getHeader();
        header.setCenter(HSSFHeader.font("Helvetica", "Bold")
                + HSSFHeader.fontSize((short) 9) + StrHeader);
    }

    public void setTitle1(String strTitle1) {
        HSSFCell cellTitle1 = row1.createCell(9);
        cellTitle1.setCellValue(strTitle1);
//        cellTitle1.setCellStyle(STYLE_TITLE_1);
        HSSFCellStyle newStyle = book.createCellStyle();
        newStyle.cloneStyleFrom(STYLE_TITLE_1);
        cellTitle1.setCellStyle(newStyle);

    }

    public void setTitle2(String strTitle2) {
        HSSFCell cellTitle2 = row4.createCell(7);
        cellTitle2.setCellValue(strTitle2);
//        cellTitle2.setCellStyle(STYLE_TITLE_2);
        HSSFCellStyle newStyle = book.createCellStyle();
        newStyle.cloneStyleFrom(STYLE_TITLE_2);
        cellTitle2.setCellStyle(newStyle);

    }

    public void setSubtitles(JSONObject object) {
        Iterator iter = object.keys();
        String key = "";
        String value = "";
        int cont = 0;
        while (iter.hasNext()) {
            key = (String) iter.next();
            value = object.getString(key);
            HSSFRow row10x = sheet.createRow(9 + cont);
            HSSFCell cellSubTitleKey1 = row10x.createCell(2);
            cellSubTitleKey1.setCellValue(key);
//          cellSubTitleKey1.setCellStyle(STYLE_SUBTITLE_BOLD);
            HSSFCellStyle newStyle = book.createCellStyle();
            newStyle.cloneStyleFrom(STYLE_SUBTITLE_BOLD);
            cellSubTitleKey1.setCellStyle(newStyle);
            HSSFCell cellSubTitleValue1 = row10x.createCell(5);
            cellSubTitleValue1.setCellValue(value);
//          cellSubTitleValue1.setCellStyle(STYLE_SUBTITLE);
            HSSFCellStyle newStyle2 = book.createCellStyle();
            newStyle2.cloneStyleFrom(STYLE_SUBTITLE);
            cellSubTitleValue1.setCellStyle(newStyle2);
            cont++;
        }
    }

    public void setImageTop(String strPathImage) {
        InputStream my_banner_image = null;
        byte[] bytes = null;
        int my_picture_id = 0;
        try {
            my_banner_image = new FileInputStream(strPathImage);
            bytes = IOUtils.toByteArray(my_banner_image);
            my_picture_id = book.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
            my_banner_image.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReportXls.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReportXls.class.getName()).log(Level.SEVERE, null, ex);
        }
        HSSFPatriarch drawing = sheet.createDrawingPatriarch();
        ClientAnchor my_anchor = new HSSFClientAnchor();
        my_anchor.setCol1(1);
        my_anchor.setRow1(1);
        HSSFPicture my_picture = drawing.createPicture(my_anchor, my_picture_id);
        my_picture.resize();

    }

    public void setDate() {
        SimpleDateFormat formato
                = new SimpleDateFormat("dd/MM/yyyy", new Locale("es", "ES"));
        String fecha = formato.format(new Date());
        HSSFRow row6 = sheet.createRow(5);
        HSSFCell cellFecha = row6.createCell(13);
        cellFecha.setCellValue(fecha);
//        cellFecha.setCellStyle(STYLE_DATE);
        HSSFCellStyle newStyle = book.createCellStyle();
        newStyle.cloneStyleFrom(STYLE_DATE);
        cellFecha.setCellStyle(newStyle);
    }

    public void fillData(String[] titles, String[] keys, String[] pesos, List data) {
        int cont = 0;
        int dim = 0;
        HSSFRow row18 = sheet.createRow(17);
        for (String object : titles) {
            //sheet.addMergedRegion(new CellRangeAddress(17, 17, 2 + 4 * cont, 5 + 4 * cont));
            sheet.addMergedRegion(new CellRangeAddress(17, 17, 2 + dim, 2 + Integer.parseInt(pesos[cont]) + dim - 1));
//            HSSFCell cellHeaderColumn = row18.createCell(2 + 4 * cont);
            HSSFCell cellHeaderColumn = row18.createCell(2 + dim);
            cellHeaderColumn.setCellValue(object);
            //cellHeaderColumn.setCellStyle(STYLE_HEADER_COLUMN);
            HSSFCellStyle styleHeaderColumn = book.createCellStyle();
            styleHeaderColumn.cloneStyleFrom(STYLE_HEADER_COLUMN);
            cellHeaderColumn.setCellStyle(styleHeaderColumn);
            //HSSFCell cellHeaderColumnLast = row18.createCell(5 + 4 * cont);
            HSSFCell cellHeaderColumnLast = row18.createCell(2 + Integer.parseInt(pesos[cont]) + dim - 1);
            //cellHeaderColumnLast.setCellStyle(STYLE_HEADER_COLUMN);
            cellHeaderColumnLast.setCellStyle(styleHeaderColumn);
            dim = Integer.parseInt(pesos[cont]) + dim;
            cont++;

        }
        cont = 0;

        for (Object object : data) {
            Map row = (Map) object;
            HSSFRow row19x = sheet.createRow(18 + cont);
            int cont2 = 0;
            dim = 0;
            String valueCell = "";
            for (int i = 0; i < keys.length; ++i) {
                if (row.get(keys[cont2]) == null) {
                    valueCell = "";
                } else {
                    valueCell = row.get(keys[cont2]).toString();
                }
//              sheet.addMergedRegion(new CellRangeAddress(18 + cont, 18 + cont, 2 + 4 * cont2, 5 + 4 * cont2));
                sheet.addMergedRegion(new CellRangeAddress(18 + cont, 18 + cont, 2 + dim, 2 + Integer.parseInt(pesos[i]) + dim - 1));
                //HSSFCell cellContentColumn = row19x.createCell(2 + 4 * cont2);
                HSSFCell cellContentColumn = row19x.createCell(2 + dim);
                cellContentColumn.setCellValue(valueCell);
                //cellContentColumn.setCellStyle(STYLE_CONTENT_COLUMN);
                HSSFCellStyle styleContentColumn = book.createCellStyle();
                styleContentColumn.cloneStyleFrom(STYLE_CONTENT_COLUMN);
                cellContentColumn.setCellStyle(styleContentColumn);
                //HSSFCell cellContentColumnLast = row19x.createCell(5 + 4 * cont2);
                HSSFCell cellContentColumnLast = row19x.createCell(2 + Integer.parseInt(pesos[i]) + dim - 1);
                //cellContentColumnLast.setCellStyle(STYLE_CONTENT_COLUMN);
                cellContentColumnLast.setCellStyle(styleContentColumn);
                dim = Integer.parseInt(pesos[i]) + dim;
                cont2++;
            }
            cont++;
        }
    }

    public void fillData2(String[] titles, String[] keys, String[] pesos, List data) {
        int cont = 0;
        int dim = 0;
        HSSFRow row18 = sheet.createRow(17);
        for (String object : titles) {
            //sheet.addMergedRegion(new CellRangeAddress(17, 17, 2 + 4 * cont, 5 + 4 * cont));
            sheet.addMergedRegion(new CellRangeAddress(17, 17, 2 + dim, 2 + Integer.parseInt(pesos[cont]) + dim - 1));
//            HSSFCell cellHeaderColumn = row18.createCell(2 + 4 * cont);
            HSSFCell cellHeaderColumn = row18.createCell(2 + dim);
            cellHeaderColumn.setCellValue(object);
            //cellHeaderColumn.setCellStyle(STYLE_HEADER_COLUMN);
            HSSFCellStyle styleHeaderColumn = book.createCellStyle();
            styleHeaderColumn.cloneStyleFrom(STYLE_HEADER_COLUMN);
            cellHeaderColumn.setCellStyle(styleHeaderColumn);
            //HSSFCell cellHeaderColumnLast = row18.createCell(5 + 4 * cont);
            HSSFCell cellHeaderColumnLast = row18.createCell(2 + Integer.parseInt(pesos[cont]) + dim - 1);
            //cellHeaderColumnLast.setCellStyle(STYLE_HEADER_COLUMN);
            cellHeaderColumnLast.setCellStyle(styleHeaderColumn);
            dim = Integer.parseInt(pesos[cont]) + dim;
            cont++;

        }
        cont = 0;

        for (Object object : data) {
            Object[] row = (Object[]) object;
//          Map row = (Map) object;
            HSSFRow row19x = sheet.createRow(18 + cont);
            int cont2 = 0;
            dim = 0;
            String valueCell = "";
            for (int i = 0; i < keys.length; ++i) {
                if (row[cont2] == null) {
                    valueCell = "";
                } else {
                    valueCell = row[cont2].toString();
                }
//              sheet.addMergedRegion(new CellRangeAddress(18 + cont, 18 + cont, 2 + 4 * cont2, 5 + 4 * cont2));
                sheet.addMergedRegion(new CellRangeAddress(18 + cont, 18 + cont, 2 + dim, 2 + Integer.parseInt(pesos[i]) + dim - 1));
                //HSSFCell cellContentColumn = row19x.createCell(2 + 4 * cont2);
                HSSFCell cellContentColumn = row19x.createCell(2 + dim);
                cellContentColumn.setCellValue(valueCell);
                //cellContentColumn.setCellStyle(STYLE_CONTENT_COLUMN);
                HSSFCellStyle styleContentColumn = book.createCellStyle();
                styleContentColumn.cloneStyleFrom(STYLE_CONTENT_COLUMN);
                cellContentColumn.setCellStyle(styleContentColumn);
                //HSSFCell cellContentColumnLast = row19x.createCell(5 + 4 * cont2);
                HSSFCell cellContentColumnLast = row19x.createCell(2 + Integer.parseInt(pesos[i]) + dim - 1);
                //cellContentColumnLast.setCellStyle(STYLE_CONTENT_COLUMN);
                cellContentColumnLast.setCellStyle(styleContentColumn);
                dim = Integer.parseInt(pesos[i]) + dim;
                cont2++;
            }
            cont++;
        }
    }

    public void fillData3(String[] titles, JSONArray campos, String[] pesos, List data, String tipo) {
        //tipo puede ser "Es" o "Pa", haciendo referencia a Lista de Estdiantes o Lista de Padres, respectivamente
        int cont = 0;
        int dim = 0;
        HSSFRow row18 = sheet.createRow(17);
        for (String object : titles) {
            //sheet.addMergedRegion(new CellRangeAddress(17, 17, 2 + 4 * cont, 5 + 4 * cont));
            sheet.addMergedRegion(new CellRangeAddress(17, 17, 2 + dim, 2 + Integer.parseInt(pesos[cont]) + dim - 1));
//            HSSFCell cellHeaderColumn = row18.createCell(2 + 4 * cont);
            HSSFCell cellHeaderColumn = row18.createCell(2 + dim);
            cellHeaderColumn.setCellValue(object);
            //cellHeaderColumn.setCellStyle(STYLE_HEADER_COLUMN);
            HSSFCellStyle styleHeaderColumn = book.createCellStyle();
            styleHeaderColumn.cloneStyleFrom(STYLE_HEADER_COLUMN);
            cellHeaderColumn.setCellStyle(styleHeaderColumn);
            //HSSFCell cellHeaderColumnLast = row18.createCell(5 + 4 * cont);
            HSSFCell cellHeaderColumnLast = row18.createCell(2 + Integer.parseInt(pesos[cont]) + dim - 1);
            //cellHeaderColumnLast.setCellStyle(STYLE_HEADER_COLUMN);
            cellHeaderColumnLast.setCellStyle(styleHeaderColumn);
            dim = Integer.parseInt(pesos[cont]) + dim;
            cont++;

        }
        cont = 0;
        for (Object object : data) {
            Matricula matr = (Matricula) object;
            //Object[] row = (Object[]) rows;
            String[] _row = new String[campos.length()];
            int cont3 = 0;
            if (tipo.equals("Es")) {
                for (int c = 0; c < campos.length(); c++) {
                    System.out.println(campos.get(c));
                    if (campos.get(c).equals("p.dni")) {
                        _row[cont3] = matr.getEstudiante().getPersona().getDni();
                    } else if (campos.get(c).equals("p.ape_mat")) {
                        _row[cont3] = matr.getEstudiante().getPersona().getApeMat();
                    } else if (campos.get(c).equals("p.ape_pat")) {
                        _row[cont3] = matr.getEstudiante().getPersona().getApePat();
                    } else if (campos.get(c).equals("p.nom")) {
                        _row[cont3] = matr.getEstudiante().getPersona().getNom();
                    } else if (campos.get(c).equals("p.per_dir")) {
                        _row[cont3] = matr.getEstudiante().getPersona().getPerDir();
                    } else if (campos.get(c).equals("p.email")) {
                        _row[cont3] = matr.getEstudiante().getPersona().getEmail();
                    } else if (campos.get(c).equals("p.fij")) {
                        _row[cont3] = matr.getEstudiante().getPersona().getFij();
                    } else if (campos.get(c).equals("o.nom")) {
                        _row[cont3] = matr.getOrganizacion().getNom();
                    } else {
                        cont3--;
                    }
                    cont3++;
                }
            } else if (tipo.equals("Pa")) {
                for (int c = 0; c < campos.length(); c++) {
                    System.out.println(campos.get(c));
                    if (campos.get(c).equals("p.dni")) {
                        _row[cont3] = matr.getApoderado().getPersona().getDni();
                    } else if (campos.get(c).equals("p.ape_mat")) {
                        _row[cont3] = matr.getApoderado().getPersona().getApeMat();
                    } else if (campos.get(c).equals("p.ape_pat")) {
                        _row[cont3] = matr.getApoderado().getPersona().getApePat();
                    } else if (campos.get(c).equals("p.nom")) {
                        _row[cont3] = matr.getApoderado().getPersona().getNom();
                    } else if (campos.get(c).equals("p.per_dir")) {
                        _row[cont3] = matr.getApoderado().getPersona().getPerDir();
                    } else if (campos.get(c).equals("p.email")) {
                        _row[cont3] = matr.getApoderado().getPersona().getEmail();
                    } else if (campos.get(c).equals("p.fij")) {
                        _row[cont3] = matr.getApoderado().getPersona().getFij();
                    } else if (campos.get(c).equals("o.nom")) {
                        _row[cont3] = matr.getOrganizacion().getNom();
                    } else {
                        cont3--;
                    }
                    cont3++;
                }
            }

            HSSFRow row19x = sheet.createRow(18 + cont);
            int cont2 = 0;
            dim = 0;
            String valueCell = "";
            for (int i = 0; i < campos.length(); ++i) {
                if (_row[i] == null) {
                    valueCell = "";
                } else {
                    valueCell = _row[i];
                }
//              sheet.addMergedRegion(new CellRangeAddress(18 + cont, 18 + cont, 2 + 4 * cont2, 5 + 4 * cont2));
                sheet.addMergedRegion(new CellRangeAddress(18 + cont, 18 + cont, 2 + dim, 2 + Integer.parseInt(pesos[i]) + dim - 1));
                //HSSFCell cellContentColumn = row19x.createCell(2 + 4 * cont2);
                HSSFCell cellContentColumn = row19x.createCell(2 + dim);
                cellContentColumn.setCellValue(valueCell);
                //cellContentColumn.setCellStyle(STYLE_CONTENT_COLUMN);
                HSSFCellStyle styleContentColumn = book.createCellStyle();
                styleContentColumn.cloneStyleFrom(STYLE_CONTENT_COLUMN);
                cellContentColumn.setCellStyle(styleContentColumn);
                //HSSFCell cellContentColumnLast = row19x.createCell(5 + 4 * cont2);
                HSSFCell cellContentColumnLast = row19x.createCell(2 + Integer.parseInt(pesos[i]) + dim - 1);
                //cellContentColumnLast.setCellStyle(STYLE_CONTENT_COLUMN);
                cellContentColumnLast.setCellStyle(styleContentColumn);
                dim = Integer.parseInt(pesos[i]) + dim;
                cont2++;

            }
            cont++;

        }

    }

    public void closeReport() {
        try {
            book.close();

        } catch (IOException ex) {
            Logger.getLogger(ReportXls.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String encodeToBase64() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] xls = null;
        try {
            book.write(baos);
            xls = baos.toByteArray();
        } catch (IOException ex) {
            Logger.getLogger(ReportXls.class.getName()).log(Level.SEVERE, null, ex);
        }
        return URI_EXCEL + Base64.encodeBase64String(xls);
    }
}
