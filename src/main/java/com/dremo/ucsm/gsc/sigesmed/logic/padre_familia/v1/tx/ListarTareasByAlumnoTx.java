/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.padre_familia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx.*;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mpf.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.TareaEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.Matricula;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.BandejaTarea;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.TareaDocumento;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Carlos
 */
public class ListarTareasByAlumnoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int areaId = 0;
        Long alumnoID;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            
            areaId = requestData.getInt("area");
            alumnoID = requestData.getLong("matriculaID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listar las tareas por alumno, datos incorrectos", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        EstudianteDao estudianteDao = (EstudianteDao)FactoryDao.buildDao("mpf.EstudianteDao");
        
        
        List<BandejaTarea> tareas = null;
        Matricula alumnoMat=null;
        
        TareaEscolarDao tareaDao = (TareaEscolarDao)FactoryDao.buildDao("web.TareaEscolarDao");
        try{
            alumnoMat=estudianteDao.getDatosMatriculaAndPlan(alumnoID);
            tareas = estudianteDao.buscarTareasPorAlumnoAndArea(alumnoMat.getPlanNivel().getPlaEstId(),alumnoMat.getEstudianteId(),areaId);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las tareas por alumno"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar las tareas por alumno", e.getMessage() );
        }
        //Fin        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        List<Integer> tareasFueraTiempo = new ArrayList<Integer>();
        int i = 0;
        
        Date hoy = new Date();//fecha y hora actual
        for(BandejaTarea tarea:tareas ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("bandejaTareaID",tarea.getBanTarId() );
            oResponse.put("nota",tarea.getNota());
            oResponse.put("alumnoID",tarea.getAlumnoId());
            
            oResponse.put("planID",tarea.getTareaEscolar().getPlaEstId() );
            oResponse.put("tareaID",tarea.getTareaEscolar().getTarEscId() );
            oResponse.put("nombre",tarea.getTareaEscolar().getNom());
            oResponse.put("descripcion",tarea.getTareaEscolar().getDes());
            oResponse.put("numeroDoc",tarea.getTareaEscolar().getNumDoc());
            oResponse.put("adjunto",tarea.getTareaEscolar().getDocAdj());
            oResponse.put("docenteID",tarea.getTareaEscolar().getUsuMod());
            oResponse.put("url",Sigesmed.UBI_ARCHIVOS+Tarea.TAREA_PATH);
            oResponse.put("areaID",tarea.getTareaEscolar().getAreCurId());
            oResponse.put("fechaEnvio",sf.format(tarea.getTareaEscolar().getFecEnv()) );
            oResponse.put("fechaMaxEntrega",sf.format(tarea.getTareaEscolar().getFecEnt()) );
            
            JSONArray aDocumentos = new JSONArray();
            for(TareaDocumento documento : tarea.getDocumentos()){
                JSONObject oDocumento = new JSONObject();
                oDocumento.put("tareaDocumentoID",documento.getTarDocId() );
                oDocumento.put("documento",documento.getNomDoc());
                oDocumento.put("url",Sigesmed.UBI_ARCHIVOS+Tarea.BANDEJA_TAREA_PATH);
                
                aDocumentos.put(oDocumento);
            }            
            oResponse.put("documentos", aDocumentos);
            
            if(tarea.getFecEnt()!=null)
                oResponse.put("fechaEntrega",sf.format(tarea.getFecEnt()) );
            if(tarea.getFecVis()!=null)
                oResponse.put("fechaRevicion",sf.format(tarea.getFecVis()) );
            
            //verificamos que las tareas esten en el tiempo indicado para ser resueltas
            if(tarea.getEstado()==Tarea.ESTADO_NUEVO && hoy.compareTo(tarea.getTareaEscolar().getFecEnt()) > 0 ){
                tareasFueraTiempo.add(tarea.getBanTarId());
                oResponse.put("estado",""+Tarea.ESTADO_FUERA_TIEMPO);
            }
            else
                oResponse.put("estado",""+tarea.getEstado());
            
            oResponse.put("i",i++);
            
            miArray.put(oResponse);
        }
        
        //actualizando el estado de las tareas a finalizado
        if(tareasFueraTiempo.size()>0)
            tareaDao.tareasFueraDeTiempo(tareasFueraTiempo);
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente las tareas por alumno",miArray);        
        //Fin
    }
    
}

