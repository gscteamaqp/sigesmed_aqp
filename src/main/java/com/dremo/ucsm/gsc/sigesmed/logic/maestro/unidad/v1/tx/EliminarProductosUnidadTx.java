package com.dremo.ucsm.gsc.sigesmed.logic.maestro.unidad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.UnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 14/12/2016.
 */
public class EliminarProductosUnidadTx implements ITransaction{
    private static Logger logger = Logger.getLogger(EliminarProductosUnidadTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idProducto = data.getInt("id");
        return eliminarMaterialUnidadDidactica(idProducto);
    }

    private WebResponse eliminarMaterialUnidadDidactica(int idProducto) {
        try{
            UnidadDidacticaDao unidadDao = (UnidadDidacticaDao) FactoryDao.buildDao("maestro.plan.UnidadDidacticaDao");
            unidadDao.eliminarProductoUnidad(idProducto);
            return WebResponse.crearWebResponseExito("Exito al eliminar el producto");
        }catch (Exception e){
            logger.log(Level.SEVERE,"eliminarMaterialUnidadDidactica",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el producto");
        }
    }
}
