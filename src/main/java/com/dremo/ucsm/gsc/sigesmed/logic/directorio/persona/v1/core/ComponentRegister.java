/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.directorio.persona.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.directorio.persona.v1.tx.ActualizarPersonaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.directorio.persona.v1.tx.BuscarPersonaxDniTx;
import com.dremo.ucsm.gsc.sigesmed.logic.directorio.persona.v1.tx.EliminarPersonaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.directorio.persona.v1.tx.InsertarPersonaTx;


/**
 *
 * @author ucsm
 */

public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_DIRECTORIO);        
        
        //Registrando el Nombre del componente
        component.setName("persona");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionPOST("insertarPersona", InsertarPersonaTx.class);      
        component.addTransactionDELETE("eliminarPersona", EliminarPersonaTx.class);
        component.addTransactionPUT("actualizarPersona", ActualizarPersonaTx.class);
        component.addTransactionGET("buscarPersonaxDni", BuscarPersonaxDniTx.class);
        //component.addTransactionGET("listarParientePorPersona", ListarParientesPorPersonaTx.class);
               
        
        return component;
    }
}
