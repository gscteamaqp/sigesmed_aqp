package com.dremo.ucsm.gsc.sigesmed.core.entity.mep;
// Generated 15/07/2016 01:43:01 PM by Hibernate Tools 4.3.1


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Type;

/**
 * @author geank
 */
@Entity
@Table(name="ficha_evaluacion_personal",schema="pedagogico")
public class FichaEvaluacionPersonal  implements java.io.Serializable {

    
    @Id 
    @Column(name="fic_eva_per_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_ficha_evaluacion", sequenceName="pedagogico.ficha_evaluacion_personal_fic_eva_per_id_seq" )
    @GeneratedValue(generator="secuencia_ficha_evaluacion")
    private int ficEvaPerId;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="crg_tra_ide", nullable=false)
    private TrabajadorCargo tipTra;
    
    @Column(name="nom", nullable=false, length=100)
    private String nom;
    
    @Temporal(TemporalType.DATE)
    @Column(name="fec_cre", nullable=false, length=13)
    private Date fecCre;
    
    @Column(name="des", nullable=false)
    private String des;
    
    @Column(name="com")
    private Integer com;
    
    @Column(name="usu_mod")
    private Integer usuMod;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_mod", length=29)
    private Date fecMod;
    
    @Column(name="est_reg", length=1)
    private Character estReg;
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy="fichaEvaluacionPersonal", cascade=CascadeType.ALL)
    private Set<EscalaValoracionFicha> escalaValoracionFichas = new HashSet(0);
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy="fichaEvaluacionPersonal",cascade=CascadeType.ALL)
    private Set<RangoPorcentualFicha> rangoPorcentualFichas = new HashSet(0);
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy="fichaEvaluacionPersonal",cascade=CascadeType.ALL)
    //@Fetch(value = FetchMode.JOIN)
    private Set<ContenidoFichaEvaluacion> contenidoFichaEvaluacions = new HashSet(0);
    
    @Temporal(TemporalType.DATE)
    @Column(name="vig", nullable=true, length=13)
    private Date vigencia;
//    
    public FichaEvaluacionPersonal() {
    }

    public FichaEvaluacionPersonal(String nom, Date fecCre, String des) {
        this.nom = nom;
        this.fecCre = fecCre;
        this.des = des;

        this.setFecMod(new Date());
        this.setEstReg('A');
    }
    public FichaEvaluacionPersonal(String nom, Date fecCre, String des, Date vigencia) {
        this.nom = nom;
        this.fecCre = fecCre;
        this.des = des;
        this.vigencia = vigencia;
                
        this.setFecMod(new Date());
        this.setEstReg('A');
    }
    public FichaEvaluacionPersonal(TrabajadorCargo tipTra, String nom, Date fecCre, String des) {
        this.tipTra = tipTra;
        this.nom = nom;
        this.fecCre = fecCre;
        this.des = des;

        this.setFecMod(new Date());
        this.setEstReg('A');
    }
    public FichaEvaluacionPersonal(TrabajadorCargo tipTra, String nom, Date fecCre, String des, Date vigencia) {
        this.tipTra = tipTra;
        this.nom = nom;
        this.fecCre = fecCre;
        this.des = des;
        this.vigencia = vigencia;
        this.setFecMod(new Date());
        this.setEstReg('A');
    }
    
    public FichaEvaluacionPersonal(int ficEvaPerId, String des) {
        this.ficEvaPerId = ficEvaPerId;
        this.des = des;

        this.setFecMod(new Date());
        this.setEstReg('A');
    }
    public FichaEvaluacionPersonal(int ficEvaPerId, TrabajadorCargo tipTra, String nom, Date fecCre, String des) {
        this.ficEvaPerId = ficEvaPerId;
        this.tipTra = tipTra;
        this.nom = nom;
        this.fecCre = fecCre;
        this.des = des;

        this.setFecMod(new Date());
        this.setEstReg('A');
    }
    public FichaEvaluacionPersonal(int ficEvaPerId, TrabajadorCargo tipTra, String nom, Date fecCre, String des, Integer com, Integer usuMod, Date fecMod, Character estReg, Set escalaValoracionFichas, Set rangoPorcentualFichas, Set contenidoFichaEvaluacions) {
        this.ficEvaPerId = ficEvaPerId;
        this.tipTra = tipTra;
        this.nom = nom;
        this.fecCre = fecCre;
        this.des = des;
        this.com = com;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
        this.escalaValoracionFichas = escalaValoracionFichas;
        this.rangoPorcentualFichas = rangoPorcentualFichas;
        this.contenidoFichaEvaluacions = contenidoFichaEvaluacions;

        this.setFecMod(new Date());
        this.setEstReg('A');
    }

    public int getFicEvaPerId() {
        return this.ficEvaPerId;
    }
    
    public void setFicEvaPerId(int ficEvaPerId) {
        this.ficEvaPerId = ficEvaPerId;
    }

    
   
    public TrabajadorCargo getTipTra() {
        return this.tipTra;
    }
    
    public void setTipTra(TrabajadorCargo tipTra) {
        this.tipTra = tipTra;
    }

    
    
    public String getNom() {
        return this.nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }

    
    public Date getFecCre() {
        return this.fecCre;
    }
    
    public void setFecCre(Date fecCre) {
        this.fecCre = fecCre;
    }

    
    
    public String getDes() {
        return this.des;
    }
    
    public void setDes(String des) {
        this.des = des;
    }

    
    
    public Integer getCom() {
        return this.com;
    }
    
    public void setCom(Integer com) {
        this.com = com;
    }
    
    public Integer getUsuMod() {
        return this.usuMod;
    }
    
    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }
    
    public Date getFecMod() {
        return this.fecMod;
    }
    
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    
    
    public Character getEstReg() {
        return this.estReg;
    }
    
    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    
    public Set<EscalaValoracionFicha> getEscalaValoracionFichas() {
        return this.escalaValoracionFichas;
    }
    
    public void setEscalaValoracionFichas(Set<EscalaValoracionFicha> escalaValoracionFichas) {
        this.escalaValoracionFichas = escalaValoracionFichas;
    }

    
    public Set<RangoPorcentualFicha> getRangoPorcentualFichas() {
        return this.rangoPorcentualFichas;
    }
    
    public void setRangoPorcentualFichas(Set<RangoPorcentualFicha> rangoPorcentualFichas) {
        this.rangoPorcentualFichas = rangoPorcentualFichas;
    }

   
    public Set<ContenidoFichaEvaluacion> getContenidoFichaEvaluacions() {
        return this.contenidoFichaEvaluacions;
    }
    
    public void setContenidoFichaEvaluacions(Set<ContenidoFichaEvaluacion> contenidoFichaEvaluacions) {
        this.contenidoFichaEvaluacions = contenidoFichaEvaluacions;
    }
    public boolean verificarRelaciones(){
        escalaValoracionFichas.size();
        rangoPorcentualFichas.size();
        contenidoFichaEvaluacions.size();
        return true;
    }
    
    public Date getVigencia() {
        return this.vigencia;
    }
    
    public void setVigencia(Date vigencia) {
        this.vigencia = vigencia;
    }
    
    public void copyFromOther(FichaEvaluacionPersonal fep){
        setNom(fep.getNom());
        setDes(fep.des);
    }
    @Override
    public String toString(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy");
        return "{\"id\":" + ficEvaPerId +",\"tip\":\"" + tipTra.getCarNom() +"\"" + ",\"tipcod\":" + tipTra.getTraCarId()  +
                ",\"nom\":\"" + nom +"\"" + ",\"des\":\"" + des +"\"" + ",\"fcre\":\"" + sdf.format(fecCre) +"\"}";
    }
    @Override
    public boolean equals(Object o){
        if(this == o)return true;
        else if(o!= null && o instanceof FichaEvaluacionPersonal){
            FichaEvaluacionPersonal fic = (FichaEvaluacionPersonal)o;
            if(fic.getTipTra() == this.getTipTra()) return true;
            else return false;
        }
        else{
            return false;
        }
    }

}


