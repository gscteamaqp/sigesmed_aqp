/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.ma;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.DynamicUpdate;

/**
 *
 * @author ucsm
 */
@Entity
@Table(name="articulo_escolar", schema="pedagogico")
@DynamicUpdate(value = true)
public class ArticuloEscolar implements java.io.Serializable{
    @Id
    @Column(name="art_esc_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_articulo_escolar", sequenceName="pedagogico.articulo_escolar_art_esc_id_seq" )
    @GeneratedValue(generator="secuencia_articulo_escolar")
    private int artEscId;
    
    @Column(name="tip_art_esc", length = 25)
    private String tipoArticulo;
    
    @Column(name="nom_art_esc", length = 25)
    private String nomArticulo;
    
    @Column(name="des_art_esc", length = 50)
    private String desArticulo;
    
    @Column(name="res_min_art", length = 20)
    private String resMinArticulo;
    
    @Column (name="pre_art_esc")
    private Double preArticulo;
    
    @Column(name = "usu_mod")
    private Integer usuMod;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length=29)
    private Date fecMod;

    @Column(name = "est_reg",length = 1)
    private Character estReg;
    
    @OneToMany (mappedBy = "articulo" , fetch = FetchType.LAZY)
    private List<ListaArticulo> listas = new ArrayList();
    
    public ArticuloEscolar() {
    }

    public ArticuloEscolar(String tipoArticulo, String nombreArticulo,Double preArticulo, String desArticulo, String resMinArticulo) {
        this.tipoArticulo = tipoArticulo;
        this.nomArticulo = nombreArticulo;
        this.desArticulo = desArticulo;
        this.resMinArticulo = resMinArticulo;
        this.preArticulo = preArticulo;
    }
    

    public ArticuloEscolar(String tipoArticulo, String nombreArticulo, String desArticulo, String resMinArticulo, Double preArticulo, Integer usuMod, Date fecMod, Character estReg) {
        this.tipoArticulo = tipoArticulo;
        this.nomArticulo = nombreArticulo;
        this.desArticulo = desArticulo;
        this.resMinArticulo = resMinArticulo;
        this.preArticulo = preArticulo;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
    }

    public int getArtEscId() {
        return artEscId;
    }

    public void setArtEscId(int artEscId) {
        this.artEscId = artEscId;
    }

    public String getTipoArticulo() {
        return tipoArticulo;
    }

    public void setTipoArticulo(String tipoArticulo) {
        this.tipoArticulo = tipoArticulo;
    }

    public String getNomArticulo() {
        return nomArticulo;
    }

    public void setNomArticulo(String nombreArticulo) {
        this.nomArticulo = nombreArticulo;
    }

    public String getDesArticulo() {
        return desArticulo;
    }

    public void setDesArticulo(String desArticulo) {
        this.desArticulo = desArticulo;
    }

    public String getResMinArticulo() {
        return resMinArticulo;
    }

    public void setResMinArticulo(String resMinArticulo) {
        this.resMinArticulo = resMinArticulo;
    }

    public Double getPreArticulo() {
        return preArticulo;
    }

    public void setPreArticulo(Double preArticulo) {
        this.preArticulo = preArticulo;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
}
