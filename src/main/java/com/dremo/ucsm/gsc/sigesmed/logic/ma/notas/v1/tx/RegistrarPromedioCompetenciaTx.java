/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.ma.notas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.ConfiguracionNotaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.NotaEvaluacionIndicadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.RegistroAuxiliarCompetenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.RegistroAuxiliarDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.DocenteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.CompetenciaAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.IndicadorAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ConfiguracionNota;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.NotaEvaluacionIndicador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.RegistroAuxiliar;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.RegistroAuxiliarCompetencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.IndicadoresSesionAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import static com.dremo.ucsm.gsc.sigesmed.logic.ma.notas.v1.tx.MA_Constantes.*;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Yemi
 */
public class RegistrarPromedioCompetenciaTx implements ITransaction{

    private List<ConfiguracionNota> config_docente = null;
    private static final Logger logger = Logger.getLogger(RegistrarPromedioCompetenciaTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idSession = data.getInt("ses");
        char idPeriodo  = data.getString("per").charAt(0);
        int numPer  = data.getInt("numper");
        char tipNot = data.optString("tip", "N").charAt(0);
        int idUsr = data.getInt("usr");
        int org = data.getInt("org");
        
        int idArea = data.getInt("are");
        JSONArray array = data.optJSONArray("estudiantes");
        
        if(array == null) return WebResponse.crearWebResponseError("Se tiene que seleccionar a los alumnos");
        listarConfiguracionDocente(org , idUsr);
        return registrarNotas(idSession, idPeriodo, numPer, idUsr, idArea, tipNot, array);
    }

    private WebResponse registrarNotas(int idSession, char idPeriodo, int numPer, int idUsr, int idArea ,char tipNot, JSONArray array) {
        try{
            RegistroAuxiliarDao regisDao = (RegistroAuxiliarDao) FactoryDao.buildDao("ma.RegistroAuxiliarDao");
            RegistroAuxiliarCompetenciaDao regisAuxComDao = (RegistroAuxiliarCompetenciaDao) FactoryDao.buildDao("ma.RegistroAuxiliarCompetenciaDao");
            IndicadorAprendizajeDao indicadorDao = (IndicadorAprendizajeDao) FactoryDao.buildDao("maestro.plan.IndicadorAprendizajeDao");
            CompetenciaAprendizajeDao compDao = (CompetenciaAprendizajeDao) FactoryDao.buildDao("maestro.plan.CompetenciaAprendizajeDao");
            NotaEvaluacionIndicadorDao notaDao = (NotaEvaluacionIndicadorDao) FactoryDao.buildDao("ma.NotaEvaluacionIndicadorDao");
            DocenteDao docDao = (DocenteDao) FactoryDao.buildDao("maestro.DocenteDao");
            EstudianteDao estDao = (EstudianteDao) FactoryDao.buildDao("maestro.EstudianteDao");
            
            String mensaje = "Se registro el promedio de las competencias exitosamente";
            Docente docente = docDao.buscarDocentePorUsuario(idUsr);
            for(int i = 0; i < array.length(); i++){
                JSONObject alumno = array.getJSONObject(i);
                int idGraOrEst = alumno.getInt("idgra");
                JSONArray indicadoresdeSesion = notaDao.buscarIndicadoresxSesionxEstudiante(idSession, idGraOrEst, idPeriodo, numPer);
                for(int j = 0; j < indicadoresdeSesion.length(); j++){
                    int idInd = indicadoresdeSesion.getJSONObject(j).getInt("indId");
                    IndicadoresSesionAprendizaje indSesApr = notaDao.obtenerCompetenciaDeIndicador(idInd);
                    int comId = indSesApr.getCompetencia().getComId();
                    List<IndicadoresSesionAprendizaje> indicadoresAsociados = notaDao.obtenerIndicadoresDeCompetencia(comId);
                    
                    Double promedio = 0.0;
                    for(IndicadoresSesionAprendizaje indAso:indicadoresAsociados){
                        RegistroAuxiliar notFinInd = regisDao.buscarNotaEstudiante(indAso.getIndicador().getIndAprId(), docente.getDoc_id(), numPer, idArea, idGraOrEst);
                        if (notFinInd == null){
                            continue; //Si no encuentra el registro auxiliar el docente aun no uso dicho indicador
                        }
                            promedio += Double.parseDouble(notFinInd.getNotInd());
                    
                    }
                    
                    promedio = promedio/indicadoresAsociados.size();
                    promedio = (Math.round(promedio * 10.0) / 10.0);
                    String nota = "";
                    String nota_lit = obtenerNotaLiteral(promedio);
                    nota = String.valueOf(promedio);
                    
                   
                    
                    RegistroAuxiliarCompetencia notComp = new RegistroAuxiliarCompetencia();
                    if(!regisDao.buscarNotaFinalCompetencia(comId, idArea, numPer, idGraOrEst)){
                        notComp.setNota(nota);
                        notComp.setNot_com_lit(nota_lit);
                        //notComp.setAreaCurricular(docDao.buscarAreaPorId(idArea));
                        notComp.setComp(compDao.buscarCompetenciPorCodigo(comId));
                        //notComp.setGradoEst(estDao.buscarGradoIEEstudiante(idGraOrEst));
                       // notComp.setPeriodoPlanEstudios(regisDao.buscarPeriodoPorId(numPer));
                        notComp.setAre_cur_id(docDao.buscarAreaPorId(idArea).getAreCurId());
                        notComp.setGrad_ie_est_id(estDao.buscarGradoIEEstudiante(idGraOrEst).getGraOrgEstId());
                        notComp.setPer_eva_id(regisDao.buscarPeriodoPorId(numPer).getPerPlaEstId());
                        regisAuxComDao.insert(notComp);
                    } else {
                        notComp = regisDao.buscarNotaCompetenciaEsp(comId, idArea, numPer, idGraOrEst);
                        notComp.setNota(nota);
                        regisAuxComDao.update(notComp);
                    }
                }  
            }
            return WebResponse.crearWebResponseExito(mensaje);
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarPromedioCompetencia",e);
            return WebResponse.crearWebResponseError("No se pudo registrar el promedio de la competencia");
        }
    }
    public String obtenerNotaLiteral(Double not){
        
        for(ConfiguracionNota conf : config_docente){
            if(not >= conf.getConf_not_min() && not <= conf.getConf_not_max()){
                return conf.getConf_cod();
            }
        }
        return "";
    }
    public void listarConfiguracionDocente(int org_id , int idUser){
        ConfiguracionNotaDao configNot = (ConfiguracionNotaDao) FactoryDao.buildDao("ma.ConfiguracionNotaDao");
        config_docente = configNot.listarConfiguracionDocente(idUser, org_id);
    }
    
}
