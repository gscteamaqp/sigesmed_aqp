package com.dremo.ucsm.gsc.sigesmed.logic.maestro.sesion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.SesionAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.TareaIndicadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.me.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.SesionAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.TareaIndicador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.TareaSesionAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaTarea;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Alertas;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx.Tarea;
import com.dremo.ucsm.gsc.sigesmed.util.AlertManager;
import java.math.BigInteger;
import java.util.ArrayList;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;

/**
 * Created by Administrador on 15/12/2016.
 */
public class RegistrarTareaSesionTx implements ITransaction{
    private Logger logger = Logger.getLogger(RegistrarTareaSesionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject)wr.getData();
        int idUnidad = data.getInt("sesid");

        String nom= data.getString("nom").toUpperCase();
        String des = data.optString("des");
        long fec = data.optLong("fec");
        JSONArray ind_tar = data.getJSONArray("ind");
        String sec = data.getString("sec");
        String tipo = data.getString("tipo");
        TareaSesionAprendizaje tareaSesion = new TareaSesionAprendizaje(nom,new Date(fec),des);
        tareaSesion.setSec(sec);
        tareaSesion.setTipo(tipo);
        return registrarTareaSesion(idUnidad, tareaSesion , ind_tar);
    }

    private WebResponse registrarTareaSesion(int idSesion,TareaSesionAprendizaje tareaSesion,JSONArray ind_tar) {
        try{
            SesionAprendizajeDao sesionDao = (SesionAprendizajeDao) FactoryDao.buildDao("maestro.plan.SesionAprendizajeDao");
            SesionAprendizaje sesion= sesionDao.buscarSesionById(idSesion);
            tareaSesion.setSesion(sesion);
            tareaSesion.setFecMod(new Date());
            tareaSesion.setEstReg('A');
            tareaSesion.setUsuMod(1);
            tareaSesion.setTarea_indicador(new ArrayList<TareaIndicador>());
            
            sesionDao.registrarTareaSesion(tareaSesion);
             
            TareaIndicadorDao tar_ind_dao = (TareaIndicadorDao) FactoryDao.buildDao("maestro.plan.TareaIndicadorDao");
            TareaIndicador tar_indi = null; 
            
             int size = ind_tar.length();
             for( int i = 0 ; i < size; i++){
                   JSONObject bo = ind_tar.getJSONObject(i);
                   int cod_ind = bo.getInt("ind");
                   tar_indi = new TareaIndicador(0,cod_ind,tareaSesion.getTarIdSes());
                        
             } 
             tar_ind_dao.insert(tar_indi);
            
            JSONObject jsonTarea = new JSONObject(EntityUtil.objectToJSONString(
                    new String[]{"tarIdSes"},
                    new String[]{"id"},
                    tareaSesion
            ));
            
           
            
            
            return WebResponse.crearWebResponseExito("Se registro el material",jsonTarea);
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarMaterialUnidad",e);
            return WebResponse.crearWebResponseError("No se puede registrar el material");
        }
    }
    
}
    
