/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.padre_familia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.AsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.AreaCurricularDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mpf.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.AreaModel;

import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.EstudianteAsistenciaModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.ListaArticulo;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Carlos
 */
public class ReporteAsistenciaByAreaTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        Long matriculaID;
        Integer gradoId;
        Integer plaID;
        Integer areaId;
        Date desde;
        Date hasta;
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
       
        Integer org;
        try {
            JSONObject requestData = (JSONObject) wr.getData();
            org = requestData.getInt("organizacionID");
            matriculaID = requestData.getLong("matriculaID");
            areaId = requestData.getInt("area");
            gradoId = requestData.getInt("gradoID");
            plaID = requestData.getInt("planID");
            String fecDesde = requestData.getString("desde");
            String fecHasta = requestData.getString("hasta");

            desde = sf.parse(fecDesde);
            hasta = sf.parse(fecHasta);

        } catch (Exception e) {
            System.out.println("No se pudo verificar los datos \n" + e);
            return WebResponse.crearWebResponseError("No se pudo verificar los datos ", e.getMessage());
        }

        OrganizacionDao organizacionDao = (OrganizacionDao) FactoryDao.buildDao("OrganizacionDao");
        Organizacion organizacion = null;
        List<EstudianteAsistenciaModel> asistencias = null;
        EstudianteDao estudianteDao = (EstudianteDao) FactoryDao.buildDao("mpf.EstudianteDao");
        AreaModel area;
        try {
            organizacion = organizacionDao.load(Organizacion.class, org);
            asistencias = estudianteDao.buscarAsistenciaByAlumno(matriculaID, areaId, desde, hasta);
            area = estudianteDao.buscarAreasById(plaID, gradoId, areaId);
        } catch (Exception e) {
            System.out.println("No se pudo listar la asistencia \n" + e);
            return WebResponse.crearWebResponseError("No se pudo listar la asistencia ", e.getMessage());
        }

        //Creando el reporte....        
        Mitext m = null;
        try {
            m = new Mitext(false);
            m.agregarTitulo("REPORTE DE ASISTENCIA");
        } catch (Exception ex) {
            System.out.println("No se pudo crear el documento \n" + ex);
            Logger.getLogger(ReporteListaUtilesByAreaTx.class.getName()).log(Level.SEVERE, null, ex);
        }

        float[] columnWidthsD = {6, 6, 3};
        Table tabla = new Table(columnWidthsD);
        tabla.setWidthPercent(100);
        tabla.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph("INSTITUCION EDUCATIVA").setFontSize(11).setTextAlignment(TextAlignment.LEFT)));
        tabla.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph(" : " + organizacion.getNom()).setFontSize(11)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER));

        tabla.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph("COD").setFontSize(11).setTextAlignment(TextAlignment.LEFT)));
        tabla.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph(" : " + organizacion.getCod()).setFontSize(11)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER));

        tabla.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph("DIRECCION").setFontSize(11)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph(" : " + organizacion.getDir()).setFontSize(11)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER));

        tabla.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph("AREA").setFontSize(11)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph(" : " + area.getArea()).setFontSize(11)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER));

        tabla.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph("DOCENTE").setFontSize(11)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph(" : " + area.getNombresPM()).setFontSize(11)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER));

        float[] columnWidthsTit = {1, 4, 6, 8, 15};
        GTabla t = new GTabla(columnWidthsTit);
        String encabezados[] = {"N", "FECHA", "DIA", "ESTADO", "JUSTIFICACION"};
        String detalle[] = {"", "", "", "", ""};

        try {
            t.build(encabezados);
            t.setWidthPercent(100);
        } catch (IOException ex) {
            Logger.getLogger(ReporteListaUtilesByAreaTx.class.getName()).log(Level.SEVERE, null, ex);
        }

        int i = 0;
        int nAsis = 0;
        int nTjus = 0;
        int nTinj = 0;
        int nFjus = 0;
        int nFinj = 0;

        for (EstudianteAsistenciaModel a : asistencias) {

            detalle[0] = ++i + "";
            detalle[1] = sf.format(a.getFecAsi());

            Integer dayOfWeek;
            Calendar c = Calendar.getInstance();
            c.setTime(a.getFecAsi());
            dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

            switch (dayOfWeek) {
                case 1:
                    detalle[2] = "Domingo";
                    break;
                case 2:
                    detalle[2] = "Lunes";
                    break;
                case 3:
                    detalle[2] = "Martes";
                    break;
                case 4:
                    detalle[2] = "Miercoles";
                    break;
                case 5:
                    detalle[2] = "Jueves";
                    break;
                case 6:
                    detalle[2] = "Viernes";
                    break;
                case 7:
                    detalle[2] = "Sabado";
                    break;

            }

            switch (a.getEstAsi()) {
                case 0:
                    detalle[3] = "Asistio";
                    nAsis++;
                    break;
                case 1:
                    detalle[3] = "Falto";
                    if (a.getJusId() != null) {
                        nFinj++;
                    } else {
                        nFjus++;
                    }

                    break;
                case 2:
                    detalle[3] = "Tardanza";
                    if (a.getJusId() != null) {
                        nTinj++;
                    } else {
                        nTjus++;
                    }
                    break;
                default:
                    detalle[3] = "";
                    break;
            }

            if (a.getDesJus() == null) {
                detalle[4] = "";
            } else {
                detalle[4] = a.getDesJus();
            }

            t.processLine(detalle);
            detalle[0] = "";
            detalle[1] = "";
            detalle[2] = "";
            detalle[3] = "";
            detalle[4] = "";
        }

        float[] columnWidthsResumen = {6, 2, 1, 6, 2, 1, 6, 2, 1};
        Table tablaResumen = new Table(columnWidthsResumen);
        tablaResumen.setWidthPercent(100);
        tablaResumen.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph("Asistencias").setFontSize(10).setTextAlignment(TextAlignment.LEFT)));
        tablaResumen.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph(" : " + nAsis).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tablaResumen.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER));
        tablaResumen.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph("Tardanza Justificada").setFontSize(10).setTextAlignment(TextAlignment.LEFT)));
        tablaResumen.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph(" : " + nTjus).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tablaResumen.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER));
        tablaResumen.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph("Tardanza Injustificada").setFontSize(10).setTextAlignment(TextAlignment.LEFT)));
        tablaResumen.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph(" : " + nTinj).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tablaResumen.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER));

        tablaResumen.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER));
        tablaResumen.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER));
        tablaResumen.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER));
        tablaResumen.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph("Falta Justificada").setFontSize(10).setTextAlignment(TextAlignment.LEFT)));
        tablaResumen.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph(" : " + nFjus).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tablaResumen.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER));
        tablaResumen.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph("Falta Injustificada").setFontSize(10).setTextAlignment(TextAlignment.LEFT)));
        tablaResumen.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER).add(new Paragraph(" : " + nFinj).setFontSize(10)).setTextAlignment(TextAlignment.LEFT));
        tablaResumen.addCell(new Cell(1, 1).setBorder(Border.NO_BORDER));

        m.agregarTabla(tabla);
        m.agregarParrafo("");
        m.agregarParrafo("");
        m.agregarTabla(t);
        m.agregarParrafo("");
        m.agregarParrafo("");
        m.agregarParrafo("Resumen:");

        m.agregarTabla(tablaResumen);
        m.cerrarDocumento();

        JSONArray miArray = new JSONArray();
        JSONObject oResponse = new JSONObject();
        oResponse.put("datareporte", m.encodeToBase64());
        miArray.put(oResponse);

        return WebResponse.crearWebResponseExito("Se genero el reporte correctamente", miArray);

    }

}
