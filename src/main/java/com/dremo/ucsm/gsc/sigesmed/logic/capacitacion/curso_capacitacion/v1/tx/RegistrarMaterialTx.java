package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.CursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.MaterialCursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.CursoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.MaterialCursoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class RegistrarMaterialTx implements ITransaction {
    
    private static final Logger logger = Logger.getLogger(RegistrarMaterialTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            JSONObject data = (JSONObject) wr.getData();            
            int codCap = data.getInt("cod");

            CursoCapacitacionDao capacitacionDao = (CursoCapacitacionDao) FactoryDao.buildDao("capacitacion.CursoCapacitacionDao");
            CursoCapacitacion capacitacion = capacitacionDao.buscarPorId(codCap);
            MaterialCursoCapacitacionDao materialCursoCapacitacionDao = (MaterialCursoCapacitacionDao) FactoryDao.buildDao("capacitacion.MaterialCursoCapacitacionDao");
            
            JSONArray materiales = data.getJSONArray("materiales");
            
            for(int i = 0;i < materiales.length();i++) {
                MaterialCursoCapacitacion material = new MaterialCursoCapacitacion( materiales.getJSONObject(i).getString("des"),
                                                                                    new BigDecimal(materiales.getJSONObject(i).getDouble("cos")));
                material.setCursoCapacitacion(capacitacion);
                materialCursoCapacitacionDao.insert(material);                     
            }     
            
            return WebResponse.crearWebResponseExito("El material de la capacitación fue creado correctamente", WebResponse.OK_RESPONSE);
        } catch (Exception e){
            logger.log(Level.SEVERE,"registrarMaterial",e);
            return WebResponse.crearWebResponseError("No se pudo registrar el material de la capacitación",WebResponse.BAD_RESPONSE);
        }
    }    
}
