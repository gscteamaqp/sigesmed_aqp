package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.matricula_masiva.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.matricula_masiva.v1.tx.BuscarVariosEstudiantesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.matricula_masiva.v1.tx.GenerarMatriculaMasivaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.matricula_masiva.v1.tx.GenerarNominaMatriculaTx;

public class ComponentRegister implements IComponentRegister {

    @Override

    public WebComponent createComponent() {
        WebComponent maComponent = new WebComponent(Sigesmed.SUBMODULO_MATRICULA_INSTITUCIONAL);
        maComponent.setName("matriculaMasiva");
        maComponent.setVersion(1);
        maComponent.addTransactionGET("buscarVariosEstudiantes", BuscarVariosEstudiantesTx.class);
        maComponent.addTransactionPOST("generarMatriculaMasiva", GenerarMatriculaMasivaTx.class);
        maComponent.addTransactionPOST("generarNominaMatricula", GenerarNominaMatriculaTx.class);
        return maComponent;
    }
}
