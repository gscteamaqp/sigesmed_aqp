/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.cuadro_horas.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.cuadro_horas.v1.tx.ActualizarPlazaMagisterialTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.cuadro_horas.v1.tx.BuscarDistribucionPorDocenteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.cuadro_horas.v1.tx.BuscarDocenteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.cuadro_horas.v1.tx.InsertarDistribucionHoraGradoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.cuadro_horas.v1.tx.InsertarPlazaMagisterialTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.cuadro_horas.v1.tx.ListarDistribucionPlazasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.cuadro_horas.v1.tx.ListarPlazasPorOrganizacionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.cuadro_horas.v1.tx.ObtenerAulasPorOrganizacionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.cuadro_horas.v1.tx.ObtenerPeriodosPlanTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.cuadro_horas.v1.tx.ObtenerPlanEstudiosProgTx;

/**
 *
 * @author abel
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_ELABORACION_CUADRO_HORAS);        
        
        //Registrando el Nombre del componente
        component.setName("cuadroHoras");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionPOST("insertarPlazaMagisterial", InsertarPlazaMagisterialTx.class);
        component.addTransactionPUT("actualizarPlazaMagisterial", ActualizarPlazaMagisterialTx.class);
        component.addTransactionPOST("insertarDistribucionGrado", InsertarDistribucionHoraGradoTx.class);
        //component.addTransactionPOST("insertarDistribucionSeccion", InsertarDistribucionHoraSeccionTx.class);
        component.addTransactionGET("listarPlazasPorOrganizacion", ListarPlazasPorOrganizacionTx.class);
        
        component.addTransactionGET("listarDistribucionPlazas", ListarDistribucionPlazasTx.class);
        component.addTransactionGET("buscarDistribucionPorDocente", BuscarDistribucionPorDocenteTx.class);
        
        component.addTransactionGET("obtenerAulasPorOrganizacion", ObtenerAulasPorOrganizacionTx.class);
        
        component.addTransactionGET("buscarDocente", BuscarDocenteTx.class);
        component.addTransactionGET("obtener_plan_estudios", ObtenerPlanEstudiosProgTx.class);
        component.addTransactionGET("obtener_periodos_plan", ObtenerPeriodosPlanTx.class);
        
        return component;
    }
}
