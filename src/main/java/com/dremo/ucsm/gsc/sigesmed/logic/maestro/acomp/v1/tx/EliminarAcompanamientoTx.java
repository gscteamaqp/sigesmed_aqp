package com.dremo.ucsm.gsc.sigesmed.logic.maestro.acomp.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.acomp.AcompanamientoDocenteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.acomp.AcompanamientoDocente;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 06/01/2017.
 */
public class EliminarAcompanamientoTx implements ITransaction{
    private static Logger logger = Logger.getLogger(EliminarAcompanamientoTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data  = (JSONObject) wr.getData();
        int idAcomp = data.getInt("id");
        return eliminarAcompanamiento(idAcomp);
    }

    private WebResponse eliminarAcompanamiento(int idAcomp) {
        try{
            AcompanamientoDocenteDao acompDao = (AcompanamientoDocenteDao) FactoryDao.buildDao("maestro.acomp.AcompanamientoDocenteDao");
            AcompanamientoDocente acomp = acompDao.buscarAcompanamiento(idAcomp);
            acompDao.deleteAbsolute(acomp);

            return WebResponse.crearWebResponseExito("Se elimino correctamente el acompanamiento");
        }catch (Exception e){
            logger.log(Level.SEVERE,"eliminarAcompanamiento",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el acompanamiento");        }
    }
}
