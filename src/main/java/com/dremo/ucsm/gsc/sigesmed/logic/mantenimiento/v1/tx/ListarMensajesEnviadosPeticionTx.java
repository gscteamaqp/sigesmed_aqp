/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.mantenimiento.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mnt.MensajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mnt.PeticionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mnt.SoporteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mnt.Mensaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mnt.Peticion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mnt.Soporte;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Mensajes;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ListarMensajesEnviadosPeticionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {        
        int usuarioID=0;
        try{
            JSONObject requestData=(JSONObject)wr.getData();
            usuarioID=requestData.getInt("usuarioID");
        }catch(Exception e){
             System.out.println("No se pudo listar los mensajes enviados: " +e.getMessage());
            return WebResponse.crearWebResponseError("No se pudo listar los mensajes enviados",e.getMessage());
        }
        
   
        
        List<Mensaje> enviados=null;
        MensajeDao menDao=(MensajeDao)FactoryDao.buildDao("mnt.MensajeDao");
//        int sesionID=menDao.buscarSessionPorIdUsuario(usuarioID);
        
        
        try{
            enviados=menDao.buscarMensajesEnviadosPorSession(usuarioID);
        }catch(Exception e){
            System.out.println("No se pudo listar los mensajes \n"+e.getMessage());
            return WebResponse.crearWebResponseError("No se pudo listar los mensajes", e.getMessage());
        }
        
        JSONArray miArray=new JSONArray();
        
        PeticionDao petDao=(PeticionDao)FactoryDao.buildDao("mnt.PeticionDao");
        SoporteDao sopDao=(SoporteDao)FactoryDao.buildDao("mnt.SoporteDao");
        SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
        char temp;
        for(Mensaje m: enviados){
            JSONObject oResponse=new JSONObject();            
            oResponse.put("mensajeID",m.getMensajeID());
            oResponse.put("asunto",m.getMenAsu());                        
            oResponse.put("autor",m.getUsuAut().getPersona().getNombrePersona());
            oResponse.put("autorIDSession",m.getUsuAut().getUsuSesId());
            oResponse.put("destinatario",m.getUsuDest().getPersona().getNombrePersona());
            oResponse.put("destinatarioIDSession",m.getUsuDest().getUsuSesId());
            oResponse.put("mensajeSoporte",m.getMenCtn());            
//            temp=String.valueOf(petDao.obtenerTipoPeticionPorIdMensaje(m.getMensajeID()));
            Peticion p =petDao.obtenerPeticionPorIdmensaje(m.getMensajeID());
            if(p!=null){
                temp=p.getPetTip();
                if(temp=='G')                
                    oResponse.put("tipoSoporte","Guia Pasos");
                else if(temp=='R')
                    oResponse.put("tipoSoporte","Reporte Error");
                oResponse.put("func",p.getFuncion().getFunSisId());
                oResponse.put("funcnom",p.getFuncion().getNom());
                oResponse.put("submod",p.getFuncion().getSubModuloSistema().getSubModSisId());
                oResponse.put("submodnom",p.getFuncion().getSubModuloSistema().getNom());
                oResponse.put("mod", p.getFuncion().getSubModuloSistema().getModuloSistema().getModSisId());
                oResponse.put("modnom", p.getFuncion().getSubModuloSistema().getModuloSistema().getNom());
            }else{//Es soporte
                Soporte s=sopDao.obtenerPorIdMensaje(m.getMensajeID());
                temp=s.getSopTip();
                 if(temp=='G')                
                    oResponse.put("tipoSoporte","Guia Pasos");
                else if(temp=='R')
                    oResponse.put("tipoSoporte","Reporte Error");
                oResponse.put("func",s.getFuncion().getFunSisId());
                oResponse.put("funcnom",s.getFuncion().getNom());
                oResponse.put("submod",s.getFuncion().getSubModuloSistema().getSubModSisId());
                oResponse.put("submodnom",s.getFuncion().getSubModuloSistema().getNom());
                oResponse.put("mod", s.getFuncion().getSubModuloSistema().getModuloSistema().getModSisId());
                oResponse.put("modnom", s.getFuncion().getSubModuloSistema().getModuloSistema().getNom());
            }             
            
            temp=m.getMenEstAut();
            if(temp=='S')
                oResponse.put("estado","Sin archivar");
            else if(temp=='A')
                oResponse.put("estado","Archivado");
            oResponse.put("fechaEnvio",sdf.format(m.getMenFecEnv()));
            if(m.getMenFecOcu()!=null)
                oResponse.put("fechaOcurrencia",sdf.format(m.getMenFecOcu()));
            oResponse.put("url","archivos/mensajes/");
            oResponse.put("nombreReal", m.getMenMsjImg());
            miArray.put(oResponse);
        }
        return WebResponse.crearWebResponseExito("Se listaron los mensajes correctamente",miArray);
    }
    
}
