/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import java.util.List;

import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BienesMuebles;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.MovimientoIngresos;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.ValorContable;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.BienesMueblesDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.CatalogoBienesDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.MovimientoIngresosDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.ValorContableDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.CatalogoBienes;
import java.text.SimpleDateFormat;
/**
 *
 * @author Administrador
 */
public class ObtenerBienTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        JSONArray miArray = new JSONArray();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        
        String folderFiles = "control_patrimonial/";
        
        try{
            BienesMuebles bm = null;
            MovimientoIngresos mi = null;
            ValorContable vc = null;
            CatalogoBienes cb = null;
            JSONObject requestData = (JSONObject)wr.getData();
            int id_bien = requestData.getInt("id_bien");
            
            BienesMueblesDAO bie_mue_dao = (BienesMueblesDAO)FactoryDao.buildDao("scp.BienesMueblesDAO");
            bm = bie_mue_dao.obtener_bien(id_bien);
            
            MovimientoIngresosDAO mov_ing_dao = (MovimientoIngresosDAO)FactoryDao.buildDao("scp.MovimientoIngresosDAO");
            mi = mov_ing_dao.obtener_movimiento(bm.getMov_ing_id());
            
            ValorContableDAO val_con_dao = (ValorContableDAO)FactoryDao.buildDao("scp.ValorContableDAO");
            vc = val_con_dao.obtenerValorBien(id_bien);
            
            CatalogoBienesDAO cat_bie_dao = (CatalogoBienesDAO)FactoryDao.buildDao("scp.CatalogoBienesDAO");
            cb = cat_bie_dao.obtenerCatalogo(bm.getCat_bie_id());
            
            JSONObject oResponse = new JSONObject();
            /*Movimiento*/
            oResponse.put("mov_ing_id",mi.getMov_ing_id());
            oResponse.put("tip_mov_ing",mi.getTipo_movimiento().getTip_mov_ing_id());
            oResponse.put("tip_mov_ing_des",mi.getTipo_movimiento().getDes());
            oResponse.put("con_pat_id",mi.getCon_pat_id());
            oResponse.put("num_res",mi.getNum_res());
            
            String fec_res = formatter.format(mi.getFec_res());
            oResponse.put("fec_res",fec_res);

            oResponse.put("obs",mi.getObs());
            
            /*Cabecera*/
            oResponse.put("id_bien",bm.getCod_bie());
            oResponse.put("amb_id",bm.getAmb_id());
            oResponse.put("cat_bie_id",bm.getCat_bie_id());
            oResponse.put("an_id",bm.getAn_id());
            oResponse.put("des_bie",bm.getDes_bie());
            oResponse.put("cant",bm.getCant_bie());
           // oResponse.put("fec_reg",bm.getFec_reg());
           
            oResponse.put("an_id",bm.getAn_id());
            oResponse.put("an_des",bm.getAnexo().getAn_des());
            
            String fec_reg = formatter.format(bm.getFec_reg());
            oResponse.put("fec_reg",fec_reg);
            
            oResponse.put("cod_int",bm.getCod_ba_bie());
            oResponse.put("estado_bie",bm.getEstado_bie());
            if("B".equals(bm.getEstado_bie())){oResponse.put("cond",0); oResponse.put("cond_nombre", "BUENO");}
            if("M".equals(bm.getEstado_bie())){oResponse.put("cond",1); oResponse.put("cond_nombre", "MALO");}
            if("R".equals(bm.getEstado_bie())){oResponse.put("cond",2); oResponse.put("cond_nombre", "REGULAR");}
            
            oResponse.put("rut_doc_bie",bm.getRut_doc_bie());
            oResponse.put("cod_ba_bie",bm.getCod_ba_bie());
            oResponse.put("org_id",bm.getOrg_id());
            
            /*Detalle Tecnico*/
            
            oResponse.put("marc",bm.getDtm().getMarc());
            oResponse.put("mod",bm.getDtm().getMod());
            
            oResponse.put("dim",bm.getDtm().getDim());
            oResponse.put("ser",bm.getDtm().getSer());
            oResponse.put("col",bm.getDtm().getCol());
            oResponse.put("tip",bm.getDtm().getTip());
            oResponse.put("nro_mot",bm.getDtm().getNro_mot());
            oResponse.put("nro_pla",bm.getDtm().getNro_pla());
            oResponse.put("nro_cha",bm.getDtm().getNro_cha());
            oResponse.put("raza",bm.getDtm().getRaza());
            oResponse.put("edad",bm.getDtm().getEdad());
            
            /*Catalogo de Bienes*/            
            oResponse.put("cb_den_bie",cb.getDen_bie());
            oResponse.put("cb_cod",cb.getCod());
            
            /*Obtenemos los archivos*/
            oResponse.put("doc_ref","archivos/"+"scp"+"/"+bm.getRut_doc_bie());
            oResponse.put("doc_ref_url","archivos/"+ folderFiles);
            oResponse.put("doc_ref_name",bm.getRut_doc_bie());
            oResponse.put("rut_imag_1","archivos/"+"scp"+"/"+bm.getDtm().getRut_imag_1());
            oResponse.put("rut_imag_1_url","archivos/"+ folderFiles);
            oResponse.put("rut_imag_1_name",bm.getDtm().getRut_imag_1());
            oResponse.put("rut_imag_2","archivos/"+"scp"+"/"+bm.getDtm().getRut_imag_2());
            oResponse.put("rut_imag_2_url","archivos/"+ folderFiles);
            oResponse.put("rut_imag_2_name",bm.getDtm().getRut_imag_2());
            oResponse.put("rut_aut_img","archivos/"+"scp"+"/"+bm.getDtm().getRut_aut_img());
            oResponse.put("rut_aut_img_url","archivos/"+ folderFiles);
            oResponse.put("rut_aut_img_name",bm.getDtm().getRut_aut_img());
            /*Valor Contable*/
            oResponse.put("val_cont_id",vc.getVal_cont_id());
            oResponse.put("valor_cont",vc.getVal_cont());
            oResponse.put("act_dep",String.valueOf(vc.getAct_dep()));
            oResponse.put("cod_cuenta",vc.getCod_cue_id());
            
            miArray.put(oResponse);
            
            
        } catch(Exception e){
            System.out.println("No se pudo Obtener el Bien Mueble\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Obtener el Bien Mueble", e.getMessage() );
        } 
        
        return WebResponse.crearWebResponseExito("Se obtuvo el Bien Correctamente",miArray); 
        
    }
    
}
