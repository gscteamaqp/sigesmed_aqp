/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.maestro.unidad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.EvaluacionesUnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.UnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CapacidadAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CompetenciaAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CompetenciaCapacidad;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CompetenciasUnidadDidactica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.IndicadorAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.maestro.UtilMaestroLogic;
import com.dremo.ucsm.gsc.sigesmed.logic.maestro.sesion.v1.tx.ListarAprendizajesUnidadTx;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author abel
 */
public class ListarAprendizajesEvaluacionTx implements ITransaction{
    
   private static Logger logger = Logger.getLogger(ListarAprendizajesUnidadTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        
         JSONObject data = (JSONObject) wr.getData();
        int idUnidad = data.getInt("unid");
        return listarAprendizajesUnidad(idUnidad);
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    private WebResponse listarAprendizajesUnidad(int idUnidad) {
         
        try{
           UnidadDidacticaDao unidadDao = (UnidadDidacticaDao) FactoryDao.buildDao("maestro.plan.UnidadDidacticaDao");
         
           
           List<CompetenciasUnidadDidactica> competenciasUnidad = unidadDao.listarCompetenciasUnidad(idUnidad);
           Map<CompetenciaAprendizaje,List<CapacidadAprendizaje>> mapCompetencias = new HashMap<>();
           for(CompetenciasUnidadDidactica competenciaUni : competenciasUnidad){
               CompetenciaCapacidad compCap = competenciaUni.getCompetenciaCapacidad();
               Set<IndicadorAprendizaje> indicadores = competenciaUni.getIndicadores();
               List<IndicadorAprendizaje> newIndicadores = new ArrayList<>();
               newIndicadores.addAll(indicadores);
               compCap.getCap().setIndicadores(newIndicadores);
               if(!mapCompetencias.containsKey(compCap.getCom())){
                    List<CapacidadAprendizaje> listCapacidades = new ArrayList<>();
                    listCapacidades.add(compCap.getCap());
                    mapCompetencias.put(compCap.getCom(),listCapacidades);
                }else{
                    mapCompetencias.get(compCap.getCom()).add(compCap.getCap());
                }
           }
           JSONArray rpta = UtilMaestroLogic.mapCompetenciasToJSON(mapCompetencias);
           return WebResponse.crearWebResponseExito("Se listo correctamente",rpta);
           
           
        }
        catch(Exception e){
             logger.log(Level.SEVERE,"listarAprendizajesUnidad",e);
            return WebResponse.crearWebResponseError("No se pudieron listar los indicadores");
        }
        
       
    }
   
   
    
     
    
    
}
