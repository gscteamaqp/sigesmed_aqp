/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.horario_escolar.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.HorarioEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.HorarioDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 *
 * @author abel
 */
public class InsertarDetalleHorarioTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        HorarioDetalle detalle = null;
        try{
            
            JSONObject r = (JSONObject)wr.getData();
            
            int horarioDetalleID = r.optInt("horarioDetalleID");
            int horarioID = r.getInt("horarioID");
            int areaID = r.getInt("areaID");
            int plazaID = r.getInt("plazaID");
            char diaID = r.getString("diaID").charAt(0);
            int aulaID = r.getInt("aulaID");
            int orgId = r.getInt("orgId");
            
            Date desde = new SimpleDateFormat("H:mm").parse( r.getString("horaInicio") );
            Date hasta = new SimpleDateFormat("H:mm").parse( r.getString("horaFin") );
            
            detalle = new HorarioDetalle(horarioDetalleID,desde,hasta,horarioID,plazaID,areaID,aulaID,diaID);
            detalle.setOrgId(orgId);            
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar el Detalle del horario, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        try{
            HorarioEscolarDao horarioDao = (HorarioEscolarDao)FactoryDao.buildDao("mech.HorarioEscolarDao");
            horarioDao.mergeHorarioDetalle(detalle);
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar el Detalle del Horario", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONObject res = new JSONObject();
        res.put("horarioDetalleID", detalle.getHorDetId());
        return WebResponse.crearWebResponseExito("El registro el detalle del horario",res);
        //Fin
    }    
    
    
}
