/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.plantilla_ficha.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.directorio.trabajador.v1.tx.ReporteTx;
import com.dremo.ucsm.gsc.sigesmed.util.GCell;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.kernel.color.Color;

import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;

import com.itextpdf.layout.element.Text;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ImprimirPlantillaTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {        
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        JSONObject requestData = (JSONObject)wr.getData();
        JSONArray grupos = requestData.getJSONArray("grupos");
                
        JSONObject objeto = requestData.getJSONObject("objeto");//objeto donde esta el codigo, nombre y tipo de la plantilla
        
        //Creando el reporte....
        Mitext m = null;        
        try {
            m = new Mitext(false);
            m.newLine(2);
            m.agregarTitulo("PLANTILLA DE FICHA DE EVALUACION");
            m.newLine(2);
            m.agregarSubtitulos(objeto);
            m.newLine(2);
        } catch (Exception ex) {
            System.out.println("No se pudo crear el documento \n"+ex);
            Logger.getLogger(ReporteTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        GTabla tablaHead = new GTabla(new float[]{1,1,1,1});
        tablaHead.setFontSize(8).setBold();
        
        tablaHead.addCell(new GCell().createCellLeft(1,4).add(new Paragraph(new Text("I. DATOS INFORMATIVOS DE LA I.E.:"))).setBorder(Border.NO_BORDER));
        tablaHead.addCell(new GCell().createCellLeft(1,4).add(new Paragraph(new Text(" "))).setBorder(Border.NO_BORDER));

//        tablaHead.addCell(new Cell().add(new Paragraph(" ")).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
        tablaHead.addCell(new GCell().createCellLeft(1,2).add("1.1 NOMBRE DE LA I.E:").setBorder(Border.NO_BORDER));
        tablaHead.addCell(new GCell().createCellLeft(1,2).add("1.2 UGEL").setBorder(Border.NO_BORDER));
        tablaHead.addCell(new GCell().createCellLeft(1,2).add("1.3 DRE:").setBorder(Border.NO_BORDER));
        tablaHead.addCell(new GCell().createCellLeft(1,2).add("1.4 LUGAR:").setBorder(Border.NO_BORDER));
        tablaHead.addCell(new GCell().createCellLeft(1,2).add("1.5 UBICACION(AVENIDA/JIRON):").setBorder(Border.NO_BORDER));
        tablaHead.addCell(new GCell().createCellLeft(1,2).add("1.6 TELEFONO").setBorder(Border.NO_BORDER));
        tablaHead.addCell(new GCell().createCellLeft(1,2).add("1.7 FECHA DE VISITA:").setBorder(Border.NO_BORDER));
        tablaHead.addCell(new GCell().createCellLeft(1,2).add("1.8 NRO. VISITA:").setBorder(Border.NO_BORDER));

        m.agregarTabla(tablaHead);
        m.newLine(1);
        //fin
        
        //tabla de cuerpo       
        
        GTabla tableBody = new GTabla(new float[]{1,1,1,1});
        tableBody.setFontSize(8);
        
        for(int i = 0; i < grupos.length(); ++i){
            
            tableBody.addCell(new GCell().createCellLeft(1,3).add(new Paragraph(grupos.getJSONObject(i).optString("gruNom"))).setBackgroundColor(Color.LIGHT_GRAY));
            tableBody.addCell(new GCell().createCellLeft(1,1).add(new Paragraph("Puntaje")).setBackgroundColor(Color.LIGHT_GRAY));
                        
            JSONArray indicadores = grupos.getJSONObject(i).getJSONArray("indicadores");
            
            for(int j = 0; j < indicadores.length(); ++j){
                tableBody.addCell(new GCell().createCellLeft(1,3).add(new Paragraph(indicadores.getJSONObject(j).optString("indNom"))));
                tableBody.addCell(new GCell().createCellLeft(1,1).add(new Paragraph("o 1 o 2 o 3")));
            }            
        }
        
        m.agregarTabla(tableBody);
        m.newLine(1);

        //tabla de pie
        
        GTabla tablaResultados = new GTabla(new float[]{1,1,1,1,1,1});
        tablaResultados.setFontSize(8);
        
        tablaResultados.addCell(new Cell(1, 1).add("").setBorder(Border.NO_BORDER));
        tablaResultados.addCell(new Cell(1, 1).add("Suma Total: ").setBorder(Border.NO_BORDER));
        tablaResultados.addCell(new Cell(1, 1).add("..........").setBorder(Border.NO_BORDER));
        tablaResultados.addCell(new Cell(1, 3).add("").setBorder(Border.NO_BORDER));
        tablaResultados.addCell(new Cell(1, 6).add("").setBorder(Border.NO_BORDER));
        tablaResultados.addCell(new Cell(1, 2).add("Resultados por Indicadores:").setBorder(Border.NO_BORDER));
        tablaResultados.addCell(new Cell(1, 4).add("").setBorder(Border.NO_BORDER));
        
        for(int i = 0; i < grupos.length(); ++i){
            tablaResultados.addCell(new Cell(1, 2).add(grupos.getJSONObject(i).optString("gruNom")).setBorder(Border.NO_BORDER));                
            tablaResultados.addCell(new Cell(1, 1).add(" ..........%").setBorder(Border.NO_BORDER));
            tablaResultados.addCell(new Cell(1, 3).add("").setBorder(Border.NO_BORDER));
        }

        tablaResultados.addCell(new Cell(1, 1).add("").setBorder(Border.NO_BORDER));
        tablaResultados.addCell(new Cell(1, 1).add("Total(%): ").setBorder(Border.NO_BORDER));
        tablaResultados.addCell(new Cell(1, 1).add("..........%").setBorder(Border.NO_BORDER));
        tablaResultados.addCell(new Cell(1, 3).add("").setBorder(Border.NO_BORDER));
        
        m.agregarTabla(tablaResultados);
        //fin
                   
        m.cerrarDocumento();  
                                
        JSONArray miArray = new JSONArray();
        JSONObject oResponse = new JSONObject();        
        oResponse.put("datareporte",m.encodeToBase64());
        miArray.put(oResponse);                       
        
        return WebResponse.crearWebResponseExito("Se genero el reporte correctamente",miArray);
        
    }    
}
