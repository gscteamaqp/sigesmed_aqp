/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.LibroCajaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.Asiento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentaContable;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.DetalleCuenta;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.LibroCaja;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONObject;

/**
 *
 * @author G-16
 */
public class ActualizarAsientoTx  implements ITransaction{   
    
    @Override    
    public WebResponse execute(WebRequest wr)  {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        Asiento nuevoAsiento = null;
        FileJsonObject docAsiento= null;
        Object obj=null;
         int tipoPagoID=0;
        String nombre;
        double importe=0;
        String nombreDoc="";
          String libro="";
       // String tipoLibro="";
        int idAsi;
        LibroCajaDao asientoDao = (LibroCajaDao)FactoryDao.buildDao("sci.LibroCajaDao");

        try{            
                                      System.out.println("entrnado al actualizar");

            JSONObject requestData = (JSONObject)wr.getData();   
                                     System.out.println("entrnado al actualizar");

            //datos cabecera Asiento
            idAsi = requestData.getInt("asiId");
            System.out.println("asiId " +idAsi);

            int operacionID = requestData.getInt("operacionID");
                        System.out.println("operacionID " +operacionID);

            String fecha=  requestData.getString("fecha");           
                                    System.out.println("fecha " +fecha);

              libro = requestData.getString("libro");
                                     System.out.println("libro " +libro);

            String glosa = requestData.getString("glosa");
                                    System.out.println("glosa " +glosa);

             importe = requestData.getDouble("importe");            
                                    System.out.println("importe " +importe);

            JSONObject tipoPago=   (JSONObject)requestData.get("tipoPago");                       
             tipoPagoID = tipoPago.getInt("tipoPagoID");
                                                 System.out.println("tipoPagoID " +tipoPagoID);

             nombre = tipoPago.getString("nombre");
                                                System.out.println("nombre " +nombre);

            String numeroD= requestData.getString("numeroD");
                                           System.out.println("numeroD " +numeroD);

            int codigoUnicoOperacionID = requestData.getInt("codUniOpeID");  
              System.out.println("codigoUnicoOperacionID " +codigoUnicoOperacionID);
              
            int libroID = requestData.getInt("libroID");                                  
              System.out.println("libroID " +libroID);

            //datos de las cuenta Haber
            JSONObject haber=   (JSONObject)requestData.get("haber");                       
            int haberCuentaContableID = haber.getInt("cuentaContableID");
              System.out.println("haberCuentaContableID " +haberCuentaContableID);
            String haberNombre = haber.getString("nombre");
              System.out.println("haberNombre " +haberNombre);
            
            //datos de las cuenta Debe
            JSONObject debe=   (JSONObject)requestData.get("debe");                       
            int debeCuentaContableID = debe.getInt("cuentaContableID");
              System.out.println("debeCuentaContableID " +debeCuentaContableID);
            String debeNombre = debe.getString("nombre");
              System.out.println("debeNombre " +debeNombre);
            
            String observacion = requestData.optString("observacion"," ");
              System.out.println("observacion " +observacion);
            
             nombreDoc = requestData.optString("nomDocAdj"," ");
               System.out.println("nombreDoc " +nombreDoc);
                      

            nuevoAsiento = new Asiento(0, new LibroCaja(libroID), operacionID, new Date(fecha), glosa, libro.charAt(0),codigoUnicoOperacionID, numeroD,observacion, new Date() , wr.getIdUsuario(), 'A' );
            
                    nuevoAsiento.getDetalleCuentas().add(new DetalleCuenta((short)1, nuevoAsiento, new CuentaContable(debeCuentaContableID,debeNombre), new BigDecimal(importe), true, new Date(), wr.getIdUsuario(), 'A'));
                    nuevoAsiento.getDetalleCuentas().add(new DetalleCuenta((short)2, nuevoAsiento, new CuentaContable(haberCuentaContableID,haberNombre), new BigDecimal(importe), false, new Date(), wr.getIdUsuario(), 'A'));

            
    
        }catch(Exception e){
          
            return WebResponse.crearWebResponseError("No se pudo Actualizar asiento, datos incorrectos", e.getMessage() );
        }           
     
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        try{
            if(libro.charAt(0) =='C')
            asientoDao.actualizarAsientoC(idAsi, nuevoAsiento);
            else if(libro.charAt(0) == 'V')
             asientoDao.actualizarAsientoV(idAsi, nuevoAsiento);

                    //;.insertarAsiento(nuevoAsiento);        
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Actualizar El Asiento y las cuentaa contables asociadas ", e.getMessage() );
        }
        //Fin
        
        DateFormat fechaHora = new SimpleDateFormat("dd/MM/yyyy");

        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        
        oResponse.put("operacionID",nuevoAsiento.getOpeId());
        oResponse.put("codUniOpeID",nuevoAsiento.getCodUniOpeId());
        oResponse.put("glosa",nuevoAsiento.getGloOpe());
        oResponse.put("importe",importe);
        oResponse.put("numeroD",nuevoAsiento.getNumDoc());
        oResponse.put("observacion",nuevoAsiento.getObs());
        oResponse.put("estado",nuevoAsiento.getEstReg());
        oResponse.put("libro",nuevoAsiento.getCodLibro());
        oResponse.put("fecha",fechaHora.format(nuevoAsiento.getFecAsi()));

        
            JSONObject jTipoPago = new JSONObject();         
            jTipoPago.put("tipoPagoID", tipoPagoID);
            jTipoPago.put("nombre",nombre);        
        oResponse.put("tipoPago",jTipoPago);
        
      
        JSONObject jDoc = new JSONObject(); 
                   jDoc.put("url",Sigesmed.UBI_ARCHIVOS+"/contable/");
                   jDoc.put("nombreArchivo",nombreDoc);
                   jDoc.put("edi",true);

        oResponse.put("doc",jDoc); 
            
        
                                
        for( DetalleCuenta det:nuevoAsiento.getDetalleCuentas()){
            if(det.getNatDetCue()){
                
                 JSONObject jDebe = new JSONObject();         
                    jDebe.put("cuentaContableID", det.getCuentaContable().getCueConId());
                    jDebe.put("nombre",det.getCuentaContable().getNomCue());  
                    jDebe.put("importe",importe); 
                oResponse.put("debe",jDebe);
                
            }
            else{
                 JSONObject jHaber = new JSONObject();         
                    jHaber.put("cuentaContableID", det.getCuentaContable().getCueConId());
                    jHaber.put("nombre",det.getCuentaContable().getNomCue());  
                    jHaber.put("importe",importe); 
                oResponse.put("haber",jHaber);
                
            }
        }                                
                
   return WebResponse.crearWebResponseExito("La Actulizacion  del Asiento se realizo correctamente", oResponse);
        //Fin
    }    
    
    
}
