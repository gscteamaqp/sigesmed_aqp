/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.web;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.MensajeElectronicoDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaMensaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaMensajeModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.MensajeDocumento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.MensajeElectronico;
import com.dremo.ucsm.gsc.sigesmed.logic.web.BandejaMensaje.v1.tx.Mensaje;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author abel
 */
public class MensajeElectronicoDaoHibernate extends GenericDaoHibernate<MensajeElectronico> implements MensajeElectronicoDao{
    
    @Override
    public void enviarMensaje(MensajeElectronico mensaje,int destinatarioID){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            //creando el mensaje
            session.persist(mensaje);       
            
            
            BandejaMensaje bandeja = new BandejaMensaje(0,null,Mensaje.ESTADO_NUEVO,mensaje.getMenEleId(),destinatarioID);
            //enviando el mensaje al destinatario
            session.persist(bandeja);
            
            miTx.commit();
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo enviar el mensaje al destinatario\\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo enviar el mensaje al destinatario\\n"+ e.getMessage());
        }
        finally{
            session.close();
        }
    }
    @Override
    public void enviarMensaje(MensajeElectronico mensaje,List<Integer> destinatarios){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            
            //creando el mensaje
            session.persist(mensaje);
            
            //creando los mensajes a lso destinatarios
            for(Integer destinatarioID: destinatarios){                
                BandejaMensaje bandeja = new BandejaMensaje(0,null,Mensaje.ESTADO_NUEVO,mensaje.getMenEleId(),destinatarioID);
                session.persist(bandeja);
            }                
            miTx.commit();        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo enviar el mensaje a los destinatarios\\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo enviar el mensaje a los destinatarios\\n"+ e.getMessage());
        }
        finally{
            session.close();
        }
    }
    
    @Override
    public List<BandejaMensaje> listarMensajesPorUsuario(int usuarioID){
        List<BandejaMensaje> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT bm FROM BandejaMensaje bm JOIN FETCH bm.mensaje m WHERE bm.usuSesId=:p1 ORDER BY m.fecEnv";
            Query query = session.createQuery(hql);
            query.setParameter("p1", usuarioID);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los mensajes del usuario\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las mensajes del usuario\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public List<MensajeElectronico> listarMensajesEnviadosPorUsuario(int usuarioID){
        List<MensajeElectronico> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT m FROM MensajeElectronico m WHERE m.usuSesId=:p1 ORDER BY m.fecEnv";
            Query query = session.createQuery(hql);
            query.setParameter("p1", usuarioID);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los mensajes enviados por el usuario\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las mensajes enviados por el usuario\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public List<BandejaMensajeModel> listarMensajesNuevosNoVistosPorUsuario(int usuarioID){
        List<BandejaMensajeModel> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaMensajeModel(bm.banMenId,bm.mensaje.asu,bm.mensaje.fecEnv,bm.mensaje.session.persona.nom,bm.mensaje.session.persona.apePat) FROM BandejaMensaje bm WHERE bm.usuSesId=:p1 and bm.estado='N' ORDER BY bm.mensaje.fecEnv";
            Query query = session.createQuery(hql);
            query.setParameter("p1", usuarioID);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los mensajes nuevos no vistos del usuario\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las mensajes nuevos no vistos del usuario\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public List<MensajeDocumento> verDocumentosMensaje(int mensajeID){
        List<MensajeDocumento> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT md FROM MensajeDocumento md WHERE md.mensaje.menEleId=:p1";
            
            Query query = session.createQuery(hql);
            query.setParameter("p1", mensajeID);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los documentos adjuntos del mensaje\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los documentos dadjuntos del mensaje\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public void marcarVistoMensaje(int bandejaMensajeID){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{            
            String hql = "UPDATE BandejaMensaje bm SET bm.estado=:p2 WHERE bm.banMenId =:p1";            
            Query query = session.createQuery(hql);
            query.setParameter("p1", bandejaMensajeID);
            query.setParameter("p2", Mensaje.ESTADO_SIN_VER);
            query.executeUpdate();
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo cambiar de estado al mensaje\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    @Override
    public void verMensaje(int bandejaMensajeID){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{            
            String hql = "UPDATE BandejaMensaje bm SET bm.fecVis=:p2,bm.estado=:p3 WHERE bm.banMenId =:p1";            
            Query query = session.createQuery(hql);
            query.setParameter("p1", bandejaMensajeID);
            query.setParameter("p2", new Date());
            query.setParameter("p3", Mensaje.ESTADO_VISTO);
            query.executeUpdate();
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo ver el contenido del mensaje\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
}
