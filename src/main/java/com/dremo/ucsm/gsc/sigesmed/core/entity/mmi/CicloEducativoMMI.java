package com.dremo.ucsm.gsc.sigesmed.core.entity.mmi;
// Generated 20/03/2017 03:37:08 PM by Hibernate Tools 4.3.1


import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * CicloEducativo generated by hbm2java
 */
@Entity
@Table(name="ciclo_educativo"
    ,schema="public"
)
public class CicloEducativoMMI  implements java.io.Serializable {


     private int cicId;
     private DisenoCurricularMMI disenoCurricular;
     private String abr;
     private String nom;
     private String des;
     private Date fecMod;
     private Integer usuMod;
     private Character estReg;

    public CicloEducativoMMI() {
    }

	
    public CicloEducativoMMI(int cicId, DisenoCurricularMMI disenoCurricular) {
        this.cicId = cicId;
        this.disenoCurricular = disenoCurricular;
    }
    public CicloEducativoMMI(int cicId, DisenoCurricularMMI disenoCurricular, String abr, String nom, String des, Date fecMod, Integer usuMod, Character estReg) {
       this.cicId = cicId;
       this.disenoCurricular = disenoCurricular;
       this.abr = abr;
       this.nom = nom;
       this.des = des;
       this.fecMod = fecMod;
       this.usuMod = usuMod;
       this.estReg = estReg;
    }
   
     @Id 

    
    @Column(name="cic_id", unique=true, nullable=false)
    public int getCicId() {
        return this.cicId;
    }
    
    public void setCicId(int cicId) {
        this.cicId = cicId;
    }

@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="dis_cur_id", nullable=false)
    public DisenoCurricularMMI getDisenoCurricular() {
        return this.disenoCurricular;
    }
    
    public void setDisenoCurricular(DisenoCurricularMMI disenoCurricular) {
        this.disenoCurricular = disenoCurricular;
    }

    
    @Column(name="abr", length=4)
    public String getAbr() {
        return this.abr;
    }
    
    public void setAbr(String abr) {
        this.abr = abr;
    }

    
    @Column(name="nom", length=32)
    public String getNom() {
        return this.nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }

    
    @Column(name="des", length=256)
    public String getDes() {
        return this.des;
    }
    
    public void setDes(String des) {
        this.des = des;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_mod", length=29)
    public Date getFecMod() {
        return this.fecMod;
    }
    
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    
    @Column(name="usu_mod")
    public Integer getUsuMod() {
        return this.usuMod;
    }
    
    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    
    @Column(name="est_reg", length=1)
    public Character getEstReg() {
        return this.estReg;
    }
    
    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
}


