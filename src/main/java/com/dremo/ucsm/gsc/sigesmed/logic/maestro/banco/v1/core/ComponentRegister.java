package com.dremo.ucsm.gsc.sigesmed.logic.maestro.banco.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.maestro.banco.v1.tx.*;

/**
 * Created by Administrador on 10/10/2016.
 */
public class ComponentRegister implements IComponentRegister {
    @Override
    public WebComponent createComponent() {
        WebComponent component = new WebComponent(Sigesmed.SUBMODULO_MAESTRO);
        component.setName("banco_lectura");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionGET("listarLecturas", ListarLecturasDocenteTx.class);
        component.addTransactionPOST("registrarLectura", RegistrarLecturaTx.class);
        component.addTransactionPUT("editarLectura",EditarLecturaTx.class);
        component.addTransactionDELETE("eliminarLectura", EliminarLecturaTx.class);
        return component;
    }
}
