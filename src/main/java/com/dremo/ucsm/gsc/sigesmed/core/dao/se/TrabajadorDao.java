/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Trabajador;

/**
 *
 * @author gscadmin
 */
public interface TrabajadorDao extends GenericDao<Trabajador>{
    public Trabajador buscarPorPerId(Integer perId);
    public Trabajador buscarPorId(Integer traId);
    public Trabajador buscarPorUsuarioId(int codUsr);
    
}
