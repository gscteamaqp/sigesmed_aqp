    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.cpe;

import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.HorarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;

import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TrabajadorCargo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.DiaSemana;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.DiasEspeciales;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.HorarioCab;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.HorarioDet;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Carlos
 */
public class HorarioDaoHibernate extends GenericDaoHibernate<HorarioCab> implements HorarioDao {

    @Override
    public List<HorarioCab> listarHorario(Organizacion org) {
        List<HorarioCab> horario = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT DISTINCT hc FROM HorarioCab hc LEFT JOIN hc.horCabCar cc  WHERE hc.estReg<>'E' AND hc.org =:p1  ORDER BY hc.horCabId DESC";

            Query query = session.createQuery(hql);
            query.setParameter("p1", org);
            horario = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo buscar el Horario \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar el Horario \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return horario;
    }

    @Override
    public List<DiasEspeciales> listarCalendario(Organizacion org) {
        List<DiasEspeciales> calendario = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT de FROM DiasEspeciales de WHERE de.estReg<>'E' AND de.organizacionId =:p1  ORDER BY de.fecha ASC";

            Query query = session.createQuery(hql);
            query.setParameter("p1", org);
            calendario = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo buscar el Horario \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar el Horario \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return calendario;
    }

    @Override
    public List<DiaSemana> listarDiaSemana() {
        List<DiaSemana> semana = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT ds FROM DiaSemana ds ";

            Query query = session.createQuery(hql);

            semana = query.list();

        } catch (Exception e) {
            System.out.println("No se puede listar los Dias de la Semana \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se puede listar los Dias de la Semana \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return semana;
    }

    @Override
    public List<HorarioDet> listarHorarioById(HorarioCab hc) {
        List<HorarioDet> horario = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT  hd FROM HorarioDet hd JOIN FETCH hd.diaSemId   WHERE hd.estReg<>'E' AND hd.horCabId =:p1";

            Query query = session.createQuery(hql);
            query.setParameter("p1", hc);
            horario = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo buscar el Horario \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar el Horario \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return horario;

    }

    @Override
    public Boolean asignarHorarioByCargo(Integer horarioCabecera, TrabajadorCargo cargo) {
        Boolean estado = Boolean.FALSE;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try {
            String hql = "UPDATE  HorarioCab p SET p.horCabCar =:p1 WHERE p.horCabId =:p2 ";

            Query query = session.createQuery(hql);
            query.setParameter("p1", cargo);
            query.setParameter("p2", horarioCabecera);
            query.executeUpdate();
            estado = Boolean.TRUE;
            miTx.commit();

        } catch (Exception e) {
            miTx.rollback();
            estado = Boolean.FALSE;
            System.out.println("No se pudo buscar los cargos \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo buscar los cargos \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return estado;

    }

    @Override
    public HorarioCab buscarHorarioCargoByTrabajador(TrabajadorCargo trab) {
        HorarioCab horario = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT  hc FROM HorarioCab hc  WHERE hc.estReg<>'E' AND hc.horCabCar=:p1";

            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            query.setParameter("p1", trab);
            horario = (HorarioCab) query.uniqueResult();

        } catch (Exception e) {
            System.out.println("No se pudo buscar el Horario \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar el Horario \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return horario;

    }

    @Override
    public Boolean asignarHorarioPersonalizado(Trabajador trabajador, HorarioCab horario) {
        Boolean estado = Boolean.FALSE;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try {
            String hql = "UPDATE  Trabajador tt SET tt.horaCabId =:p1 WHERE tt.traId =:p2 ";

            Query query = session.createQuery(hql);
            query.setParameter("p1", horario);
            query.setParameter("p2", trabajador.getTraId());
            query.executeUpdate();
            estado = Boolean.TRUE;
            miTx.commit();

        } catch (Exception e) {
            miTx.rollback();
            estado = Boolean.FALSE;
            System.out.println("No se asignar el Horario \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se asignar el Horario \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return estado;

    }

    @Override
    public HorarioCab buscarHorarioActualByTrabajador(Trabajador trab) {

        HorarioCab horario = null;
        Trabajador trabajador = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT  tt FROM Trabajador tt JOIN FETCH  tt.horaCabId hc WHERE hc.estReg<>'E' AND tt.traId=:p1";

            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            query.setParameter("p1", trab.getTraId());
            trabajador = (Trabajador) query.uniqueResult();
            if(trabajador!=null)
            {
                horario = trabajador.getHoraCabId();
            }
          
        } catch (Exception e) {
            System.out.println("No se pudo buscar el Horario \\n " + e.getMessage());
            e.printStackTrace();
            throw new UnsupportedOperationException("No se pudo buscar el Horario \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return horario;

    }

    @Override
    public Boolean actualizarHorariosByCargo(HorarioCab horarioCab, TrabajadorCargo trabajadorCargo, Organizacion organizacion) {
        Boolean estado = Boolean.FALSE;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try {
            String hql = "UPDATE  Trabajador tt SET tt.horaCabId =:p1 WHERE tt.organizacion =:p3  AND tt.traCar =:p2 AND tt.estReg<>'E' AND tt.horaCabId IS NULL";
            Query query = session.createQuery(hql);
            query.setParameter("p1", horarioCab);
            query.setParameter("p2", trabajadorCargo);
            query.setParameter("p3", organizacion);
            query.executeUpdate();
            estado = Boolean.TRUE;
            miTx.commit();

        } catch (Exception e) {
            miTx.rollback();
            e.printStackTrace();
            estado = Boolean.FALSE;
            System.out.println("No se asignar el Horario \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se asignar el Horario \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return estado;

    }

    @Override
    public Boolean liberarHorarioById(Integer horarioCabecera) {
       Boolean estado = Boolean.FALSE;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try {
            String hql = "UPDATE  HorarioCab hc SET hc.horCabCar=NULL WHERE  hc.horCabId =:p1 AND hc.estReg<>'E'";
            Query query = session.createQuery(hql);
            query.setParameter("p1", horarioCabecera);
            
            query.executeUpdate();
            estado = Boolean.TRUE;
            miTx.commit();

        } catch (Exception e) {
            miTx.rollback();
            e.printStackTrace();
            estado = Boolean.FALSE;
            System.out.println("No se Libero el Horario \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se libero el Horario \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return estado;
    }

    @Override
    public Boolean actualizarHorarioFromTrabajador(HorarioCab horarioCabeceraInicial, HorarioCab horarioCabeceraFinal,Organizacion org) {
        Boolean estado = Boolean.FALSE;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try {
            String hql = "UPDATE  Trabajador tt SET tt.horaCabId =:p2 WHERE tt.organizacion =:p3  AND tt.horaCabId =:p1 AND tt.estReg<>'E'";
            Query query = session.createQuery(hql);
            query.setParameter("p1", horarioCabeceraInicial);
            query.setParameter("p2", horarioCabeceraFinal);
            query.setParameter("p3", org);
            query.executeUpdate();
            estado = Boolean.TRUE;
            miTx.commit();

        } catch (Exception e) {
            miTx.rollback();
            e.printStackTrace();
            estado = Boolean.FALSE;
            System.out.println("No se libero el Horario \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se libero el Horario \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return estado;
    
    }

    @Override
    public Boolean liberarHorarioPersonalizado(Trabajador trabajador) {
        Boolean estado = Boolean.FALSE;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try {
            String hql = "UPDATE  Trabajador tt SET tt.horaCabId = NULL WHERE tt.traId=:p1  AND tt.estReg<>'E'";
            Query query = session.createQuery(hql);
            query.setParameter("p1", trabajador.getTraId());
            
            query.executeUpdate();
            estado = Boolean.TRUE;
            miTx.commit();

        } catch (Exception e) {
            miTx.rollback();
            e.printStackTrace();
            estado = Boolean.FALSE;
            System.out.println("No se libero el Horario del Trabajador \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se libero el Horario del Trabajador \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return estado;
    
    }

    @Override
    public Boolean actualizarHorarioEnTrabajador(HorarioCab idHorarioCabOrigen, HorarioCab horarioDestino) {
        Boolean estado = Boolean.FALSE;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try {
            String hql = "UPDATE  Trabajador tt SET tt.horaCabId =:p2 WHERE tt.horaCabId =:p1 AND tt.estReg<>'E'";

            Query query = session.createQuery(hql);
            query.setParameter("p1", idHorarioCabOrigen);
            query.setParameter("p2", horarioDestino);
            query.executeUpdate();
            estado = Boolean.TRUE;
            miTx.commit();

        } catch (Exception e) {
            miTx.rollback();
            estado = Boolean.FALSE;
            System.out.println("No se pudo actualizar el horario \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se actualizar el horario \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return estado;

    }

}
