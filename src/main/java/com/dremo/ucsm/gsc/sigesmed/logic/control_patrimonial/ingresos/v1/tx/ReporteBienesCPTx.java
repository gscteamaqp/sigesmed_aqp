/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;

import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BienesMuebles;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.BienesMueblesDAO;

import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.dremo.ucsm.gsc.sigesmed.util.GCell;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.border.SolidBorder;
import com.itextpdf.layout.element.Paragraph;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import java.text.SimpleDateFormat;

import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.dremo.ucsm.gsc.sigesmed.util.GCell;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;

/**
 *
 * @author Administrador
 */
public class ReporteBienesCPTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        try {

            /*Obtener data frontend*/
            JSONObject requestData = (JSONObject) wr.getData();
            int org_id = requestData.getInt("org_id");
            Date fec_ini = formatter.parse(requestData.getString("fec_ini"));
            Date fec_fin = formatter.parse(requestData.getString("fec_fin"));
            int an_id = requestData.getInt("ane_id");
            int cod_amb = requestData.getInt("cod_amb");
            int cod_pat = requestData.getInt("cod_pat");
            String est_bie = requestData.getString("est");


            /*Listar Organizacion (Nombre , Ubicacion , Responsable) */
            Organizacion org = null;
            OrganizacionDao org_dao = (OrganizacionDao) FactoryDao.buildDao("OrganizacionDao");
            org = org_dao.buscarConTipoOrganizacionYPadre(org_id);

            /*Listamos los Bienes*/
            List<BienesMuebles> bm = null;
            BienesMueblesDAO bm_dao = (BienesMueblesDAO) FactoryDao.buildDao("scp.BienesMueblesDAO");

            bm = bm_dao.listarPorAtributos(fec_ini, fec_fin, an_id, org_id, cod_amb, cod_pat, est_bie);

            Mitext mi = null;
            
            System.out.println("anexo: " + an_id);
            
            String pdfBase64 = "";
            
            switch (an_id) {
                case 1:
                    /*MOBILIARIO*/
                    Reporte_Bienes_MobiliarioTx rbmob = new Reporte_Bienes_MobiliarioTx();
                    //mi = rbmob.generar_reporte_mobiliario(bm, org);
                    pdfBase64 = rbmob.generar_reporte_mobiliario(bm, org);
                    break;
                case 2:
                    /*MAQUINARIAS EQUIPOS*/
                    Reporte_Bienes_MaquinariasTx rbm = new Reporte_Bienes_MaquinariasTx();
                    pdfBase64 = rbm.generar_reporte_maquinarias(bm, org);
                    break;
                case 3:
                    /*BIENES MENORES*/
                    Reporte_Bienes_Menores_Tx rbme = new Reporte_Bienes_Menores_Tx();
                    pdfBase64 = rbme.generar_reporte_bienes_menores(bm, org);
                    break;
                case 4:
                    /*BIBLIOGRAFIA*/
                    Reporte_Bienes_BibliografiaTx rbb = new Reporte_Bienes_BibliografiaTx();
                    //mi = rbb.generar_reporte_bibliografia(bm, org);
                    pdfBase64 = rbb.generar_reporte_bibliografia(bm, org);
                    break;
                case 5:
                    /*MATERIALES RECUPERADOS*/
                    Reporte_Bienes_RecuperadosTx rbr = new Reporte_Bienes_RecuperadosTx();
                    pdfBase64 = rbr.generar_reporte_recuperados(bm, org);
                    break;
                case 6:
                    /*INSFRAESTRUCTURA INMUEBLES*/
                    // PENDIENTE

                    break;
                case 7:
                    /*VEHICULOS MOTOCICLETAS*/
                    // PENDIENTE

                    break;
                case 8:
                    /*SEMOVIENTES*/
                    Reporte_Bienes_SemovientesTx rbs = new Reporte_Bienes_SemovientesTx();
                    pdfBase64 = rbs.generar_reporte_semoviente(bm, org);
                    break;
            }

            JSONArray miArray = new JSONArray();

            //mi.cerrarDocumento();
            JSONObject oResponse = new JSONObject();
            //oResponse.put("datareporte", mi.encodeToBase64());
            oResponse.put("datareporte", pdfBase64);
            miArray.put(oResponse);

            return WebResponse.crearWebResponseExito("Se genero el Reporte con exito ...", miArray);

        } catch (Exception e) {
            System.out.println(e);
            return WebResponse.crearWebResponseError("ERROR AL GENERAR REPORTE ", e.getMessage());
        }

    }

}
