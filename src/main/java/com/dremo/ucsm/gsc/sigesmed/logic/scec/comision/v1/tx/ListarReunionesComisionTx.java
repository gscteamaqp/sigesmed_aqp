package com.dremo.ucsm.gsc.sigesmed.logic.scec.comision.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.ReunionComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.ActasReunionComision;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.ReunionComision;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 03/10/2016.
 */
public class ListarReunionesComisionTx implements ITransaction {
    private static  final Logger logger = Logger.getLogger(ListarReunionesComisionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        String com = wr.getMetadataValue("com");

        return listarReuniones(Integer.parseInt(com));
    }
    public WebResponse listarReuniones(int com){
        try{
            ReunionComisionDao reunionComisionDao = (ReunionComisionDao) FactoryDao.buildDao("scec.ReunionComisionDao");
            List<ReunionComision> reuniones = reunionComisionDao.buscarReunionesPorComision(com);
            JSONArray jsonReuniones = new JSONArray();
            for(ReunionComision reunion : reuniones){
                Date date = reunion.getFecReu();
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                List<ActasReunionComision> actas = reunion.getActasReunion();
                JSONArray jsonActas = new JSONArray();
                for(ActasReunionComision acta : actas){
                    JSONObject jsonActa = new JSONObject(EntityUtil.objectToJSONString(
                            new String[]{"actComId","des","fecRegAct"},
                            new String[]{"cod","des","fecRegAct"},
                            acta
                    ));
                    jsonActa.put("url", Sigesmed.UBI_ARCHIVOS+"/actas_reunion/");
                    jsonActa.put("nom",acta.getArcAdj());
                    jsonActas.put(jsonActa);
                }
                JSONObject jsonReu = new JSONObject(EntityUtil.objectToJSONString(new String[]{"reuId","fecReu","resConReu","lugReu"},new String[]{"cod","fec","res","lug"},reunion));
                jsonReu.put("act",jsonActas);
                jsonReu.put("mosAct",false);
                jsonReuniones.put(jsonReu);

            }
            return WebResponse.crearWebResponseExito("Se listo correctamente las reuniones", WebResponse.OK_RESPONSE).setData(jsonReuniones);
        }catch (Exception e){
            logger.log(Level.SEVERE,"listarReuniones",e);
            return WebResponse.crearWebResponseError("Error al listar las reuniones",WebResponse.BAD_RESPONSE);
        }
    }
}
