/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.ItemFileDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.ItemFileDetalleDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.ItemFile;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.ItemFileDetalle;
import java.text.SimpleDateFormat;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class MostrarContentDirTx implements ITransaction {

    
    JSONObject datosPadreHijo = new JSONObject();
    @Override
    public WebResponse execute(WebRequest wr) {
        //Obteniendo datos...
        
        int codPad = 0;
        int usuCod=0;
        int orgID=0;
        boolean flagUgel=false;
        try{
            JSONObject requestData = (JSONObject) wr.getData();
            codPad = requestData.getInt("roleId");
            usuCod=requestData.getInt("usuarioID");
            orgID=requestData.getInt("organizacionID");
            flagUgel=requestData.getBoolean("flag");
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo leer los datos de entrada");
        }
        
        
        SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
        
        if (codPad != -1) {
            /* Parte para la operacion en la Base de Datos */
            ItemFileDao itemFileDao = (ItemFileDao) FactoryDao.buildDao("rdg.ItemFileDao");
            ItemFileDetalleDao itemFileDetDao = (ItemFileDetalleDao) FactoryDao.buildDao("rdg.ItemFileDetalleDao");

            /*Realizamos tambien la lectura del mismo padre con sus datos*/
            JSONObject datosPadre = new JSONObject();
            ItemFile padre = itemFileDao.buscarPorID(codPad);
            datosPadre.put("ideFil", padre.getIteIde());
            datosPadre.put("nomFil", padre.getIteNom());
            if(padre.getItePadIde()!=null){
                datosPadre.put("idePadFil", padre.getItePadIde().getIteIde());
                datosPadre.put("nomPadFil", padre.getItePadIde().getIteNom());
            }else{
                datosPadre.put("idePadFil", 0);
                datosPadre.put("nomPadFil", "");
            }
            datosPadre.put("verFil", padre.getIteVer());
            datosPadre.put("tipFil", padre.getIteCodCat());
        datosPadre.put("tamFil", padre.getIteTam());
            datosPadre.put("fecCre", sdf.format(padre.getIteFecCre()));
            datosPadre.put("urlFil", padre.getIteUrlDes());
            if(padre.getItePlaIde()!=null){
                datosPadre.put("plazoIni",padre.getItePlaIde().getPlzFecIniSub());
                datosPadre.put("plazoFin",padre.getItePlaIde().getPlzFecFinSub());
            }                        
              if(padre.getIteProt()!=null){
                    String temp=padre.getIteProt();

                    datosPadre.put("iteProt", temp.replace(" ","")); //String                                  
              }else
                datosPadre.put("iteProt", ""); //String
              
              

            JSONArray listHijos = new JSONArray();
            List<ItemFile> docs = null;
            List<ItemFileDetalle> compartidos=null;
            
            
            if(padre.getIteTam()==0){// es carpeta
                if(flagUgel){//flagUGEL
                    docs = itemFileDao.buscarHijosPorIdPadreUgel(new ItemFile(codPad),orgID, usuCod); 
                }else{
                    docs = itemFileDao.buscarHijosPorIdPadre(new ItemFile(codPad),usuCod);
                }  
                System.out.println("123123");
                System.out.println(usuCod);
                for (int i = 0; i < docs.size(); i++) {
                    ItemFile elemFile = docs.get(i);
                    JSONObject hijo = new JSONObject();//Propios
                    hijo.put("ideFil", elemFile.getIteIde());
                    hijo.put("nomFil", elemFile.getIteNom());
                    hijo.put("idePadFil", elemFile.getItePadIde().getIteIde());
                    hijo.put("nomPadFil", elemFile.getItePadIde().getIteNom());
                    hijo.put("verFil", elemFile.getIteVer());
                    if(flagUgel){
                        if(elemFile.getIteOrgIde().getOrgId()==orgID)
                            hijo.put("autoria","Propio");
                        else
                           hijo.put("autoria","Colegio");
                    }else
                        hijo.put("autoria","Propio");
                    hijo.put("tipFil", elemFile.getIteCodCat());
                    hijo.put("tamFil", elemFile.getIteTam());
                    hijo.put("fecCre", sdf.format(elemFile.getIteFecCre()));
                    
                    hijo.put("urlFil", "archivos/"+elemFile.getIteUrlDes());

                    if(elemFile.getIteProt()!=null){
                        String temp=elemFile.getIteProt();
                        hijo.put("iteProt", temp.replace(" ","")); //String                    
                    }else
                        hijo.put("iteProt", ""); //String
              
                    
                    if(elemFile.getTesIde()!=null){
                        hijo.put("tesFil", elemFile.getTesIde());
                        hijo.put("tesFilAli", elemFile.getTesIde().getTesAli());
//                        hijo.put("tifFil", elemFile.getIteTifIde().getTifIde());
                    }

                    listHijos.put(hijo);
                }
                compartidos = itemFileDetDao.buscarCompartidos(usuCod,padre);                        
                for (int i = 0; i < compartidos.size(); i++) {
                    ItemFileDetalle det = compartidos.get(i);
                    ItemFile elemFile = itemFileDao.buscarCompartido(det);
                    JSONObject hijo = new JSONObject();
                    hijo.put("ideFil", elemFile.getIteIde());
                    hijo.put("nomFil", elemFile.getIteNom());
                    hijo.put("idePadFil", elemFile.getItePadIde().getIteIde());
                    hijo.put("nomPadFil", elemFile.getItePadIde().getIteNom());
                    hijo.put("verFil", elemFile.getIteVer());
                    hijo.put("autoria","Compartido");

                    hijo.put("tipFil", elemFile.getIteCodCat());
                    hijo.put("tamFil", elemFile.getIteTam());
                    hijo.put("fecCre", sdf.format(elemFile.getIteFecCre()));
                    hijo.put("urlFil", "archivos/"+elemFile.getIteUrlDes());
                    
                    if(elemFile.getTesIde()!=null){
                        hijo.put("tesFil", elemFile.getTesIde());
                        hijo.put("tesFilAli", elemFile.getTesIde().getTesAli());
                        //hijo.put("tifFil", elemFile.getIteTifIde().getTifIde());
                    }else{
                        hijo.put("tifFil", 0);
                    }

                    listHijos.put(hijo);
                }
                
                
                
                

                datosPadreHijo.put("datosHijos", listHijos);
                datosPadreHijo.put("datosPadre", datosPadre); 
            }
        }
        return WebResponse.crearWebResponseExito("Se Listo correctamente", datosPadreHijo);
        //Fin
    }

}
