/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.rdg;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TrabajadorCargo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.Trabajador;
import java.util.List;

/**
 *
 * @author christian
 */
public interface TrabajadorDao  extends GenericDao<Trabajador>{
    public Trabajador buscarPorPersonayCargo(Long idePersona, TrabajadorCargo ideCargo);
    public List<UsuarioSession> buscarConRolPorOrganizacionYNombre(int orgID, String nom);
    public UsuarioSession getByID(int id);
    public UsuarioSession getByUsuarioSesionID(int id);
}
