package com.dremo.ucsm.gsc.sigesmed.core.entity.cpe;


import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="justificacion_inasistencia" ,schema="administrativo")
public class JustificacionInasistenciaTrabajador  implements java.io.Serializable {


    @Id
    @Column(name="jus_ina_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_justificacion", sequenceName="administrativo.justificacion_inasistencia_jus_ina_id_seq" )
    @GeneratedValue(generator="secuencia_justificacion")
    private Integer jusInaId;
    
    @Column(name="jus_ina_doc")
    private String jusInaDoc;
    
    @Column(name="jus_ina_obs")
    private String jusInaObs;
    
    @Column(name="est_reg")
    private String estReg;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_reg")
    private Date fecReg;
    
    @OneToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="ina_id")
    private Inasistencia jusIna;

    public JustificacionInasistenciaTrabajador() {
    }

    public JustificacionInasistenciaTrabajador(Integer jusInaId) {
        this.jusInaId = jusInaId;
    }

    public JustificacionInasistenciaTrabajador(String jusInaDoc, String jusInaObs, String estReg, Inasistencia jusIna,Date fecReg) {
        this.jusInaDoc = jusInaDoc;
        this.jusInaObs = jusInaObs;
        this.estReg = estReg;
        this.jusIna = jusIna;
        this.fecReg=fecReg;
    }

    public Integer getJusInaId() {
        return jusInaId;
    }

    public void setJusInaId(Integer jusInaId) {
        this.jusInaId = jusInaId;
    }

    public String getJusInaDoc() {
        return jusInaDoc;
    }

    public void setJusInaDoc(String jusInaDoc) {
        this.jusInaDoc = jusInaDoc;
    }

    public String getJusInaObs() {
        return jusInaObs;
    }

    public void setJusInaObs(String jusInaObs) {
        this.jusInaObs = jusInaObs;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public Inasistencia getJusIna() {
        return jusIna;
    }

    public void setJusIna(Inasistencia jusIna) {
        this.jusIna = jusIna;
    }

    public Date getFecReg() {
        return fecReg;
    }

    public void setFecReg(Date fecReg) {
        this.fecReg = fecReg;
    }
    
    
    
   
    
}


