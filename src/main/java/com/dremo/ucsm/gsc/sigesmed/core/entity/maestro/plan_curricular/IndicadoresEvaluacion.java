/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular;
import javax.persistence.*;
import java.util.Date;
/**
 *
 * @author Jeferson
 */
@Entity
@Table(name = "indicadores_evaluacion", schema = "pedagogico")
public class IndicadoresEvaluacion {
    @Embeddable
    public static class Id implements java.io.Serializable{
        @Column(name = "eva_uni_did_id", nullable = false)
        protected int eva_uni_did_id;
        @Column(name = "uni_did_id", nullable = false)
        protected int uni_did_id;
        @Column(name = "com_id", nullable = false)
        protected int comId;
         @Column(name = "cap_id", nullable = false)
        protected int capId;
        @Column(name = "ind_apr_id", nullable = false)
        protected int indAprId;
        
        public Id() {
        }

        public Id(int eva_uni_did_id, int uni_did_id, int comId, int capId, int indAprId) {
            this.eva_uni_did_id = eva_uni_did_id;
            this.uni_did_id = uni_did_id;
            this.comId = comId;
            this.capId = capId;
            this.indAprId = indAprId;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 97 * hash + this.eva_uni_did_id;
            hash = 97 * hash + this.uni_did_id;
            hash = 97 * hash + this.comId;
            hash = 97 * hash + this.capId;
            hash = 97 * hash + this.indAprId;
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Id other = (Id) obj;
            if (this.eva_uni_did_id != other.eva_uni_did_id) {
                return false;
            }
            if (this.uni_did_id != other.uni_did_id) {
                return false;
            }
            if (this.comId != other.comId) {
                return false;
            }
            if (this.capId != other.capId) {
                return false;
            }
            if (this.indAprId != other.indAprId) {
                return false;
            }
            return true;
        }

    }
    
    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "eva_uni_did_id", column = @Column(name = "eva_uni_did_id", nullable = false)),
            @AttributeOverride(name = "uni_did_id", column = @Column(name = "uni_did_id", nullable = false)),
            @AttributeOverride(name = "comId", column = @Column(name = "com_id", nullable = false)),
            @AttributeOverride(name = "capId", column = @Column(name = "cap_id", nullable = false)),
            @AttributeOverride(name = "indAprId", column = @Column(name = "ind_apr_id", nullable = false))

    })
    private Id id = new Id();
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "eva_uni_did_id", insertable = false, updatable = false)
    private EvaluacionesUnidadDidactica evaluaciones;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "uni_did_id", insertable = false, updatable = false)
    private UnidadDidactica unidad;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "com_id", insertable = false, updatable = false)
    private CompetenciaAprendizaje competencia;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cap_id", insertable = false, updatable = false)
    private CapacidadAprendizaje capacidad;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ind_apr_id", insertable = false, updatable = false)
    private IndicadorAprendizaje indicador;
    
    
    @Column(name = "usu_mod")
    private Integer usuMod;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;

    @Column(name = "est_reg", length = 1)
    private Character estReg;

    public IndicadoresEvaluacion() {
    }

    public IndicadoresEvaluacion(EvaluacionesUnidadDidactica evaluaciones, UnidadDidactica unidad, CompetenciaAprendizaje competencia, CapacidadAprendizaje capacidad, IndicadorAprendizaje indicador) {
        this.evaluaciones = evaluaciones;
        this.unidad = unidad;
        this.competencia = competencia;
        this.capacidad = capacidad;
        this.indicador = indicador;
        
        this.id.eva_uni_did_id = evaluaciones.getEvaUniDidId();
        this.id.uni_did_id = unidad.getUniDidId();
        this.id.comId = competencia.getComId();
        this.id.capId = capacidad.getCapId();
        this.id.indAprId = indicador.getIndAprId();
        
        this.estReg = 'A';
        this.fecMod = new Date();
    }

    public void setEvaluaciones(EvaluacionesUnidadDidactica evaluaciones) {
        this.evaluaciones = evaluaciones;
    }

    public void setCompetencia(CompetenciaAprendizaje competencia) {
        this.competencia = competencia;
    }

    public void setCapacidad(CapacidadAprendizaje capacidad) {
        this.capacidad = capacidad;
    }

    public void setIndicador(IndicadorAprendizaje indicador) {
        this.indicador = indicador;
    }

    public EvaluacionesUnidadDidactica getEvaluaciones() {
        return evaluaciones;
    }

    public UnidadDidactica getUnidad() {
        return unidad;
    }

    public CompetenciaAprendizaje getCompetencia() {
        return competencia;
    }

    public CapacidadAprendizaje getCapacidad() {
        return capacidad;
    }

    public IndicadorAprendizaje getIndicador() {
        return indicador;
    }
    
    
    

    

    
    
}
