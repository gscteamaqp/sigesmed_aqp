/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author gscadmin
 */
@Entity
@Table(name = "demerito", schema="administrativo")

public class Demerito implements Serializable {

    @Id
    @Column(name = "dem_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_demerito", sequenceName="administrativo.demerito_dem_id_seq" )
    @GeneratedValue(generator="secuencia_demerito")
    private Integer demId;
    
    @Column(name = "ent_emi")
    private String entEmi;
    
    @Column(name = "num_res")
    private String numRes;
    
    @Column(name = "fec_res")
    @Temporal(TemporalType.DATE)
    private Date fecRes;
    
    @Column(name = "sep")
    private Boolean sep;
    
    @Column(name = "fec_ini")
    @Temporal(TemporalType.DATE)
    private Date fecIni;
    
    @Column(name = "fec_fin")
    @Temporal(TemporalType.DATE)
    private Date fecFin;
    
    @Column(name = "mot")
    private String mot;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "fic_esc_id", referencedColumnName = "fic_esc_id")
    private FichaEscalafonaria fichaEscalafonaria;

    public Demerito() {
    }

    public Demerito(Integer demId) {
        this.demId = demId;
    }
    
    public Demerito(FichaEscalafonaria fichaEscalafonaria, String entEmi, String numRes, Date fecRes, Boolean sep, Date fecIni, Date fecFin, String mot, Integer usuMod, Date fecMod, Character estReg) {
        this.fichaEscalafonaria = fichaEscalafonaria;
        this.entEmi = entEmi;
        this.numRes = numRes;
        this.fecRes = fecRes;
        this.sep = sep;
        this.fecIni = fecIni;
        this.fecFin = fecFin;
        this.mot = mot;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
        
    }

    public Integer getDemId() {
        return demId;
    }

    public void setDemId(Integer demId) {
        this.demId = demId;
    }

    public String getEntEmi() {
        return entEmi;
    }

    public void setEntEmi(String entEmi) {
        this.entEmi = entEmi;
    }

    public String getNumRes() {
        return numRes;
    }

    public void setNumRes(String numRes) {
        this.numRes = numRes;
    }

    public Date getFecRes() {
        return fecRes;
    }

    public void setFecRes(Date fecRes) {
        this.fecRes = fecRes;
    }

    public Boolean getSep() {
        return sep;
    }

    public void setSep(Boolean sep) {
        this.sep = sep;
    }

    public Date getFecIni() {
        return fecIni;
    }

    public void setFecIni(Date fecIni) {
        this.fecIni = fecIni;
    }

    public Date getFecFin() {
        return fecFin;
    }

    public void setFecFin(Date fecFin) {
        this.fecFin = fecFin;
    }

    public String getMot() {
        return mot;
    }

    public void setMot(String mot) {
        this.mot = mot;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public FichaEscalafonaria getFichaEscalafonaria() {
        return fichaEscalafonaria;
    }

    public void setDatosEscalafon(FichaEscalafonaria fichaEscalafonaria) {
        this.fichaEscalafonaria = fichaEscalafonaria;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (demId != null ? demId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Demerito)) {
            return false;
        }
        Demerito other = (Demerito) object;
        if ((this.demId == null && other.demId != null) || (this.demId != null && !this.demId.equals(other.demId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Demerito{" + "demId=" + demId + ", entEmi=" + entEmi + ", numRes=" + numRes + ", fecRes=" + fecRes + ", sep=" + sep + ", fecIni=" + fecIni + ", fecFin=" + fecFin + ", mot=" + mot + ", usuMod=" + usuMod + ", fecMod=" + fecMod + ", estReg=" + estReg + ", fichaEscalafonaria=" + fichaEscalafonaria + '}';
    }

    
}
