/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.plantilla_indicadores.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.PlantillaIndicadoresDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaFichaInstitucional;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaGrupo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaIndicadores;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.TipoGrupo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ActualizarIndicadorTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        PlantillaIndicadores grupoAct = null;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            int indId = requestData.getInt("indId");
            int gruId = requestData.getInt("gruId");
            String indDes = requestData.optString("indNom");
            

            grupoAct = new PlantillaIndicadores(indId, new PlantillaGrupo(gruId), indDes);
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        PlantillaIndicadoresDao dexDao = (PlantillaIndicadoresDao)FactoryDao.buildDao("smdg.PlantillaIndicadoresDao");
        try{
            dexDao.update(grupoAct);
        
        }catch(Exception e){
            System.out.println("No se pudo actualizar el Indicador\n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar el Indicador", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Indicador se actualizo correctamente");
        //Fin
    }
    
}
