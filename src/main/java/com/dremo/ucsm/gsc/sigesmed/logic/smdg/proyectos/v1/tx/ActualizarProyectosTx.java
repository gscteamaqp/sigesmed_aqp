/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.proyectos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.ProyectoDetalleDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.ProyectosDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.ProyectoActividades;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.ProyectoDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.ProyectoRecursos;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.Proyectos;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ActualizarProyectosTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        
        JSONObject requestData = (JSONObject)wr.getData();
//        JSONArray actividades = requestData.getJSONArray("actividades");
        
        int proid = requestData.getInt("proid");
        String pronom = requestData.getString("pronom");
        String protip = requestData.optString("protip");
        int prores = requestData.optInt("prores");        
            
        Date proini = null;
        Date profin = null;
                
        try {
            proini = formatter.parse(requestData.optString("proini"));
            profin = formatter.parse(requestData.optString("profin"));            
        } catch (ParseException ex) {
            Logger.getLogger(RegistrarProyectosTx.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        //documento
        
        String prodoc = requestData.optString("prodoc");        
        JSONObject arcJSON = requestData.optJSONObject("archivo");
        if(arcJSON != null){
            FileJsonObject file=null;        
            
            if(arcJSON!=null && arcJSON.length()>0){
                file=new FileJsonObject(arcJSON,prodoc);
            }else{
                return WebResponse.crearWebResponseError("No se pudo leer el archvo, datos incorrectos");  
            }
            BuildFile.buildFromBase64("smdg/", file.getName(),file.getData());
        }
        
//        fin documento        
        
        ProyectosDao proyectoDao = (ProyectosDao)FactoryDao.buildDao("smdg.ProyectosDao");
        Proyectos proyecto = proyectoDao.load(Proyectos.class,proid);//new Proyectos(pronom, protip, proini, profin, prores);
        proyecto.setProNom(pronom);
        proyecto.setProTip(protip);
        proyecto.setProIni(proini);
        proyecto.setProFin(profin);
        proyecto.setProRes(prores);
        
//        Set<ProyectoActividades> actividadesSet = new HashSet<ProyectoActividades>();
//        ProyectoActividades actividad = null;
//        
//        Date pacini = null;
//        Date pacfin = null;
//        
//        for(int i = 0; i < actividades.length(); ++i){            
//            try {
//                pacini = formatter.parse(actividades.getJSONObject(i).optString("pacini"));
//                pacfin = formatter.parse(actividades.getJSONObject(i).optString("pacfin"));            
//            } catch (ParseException ex) {
//                Logger.getLogger(RegistrarProyectosTx.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            
//            if (actividades.getJSONObject(i).optInt("pacid") == 0) { 
//                actividad = new ProyectoActividades(actividades.getJSONObject(i).optString("pacdes"), pacini, pacfin, actividades.getJSONObject(i).optJSONObject("pacres").optInt("traId"), proyecto);
//            }
//            else{
//                actividad = new ProyectoActividades(actividades.getJSONObject(i).getInt("pacid"), actividades.getJSONObject(i).optString("pacdes"), pacini, pacfin, actividades.getJSONObject(i).optJSONObject("pacres").optInt("traId"), proyecto);
//            }
//
//            if(null != actividades.getJSONObject(i).optJSONArray("recursos")){
//                JSONArray recursos = actividades.getJSONObject(i).optJSONArray("recursos");
//                Set<ProyectoRecursos> recursosSet = new HashSet<ProyectoRecursos>();
//
//                for(int j = 0; j < recursos.length(); ++j){                
//                    if(recursos.getJSONObject(j).optInt("recid") == 0){
//                        recursosSet.add(new ProyectoRecursos(recursos.getJSONObject(j).optString("recnom"), recursos.getJSONObject(j).optDouble("reccos"), actividad));
//                    }
//                    else{
//                        recursosSet.add(new ProyectoRecursos(recursos.getJSONObject(j).getInt("recid"), recursos.getJSONObject(j).optString("recnom"), recursos.getJSONObject(j).optDouble("reccos"), actividad));
//                    }                                    
//                }
//                actividad.setRecursos(recursosSet);
//            }
//            
//            actividadesSet.add(actividad);
//            
//        }
        
//        proyecto.setActividades(actividadesSet);
        proyectoDao.update(proyecto);
        
        ProyectoDetalle detalle = new ProyectoDetalle(proyecto.getProId(), prodoc);        
        ProyectoDetalleDao detalleDao = (ProyectoDetalleDao)FactoryDao.buildDao("smdg.ProyectoDetalleDao");        
        detalleDao.update(detalle);        
        //Fin                
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("proid",proyecto.getProId());        
        return WebResponse.crearWebResponseExito("Se actualizo el proyecto correctamente", oResponse);
        //Fin
    }
}
