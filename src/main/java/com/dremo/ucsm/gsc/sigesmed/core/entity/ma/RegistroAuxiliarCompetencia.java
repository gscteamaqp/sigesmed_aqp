package com.dremo.ucsm.gsc.sigesmed.core.entity.ma;

import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.GradoIEEstudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.PeriodosPlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CompetenciaAprendizaje;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrador on 26/01/2017.
 */
@Entity
@Table(name = "registro_auxiliar_competencia", schema = "pedagogico")
public class RegistroAuxiliarCompetencia implements java.io.Serializable {
    @Id
    @Column(name = "reg_aux_com_id",nullable = false, unique = true)
    @SequenceGenerator(name = "registro_auxiliar_competencia_reg_aux_com_id_seq", sequenceName = "pedagogico.registro_auxiliar_competencia_reg_aux_com_id_seq")
    @GeneratedValue(generator = "registro_auxiliar_competencia_reg_aux_com_id_seq")
    private int regAuxComId;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gra_ie_est_id" , insertable=false , updatable=false)
    private GradoIEEstudiante gradoEst;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "comp_id")
    private CompetenciaAprendizaje comp;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "are_cur_id" , insertable=false , updatable=false)
    private AreaCurricular areaCurricular;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "per_eva_id" , insertable=false , updatable=false )
    private PeriodosPlanEstudios periodoPlanEstudios;
    
    @Column(name = "not_com",length = 4)
    private String nota;
    
    @Column(name="not_com_lit", length= 2)
    private String not_com_lit;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    
    @Column(name="gra_ie_est_id")
    private int gra_ie_est_id;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length=29)
    private Date fecMod;
    
    @Column(name = "est_reg",length = 1)
    private Character estReg;
    
    
    
    
    @Column(name = "are_cur_id")
    private int are_cur_id;
    
    @Column(name = "per_eva_id")
    private int per_eva_id;
    

    public void setGrad_ie_est_id(int grad_ie_est_id) {
        this.gra_ie_est_id = grad_ie_est_id;
    }

    public int getGrad_ie_est_id() {
        return gra_ie_est_id;
    }

    public void setGra_ie_est_id(int gra_ie_est_id) {
        this.gra_ie_est_id = gra_ie_est_id;
    }

    public void setAre_cur_id(int are_cur_id) {
        this.are_cur_id = are_cur_id;
    }

    public void setPer_eva_id(int per_eva_id) {
        this.per_eva_id = per_eva_id;
    }

    public int getGra_ie_est_id() {
        return gra_ie_est_id;
    }
    

    public int getAre_cur_id() {
        return are_cur_id;
    }

    public int getPer_eva_id() {
        return per_eva_id;
    }

    public void setNot_com_lit(String not_com_lit) {
        this.not_com_lit = not_com_lit;
    }

    public String getNot_com_lit() {
        return not_com_lit;
    }
    
    
    
    
    public RegistroAuxiliarCompetencia() {
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public RegistroAuxiliarCompetencia(String nota) {
        this.nota = nota;
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public int getRegAuxComId() {
        return regAuxComId;
    }

    public void setRegAuxComId(int regAuxComId) {
        this.regAuxComId = regAuxComId;
    }

    public GradoIEEstudiante getGradoEst() {
        return gradoEst;
    }

    public void setGradoEst(GradoIEEstudiante gradoEst) {
        this.gradoEst = gradoEst;
    }

    public CompetenciaAprendizaje getComp() {
        return comp;
    }

    public void setComp(CompetenciaAprendizaje comp) {
        this.comp = comp;
    }

    public AreaCurricular getAreaCurricular() {
        return areaCurricular;
    }

    public void setAreaCurricular(AreaCurricular areaCurricular) {
        this.areaCurricular = areaCurricular;
    }

    public PeriodosPlanEstudios getPeriodoPlanEstudios() {
        return periodoPlanEstudios;
    }

    public void setPeriodoPlanEstudios(PeriodosPlanEstudios periodoPlanEstudios) {
        this.periodoPlanEstudios = periodoPlanEstudios;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    @Override
    public String toString() {
        return "RegistroAuxiliarCompetencia{" + "nota=" + nota + '}';
    }
    
    
}
