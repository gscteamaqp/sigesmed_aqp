package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.EvaluacionParticipanteCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.RespuestaEncuestaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.EvaluacionParticipanteCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.RespuestaEncuesta;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class RegistrarRespuestaEncuestaTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(RegistrarRespuestaEncuestaTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();
            EvaluacionParticipanteCapacitacionDao evaluacionParticipanteCapacitacionDao = (EvaluacionParticipanteCapacitacionDao) FactoryDao.buildDao("capacitacion.EvaluacionParticipanteCapacitacionDao");
            RespuestaEncuestaDao respuestaEncuestaDao = (RespuestaEncuestaDao) FactoryDao.buildDao("capacitacion.RespuestaEncuestaDao");
            EvaluacionParticipanteCapacitacion evaPar = evaluacionParticipanteCapacitacionDao.buscarUltima(data.getInt("codEva"), data.getInt("codDoc"));
            
            evaPar.setEstReg('C');
            evaPar.setFecApl(new Date());

            evaluacionParticipanteCapacitacionDao.update(evaPar);

            JSONArray array = data.getJSONArray("questions");

            for (int i = 0; i < array.length(); i++) {                
                JSONObject question = array.getJSONObject(i);
                String answer = "";
                
                switch (question.getString("tip")) {
                    case "S":
                        answer = question.getString("resUsu");
                        break;

                    case "L":
                        answer = Boolean.toString(question.getBoolean("resUsu"));
                        break;

                    case "M":
                        JSONArray opciones = question.getJSONArray("res");

                        for (int j = 0; j < opciones.length(); j++) {
                            if (opciones.getJSONObject(j).getBoolean("resUsu")) {
                                answer = answer + j + "#$";
                            }
                        }

                        break;
                }
                
                respuestaEncuestaDao.insert(new RespuestaEncuesta(question.getInt("id"), evaPar.getEvaParCapId(), answer));                
            }
            
            return WebResponse.crearWebResponseExito("Exito al registrar respuestas de encuesta", WebResponse.OK_RESPONSE);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "registrarRespuestaEncuesta", e);
            return WebResponse.crearWebResponseError("Error al registrar respuestas de encuesta", WebResponse.BAD_RESPONSE);
        }
    }
}
