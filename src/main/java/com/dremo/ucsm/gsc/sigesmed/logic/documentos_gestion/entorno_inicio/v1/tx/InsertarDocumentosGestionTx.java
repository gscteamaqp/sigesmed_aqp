/* * To change this license header, choose License Headers in Project Properties. * To change this template file, choose Tools | Templates * and open the template in the editor. */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.ItemFileDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.ItemFileDetalleDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.TipoEspecializadoDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.ItemFile;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.TipoEspecializado;
import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;
/** * * @author Administrador */
public class InsertarDocumentosGestionTx implements ITransaction{    
  
    public static String cerosIzquierda(long codigo,int numDigitos){
            String nuevoCodigo = String.valueOf( codigo ); 
            int numCeros = numDigitos - nuevoCodigo.length();  
            for (int i=0;i < numCeros;i++)       
                nuevoCodigo="0"+nuevoCodigo;   
            return nuevoCodigo;  
        }   
    
    @Override   
    public WebResponse execute(WebRequest wr) { 
        /*        
        *   Parte para la lectura, verificacion y validacion de datos        
        */  
            int coddoc = 0;    
            String desdoc = "";    
            Integer verdoc = 0;    
            Integer tamanio = 0;    
            Long ntamanio;    
            String strFecha="";   
            String urldoc="";    
            String tipoArchivo="";    
            int tipoIteFil = 0;   
            Date fecdoc = null;        
            int id_region = 0;    
            int id_org = 0;    int id_resp = 0;  
            String proteccion="0";
            boolean flagProteccion=false;
              FileJsonObject miF=null;
            
            
//            BuildFiles bfile = null;   
            
        ItemFile nuevoDoc = null;        
        try{           
                JSONObject requestData = (JSONObject)wr.getData();          
            coddoc = requestData.getInt("coddoc");          
            desdoc = requestData.getString("desdoc");        
            //verdoc = requestData.getInt("verdoc");       
            tamanio = requestData.getInt("tamanio");          
            id_org = requestData.getInt("org");           
            id_resp = requestData.getInt("idResponsable");  
            int aux = tamanio/1024;
            ntamanio = (tamanio > 0 && aux == 0) ? (long)1 : (long)aux;     
            SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");      
            strFecha = requestData.getString("fecCre");        
            urldoc = requestData.getString("urldoc");        
            tipoIteFil = requestData.getInt("selTipDoc");
            flagProteccion=requestData.getBoolean("flagProteccion");
            if(flagProteccion)
                proteccion=requestData.getString("proteccion");

            JSONObject archivo=requestData.optJSONObject("archivo");
            
          
            if(archivo!=null && archivo.length()>0){
                miF=new FileJsonObject(archivo,desdoc);
                miF.getName();
            }else{
                return WebResponse.crearWebResponseError("No se pudo leer el archvo, datos incorrectos");  
            }
            
            //Construyendo la fecha de un string
            fecdoc = formatoDelTexto.parse(strFecha);            
            //Armar el codigo del padre            
            //finalmente la construccion del archivo con todos los datos armados                    
            //para este caso debemos colocar el id de padre en forma de objeto asi podremos insertarlo        
        }catch(Exception e){   
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage() );  
        }    
        
        
        
        
        ItemFileDao itemFileDao = (ItemFileDao) FactoryDao.buildDao("rdg.ItemFileDao");      
        ItemFile itefilPadre = itemFileDao.buscarPorID(coddoc);   
       
       
        
        if(itefilPadre.getItePlaIde()!=null && itefilPadre.getItePlaIde().getPlzIde()!=0){//Existe plazo
            if(itefilPadre.getItePlaIde().getPlzFecIniSub().compareTo(fecdoc)>0 
                    || itefilPadre.getItePlaIde().getPlzFecFinSub().compareTo(fecdoc)<0){
                return WebResponse.crearWebResponseError("La fecha actual no esta dentro del rango de plazo de la carpeta");
            }
        }
        

        //Buscamos el tipo especializado de archivo que nos mando        
        TipoEspecializadoDao tipEspDao = (TipoEspecializadoDao) FactoryDao.buildDao("rdg.TipoEspecializadoDao");  
        TipoEspecializado tipEsp = tipEspDao.tipoEspById(tipoIteFil);     
        
        //Una vez hecha la persistencia hacemos recien la creacion del nuevo doc y su persistencia        
        //Ademas la url debe ser del padre concatenado con el nombre del padre   
        String nombreArchivo=miF.getName();
        String formato=nombreArchivo.substring(nombreArchivo.indexOf('.'),nombreArchivo.length());
        //nuevoDoc = new ItemFile(desdoc, "A", tipEsp, ntamanio.doubleValue(), fecdoc, urldoc +"/"+ itefilPadre.getIteNom() +"/"+desdoc+formato, id_resp, "A", itefilPadre,new Organizacion(id_org));
        nuevoDoc = new ItemFile(desdoc, "A", tipEsp, ntamanio.doubleValue(), fecdoc, itefilPadre.getIteNom() +"/"+desdoc+formato, id_resp, "A", itefilPadre,new Organizacion(id_org));
        String ultimoCodigo = itemFileDao.buscarUltimoCodigo();    
        Integer nextCode = Integer.parseInt(ultimoCodigo)+1;      
        nuevoDoc.setIteIde(nextCode);     
        nuevoDoc.setIteAltIde(cerosIzquierda(nextCode , 4));  
        nuevoDoc.setIteProt(proteccion);
        
        ItemFile fileUltVer=itemFileDao.obtenerUltimaVersion(nuevoDoc,itefilPadre);
        int ver=0;
        if(fileUltVer!=null)
           ver=fileUltVer.getIteVer();
  
        //Adjuntamos la versión del archivo
        nuevoDoc.setIteVer(((Integer)(ver+1)).shortValue());
        nuevoDoc.setIteNom(nuevoDoc.getIteNom()+"V"+(ver+1));
        if(urldoc.equals(itefilPadre.getIteNom())){
            nuevoDoc.setIteUrlDes(itefilPadre.getIteNom() +"/"+desdoc+"V"+(ver+1)+formato);
        }else{
            nuevoDoc.setIteUrlDes(urldoc +"/"+ itefilPadre.getIteNom() +"/"+desdoc+"V"+(ver+1)+formato);
        }
        
        nuevoDoc.setIteMod('N');
        
        if(ver!=0){
            if(itemFileDao.desactivarVersionesAnteriores(nuevoDoc,itefilPadre)){//Desactiva versiones anteriores
                //Hacemos persistencia del archivo11        
                itemFileDao.insert(nuevoDoc);
            }
        }else{
                //Hacemos persistencia del archivo        
                itemFileDao.insert(nuevoDoc);
        }
        
        
        
        
        
          /* 
        *  Repuesta Correcta  
        */       
        
         if(urldoc.equals(itefilPadre.getIteNom())){
            BuildFile.buildFromBase64(itefilPadre.getIteNom(), nuevoDoc.getIteNom()+formato,miF.getData());
        }else{
            BuildFile.buildFromBase64(urldoc +"/"+ itefilPadre.getIteNom(), nuevoDoc.getIteNom()+formato,miF.getData());
        }
        //BuildFile.buildFromBase64(urldoc +"/"+ itefilPadre.getIteNom(), nuevoDoc.getIteNom()+formato,miF.getData());
        //
        
        
        
        //Actualizar comparticion
        ItemFileDetalleDao itemFileDetDao = (ItemFileDetalleDao) FactoryDao.buildDao("rdg.ItemFileDetalleDao");      
        if(itemFileDetDao.esCompartido(fileUltVer)){
            itemFileDetDao.actualizarComparticionConNuevaVersionDoc(nuevoDoc.getIteIde(),fileUltVer.getIteIde());
        }
        
        
        
        
        JSONObject oResponse = new JSONObject();    
        oResponse.put("coddoc",nuevoDoc.getIteIde()); 
        return WebResponse.crearWebResponseExito("El registro del documento se realizó correctamente", oResponse);   
           
           
        //Fin    
        }
    }
    
    
        
