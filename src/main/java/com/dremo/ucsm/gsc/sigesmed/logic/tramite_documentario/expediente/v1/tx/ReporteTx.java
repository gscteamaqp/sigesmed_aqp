/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;

/**
 *
 * @author abel
 */
public class ReporteTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        FileJsonObject miGrafico = null;
        GTabla tablaOrg = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            JSONArray tabla = requestData.optJSONArray("resumen");
            
            if(tabla!=null && tabla.length() > 0){
                
                float[] cols = {0.5f,5f,1.5f,1.5f,1.5f};
                tablaOrg = new GTabla(cols);

                String[] labels = {"N°","Organizacion","N° Expedientes","N° Finalizados","N° Entregados"};
                tablaOrg.build(labels);
                for(int i = 0; i < tabla.length();i++){
                    JSONObject bo =tabla.getJSONObject(i);
                    
                    String[] fila = new String [5];
                    fila[0] = i+1+"";
                    fila[1] = bo.getString("organizacion");
                    fila[2] = ""+bo.getInt("expedientes");
                    fila[3] = ""+bo.getInt("finalizados");
                    fila[4] = ""+bo.getInt("entregados");
                    
                    tablaOrg.processLine(fila);
                }
                
            }
            else            
                miGrafico = new FileJsonObject( requestData  );
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo realizar el reporte, grafico iicorrecto", e.getMessage() );
        }
        //Fin        
        
        
        /*
        *  Repuesta Correcta
        */
        
        JSONObject response = new JSONObject();
        try {
            Mitext r = new Mitext();
            
            
            if(tablaOrg==null){
                r.agregarTitulo(miGrafico.getName());
                r.newLine(1);
                r.agregarImagen64(miGrafico.getData());
            }
            else{
                r.agregarTitulo("Estadisticas Gasto Por Centro de Costo");
                r.newLine(1);
                r.agregarTabla(tablaOrg);
            }
            
            r.cerrarDocumento();
            response.append("reporte", r.encodeToBase64() );
        } catch (Exception ex) {
            Logger.getLogger(ReporteTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return WebResponse.crearWebResponseExito("el reporte se realizo",response);
        //Fin
    }
    
}

