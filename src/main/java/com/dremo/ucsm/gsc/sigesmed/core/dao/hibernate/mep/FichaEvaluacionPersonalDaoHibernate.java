package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mep;

import ch.qos.logback.classic.util.ContextInitializer;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mep.FichaEvaluacionPersonalDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.*;
import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.logic.mep.v1.tx.MEP;
import org.hibernate.*;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.*;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;

/**
 * Created by Administrador on 12/10/2016.
 */
public class FichaEvaluacionPersonalDaoHibernate extends GenericDaoHibernate<FichaEvaluacionPersonal> implements FichaEvaluacionPersonalDao{

    @Override
    public void insertFichaEvaluacion(FichaEvaluacionPersonal fev) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try {
            if(fev.getTipTra().getCarNom().equals("Docente")){
                insertEscalas(fev, MEP.ESCALAS_DOCENTE);
                insertRangosFicha(fev,MEP.RANGOS_DOCENTE);
                insertContenidoFichaDocente(fev);
            }else{
                JSONObject jsonContenido = getJSONContenidoFicha(fev.getTipTra().getCarNom());
                if (jsonContenido != null){
                    insertEscalas(fev,MEP.ESCALAS);
                    insertRangosFicha(fev, MEP.RANGOS);
                    insertarContenidoFichaPersonal(fev,jsonContenido);
                }
                else{ 
                    System.out.println("JsonContenido == null");
                }
            }
            session.persist(fev);
            tx.commit();
        }catch (Exception e){
            tx.rollback();
            throw e;
        }
    }

    @Override
    public List<FichaEvaluacionPersonal> getFichas() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{

            Criteria query = session.createCriteria(FichaEvaluacionPersonal.class)
                    .addOrder(Order.asc("ficEvaPerId"));
            query.add(Restrictions.eq("estReg", 'A'));
            List<FichaEvaluacionPersonal> lfevp = query.list();
            return lfevp;
        }catch(Exception e){
            throw e;
        }finally{
            session.close();
        }
    }

    @Override
    public FichaEvaluacionPersonal getFichaById(int id) {
        return null;
    }

    @Override
    public ElementoFicha insertElementoFicha(int id, ElementoFicha elemento) {
        return null;
    }

    @Override
    public List<ElementoFicha> getElementos(int idFicha, Class<? extends ElementoFicha> elemento, String... variables) {
        return null;
    }
    @Override
    public FichaEvaluacionPersonal getFichaByCargo(String cargo){
        Session session = HibernateUtil.getSessionFactory().openSession();
        FichaEvaluacionPersonal fev  = null;
        try{
            Criteria criteria = session.createCriteria(FichaEvaluacionPersonal.class);
            criteria.setFetchMode("escalaValoracionFichas", FetchMode.JOIN);
            criteria.setFetchMode("rangoPorcentualFichas", FetchMode.JOIN);
            criteria.setFetchMode("contenidoFichaEvaluacions", FetchMode.JOIN);
            criteria.setFetchMode("contenidoFichaEvaluacions.hijosContenido", FetchMode.JOIN);
            criteria.setFetchMode("contenidoFichaEvaluacions.indicadorFichaEvaluacions", FetchMode.JOIN);
            criteria.add(Restrictions.eq("estReg",'A'));
            //criteria.add(Restrictions.like("tipTra",TipoRol.valueOf(cargo)));
            criteria.createCriteria("tipTra").add(Restrictions.like("carNom",cargo));
            criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
            fev = (FichaEvaluacionPersonal)criteria.uniqueResult();
            if(cargo.toUpperCase().equals("DOCENTE")){
                FichaEvaluacionPersonal fichaDocente = new FichaEvaluacionPersonal(fev.getFicEvaPerId(),fev.getTipTra(),
                        fev.getNom(),fev.getFecCre(),fev.getDes());
                fichaDocente.setEscalaValoracionFichas(fev.getEscalaValoracionFichas());
                fichaDocente.setRangoPorcentualFichas(fev.getRangoPorcentualFichas());
                Iterator<ContenidoFichaEvaluacion> itc = fev.getContenidoFichaEvaluacions().iterator();
                while(itc.hasNext()){
                    ContenidoFichaEvaluacion contenidoFichaEvaluacion = itc.next();
                    if(contenidoFichaEvaluacion.getTip() == 'd'){
                        fichaDocente.getContenidoFichaEvaluacions().add(contenidoFichaEvaluacion);
                    }
                }
                return fichaDocente;
            }
            return fev;
        }catch(Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
    private void insertEscalas(FichaEvaluacionPersonal fev, String[] escalas){
        for(String escala : escalas){
            String[] datosEscala = escala.split(":");
            fev.getEscalaValoracionFichas().add(new EscalaValoracionFicha(datosEscala[0],datosEscala[1],Integer.parseInt(datosEscala[2]),new Date(),'A',fev));
        }
    }
    private void insertContenidoFichaDocente(FichaEvaluacionPersonal fev){

        try {
            //getClass().getClassLoader().getResource("fep_docente.json").getFile()
            Reader reader = new InputStreamReader(new FileInputStream(ServicioREST.PATH_SIGESMED+ File.separator +"WEB-INF" + File.separator +"classes" + File.separator + "fep_docente.json"),"UTF-8");
            JSONObject obj = new JSONObject(new JSONTokener(reader));
            JSONArray array = obj.getJSONArray("dominios");
            for(int i = 0; i < array.length(); i++){
                JSONObject dominio = array.getJSONObject(i);
                JSONArray competencias = dominio.getJSONArray("competencias");
                ContenidoFichaEvaluacion padreCont = new ContenidoFichaEvaluacion(dominio.getString("tip").charAt(0),dominio.getString("nom"),new Date(),'A',fev);
                for(int j = 0; j < competencias.length();j++){
                    JSONObject competencia = competencias.getJSONObject(j);
                    JSONArray indicadores = competencia.getJSONArray("inds");
                    ContenidoFichaEvaluacion objCont = new ContenidoFichaEvaluacion(competencia.getString("tip").charAt(0),
                            competencia.getString("nom"),new Date(),'A',fev);
                    objCont.setParCont(padreCont);
                    for(int k = 0; k < indicadores.length(); k++){
                        JSONObject indicador = indicadores.getJSONObject(k);
                        objCont.getIndicadorFichaEvaluacions().add(new IndicadorFichaEvaluacion(indicador.getString("nom"),
                                indicador.getString("des"), indicador.getString("doc"), objCont));
                    }
                    fev.getContenidoFichaEvaluacions().add(objCont);
                }
            }
        } catch (FileNotFoundException e) {

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }
    private void insertRangosFicha(FichaEvaluacionPersonal fev, String[] rangos){
        for(String rango : rangos){
            String[] datosRango = rango.split(":");
            fev.getRangoPorcentualFichas().add(new RangoPorcentualFicha(Integer.parseInt(datosRango[0]),Integer.parseInt(datosRango[1]),datosRango[2],new Date(),'A',fev));
        }
    }
    private void insertarContenidoFichaPersonal(FichaEvaluacionPersonal fep, JSONObject jsonContenido){
        insertContenidoTipo(fep, jsonContenido, "funciones", 'f');
        insertContenidoTipo(fep,jsonContenido,"competencias",'c');
    }
    private void insertContenidoTipo(FichaEvaluacionPersonal fep, JSONObject cont,String nomfic,char tipo){
        JSONArray jsonContenido = cont.getJSONArray(nomfic);
        for(int i = 0; i < jsonContenido.length(); i++){
            JSONObject jsonCont = jsonContenido.getJSONObject(i);
            ContenidoFichaEvaluacion objCont = new ContenidoFichaEvaluacion(tipo,
                    jsonCont.getString("nom"),new Date(),'A',fep);
            JSONArray jsonIndicadores = jsonCont.getJSONArray("indicadores");
            for(int j = 0; j < jsonIndicadores.length(); j++){
                JSONObject jsonIndicador = jsonIndicadores.getJSONObject(j);
                IndicadorFichaEvaluacion objIndicador = new IndicadorFichaEvaluacion(jsonIndicador.getString("nom"),
                        jsonIndicador.getString("des"), jsonIndicador.getString("docVer"), objCont);
                objIndicador.setEstReg('A');
                objIndicador.setFecMod(new Date());
                objCont.getIndicadorFichaEvaluacions().add(objIndicador);
            }
            fep.getContenidoFichaEvaluacions().add(objCont);
        }
    }
    private JSONObject getJSONContenidoFicha(String nombre){

        try {
            //logger.log(Level.INFO,"esta es la ruta del docuemento: {0}",ServicioREST.PATH_SIGESMED+"\\WEB-INF\\classes\\fep_otros.json");
            Reader reader = new InputStreamReader(new FileInputStream(ServicioREST.PATH_SIGESMED+ File.separator +"WEB-INF" + File.separator +"classes" + File.separator + "fep_otros.json"),"UTF-8");
            JSONArray array = new JSONArray(new JSONTokener(reader));
            JSONObject jsonFichaOtros = array.getJSONObject(0);
            for(int i = 0; i< array.length(); i++){
                JSONObject jsonFicha = array.getJSONObject(i);
                if(jsonFicha.getString("rol").equals(EntityUtil.getTipoCargo(nombre))) 
                    return jsonFicha;
            }
            return jsonFichaOtros;
        } catch (UnsupportedEncodingException e) {
           return null;
        } catch (FileNotFoundException e) {
            return null;
        }

    }
}
