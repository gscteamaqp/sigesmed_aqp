package com.dremo.ucsm.gsc.sigesmed.util;

import com.itextpdf.kernel.color.Color;
import com.itextpdf.layout.Style;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.VerticalAlignment;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ITextMatricula extends ITextUtil {

    SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");

    public ITextMatricula(String path, String name) {
        super(path, name);
    }

    public ITextMatricula(String path, String name, boolean pageRotate) {
        super(path, name, pageRotate);
    }

    public ITextMatricula(String path, String name, boolean pageRotate,
            double leftMargin, double rightMargin, double topMargin, double bottonMargin) {
        super(path, name, pageRotate, leftMargin, rightMargin, topMargin, bottonMargin);
    }

    public void insertTableConstanciaMatricula(Style fontTitles, Style fontContent,
            String estId, String estNom, String orgId, String orgNom, String nivel,
            String jornadaAbr, String jornada, String turno, String grado, char seccion,
            String apoderadoDNI, String apoderadoNom) {

        Table table = new Table(4);
        Cell cell;

        cell = new Cell(1, 1);
        cell.add(new Paragraph("Estudiante: ").addStyle(fontTitles));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 1);
        cell.add(new Paragraph(estId).addStyle(fontContent));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 2);
        cell.add(new Paragraph(estNom).addStyle(fontContent));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 1);
        cell.add(new Paragraph("Institucion Educativa: ").addStyle(fontTitles));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 1);
        cell.add(new Paragraph(orgId).addStyle(fontContent));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 2);
        cell.add(new Paragraph(orgNom).addStyle(fontContent));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 1);
        cell.add(new Paragraph("Nivel:").addStyle(fontTitles));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 1);
        cell.add(new Paragraph(nivel).addStyle(fontContent));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 1);
        cell.add(new Paragraph("Turno:").addStyle(fontTitles));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 1);
        cell.add(new Paragraph(turno).addStyle(fontContent));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 1);
        cell.add(new Paragraph("Jornada:").addStyle(fontTitles));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 1);
        cell.add(new Paragraph(jornadaAbr).addStyle(fontContent));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 2);
        cell.add(new Paragraph(jornada).addStyle(fontContent));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 1);
        cell.add(new Paragraph("Grado:").addStyle(fontTitles));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 1);
        cell.add(new Paragraph(grado).addStyle(fontContent));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 1);
        cell.add(new Paragraph("Seccion:").addStyle(fontTitles));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 1);
        cell.add(new Paragraph("" + seccion).addStyle(fontContent));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 1);
        cell.add(new Paragraph("Apoderado:").addStyle(fontTitles));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 1);
        cell.add(new Paragraph(apoderadoDNI).addStyle(fontContent));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 2);
        cell.add(new Paragraph(apoderadoNom).addStyle(fontContent));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        doc.add(table);
    }

    public void insertTableConstanciaVacante(Style fontTitles, Style fontContent,
            String estId, String estNom, String orgDesId, String orgDesNom, String nivel,
            String jornadaAbr, String jornada, String turno, String grado, char seccion) {

        Table table = new Table(4);
        Cell cell;

        cell = new Cell(1, 1);
        cell.add(new Paragraph("Estudiante:").addStyle(fontTitles));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 1);
        cell.add(new Paragraph(estId).addStyle(fontContent));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 2);
        cell.add(new Paragraph(estNom).addStyle(fontContent));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 1);
        cell.add(new Paragraph("Institucion Educativa Destino:").addStyle(fontTitles));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 1);
        cell.add(new Paragraph(orgDesId).addStyle(fontContent));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 2);
        cell.add(new Paragraph(orgDesNom).addStyle(fontContent));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 1);
        cell.add(new Paragraph("Nivel:").addStyle(fontTitles));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 1);
        cell.add(new Paragraph(nivel).addStyle(fontContent));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 1);
        cell.add(new Paragraph("Turno:").addStyle(fontTitles));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 1);
        cell.add(new Paragraph(turno).addStyle(fontContent));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 1);
        cell.add(new Paragraph("Jornada:").addStyle(fontTitles));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 1);
        cell.add(new Paragraph(jornadaAbr).addStyle(fontContent));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 2);
        cell.add(new Paragraph(jornada).addStyle(fontContent));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 1);
        cell.add(new Paragraph("Grado:").addStyle(fontTitles));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 1);
        cell.add(new Paragraph(grado).addStyle(fontContent));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 1);
        cell.add(new Paragraph("Seccion:").addStyle(fontTitles));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 1);
        cell.add(new Paragraph("" + seccion).addStyle(fontContent));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        doc.add(table);
    }

    public void insertTableNominaMatricula(Style normalTitle, Style content,
            String orgNombre, String orgGestion, String orgCodigoModular, String orgCaracteristica,
            String orgPrograma, String orgResolucionCreacion, String orgForma, String orgNivelCiclo,
            String orgGradoEdad, String orgVariante, String orgSeccion, String orgSeccionNombre, String orgTurno, String orgModalidad,
            String orgJornada, String periodoLecIni, String periodoLecFin, String ubicacionGeoDpto, String ubicacionGeoProv,
            String ubicacionGeoDist, String ubicacionGeoCentroPoblado, String gestionEduCodigo,
            String gestionEduNombreUgelDre, List<Object[]> alumnos) {
        Table table = new Table(102);
        Cell cell;

        insertHeader(table, normalTitle);

        cell = new Cell(2, 15);
        cell.add(new Paragraph("Datos de la Instancia de Gestion Educativa Descentralizada (DRE-UGEL)").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 36);
        cell.add(new Paragraph("Datos de la Institucion Educativa o Programa Educativo").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 24);
        cell.add(new Paragraph("Periodo Lectivo").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 27);
        cell.add(new Paragraph("Ubicacion Geografica").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 8);
        cell.add(new Paragraph("Numero y/o Nombre").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 16);
        cell.add(new Paragraph(orgNombre).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 8);
        cell.add(new Paragraph("Gestion(7)").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 4);
        cell.add(new Paragraph(orgGestion).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 4);
        cell.add(new Paragraph("Inicio").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 8);
        cell.add(new Paragraph(periodoLecIni).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 4);
        cell.add(new Paragraph("Fin").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 8);
        cell.add(new Paragraph(periodoLecFin).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 4);
        cell.add(new Paragraph("Dpto.").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 23);
        cell.add(new Paragraph(ubicacionGeoDpto).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 6);
        cell.add(new Paragraph("Codigo").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 9);
        cell.add(new Paragraph(gestionEduCodigo).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 8);
        cell.add(new Paragraph("Codigo Modular").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 16);
        cell.add(new Paragraph(orgCodigoModular).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 8);
        cell.add(new Paragraph("Caracteristica(4)").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 4);
        cell.add(new Paragraph(orgCaracteristica).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 12);
        cell.add(new Paragraph("Programa(8)").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 12);
        cell.add(new Paragraph(orgPrograma).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 4);
        cell.add(new Paragraph("Prov.").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 23);
        cell.add(new Paragraph(ubicacionGeoProv).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(3, 6);
        cell.add(new Paragraph("Nombre de la DRE-UGEL").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(3, 9);
        cell.add(new Paragraph(gestionEduNombreUgelDre).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 8);
        cell.add(new Paragraph("Resolucion de Creacion N°").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 16);
        cell.add(new Paragraph(orgResolucionCreacion).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 8);
        cell.add(new Paragraph("Forma(5)").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 4);
        cell.add(new Paragraph(orgForma).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 12);
        cell.add(new Paragraph("Nivel/Ciclo(1)").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 12);
        cell.add(new Paragraph(orgNivelCiclo).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 4);
        cell.add(new Paragraph("Dist.").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 23);
        cell.add(new Paragraph(ubicacionGeoDist).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 8);
        cell.add(new Paragraph("Grado/Edad(3)").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 4);
        cell.add(new Paragraph(orgGradoEdad).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 8);
        cell.add(new Paragraph("Seccion(6)").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 4);
        cell.add(new Paragraph(orgSeccion).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 8);
        cell.add(new Paragraph("Variante(10)").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 4);
        cell.add(new Paragraph(orgVariante).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 12);
        cell.add(new Paragraph("Nombre Seccion (Solo Inicial)").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 12);
        cell.add(new Paragraph(orgSeccionNombre).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 27);
        cell.add(new Paragraph("Centro Poblado").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 8);
        cell.add(new Paragraph("Modalidad(2)").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 16);
        cell.add(new Paragraph(orgModalidad).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 8);
        cell.add(new Paragraph("Turno(9)").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 4);
        cell.add(new Paragraph(orgTurno).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 12);
        cell.add(new Paragraph("Jornada Escolar").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 12);
        cell.add(new Paragraph(orgJornada).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 27);
        cell.add(new Paragraph(ubicacionGeoCentroPoblado).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        insertCabecera(table, normalTitle);

        for (int i = 0; i < alumnos.size(); i++) {
            insertEstudiante(i + 1, table, content, alumnos.get(i));
        }
        doc.add(table);

    }

    public void insertTableIndiceNomina(Style normal) {
        Table table = new Table(102);
        Cell cell;

        cell = new Cell(1, 9);
        cell.add(new Paragraph("(1) Nivel / Ciclo").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 25);
        cell.add(new Paragraph(
                "Para el caso EBR/EDA/EAD/EBE: (INI) Inicial, (PRI) Primaria, (SEC) Secundaria\n"
                + "Para el caso EBA: (INI) Inicial, (INT) Intermedio, (AVZ) Avanzado").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 9);
        cell.add(new Paragraph("(2) Modalidad").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 25);
        cell.add(new Paragraph(
                "(EBR) Educ.Básica Regular, (EBA) Educ.Básica Alternativa,(EDA) Educ. Adultos, "
                + "(EBE) Educ. Básica Especial,(EAD) Educ. a Distancia").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 9);
        cell.add(new Paragraph("(3) Grado/Edad").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 25);
        cell.add(new Paragraph(
                "En el caso de Primaria o Secundaria: registrar grados: 1,2,3,4,5 ó 6.\n"
                + "En caso Inicial: registrar Edad (0,1,2,3,4,5). Colocar \"-\" si la presente Nómina contiene alumnos  de varias  edades").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 9);
        cell.add(new Paragraph("(4) Característ.").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 25);
        cell.add(new Paragraph("(U) Unidocente, (PM) Polidocente Multigrado, (PC) Polidocente Completo").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 9);
        cell.add(new Paragraph("(5) Forma").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 25);
        cell.add(new Paragraph("(Esc) Escolarizado, (NoEsc) No Escolarizado\n"
                + "Para el caso EBA:(P) Presencial,  (SP) Semi Presencial,(AD) A distancia").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 9);
        cell.add(new Paragraph("(6) Sección").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 25);
        cell.add(new Paragraph("A,B,C,…  Colocar \"-\" si es sección única o si se trata de Nivel Inicial").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 9);
        cell.add(new Paragraph("(7) Gestión").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 25);
        cell.add(new Paragraph("(P) Púbico (PR) Privado").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 9);
        cell.add(new Paragraph("(8) Programa").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 25);
        cell.add(new Paragraph(
                "(PBN) PEBANA:  Prog.de Educ.Bás.Alter.de Niños y Adolesc.\n"
                + "(PBJ) PEBAJA:  Prog. de Educ.Bás. Alter.de Jóvenes y Adultos\n"
                + "Colocar \"-\" en caso de no corresponder").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 9);
        cell.add(new Paragraph("(9) Turno").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 25);
        cell.add(new Paragraph("(M) Mañana,  (T) Tarde, (V) Vespertino, (N) Noche").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 9);
        cell.add(new Paragraph("(10) Variante").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 25);
        cell.add(new Paragraph("Sólo para Educ.Sec. de Adultos: (CH) Científ. Humanista,(T) Técnica.\nColocar \"-\" en caso de no corresponder").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 9);
        cell.add(new Paragraph("(11) Situación de Matricula").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 25);
        cell.add(new Paragraph("(I) Ingresante, (P) Promovido, (R) Repitente, (RE) Reentrante").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 9);
        cell.add(new Paragraph("(12) País").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 25);
        cell.add(new Paragraph("(P) Perú, (E) Ecuador, (C) Colombia, (B) Brasil, (Bo) Bolivia, (Ch) Chile, (OT) Otro").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 9);
        cell.add(new Paragraph("(13) Lengua").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 25);
        cell.add(new Paragraph("(C) Castellano, (Q) Quechua, (AI) Aimara, (OT) Otra lengua, (E) Lengua extranjera").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 9);
        cell.add(new Paragraph("(14) Escolarid.de la Madre").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 25);
        cell.add(new Paragraph("(A) Analfabeta, (P) Primaria, (S) Secundaria, y (SP) Superior").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 9);
        cell.add(new Paragraph("(15) Tipo de discapacidad").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 25);
        cell.add(new Paragraph(""
                + "(DI) Intelectual, (DA) Auditiva, (DV) Visual, (DM) Motora, (OT) Otra.\n"
                + "En caso de no adolecer discapacidad, dejar en blanco").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 9);
        cell.add(new Paragraph("(16) IE de procedencia").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 25);
        cell.add(new Paragraph("Sólo para el caso de estudiantes que proceden de otra Institución Educativa.").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 68);
        cell.add(new Paragraph(""));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        addColumnasNumericas(table, normal);

        doc.add(table);

    }

    public void addColumnasNumericas(Table table, Style normal) {
        Cell cell;

        cell = new Cell(1, 9);
        cell.add(new Paragraph("(17) Datos del Estudiante").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 12);
        cell.add(new Paragraph("(01E) Sexo H/M").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 13);
        cell.add(new Paragraph("(02E) Situacion Matricula(11)").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 17);
        cell.add(new Paragraph("(03E) Pais(12)").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 17);
        cell.add(new Paragraph("(04E) Padre Vive SI/NO").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 17);
        cell.add(new Paragraph("(05E) Madre Vive SI/NO").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 17);
        cell.add(new Paragraph("(06E) Lengua Materna(13)").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 9);
        cell.add(new Paragraph("").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 12);
        cell.add(new Paragraph("(07E) Segunda Lengua(13)").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 13);
        cell.add(new Paragraph("(08E) Trabaja el Estudiante SI/NO").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 17);
        cell.add(new Paragraph("(09E) Horas Semanales que labora").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 17);
        cell.add(new Paragraph("(10E) Escolaridad de la madre(14)").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 17);
        cell.add(new Paragraph("(11E) Nacimiento Registrado SI/NO").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 17);
        cell.add(new Paragraph("(12E) Tipo de Discapacidad(15)").addStyle(normal));
        cell.setVerticalAlignment(VerticalAlignment.TOP);
        cell.setTextAlignment(TextAlignment.LEFT);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

    }

    public void insertPiePaginaNomina(int numMujeres, int numHombre, Style normalTitle, Style content) {
        Table table = new Table(102);

        Cell cell;

        cell = new Cell(1, 15);
        cell.add(new Paragraph("Resumen").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 60);
        cell.add(new Paragraph("").addStyle(content));
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 27);
        cell.add(new Paragraph("Aprobacion de la Nomina").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 10);
        cell.add(new Paragraph("Hombres").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 5);
        cell.add(new Paragraph("" + numHombre).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 26);
        cell.add(new Paragraph("_________________________________").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.BOTTOM);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 34);
        cell.add(new Paragraph("______________________________________________").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.BOTTOM);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 17);
        cell.add(new Paragraph("R.D. Institucional").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 3);
        cell.add(new Paragraph("Dia").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 3);
        cell.add(new Paragraph("Mes").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 4);
        cell.add(new Paragraph("Año").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 10);
        cell.add(new Paragraph("Mujeres").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 5);
        cell.add(new Paragraph("" + numMujeres).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 5);
        cell.add(new Paragraph("").addStyle(content));
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 16);
        cell.add(new Paragraph("Responsable de la Matricula").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 10);
        cell.add(new Paragraph("").addStyle(content));
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 24);
        cell.add(new Paragraph("Director(a) de la Institucion Educativa").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 5);
        cell.add(new Paragraph("").addStyle(content));
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 17);
        cell.add(new Paragraph("").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 3);
        cell.add(new Paragraph("").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 3);
        cell.add(new Paragraph("").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 4);
        cell.add(new Paragraph("").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        int total = numHombre + numMujeres;

        cell = new Cell(1, 10);
        cell.add(new Paragraph("Total").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 5);
        cell.add(new Paragraph("" + total).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 5);
        cell.add(new Paragraph("").addStyle(content));
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 16);
        cell.add(new Paragraph("Firma - Post Firma").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 10);
        cell.add(new Paragraph("").addStyle(content));
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 24);
        cell.add(new Paragraph("Firma - Post Firma y Sello").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 5);
        cell.add(new Paragraph("").addStyle(content));
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        cell = new Cell(1, 27);
        cell.add(new Paragraph("").addStyle(content));
        cell.setBorder(Border.NO_BORDER);
        table.addCell(cell);

        table.setKeepTogether(true);
        doc.add(table);
    }

    private void insertCabecera(Table table, Style normalTitle) {
        Cell cell;
        cell = new Cell(2, 2);
        cell.add(new Paragraph("N").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(2, 13);
        cell.add(new Paragraph("N° de DNI o Codigo del Estudiante").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(2, 26);
        cell.add(new Paragraph("Apellidos y Nombres (Orden Alfabetico)").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 10);
        cell.add(new Paragraph("Fecha de Nacimiento").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 24);
        cell.add(new Paragraph("Datos del Estudiante(17)").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 27);
        cell.add(new Paragraph("Institucion Educativa de Procedencia(16)").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 3);
        cell.add(new Paragraph("Dia").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 3);
        cell.add(new Paragraph("Mes").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 4);
        cell.add(new Paragraph("Año").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 2);
        cell.add(new Paragraph("01E").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);
        cell = new Cell(1, 2);
        cell.add(new Paragraph("02E").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);
        cell = new Cell(1, 2);
        cell.add(new Paragraph("03E").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);
        cell = new Cell(1, 2);
        cell.add(new Paragraph("04E").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);
        cell = new Cell(1, 2);
        cell.add(new Paragraph("05E").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);
        cell = new Cell(1, 2);
        cell.add(new Paragraph("06E").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);
        cell = new Cell(1, 2);
        cell.add(new Paragraph("07E").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);
        cell = new Cell(1, 2);
        cell.add(new Paragraph("08E").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);
        cell = new Cell(1, 2);
        cell.add(new Paragraph("09E").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);
        cell = new Cell(1, 2);
        cell.add(new Paragraph("10E").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);
        cell = new Cell(1, 2);
        cell.add(new Paragraph("11E").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);
        cell = new Cell(1, 2);
        cell.add(new Paragraph("12E").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 7);
        cell.add(new Paragraph("Codigo Modular").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);

        cell = new Cell(1, 20);
        cell.add(new Paragraph("Numero y/o Nombre").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addCell(cell);
    }

    private void insertHeader(Table table, Style normalTitle) {
        Cell cell;
        cell = new Cell(2, 2);
        cell.add(new Paragraph("N").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addHeaderCell(cell);

        cell = new Cell(2, 13);
        cell.add(new Paragraph("N° de DNI o Codigo del Estudiante").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addHeaderCell(cell);

        cell = new Cell(2, 26);
        cell.add(new Paragraph("Apellidos y Nombres (Orden Alfabetico)").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addHeaderCell(cell);

        cell = new Cell(1, 10);
        cell.add(new Paragraph("Fecha de Nacimiento").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addHeaderCell(cell);

        cell = new Cell(1, 24);
        cell.add(new Paragraph("Datos del Estudiante(17)").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addHeaderCell(cell);

        cell = new Cell(1, 27);
        cell.add(new Paragraph("Institucion Educativa de Procedencia").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addHeaderCell(cell);

        cell = new Cell(1, 3);
        cell.add(new Paragraph("Dia").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addHeaderCell(cell);

        cell = new Cell(1, 3);
        cell.add(new Paragraph("Mes").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addHeaderCell(cell);

        cell = new Cell(1, 4);
        cell.add(new Paragraph("Año").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addHeaderCell(cell);

        cell = new Cell(1, 2);
        cell.add(new Paragraph("01E").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addHeaderCell(cell);

        cell = new Cell(1, 2);
        cell.add(new Paragraph("02E").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addHeaderCell(cell);

        cell = new Cell(1, 2);
        cell.add(new Paragraph("03E").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addHeaderCell(cell);

        cell = new Cell(1, 2);
        cell.add(new Paragraph("04E").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addHeaderCell(cell);

        cell = new Cell(1, 2);
        cell.add(new Paragraph("05E").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addHeaderCell(cell);

        cell = new Cell(1, 2);
        cell.add(new Paragraph("06E").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addHeaderCell(cell);

        cell = new Cell(1, 2);
        cell.add(new Paragraph("07E").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addHeaderCell(cell);

        cell = new Cell(1, 2);
        cell.add(new Paragraph("08E").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addHeaderCell(cell);

        cell = new Cell(1, 2);
        cell.add(new Paragraph("09E").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addHeaderCell(cell);

        cell = new Cell(1, 2);
        cell.add(new Paragraph("10E").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addHeaderCell(cell);

        cell = new Cell(1, 2);
        cell.add(new Paragraph("11E").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addHeaderCell(cell);

        cell = new Cell(1, 2);
        cell.add(new Paragraph("12E").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addHeaderCell(cell);

        cell = new Cell(1, 7);
        cell.add(new Paragraph("Codigo Modular").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addHeaderCell(cell);

        cell = new Cell(1, 20);
        cell.add(new Paragraph("Numero y/o Nombre").addStyle(normalTitle));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        cell.setBackgroundColor(Color.GRAY);
        table.addHeaderCell(cell);

        table.setSkipFirstHeader(true);
    }

    private void insertEstudiante(int id, Table table, Style content, Object[] data) {
        Cell cell;
        cell = new Cell(1, 2);
        cell.add(new Paragraph("" + id).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 13);
        cell.add(new Paragraph(data[0].toString()).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        String nombreCompleto = data[2].toString() + " " + data[3].toString() + " " + data[4].toString();
        cell = new Cell(1, 26);
        cell.add(new Paragraph(nombreCompleto).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        Date myDate = DateUtil.getDate2String(data[5].toString());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(myDate);

        cell = new Cell(1, 3);
        cell.add(new Paragraph("" + calendar.get(Calendar.DAY_OF_MONTH)).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        int month = calendar.get(Calendar.MONTH) + 1;
        cell = new Cell(1, 3);
        cell.add(new Paragraph("" + month).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 4);
        cell.add(new Paragraph("" + calendar.get(Calendar.YEAR)).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 2);
        cell.add(new Paragraph(data[6].toString()).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 2);
        cell.add(new Paragraph(data[7].toString()).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 2);
        cell.add(new Paragraph(getPais(data[8].toString())).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 2);
        cell.add(new Paragraph("Si").addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 2);
        cell.add(new Paragraph("Si").addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 2);
        cell.add(new Paragraph(getLengua(data[9].toString())).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 2);
        cell.add(new Paragraph(getLengua(data[10].toString())).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 2);
        cell.add(new Paragraph("No").addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 2);
        cell.add(new Paragraph("").addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 2);
        cell.add(new Paragraph("S").addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 2);
        cell.add(new Paragraph(getNacimientoRegistrado(getLengua(data[11].toString()))).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 2);
        cell.add(new Paragraph(getLengua(data[12].toString())).addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 7);
        cell.add(new Paragraph("").addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

        cell = new Cell(1, 20);
        cell.add(new Paragraph("").addStyle(content));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setTextAlignment(TextAlignment.CENTER);
        table.addCell(cell);

//        cell = new Cell(1, 7);
//        cell.add(new Paragraph(getLengua(data[13].toString())).addStyle(content));
//        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
//        cell.setTextAlignment(TextAlignment.CENTER);
//        table.addCell(cell);
//
//        cell = new Cell(1, 20);
//        cell.add(new Paragraph(getLengua(data[14].toString())).addStyle(content));
//        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
//        cell.setTextAlignment(TextAlignment.CENTER);
//        table.addCell(cell);
    }

    private String getPais(String pais) {
        switch (pais) {
            case "1": { //peru
                return "P";
            }
            case "2": { //ecuador
                return "E";
            }
            case "3": { //colombia
                return "C";
            }
            case "4": { //brasil
                return "B";
            }
            case "5": { //bolivia
                return "Bo";
            }
            case "6": { //chile
                return "Ch";
            }
            case "7": { //otro
                return "OT";
            }
            default: {
                return "P";
            }
        }
    }

    private String getLengua(String lengua) {
        switch (lengua) {
            case "1": { //castelano
                return "C";
            }
            case "2": { //quechua
                return "Q";
            }
            case "3": { //aimara
                return "AI";
            }
            case "4": { //otra lengua
                return "OT";
            }
            case "5": { //lengua extrangera
                return "E";
            }
            default: {
                return "C";
            }
        }
    }

    private String getNacimientoRegistrado(String nac) {
        switch (nac) {
            case "true": { //
                return "Si";
            }
            case "false": { //
                return "No";
            }
            default: {
                return "Si";
            }
        }
    }
    
    private String getSituacionMatricula(int sit) {
        switch (sit) {
            case 1: { // Ingresante
                return "I";
            }
            case 2: { // Promovido
                return "P";
            }
            case 3: { // Repitente
                return "R";
            }
            case 4: { // Reentrante
                return "RE";
            }
            default: {
                return "I";
            }
        }
    }
}
