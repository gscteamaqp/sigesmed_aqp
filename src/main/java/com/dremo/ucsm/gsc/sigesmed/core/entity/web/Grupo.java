package com.dremo.ucsm.gsc.sigesmed.core.entity.web;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="grupo" ,schema="pedagogico" )
public class Grupo  implements java.io.Serializable {

    @Id
    @Column(name="gru_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_grupo", sequenceName="pedagogico.grupo_gru_id_seq" )
    @GeneratedValue(generator="secuencia_grupo")
    private int gruId;
    @Column(name="tip_gru")
    private boolean tipGru;
    @Column(name="nom",length=64)
    private String nom;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_mod", nullable=false, length=29)
    private Date fecMod;    
    @Column(name="usu_mod")
    private int usuMod;
    @Column(name="est_reg")
    private char estReg;
    
    @Column(name="org_id")
    private int orgId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="org_id",updatable = false,insertable = false)
    private Organizacion organizacion;
    
    @JoinTable(name = "pedagogico.grupo_usuario",joinColumns = {
        @JoinColumn(name = "gru_id", referencedColumnName = "gru_id")}, inverseJoinColumns = {
        @JoinColumn(name = "usu_ses_id", referencedColumnName = "usu_ses_id")})
    @ManyToMany//(cascade=CascadeType.ALL)
    private List<UsuarioSession> usuarios;

    public Grupo() {
    }
    public Grupo(int gruId) {
        this.gruId = gruId;
    }
    public Grupo(int gruId,boolean tipGru, String nom,int orgId, Date fecMod, int usuMod, char estReg) {
       this.gruId = gruId;
       this.tipGru = tipGru;
       this.nom = nom;
       this.orgId = orgId;
       this.fecMod = fecMod;
       this.usuMod = usuMod;
       this.estReg = estReg;
    }
   
     
    public int getGruId() {
        return this.gruId;
    }    
    public void setGruId(int gruId) {
        this.gruId = gruId;
    }
    
    public boolean getTipGru() {
        return this.tipGru;
    }
    public void setTipGru(boolean tipGru) {
        this.tipGru = tipGru;
    }
    public String getNom() {
        return this.nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    public Date getFecMod() {
        return this.fecMod;
    }
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }
    
    public int getUsuMod() {
        return this.usuMod;
    }
    public void setUsuMod(int usuMod) {
        this.usuMod = usuMod;
    }
    
    public char getEstReg() {
        return this.estReg;
    }
    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }
    
    public int getOrgId() {
        return this.orgId;
    }    
    public void setOrgId(int orgId) {
        this.orgId = orgId;
    }
    
    public Organizacion getOrganizacion() {
        return this.organizacion;
    }
    public void setOrganizacion(Organizacion organizacion) {
        this.organizacion = organizacion;
    }
    
    public List<UsuarioSession> getUsuarios() {
        return this.usuarios;
    }
    public void setUsuarios(List<UsuarioSession> usuarios) {
        this.usuarios = usuarios;
    }
}


