/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.smdg;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.ItemFileDetalle;

/**
 *
 * @author Administrador
 */
public interface ItemFileDetalleDao extends GenericDao<ItemFileDetalle>{
    public String buscarUltimoCodigo();
}
