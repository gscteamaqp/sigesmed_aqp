/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.web;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.TareaEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaTarea;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaTareaModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.TareaDocumento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.TareaEscolar;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author abel
 */
public class TareaEscolarDaoHibernate extends GenericDaoHibernate<TareaEscolar> implements TareaEscolarDao{
    
    @Override
    public void enviarTarea(TareaEscolar tarea,BandejaTarea bandeja){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            String hql = "UPDATE TareaEscolar te SET te.fecEnv=:p2,te.fecEnt=:p3,te.estado=:p4 WHERE te.tarEscId =:p1";            
            Query query = session.createQuery(hql);
            query.setParameter("p1", tarea.getTarEscId());
            query.setParameter("p2", tarea.getFecEnv());
            query.setParameter("p3", tarea.getFecEnt());
            query.setParameter("p4", tarea.getEstado());
            query.executeUpdate();
            
            //actualizando la bandeja de tarea
            session.persist(bandeja);
            miTx.commit();
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo enviar la tarea a un alumno\\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo enviar la tarea a un alumno\\n"+ e.getMessage());
        }
        finally{
            session.close();
        }
    }
    @Override
    public void enviarTarea(TareaEscolar tarea,List<BandejaTarea> bandejas){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            String hql = "UPDATE TareaEscolar te SET te.fecEnv=:p2,te.fecEnt=:p3,te.estado=:p4 WHERE te.tarEscId =:p1";            
            Query query = session.createQuery(hql);
            query.setParameter("p1", tarea.getTarEscId());
            query.setParameter("p2", tarea.getFecEnv());
            query.setParameter("p3", tarea.getFecEnt());
            query.setParameter("p4", tarea.getEstado());            
            query.executeUpdate();
            
            //actualizando la bandeja de tareas
            for(BandejaTarea bt: bandejas){
                session.persist(bt);
            }                
            miTx.commit();        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo enviar la tarea a los alumnos\\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo enviar la tarea a los alumnos\\n"+ e.getMessage());
        }
        finally{
            session.close();
        }
    }

    @Override
    public List<TareaEscolar> buscarPorPlanEstudios(int planID){
        List<TareaEscolar> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT t FROM TareaEscolar t WHERE t.plaEstId=:p1 and t.estReg='A'";
            Query query = session.createQuery(hql);
            query.setParameter("p1", planID);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las tareas por plan de estudios\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las tareas por plan de estudios\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public List<TareaEscolar> buscarPorDocente(int planID,int docenteID){
        List<TareaEscolar> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT t FROM TareaEscolar t WHERE t.plaEstId=:p1 and t.usuMod=:p2 and t.estReg='A'";
            Query query = session.createQuery(hql);
            query.setParameter("p1", planID);
            query.setParameter("p2", docenteID);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las tareas por plan de estudios y docente\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las tareas por plan de estudios y docente\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public List<BandejaTareaModel> buscarAlumnosQueCumplieronTarea(int tareaID){
        List<BandejaTareaModel> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaTareaModel(b.banTarId,b.fecEnt,b.fecVis,b.nota,b.not_lit,b.estado,b.alumno.nom,b.alumno.apePat,b.alumno.apeMat,b.alumno.perId,b.tareaEscolar.tip_per,b.tareaEscolar.id_per) FROM BandejaTarea b WHERE b.tarEscId=:p1 and (b.estado='E' or b.estado='C')";
            
            Query query = session.createQuery(hql);
            query.setParameter("p1", tareaID);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los alumnos que cumplieron con tarea\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los alumnos que cumplieron con tarea\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public List<TareaDocumento> verDocumentosTarea(int bandejaTareaID){
        List<TareaDocumento> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT td FROM TareaDocumento td WHERE td.bandejaTarea.banTarId=:p1";// and b.estado='E'";
            
            Query query = session.createQuery(hql);
            query.setParameter("p1", bandejaTareaID);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los documentos de la tarea resuelta\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los documentos de la tarea resuelta\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    
    @Override
    public List<BandejaTarea> buscarTareasPorAlumno(int planID,int alumnoID){
        List<BandejaTarea> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT DISTINCT bt FROM BandejaTarea bt JOIN FETCH bt.tareaEscolar LEFT JOIN FETCH bt.documentos WHERE bt.tareaEscolar.plaEstId=:p1 and bt.alumnoID=:p2";
            Query query = session.createQuery(hql);
            query.setParameter("p1", planID);
            query.setParameter("p2", alumnoID);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las tareas por plan de estudios y alumno\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las tareas por plan de estudios y alumno\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    
    @Override
    public void cambiarNombreAdjunto(int tarEscID,String nombreAdjunto ){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{            
            String hql = "UPDATE TareaEscolar t SET t.docAdj=:p2 WHERE t.tarEscId =:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", tarEscID);
            query.setParameter("p2", nombreAdjunto);
            query.executeUpdate();
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo cambiar el nombre del documentos adjunto tarea\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    
    @Override
    public void enviarTareaResuelta(BandejaTarea bandeja ){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            //insertando todos los documentos adjuntos de la tarea
            for(TareaDocumento td : bandeja.getDocumentos()){
                
                String hql = "insert into pedagogico.tarea_documento_adjunto ( ban_tar_esc_id, ban_doc_adj_id, nom_doc ) VALUES (:p1,:p2,:p3)";
                Query query = session.createSQLQuery(hql);
                query.setParameter("p1", bandeja.getBanTarId());
                query.setParameter("p2", td.getTarDocId());
                query.setParameter("p3", td.getNomDoc());

                query.executeUpdate();
            }
            //actualizando el estado y la fecha de entrega de la tarea
            String hql = "UPDATE BandejaTarea bt SET bt.fecEnt=:p2,bt.estado=:p3 WHERE bt.banTarId=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", bandeja.getBanTarId());
            query.setParameter("p2", bandeja.getFecEnt());
            query.setParameter("p3", bandeja.getEstado());
            
            query.executeUpdate();
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo enviar la tarea resuelta\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    
    @Override
    public void calificarTareaResuelta(BandejaTarea bandeja ){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{            
            String hql = "UPDATE BandejaTarea bt SET bt.estado=:p2, bt.nota=:p3, bt.fecVis=:p4 , bt.not_lit=:p5 WHERE bt.banTarId =:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", bandeja.getBanTarId());
            query.setParameter("p2", bandeja.getEstado());
            query.setParameter("p3", bandeja.getNota());
            query.setParameter("p4", bandeja.getFecVis());
            query.setParameter("p5",bandeja.getNot_lit());
            query.executeUpdate();
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo calificar la tarea\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    
    @Override
    public void finalizarTarea(TareaEscolar tarea){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{            
            String hql = "UPDATE TareaEscolar t SET t.estado=:p2,t.fecMod=:p3 WHERE t.tarEscId =:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", tarea.getTarEscId() );
            query.setParameter("p2", tarea.getEstado());
            query.setParameter("p3", tarea.getFecMod());
            query.executeUpdate();
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo dar por finalizado la tarea\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    @Override
    public List<BandejaTareaModel> buscarAlumnosQueFaltaCalificarTarea(int tareaID){
        List<BandejaTareaModel> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaTareaModel(b.banTarId,b.fecEnt,b.fecVis,b.nota,b.estado,b.alumno.persona.nom,b.alumno.persona.apePat,b.alumno.persona.apeMat) FROM BandejaTarea b WHERE b.tarEscId=:p1 and b.estado='E'";
            
            Query query = session.createQuery(hql);
            query.setParameter("p1", tareaID);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los alumnos que faltan calificar la tarea\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los alumnos que faltan calificar la tarea\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public void tareasFueraDeTiempo(List<Integer> tareas){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            for(Integer tareaID:tareas){
                String hql = "UPDATE BandejaTarea bt SET bt.estado=:p2 WHERE bt.banTarId=:p1";
                Query query = session.createQuery(hql);
                query.setParameter("p1", tareaID);
                query.setParameter("p2", 'F');
                query.executeUpdate();
            }            
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo actualizar el estado a finalizado de las tareas\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }

    @Override
    public List<BandejaTarea> buscarTareasPorAlumnoAndArea(int planID, int alumnoID, int areaId) {
        List<BandejaTarea> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT DISTINCT bt FROM BandejaTarea bt JOIN FETCH bt.tareaEscolar te LEFT JOIN FETCH bt.documentos WHERE bt.tareaEscolar.plaEstId=:p1 and bt.alumnoID=:p2 AND te.areCurId=:p3";
            Query query = session.createQuery(hql);
            query.setParameter("p1", planID);
            query.setParameter("p2", alumnoID);
            query.setParameter("p3", areaId);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las tareas por plan de estudios y alumno\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las tareas por plan de estudios y alumno\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public TareaEscolar obtenertarea(int tar_id){

         Session session = HibernateUtil.getSessionFactory().openSession();
         TareaEscolar tarea ;
         try{
            String hql = "SELECT DISTINCT ta FROM TareaEscolar ta  WHERE ta.tarEscId=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", tar_id); 
            query.setMaxResults(1);
            tarea = ((TareaEscolar)query.uniqueResult());

         }catch(Exception e){
             System.out.println("No se pudo Obtener la tarea\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Obtener la tarea\\n "+ e.getMessage());            

         }
         finally{
            session.close();
        }
        return tarea;


    }

    @Override
    public TareaEscolar obtenertareaWeb(int tar_web_id) {
        
         TareaEscolar tarea_escolar = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT  e FROM TareaEscolar e JOIN FETCH e.bandeja WHERE e.id_tar=:p1 ";
            Query query = session.createQuery(hql);
            query.setParameter("p1",tar_web_id);
         //   return query.list();
          
           tarea_escolar = ((TareaEscolar)query.uniqueResult());
           
           if(tarea_escolar == null){
               hql = "SELECT  e FROM TareaEscolar e  WHERE e.id_tar=:p1 ";
                query = session.createQuery(hql);
                query.setParameter("p1",tar_web_id);
                tarea_escolar = ((TareaEscolar)query.uniqueResult());
           }
           return tarea_escolar;
        }
        catch(Exception e){
              System.out.println("No se pudo Obtener la Tarea Solicitada\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Obtener la Tarea Solicitada\n "+ e.getMessage());           
        }
         finally{
            session.close();
        }
      //  return tarea_escolar;
        
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
