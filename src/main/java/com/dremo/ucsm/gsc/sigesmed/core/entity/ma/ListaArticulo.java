/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.ma;

import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Seccion;
import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author ucsm
 */
@Entity
@Table(name = "lista_articulo", schema = "pedagogico")
public class ListaArticulo implements java.io.Serializable{
    @Embeddable
    public static class Id implements java.io.Serializable{
        @Column(name = "list_uti_id", nullable = false)
        protected int listUtiId;
        
        @Column(name = "art_esc_id", nullable = false)
        protected int artEscId;
        
        public Id(){}
        public Id(int listUtiId, int artEscId){
            this.listUtiId = listUtiId;
            this.artEscId = artEscId;
        }
        @Override
        public boolean equals(Object o){
            if(o != null && o instanceof Id){
                Id id = (Id) o;
                return this.listUtiId == id.listUtiId
                        && this.artEscId == id.artEscId;
            }
            return false;
        }
        @Override
        public int hashCode(){
            return this.listUtiId*this.artEscId + 1;
        }
    }
    @EmbeddedId
    @AttributeOverrides( {
            @AttributeOverride(name="listUtiId", column=@Column(name="list_uti_id", nullable=false) ),
            @AttributeOverride(name="artEscId", column=@Column(name="art_esc_id", nullable=false)) })
    
    private Id id = new Id();
    
    @Column(name = "can_art")
    private Integer canArt;

    @Column(name = "des_art")
    private String desArt;
    
    @Column (name="prec_art")
    private Double preArt;
    
    @Column(name = "usu_mod")
    private Integer usuMod;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length=29)
    private Date fecMod;

    @Column(name = "est_reg",length = 1)
    private Character estReg;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "list_uti_id",insertable = false, updatable = false)
    private ListaUtiles listaUtiles;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "art_esc_id",insertable = false, updatable = false)
    private ArticuloEscolar articulo;
    
    protected ListaArticulo(){}
    public ListaArticulo(ListaUtiles listaUtiles, ArticuloEscolar articulo,int canArt, String desArt,double preArt){
        this.listaUtiles = listaUtiles;
        this.articulo = articulo;
        this.canArt = canArt;
        this.desArt = desArt;
        this.preArt = preArt;
        
        this.id.listUtiId = listaUtiles.getListUtiId();
        this.id.artEscId = articulo.getArtEscId();
        
        //this.listaUtiles.getListaArticulo().add(this);
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public Id getId() {
        return id;
    }

    public void setId(Id id) {
        this.id = id;
    }

    public Integer getCanArt() {
        return canArt;
    }

    public void setCanArt(Integer canArt) {
        this.canArt = canArt;
    }

    public String getDesArt() {
        return desArt;
    }

    public void setDesArt(String desArt) {
        this.desArt = desArt;
    }

    public Double getPreArt() {
        return preArt;
    }

    public void setPreArt(Double preArt) {
        this.preArt = preArt;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
        
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public ListaUtiles getListaUtiles() {
        return listaUtiles;
    }

    public void setListaUtiles(ListaUtiles listaUtiles) {
        this.listaUtiles = listaUtiles;
    }

    public ArticuloEscolar getArticulo() {
        return articulo;
    }
    
    public void setArticulo(ArticuloEscolar articulo) {
        this.articulo = articulo;
    }  
}
