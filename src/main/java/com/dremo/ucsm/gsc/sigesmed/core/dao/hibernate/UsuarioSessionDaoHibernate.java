package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate;

import com.dremo.ucsm.gsc.sigesmed.core.dao.UsuarioSessionDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

public class UsuarioSessionDaoHibernate extends GenericDaoHibernate<UsuarioSession> implements UsuarioSessionDao {

    private static final Logger logger = Logger.getLogger(UsuarioSessionDaoHibernate.class.getName());
        
    @Override
    public List<Integer> listarUsuariosCalendario(int orgCod, int funCod){
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            String hql = "SELECT DISTINCT u.usuSesId FROM UsuarioSession u "
                        + "WHERE u.organizacion.orgId = :orgId AND u.organizacion.estReg = :estRegOrg AND u.estReg = :estRegUsu AND "
                        + "u.rolId IN "            
                        + "(SELECT DISTINCT r.claveRolFuncion.rol.rolId FROM RolFuncion r "
                            + "WHERE r.claveRolFuncion.funcionSistema.funSisId = :funSisId AND "
                            + "r.claveRolFuncion.rol.estReg = :estRegRol)";

            Query query = session.createQuery(hql);
            query.setParameter("orgId", orgCod);
            query.setParameter("estRegOrg", 'A');
            query.setParameter("estRegUsu", 'A');
            query.setParameter("funSisId", funCod);
            query.setParameter("estRegRol", 'A');

            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarUsuariosCalendario", e);
            throw e;
        } finally {
            session.close();
        }
    }
    
    @Override
    public List<Integer> listarAutoridadesCalendario(int usuCod, char tipAct) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            String hql = "SELECT u.organizacion.orgId FROM UsuarioSession u WHERE u.usuSesId = :usuSesId";
            
            Query query = session.createQuery(hql);
            query.setParameter("usuSesId", usuCod);
            
            int codOrg = (Integer) query.uniqueResult();
            
            switch(tipAct) {
                case 'D': hql = "SELECT DISTINCT u.usuSesId FROM UsuarioSession u WHERE u.organizacion.orgId = :orgId AND u.rol.rolId = 5";
                    break;
                    
                case 'U': hql = "SELECT DISTINCT u.usuSesId FROM UsuarioSession u WHERE u.organizacion.orgId = :orgId AND u.rol.rolId = 6";
                    break;
                    
                case 'I': hql = "SELECT DISTINCT u.usuSesId FROM UsuarioSession u WHERE u.organizacion.orgId = :orgId AND u.rol.rolId = 7";
                    break;
            }
            
            query = session.createQuery(hql);
            query.setParameter("orgId", codOrg);

            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarUsuariosCalendario", e);
            throw e;
        } finally {
            session.close();
        }
    }
}
