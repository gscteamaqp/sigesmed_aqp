package com.dremo.ucsm.gsc.sigesmed.core.entity.web;

import java.util.Date;

public class BandejaEvaluacionModel{

    public int banEvaId;    
    public int nota;
    
    public Date fecEnt;
    public Date fecVis;    
    
    public String nombres;
    public String apellido1;
    public String apellido2;
    
    public char estado;
    
    public int eva_esc_id;
    public int per_id;
    
    public String not_lit;

    public BandejaEvaluacionModel() {
    }
    public BandejaEvaluacionModel(int banEvaId) {
        this.banEvaId = banEvaId;
    }
    public BandejaEvaluacionModel(int banTarId, Date fecEnt,Date fecVis, int nota,String not_lit ,char estado,String nombres,String apellido1,String apellido2,int eva_esc_id,int per_id) {
       this.banEvaId = banTarId;
       this.fecVis = fecVis;
       this.fecEnt = fecEnt;
       this.nota = nota;
       this.not_lit = not_lit;
       this.estado = estado;
       this.nombres = nombres;
       this.apellido1 = apellido1;
       this.apellido2 = apellido2;
       this.eva_esc_id = eva_esc_id;
       this.per_id = per_id;
    }
}


