/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.directorio.parientes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.di.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.di.ParientesDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Parientes;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.TipoPariente;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ActualizarParienteTx implements ITransaction{
    
    @Override
    public WebResponse execute(WebRequest wr) {        
        /*
        *  Parte para la operacion en la Base de Datos
        */        
                
        Long parId = 0L,
            perId = 0L;
        int tpaId = 0;
        String    parDni = "";
            
        String parPat = "",
               parMat = "",
               parNom = "",
               parDir = "",
               //parDes = "",
               parTel = "";
        
        
        JSONObject requestData = (JSONObject)wr.getData();
        try{
            //JSONObject requestData = (JSONObject)wr.getData();
            parId = requestData.getLong("parId");
            perId = requestData.getLong("perId");
            tpaId = requestData.getInt("tpaId");
            parMat = requestData.optString("parMat");
            parPat = requestData.optString("parPat");
            parNom = requestData.optString("parNom");
            parDir = requestData.optString("parDir");
            parTel = requestData.optString("parTel");
            parDni = requestData.optString("parDni");
            //parDes = requestData.getString("parDes");            
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo actualizar", e.getMessage() );
        }
        
        //Fin        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        PersonaDao peDao = (PersonaDao)FactoryDao.buildDao("di.PersonaDao");        
        Persona p = peDao.buscarPersonaxId(parId);//(Persona)session.load(Persona.class, new Long(parId));//new Parientes(3);
                
        p.setApeMat(parMat);
        p.setApePat(parPat);
        p.setNom(parNom);
        p.setPerDir(parDir);
        p.setFij(parTel);
        p.setDni(parDni);
        
        ParientesDao paDao = (ParientesDao)FactoryDao.buildDao("di.ParientesDao");        
        Parientes pa = new Parientes(parId, perId, new TipoPariente(tpaId));
        
        pa.setPariente(p);
        
        try {                                    
            paDao.update(pa);                     
        }catch (Exception e) {        
            return WebResponse.crearWebResponseError("No se pudo actualizar el pariente",e.getMessage());
        }
        
        //Fin        
        /*
        *  Repuesta Correcta
        */        
        JSONObject oResponse = new JSONObject();            
        oResponse.put("est",true);
        return WebResponse.crearWebResponseExito("Se actualizo el Pariente correctamente", oResponse);
        
        //Fin
                
    }
    
}
