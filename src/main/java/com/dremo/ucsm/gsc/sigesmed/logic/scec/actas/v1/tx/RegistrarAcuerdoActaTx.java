package com.dremo.ucsm.gsc.sigesmed.logic.scec.actas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.ActasReunionComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.AcuerdosActaComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.ActasReunionComision;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.AcuerdosActaComision;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 05/10/2016.
 */
public class RegistrarAcuerdoActaTx implements ITransaction {
    private static final Logger logger = Logger.getLogger(RegistrarAcuerdoActaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        String meta = wr.getMetadataValue("cod");
        JSONObject data = (JSONObject) wr.getData();
        JSONArray delArray =  data.getJSONArray("del");
        JSONArray nueAray = data.getJSONArray("arr");
        return registrarAcuerdos(nueAray, delArray,Integer.parseInt(meta));
    }

    private WebResponse registrarAcuerdos(JSONArray data,JSONArray del, int idAct) {

        try{

            AcuerdosActaComisionDao acuerdoDao = (AcuerdosActaComisionDao) FactoryDao.buildDao("scec.AcuerdosActaComisionDao");
            for(int i = 0; i < del.length(); i++){
                int id = del.getInt(i);
                acuerdoDao.deleteAbsolute(acuerdoDao.buscarAcuerdosPorId(id));
            }

            ActasReunionComisionDao actaDao = (ActasReunionComisionDao) FactoryDao.buildDao("scec.ActasReunionComisionDao");
            ActasReunionComision acta = actaDao.buscarConAcuerdos(idAct);
            acta.getAcuerdos().clear();
            for(int i = 0; i < data.length(); i++){
                JSONObject jsonAcuerdo = data.getJSONObject(i);
                int id = jsonAcuerdo.optInt("cod",-1);
                String des = jsonAcuerdo.optString("des","");
                Date fec = jsonAcuerdo.optLong("dat") == 0? null : new Date(jsonAcuerdo.getLong("dat"));
                Boolean est = jsonAcuerdo.optBoolean("est",false);
                ///
                int votFav = jsonAcuerdo.optInt("afav");
                int votCon = jsonAcuerdo.optInt("enco");
                String obs = jsonAcuerdo.optString("obs");
                if( id != -1){
                    //el elemento ya esta registrado , asi que procedermos a editar
                    AcuerdosActaComision acuerdo = new AcuerdosActaComision(id,des,fec,est,votFav,votCon,obs,acta);
                    acta.getAcuerdos().add(acuerdo);

                }else{
                    // el elemento no esta regitrado a si que lo vamos a crear
                    AcuerdosActaComision acuerdo = new AcuerdosActaComision(des,fec,est,votFav,votCon,obs);
                    acuerdo.setActaReunion(acta);
                    acta.getAcuerdos().add(acuerdo);
                }
                actaDao.registrarAcuerdos(acta);

            }
            return WebResponse.crearWebResponseExito("Se realizo la operacion",WebResponse.OK_RESPONSE);
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarAcuerdos",e);
            return WebResponse.crearWebResponseError("No se pudieron registrar los acuerdos",WebResponse.BAD_RESPONSE);
        }

    }
}