package com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion;

import java.io.Serializable;
import org.hibernate.annotations.DynamicUpdate;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "curso_capacitacion", schema = "pedagogico")
@DynamicUpdate(value = true)
public class CursoCapacitacion implements Serializable {
    @Id
    @Column(name="cur_cap_id", unique = true, nullable = false)
    @SequenceGenerator(name = "secuencia_curso_capacitacion", sequenceName="pedagogico.curso_capacitacion_cur_cap_id_seq" )
    @GeneratedValue(generator="secuencia_curso_capacitacion")
    private int curCapId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "org_id_org")
    private Organizacion organizacion;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "org_id_aut")
    private Organizacion organizacionAut;

    @Column(name = "nom",nullable = false,length = 40)
    private String nom;

    @Column(name = "tip",nullable = false,length = 20)
    private String tip;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_cre",nullable = false)
    private Date fecCre;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_ini")
    private Date fecIni;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_fin")
    private Date fecFin;

    @Column(name = "est",nullable = false,length = 1)
    private Character est;

    @Column(name = "num_par")
    private Integer numPar;

    @Column(name = "usu_mod")
    private Integer usuMod;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length=29)
    private Date fecMod;

    @Column(name = "est_reg",length = 1)
    private Character estReg;

    @OneToMany(mappedBy = "cursoCapacitacion", cascade = CascadeType.PERSIST)
    List<SedeCapacitacion> sedesCapacitacion = new ArrayList<>();
    
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "objetivos_curso_capacitacion", schema = "pedagogico",
            joinColumns = @JoinColumn(name = "cur_cap_id"), 
            inverseJoinColumns = @JoinColumn(name = "obj_cap_id")
    )
    private Set<ObjetivoCapacitacion> objetivosCapacitacion = new HashSet<>(0);
    
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "capacitacion_perfiles_capacitador", schema = "pedagogico",
            joinColumns = @JoinColumn(name = "cur_cap_id"), 
            inverseJoinColumns = @JoinColumn(name = "req_cap_id")
    )
    private Set<PerfilCapacitador> perfilesCapacitacion = new HashSet<>(0);
    
    public CursoCapacitacion() {}
    
    public CursoCapacitacion(Organizacion organizacion, Organizacion organizacionAut, String nom, String tip, Integer usuMod) {
        this.organizacion = organizacion;
        this.organizacionAut = organizacionAut;
        this.nom = nom;
        this.tip = tip;
        this.fecCre = new Date();
        this.est = 'A';      
        this.usuMod = usuMod;
        this.estReg = 'A';
        this.fecMod = new Date();
    }

    public int getCurCapId() {
        return curCapId;
    }

    public void setCurCapId(int curCapId) {
        this.curCapId = curCapId;
    }

    public Organizacion getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(Organizacion organizacion) {
        this.organizacion = organizacion;
    }

    public Organizacion getOrganizacionAut() {
        return organizacionAut;
    }

    public void setOrganizacionAut(Organizacion organizacionAut) {
        this.organizacionAut = organizacionAut;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public Date getFecCre() {
        return fecCre;
    }

    public void setFecCre(Date fecCre) {
        this.fecCre = fecCre;
    }

    public Date getFecIni() {
        return fecIni;
    }

    public void setFecIni(Date fecIni) {
        this.fecIni = fecIni;
    }

    public Date getFecFin() {
        return fecFin;
    }

    public void setFecFin(Date fecFin) {
        this.fecFin = fecFin;
    }

    public Character getEst() {
        return est;
    }

    public void setEst(Character est) {
        this.est = est;
    }

    public Integer getNumPar() {
        return numPar;
    }

    public void setNumPar(Integer numPar) {
        this.numPar = numPar;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public List<SedeCapacitacion> getSedesCapacitacion() {
        return sedesCapacitacion;
    }

    public void setSedesCapacitacion(List<SedeCapacitacion> sedesCapacitacion) {
        this.sedesCapacitacion = sedesCapacitacion;
    }

    public Set<ObjetivoCapacitacion> getObjetivosCapacitacion() {
        return objetivosCapacitacion;
    }

    public void setObjetivosCapacitacion(Set<ObjetivoCapacitacion> objetivosCapacitacion) {
        this.objetivosCapacitacion = objetivosCapacitacion;
    }

    public Set<PerfilCapacitador> getPerfilesCapacitacion() {
        return perfilesCapacitacion;
    }

    public void setPerfilesCapacitacion(Set<PerfilCapacitador> perfilesCapacitacion) {
        this.perfilesCapacitacion = perfilesCapacitacion;
    }

    @Override
    public String toString() {
        return "CursoCapacitacion{" + "curCapId=" + curCapId + ", nom=" + nom + ", tip=" + tip + "}";
    }
    
    
}
