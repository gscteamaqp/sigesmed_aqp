/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.asistencia.v1.tx;


import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.AsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.HorarioDet;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.Inasistencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.RegistroAsistencia;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.DateUtil;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author carlos
 */
public class ListarControlAsistenchaPrevTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        Integer organizacoinId, rolId;
        Organizacion _user =null;
        Date fecha;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            organizacoinId = requestData.getInt("organizacionID");  
            String fec=requestData.getString("diaActual");
            fecha=DateUtil.sumarRestarHorasFecha(formatter.parse(fec), -168);
            
            _user = new Organizacion(organizacoinId);
            
            rolId = requestData.getInt("rolId"); 
            
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo verificar los datos", e.getMessage() );
        }
        
        
        Date inicioSemana = DateUtil.getInicioSemana(fecha);
        inicioSemana = DateUtil.addDays(inicioSemana, 1);
        Date finSemana=DateUtil.getFinSemana(fecha);
        
        
        List<Trabajador> trabajadores = new ArrayList<>();
        AsistenciaDao asistenciaDao = (AsistenciaDao)FactoryDao.buildDao("cpe.AsistenciaDao");
        try{
            trabajadores =asistenciaDao.listarAsistenciaTrabajadorByFecha(inicioSemana, finSemana, _user, rolId);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar los Trabajadores de la Institucion ", e.getMessage() );
        }

        JSONArray miArray = new JSONArray();
        
        
        for(Trabajador tra:trabajadores ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("id",tra.getTraId());
            oResponse.put("personaID",tra.getPersona().getPerId());
            oResponse.put("dni",tra.getPersona().getDni());
            oResponse.put("datos",tra.getPersona().getNombrePersona());
            
            oResponse.put("orgId", tra.getOrganizacion().getOrgId());
            oResponse.put("orgNom", tra.getOrganizacion().getNom());
            
            if(tra.getTraCar()!=null)
            {
                oResponse.put("idCargo",new Integer(tra.getTraCar().getCrgTraIde()));
                oResponse.put("cargo",tra.getTraCar().getCrgTraNom());
            }
            else
            {
                oResponse.put("idCargo","");
                oResponse.put("cargo","");
            }
            oResponse.put("l_h",false);
            oResponse.put("m_h",false);
            oResponse.put("mi_h",false);
            oResponse.put("j_h",false);
            oResponse.put("v_h",false);
            oResponse.put("s_h",false);
            oResponse.put("d_h",false);
            if(tra.getHoraCabId()!=null)
            {
                oResponse.put("idHorario",tra.getHoraCabId().getHorCabId());
                oResponse.put("horario",tra.getHoraCabId().getHorCabDes());
//                if(tra.getHoraCabId().getHorarioDetalle().size()>0)
//                {
//                    for(HorarioDet hdt:tra.getHoraCabId().getHorarioDetalle())
//                    switch(hdt.getDiaSemId().getDiaSemId())
//                    {
//                        case 'D': oResponse.put("d_h",true);
//                            break;
//                        case 'L': oResponse.put("l_h",true);
//                            break;
//                        case 'M':  oResponse.put("m_h",true);
//                            break;    
//                        case 'W': oResponse.put("mi_h",true);
//                            break;
//                        case 'J': oResponse.put("j_h",true);
//                            break;
//                        case 'V': oResponse.put("v_h",true);
//                            break;    
//                        case 'S': oResponse.put("s_h",true);
//                            break;    
//                    }
//                }
            }
            else
            {
                oResponse.put("idHorario","");
                oResponse.put("horario","");
            }
            
            oResponse.put("l","");
            oResponse.put("m","");
            oResponse.put("mi","");
            oResponse.put("j","");
            oResponse.put("v","");
            oResponse.put("s","");
            oResponse.put("d","");
            for(Inasistencia ina:tra.getInasistencias())
            {
                Calendar cin = Calendar.getInstance();
                cin.setTime(ina.getInaFecha());
                int tin = cin.get(Calendar.DAY_OF_WEEK);
                switch(tin)
                    {
                        case 1: oResponse.put("d_h",true);
                                if(ina.getJustificacion() != null)
                                {
                                    oResponse.put("d",5);
                                }
                            break;
                        case 2: oResponse.put("l_h",true);
                                if(ina.getJustificacion() != null)
                                {
                                    oResponse.put("l",5);
                                }
                            break;
                        case 3:  oResponse.put("m_h",true);
                                if(ina.getJustificacion() != null)
                                {
                                    oResponse.put("m",5);
                                }
                            break;    
                        case 4: oResponse.put("mi_h",true);
                                if(ina.getJustificacion() != null)
                                {
                                    oResponse.put("mi",5);
                                }
                            break;
                        case 5: oResponse.put("j_h",true);
                                if(ina.getJustificacion() != null)
                                {
                                    oResponse.put("j",5);
                                }
                            break;
                        case 6: oResponse.put("v_h",true);
                                if(ina.getJustificacion() != null)
                                {
                                    oResponse.put("v",5);
                                }
                            break;    
                        case 7: oResponse.put("s_h",true);
                                if(ina.getJustificacion() != null)
                                {
                                    oResponse.put("s",5);
                                }
                            break;    
                    }
            }
            
           
            oResponse.put("lf",formatter.format(inicioSemana));
            oResponse.put("mf",formatter.format(DateUtil.sumarRestarHorasFecha(inicioSemana,24)));
            oResponse.put("mif",formatter.format(DateUtil.sumarRestarHorasFecha(inicioSemana,48)));
            oResponse.put("jf",formatter.format(DateUtil.sumarRestarHorasFecha(inicioSemana,72)));
            oResponse.put("vf",formatter.format(DateUtil.sumarRestarHorasFecha(inicioSemana,96)));
            oResponse.put("sf",formatter.format(DateUtil.sumarRestarHorasFecha(inicioSemana,120)));
            oResponse.put("df",formatter.format(DateUtil.sumarRestarHorasFecha(inicioSemana,144)));
            for(RegistroAsistencia reg:tra.getAsistencias())
            {
                Calendar cin = Calendar.getInstance();
                if(reg.getHoraIngreso()!=null)
                {
                    Date tempIn=reg.getHoraIngreso();
                    cin.setTime(tempIn);
                }
                if(cin.getTime()==null || reg.getHoraSalida()!=null)
                {
                    Date tempOut=reg.getHoraSalida();
                    cin.setTime(tempOut);
                }

                int tin = cin.get(Calendar.DAY_OF_WEEK);
                
                switch (tin) {
                    case 1:
                        if(reg.getJustificacion() != null)
                        {
                            oResponse.put("d",4);
                        }
                        else if(reg.getEstAsi().equals("1"))
                        {
                            oResponse.put("d",1);
                        } else if(reg.getEstAsi().equals("2")){
                             oResponse.put("d",2);
                        } else{
                             oResponse.put("d",3);
                        }      
                        oResponse.put("d_",reg.getRegAsiId());
                        oResponse.put("d_h",true);
                        break;
                    case 2:
                        if(reg.getJustificacion() != null)
                        {
                            oResponse.put("l",4);
                        }
                        else if(reg.getEstAsi().equals("1"))
                        {
                            oResponse.put("l",1);
                        } else if(reg.getEstAsi().equals("2")){
                             oResponse.put("l",2);
                        } else{
                             oResponse.put("l",3);
                        }
                        oResponse.put("l_",reg.getRegAsiId());
                        oResponse.put("l_h",true);
                        break;
                    case 3:
                        if(reg.getJustificacion() != null)
                        {
                            oResponse.put("m",4);
                        }
                        else if(reg.getEstAsi().equals("1"))
                        {
                            oResponse.put("m",1);
                        } else if(reg.getEstAsi().equals("2")){
                             oResponse.put("m",2);
                        } else{
                             oResponse.put("m",3);
                        }
                        oResponse.put("m_",reg.getRegAsiId());
                        oResponse.put("m_h",true);
                        break;
                    case 4:
                        if(reg.getJustificacion() != null)
                        {
                            oResponse.put("mi",4);
                        }
                        else if(reg.getEstAsi().equals("1"))
                        {
                            oResponse.put("mi",1);
                        } else if(reg.getEstAsi().equals("2")){
                             oResponse.put("mi",2);
                        } else{
                             oResponse.put("mi",3);
                        }
                        oResponse.put("mi_",reg.getRegAsiId());
                        oResponse.put("mi_h",true);
                        break;
                    case 5:
                        if(reg.getJustificacion() != null)
                        {
                            oResponse.put("j",4);
                        }
                        else if(reg.getEstAsi().equals("1"))
                        {
                            oResponse.put("j",1);
                        } else if(reg.getEstAsi().equals("2")){
                             oResponse.put("j",2);
                        } else{
                             oResponse.put("j",3);
                        }
                        oResponse.put("j_",reg.getRegAsiId());
                        oResponse.put("j_h",true);
                        break;
                    case 6:
                        if(reg.getJustificacion() != null)
                        {
                            oResponse.put("v",4);
                        }
                        else if(reg.getEstAsi().equals("1"))
                        {
                            oResponse.put("v",1);
                        } else if(reg.getEstAsi().equals("2")){
                             oResponse.put("v",2);
                        } else{
                             oResponse.put("v",3);
                        }
                        oResponse.put("v_",reg.getRegAsiId());
                        oResponse.put("v_h",true);
                        break;
                    case 7:
                        if(reg.getJustificacion() != null)
                        {
                            oResponse.put("s",4);
                        }
                        else if(reg.getEstAsi().equals("1"))
                        {
                            oResponse.put("s",1);
                        } else if(reg.getEstAsi().equals("2")){
                             oResponse.put("s",2);
                        } else{
                             oResponse.put("s",3);
                        }
                        oResponse.put("s_",reg.getRegAsiId());
                        oResponse.put("s_h",true);
                        break;
                    
                }
            }            
            miArray.put(oResponse);
        }
        
        JSONObject oCabecera = new JSONObject();
            Calendar cal = Calendar.getInstance();
            cal.setTime(fecha);
            String currentYear=cal.get(Calendar.YEAR)+"";
            int mes=cal.get(Calendar.MONTH);
            switch(mes)
            {
                case 0:
                     oCabecera.put("cabecera",currentYear +" Enero");
                    break; 
                case 1:
                    oCabecera.put("cabecera",currentYear +" Febrero");
                    break;     
                case 2:
                    oCabecera.put("cabecera",currentYear +" Marzo");
                    break;
                case 3:
                    oCabecera.put("cabecera",currentYear +" Abril");
                    break;     
                case 4:
                    oCabecera.put("cabecera",currentYear +" Mayo");
                    break;     
                case 5:
                    oCabecera.put("cabecera",currentYear +" Junio");
                    break;     
                case 6:
                    oCabecera.put("cabecera",currentYear +" Julio");
                    break;     
                case 7:
                    oCabecera.put("cabecera",currentYear +" Agosto");
                    break;
                case 8:
                    oCabecera.put("cabecera",currentYear +" Septiembre");
                    break;
                case 9:
                    oCabecera.put("cabecera",currentYear +" Octubre");
                    break;
                case 10:
                    oCabecera.put("cabecera",currentYear +" Noviembre");
                    break;     
                case 11:
                    oCabecera.put("cabecera",currentYear +" Diciembre");
                    break;     
            }
            oCabecera.put("semana", "Semana del "+inicioSemana.getDate()+" al "+finSemana.getDate());
            
            JSONObject oResult=new JSONObject();
            oResult.put("trabajadores", miArray);
            oResult.put("cabecera", oCabecera);
            oResult.put("fecha", formatter.format(fecha));
           
        return WebResponse.crearWebResponseExito("Se Listo correctamente",oResult);        
        //Fin
    }
    
}

