package com.dremo.ucsm.gsc.sigesmed.core.dao.maestro;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Seccion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Grado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Nivel;

import java.util.List;

/**
 * Created by Administrador on 13/10/2016.
 */
public interface DocenteDao extends GenericDao<Docente> {
    Trabajador buscarTrabajadorPorUsuario(int codUsr);
    Docente buscarDocentePorUsuario(int codUsr);
    Docente buscarDocentePorId(int idDocente);
    List<Docente> buscarDocentesIE(int idIE);
    Usuario buscarUsuarioPorId(int idUsuario);
    Nivel buscarNivelPorID(int idNivel);
    Grado buscarGradoPorID(int idGrado);
    Seccion buscarSeccionPorID(char idSeccion);
    AreaCurricular buscarAreaPorId(int id);
    List<AreaCurricular> listarAreasDocente(int idOrg,int idDoc,int idGra, char idSecc );
    List<AreaCurricular> listarAreasDocente(int idOrg,int idDoc,int idGra);
}
