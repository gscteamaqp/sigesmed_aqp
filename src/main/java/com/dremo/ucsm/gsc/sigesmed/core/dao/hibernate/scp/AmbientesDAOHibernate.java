/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scp;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.Ambientes;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.AmbientesDAO;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import java.util.Date;
import javax.persistence.TemporalType;
import org.hibernate.SQLQuery;

/**
 *
 * @author Administrador
 */
public class AmbientesDAOHibernate extends GenericDaoHibernate<Ambientes> implements AmbientesDAO {

    @Override
    public List<Ambientes> listarAmbientes(int org_id) {
           
        List<Ambientes> ambientes = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT amb FROM Ambientes amb WHERE amb.est_reg!='E' and amb.org_id=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1",org_id);
            ambientes = query.list();
            
        }
        catch(Exception e){
         System.out.println("No se pudo Mostrar la lista de Ambientse \\n "+ e.getMessage());
             throw new UnsupportedOperationException("No se pudo Mostrar la lista de Ambientes \\n "+ e.getMessage());
        }
        finally{
            session.close();
        }
        return ambientes;  
        
    }

    @Override
    public void eliminarAmbiente(int amb_id) {
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        
        try{
            String hql = "UPDATE Ambientes amb SET amb.est_reg='E' WHERE amb.amb_id =:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", amb_id);
            query.executeUpdate();  
            miTx.commit();
        }
        
        catch(Exception e){
            System.out.println("No se pudo eliminar el ambiente \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo eliminar el ambiente \\n "+ e.getMessage());
        }
        finally{
            session.close();
        }
    }

    @Override
    public List<Ambientes> buscarAmbientesDisponibles(int orgId, Date horaInicio, Date horaFin, String diaSem) {
           
        List<Ambientes> ambientes = null;
        
        char diaSemId = diaSem.charAt(0); 
        
        System.out.println(diaSemId);
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        
        try{

            String hql = "SELECT amb FROM Ambientes amb WHERE amb.org_id=:p1 AND amb.est_reg = 'A'" +
                         "AND NOT EXISTS(SELECT hd FROM HorarioDetalle hd WHERE hd.aulId = amb.amb_id"
                    + " AND ((hd.horIni >=:p2  AND hd.horIni <:p3) OR (hd.horFin >:p2 AND hd.horFin <=:p3)) AND hd.diaId=:p4)";
            Query query = session.createQuery(hql);
            query.setParameter("p1",orgId);
            query.setParameter("p2", horaInicio);
            query.setParameter("p3", horaFin);
            query.setParameter("p4", diaSemId);
            ambientes = query.list();           
                       
            miTx.commit();
        }
        
        catch(Exception e){
            System.out.println("No se pudo listar los ambientes disponibles \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los ambientes disponibles \\n "+ e.getMessage());
        }
        finally{
            session.close();
        }
        
        return ambientes;

    }

    @Override
    public Ambientes buscarAmbientePorId(int ambId) {
        
        Ambientes ambiente = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT amb FROM Ambientes amb WHERE amb.amb_id=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1",ambId);
            ambiente = (Ambientes)(query.uniqueResult());
            
        }
        catch(Exception e){
         System.out.println("No se pudo obtener el ambiente \\n "+ e.getMessage());
             throw new UnsupportedOperationException("No se pudo obtener el ambiente \\n "+ e.getMessage());
        }
        finally{
            session.close();
        }
        return ambiente;  
    }
    
    
}
