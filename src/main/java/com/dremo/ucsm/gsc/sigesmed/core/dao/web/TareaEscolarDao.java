/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.web;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaTarea;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaTareaModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.TareaDocumento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.TareaEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.SesionAprendizaje;
import java.util.List;

/**
 *
 * @author abel
 */
public interface TareaEscolarDao extends GenericDao<TareaEscolar>{
    
    public void cambiarNombreAdjunto(int tarEscID,String nombreAdjunto );    
    public void finalizarTarea(TareaEscolar tarea );    
    public void enviarTarea(TareaEscolar tarea,BandejaTarea bandeja);
    public void enviarTarea(TareaEscolar tarea,List<BandejaTarea> bandejas);
    
    public void enviarTareaResuelta(BandejaTarea bandeja);
    public void calificarTareaResuelta(BandejaTarea bandeja );
    public void tareasFueraDeTiempo(List<Integer> tareas );
    
    public List<TareaEscolar> buscarPorPlanEstudios(int planID);
    public List<TareaEscolar> buscarPorDocente(int planID,int docenteID);
    
    public List<BandejaTareaModel> buscarAlumnosQueCumplieronTarea(int tareaID);
    public List<BandejaTareaModel> buscarAlumnosQueFaltaCalificarTarea(int tareaID);
    public List<TareaDocumento> verDocumentosTarea(int bandejaTareaID);
    
    public List<BandejaTarea> buscarTareasPorAlumno(int planID,int alumnoID);//buscarTareasPorAlumnoAndArea
    public List<BandejaTarea> buscarTareasPorAlumnoAndArea(int planID,int alumnoID,int areaId);
    public TareaEscolar obtenertarea(int tar_id);
    public TareaEscolar obtenertareaWeb(int tar_web_id);
}
