package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.PreguntaEvaluacionCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.PreguntaEvaluacionCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class EditarPreguntaTx implements ITransaction {
    
    private static final Logger logger = Logger.getLogger(EditarPreguntaTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();            
            PreguntaEvaluacionCapacitacionDao preguntaEvaluacionCapacitacionDao = (PreguntaEvaluacionCapacitacionDao) FactoryDao.buildDao("capacitacion.PreguntaEvaluacionCapacitacionDao");
            PreguntaEvaluacionCapacitacion pregunta = preguntaEvaluacionCapacitacionDao.buscarPorId(data.getInt("id"));
            JSONObject question = data.getJSONObject("question");
            
            String answer = "";
            String options = "";

            switch (question.getString("tip")) {
                case "S":
                    answer = question.getJSONObject("res").getString("s");
                    break;

                case "L":
                    answer = Boolean.toString(question.getJSONObject("res").getBoolean("l"));
                    break;

                case "M":
                    JSONArray opciones = question.getJSONObject("res").getJSONArray("m");

                    for (int i = 0; i < opciones.length(); i++) {
                        JSONObject option = opciones.getJSONObject(i);
                        options = options + option.getString("nom") + "#$";
                        if (option.getBoolean("sel")) {
                            answer = answer + i + "#$";
                        }
                    }

                    break;
            }
            
            pregunta.setTip(question.getString("tip").charAt(0));
            pregunta.setRes(answer);
            pregunta.setOpc(options);
            pregunta.setUsuMod(data.getInt("usuMod"));
            pregunta.setFecMod(new Date());
            pregunta.setPun(question.getDouble("pun"));
            pregunta.setNom(question.getString("nom"));
            pregunta.setDes(question.getString("des"));
            
            preguntaEvaluacionCapacitacionDao.update(pregunta);
            
            JSONObject result = new JSONObject();
            if(pregunta.getRes().isEmpty())
                result.put("emp", true);
            else
                result.put("emp", false);
            
            return WebResponse.crearWebResponseExito("Exito al editar la pregunta de la evaluación", WebResponse.OK_RESPONSE).setData(result);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "editarPregunta", e);
            return WebResponse.crearWebResponseError("Error al editar la pregunta de la evaluación", WebResponse.BAD_RESPONSE);
        }        
    }
}
