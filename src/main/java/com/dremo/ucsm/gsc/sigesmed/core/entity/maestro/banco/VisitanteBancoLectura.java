package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.banco;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrador on 19/12/2016.
 */
@Entity
@Table(name = "visitantes_banco_lectura",schema = "pedagogico")
public class VisitanteBancoLectura implements java.io.Serializable{
    @Id
    @Column(name = "vis_ban_lec_id",unique = true,nullable = false)
    @SequenceGenerator(name = "visitantes_banco_lectura_vis_ban_lec_id_seq",sequenceName = "pedagogico.visitantes_banco_lectura_vis_ban_lec_id_seq")
    @GeneratedValue(generator = "visitantes_banco_lectura_vis_ban_lec_id_seq")
    private int visBanLecId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "lec_id")
    private Lectura lectura;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "ban_lec_id", referencedColumnName = "ban_lec_id",insertable = false, updatable = false),
            @JoinColumn(name = "per_id", referencedColumnName = "per_id",insertable = false, updatable = false)
    })
    private DetallePermisosBanco permiso;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_vis")
    private Date fecVis;

    @Column(name = "usu_mod")
    private Integer usuMod;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;
    @Column(name = "est_reg", length = 1)
    private Character estReg;

    public VisitanteBancoLectura() {
    }

    public VisitanteBancoLectura(Date fecVis, DetallePermisosBanco permiso) {
        this.fecVis = fecVis;
        this.permiso = permiso;
        this.estReg = 'A';
        this.fecMod = new Date();
    }

    public int getVisBanLecId() {
        return visBanLecId;
    }

    public void setVisBanLecId(int visBanLecId) {
        this.visBanLecId = visBanLecId;
    }

    public Lectura getLectura() {
        return lectura;
    }

    public void setLectura(Lectura lectura) {
        this.lectura = lectura;
    }

    public DetallePermisosBanco getPermiso() {
        return permiso;
    }

    public void setPermiso(DetallePermisosBanco permiso) {
        this.permiso = permiso;
    }

    public Date getFecVis() {
        return fecVis;
    }

    public void setFecVis(Date fecVis) {
        this.fecVis = fecVis;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
}
