/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ParientesDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Parientes;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Persona;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class ParientesDaoHibernate extends GenericDaoHibernate<Parientes> implements ParientesDao{

    private static final Logger logger = Logger.getLogger(ParientesDaoHibernate.class.getName());

    @Override
    public List<Parientes> listarxTrabajador(int traId){
        
        List<Parientes> parientes = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT pa FROM com.dremo.ucsm.gsc.sigesmed.core.entity.se.Trabajador as t, com.dremo.ucsm.gsc.sigesmed.core.entity.se.Parientes as pa "
                    + "join fetch pa.pariente "
                    + "join fetch pa.parentesco "
                    + "WHERE t.traId=" + traId + " AND pa.persona = t.persona" + " AND pa.estReg='A'";
            Query query = session.createQuery(hql);
            parientes = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los parientes \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo los parientes \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return parientes;
    }
    
    @Override
    public JSONArray numeroHijosxTrabajador(int orgId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            
            String queryStr = "SELECT tra.tra_id, COUNT(par.per_id) \n" +
                            "  FROM public.parientes as par, public.trabajador as tra \n" +
                            "  WHERE par.tpa_id = 3 AND tra.per_id = par.per_id AND tra.org_id =" + orgId + 
                            "  GROUP BY tra.tra_id\n" +
                            "  ORDER BY tra_id";
            
            
            JSONArray resultado = new JSONArray();
            List<Object[]> respuesta = (List<Object[]>) session.createSQLQuery(queryStr).list();
            
            for (Object[] res:respuesta){
                JSONObject object = new JSONObject();
                object.put("tra_id", Integer.parseInt(res[0].toString()));
                object.put("numHijos", Integer.parseInt(res[1].toString()));
                resultado.put(object);
            }
            return resultado;
        } catch (NumberFormatException | JSONException e){
            logger.log(Level.SEVERE,"numeroHijosxTrabajador",e);
            throw e;
        } finally {
            session.close();
        }   
    }
}
