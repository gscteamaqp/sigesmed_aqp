/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scp;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import java.util.List;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.GrupoGenericoDAO;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.CatalogoBienes;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.GrupoGenerico;
/**
 *
 * @author Administrador
 */
public class GrupoGenericoDAOHibernate extends GenericDaoHibernate<GrupoGenerico> implements GrupoGenericoDAO {

    @Override
    public List<GrupoGenerico> listarGrupos() {
        
        List<GrupoGenerico> grupos_genericos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            
            String hql = "SELECT  grupos FROM GrupoGenerico grupos ";
            Query query = session.createQuery(hql);
            grupos_genericos = query.list();
        }catch(Exception e){
              System.out.println("No se pudo Obtener Los Grupos Genericos \\n "+ e.getMessage());
             throw new UnsupportedOperationException("No se pudo Obtener Los Grupos Genericos\\n "+ e.getMessage());
        }
         finally{
            session.close();
        }
        return grupos_genericos;
    }

    @Override
    public GrupoGenerico mostrarDetalleGrupo(int gru_gem_id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
