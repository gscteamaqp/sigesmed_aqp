package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate;

import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

public class OrganizacionDaoHibernate extends GenericDaoHibernate<Organizacion> implements OrganizacionDao {

    private static final Logger logger = Logger.getLogger(OrganizacionDaoHibernate.class.getName());
    
    @Override
    public String buscarUltimoCodigo() {
        String codigo = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar ultimo codigo de tramite
            String hql = "SELECT o.cod FROM Organizacion o WHERE o.estReg!='E' ORDER BY o.cod DESC ";
            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            codigo = ((String)query.uniqueResult());
            
            //solo cuando no hay ningun registro aun
            if(codigo==null)
                codigo = "0000";
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return codigo;
    }

    @Override
    public List<Organizacion> buscarPorTipoOrganizacion(int tipoOrganizacionID) {
        List<Organizacion> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar Organizaciones
            
            String hql = "SELECT o FROM Organizacion o JOIN FETCH o.tipoOrganizacion LEFT JOIN FETCH o.organizacionPadre WHERE o.tipoOrganizacion.tipOrgID=:p1 and o.estReg!='E'" ;
            Query query = session.createQuery(hql);
         //   Query query = session.createSQLQuery(sql);
            query.setParameter("p1", tipoOrganizacionID);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las Organizaciones \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las Organizaciones \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;        
    }

    @Override
    public List<Organizacion> buscarConTipoOrganizacionYPadre() {
        List<Organizacion> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar Organizaciones
            String hql = "SELECT o FROM Organizacion o JOIN FETCH o.tipoOrganizacion to LEFT JOIN FETCH o.organizacionPadre WHERE o.estReg!='E' ORDER BY to.tipOrgId" ;
            Query query = session.createQuery(hql);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las Organizaciones \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las Organizaciones \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public Organizacion buscarConTipoOrganizacionYPadre(int orgID) {
        Organizacion objeto = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar Organizaciones
            String hql = "SELECT o FROM Organizacion o JOIN FETCH o.tipoOrganizacion to LEFT JOIN FETCH o.organizacionPadre WHERE o.estReg!='E' and o.orgId=:p1" ;
            Query query = session.createQuery(hql);
            query.setParameter("p1", orgID);
            query.setMaxResults(1);
            //buscando 
            objeto =  (Organizacion)query.uniqueResult(); 
                                                                                                                                                                                                                                                                                                    
        }catch(Exception e){
            System.out.println("No se pudo encontrar a la Organizacion\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar a la Organizacion\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objeto;
    }

    @Override
    public List<Organizacion> buscarHijosOrganizacion(int orgId) {
        List<Organizacion> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar Organizaciones
            String hql = "SELECT o FROM Organizacion o  WHERE o.estReg!='E' AND o.organizacionPadre.id = :p1 ORDER BY o.id ASC" ;
            Query query = session.createQuery(hql);
            query.setParameter("p1", orgId);
            objetos = query.list();

        }catch(Exception e){
            throw e;
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public Organizacion buscarConTipoOrganizacion(int orgID) {
        Organizacion objeto = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar Organizaciones
            String hql = "SELECT o FROM Organizacion o JOIN FETCH o.tipoOrganizacion to  WHERE o.estReg!='E' and o.orgId=:p1" ;
            Query query = session.createQuery(hql);
            query.setParameter("p1", orgID);
            query.setMaxResults(1);
            //buscando 
            objeto =  (Organizacion)query.uniqueResult(); 
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar a la Organizacion\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar a la Organizacion\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objeto;
    }
    
    public List<Integer> listarOrganizacionesCalendario(int orgCod) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            String hql = "SELECT DISTINCT o.orgId FROM Organizacion o "
                        + "WHERE o.organizacionPadre.orgId = :orgId AND o.organizacionPadre.estReg = :estReg";

            Query query = session.createQuery(hql);
            query.setParameter("orgId", orgCod);
            query.setParameter("estReg", 'A');

            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarOrganizacionesCalendario", e);
            throw e;
        } finally {
            session.close();
        }
    }
}
