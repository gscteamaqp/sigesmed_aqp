package com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "adjuntos_tema_capacitacion", schema = "pedagogico")
public class AdjuntoTemaCapacitacion implements Serializable {
    @Id
    @Column(name = "adj_tem_cap_id", unique = true, nullable = false)
    @SequenceGenerator(name = "secuencia_adjuntos_tema_capacitacion", sequenceName = "pedagogico.adjuntos_tema_capacitacion_adj_tem_cap_id_seq")
    @GeneratedValue(generator = "secuencia_adjuntos_tema_capacitacion")
    private int adjTemCapId;
    
    @Column(name = "nom",nullable = false)
    private String nom;
    
    @Column(name = "usu_mod",nullable = false)
    private int usuMod;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod",nullable = false)
    private Date fecMod;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "des_tem_cap_id")
    private DesarrolloTemaCapacitacion desarrolloTemaCapacitacion;

    
    @Column(name = "tipo",nullable = false)
    private char tipo;

    public void setTipo(char tipo) {
        this.tipo = tipo;
    }

    public char getTipo() {
        return tipo;
    }
    
    
    
    
    
    public AdjuntoTemaCapacitacion() {}

    public AdjuntoTemaCapacitacion(String nom, int usuMod, DesarrolloTemaCapacitacion desarrolloTemaCapacitacion) {
        this.nom = nom;
        this.usuMod = usuMod;
        this.desarrolloTemaCapacitacion = desarrolloTemaCapacitacion;
        this.fecMod = new Date();
    }

    public int getAdjTemCapId() {
        return adjTemCapId;
    }

    public void setAdjTemCapId(int adjTemCapId) {
        this.adjTemCapId = adjTemCapId;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(int usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public DesarrolloTemaCapacitacion getDesarrolloTemaCapacitacion() {
        return desarrolloTemaCapacitacion;
    }

    public void setDesarrolloTemaCapacitacion(DesarrolloTemaCapacitacion desarrolloTemaCapacitacion) {
        this.desarrolloTemaCapacitacion = desarrolloTemaCapacitacion;
    }
}
