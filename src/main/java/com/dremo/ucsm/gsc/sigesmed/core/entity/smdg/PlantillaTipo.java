/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.smdg;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "plantilla_tipo", schema = "institucional")
//@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "PlantillaTipo.findAll", query = "SELECT p FROM PlantillaTipo p"),
//    @NamedQuery(name = "PlantillaTipo.findByPtiId", query = "SELECT p FROM PlantillaTipo p WHERE p.ptiId = :ptiId"),
//    @NamedQuery(name = "PlantillaTipo.findByPtiNom", query = "SELECT p FROM PlantillaTipo p WHERE p.ptiNom = :ptiNom"),
//    @NamedQuery(name = "PlantillaTipo.findByPtiDes", query = "SELECT p FROM PlantillaTipo p WHERE p.ptiDes = :ptiDes"),
//    @NamedQuery(name = "PlantillaTipo.findByFecMod", query = "SELECT p FROM PlantillaTipo p WHERE p.fecMod = :fecMod"),
//    @NamedQuery(name = "PlantillaTipo.findByUsuMod", query = "SELECT p FROM PlantillaTipo p WHERE p.usuMod = :usuMod"),
//    @NamedQuery(name = "PlantillaTipo.findByEstReg", query = "SELECT p FROM PlantillaTipo p WHERE p.estReg = :estReg")})
public class PlantillaTipo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pti_id")
    private Integer ptiId;
    @Column(name = "pti_nom")
    private String ptiNom;
    @Column(name = "pti_des")
    private String ptiDes;
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "est_reg")
    private String estReg;
//    @OneToMany(mappedBy = "ptiId")
//    private Collection<PlantillaFichaInstitucional> plantillaFichaInstitucionalCollection;

    public PlantillaTipo() {
    }

    public PlantillaTipo(Integer ptiId) {
        this.ptiId = ptiId;
    }

    public Integer getPtiId() {
        return ptiId;
    }

    public void setPtiId(Integer ptiId) {
        this.ptiId = ptiId;
    }

    public String getPtiNom() {
        return ptiNom;
    }

    public void setPtiNom(String ptiNom) {
        this.ptiNom = ptiNom;
    }

    public String getPtiDes() {
        return ptiDes;
    }

    public void setPtiDes(String ptiDes) {
        this.ptiDes = ptiDes;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

//    @XmlTransient
//    public Collection<PlantillaFichaInstitucional> getPlantillaFichaInstitucionalCollection() {
//        return plantillaFichaInstitucionalCollection;
//    }
//
//    public void setPlantillaFichaInstitucionalCollection(Collection<PlantillaFichaInstitucional> plantillaFichaInstitucionalCollection) {
//        this.plantillaFichaInstitucionalCollection = plantillaFichaInstitucionalCollection;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ptiId != null ? ptiId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlantillaTipo)) {
            return false;
        }
        PlantillaTipo other = (PlantillaTipo) object;
        if ((this.ptiId == null && other.ptiId != null) || (this.ptiId != null && !this.ptiId.equals(other.ptiId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.smdg.PlantillaTipo[ ptiId=" + ptiId + " ]";
    }
    
}
