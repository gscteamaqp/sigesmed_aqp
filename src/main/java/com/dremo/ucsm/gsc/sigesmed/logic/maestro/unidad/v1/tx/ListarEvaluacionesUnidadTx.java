package com.dremo.ucsm.gsc.sigesmed.logic.maestro.unidad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.EvaluacionesUnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.EvaluacionesUnidadDidactica;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 03/01/2017.
 */
public class ListarEvaluacionesUnidadTx implements ITransaction{
    private static Logger logger = Logger.getLogger(ListarEvaluacionesUnidadTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idUnidad = data.getInt("uni");
        return listarEvaluaciones(idUnidad);
    }

    private WebResponse listarEvaluaciones(int idUnidad) {
        try{
            EvaluacionesUnidadDidacticaDao evaDao = (EvaluacionesUnidadDidacticaDao) FactoryDao.buildDao("maestro.plan.EvaluacionesUnidadDidacticaDao");
            List<EvaluacionesUnidadDidactica> evaluaciones = evaDao.listarEvaluacionesPorUnidad(idUnidad);
            JSONArray jsonEvaluaciones = new JSONArray(EntityUtil.listToJSONString(
                    new String[]{"evaUniDidId","nom_eva", "tipEva", "des", "fecEva"},
                    new String[]{"id","nom", "tip", "des", "fec"},
                    evaluaciones
            ));
            return WebResponse.crearWebResponseExito("Se listo correctamente las evaluaciones",jsonEvaluaciones);
        }catch (Exception e){
            logger.log(Level.SEVERE,"listarEvaluaciones",e);
            return WebResponse.crearWebResponseError("no se pueden listar las anecdotas");
        }
    }
}
