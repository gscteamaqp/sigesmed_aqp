package com.dremo.ucsm.gsc.sigesmed.logic.maestro.curriculabanco.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.AreaCurricularDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.CapacidadAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.CompetenciaAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.CompetenciaCapacidadDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CapacidadAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CompetenciaAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CompetenciaCapacidad;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.TipoCurriculaEnum;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 14/10/2016.
 */
public class RegistrarCompetenciaBancoTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(RegistrarCompetenciaBancoTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            JSONObject data = (JSONObject) wr.getData();
            JSONObject jsonCom = data.getJSONObject("com");
            JSONArray jsonCap = data.optJSONArray("cap");
            CompetenciaAprendizaje competencia = new CompetenciaAprendizaje(jsonCom.getString("nom").toUpperCase(),
                    TipoCurriculaEnum.valueOf(jsonCom.getString("cur")),jsonCom.optString("des"));
            List<CapacidadAprendizaje> capacidades = null;
            if(jsonCap != null){
                capacidades = new ArrayList<>();
                for(int i = 0; i < jsonCap.length(); i++){
                    JSONObject json = jsonCap.getJSONObject(i);
                    CapacidadAprendizaje capacidad = new CapacidadAprendizaje(json.getString("nom"),json.optString("des"));
                    capacidades.add(capacidad);
                }
            }
            return registrarCompetencia(competencia,capacidades,jsonCom.optInt("are",-1));
        }catch (Exception e){
            logger.log(Level.SEVERE,"execute",e);
            return WebResponse.crearWebResponseError("Error al realizar la consulta",WebResponse.BAD_RESPONSE);
        }
    }
    private WebResponse registrarCompetencia(CompetenciaAprendizaje competencia, List<CapacidadAprendizaje> capacidades, int area){
        try {
            CompetenciaAprendizajeDao competenciaDao = (CompetenciaAprendizajeDao) FactoryDao.buildDao("maestro.plan.CompetenciaAprendizajeDao");
            CapacidadAprendizajeDao capacidadDao = (CapacidadAprendizajeDao)FactoryDao.buildDao("maestro.plan.CapacidadAprendizajeDao");
            CompetenciaCapacidadDao compCapDao  = (CompetenciaCapacidadDao)FactoryDao.buildDao("maestro.plan.CompetenciaCapacidadDao");
            /*CompetenciaAprendizaje auxComp = competenciaDao.buscarCompetenciaPorNombre(competencia.getNom());
            if(auxComp != null){
                auxComp.setEstReg('A');

            }*/
            if(area != -1){
                AreaCurricularDao areaDao = (AreaCurricularDao) FactoryDao.buildDao("maestro.plan.AreaCurricularDao");
                competencia.setArea(areaDao.buscarPorId(area));
            }

            competenciaDao.insert(competencia);
            if(capacidades != null){
                List<CompetenciaCapacidad> compCaps = new ArrayList<>();
                for(CapacidadAprendizaje capacidad : capacidades){
                    capacidadDao.insert(capacidad);

                    CompetenciaCapacidad compCap = new CompetenciaCapacidad(competencia,capacidad);
                    compCapDao.insert(compCap);

                    compCaps.add(compCap);
                }
                competencia.setCapacidades(compCaps);
            }
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("cur",competencia.getDcn().getNombre());
            jsonObj.put("cod",competencia.getComId());
            jsonObj.put("cap",competencia.getCapacidades().size());
            return WebResponse.crearWebResponseExito("Exito al crear la consulta", WebResponse.OK_RESPONSE).setData(jsonObj);
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarCompetencia",e);
            return WebResponse.crearWebResponseError("Error al realizar la consulta",WebResponse.BAD_RESPONSE);
        }
    }
}
