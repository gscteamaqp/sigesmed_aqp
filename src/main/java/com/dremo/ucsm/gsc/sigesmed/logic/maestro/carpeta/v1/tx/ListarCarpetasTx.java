package com.dremo.ucsm.gsc.sigesmed.logic.maestro.carpeta.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.carpeta.CarpetaPedagogicaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.CarpetaPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 10/10/2016.
 */
public class ListarCarpetasTx implements ITransaction {
    private static final Logger logger = Logger.getLogger(ListarCarpetasTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        return listar();
    }
    private WebResponse listar(){
        try{
            CarpetaPedagogicaDao carpetaDao = (CarpetaPedagogicaDao) FactoryDao.buildDao("maestro.carpeta.CarpetaPedagogicaDao");
            List<CarpetaPedagogica> carpetas = carpetaDao.buscarTodos(CarpetaPedagogica.class);

            return WebResponse.crearWebResponseExito("Se listo correctamente la carpeta",new JSONArray(EntityUtil.listToJSONString(
                    new String[]{"carDigId","eta","des","fecCre","act"},
                    new String[]{"cod","eta","des","cre","act"},
                    carpetas
            )));
        }catch (Exception e){
            logger.log(Level.SEVERE,"Error al listar las carpetas",e);
            return WebResponse.crearWebResponseError("Error al listar los datos");
        }
    }
}
