/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.estudio_postgrado.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.EstudioPostgradoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.EstudioPostgrado;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Yemi
 */
public class ActualizarEstudioPostgradoTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ActualizarEstudioPostgradoTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try{
     
            DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
            
            JSONObject requestData = (JSONObject)wr.getData();
        
            Integer estPosId = requestData.getInt("estPosId");
            Character tip = requestData.getString("tip").charAt(0);
            String numRes = requestData.getString("numRes");
            Date fecRes = sdi.parse(requestData.getString("fecRes").substring(0, 10));
            Date fecIniEst = sdi.parse(requestData.getString("fecIniEst").substring(0, 10));
            Date fecTerEst = sdi.parse(requestData.getString("fecTerEst").substring(0, 10));
            String ins = requestData.getString("ins");       
            
            return actualizarEstPos(estPosId, tip, numRes, fecRes, fecIniEst, fecTerEst, ins);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar estudio postgrado",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    
    private WebResponse actualizarEstPos(Integer estPosId, Character tip, String numRes, Date fecRes, Date fecIniEst, Date fecTerEst, String ins) {
        try{
            EstudioPostgradoDao estPosDao = (EstudioPostgradoDao)FactoryDao.buildDao("se.EstudioPostgradoDao");        
            EstudioPostgrado estPos = estPosDao.buscarPorId(estPosId);

            estPos.setTip(tip);
            estPos.setNumRes(numRes);
            estPos.setFecRes(fecRes);
            estPos.setFecIniEst(fecIniEst);
            estPos.setFecTerEst(fecTerEst);
            estPos.setIns(ins);
            
            estPosDao.update(estPos);

            JSONObject oResponse = new JSONObject();
            DateFormat sdo = new SimpleDateFormat("yyyy-MM-dd");
            oResponse.put("estPosId", estPos.getEstPosId());
            oResponse.put("tip", estPos.getTip());
            oResponse.put("tipDes", "");
            oResponse.put("numRes", estPos.getNumRes());
            oResponse.put("fecRes", sdo.format(estPos.getFecRes()));
            oResponse.put("fecIniEst", sdo.format(estPos.getFecIniEst()));
            oResponse.put("fecTerEst", sdo.format(estPos.getFecTerEst()));
            oResponse.put("ins", estPos.getIns());
            return WebResponse.crearWebResponseExito("Estudio postgrado actualizado exitosamente",oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizarEstudioPostgrado",e);
            return WebResponse.crearWebResponseError("Error, el estudio postgrado no fue actualizado");
        }
    } 
    
}
