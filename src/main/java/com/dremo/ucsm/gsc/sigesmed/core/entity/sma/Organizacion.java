/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma;

import com.dremo.ucsm.gsc.sigesmed.core.entity.TipoOrganizacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author gscadmin
 */
@Entity(name = "com.dremo.ucsm.gsm.sigesmed.core.entity.sma.Organizacion")
@Table(name="organizacion")
public class Organizacion  implements java.io.Serializable {
    @Id
    @Column(name="org_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_organizacion", sequenceName="organizacion_org_id_seq" )
    @GeneratedValue(generator="secuencia_organizacion")
    private int orgId;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="tip_org_id", nullable=false)
    private TipoOrganizacion tipoOrganizacion;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="org_pad_id")
    private Organizacion organizacionPadre;
    
    @Column(name="cod", nullable=false, length=16)
    private String cod;
    
    @Column(name="nom", nullable=false, length=64)
    private String nom;
    
    @Column(name="ali", nullable=false, length=64)
    private String ali;
    
    @Column(name="des", length=256)
    private String des;
    
    @Column(name="niv_edu_des")
    private String nivDes;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_mod", length=29)
    private Date fecMod;
    
    @Column(name="usu_mod")
    private Integer usuMod;
    
    @Column(name="est_reg", length=1)
    private Character estReg;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "niv_id", referencedColumnName = "niv_id")    
    private Nivel nivel;
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy="organizacionPadre")
    private List<Organizacion> organizaciones;
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy="organizacion")
    private List<Monitoreo> monitoreos;
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy="institucionEducativa")
    private List<FichaEvaluacion> fichasEvaluacion;

    //fin
    
    public Organizacion() {
    }
    
    public Organizacion(int orgId) {
        this.orgId = orgId;
    }
    
    public Organizacion(int orgId, Nivel n) {
        this.orgId = orgId;
        this.nivel = n;
    }

    public int getOrgId() {
        return this.orgId;
    }
    
    public void setOrgId(int orgId) {
        this.orgId = orgId;
    }

    public TipoOrganizacion getTipoOrganizacion() {
        return this.tipoOrganizacion;
    }    
    public void setTipoOrganizacion(TipoOrganizacion tipoOrganizacion) {
        this.tipoOrganizacion = tipoOrganizacion;
    }

    public Organizacion getOrganizacionPadre() {
        return this.organizacionPadre;
    }    
    public void setOrganizacionPadre(Organizacion organizacionPadre) {
        this.organizacionPadre = organizacionPadre;
    }
    
    public String getCod() {
        return this.cod;
    }
    
    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getNom() {
        return this.nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    public String getAli() {
        return this.ali;
    }
    public void setAli(String ali) {
        this.ali = ali;
    }
    
    public String getDes() {
        return this.des;
    }
    public void setDes(String des) {
        this.des = des;
    }

    public String getNivDes() {
        return nivDes;
    }

    public void setNivDes(String nivDes) {
        this.nivDes = nivDes;
    }
    
    public Date getFecMod() {
        return this.fecMod;
    }
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return this.usuMod;
    }
    
    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    
    public Character getEstReg() {
        return this.estReg;
    }
    
    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
    
    public Nivel getNivel() {
        return this.nivel;
    }
    
    public void setNivel(Nivel nivel) {
        this.nivel = nivel;
    }

    public List<Organizacion> getOrganizaciones() {
        return this.organizaciones;
    }
    public void setOrganizaciones(List<Organizacion> organizaciones) {
        this.organizaciones = organizaciones;
    }

    @Override
    public String toString() {
        return "Organizacion{" + "orgId=" + orgId + ", cod=" + cod + ", nom=" + nom + ", des=" + des + ", fecMod=" + fecMod + '}';
    }

    
}
