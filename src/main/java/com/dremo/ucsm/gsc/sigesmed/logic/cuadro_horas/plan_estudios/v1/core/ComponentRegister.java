/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.plan_estudios.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.plan_estudios.v1.tx.BuscarPlanEstudiosDesdeTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.plan_estudios.v1.tx.BuscarPlanEstudiosPorOrganizacionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.plan_estudios.v1.tx.BuscarVigentePorOrganizacionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.plan_estudios.v1.tx.EliminarPlanNivelTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.plan_estudios.v1.tx.EliminarTurnoPeriodoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.plan_estudios.v1.tx.InsertarPlanEstudiosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.plan_estudios.v1.tx.ListarMetasAtencionPorOrganizacionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.plan_estudios.v1.tx.ListarPeriodosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.plan_estudios.v1.tx.ListarTurnosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.plan_estudios.v1.tx.PersistenciaHoraDisponibilidadTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.plan_estudios.v1.tx.PersistenciaMetaAtencionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.plan_estudios.v1.tx.PersistenciaPeriodoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.plan_estudios.v1.tx.PersistenciaPlanNivelTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.plan_estudios.v1.tx.PersistenciaTurnoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.plan_estudios.v1.tx.ReporteTx;

/**
 *
 * @author abel
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_ELABORACION_CUADRO_HORAS);        
        
        //Registrando el Nombre del componente
        component.setName("planEstudios");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionPOST("insertarPlanEstudios", InsertarPlanEstudiosTx.class);
        component.addTransactionPOST("persistenciaTurno", PersistenciaTurnoTx.class);
        component.addTransactionPOST("persistenciaPeriodo", PersistenciaPeriodoTx.class);
        component.addTransactionPOST("persistenciaPlanNivel", PersistenciaPlanNivelTx.class);
        component.addTransactionPOST("persistenciaMetaAtencion", PersistenciaMetaAtencionTx.class);
        component.addTransactionPOST("persistenciaHoraDisponibilidad", PersistenciaHoraDisponibilidadTx.class);
        
        component.addTransactionGET("buscarPlanEstudios", BuscarPlanEstudiosPorOrganizacionTx.class);
        component.addTransactionGET("buscarVigente", BuscarVigentePorOrganizacionTx.class);
        component.addTransactionGET("buscarPlanDesde", BuscarPlanEstudiosDesdeTx.class);
        
        component.addTransactionGET("listarTurnos", ListarTurnosTx.class);
        component.addTransactionGET("listarPeriodos", ListarPeriodosTx.class);
        component.addTransactionGET("listarMetasAtencion", ListarMetasAtencionPorOrganizacionTx.class);
        
        component.addTransactionDELETE("eliminarTurnoPeriodo", EliminarTurnoPeriodoTx.class);
        component.addTransactionDELETE("eliminarPlanNivel", EliminarPlanNivelTx.class);
        
        component.addTransactionPOST("reporte", ReporteTx.class);
        
        return component;
    }
}
