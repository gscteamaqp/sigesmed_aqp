/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.padre_familia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mpf.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.GradoIeEstudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.Matricula;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.RegistroAuxiliarCompetencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.RegistroAuxiliarCompetenciaModel;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
/**
 *
 * @author carlos
 */
public class ListarNotasTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        int orgId = 0;   
        Long matriculaId;
        
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            
            orgId = requestData.getInt("organizacionID");
            matriculaId = requestData.getLong("matriculaID");  
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo verificar los datos", e.getMessage() );
        }
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        EstudianteDao estudianteDao = (EstudianteDao)FactoryDao.buildDao("mpf.EstudianteDao");
        
        GradoIeEstudiante gradoIee=null;
        List<RegistroAuxiliarCompetencia> registroNotas=null;
       
        
        try{
            gradoIee=estudianteDao.getGradoIeeActual(new Matricula(matriculaId));
            registroNotas=estudianteDao.listarNotasByCompetencias(gradoIee);

            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo buscar las Notas", e.getMessage() );
        }
        
        JSONArray aAre = new JSONArray();
        
        try{
            if(registroNotas.size()==0)
            {
                 return WebResponse.crearWebResponseError("No existe Notas" );
            }    
            
            
           
            List<RegistroAuxiliarCompetenciaModel> report=new ArrayList<>();
            
            String[] notas=new String []{"0","0","0","0"};//4 periodos de plan de estudios
            
            RegistroAuxiliarCompetenciaModel nuevo=new RegistroAuxiliarCompetenciaModel(registroNotas.get(0).getComp().getNom(),notas, registroNotas.get(0).getAreaCurricular().getNom(),registroNotas.get(0).getComp().getComId(),registroNotas.get(0).getAreaCurricular().getAreCurId());
            report.add(nuevo);
            for (RegistroAuxiliarCompetencia a : registroNotas) {
                int competencia = a.getComp().getComId();
                boolean flag=true;
                for (RegistroAuxiliarCompetenciaModel b : report) {
                    if (competencia==b.getCompetenciaId()) {
                        b.setNota(a.getNota(), a.getPeriodoPlanEstudios().getEta()-1);
                        flag=false;
                    }
                    else if(flag && report.get(report.size()-1)==b)
                    {
                        RegistroAuxiliarCompetenciaModel nuevo_=new RegistroAuxiliarCompetenciaModel();//(a.getComp().getNom(), a.getNota(), a.getPeriodoPlanEstudios().getEta(), a.getAreaCurricular().getNom());
                        nuevo_.setArea(a.getAreaCurricular().getNom());
                        nuevo_.setCompetencia(a.getComp().getNom());
                        nuevo_.setNota(new String []{"0","0","0","0"});
                        nuevo_.setNota(a.getNota(), a.getPeriodoPlanEstudios().getEta()-1);
                        nuevo_.setAreaId(a.getAreaCurricular().getAreCurId());
                        nuevo_.setCompetenciaId(a.getComp().getComId());
                        report.add(nuevo_);
                        
                        break;
                    }
                }
                
                    
            }
                
             int i=1;
            for(RegistroAuxiliarCompetenciaModel a : report){
                
                
                
                JSONObject oAre = new JSONObject();
                oAre.put("i",i++);
                oAre.put("area", a.getArea());
                oAre.put("logro", a.getCompetencia());
                
                oAre.put("t1", a.getNota(0) );
                oAre.put("t2", a.getNota(1) );
                oAre.put("t3", a.getNota(2) );
                oAre.put("t4", a.getNota(3) );
                
                
               
                aAre.put(oAre);
            }            
        }catch(Exception e){
            e.printStackTrace();
            return WebResponse.crearWebResponseError("No se pudo Listar las Notas", e.getMessage() );
        }    
           

        return WebResponse.crearWebResponseExito("Se listo correctamente",aAre);
        
    }    
    
    
}
