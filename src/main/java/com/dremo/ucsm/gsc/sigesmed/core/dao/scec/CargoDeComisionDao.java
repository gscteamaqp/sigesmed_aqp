package com.dremo.ucsm.gsc.sigesmed.core.dao.scec;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.CargoDeComision;
import java.util.List;

/**
 * Created by geank on 28/09/16.
 */
public interface CargoDeComisionDao extends GenericDao<CargoDeComision>{
    void eliminarCargosDeComision(int idCom);
    List<CargoDeComision> buscarPorCargo (int idCargo);
    void eliminarCargoComision(int cargoAnterior);
}
