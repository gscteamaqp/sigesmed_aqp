/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.scec.comision.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.UsuarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scec.IntegranteComisionDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.CargoComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.CargoDeComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.ComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.IntegranteComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Rol;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.CargoComision;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.CargoDeComision;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.Comision;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.IntegranteComision;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class RegistrarComisionTx implements ITransaction{

    private final static Logger logger = Logger.getLogger(RegistrarComisionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        String org = wr.getMetadataValue("org");
        JSONObject jsonData = (JSONObject)wr.getData();

        JSONObject jsonComision = jsonData.getJSONObject("com");
        JSONArray jsonCargos = jsonData.getJSONArray("car");
        JSONObject jsonPresidente = jsonData.getJSONObject("pres");

        return registrarComision(jsonComision,jsonCargos,Integer.parseInt(org),jsonPresidente);
        
    }
    private WebResponse registrarComision(JSONObject jsonComision, JSONArray jsonCargos, int idOrg,JSONObject jsonPres){

        try{
            ComisionDao comisionDao = (ComisionDao) FactoryDao.buildDao("scec.ComisionDao");
            OrganizacionDao organizacionDao = (OrganizacionDao)FactoryDao.buildDao("OrganizacionDao");
            CargoDeComisionDao cargoDeComisionDao = (CargoDeComisionDao)FactoryDao.buildDao("scec.CargoDeComisionDao");
            
            //insertamos la comision

            Organizacion organizacion = organizacionDao.buscarConTipoOrganizacionYPadre(idOrg);

            Comision comision = new Comision(jsonComision.getString("nom"),jsonComision.getString("des"),new Date());
            comision.setOrganizacion(organizacion);
            comision.setFecMod(new Date());
            comision.setEstReg('A');

            comisionDao.insert(comision);

            registrarCargoDeComision(jsonCargos,comision,cargoDeComisionDao);            
            jsonComision.put("cod",comision.getComId());
            jsonComision.put("fec",new SimpleDateFormat("dd-MM-yyyy").format(comision.getFecConCo()));
            jsonComision.put("org",new JSONObject(EntityUtil.objectToJSONString(new String[]{"orgId","nom"}, new String[]{"cod","nom"},organizacion)));
            
            registrarPresidente(jsonPres, comision,idOrg);
            return WebResponse.crearWebResponseExito("Registro realizado con exito",jsonComision);
        }catch(Exception e){
            logger.log(Level.SEVERE,"registrarComision",e);
            return WebResponse.crearWebResponseError("Error al realizar el registro",WebResponse.BAD_RESPONSE);
        }
    }
    public static void registrarCargoDeComision(JSONArray jsonCargos, Comision comision,CargoDeComisionDao cargoDeComisionDao){
        CargoComisionDao cargoComisionDao = (CargoComisionDao)FactoryDao.buildDao("scec.CargoComisionDao");
        CargoComision cPre = cargoComisionDao.buscarCargoPorNombre("presidente");
        CargoComision cPar = cargoComisionDao.buscarCargoPorNombre("participante");
        
        cargoDeComisionDao.insert(new CargoDeComision(comision,cPre,cPre.getDesCar()));
        cargoDeComisionDao.insert(new CargoDeComision(comision,cPar,cPar.getDesCar()));
        
        for(int i = 0; i < jsonCargos.length(); i++){
            JSONObject jsonCargo = jsonCargos.getJSONObject(i);
            CargoComision cargoComision = cargoComisionDao.buscarPorId(jsonCargo.getInt("cod"));
            try{
                CargoDeComision cargoDeComision = new CargoDeComision(comision,cargoComision,jsonCargo.getString("des"));
                cargoDeComisionDao.insert(cargoDeComision);
            }catch (Exception e){
                logger.log(Level.SEVERE,"registrarCargoDeComision",e);
            }

        }
    }
    public static void registrarPresidente(JSONObject jsonPresidente, Comision comision, int org){
        //integranteComisionDao.insert(jsonPresidente);
            String dni = jsonPresidente.getString("dni");
      
            PersonaDao personaDao = (PersonaDao) FactoryDao.buildDao("PersonaDao");
            UsuarioDao userDao = (UsuarioDao) FactoryDao.buildDao("UsuarioDao");
            OrganizacionDao organizacionDao = (OrganizacionDao) FactoryDao.buildDao("OrganizacionDao");
            IntegranteComisionDao integranteComisionDao = (IntegranteComisionDao)FactoryDao.buildDao("scec.IntegranteComisionDao");
            ComisionDao comisionDao = (ComisionDao)FactoryDao.buildDao("scec.ComisionDao");
            CargoComisionDao cargoComisionDao = (CargoComisionDao)FactoryDao.buildDao("scec.CargoComisionDao");

            Persona persona = personaDao.buscarPorDNI(dni);
            CargoComision cargoComision = cargoComisionDao.buscarCargoPorNombre("PRESIDENTE");

            IntegranteComision integranteComision = new IntegranteComision(persona,comision,cargoComision,new Date(),null);
            integranteComision.setEstReg('A');
            integranteComision.setUsuMod(1);
            integranteComision.setFecMod(new Date());

            JSONObject jsonIntegrante = new JSONObject(EntityUtil.objectToJSONString(new String[]{"perId","dni","nom","apeMat","apePat","num1","num2","email"},new String[]{"cod","dni","nom","apem","apep","cel","fij","ema"},persona));
            jsonIntegrante.put("car",cargoComision.getNomCar());

            integranteComisionDao.insert(integranteComision);
            persona = personaDao.buscarPorOrganizacionConUsuario(dni, org);
            Usuario user = persona.getUsuario();
            Organizacion organizacion = organizacionDao.buscarConTipoOrganizacionYPadre(org);
            Rol rol = integranteComisionDao.buscarRolPorNombre("Presidente SCEC");
            user.getSessiones().add(new UsuarioSession(0, organizacion, rol, user, new Date(), new Date(), 1, 'A'));
            userDao.update(user);
    }
    
}
