package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.PreguntaEvaluacionCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.PreguntaEvaluacionCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ListarPreguntasTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(ListarPreguntasTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();

            PreguntaEvaluacionCapacitacionDao preguntaEvaluacionCapacitacionDao = (PreguntaEvaluacionCapacitacionDao) FactoryDao.buildDao("capacitacion.PreguntaEvaluacionCapacitacionDao");
            List<PreguntaEvaluacionCapacitacion> preguntas = preguntaEvaluacionCapacitacionDao.buscarPorEvaluacion(data.getInt("evaCod"));
            JSONArray array = new JSONArray();

            for (PreguntaEvaluacionCapacitacion question : preguntas) {
                JSONObject object = new JSONObject();
                object.put("id", question.getPreEvaCapId());
                object.put("tip", question.getTip());
                object.put("pun", question.getPun());
                object.put("nom", question.getNom());
                object.put("des", question.getDes());

                switch (question.getTip()) {
                    case 'S':
                        object.put("res", question.getRes());
                        break;

                    case 'L':
                        object.put("res", Boolean.valueOf(question.getRes()));
                        break;

                    case 'M':
                        JSONArray aryOpt = new JSONArray();

                        StringTokenizer options = new StringTokenizer(question.getOpc(), "#$");
                        while (options.hasMoreTokens()) {
                            JSONObject objOpt = new JSONObject();
                            objOpt.put("sel", false);
                            objOpt.put("nom", options.nextToken());
                            aryOpt.put(objOpt);
                        }

                        StringTokenizer answers = new StringTokenizer(question.getRes(), "#$");
                        while (answers.hasMoreTokens()) {
                            aryOpt.getJSONObject(Integer.valueOf(answers.nextToken())).put("sel", true);
                        }

                        if (question.getRes().isEmpty()) {
                            object.put("emp", true);
                        } else {
                            object.put("emp", false);
                        }

                        object.put("res", aryOpt);
                        break;
                }

                array.put(object);
            }

            return WebResponse.crearWebResponseExito("Se listo correctamente", WebResponse.OK_RESPONSE).setData(array);
        } catch (JSONException | NumberFormatException e) {
            logger.log(Level.SEVERE, "listarPreguntas", e);
            return WebResponse.crearWebResponseError("Error al listar las preguntas", WebResponse.BAD_RESPONSE);
        }
    }
}
