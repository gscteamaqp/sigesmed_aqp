/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.UsuarioDaoHibernate;

import com.dremo.ucsm.gsc.sigesmed.core.dao.std.ExpedienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.DocumentoExpediente;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.Expediente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.HistorialExpediente;
import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildCodigo;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import com.dremo.ucsm.gsc.sigesmed.util.FirmaDigital;
import java.io.File;
/**
 *
 * @author abel
 */
public class InsertarExpedienteTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
            
        
        /*-------@-----------------*/
        Calendar cal= Calendar.getInstance(); 
        int year= cal.get(Calendar.YEAR); 
        String anio = "";
        anio= Integer.toString(year)+"-";
        /*-------@-----------------*/
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        ExpedienteDao expedienteDao = (ExpedienteDao)FactoryDao.buildDao("std.ExpedienteDao");
        
       UsuarioDaoHibernate userdao = new UsuarioDaoHibernate(); 
       
       
               
        Expediente expediente = null;
        List<FileJsonObject> listaArchivos = new ArrayList<FileJsonObject>();
        Usuario user;
       
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            
            int responsableId = requestData.getInt("responsableID");
            
             user = userdao.buscarPorId(responsableId);
           
          //  System.out.println("nombre certificado -->>>>"+user.getNomCert());
            //System.out.println("PASSWORD certificado -->>>>"+user.getPasCert());
            
            boolean modo = requestData.getBoolean("modo");            
            int folios = requestData.getInt("folios");
            String asunto = requestData.getString("asunto");
            
            int tipoTramiteId = requestData.getInt("tipoTramiteID");
            int prioridadId = requestData.getInt("prioridadID");
            int organizacionId = requestData.getInt("organizacionID");
            //datos para el primer historial
            int areaID = requestData.getInt("areaInicialID");
            //String observacion = requestData.optString("observacion");
            JSONArray listaDocumentos = requestData.getJSONArray("documentos");
            
            Date fechaInicio = new Date();
            
            String cod="";
            cod = BuildCodigo.cerosIzquierda(Integer.parseInt(expedienteDao.buscarUltimoCodigo())+1,3);
            cod=anio+cod;
            
            if(requestData.isNull("personaID"))
            {
                int empresaId = requestData.getInt("empresaID");
                expediente = new Expediente(0, cod , modo, folios, asunto, fechaInicio, wr.getIdUsuario(), 'A', tipoTramiteId,prioridadId,organizacionId,fechaInicio,empresaId);
            }
            else
            {
                int personaId = requestData.getInt("personaID");
                expediente = new Expediente(0, cod , modo, folios, asunto, fechaInicio, wr.getIdUsuario(), 'A', tipoTramiteId,prioridadId,organizacionId,personaId,fechaInicio);
            }
            
            
           

             
            //leendo los documentos           
            if(listaDocumentos.length() > 0){
                expediente.setDocumentos(new ArrayList<DocumentoExpediente>());
                int numDoc = 0;
                for(int i = 0; i < listaDocumentos.length();i++){
                    JSONObject bo =listaDocumentos.getJSONObject(i);

                    String nombreArchivo = "";
                    String documentoDescripcion = bo.getString("descripcion");
                   int tipoDocumentoId = bo.getInt("tipoDocumentoID");
                    
                    //verificamos si existe un archivo adjunto al requisito
                    JSONObject jsonArchivo = bo.optJSONObject("archivo");
                    if( jsonArchivo !=null && jsonArchivo.length() > 0 ){
                        
                        FileJsonObject miF = new FileJsonObject( jsonArchivo ,expediente.getCodigo()+"_doc_req_"+BuildCodigo.cerosIzquierda(++numDoc,2) );
                       
                        nombreArchivo = miF.getName();
                        listaArchivos.add(miF);
                    }
                    expediente.getDocumentos().add( new DocumentoExpediente(i+1, expediente,documentoDescripcion,nombreArchivo,tipoDocumentoId) );
                }
            }
            //registrando el primer historial del expediente
            expediente.setHistorial( new ArrayList<HistorialExpediente>() );
            expediente.getHistorial().add( new HistorialExpediente(1,expediente,"",EstadoExpediente.NUEVO, areaID, responsableId,fechaInicio ) );
            
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar el expediente, datos incorrectos", e.getMessage() );
        }
        //Fin
                
        /*
        *  Parte para la operacion en la Base de Datos
        */
        try{
            expedienteDao.insert(expediente);            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar el expediente ", e.getMessage() );
        }
        //Fin
        
        //si ya se registro el tipo de tramite 
        //ahora creamos los archivos que se desean subir
        for(FileJsonObject archivo : listaArchivos){
           // String clave="";
            
            BuildFile.buildFromBase64("expediente", archivo.getName(), archivo.getData());
        
        //procedomos a firmar cada uno de los documentos subido al servidor
       /*
           String rutapdf = ServicioREST.PATH_SIGESMED+ File.separator +"archivos"+File.separator+"expediente"+File.separator + archivo.getName();
        //   System.out.println(rutapdf);
           String rutacertificado = ServicioREST.PATH_SIGESMED+ File.separator +"archivos"+File.separator+"expediente"+File.separator+user.getNomCert() ;
        //   System.out.println(rutacertificado);
        //   System.out.println(user.getPasCert());
           clave = user.getPasCert();
           String pathPDFM = FirmaDigital.sign(rutapdf, rutacertificado,clave);
           FirmaDigital.limpiar(pathPDFM,rutapdf);
           FirmaDigital.reenombrar(pathPDFM,rutapdf);*/
         }
        
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("expedienteID",expediente.getExpId());
        oResponse.put("codigo",expediente.getCodigo());
        return WebResponse.crearWebResponseExito("El registro del expediente se realizo correctamente", oResponse);
        //Fin
    }
    
}
