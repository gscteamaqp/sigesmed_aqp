/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scp;

import org.hibernate.Query;
import org.hibernate.Session;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.BienesMueblesDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BienesMuebles;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import java.util.Date;

import java.util.List;
import org.hibernate.Transaction;

/**
 *
 * @author Administrador
 */
public class BienesMueblesDAOHibernate extends GenericDaoHibernate<BienesMuebles> implements BienesMueblesDAO {

    @Override
    public List<BienesMuebles> listarBienesMuebles(int org_id) {

        List<BienesMuebles> bienes_muebles = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            //  String hql = "SELECT bm FROM BienesMuebles bm JOIN FETCH bm.ambiente  WHERE bm.org_id=:p1 AND bm.est_reg!='E'";

            String hql = "SELECT bm FROM BienesMuebles  bm JOIN FETCH bm.dtm  JOIN FETCH bm.ambiente JOIN FETCH bm.anexo WHERE  bm.org_id=:p1 AND bm.est_reg!='E'";

            //     String hql = "SELECT bm FROM BienesMuebles  bm JOIN FETCH bm.dtm JOIN FETCH bm.ambiente WHERE  bm.org_id=:p1 AND bm.est_reg!='E'";
            //  String hql = "SELECT bm FROM BienesMuebles bm  JOIN FETCH bm.ambiente WHERE bm.est_reg!='E'";
            Query query = session.createQuery(hql);
            query.setParameter("p1", org_id);
            bienes_muebles = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo Mostrar la Lista de Bienes Muebles \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Mostrar la lista de Bienes Muebles \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return bienes_muebles;

        //    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<BienesMuebles> listarPorFecha(Date fec_ini, Date fec_fin, int an_id, int org_id) {

        List<BienesMuebles> bienes_muebles = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT bm FROM BienesMuebles  bm JOIN FETCH bm.dtm  JOIN FETCH bm.ambiente WHERE bm.fec_reg > :p1 AND bm.fec_reg < :p2 AND bm.an_id=:p3 AND bm.org_id=:p4 AND bm.est_reg!='E'";
            Query query = session.createQuery(hql);
            query.setParameter("p1", fec_ini);
            query.setParameter("p2", fec_fin);
            query.setParameter("p3", an_id);
            query.setParameter("p4", org_id);

            bienes_muebles = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo Mostrar la Lista de Bienes Muebles por Fecha \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Mostrar la lista de Bienes Muebles por Fecha \\n " + e.getMessage());

        } finally {
            session.close();
        }
        return bienes_muebles;

        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BienesMuebles obtener_bien(int id_bien) {

        BienesMuebles bm = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {

            String hql = "SELECT bm FROM BienesMuebles bm JOIN FETCH bm.dtm JOIN FETCH bm.ambiente JOIN FETCH bm.anexo WHERE bm.cod_bie=:p1 ";
            Query query = session.createQuery(hql);
            query.setParameter("p1", id_bien);
            bm = (BienesMuebles) (query.uniqueResult());

        } catch (Exception e) {
            System.out.println("No se pudo Mostrar la Lista de Bienes Muebles por Fecha \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Mostrar la lista de Bienes Muebles por Fecha \\n " + e.getMessage());

        } finally {
            session.close();
        }
        return bm;

    }

    @Override
    public List<BienesMuebles> listarPorAtributos(Date fec_ini, Date fec_fin, int an_id, int org_id, int amb_id, int cod_pat, String est_bie) {

        List<BienesMuebles> bienes_muebles = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT bm FROM BienesMuebles  bm JOIN FETCH bm.dtm  JOIN FETCH bm.ambiente WHERE bm.fec_reg > :p1 AND bm.fec_reg < :p2 AND bm.an_id=:p3 AND bm.org_id=:p4 AND bm.con_pat_id=:p5 AND bm.amb_id=:p6 AND bm.estado_bie=:p7 AND bm.est_reg!='E'";
            Query query = session.createQuery(hql);
            query.setParameter("p1", fec_ini);
            query.setParameter("p2", fec_fin);
            query.setParameter("p3", an_id);
            query.setParameter("p4", org_id);
            query.setParameter("p5", cod_pat);
            query.setParameter("p6", amb_id);
            query.setParameter("p7", est_bie);

            bienes_muebles = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo Mostrar la Lista de Bienes Muebles por Fecha \\n " + e.getMessage());
            //  throw new UnsupportedOperationException("No se pudo Mostrar la lista de Bienes Muebles por Fecha \\n "+ e.getMessage());
            e.printStackTrace();
        } finally {

            session.close();
        }
        return bienes_muebles;

        //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update_verificar_bien(int id_bien, String ver) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try {
            String hql = "UPDATE BienesMuebles  set verificar = :p1  WHERE cod_bie = :p2";

            Query query = session.createQuery(hql);
            query.setParameter("p1", ver);
            query.setParameter("p2", id_bien);

            query.executeUpdate();
            miTx.commit();

        } catch (Exception e) {
            miTx.rollback();
            System.out.println("No se pudo Actualizar la Verificacion del Bien Mueble  \\n " + e.getMessage());
            //  throw new UnsupportedOperationException("No se pudo Mostrar la lista de Bienes Muebles por Fecha \\n "+ e.getMessage());
            e.printStackTrace();
        }

        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void eliminar_bien(int bie_mue_id) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try {
            String hql = "UPDATE BienesMuebles  set est_reg = 'E'  WHERE cod_bie = :p1";

            Query query = session.createQuery(hql);
            query.setParameter("p1", bie_mue_id);
            query.executeUpdate();
            miTx.commit();

        } catch (Exception e) {
            miTx.rollback();
            System.out.println("No se pudo Eliminar el  Bien Mueble  \\n " + e.getMessage());
            //  throw new UnsupportedOperationException("No se pudo Mostrar la lista de Bienes Muebles por Fecha \\n "+ e.getMessage());
            e.printStackTrace();
        }

        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Long obtener_correlativo(int org_id) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Long correlativo = 0L;
        try {

            String hql = "SELECT COUNT(*) FROM BienesMuebles WHERE org_id=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", org_id);
            correlativo = (Long) query.uniqueResult();

            //correlativo = 2;
        } catch (Exception e) {
            System.out.println("No se pudo Obtener el Correlativo del Bien \\n " + e.getMessage());
            //  throw new UnsupportedOperationException("No se pudo Mostrar la lista de Bienes Muebles por Fecha \\n "+ e.getMessage());
            e.printStackTrace();
        } finally {

            session.close();
        }
        return correlativo;

    }

    @Override
    public BienesMuebles obtenerBienCodigoBarras(String cod_ba_bie) {
        
        BienesMuebles bm = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {

            String hql = "SELECT bm FROM BienesMuebles bm JOIN FETCH bm.dtm JOIN FETCH bm.ambiente JOIN FETCH bm.anexo WHERE bm.cod_ba_bie=:p1 ";
            Query query = session.createQuery(hql);
            query.setParameter("p1", cod_ba_bie);
            bm = (BienesMuebles) (query.uniqueResult());

        } catch (Exception e) {
            System.out.println("No se pudo Mostrar la Lista de Bienes Muebles por Fecha \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Mostrar la lista de Bienes Muebles por Fecha \\n " + e.getMessage());

        } finally {
            session.close();
        }
        return bm;
        
    }

    @Override
    public void updateBienPorSalida(int bienId, int orgId, String ver) {
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try {
            String hql = "UPDATE BienesMuebles  SET org_id =:p1, verificar = :p2  WHERE cod_bie = :p3";

            Query query = session.createQuery(hql);
            query.setParameter("p1", orgId);
            query.setParameter("p2", ver);
            query.setParameter("p3", bienId);

            query.executeUpdate();
            miTx.commit();

        } catch (Exception e) {
            miTx.rollback();
            System.out.println("No se pudo actualizar la verificacion del bien mueble  \\n " + e.getMessage());
            //  throw new UnsupportedOperationException("No se pudo Mostrar la lista de Bienes Muebles por Fecha \\n "+ e.getMessage());
            e.printStackTrace();
        }
    }

}
