/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.std;

import com.dremo.ucsm.gsc.sigesmed.core.dao.*;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.TipoDocumento;
/**
 *
 * @author abel
 */
public interface TipoDocumentoDao extends GenericDao<TipoDocumento>{
    
}
