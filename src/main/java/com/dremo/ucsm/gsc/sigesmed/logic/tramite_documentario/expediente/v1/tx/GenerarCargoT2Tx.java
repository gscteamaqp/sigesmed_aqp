/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
/**
 *
 * @author angel
 */
public class GenerarCargoT2Tx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        FileJsonObject miGrafico = null;
        GTabla tablaCar = null;
        JSONObject cabecera = new JSONObject();
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
                        
            cabecera.put("Area Origen", requestData.getString("areaOri"));
            
            
            
            JSONArray tabla = requestData.optJSONArray("derivados");
            
            if(tabla!=null && tabla.length() > 0){
                
                float[] cols = {0.5f,3f,1f,3f,4f,4f,1.5f};
                tablaCar = new GTabla(cols);

                String[] labels = {"N°","Fecha ","N° Expediente","Tipo Tramite","Observaciones","Area Destino","N° Folios"};
                tablaCar.build(labels);
                for(int i = 0; i < tabla.length();i++){
                    JSONObject bo =tabla.getJSONObject(i);
                    
                    String[] fila = new String [7];
                    fila[0] = i+1+"";
                    fila[1] = bo.optString("fechaEnvio");
                    fila[2] = bo.getString("codigo");
                    fila[3] = bo.getString("nombreTramite");
                    fila[4] = bo.optString("observacion");
                    fila[5] = bo.optString("area");
                    fila[6] = ""+bo.getInt("folios");
                    tablaCar.processLine(fila);
                }
                
            }
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo realizar el cargo, datos iicorrecto", e.getMessage() );
        }
        //Fin        
        
        
        /*
        *  Repuesta Correcta
        */
        
        JSONObject response = new JSONObject();
        try {
            Mitext r = new Mitext();
            
            r.agregarTitulo("Reporte de Cargo");
            r.newLine(1);
            r.agregarSubtitulos(cabecera);
            r.newLine(1);
            r.agregarTabla(tablaCar);
            
            Table table = new Table(2);
            
            Cell c = new Cell();
            c.setPadding(0);
            c.setTextAlignment(TextAlignment.CENTER);
            c.setBorder(Border.NO_BORDER);
            table.addCell(c);
            
            c = new Cell().add(new Paragraph("--------------------------------------------------"));
            c.setPadding(0);
            c.setTextAlignment(TextAlignment.CENTER);
            c.setBorder(Border.NO_BORDER);
            table.addCell(c);
            
            c = new Cell();
            c.setPadding(0);
            c.setTextAlignment(TextAlignment.CENTER);
            c.setBorder(Border.NO_BORDER);
            table.addCell(c);
            
            Cell cFir = new Cell().add(new Paragraph("FIRMA DE RECEPCION"));
            cFir.setPadding(0);
            cFir.setTextAlignment(TextAlignment.CENTER);
            cFir.setBorder(Border.NO_BORDER);
            table.addCell(cFir);
        
            r.newLine(4);
            r.agregarTabla(table);
            
            r.cerrarDocumento();
            response.append("cargo", r.encodeToBase64() );
        } catch (Exception ex) {
            Logger.getLogger(GenerarCargoTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return WebResponse.crearWebResponseExito("el reporte se realizo",response);
        //Fin
    }
    
}

