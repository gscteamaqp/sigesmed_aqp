/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scp;

import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BienesInmuebles;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.BienInmuebleDAO;
import org.hibernate.Query;
import org.hibernate.Session;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BienesMuebles;
import java.util.List;
import org.hibernate.Transaction;

/**
 *
 * @author Administrador
 */
public class BienInmuebleDAOHibernate extends GenericDaoHibernate<BienesInmuebles> implements BienInmuebleDAO {

    @Override
    public List<BienesInmuebles> listar_bienes_inmuebles(int org_id) {

        List<BienesInmuebles> bienes_inmuebles = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {

            String hql = "SELECT bm FROM BienesInmuebles  bm  JOIN FETCH bm.amb WHERE  bm.org_id=:p1 AND bm.est_reg!='E'";

            Query query = session.createQuery(hql);
            query.setParameter("p1", org_id);
            bienes_inmuebles = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo Mostrar la Lista de Bienes Muebles \\n " + e.getMessage());
            //    throw new UnsupportedOperationException("No se pudo Mostrar la lista de Bienes Muebles \\n "+ e.getMessage());
            e.printStackTrace();
        } finally {
            session.close();
        }
        return bienes_inmuebles;

        //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BienesInmuebles obtener_bien(int id_bien) {

        BienesInmuebles bm = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {

            String hql = "SELECT bm FROM BienesInmuebles bm JOIN FETCH bm.amb WHERE bm.bie_inm_id=:p1 ";
            Query query = session.createQuery(hql);
            query.setParameter("p1", id_bien);
            bm = (BienesInmuebles) (query.uniqueResult());

        } catch (Exception e) {
            System.out.println("No se pudo Obtener el Bien Inmueble \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Obtener el Bien Inmueble \\n " + e.getMessage());

        } finally {
            session.close();
        }
        return bm;

        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void eliminar_bien_inmueble(int id_bie) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try {
            String hql = "UPDATE BienesInmuebles  set est_reg = 'E'  WHERE bie_inm_id = :p1";

            Query query = session.createQuery(hql);
            query.setParameter("p1", id_bie);
            query.executeUpdate();
            miTx.commit();

        } catch (Exception e) {
            miTx.rollback();
            System.out.println("No se pudo Eliminar el  Bien Mueble  \\n " + e.getMessage());
            //  throw new UnsupportedOperationException("No se pudo Mostrar la lista de Bienes Muebles por Fecha \\n "+ e.getMessage());
            e.printStackTrace();
        }

        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<BienesInmuebles> reporteBieneInmuebles(int org_id) {

        List<BienesInmuebles> bienes_inmuebles = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {

            String hql = "SELECT bm FROM BienesInmuebles  bm  JOIN FETCH bm.amb JOIN FETCH bm.dtm WHERE  bm.org_id=:p1 AND bm.est_reg!='E'";

            Query query = session.createQuery(hql);
            query.setParameter("p1", org_id);
            bienes_inmuebles = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo Mostrar la Lista de Bienes Muebles \\n " + e.getMessage());
            //    throw new UnsupportedOperationException("No se pudo Mostrar la lista de Bienes Muebles \\n "+ e.getMessage());
            e.printStackTrace();
        } finally {
            session.close();
        }
        return bienes_inmuebles;

    }

}
