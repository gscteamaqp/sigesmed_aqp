package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.UsuarioSessionCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.UsuarioSession;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class UsuarioSessionCapacitacionDaoHibernate extends GenericDaoHibernate<UsuarioSession> implements UsuarioSessionCapacitacionDao {
    private static final Logger logger = Logger.getLogger(UsuarioSessionCapacitacionDaoHibernate.class.getName());

    @Override
    public UsuarioSession buscarPorPersona(int perId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Criteria query = session.createCriteria(UsuarioSession.class)
                    .createAlias("usuario", "usu")
                    .add(Restrictions.eq("usu.usuId", perId))
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            return (UsuarioSession) query.uniqueResult();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarPorPersona", e);
            throw e;
        } finally {
            session.close();
        }
    }
}
