/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.directorio.trabajador.v1.tx;

import ch.qos.logback.classic.util.ContextInitializer;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.di.DirectorioExternoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.di.DirectorioExternoDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.di.ParientesDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.MChart;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.dremo.ucsm.gsc.sigesmed.util.ReportXls;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Parientes;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.DirectorioExterno;
import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.itextpdf.io.source.ByteArrayOutputStream;
import com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Base64;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFHeader;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFPicture;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Header;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.AreaReference;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.ss.util.CellUtil;
import org.apache.poi.util.IOUtils;
import org.jfree.data.general.DefaultPieDataset;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ReporteDirectorioExternoTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
         */

        JSONObject requestData = (JSONObject) wr.getData();

        JSONObject objeto = requestData.getJSONObject("objeto");

        PersonaDao personaDao = (PersonaDao) FactoryDao.buildDao("PersonaDao");
        Persona director = personaDao.buscarPorCod(objeto.getInt("Director"));
        objeto.put("Director", director.getNom() + " " + director.getApePat() + " " + director.getApeMat());
        
        OrganizacionDao organizacionDao = (OrganizacionDao) FactoryDao.buildDao("OrganizacionDao");
        Organizacion org = organizacionDao.buscarConTipoOrganizacion(objeto.getInt("OrganizacionId"));
        objeto.put("Dirección", org.getDir());
        //FALTA
        //Usando org.getUbiCod() agregar a 'objeto' los atributos distrito, provincia, departamento
        //objeto.put("Distrito", org.getDir());
        //objeto.put("Provincia", org.getDir());
        //objeto.put("Departamento", org.getDir());
        objeto.remove("OrganizacionId");
        
        JSONArray campos = requestData.getJSONArray("campos");
        JSONArray titulos = requestData.getJSONArray("titulos");
        JSONArray pesos = requestData.getJSONArray("pesos");

        String _campos = campos.toString().replace("[", "").replace("]", "").replace("\"", "");
        String[] _keys = _campos.split(",");
//        for(int i=0; i < _keys.length; ++i) _keys[i] =  _keys[i].substring(2);

        String[] _titulos = titulos.toString().replace("[", "").replace("]", "").replace("\"", "").split(",");
        String[] _pesos = pesos.toString().replace("[", "").replace("]", "").replace("\"", "").split(",");
        float[] _p = new float[_pesos.length];
        for (int i = 0; i < _pesos.length; ++i) {
            _p[i] = Float.parseFloat(_pesos[i]);
        }

        List dirExt = null;
//        DirectorioExternoDao dirExtDao = (DirectorioExternoDao)FactoryDao.buildDao("di.DirectorioExternoDao");
        DirectorioExternoDaoHibernate dirExtDao = new DirectorioExternoDaoHibernate();

        try {
            dirExt = dirExtDao.ListarDirectorioExterno(_campos);

        } catch (Exception e) {
            System.out.println("No se pudo Listar el directorio externo \n" + e);
            return WebResponse.crearWebResponseError("No se pudo Listar el directorio externo ", e.getMessage());
        }

        //Creando el reporte....        
        Mitext m = null;
        try {
            m = new Mitext();
            m.newLine(2);
            m.agregarTitulo("REPORTE DE DIRECTORIO EXTERNO");
            m.newLine(2);
            m.agregarSubtitulos(objeto);
            m.newLine(2);
        } catch (Exception ex) {
            System.out.println("No se pudo crear el documento \n" + ex);
            Logger.getLogger(ReporteDirectorioExternoTx.class.getName()).log(Level.SEVERE, null, ex);
        }

        //agregar tabla
        GTabla t = new GTabla(_p);

        try {
            t.build(_titulos);
        } catch (IOException ex) {
            Logger.getLogger(ReporteDirectorioExternoTx.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (Object object : dirExt) {
            Map row = (Map) object;
            String[] _row = new String[_keys.length];
            for (int i = 0; i < _keys.length; ++i) {
                if (row.get(_keys[i]) == null) {
                    _row[i] = "";
                } else {
                    _row[i] = row.get(_keys[i]).toString();
                }
            }
            t.processLine(_row);
        }

        //fin tabla
        m.agregarTabla(t);

//        agregar grafico
//        MChart chart = new MChart();
//        
//        DefaultPieDataset dataset = new DefaultPieDataset( );
//        dataset.setValue( "IPhone 5s" , new Double( 20 ) );  
//        dataset.setValue( "SamSung Grand" , new Double( 20 ) );   
//        dataset.setValue( "MotoG" , new Double( 40 ) );    
//        dataset.setValue( "Nokia Lumia" , new Double( 10 ) );  
//        
//        try {
//            m.agregarGrafico(chart.createPieChart(dataset, "Grafica de Ejemplo"), 400, 300);
//        } catch (IOException ex) {
//            System.out.println("No se pudo agregar el grafico \n"+ex);
//            Logger.getLogger(ReporteTx.class.getName()).log(Level.SEVERE, null, ex);
//        }
        m.cerrarDocumento();

        ///////////EXCEL-BEGIN
        ReportXls myReportXls = new ReportXls();
        try {
//           
            //set header
            myReportXls.addHeader("SIGESMED MOQUEGUA");

            //set title1
            myReportXls.setTitle1("SIGESMED MOQUEGUA");

            //set title2   
            myReportXls.setTitle2("REPORTE DE DIRECTORIO EXTERNO");

            //Show image
            myReportXls.setImageTop(ServicioREST.PATH_SIGESMED + "/recursos/img/minedu.png");

            //add fecha
            myReportXls.setDate();

            //create subtitles
            myReportXls.setSubtitles(objeto);

            //add directorio
            
            myReportXls.fillData(_titulos, _keys, _pesos, dirExt);

            //close book
            myReportXls.closeReport();

        } catch (Exception e) {
            System.out.println("No se pudo generar reporte en xls \n" + e);
        }
        ///////////EXCEL-END    

        System.out.print("XLS:\n");
        System.out.print(myReportXls.encodeToBase64());
        System.out.print("PDF:\n");
        System.out.print(m.encodeToBase64());

        JSONArray miArray = new JSONArray();
        JSONObject oResponse = new JSONObject();
        oResponse.put("datareporte", m.encodeToBase64());
        oResponse.put("datareporte2", myReportXls.encodeToBase64());
        miArray.put(oResponse);

        return WebResponse.crearWebResponseExito("Se genero el reporte correctamente", miArray);

    }

}
