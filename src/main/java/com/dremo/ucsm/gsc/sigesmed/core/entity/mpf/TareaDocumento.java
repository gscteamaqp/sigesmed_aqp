package com.dremo.ucsm.gsc.sigesmed.core.entity.mpf;

import com.dremo.ucsm.gsc.sigesmed.core.entity.web.*;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@IdClass(TareaDocumentoId.class)
@Entity(name = "TareaDocumentoMpf")
@Table(name="tarea_documento_adjunto" ,schema="pedagogico")
public class TareaDocumento  implements java.io.Serializable {

    @Id 
    @Column(name="ban_doc_adj_id", unique=true, nullable=false)
    private int tarDocId;
    @Id
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ban_tar_esc_id", nullable=false)
    private BandejaTarea bandejaTarea;
    
    @Column(name="nom_doc", length=64)
    private String nomDoc;

    public TareaDocumento() {
    }

	
    public TareaDocumento(int tarDocId, BandejaTarea bandejaTarea) {
        this.tarDocId = tarDocId;
        this.bandejaTarea = bandejaTarea;
    }
    public TareaDocumento(int tarDocId, BandejaTarea bandejaTarea, String nomDoc) {
       this.tarDocId = tarDocId;
       this.bandejaTarea = bandejaTarea;
       this.nomDoc = nomDoc;
    }
   
     
    public int getTarDocId() {
        return this.tarDocId;
    }
    public void setTarDocId(int tarDocId) {
        this.tarDocId = tarDocId;
    }

    public BandejaTarea getBandejaTarea() {
        return this.bandejaTarea;
    }
    
    public void setBandejaTarea(BandejaTarea bandejaTarea) {
        this.bandejaTarea = bandejaTarea;
    }

    
    public String getNomDoc() {
        return this.nomDoc;
    }
    
    public void setNomDoc(String nomDoc) {
        this.nomDoc = nomDoc;
    }

}


