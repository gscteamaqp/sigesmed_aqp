/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.tipo_pago_documento.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.TipoDocumentoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.TipoDocumentoIdentidad;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author RE
 */
public class EliminarTipoDocumentoIdentidadTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int tipoDocumentoID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            tipoDocumentoID = requestData.getInt("tipoDocumentoID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo eliminar, datos incorrectos");
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
       TipoDocumentoDao tipoDocumentoDao = (TipoDocumentoDao)FactoryDao.buildDao("sci.TipoDocumentoDao");
        try{
            tipoDocumentoDao.delete(new TipoDocumentoIdentidad((short)tipoDocumentoID));
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar el Tipo documento\n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el  Tipo documento", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El  Tipo documento se elimino correctamente");
        //Fin
    }
}
