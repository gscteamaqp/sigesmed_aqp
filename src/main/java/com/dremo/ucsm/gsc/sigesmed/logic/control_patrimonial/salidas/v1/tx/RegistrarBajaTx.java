/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.salidas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.AltasDAO;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.Bajas;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BajasDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.MovimientoIngresos;

import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.BajasDAO;

import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.BienesMueblesDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.AltasDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.DetalleAltasDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.DetalleBajasDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.MovimientoIngresosDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BienesMuebles;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.Altas;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.AltasDetalle;


import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.text.SimpleDateFormat;
/**
 *
 * @author Administrador
 */
public class RegistrarBajaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        JSONObject requestData = (JSONObject)wr.getData();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Bajas baja = null;
        BajasDetalle baj_det = null;
        MovimientoIngresos mov_ing = null;
        BienesMuebles bm = null;
        BienesMueblesDAO bie_mue_dao = (BienesMueblesDAO)FactoryDao.buildDao("scp.BienesMueblesDAO");
        BajasDAO baj_dao = (BajasDAO)FactoryDao.buildDao("scp.BajasDAO");
        AltasDAO alt_dao = (AltasDAO)FactoryDao.buildDao("scp.AltasDAO");
        DetalleAltasDAO det_alt_dao = (DetalleAltasDAO)FactoryDao.buildDao("scp.DetalleAltasDAO");
        try{
            String verificar = requestData.getString("verificar");
            int id_bien = requestData.getInt("id_bien");
            int mov_ing_id = requestData.getInt("mov_ing_id");
            int cau_baj_id = requestData.getInt("causal_id");
            int usu_mod = requestData.getInt("usu_mod");
            int org_id = requestData.getInt("org_id");
            char est_reg = 'A';
            
            bm = new BienesMuebles();
            bm.setCod_bie(id_bien);
            bm.setVerificar("B");
            bie_mue_dao.update_verificar_bien(id_bien, "B");
            
            
            baja = new Bajas(0,mov_ing_id,cau_baj_id,usu_mod,org_id,est_reg);

            
             switch(verificar){
                case "A":
                    Altas alt = null;
                    AltasDetalle alt_det = null;    
                    int alt_id;
                    /*Eliminamos la Alta (Cabecera+Detalle)*/
                    alt_det = baj_dao.alta_baja(id_bien);
                    alt_id = alt_det.getAltas_id(); 
                    alt= new Altas();
                    alt.setAltas_id(alt_id);
                    det_alt_dao.deleteAbsolute(alt_det);
                    alt_dao.deleteAbsolute(alt);
                    
                   // return WebResponse.crearWebResponseExito("ERROR: El Bien Mueble ya ha sido dado de Alta");
                    break;
                case "B":
                    return WebResponse.crearWebResponseExito("ERROR: El Bien Mueble ya ha sido dado de Baja");
                   
             }
            /*INSERTAMOS EL BIEN DADO DE BAJA*/ 
            baj_dao.insert(baja);
            baj_det = new BajasDetalle(0,id_bien,baja.getBajas_id());
            DetalleBajasDAO det_baj_dao = (DetalleBajasDAO)FactoryDao.buildDao("scp.DetalleBajasDAO");
            det_baj_dao.insert(baj_det);
           
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar la Baja del Bien, datos incorrectos", e.getMessage() );
        }
        JSONObject oResponse = new JSONObject();
        oResponse.put("baja_id",baja.getBajas_id());
        return WebResponse.crearWebResponseExito("El registro de la Baja del Bien  se realizo correctamente",oResponse);

    }
    
    

    
}
