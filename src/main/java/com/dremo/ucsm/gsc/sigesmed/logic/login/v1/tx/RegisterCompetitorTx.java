package com.dremo.ucsm.gsc.sigesmed.logic.login.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.CursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.OrganizacionCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.ParticipanteCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.PersonaCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.SedeCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.TrabajadorCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.UsuarioCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.UsuarioSessionCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.ParticipanteCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.ParticipanteCapacitacionId;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.SedeCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.UsuarioSession;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class RegisterCompetitorTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(RegisterCompetitorTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();
            int sedCod = data.getInt("s");

            SedeCapacitacionDao sedeCapacitacionDao = (SedeCapacitacionDao) FactoryDao.buildDao("capacitacion.SedeCapacitacionDao");
            SedeCapacitacion sede = sedeCapacitacionDao.buscarSedeRegPar(sedCod);

            ParticipanteCapacitacionDao participanteCapacitacionDao = (ParticipanteCapacitacionDao) FactoryDao.buildDao("capacitacion.ParticipanteCapacitacionDao");
            JSONObject object = new JSONObject();

            if (sede.getCronograma().getFecIni().before(new Date())) {
                object.put("expiration", true);
            } else {
                switch (data.getString("t")) {
                    case "S":
                        ParticipanteCapacitacion participante = participanteCapacitacionDao.buscarPorId(data.getInt("p"), sedCod);

                        if (participante.getEstReg().equals('A')) {
                            object.put("registered", true);
                        } else {
                            participante.setEstReg('A');
                            participante.setFecMod(new Date());
                            participanteCapacitacionDao.update(participante);                            
                            object.put("updated", true);
                            int numPar = sede.getCronograma().getNumPar();
                            sede.getCronograma().setNumPar(numPar + 1);
                            sedeCapacitacionDao.update(sede);
                        }
                        break;

                    case "P":
                        DateFormat format = new SimpleDateFormat("dd-MM-yyyy");

                        JSONObject cap = new JSONObject();
                        cap.put("nom", sede.getCursoCapacitacion().getNom());
                        cap.put("tip", sede.getCursoCapacitacion().getTip());
                        cap.put("cro", format.format(sede.getCronograma().getFecIni()) + " / " + format.format(sede.getCronograma().getFecFin()));

                        if (data.getBoolean("m")) {
                            participante = participanteCapacitacionDao.buscarPorId(data.getInt("p"), sedCod);
                            if (participante.getEstReg().equals('A')) {
                                object.put("registered", true);
                            } else {
                                Persona persona = participante.getPersona();
                                JSONObject per = new JSONObject(EntityUtil.objectToJSONString(
                                        new String[]{"apePat", "apeMat", "nom"}, new String[]{"pat", "mat", "nom"},
                                        persona
                                ));

                                cap.put("per", per);
                            }
                        } else {
                            CursoCapacitacionDao capacitacionDao = (CursoCapacitacionDao) FactoryDao.buildDao("capacitacion.CursoCapacitacionDao");
                            List<Organizacion> organizaciones = capacitacionDao.listarOrganizaciones();
                            JSONArray org = new JSONArray(EntityUtil.listToJSONString(
                                    new String[]{"orgId", "ali"}, new String[]{"cod", "nom"},
                                    organizaciones
                            ));

                            cap.put("org", org);
                        }

                        object.accumulate("cap", cap);

                        break;
                    
                    case "B":
                        PersonaCapacitacionDao personaCapacitacionDao = (PersonaCapacitacionDao) FactoryDao.buildDao("capacitacion.PersonaCapacitacionDao");
                        Persona persona = personaCapacitacionDao.buscarPorDni(data.getString("d"));
                        object.put("available", (persona == null));
                        break;
                        
                    case "A":
                        participante = participanteCapacitacionDao.buscarPorId(data.getInt("p"), sedCod);
                        if (participante.getEstReg().equals('A')) {
                            object.put("registered", true);
                        } else {
                            participante.setEstReg('A');
                            participante.setFecMod(new Date());
                            participanteCapacitacionDao.update(participante);
                            object.put("updated", true);
                            int numPar = sede.getCronograma().getNumPar();
                            sede.getCronograma().setNumPar(numPar + 1);
                            sedeCapacitacionDao.update(sede);
                            
                            personaCapacitacionDao = (PersonaCapacitacionDao) FactoryDao.buildDao("capacitacion.PersonaCapacitacionDao");
                            persona = personaCapacitacionDao.buscarPorId(data.getInt("p"));
                            
                            JSONObject person = data.getJSONObject("a");
                            persona.setApePat(person.getString("pat"));
                            persona.setApeMat(person.getString("mat"));                            
                            persona.setNom(person.getString("nom"));
                            personaCapacitacionDao.update(persona);                 
                        }
                        break;
                        
                    case "N":
                        JSONObject person = data.getJSONObject("a");
                        
                        personaCapacitacionDao = (PersonaCapacitacionDao) FactoryDao.buildDao("capacitacion.PersonaCapacitacionDao");
                        persona = new Persona(person.getString("pat"), person.getString("mat"), person.getString("nom"), new Date(), 
                                person.getString("dni"), data.getString("c"));
                        personaCapacitacionDao.insert(persona);

                        OrganizacionCapacitacionDao organizacionCapacitacionDao = (OrganizacionCapacitacionDao) FactoryDao.buildDao("capacitacion.OrganizacionCapacitacionDao");
                        Organizacion organizacion = organizacionCapacitacionDao.buscarPorId(person.getInt("org"));
                        
                        TrabajadorCapacitacionDao trabajadorCapacitacionDao = (TrabajadorCapacitacionDao) FactoryDao.buildDao("capacitacion.TrabajadorCapacitacionDao");

                        Trabajador trabajador = new Trabajador(organizacion, persona);
                        trabajadorCapacitacionDao.insert(trabajador);
                        int rolId = Integer.parseInt(data.getString("rolId"));
                        ParticipanteCapacitacionId id = new ParticipanteCapacitacionId(sedCod, persona.getPerId());
                        participante = new ParticipanteCapacitacion(id, new Date(), new Date(), 'A', persona.getPerId());
                        participante.setRol_id(rolId);
                        participanteCapacitacionDao.insert(participante);
                        object.put("updated", true);
                        int numPar = sede.getCronograma().getNumPar();
                        sede.getCronograma().setNumPar(numPar + 1);
                        sedeCapacitacionDao.update(sede);
                       
                        UsuarioCapacitacionDao usuarioCapacitacionDao = (UsuarioCapacitacionDao) FactoryDao.buildDao("capacitacion.UsuarioCapacitacionDao");
                        Usuario usuario = new Usuario(persona, persona.getDni(), persona.getDni());
                        usuarioCapacitacionDao.insert(usuario);
                        
                        UsuarioSessionCapacitacionDao usuarioSessionCapacitacionDao = (UsuarioSessionCapacitacionDao) FactoryDao.buildDao("capacitacion.UsuarioSessionCapacitacionDao");
                      
       
                        UsuarioSession usuarioSession = new UsuarioSession(organizacion, rolId, usuario);
                        usuarioSessionCapacitacionDao.insert(usuarioSession);
                        break;
                }
            }

            return WebResponse.crearWebResponseExito("El participante se registró correctamente", WebResponse.OK_RESPONSE).setData(object);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "actualizarEstadoParticipante", e);
            return WebResponse.crearWebResponseError("No se pudo registrar al participante", WebResponse.BAD_RESPONSE);
        }
    }
}
