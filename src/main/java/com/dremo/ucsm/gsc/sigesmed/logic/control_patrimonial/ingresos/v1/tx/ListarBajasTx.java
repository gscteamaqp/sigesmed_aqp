/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import java.util.List;

import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.Bajas;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BajasDetalle;

import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.BajasDAO;

/**
 *
 * @author Administrador
 */
public class ListarBajasTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        JSONArray miArray = new JSONArray();
        
        try{
            List<Bajas> bajas = null;
            JSONObject requestData = (JSONObject)wr.getData();
            int org_id = requestData.getInt("org_id");
            
            BajasDAO baj_dao = (BajasDAO)FactoryDao.buildDao("scp.BajasDAO");
            bajas = baj_dao.listar_bajas_bienes(org_id);
            
            System.out.println("Numero de bajas: " + bajas.size());
            
            for (Bajas baj : bajas) {
                JSONObject oResponse = new JSONObject();
                oResponse.put("bajas_id", baj.getBajas_id());
                oResponse.put("cod_pat_bie", baj.getBd().get(0).getBm().getCon_pat_id());
                oResponse.put("des_bie", baj.getBd().get(0).getBm().getDes_bie());
                oResponse.put("cau_baj", baj.getCausal_baja().getDes());
                oResponse.put("estado", baj.getBd().get(0).getBm().getEstado_bie());
                
                if ("B".equals(baj.getBd().get(0).getBm().getEstado_bie())) {
                    oResponse.put("cond", 0);
                    oResponse.put("cond_nombre", "BUENO");
                }
                if ("M".equals(baj.getBd().get(0).getBm().getEstado_bie())) {
                    oResponse.put("cond", 1);
                    oResponse.put("cond_nombre", "MALO");
                }
                if ("R".equals(baj.getBd().get(0).getBm().getEstado_bie())) {
                    oResponse.put("cond", 2);
                    oResponse.put("cond_nombre", "REGULAR");
                }
                
                miArray.put(oResponse);
            }

        } catch(Exception e){
            System.out.println("No se pudo Listar los Bienes de Baja\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los Bienes de Baja", e.getMessage() );  

        }
           return WebResponse.crearWebResponseExito("Se Listo Las Bajas de Bienes Correctamente",miArray); 
     }
    
}
