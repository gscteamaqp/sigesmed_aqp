/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.plan_estudios.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.PlanEstudiosDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.MetaAtencion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanHoraArea;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanNivel;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
/**
 *
 * @author abel
 */
public class BuscarVigentePorOrganizacionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        int orgId = 0;        
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            
            orgId = requestData.getInt("organizacionID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo encontrar plan de estudios vigente, datos incorrectos", e.getMessage() );
        }
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        PlanEstudios plan = null;
        try{
            PlanEstudiosDao planDao = (PlanEstudiosDao)FactoryDao.buildDao("mech.PlanEstudiosDao");
            plan = planDao.buscarVigentePorOrganizacion(orgId);
            if(plan!=null)
                plan.setNiveles( planDao.listarNivelesPorPlanEstudios(plan.getPlaEstId()) );
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo encontrar plan de estudios vigente", e.getMessage() );
        }
        
        if(plan != null){
        
            JSONObject res = new JSONObject();
            res.put("planID", plan.getPlaEstId());
            res.put("anoEscolar", plan.getAñoEsc());
            res.put("nombre", plan.getNom());

            JSONArray aNiv = new JSONArray();
            for(PlanNivel pn : plan.getNiveles()){
                JSONObject oNiv = new JSONObject();
                oNiv.put("planNivelID", pn.getPlaNivId() );
                oNiv.put("jornadaID", pn.getJorId() );
                oNiv.put("turnoID", ""+pn.getTurId() );
                oNiv.put("periodoID", ""+pn.getPerId());
                oNiv.put("descripcion", pn.getDes());
                
                JSONArray aMet = new JSONArray();
                for(MetaAtencion m : pn.getMetas()){
                    JSONObject oMet = new JSONObject();
                    oMet.put("metaID", m.getMetAteId() );
                    oMet.put("planNivelID", m.getPlaNivId() );
                    oMet.put("gradoID", m.getGraId() );
                    oMet.put("secciones", m.getNumSec() );
                    oMet.put("alumnos", m.getNumAlu() );
                    oMet.put("horas", m.getHorCla() );
                    oMet.put("carga", m.getCarDoc() );
                    aMet.put(oMet);
                }
                oNiv.put("metas", aMet);
                
                JSONArray aDis = new JSONArray();
                for(PlanHoraArea p : pn.getHorasDisponibles()){
                    JSONObject oDis = new JSONObject();
                    oDis.put("horaDisponibleID", p.getPlaHorAreId() );
                    oDis.put("planNivelID", p.getPlaNivId() );
                    oDis.put("gradoID", p.getGraId() );
                    oDis.put("areaID", p.getAreCurId() );
                    oDis.put("horaL", p.getHorDis() );
                    aDis.put(oDis);
                }
                oNiv.put("horasDisponibles", aDis);
                
                aNiv.put(oNiv);
            }
            res.put("niveles",aNiv);

            return WebResponse.crearWebResponseExito("Se encontro el plan de estudios vigente",res);
        }
        else{
            return WebResponse.crearWebResponseError("No se encontro el plan de estudios vigente");
        }
    }    
    
    
}
