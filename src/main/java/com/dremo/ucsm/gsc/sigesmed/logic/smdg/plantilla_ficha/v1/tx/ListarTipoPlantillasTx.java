/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.plantilla_ficha.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.PlantillaTipoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaTipo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ListarTipoPlantillasTx implements ITransaction{
    public WebResponse execute(WebRequest wr) {        
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
//        JSONObject requestData = (JSONObject)wr.getData();
//        int orgId = requestData.getInt("orgId");
                
        List<PlantillaTipo> tipos = null;
        PlantillaTipoDao tiposDao = (PlantillaTipoDao)FactoryDao.buildDao("smdg.PlantillaTipoDao");
        
        try{
            tipos = tiposDao.buscarTodos(PlantillaTipo.class);        
        }catch(Exception e){            
            return WebResponse.crearWebResponseError("No se pudo Listar las tipos", e.getMessage() );
        }
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(PlantillaTipo t:tipos ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("id",t.getPtiId());
            oResponse.put("nom",t.getPtiNom());            
            miArray.put(oResponse);
        }        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);                        
    }
}
