/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.di;

import com.dremo.ucsm.gsc.sigesmed.core.dao.di.TrabajadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Persona;
import java.util.List;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.hibernate.type.StringType;

/**
 *
 * @author Administrador
 */
public class TrabajadorDaoHibernate extends GenericDaoHibernate<Trabajador> implements TrabajadorDao{
    
    @Override
    public String buscarUltimoCodigo() {
        String codigo = null;
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        try{
//            //listar ultimo codigo de tramite
//            String hql = "SELECT a.apoId FROM Apoderado a ORDER BY a.apoId DESC ";
//            Query query = session.createQuery(hql);
//            query.setMaxResults(1);
//            codigo = ((String)query.uniqueResult());
//            
//            //solo cuando no hay ningun registro aun
//            if(codigo==null)
//                codigo = "0000";
//        
//        }catch(Exception e){
//            System.out.println("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());
//            throw new UnsupportedOperationException("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());            
//        }
//        finally{
//            session.close();
//        }
        return codigo;
    }
    
    @Override
    public List<Trabajador> ListarxOrganizacion(int orgId) {
        List<Trabajador> trabajadores = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            //listar FuncionSistemas
            //String hql = "SELECT p FROM Persona p" ;
            String hql = "SELECT t FROM com.dremo.ucsm.gsc.sigesmed.core.entity.di.Trabajador t join fetch t.persona p join fetch t.organizacion o WHERE o.orgId =:p1";// 
            //String hql = "SELECT t FROM Trabajador t join fetch t.persona p WHERE t.orgId =:p1"; 
            Query query = session.createQuery(hql);            
            query.setParameter("p1", orgId);
            trabajadores = query.list();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo Listar el Directorio Interno \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar el Directorio Interno \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return trabajadores;
    }
    
    @Override
    public List<Trabajador> ListarxOrganizacionxTipo(int orgId, String tipo) {
        
        List<Trabajador> trabajadores = null;        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        
        tipo = tipo;
        String qry = " WHERE (o.orgId = " + orgId + " OR o.organizacionPadre = "+ orgId +")";
                
        qry += (tipo.isEmpty()?"":(" AND tc.traTip = '" + tipo + "'"));
        
        try{
            //listar FuncionSistemas
            //String hql = "SELECT p FROM Persona p" ;
            String hql = "SELECT t FROM com.dremo.ucsm.gsc.sigesmed.core.entity.di.Trabajador t join fetch t.traCar tc join fetch t.persona p join fetch t.organizacion o" + qry;
            Query query = session.createQuery(hql);                        
            trabajadores = query.list();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo Listar el Directorio Interno \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar el Directorio Interno \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return trabajadores;
    }
    
    @Override
    public Trabajador obtenerDatosPersonales(int orgId, int perId) {
        
        Trabajador trabajador = null;        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
                
        try{
        
            String hql = "SELECT t FROM com.dremo.ucsm.gsc.sigesmed.core.entity.di.Trabajador t "
                    + "join fetch t.traCar tc "
                    + "join fetch t.persona p "
                    + "join fetch t.organizacion o "
                    + "WHERE p.perId = "+perId+" AND o.orgId="+orgId;
            
            Query query = session.createQuery(hql);    
//            query.setParameter("p1", perId);
//            query.setParameter("p2", orgId);
            query.setMaxResults(1);
            trabajador = (Trabajador)query.uniqueResult();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo Listar los datos del trabajador \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los datos del trabajdor \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return trabajador;
    }
    
    @Override
    public List ListarxOrganizacionxTipo(String s, int orgId, String tipo){
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        
        String[] cmp = s.split(",");
        s="";
        for(int i = 0; i<cmp.length - 1; ++i){
            if(cmp[i].equals("o.nom")){
                s += cmp[i] + " as orgnom,";
            } 
            else    s+=cmp[i] + ",";
        }
        if(cmp[cmp.length-1].equals("o.nom")){
            s += cmp[cmp.length-1] + " as orgnom";
        }
        else{
            s += cmp[cmp.length-1];
        }
        
        
        String qry = " WHERE (t.org_id = " + orgId + " OR o.org_pad_id= " + orgId + ")";
        
        qry += (tipo.isEmpty()?"":(" AND tc.crg_tra_tip = '" + tipo + "'"));
        
        List data = null;
        
        try{
            SQLQuery query = session.createSQLQuery("select "+s+" from trabajador t " +
                                        "join pedagogico.persona p on t.per_id = p.per_id "
                    + "join trabajador_cargo tc on tc.crg_tra_ide = t.tra_car join organizacion o on o.org_id = t.org_id " + qry);
            
            data=query.list();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo Listar el Directorio Interno \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar el Directorio Interno \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }        
        
        return data;         
    }
    
    public List<String[]> ListarxOrganizacionxTipo2(){
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        
        List<String[]> data = null;
        
        try{
            Query query = session.createQuery("select t from com.dremo.ucsm.gsc.sigesmed.core.entity.di.Trabajador t");            
            data = query.list();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo Listar el Directorio Interno \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar el Directorio Interno \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }        
        
        return data;         
    }
    
}
