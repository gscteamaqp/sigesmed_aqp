/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.LibroCajaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.Asiento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentaContable;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentaCorriente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentasEfectivo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.DetalleCuenta;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.HechosLibro;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.LibroCaja;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.RegistroCompras;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.RegistroVentas;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.Tesorero;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author RE
 */
public class SaldosDelLibroTx implements ITransaction{
    
     @Override
    public WebResponse execute(WebRequest wr){
        //Parte de la operacion con la Base de Datos
        boolean flagH =false; // flag que permite verificar si existen registros de hechos del mes actual
        List<HechosLibro> hechosLibroCajaM= new ArrayList<>(); //list para almacenar los nuevos hechos del mes actual
        int indice=0;
        HechosLibro hecho=null;
        
        LibroCaja libro=null;
        int organizacionID = 0;  
        int personaID =0;
        boolean flag=false;
        Date fechaActual= new Date();
        List<CuentasEfectivo> cuentasEfectivo=null;
        List<CuentaCorriente> cuentasCorrientes=null;
        List<HechosLibro> hechosLibroCaja=null;
        List<List<Object>> objetos=null;  
        
        DateFormat fecha = new SimpleDateFormat("dd-MMMM-yyyy");
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();           
            organizacionID = requestData.getInt("organizacionID");
            personaID = requestData.getInt("personaID");
           
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo obtener la organizacion a la que pertenece", e.getMessage() );
        }
        
        LibroCajaDao libroDao = (LibroCajaDao)FactoryDao.buildDao("sci.LibroCajaDao");
      
              
        try {                        
            libro= libroDao.estadoLibroCaja(organizacionID);
            if(libro==null){
                
                JSONObject oResponse = new JSONObject();
                oResponse.put("estado","N");                    
                return WebResponse.crearWebResponseExito("El LIBRO DE LA I.E. NO ESTA REGISTRADO" ,oResponse);  
            }
              
            else if(libro.getEstReg().equals('I')){
                
                JSONObject oResponse = new JSONObject();
                oResponse.put("estado","I");                    
                return WebResponse.crearWebResponseExito("El ESTADO DEL LIBRO ES INACTIVO" ,oResponse);  
            }
            else if(libro.getEstReg().equals('C')){
                
                JSONObject oResponse = new JSONObject();
                oResponse.put("estado","C");                    
                return WebResponse.crearWebResponseExito("El ESTADO DEL LIBRO SE ENCUENTRA CERRADO" ,oResponse);  
            }
            
            flag = libroDao.verificarTesorero(libro,new Persona(personaID));
            if(!flag){ 

                JSONObject oResponse = new JSONObject();
                oResponse.put("estado","N");                    
                return WebResponse.crearWebResponseExito("SU ESTADO ES INACTIVO O SU FECHA DE INICIO ES POSTERIOR :"+fecha.format(fechaActual) ,oResponse);  
            }
            
            if (libro!=null){                                               
                cuentasEfectivo = libroDao.buscarCuentasEfectivo(libro);
                cuentasCorrientes = libroDao.buscarCuentasCorrientes(libro);
                hechosLibroCaja = libroDao.buscarHechosLibroCaja(libro);
                
                for(HechosLibro hl:hechosLibroCaja){
                    if(hl.getFecMes().getMonth()== fechaActual.getMonth()){
                        flagH=true;                        
                    }
                    else if(hl.getFecMes().getMonth()== fechaActual.getMonth()-1){
                        hechosLibroCajaM.add(hl);
                    }
                }
                if(!flagH){
                    indice = hechosLibroCaja.size();
                    for(CuentasEfectivo ce:cuentasEfectivo){
                        for(HechosLibro h:hechosLibroCajaM){
                            if(ce.getCuenta().getCueConId()==h.getCuenta().getCueConId()){
                                indice++;
                                hecho= new HechosLibro((short)indice, new LibroCaja(h.getLibroCaja().getLibCajId()),new CuentaContable(h.getCuenta().getCueConId()),fechaActual,h.getImpDeb(),h.getImpHab(),fechaActual,h.getUsuMod(),'A');
                                                              
                                libroDao.insertarHechos(hecho);
                                hechosLibroCaja.add(hecho);
                            }
                        }
                    }
                   
                }
                
                objetos= libroDao.listarAsientosConCompraVenta(fechaActual,libro);
            }
            
        } catch (Exception e) {
            System.out.println("No se pudo encontrar datos del Libro \n"+e);
            return WebResponse.crearWebResponseError("No se pudo encontrar datos del Libro", e.getMessage() );
        }
        /*
        *  Repuesta Correcta
        */ 
        DateFormat fechaHora = new SimpleDateFormat("yyyy/MM/dd");
          
        JSONObject oResponse = new JSONObject();
        
            JSONObject oLibro = new JSONObject();

            oLibro.put("libroID",libro.getLibCajId());
            oLibro.put("nombre",libro.getNom());
            oLibro.put("observacion",libro.getObs());
            oLibro.put("fechaApertura",fechaHora.format(libro.getFecApe())); 
            oLibro.put("fechaCierre",fechaHora.format(libro.getFecCie()));  
            oLibro.put("saldoActual",libro.getSalAct());   
            oLibro.put("saldoApertura",libro.getSalApe());   
            oLibro.put("estado",""+libro.getEstReg());
            oLibro.put("organizacionID",organizacionID);
            oLibro.put("personaID",libro.getPersona().getPerId());
            
        oResponse.put("libro",oLibro);
        oResponse.put("estado",libro.getEstReg());
        oResponse.put("saldoMes",libro.getSalAct());

      

                JSONArray cuentasE = new JSONArray();                              
                    for(CuentasEfectivo cE:cuentasEfectivo){
                         JSONObject cuenta = new JSONObject();
                        cuenta.put("cuentaEfectivoID",cE.getId()) ;
                        cuenta.put("cuentaContableID",cE.getCuenta().getCueConId());
                        cuenta.put("nombre",cE.getCuenta().getNomCue());
                        cuenta.put("numero",cE.getCuenta().getNumCue());
                        cuenta.put("saldoApertura",cE.getSalApe());
                        cuenta.put("saldoActual",cE.getSalAct());
                        for(CuentaCorriente cC:cuentasCorrientes){
                            if(cC.getCuenta().getCueConId()==cE.getCuenta().getCueConId()){
                                cuenta.put("subClase",cC.getCuenta().getSubCla());
                                cuenta.put("bancoID",cC.getBanco().getEmpId());
                                cuenta.put("banco",cC.getBanco().getRazSoc());
                                cuenta.put("num",cC.getNum()); 
                            }
                        }
                         cuentasE.put(cuenta);
                    } 
                oResponse.put("cuentasEfectivo",cuentasE); 

                    BigDecimal debeM= new BigDecimal(BigInteger.ZERO);
                    BigDecimal haberM= new BigDecimal(BigInteger.ZERO);
                            
                    BigDecimal debeMA= new BigDecimal(BigInteger.ZERO);
                    BigDecimal haberMA= new BigDecimal(BigInteger.ZERO);

                    
                JSONArray hechosL = new JSONArray();                              
                    for(HechosLibro hl:hechosLibroCaja){
                         JSONObject cuenta = new JSONObject();
                        cuenta.put("hechosID", hl.getId());
                        cuenta.put("nombre",hl.getCuenta().getNomCue());
                        cuenta.put("numero",hl.getCuenta().getNumCue());
                        cuenta.put("cuentaContableID",hl.getCuenta().getCueConId());
                        cuenta.put("fecha",fechaHora.format(hl.getFecMes()));

                        cuenta.put("fechaMes",hl.getFecMes().getMonth());

                        
                        if(hl.getFecMes().getMonth()==fechaActual.getMonth()){
                           
                            debeM=debeM.add(hl.getImpDeb()).setScale(2);
                            haberM=haberM.add(hl.getImpHab()).setScale(2);
                        }
                        else if(hl.getFecMes().getMonth()==fechaActual.getMonth()-1){
                            debeMA=debeMA.add(hl.getImpDeb()).setScale(2);
                            haberMA=haberMA.add(hl.getImpHab()).setScale(2);
                        }                                                    
                        cuenta.put("importeD",hl.getImpDeb());
                        cuenta.put("importeH",hl.getImpHab());
                      
                         hechosL.put(cuenta);
                    }
                    
            oResponse.put("saldoMesAnterior",((debeMA.subtract(haberMA).setScale(2)).toString()));
            oResponse.put("debe",debeM.setScale(2).toString());
            oResponse.put("haber",haberM.setScale(2).toString());
            oResponse.put("hechosLibroCaja",hechosL); 

               
        DateFormat fechaA = new SimpleDateFormat("dd/MM/yyyy");    
        JSONArray miArray = new JSONArray();
        int i=1;
        for(List<Object> list:objetos ){
              JSONObject oAsiento = new JSONObject();
              
            if(list.get(0)!=null){
                Asiento objA=(Asiento)list.get(0);               
                oAsiento.put("i",i++); 
                oAsiento.put("asiId",objA.getAsiId());
                oAsiento.put("operacionID",objA.getOpeId());
                oAsiento.put("codUniOpeId",objA.getCodUniOpeId());
                oAsiento.put("glosa",objA.getGloOpe());
              //oResponse.put("importe",importe);
                oAsiento.put("numeroD",objA.getNumDoc());
                oAsiento.put("observacion",objA.getObs());
                oAsiento.put("estado",objA.getEstReg());
                oAsiento.put("libro",objA.getCodLibro());                             
                oAsiento.put("fecha",fechaA.format(objA.getFecAsi()));                             
                
                String registro= ""+objA.getCodLibro();
                
                  for(DetalleCuenta det:objA.getDetalleCuentas()){                       
                                             
                       if(det.getNatDetCue()){
                           JSONObject jDebe = new JSONObject();         
                                jDebe.put("cuentaContableID", det.getCuentaContable().getCueConId());
                                jDebe.put("nombre",det.getCuentaContable().getNomCue());  
                                jDebe.put("importe",det.getImpDetCue()); 
                           oAsiento.put("importe",det.getImpDetCue());
                           oAsiento.put("debe",jDebe);
                       }
                       else{
                            JSONObject jHaber = new JSONObject();         
                                jHaber.put("cuentaContableID", det.getCuentaContable().getCueConId());
                                jHaber.put("nombre",det.getCuentaContable().getNomCue());  
                                jHaber.put("importe",det.getImpDetCue()); 
                            oAsiento.put("importe",det.getImpDetCue());
                            oAsiento.put("haber",jHaber);
                                   }
                       
                  }
                   
                                                                  
              
                
                 if(list.get(1)!=null && registro.equals("C")){
                     RegistroCompras objC=(RegistroCompras)list.get(1);
                     
                    JSONObject jTipoPago = new JSONObject();         
                        jTipoPago.put("tipoPagoID", objC.getTipoPago().getTipPagId());
                        jTipoPago.put("nombre",objC.getTipoPago().getNom());        
                    oAsiento.put("tipoPago",jTipoPago);  
                    
                     JSONObject jClienteProveedor = new JSONObject();         
                        jClienteProveedor.put("clienteProveedorID", objC.getClienteProveedor().getCliProId());
                        jClienteProveedor.put("datos",objC.getClienteProveedor().getDat());        
                        jClienteProveedor.put("estado",objC.getClienteProveedor().getEstReg());   
                        jClienteProveedor.put("tipoDocumento",objC.getClienteProveedor().getTipoDocumentoIdentidad().getNom());        
                        jClienteProveedor.put("estado",objC.getClienteProveedor().getEstReg()); 
                    oAsiento.put("clienteProveedor",jClienteProveedor); 
                    
                    JSONObject jDoc = new JSONObject(); 
                        jDoc.put("url",Sigesmed.UBI_ARCHIVOS+"/contable/");
                        jDoc.put("nombreArchivo",objC.getNomDocAdj());
                        jDoc.put("edi",true);
                        
                    oAsiento.put("doc",jDoc); 
                    
                     
                 }                 
                 else if(list.get(1)!=null && registro.equals("V")){
                     RegistroVentas objV=(RegistroVentas)list.get(1);
                    JSONObject jTipoPago = new JSONObject();         
                        jTipoPago.put("tipoPagoID", objV.getTipoPago().getTipPagId());
                        jTipoPago.put("nombre",objV.getTipoPago().getNom());        
                    oAsiento.put("tipoPago",jTipoPago);    
                    
                     JSONObject jClienteProveedor = new JSONObject();         
                        jClienteProveedor.put("clienteProveedorID", objV.getClienteProveedor().getCliProId());
                        jClienteProveedor.put("datos",objV.getClienteProveedor().getDat());        
                        jClienteProveedor.put("estado",objV.getClienteProveedor().getEstReg());   
                        jClienteProveedor.put("tipoDocumento",objV.getClienteProveedor().getTipoDocumentoIdentidad().getNom());        
                        jClienteProveedor.put("estado",objV.getClienteProveedor().getEstReg()); 
                    oAsiento.put("clienteProveedor",jClienteProveedor);
                    
                        JSONObject jDoc = new JSONObject(); 
                        jDoc.put("url",Sigesmed.UBI_ARCHIVOS+"/contable/");
                        jDoc.put("nombreArchivo",objV.getNomDocAdj());
                        jDoc.put("edi",true);
                        
                    
                    oAsiento.put("doc",jDoc); 
                                        
                 }
                 
          
            
        }                    
          miArray.put(oAsiento);       
            
        }
        
        oResponse.put("asientos",miArray); 
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",oResponse);        
        //Fin
    }
    
}
