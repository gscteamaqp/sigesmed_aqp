/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.gestion_series_documentales.v1.tx;


import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.dremo.ucsm.gsc.sigesmed.util.GCell;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.border.SolidBorder;
import com.itextpdf.layout.element.Paragraph;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.SerieDocumentalDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.SerieDocumental;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;

import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.UnidadOrganicaDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.UnidadOrganica;

import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.InventarioTransferenciaDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.InventarioTransferencia;

import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.ArchivoInventarioDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.ArchivosInventarioTransferencia;                
                          
import java.text.SimpleDateFormat;


/**
 *
 * @author Jeferson
 */
public class Reporte_UnidadTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");

        int area_id = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            area_id = requestData.getInt("area_id");
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("Error al Elegir Area , ERROR AL GENERAR REPORTE ", e.getMessage() );
        }
        
        
        /*Listamos las Unidades Organicas perteneciente a dicha area*/
        List<UnidadOrganica> unidades_organicas ;
        try{
            UnidadOrganicaDAO unidad_dao = (UnidadOrganicaDAO)FactoryDao.buildDao("sad.UnidadOrganicaDAO");
            unidades_organicas=unidad_dao.buscarPorArea(area_id);
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listar las Unidades Organicas , ERROR AL GENERAR REPORTE  ", e.getMessage() );
        }
        
        /*Listamos Las Series Asociadas a dicha unidad organica */
         List<List<SerieDocumental>> total_series_unidad = new ArrayList<>();
        try{
            List<SerieDocumental> series_por_unidad;
            for(int i= 0 ; i<unidades_organicas.size();i++){
                SerieDocumentalDAO serie_dao = (SerieDocumentalDAO)FactoryDao.buildDao("sad.SerieDocumentalDAO");
                series_por_unidad =  serie_dao.buscarPorUnidadOrganica(unidades_organicas.get(i).getUniOrgId());
                total_series_unidad.add(series_por_unidad);
            }
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listar las series por unidad organica , ERROR REPORTE  ", e.getMessage() );
        }
        
        /*Listamos Los Inventarios Asociadas a cada Serie Documental*/
        
        List<List<List<ArchivosInventarioTransferencia>>> archivos_unidad = new ArrayList<>();
        try{
            for(int i=0 ; i<total_series_unidad.size();i++){
                List<List<ArchivosInventarioTransferencia>> archivos_series = new ArrayList<>();
                /*Seleccionamos todas las series por unidad*/
                List<ArchivosInventarioTransferencia> archivos_serie ; 
                for(int j=0 ;j<total_series_unidad.get(i).size();j++){
                   ArchivoInventarioDAO archivo_dao = (ArchivoInventarioDAO)FactoryDao.buildDao("sad.ArchivoInventarioDAO");
                   archivos_serie =  archivo_dao.listarArchivosSerie(total_series_unidad.get(i).get(j).getSerDocId());
                   if(archivos_serie== null | archivos_serie.size()==0){continue ;}
                   else{
                        archivos_series.add(archivos_serie);
                   }    
                                 
                }
                archivos_unidad.add(archivos_series); /*Añadimos archivos por unidad organica*/
                archivos_serie = null;
                archivos_series = null;
            }
            
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listar los Inventarios de Transferencia , ERROR REPORTE  ", e.getMessage() );
        }
        
        Mitext m = null; 
        try{
            m = new Mitext(true,"REPORTE DE ARCHIVOS POR UNIDAD ORGANICA");
            m.newLine(2);
            m.agregarParrafo("AREA : Archivo Central");
            
            String [] titulos ={"Expediente","Registro","Fecha","Documentos","Folio"};   
            float[] columnWidths={2,2,2,2,2};
          /*Preparamos el reporte*/
            for(int u=0 ; u<unidades_organicas.size();u++){
                GTabla t_archivos = new GTabla(columnWidths);
                t_archivos.build(titulos);
                GCell[] cell ={t_archivos.createCellCenter(1,1),t_archivos.createCellCenter(1,1),t_archivos.createCellCenter(1,1),t_archivos.createCellCenter(1,1),t_archivos.createCellCenter(1,1)};
                t_archivos.addHeaderCell(new Cell(1,12).setBorder(Border.NO_BORDER).add(new Paragraph(unidades_organicas.get(u).getnombre())).setFontSize(10).setBorder(Border.NO_BORDER));
                for(int s=0 ; s<archivos_unidad.get(u).size();s++){
                    for(int a=0 ; a<archivos_unidad.get(u).get(s).size() ; a++){
                        
                        String[] archivos_data = new String[titulos.length];
                        ArchivosInventarioTransferencia archivo;
                        archivo = archivos_unidad.get(u).get(s).get(a);
                        /*Llenamos la data a la tabla del Reporte*/
                        archivos_data[0]= archivo.getcod_exp();
                        archivos_data[1]= Integer.toString(archivo.getarcinvtraID());
                        archivos_data[2]= formatter.format(archivo.getfecha());
                        archivos_data[3]= archivo.gettitulo();
                        archivos_data[4]= Integer.toString(archivo.getnumfol());
                        t_archivos.processLineCell(archivos_data,cell);
                    }
                }  
                m.agregarTabla(t_archivos);
                t_archivos = null;
            }
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError(" ERROR AL GENERAR EL  REPORTE  ", e.getMessage() );
        }
        
         /*Mostramos el reporte*/
        JSONArray miArray = new JSONArray();
        try{
            
            m.cerrarDocumento();
            JSONObject oResponse = new JSONObject();        
            oResponse.put("datareporte",m.encodeToBase64());
            miArray.put(oResponse); 
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError(" ERROR AL GENERAR REPORTE  ", e.getMessage() );
        }
        
        
        return WebResponse.crearWebResponseExito("Se genero el Reporte con exito ...",miArray); 
        
        
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
