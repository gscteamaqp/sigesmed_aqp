package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author G-16
 */
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.LibroCajaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Area;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.Asiento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.ClienteProveedor;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.RegistroCompras;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.TipoPago;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONObject;

public class InsertarCompraReciboAuxiliarTx implements ITransaction{   
    
    @Override    
    public WebResponse execute(WebRequest wr)  {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        
        
        Object obj=null;
        FileJsonObject docCompra= null;
        String nomDocAdj="";
        
        LibroCajaDao compraDao = (LibroCajaDao)FactoryDao.buildDao("sci.LibroCajaDao"); 
        RegistroCompras nuevaCompra = null;
        // RegistroCompras(int codUniOpeId, Date fecReg)
        try{            
             
            JSONObject requestData = (JSONObject)wr.getData();           
            int clienteProveedorID = requestData.getInt("clienteProveedorID");                       
            short tipoPagoID = (short)requestData.getInt("tipoPagoID");
            //int areaID = requestData.getInt("areaID");
            int orgID = requestData.getInt("orgID");
            //String datos= requestData.getString("datos");
            
            String fecha=requestData.getString("fechaR");           
          
            double importe = requestData.getDouble("importe");
            String numeroD = requestData.optString("numeroD","");
            String dni = requestData.optString("dni");
            int plazo = requestData.getInt("plazo");

            //verificamos si existe un archivo adjunto
            JSONObject jDoc = (JSONObject)requestData.opt("doc");        
            
            JSONObject jsonArchivo = jDoc.optJSONObject("archivo");
            
            
            if( jsonArchivo !=null && jsonArchivo.length() > 0 ){
                obj = compraDao.llave(RegistroCompras.class,"codUniOpeId");                
                docCompra = new FileJsonObject( jsonArchivo ,((int)obj+1)+"_compra"); 
                nomDocAdj = docCompra.getName();
            }         
            nuevaCompra = new  RegistroCompras(0,new Date(fecha));
            nuevaCompra.setTipoPago(new TipoPago(tipoPagoID));
            nuevaCompra.setClienteProveedor(new ClienteProveedor(clienteProveedorID));
            nuevaCompra.setOrgId(orgID);
            nuevaCompra.setImpTot(new BigDecimal(importe));
            nuevaCompra.setNumCor(numeroD);
            nuevaCompra.setFecMod(new Date());
            nuevaCompra.setUsuMod(wr.getIdUsuario());
            nuevaCompra.setEstReg('A');
            nuevaCompra.setNomDocAdj(nomDocAdj);
            nuevaCompra.setPerDni(dni);
            nuevaCompra.setPlaDia(plazo);
            //nuevaCompra = new RegistroCompras(0, new ClienteProveedor(clienteProveedorID,datos),new TipoPago(tipoPagoID),new Date(fecha), numeroD,new BigDecimal(importe),new Date(), wr.getIdUsuario(), 'A',nomDocAdj);
            
            
        }catch(Exception e){
          
            return WebResponse.crearWebResponseError("No se pudo registrar Compra, datos incorrectos", e.getMessage() );
        }
              
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        try{
            compraDao.insertarCompra(nuevaCompra);        
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo registrar la compra ", e.getMessage() );
        }
        //Fin
        
      
        /*
        *  Repuesta Correcta
        */
        
         if(docCompra !=null){
            BuildFile.buildFromBase64("contable", docCompra.getName(), docCompra.getData());
        }
         
        JSONObject oResponse = new JSONObject();
        oResponse.put("codUniOpeID",nuevaCompra.getCodUniOpeId());
        oResponse.put("nomDocAdj",nuevaCompra.getNomDocAdj());
       
       
       

        return WebResponse.crearWebResponseExito("El registro de la Compra se realizo correctamente", oResponse);
        //Fin
    }    
    
    
}
