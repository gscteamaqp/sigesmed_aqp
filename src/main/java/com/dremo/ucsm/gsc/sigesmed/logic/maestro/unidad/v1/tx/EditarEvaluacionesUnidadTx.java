package com.dremo.ucsm.gsc.sigesmed.logic.maestro.unidad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.EvaluacionesUnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.EvaluacionEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.EvaluacionesUnidadDidactica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaEvaluacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaTarea;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.EvaluacionEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.hibernate.Hibernate.isInitialized;

/**
 * Created by Administrador on 03/01/2017.
 */
public class EditarEvaluacionesUnidadTx implements ITransaction{
    private static Logger logger = Logger.getLogger(EditarEvaluacionesUnidadTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
           
        try{
            JSONObject data = (JSONObject) wr.getData();
            int idEvaluacion = data.getInt("id");
            return editarEvaluacion(idEvaluacion,data);
            
        }catch(Exception e){
            return WebResponse.crearWebResponseError("no se puede Actualizar la evaluacion");
        }
        
        
    }

    private WebResponse editarEvaluacion(int idEvaluacion, JSONObject data) {
        try{
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            EvaluacionesUnidadDidacticaDao evaDao = (EvaluacionesUnidadDidacticaDao) FactoryDao.buildDao("maestro.plan.EvaluacionesUnidadDidacticaDao");
           
            Date fec_nueva = formatter.parse(data.getString("fec"));
            Date fec_ant = formatter.parse(data.getString("fec_ant"));
            
            EvaluacionesUnidadDidactica evaluacion = evaDao.buscarEvaluacionId(idEvaluacion);
            evaluacion.setNom_eva(data.getString("nom"));
            evaluacion.setDes(data.optString("des",evaluacion.getDes()));
            evaluacion.setFecEva(fec_nueva);
            evaluacion.setFecMod(new Date());
            evaluacion.setEstReg(evaluacion.getEstReg());
            evaluacion.setTipEva(data.optInt("tip",evaluacion.getTipEva()));
            evaDao.update(evaluacion);
            
            EvaluacionEscolarDao evaEsDao = (EvaluacionEscolarDao) FactoryDao.buildDao("web.EvaluacionEscolarDao");
            EvaluacionEscolar evaluacion_esc = evaEsDao.obtenerEvaluacion(idEvaluacion);
            evaluacion_esc.setNom(data.getString("nom"));
            evaluacion_esc.setDes(data.optString("des",evaluacion.getDes()));
            evaluacion_esc.setFecEnv(fec_nueva);
            evaluacion_esc.setFecMod(new Date());
            evaluacion_esc.setEstReg(evaluacion_esc.getEstReg());
            
            
            if(fec_nueva.compareTo(fec_ant) > 0){
                evaluacion_esc.setEstado('N');
            }
            if(fec_nueva.compareTo(new Date()) < 0){
                evaluacion_esc.setEstado('F');
            }
            
            //Actualizamos la Bandeja de Alumnos
            List<BandejaEvaluacion> bandejas;
            bandejas = evaluacion_esc.getBandeja_evaluacion();
            
            if(isInitialized(bandejas)!=false)
            {
                
                if(fec_nueva.compareTo(fec_ant) > 0 && fec_nueva.compareTo(new Date()) > 0){
                    for(BandejaEvaluacion bandeja : bandejas){
                        bandeja.setEstado('N');
                    }
                }
                else if(fec_nueva.compareTo(new Date()) < 0){
                    for(BandejaEvaluacion bandeja : bandejas){
                        if(bandeja.getEstado() == 'N'){
                            bandeja.setEstado('F');
                        }   
                    }       
                }
                
            }
            evaEsDao.update(evaluacion_esc);
            
            return WebResponse.crearWebResponseExito("Se edito la evaluacion");
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarEvaluacion",e);
            return WebResponse.crearWebResponseError("no se puede regitrar la evaluacion");
        }
    }
}
