/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.web;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaEvaluacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaEvaluacionModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.RespuestaEvaluacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.EvaluacionEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.MensajeCalificacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.PreguntaEvaluacion;

import java.util.List;

/**
 *
 * @author abel
 */
public interface EvaluacionEscolarDao extends GenericDao<EvaluacionEscolar>{
    
    public void insertarPreguntas(List<PreguntaEvaluacion> preguntas );
    public void eliminarPreguntas(int evaluacionID );
    public void insertarMensajes(List<MensajeCalificacion> mensajes );
    public void eliminarMensajes(int evaluacionID );
    public void finalizarEvaluacion(EvaluacionEscolar evaluacion );
    public void enviarEvaluacion(EvaluacionEscolar evaluacion,BandejaEvaluacion bandeja);
    public void enviarEvaluacion(EvaluacionEscolar evaluacion,List<BandejaEvaluacion> bandejas);
    
    public void enviarEvaluacionResuelta(BandejaEvaluacion bandeja);
    public void calificarEvaluacionResuelta(BandejaEvaluacion bandeja );
    public void evaluacionesFueraDeTiempo(List<Integer> evaluaciones );
    public void insertarRespuestasAlumno(List<RespuestaEvaluacion> respuestas);
    
    public List<EvaluacionEscolar> buscarPorPlanEstudios(int planID);
    public List<EvaluacionEscolar> buscarPorDocente(int planID,int docenteID);
    
    public List<MensajeCalificacion> buscarMensajesPorEvaluacion(int evaluacionID);
    public List<PreguntaEvaluacion> buscarPreguntasPorEvaluacion(int evaluacionID);
    
    public List<BandejaEvaluacionModel> buscarAlumnosQueCumplieronEvaluacion(int evaluacionID);
    public List<RespuestaEvaluacion> verRespuestasEvaluacion(int bandejaEvaluacionID);
    
    public List<BandejaEvaluacion> buscarEvaluacionesPorAlumno(int planID,int alumnoID);
    public EvaluacionEscolar obtenerEvaluacion(int evaluacionID);
    public BandejaEvaluacion obtenerEvaluacionPreguntas(int bandejaID);
    
    
}
