/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.rol.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.RolDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Rol;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class ListarRolPorOrganizacionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        int organizacionID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            organizacionID = requestData.getInt("organizacionID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo listar los roles por organizacion, datos incorrectos", e.getMessage() );
        }      
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<Rol> roles = null;
        RolDao rolDao = (RolDao)FactoryDao.buildDao("RolDao");
        try{
            roles = rolDao.buscarPorOrganizacion(organizacionID );
        
        }catch(Exception e){
            System.out.println("No se pudo Listar Roles por Organizacion\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los Roles por Organizacion", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Rol rol:roles ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("rolID",rol.getRolId() );
            oResponse.put("abreviatura",rol.getAbr());
            oResponse.put("nombre",rol.getNom());
            oResponse.put("descripcion",rol.getDes());
            oResponse.put("fecha",rol.getFecMod().toString());
            oResponse.put("estado",""+rol.getEstReg());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}

