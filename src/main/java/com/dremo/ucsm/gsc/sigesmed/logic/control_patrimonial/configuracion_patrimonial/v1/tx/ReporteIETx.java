/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.configuracion_patrimonial.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;

import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BienesMuebles;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.BienesMueblesDAO;
import java.text.SimpleDateFormat;

import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.dremo.ucsm.gsc.sigesmed.util.GCell;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.border.SolidBorder;
import com.itextpdf.layout.element.Paragraph;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ReporteIETx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {

            JSONObject requestData = (JSONObject) wr.getData();
            int org_id = requestData.getInt("org_id");

            /*Listar Organizacion (Nombre , Ubicacion , Responsable) */
            Organizacion org = null;
            OrganizacionDao org_dao = (OrganizacionDao) FactoryDao.buildDao("OrganizacionDao");
            org = org_dao.buscarConTipoOrganizacionYPadre(org_id);
            String nombre_ie = org.getNom();
            String ubic_ie = org.getDir();

            List<BienesMuebles> bm = null;
            BienesMueblesDAO bm_dao = (BienesMueblesDAO) FactoryDao.buildDao("scp.BienesMueblesDAO");
            bm = bm_dao.listarBienesMuebles(org_id);

            String cab1[] = {"DATOS DEL BIEN PATRIMONIAL", "DATOS DE LA ASIGNACION DEL BIEN", "DATOS DE ADQUISICION", "DATOS CONTABLES DEL BIEN"};

            String cab3[] = {"Cod.Patrimonial", "Nro.Correlat", "Descripcion Bien", "Medidas", "Marca", "Modelo", "Serie", "Color",
                "Centro de Costo", "Empleado Final",
                "Tipo Coc", "Num Coc", "Fecha Adquisicion",
                "Valor Inicial", "Valor Depreciacion", "Cod.Cuenta", "SIB Cuenta"};

            Mitext m = null;
            m = new Mitext(true, "REPORTE DE BIENES POR INSTITUCION");
            m.newLine(2);
            m.agregarParrafo("Nombre de la Institucion : " + nombre_ie);
            m.newLine(3);
            m.agregarParrafo("Ubicacion : " + ubic_ie);

            // Creamos el Objeto Reporte
            float columnWidths_1[] = {12, 4, 3, 4};
            float columnWidths_2[] = {1, 1, 3, 1, 2, 2, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1};
            GTabla tabla_1 = new GTabla(columnWidths_1);
            tabla_1.setWidthPercent(100);
            tabla_1.build(cab1);
            GTabla tabla_2 = new GTabla(columnWidths_2);
            tabla_2.setWidthPercent(100);
            tabla_2.build(cab3);

            m.agregarTabla(tabla_1);

            /*Inizializamos la data del reporte en vacio*/
            int data_length = cab3.length;
            String[] archivos_data = new String[data_length];
            for (int i = 0; i < data_length; i++) {
                archivos_data[i] = " ";
            }

            GCell[] cell = {tabla_2.createCellCenter(1, 1), tabla_2.createCellCenter(1, 1), tabla_2.createCellCenter(1, 1), tabla_2.createCellCenter(1, 1), tabla_2.createCellCenter(1, 1), tabla_2.createCellCenter(1, 1), tabla_2.createCellCenter(1, 1), tabla_2.createCellCenter(1, 1), tabla_2.createCellCenter(1, 1), tabla_2.createCellCenter(1, 1), tabla_2.createCellCenter(1, 1), tabla_2.createCellCenter(1, 1), tabla_2.createCellCenter(1, 1), tabla_2.createCellCenter(1, 1), tabla_2.createCellCenter(1, 1), tabla_2.createCellCenter(1, 1), tabla_2.createCellCenter(1, 1)};

            /*Llenado de la Data de los Bienes en las Tablas*/
            int size = bm.size();

            for (int i = 0; i < size; i++) {
                BienesMuebles bien_mueble = bm.get(i);

                String cat_bie_id = Integer.toString(bien_mueble.getCat_bie_id());
                String cod_int = bien_mueble.getCod_int();
                String desc = bien_mueble.getDes_bie();
                String dim = Integer.toString(bien_mueble.getDtm().getDim());
                String marca = bien_mueble.getDtm().getMarc();
                String mod = bien_mueble.getDtm().getMod();
                String serie = bien_mueble.getDtm().getSer();
                String ambiente = bien_mueble.getAmbiente().getDes();
                String fec_reg = formatter.format(bien_mueble.getFec_reg());
                //    String val_cont = Integer.toString(bien_mueble.getVal_cont().getVal_cont());
                //    String cod_cue = bien_mueble.getVal_cont().getCod_cue();

                /*Llenamos la data*/
                archivos_data[1] = cod_int;
                archivos_data[2] = desc;
                archivos_data[3] = dim;
                archivos_data[4] = marca;
                archivos_data[5] = mod;
                archivos_data[6] = serie;
                archivos_data[12] = fec_reg;
                //    archivos_data[13]=val_cont;
                archivos_data[16] = cat_bie_id;
                archivos_data[8] = ambiente;
                //    archivos_data[15]=cod_cue;

                tabla_2.processLineCell(archivos_data, cell);
            }
            m.agregarTabla(tabla_2);


            /*Construir la Data*/
 /*Mostrar el Reporte*/
 /*Mostramos el reporte*/
            JSONArray miArray = new JSONArray();

            m.cerrarDocumento();
            JSONObject oResponse = new JSONObject();
            oResponse.put("datareporte", m.encodeToBase64());
            miArray.put(oResponse);

            return WebResponse.crearWebResponseExito("Se genero el Reporte con exito ...", miArray);

        } catch (Exception e) {
            System.out.println(e);
            return WebResponse.crearWebResponseError("ERROR AL GENERAR REPORTE ", e.getMessage());

        }

        //    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
