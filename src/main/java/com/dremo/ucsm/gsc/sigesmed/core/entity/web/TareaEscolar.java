package com.dremo.ucsm.gsc.sigesmed.core.entity.web;

import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.*;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="pedagogico.tarea_escolar" )
public class TareaEscolar  implements java.io.Serializable {

    @Id
    @Column(name="tar_esc_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_tareaescolar", sequenceName="pedagogico.tarea_escolar_tar_esc_id_seq" )
    @GeneratedValue(generator="secuencia_tareaescolar")
    private int tarEscId;
    @Column(name="nom",length=64)
    private String nom;
    @Column(name="des", length=512)
    private String des;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_env", nullable=false, length=29)
    private Date fecEnv; 
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_ent", nullable=false, length=29)
    private Date fecEnt; 
    
    @Column(name="num_doc")
    private int numDoc;
    
    @Column(name="not1")
    private int nota1;
    
    @Column(name="not2")
    private int nota2;
    
    @Column(name="doc_adj")
    private String docAdj;
    
    @Column(name="est_tar")
    private char estado;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_mod", nullable=false, length=29)
    private Date fecMod;    
    @Column(name="usu_mod")
    private int usuMod;
    @Column(name="est_reg")
    private char estReg;
    
    @Column(name="pla_est_id")
    private int plaEstId;    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="pla_est_id",insertable = false,updatable = false)
    private PlanEstudios planEstudios;
    
    @Column(name="gra_id")
    private int graId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="gra_id",updatable = false,insertable = false)
    private Grado grado;
    
    @Column(name="sec_id")
    private char secId;
    
    @Column(name="are_cur_id")
    private int areCurId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="are_cur_id",updatable = false,insertable = false)
    private AreaCurricular area;    

    
    @OneToMany(mappedBy="tareaEscolar" , cascade= CascadeType.ALL)
    private List<BandejaTarea> bandeja;

    public List<BandejaTarea> getBandeja() {
        return bandeja;
    }

    
  
    
    @Column(name="ses_id")
    private int ses_id;
    
    @Column(name="tip_per")
    private String tip_per;
    
    @Column(name="id_per")
    private int id_per;

    @Column(name="id_tar")
    private int id_tar;

    public void setid_tar(int id_tar){
        this.id_tar = id_tar;
    }
    public int getid_tar(){
        return this.id_tar;
    }

    public void setSes_id(int ses_id) {
        this.ses_id = ses_id;
    }

    public void setTip_per(String tip_per) {
        this.tip_per = tip_per;
    }

    public void setId_per(int id_per) {
        this.id_per = id_per;
    }

    public int getSes_id() {
        return ses_id;
    }

    public String getTip_per() {
        return tip_per;
    }

    public int getId_per() {
        return id_per;
    }
    
    
    
    
    public TareaEscolar() {
        this.bandeja = null;
    }
    public TareaEscolar(int tarEscId) {
        this.tarEscId = tarEscId;
        this.bandeja = null;
    }
    public TareaEscolar(int tarEscId,String nom, String des,Date fecEnv, Date fecEnt,int numDoc, int nota1, int nota2, String docAdj,char estado,int plaEstId,int graId,char secId,int areCurId, Date fecMod, int usuMod, char estReg) {
       this.tarEscId = tarEscId;
       this.nom = nom;
       this.des = des;
       this.fecEnv = fecEnv;
       this.fecEnt = fecEnt;
       this.numDoc = numDoc;
       this.nota1 = nota1;
       this.nota2 = nota2;
       this.docAdj = docAdj;
       this.estado = estado;
       this.plaEstId = plaEstId;
       this.graId = graId;
       this.secId = secId;
       this.areCurId = areCurId;
               
       this.fecMod = fecMod;
       this.usuMod = usuMod;
       this.estReg = estReg;
       this.bandeja = null;
    }
   
     
    public int getTarEscId() {
        return this.tarEscId;
    }    
    public void setTarEscId(int tarEscId) {
        this.tarEscId = tarEscId;
    }
    
    public String getNom() {
        return this.nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }    
    public String getDes() {
        return this.des;
    }
    public void setDes(String des) {
        this.des = des;
    }
    
    public Date getFecEnv() {
        return this.fecEnv;
    }
    public void setFecEnv(Date fecEnv) {
        this.fecEnv = fecEnv;
    }
    
    public Date getFecEnt() {
        return this.fecEnt;
    }
    public void setFecEnt(Date fecEnt) {
        this.fecEnt = fecEnt;
    }
    
    public int getNumDoc() {
        return this.numDoc;
    }
    public void setNumDoc(int numDoc) {
        this.numDoc = numDoc;
    }
    
    public int getNota1() {
        return this.nota1;
    }
    public void setNota1(int nota1) {
        this.nota1 = nota1;
    }
    
    public int getNota2() {
        return this.nota2;
    }
    public void setNota2(int nota2) {
        this.nota2 = nota2;
    }
    
    public String getDocAdj(){
        return this.docAdj;
    }
    public void setDocAdj(String docAdj){
        this.docAdj = docAdj;
    }
    
    public char getEstado(){
        return this.estado;
    }
    public void setEstado(char estado){
        this.estado = estado;
    }
    
    public Date getFecMod() {
        return this.fecMod;
    }
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }
    
    public int getUsuMod() {
        return this.usuMod;
    }
    public void setUsuMod(int usuMod) {
        this.usuMod = usuMod;
    }
    
    public char getEstReg() {
        return this.estReg;
    }
    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }
    
    public int getPlaEstId() {
        return this.plaEstId;
    }    
    public void setPlaEstId(int plaEstId) {
        this.plaEstId = plaEstId;
    }
    
    public PlanEstudios getPlanEstudios() {
        return this.planEstudios;
    }
    public void setPlanEstudios(PlanEstudios planEstudios) {
        this.planEstudios = planEstudios;
    }
    
    public int getGraId() {
        return this.graId;
    }    
    public void setGraId(int graId) {
        this.graId = graId;
    }
    
    public Grado getGrado() {
        return this.grado;
    }
    public void setGrado(Grado grado) {
        this.grado = grado;
    }
    
    
    public int getAreCurId() {
        return this.areCurId;
    }    
    public void setAreCurId(int areCurId) {
        this.areCurId = areCurId;
    }
    
    public AreaCurricular getArea() {
        return this.area;
    }
    public void setArea(AreaCurricular area) {
        this.area = area;
    }
    
    public char getSecId() {
        return this.secId;
    }    
    public void setSecId(char secId) {
        this.secId = secId;
    }
}


