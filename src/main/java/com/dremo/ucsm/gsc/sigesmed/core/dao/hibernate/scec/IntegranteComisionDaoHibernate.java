package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scec;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.IntegranteComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Rol;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.IntegranteComision;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Transaction;

/**
 * Created by geank on 30/09/16.
 */
public class IntegranteComisionDaoHibernate extends GenericDaoHibernate<IntegranteComision> implements IntegranteComisionDao{
    private static final Logger logger = Logger.getLogger(IntegranteComisionDaoHibernate.class.getName());
    @Override
    public List<IntegranteComision> buscarPorComision(int idCom) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(IntegranteComision.class);
            query.createCriteria("comision")
                    .add(Restrictions.eq("comId",idCom));

            List<IntegranteComision> integrantes = query.list();
            return integrantes;
        }catch (Exception e){
            logger.log(Level.SEVERE,"buscarPorComision",e);
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public IntegranteComision buscarPorComision(int idPersona, int idComision) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            IntegranteComision integranteComision = (IntegranteComision)session.get(IntegranteComision.class,new IntegranteComision.IntegranteId(idPersona,idComision));
            return integranteComision;
        }catch (Exception e){
            logger.log(Level.SEVERE,"buscarPorComision",e);
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public IntegranteComision buscarPorComisionConAsistencias(int idPersona, int idComision) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(IntegranteComision.class)
                    .setFetchMode("asistenciaReuniones", FetchMode.JOIN);
            query.createCriteria("comision")
                    .add(Restrictions.eq("comId",idComision));
            query.createCriteria("persona")
                    .add(Restrictions.eq("perId",idPersona));

            return (IntegranteComision)query.uniqueResult();

        }catch (Exception e){
            logger.log(Level.SEVERE,"buscarPorComisionConAsistencias",e);
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public void cambiarCargoComision(int nuevoCargo, int anteriorCargo) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            String sql1 = "UPDATE pedagogico.integrante_comision SET car_com_id =:ncar WHERE car_com_id = :acar";
            SQLQuery query = session.createSQLQuery(sql1);
            query.setInteger("ncar",nuevoCargo);
            query.setInteger("acar",anteriorCargo);
            query.executeUpdate();
            tx.commit();
        }catch (Exception e){
            tx.rollback();
            logger.log(Level.SEVERE,"actualizarCargoComision",e);
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public IntegranteComision buscarPresidenteComision(int idComision) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        
        try{
            String cargo = "PRESIDENTE";
            String hql = "SELECT i FROM IntegranteComision i INNER JOIN i.cargoComision c INNER JOIN i.comision co WHERE co.comId=:id and c.nomCar=:car";
            Query query = session.createQuery(hql);
            query.setInteger("id", idComision);
            query.setString("car", cargo);
            query.setMaxResults(1);
            return (IntegranteComision)query.uniqueResult();
        }catch(Exception e){
            throw  e;
        }
    }

    @Override
    public Rol buscarRolPorNombre(String nombreRol) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT r FROM Rol r where r.nom=:nom";
            Query query = session.createQuery(hql);
            query.setString("nom", nombreRol);
            query.setMaxResults(1);
            return (Rol)query.uniqueResult();
            
        }catch(Exception e){
        throw e;
        }
    }

}
