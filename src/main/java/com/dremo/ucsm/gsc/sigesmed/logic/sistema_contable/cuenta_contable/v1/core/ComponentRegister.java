/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.cuenta_contable.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.cuenta_contable.v1.tx.ActualizarCuentaContableTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.cuenta_contable.v1.tx.EliminarCuentaContableTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.cuenta_contable.v1.tx.InsertarCuentaContableTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.cuenta_contable.v1.tx.InsertarCuentaContableExcelTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.cuenta_contable.v1.tx.ListarCuentaContableTx;

/**
 *
 * @author ucsm
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_SISTEMA_CONTABLE_INSTITUCIONAL);        
        
        //Registrnado el Nombre del componente
        component.setName("cuentaContable");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionPOST("insertarCuentaContable", InsertarCuentaContableTx.class);
        component.addTransactionPOST("insertarCuentaContableExcel",InsertarCuentaContableExcelTx.class);
        component.addTransactionGET("listarCuentaContable", ListarCuentaContableTx.class);        
        component.addTransactionDELETE("eliminarCuentaContable", EliminarCuentaContableTx.class);
        component.addTransactionPUT("actualizarCuentaContable", ActualizarCuentaContableTx.class);
        
        return component;
    }
}


