/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.mnt;

import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name="mensaje", schema="pedagogico")
public class Mensaje implements java.io.Serializable{
    @Id
    @Column(name="men_id", unique=true, nullable=false)
    @SequenceGenerator(name="secuencia_men", sequenceName="pedagogico.mensaje_men_id_seq" )
    @GeneratedValue(generator="secuencia_men")
    private int mensajeID;
    
    @Column(name="men_asu", length=30)
    private String menAsu;
    
    @Column (name="men_ctn", length=500)
    private String menCtn;
    
    @Temporal (TemporalType.TIMESTAMP)
    @Column(name="men_fec_env", length=29, updatable=false, nullable=false)
    private Date menFecEnv;
    
    @Temporal (TemporalType.TIMESTAMP)
    @Column(name="men_fec_ocu", length=29, updatable=false, nullable=false)
    private Date menFecOcu;
    
    @Column(name="men_tip_men", length=1)
    private char menTipMen;
    
    @Column(name="men_est_aut", length=1)
    private char menEstAut;
    
     @Column(name="men_est_dest", length=1)
    private char menEstDest;
    
    @Column(name="msj_img_adj", length=50)
    private String menMsjImg;
    
    @OneToOne(fetch=FetchType.LAZY) 
    @JoinColumn(name="usu_ses_aut_id")
    private UsuarioSession usuAut;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="usu_ses_des_id")
    private UsuarioSession usuDest;
    
    
    @Column(name="est_reg_aut", length=1)
    private char estRegAut;
    
    @Column(name="est_reg_dest", length=1)
    private char estRegDest;

    public Mensaje(){}
    public Mensaje(int mensajeID) {
        this.mensajeID = mensajeID;
    }


    public Mensaje(int mensajeID, String menAsu, String menCtn, Date menFecEnv,Date menFecOcu, char menTipMen, char menEst, String menMsjImd, UsuarioSession usuAut, UsuarioSession usuDest, char menEstReg) {
        this.mensajeID = mensajeID;
        this.menAsu = menAsu;
        this.menCtn = menCtn;
        this.menFecEnv = menFecEnv;
        this.menTipMen = menTipMen;
        this.menEstAut = menEst;
        this.menEstDest = menEst;
        this.menMsjImg = menMsjImd;
        this.usuAut = usuAut;
        this.usuDest = usuDest;
        this.estRegAut = menEstReg;
        this.estRegDest = menEstReg;
        this.menFecOcu= menFecOcu;
    }
    public Mensaje(int mensajeID, String menAsu, String menCtn, Date menFecEnv, char menTipMen, char menEst, String menMsjImd, UsuarioSession usuAut, UsuarioSession usuDest, char menEstReg) {
        this.mensajeID = mensajeID;
        this.menAsu = menAsu;
        this.menCtn = menCtn;
        this.menFecEnv = menFecEnv;
        this.menTipMen = menTipMen;
        this.menEstAut = menEst;
        this.menEstDest = menEst;
        this.menMsjImg = menMsjImd;
        this.usuAut = usuAut;
        this.usuDest = usuDest;
        this.estRegAut = menEstReg;
        this.estRegDest = menEstReg;
       
    }

    public int getMensajeID() {
        return mensajeID;
    }

    public void setMensajeID(int mensajeID) {
        this.mensajeID = mensajeID;
    }

    public String getMenAsu() {
        return menAsu;
    }

    public void setMenAsu(String menAsu) {
        this.menAsu = menAsu;
    }

    public String getMenCtn() {
        return menCtn;
    }

    public void setMenCtn(String menCtn) {
        this.menCtn = menCtn;
    }

    public Date getMenFecEnv() {
        return menFecEnv;
    }

    public void setMenFecEnv(Date menFecEnv) {
        this.menFecEnv = menFecEnv;
    }

    public char getMenTipMen() {
        return menTipMen;
    }

    public void setMenTipMen(char menTipMen) {
        this.menTipMen = menTipMen;
    }

    public char getMenEstAut() {
        return menEstAut;
    }

    public void setMenEstAut(char menEst) {
        this.menEstAut = menEst;
    }
    public char getMenEstDest() {
        return menEstDest;
    }

    public void setMenEstDest(char menEst) {
        this.menEstDest = menEst;
    }

    public String getMenMsjImg() {
        return menMsjImg;
    }

    public void setMenMsjImg(String menMsjImd) {
        this.menMsjImg = menMsjImd;
    }

    public UsuarioSession getUsuAut() {
        return usuAut;
    }

    public void setUsuAut(UsuarioSession usuAut) {
        this.usuAut = usuAut;
    }

    public UsuarioSession getUsuDest() {
        return usuDest;
    }

    public void setUsuDest(UsuarioSession usuDest) {
        this.usuDest = usuDest;
    }

    public char getMenEstRegAut() {
        return estRegAut;
    }

    public void setMenEstRegAut(char menEstReg) {
        this.estRegAut = menEstReg;
    }
      public char getMenEstRegDest() {
        return estRegDest;
    }

    public void setMenEstRegDest(char menEstReg) {
        this.estRegDest = menEstReg;
    }

    public Date getMenFecOcu() {
        return menFecOcu;
    }

    public void setMenFecOcu(Date menFecOcu) {
        this.menFecOcu = menFecOcu;
    }

    public char getEstRegAut() {
        return estRegAut;
    }

    public void setEstRegAut(char estRegAut) {
        this.estRegAut = estRegAut;
    }

    public char getEstRegDest() {
        return estRegDest;
    }

    public void setEstRegDest(char estRegDest) {
        this.estRegDest = estRegDest;
    }
    
}
