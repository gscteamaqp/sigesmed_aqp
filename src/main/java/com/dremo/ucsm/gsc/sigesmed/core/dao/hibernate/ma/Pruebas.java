/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.ma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.NotaEvaluacionIndicadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.NotaIndicador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.IndicadorAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.IndicadoresSesionAprendizaje;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 *
 */
public class Pruebas {

    public static void main(String[] args) {
        
        NotaEvaluacionIndicadorDao notaDao = (NotaEvaluacionIndicadorDao) FactoryDao.buildDao("ma.NotaEvaluacionIndicadorDao");
        JSONArray indicadores = notaDao.buscarIndicadoresxSesionxEstudiante(1, 21, 'B', 1);
        IndicadoresSesionAprendizaje indicadorSesApr  = notaDao.obtenerCompetenciaDeIndicador(32);
        List<IndicadoresSesionAprendizaje> indicadoresSesApr  = notaDao.obtenerIndicadoresDeCompetencia(6);
        
        /*for(int i=0; i< indicadores.length(); i++){
            JSONArray notas = notaDao.buscarNotasxIndicadorxEstudiante(indicadores.getJSONObject(i).getInt("indId"), 21, 'B', 1);
            
            Double promedio = 0.0;
            for(int j=0; j< notas.length(); j++){
                promedio += notas.getJSONObject(i).getInt("nota");
            }
            promedio = promedio/notas.length();
            indicadores.getJSONObject(i).put("promedio", promedio);
        }*/
        /*String numero = "14.5";
        System.out.println(Double.parseDouble(numero));*/
        Double nota = 14.5;
        System.out.println(Math.round(nota));
        
        
    }
}
 