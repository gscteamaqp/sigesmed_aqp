package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.JustificacionInasistenciaCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.JustificacionInasistenciaCapacitacion;

public class JustificacionInasistenciaCapacitacionDaoHibernate extends GenericDaoHibernate<JustificacionInasistenciaCapacitacion> implements JustificacionInasistenciaCapacitacionDao {
    
}
