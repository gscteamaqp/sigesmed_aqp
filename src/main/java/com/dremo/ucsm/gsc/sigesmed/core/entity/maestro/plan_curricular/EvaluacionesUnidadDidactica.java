package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrador on 03/01/2017.
 */
@Entity
@Table(name = "evaluaciones_unidad_didactica",schema = "pedagogico")
public class EvaluacionesUnidadDidactica implements java.io.Serializable {
    @Id
    @Column(name = "eva_uni_did_id", nullable = false,unique = true)
    @SequenceGenerator(name = "evaluaciones_unidad_didactica_eva_uni_did_id_seq",sequenceName = "pedagogico.evaluaciones_unidad_didactica_eva_uni_did_id_seq")
    @GeneratedValue(generator = "evaluaciones_unidad_didactica_eva_uni_did_id_seq")
    private int evaUniDidId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "uni_did_id",nullable = false , insertable = false , updatable = false)
    private UnidadDidactica unidad;

    @Column(name="tip_eva")
    private Integer tipEva;
    
    @Column(name="uni_did_id")
    private int uni_did_id;
    

    @Column(name = "des")
    private String des;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_eva")
    private Date fecEva;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "est_reg",length = 1)
    private Character estReg;
    
    @Column(name= "nom_eva")
    private String nom_eva;
    
    @Column(name="sec")
    private char sec;
    
    

    public void setUni_did_id(int uni_did_id) {
        this.uni_did_id = uni_did_id;
    }

    public int getUni_did_id() {
        return uni_did_id;
    }
    
    
    

    public EvaluacionesUnidadDidactica() {
    }

    public EvaluacionesUnidadDidactica(String des, Date fecEva, Integer tipEva) {
        this.des = des;
        this.fecEva = fecEva;
        this.tipEva = tipEva;

        this.estReg = 'A';
        this.fecMod = new Date();
    }

    public void setNom_eva(String nom_eva) {
        this.nom_eva = nom_eva;
    }

    public void setSec(char sec) {
        this.sec = sec;
    }

    public String getNom_eva() {
        return nom_eva;
    }

    public char getSec() {
        return sec;
    }
    
    

    public int getEvaUniDidId() {
        return evaUniDidId;
    }

    public void setEvaUniDidId(int evaUniDidId) {
        this.evaUniDidId = evaUniDidId;
    }

    public UnidadDidactica getUnidad() {
        return unidad;
    }

    public void setUnidad(UnidadDidactica unidad) {
        this.unidad = unidad;
    }

    public Integer getTipEva() {
        return tipEva;
    }

    public void setTipEva(Integer tipEva) {
        this.tipEva = tipEva;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public Date getFecEva() {
        return fecEva;
    }

    public void setFecEva(Date fecEva) {
        this.fecEva = fecEva;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

  
    
}
