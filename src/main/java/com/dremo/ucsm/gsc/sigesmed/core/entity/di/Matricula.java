/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.di;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "matricula", schema="pedagogico")
//@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "Matricula.findAll", query = "SELECT m FROM Matricula m"),
//    @NamedQuery(name = "Matricula.findByMatId", query = "SELECT m FROM Matricula m WHERE m.matId = :matId"),
//    @NamedQuery(name = "Matricula.findByApoId", query = "SELECT m FROM Matricula m WHERE m.apoId = :apoId"),
//    @NamedQuery(name = "Matricula.findByEstId", query = "SELECT m FROM Matricula m WHERE m.estId = :estId"),
//    @NamedQuery(name = "Matricula.findByEstPerId", query = "SELECT m FROM Matricula m WHERE m.estPerId = :estPerId"),
//    @NamedQuery(name = "Matricula.findByOrgId", query = "SELECT m FROM Matricula m WHERE m.orgId = :orgId")})
public class Matricula implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "mat_id")
    private Long matId;
//    @Column(name = "apo_id")
//    private BigInteger apoId;
//    @Column(name = "est_id")
//    private BigInteger estId;
//    @Column(name = "est_per_id")
//    private BigInteger estPerId;
    @Column(name = "org_des_id")
    private Integer orgId;
    
    @JoinColumn(name = "apo_id", referencedColumnName = "apo_id")
    @ManyToOne(fetch=FetchType.LAZY)
    private Apoderado apoderado;
    
//    @JoinColumn(name = "est_id", referencedColumnName = "est_id")
//    @ManyToOne(fetch=FetchType.LAZY)
//    
//    @JoinColumns({
//                @JoinColumn(name="est_id",
//                referencedColumnName="est_id"),
//                @JoinColumn(name="est_per_id",
//                referencedColumnName="per_id")
//                })
//    @ManyToOne(fetch=FetchType.LAZY)
//    private Estudiante estudiante;
    
    @JoinColumn(name = "est_id", referencedColumnName = "per_id")
    @ManyToOne(fetch=FetchType.LAZY)
    private Estudiante estudiante;
    
    
    @JoinColumn(name = "org_des_id", referencedColumnName = "org_id", insertable = false, updatable = false)
    @ManyToOne(fetch=FetchType.LAZY)
    private Organizacion organizacion;

    public Matricula() {
    }

    public Matricula(Long matId) {
        this.matId = matId;
    }

    public Long getMatId() {
        return matId;
    }

    public void setMatId(Long matId) {
        this.matId = matId;
    }

//    public BigInteger getApoId() {
//        return apoId;
//    }
//
//    public void setApoId(BigInteger apoId) {
//        this.apoId = apoId;
//    }
//
//    public BigInteger getEstId() {
//        return estId;
//    }
//
//    public void setEstId(BigInteger estId) {
//        this.estId = estId;
//    }

//    public BigInteger getEstPerId() {
//        return estPerId;
//    }
//
//    public void setEstPerId(BigInteger estPerId) {
//        this.estPerId = estPerId;
//    }

    public Integer getOrgId() {
        return orgId;
    }

    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (matId != null ? matId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula)) {
            return false;
        }
        Matricula other = (Matricula) object;
        if ((this.matId == null && other.matId != null) || (this.matId != null && !this.matId.equals(other.matId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Matricula[ matId=" + matId + " ]";
    }

    public Apoderado getApoderado() {
        return apoderado;
    }

    public void setApoderado(Apoderado apoderado) {
        this.apoderado = apoderado;
    }

    public Estudiante getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(Estudiante estudiante) {
        this.estudiante = estudiante;
    }

    public Organizacion getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(Organizacion organizacion) {
        this.organizacion = organizacion;
    }
    
}
