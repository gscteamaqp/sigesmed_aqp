package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.agregar_estudiante.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.PersonaMMIDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.PersonaMMI;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

public class BuscarPersonaTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        PersonaMMIDaoHibernate dh = new PersonaMMIDaoHibernate();
        PersonaMMI personaMMI;
        JSONObject requestData = (JSONObject) wr.getData();
        JSONObject persona = new JSONObject();

        String codigoBusqueda = requestData.getString("codigoBusqueda");

        try {
            personaMMI = dh.find4Dni(codigoBusqueda);
            if (personaMMI != null) {
                if ((Long) personaMMI.getPerId() != null) {
                    persona.put("perId", personaMMI.getPerId());
                }
                if (personaMMI.getNom() != null) {
                    persona.put("nom", personaMMI.getNom());
                }
                if (personaMMI.getApePat() != null) {
                    persona.put("apePat", personaMMI.getApePat());
                }
                if (personaMMI.getApeMat() != null) {
                    persona.put("apeMat", personaMMI.getApeMat());
                }
                if (personaMMI.getDni() != null) {
                    persona.put("dni", personaMMI.getDni());
                }
                if (personaMMI.getNum1() != null) {
                    persona.put("num1", personaMMI.getNum1());
                }
                return WebResponse.crearWebResponseExito("Se encontro a la persona", persona);

            } else {
                return WebResponse.crearWebResponseError("No se encontro a la persona! ", persona);
            }

        } catch (Exception e1) {
            return WebResponse.crearWebResponseError("No se encontro a la persona! ", e1);
        }

    }

}
