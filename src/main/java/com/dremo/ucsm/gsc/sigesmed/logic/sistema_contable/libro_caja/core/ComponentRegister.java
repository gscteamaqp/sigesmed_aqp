/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.ActualizarLibroCajaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.ActualizarLibroYSaldosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.EliminarLibroCajaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.EstadoLibroAnteriorTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.EstadoLibroCajaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.InsertarAsientoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.InsertarLibroCajaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.InsertarTesoreroTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.InsertarCompraTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.InsertarCompraReciboAuxiliarTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.InsertarVentaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.ListarBancosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.ListarClienteProveedorTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.ListarCuentasEfectivoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.ListarLibroCajaConTesorerosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.ListarLibrosCajaPorUGELTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.ListarTipoPagoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.ListarTrabajadorPorOrganizacionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.ListarTransaccionesConDocumentoPorOrganizacionTX;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.ReporteBalanceGeneralTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.ListarCuentaContableTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.ListarEgresosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.ListarEgresosIngresosPorIntTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.ListarMesEgresosIngresosIIEETx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.ListarControlFechaCierreCajaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.ListarHitorialCajaPorIIEETx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.ReporteIngresosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.ReporteLibroCajaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.ReporteLibroTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.SaldoLibroTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.SaldosDelLibroTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.VerificarTesoreroTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.pruebasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.ImprimirReporteCuentConTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.ImprimirReporte2ImgTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.ImpReciboAuxiliarTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.ActualizarComprasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.ActualizarVentasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.ActualizarAsientoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.EliminarRegistroTransaccionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1.ReporteEgresosTx;

/**
 *
 * @author ucsm
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_SISTEMA_CONTABLE_INSTITUCIONAL);        
        
        //Registrnado el Nombre del componente
        component.setName("libroCaja");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionPOST("insertarLibroCaja", InsertarLibroCajaTx.class);
        component.addTransactionPOST("insertarTesorero", InsertarTesoreroTx.class);
        component.addTransactionGET("estadoLibroCaja", EstadoLibroCajaTx.class);    
        component.addTransactionGET("saldosDelLibro", SaldosDelLibroTx.class);
        component.addTransactionGET("saldoLibro", SaldoLibroTx.class);

        component.addTransactionPUT("actualizarLibroYSaldos", ActualizarLibroYSaldosTx.class);

         component.addTransactionGET("estadoLibroAnterior", EstadoLibroAnteriorTx.class); 
         component.addTransactionGET("verificarTesorero", VerificarTesoreroTx.class);  
        component.addTransactionGET("listarTrabajador", ListarTrabajadorPorOrganizacionTx.class);   
        component.addTransactionGET("listarLibroCajaConTesoreros", ListarLibroCajaConTesorerosTx.class);  
        component.addTransactionGET("listarClienteProveedor", ListarClienteProveedorTx.class);  
        component.addTransactionGET("listarTipoPago", ListarTipoPagoTx.class);  
         component.addTransactionGET("listarCuentasEfectivo",ListarCuentasEfectivoTx.class); 
          component.addTransactionGET("listarBancos",ListarBancosTx.class); 
        component.addTransactionGET("listarTransaccionesConDocumentosPorOrganizacion",ListarTransaccionesConDocumentoPorOrganizacionTX.class);  
         component.addTransactionGET("listarLibrosCajaPorUGEL",ListarLibrosCajaPorUGELTx.class);  
         
         component.addTransactionGET("listarEgresos",ListarEgresosTx.class); 
         component.addTransactionGET("listarCuentaContable",ListarCuentaContableTx.class);
         component.addTransactionGET("listarEgresosIngresosPorInt",ListarEgresosIngresosPorIntTx.class);
         component.addTransactionGET("listarMesEgresosIngresosIIEE",ListarMesEgresosIngresosIIEETx.class);
         component.addTransactionGET("listarControlFechaCierreCaja",ListarControlFechaCierreCajaTx.class);
        component.addTransactionGET("listarHitorialCajaPorIIEETx",ListarHitorialCajaPorIIEETx.class);
                
        component.addTransactionPOST("imprimirReporteCuentCon",ImprimirReporteCuentConTx.class);
        component.addTransactionPOST("imprimirReporte2Img",ImprimirReporte2ImgTx.class);
        component.addTransactionPOST("impReciboAuxiliar", ImpReciboAuxiliarTx.class);

        
        component.addTransactionGET("reporteLibro",ReporteLibroTx.class);        
        component.addTransactionGET("pruebas",pruebasTx.class);  

        component.addTransactionPOST("reporteLibroCaja",ReporteLibroCajaTx.class);  
        component.addTransactionPOST("reporteIngresos",ReporteIngresosTx.class); 
        component.addTransactionPOST("reporteEgresos",ReporteEgresosTx.class); 
        component.addTransactionPOST("reporteBalanceGeneral",ReporteBalanceGeneralTx.class); 
        
        component.addTransactionPOST("insertarCompra", InsertarCompraTx.class);
        component.addTransactionPOST("insertarCompraReciboAuxiliar", InsertarCompraReciboAuxiliarTx.class);
        component.addTransactionPOST("insertarVenta", InsertarVentaTx.class);
        component.addTransactionPOST("insertarAsiento", InsertarAsientoTx.class);
        
     //   component.addTransactionDELETE("eliminarLibroCaja", EliminarLibroCajaTx.class);
    //    component.addTransactionPUT("actualizarLibroCaja", ActualizarLibroCajaTx.class);
        component.addTransactionPUT("actualizarCompras", ActualizarComprasTx.class);
        component.addTransactionPUT("actualizarVentas", ActualizarVentasTx.class);
        component.addTransactionPUT("actualizarAsiento", ActualizarAsientoTx.class);
       
        
        component.addTransactionDELETE("eliminarRegistroTransaccion", EliminarRegistroTransaccionTx.class);

        return component;
    }
}

