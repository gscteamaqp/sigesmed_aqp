/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.util;

import com.itextpdf.kernel.pdf.tagutils.AccessibilityProperties;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.property.HorizontalAlignment;
import com.itextpdf.layout.property.TextAlignment;

/**
 *
 * @author Administrador
 */
public class GCell extends Cell{
    private final TextAlignment left= TextAlignment.LEFT ;
    private final TextAlignment center= TextAlignment.CENTER ;
    private final TextAlignment right= TextAlignment.RIGHT ;
       
    private int row;
    private int col;
    private int rowspan;
    private int colspan;
    private boolean isBold=false;
    private String pos="center";

    public GCell(int rowspan, int colspan,String pos) {
        super(rowspan, colspan);
        this.pos=pos;
    }
    
    public GCell(int rowspan, int colspan,String pos,boolean isBold) {
        super(rowspan, colspan);
        this.isBold=isBold;
        this.pos=pos;
    }
    
    public GCell(int rowspan, int colspan) {
        super(rowspan, colspan);      
    }
    
    public GCell(int rowspan, int colspan,boolean isBold) {
        super(rowspan, colspan);   
        this.isBold=isBold;
    }

    public GCell() {
    }

    
     public TextAlignment getTextAlignment() {
         TextAlignment a=center;        
        switch (this.pos) {
            case "left":        
                a=left;
                break;
            case "right":
                a=right;
                break;
        }
          return a;
         
     }   
     
     public TextAlignment setTextAlignment() {
         TextAlignment a=center;        
        switch (this.pos) {
            case "left":        
                a=left;
                break;
            case "right":
                a=right;
                break;
        }
          return a;
         
     } 
            
    
     public Cell createCellLeft(int rowspan ,int colspan) {
        Cell cell = new Cell(rowspan, colspan);
        cell.setTextAlignment(left);
       
        return cell;
    }
    public Cell createCellCenter( int rowspan ,int colspan) {
        Cell cell = new Cell(rowspan, colspan);
        cell.setTextAlignment(center);
        return cell;
    }
    public Cell createCellRight(int rowspan ,int colspan) {
        Cell cell = new Cell(rowspan, colspan);
        cell.setTextAlignment(right);
        return cell;
    }

    public boolean isIsBold() {
        return isBold;
    }

    public void setIsBold(boolean isBold) {
        this.isBold = isBold;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }
    
    
}
