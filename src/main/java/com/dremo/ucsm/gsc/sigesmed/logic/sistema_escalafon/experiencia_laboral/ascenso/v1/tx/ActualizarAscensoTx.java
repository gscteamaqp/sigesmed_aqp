/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.ascenso.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.AscensoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Ascenso;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Yemi
 */
public class ActualizarAscensoTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ActualizarAscensoTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try{
     
            DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
            
            JSONObject requestData = (JSONObject)wr.getData();
        
            Integer ascId = requestData.getInt("ascId");
            String numRes = requestData.getString("numRes");
            Date fecRes = sdi.parse(requestData.getString("fecRes").substring(0, 10));
            Date fecEfe = sdi.parse(requestData.getString("fecEfe").substring(0, 10));
            String esc = requestData.getString("esc");       
            
            return actualizarAscenso(ascId, numRes, fecRes, fecEfe, esc);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar ascenso",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    
    private WebResponse actualizarAscenso(Integer ascId, String numRes, Date fecRes, Date fecEfe, String esc) {
        try{
            AscensoDao ascensoDao = (AscensoDao)FactoryDao.buildDao("se.AscensoDao");        
            Ascenso ascenso = ascensoDao.buscarPorId(ascId);

            ascenso.setNumRes(numRes);
            ascenso.setFecRes(fecRes);
            ascenso.setFecEfe(fecEfe);
            ascenso.setEsc(esc);
            
            ascensoDao.update(ascenso);

            JSONObject oResponse = new JSONObject();
            DateFormat sdo = new SimpleDateFormat("yyyy-MM-dd");
            oResponse.put("ascId", ascenso.getAscId());
            oResponse.put("numRes", ascenso.getNumRes());
            oResponse.put("fecRes", sdo.format(ascenso.getFecRes()));
            oResponse.put("fecEfe", sdo.format(ascenso.getFecEfe()));
            oResponse.put("esc", ascenso.getEsc());
            return WebResponse.crearWebResponseExito("Ascenso actualizado exitosamente",oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizarAscenso",e);
            return WebResponse.crearWebResponseError("Error, el ascenso no fue actualizado");
        }
    } 
    
}
