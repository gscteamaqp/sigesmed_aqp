package com.dremo.ucsm.gsc.sigesmed.logic.maestro.curriculabanco.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.AreaCurricularDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 19/10/2016.
 */
public class ListarAreasCurricularesTx implements ITransaction{
    private static final Logger logger = Logger.getLogger( ListarAreasCurricularesTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            JSONObject data = (JSONObject) wr.getData();

            AreaCurricularDao areaDao = (AreaCurricularDao) FactoryDao.buildDao("maestro.plan.AreaCurricularDao");
            List<AreaCurricular> areas = data.optBoolean("all",true) ? areaDao.buscarAreasConCurricula() : areaDao.buscarTodos(AreaCurricular.class);
            JSONArray jsonAreas = new JSONArray();
            for(AreaCurricular area : areas){
                JSONObject obj = new JSONObject(EntityUtil.objectToJSONString(
                        new String[]{"areCurId","abr","nom","des","estReg"},
                        new String[]{"cod","abr","nom","des","est"},
                        area
                ));
                if (data.optBoolean("all",true)) obj.put("curr",area.getDisenoCurr().getNom());
                jsonAreas.put(obj);
            }
            return WebResponse.crearWebResponseExito("Se listo correctamente",jsonAreas);
        }catch (Exception e){
            logger.log(Level.SEVERE, "execute", e);
            return WebResponse.crearWebResponseError("Error al listar", e.getMessage());
        }
    }
}
