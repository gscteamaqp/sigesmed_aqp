package com.dremo.ucsm.gsc.sigesmed.logic.maestro.sesion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.SesionAprendizajeDao;

import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.SesionAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 15/12/2016.
 */
public class EliminarSesionAprendizajeTx implements ITransaction{
    private static Logger logger = Logger.getLogger( EliminarSesionAprendizajeTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idSesion= data.getInt("id");
        return eliminarSesionAprendizaje(idSesion);
    }

    private WebResponse eliminarSesionAprendizaje(int idSesion) {
        try{
            SesionAprendizajeDao sesionDao = (SesionAprendizajeDao) FactoryDao.buildDao("maestro.plan.SesionAprendizajeDao");
            SesionAprendizaje sesion = sesionDao.buscarSesionById(idSesion);
            sesionDao.deleteAbsolute(sesion);
            return WebResponse.crearWebResponseExito("Exito al eliminar la sesion");
        }catch (Exception e){
            logger.log(Level.SEVERE,"eliminarSesionAprendizaje",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar la sesion");
        }
    }
}
