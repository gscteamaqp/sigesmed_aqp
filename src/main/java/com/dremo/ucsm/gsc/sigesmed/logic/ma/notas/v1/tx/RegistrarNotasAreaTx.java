package com.dremo.ucsm.gsc.sigesmed.logic.ma.notas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.RegistroAuxiliarDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.DocenteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.CompetenciaAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.IndicadorAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.HistoricoNotasEstudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.RegistroAuxiliar;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.RegistroAuxiliarCompetencia;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 27/01/2017.
 */
public class RegistrarNotasAreaTx implements ITransaction{
    private static Logger logger = Logger.getLogger(RegistrarNotasRegistroAuxiliarTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idPeriodo = data.getInt("per");
        char tipNot = data.optString("tip", "N").charAt(0);
        int idUsr = data.getInt("usr");
        int idArea = data.getInt("are");
        JSONArray array = data.optJSONArray("estudiantes");
        if(array == null) return WebResponse.crearWebResponseError("Se tiene que seleccionar a los alumnos");
        return registrarNotas(idPeriodo,idUsr,idArea,tipNot,array);
    }

    private WebResponse registrarNotas(int idPeriodo, int idUsr, int idArea ,char tipNot, JSONArray array) {
        try{
            RegistroAuxiliarDao regisDao = (RegistroAuxiliarDao) FactoryDao.buildDao("ma.RegistroAuxiliarDao");
            IndicadorAprendizajeDao indicadorDao = (IndicadorAprendizajeDao) FactoryDao.buildDao("maestro.plan.IndicadorAprendizajeDao");
            CompetenciaAprendizajeDao compDao = (CompetenciaAprendizajeDao) FactoryDao.buildDao("maestro.plan.CompetenciaAprendizajeDao");
            DocenteDao docDao = (DocenteDao) FactoryDao.buildDao("maestro.DocenteDao");
            EstudianteDao estDao = (EstudianteDao) FactoryDao.buildDao("maestro.EstudianteDao");
            
            String mensaje = "";
            for(int i = 0; i < array.length(); i++){
                JSONObject alumno = array.getJSONObject(i);
                JSONArray notas = alumno.getJSONArray("notas");
                int idAlum = alumno.getInt("idgra");
                for(int j=0; j < notas.length(); j++){
                    JSONObject nota = notas.getJSONObject(j);
                    int idComp = nota.optInt("comp");
                    String not = nota.optString("not", tipNot == 'L' ? "C":"0");
                    if(!regisDao.buscarNotaFinalCompetencia(idComp, idArea, idPeriodo, idAlum)){
                        RegistroAuxiliarCompetencia regAuxComp = new RegistroAuxiliarCompetencia(not);
                        regAuxComp.setComp(compDao.buscarCompetenciPorCodigo(idComp));
                        regAuxComp.setPeriodoPlanEstudios(regisDao.buscarPeriodoPorId(idPeriodo));
                        regAuxComp.setAreaCurricular(docDao.buscarAreaPorId(idArea));
                        regAuxComp.setGrad_ie_est_id(estDao.buscarGradoIEEstudiante(idAlum).getGraOrgEstId());
                        regisDao.registrarNotaCompetencia(regAuxComp);
                        mensaje = "Se registro correctamente";
                    }else {
                        RegistroAuxiliarCompetencia regAux = regisDao.buscarNotaCompetenciaEsp(idComp, idArea, idPeriodo, idAlum);
                        regAux.setNota(not);
                        regAux.setFecMod(new Date());
                        regisDao.registrarNotaCompetencia(regAux);
                        mensaje = "Se actualizo correctamente";
                    }
                }
                if(!alumno.optString("notAre").isEmpty()){
                    HistoricoNotasEstudiante historico = regisDao.buscaHistoricoNotasEstudiante(idArea,idPeriodo,idAlum);
                    if(historico == null){
                        historico = new HistoricoNotasEstudiante();
                        historico.setAreaCurricular(docDao.buscarAreaPorId(idArea));
                        historico.setGradoEst(estDao.buscarGradoIEEstudiante(idAlum));
                        historico.setPeriodos(regisDao.buscarPeriodoPorId(idPeriodo));
                    }
                    historico.setNota(alumno.getString("notAre"));
                    regisDao.guardarActualizarNotaArea(historico);
                }


            }
            return WebResponse.crearWebResponseExito(mensaje);
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrar nota",e);
            return WebResponse.crearWebResponseError("No se pudo registrar los datos");
        }
    }
}
