/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.programacion_monitoreo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.DateUtil;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Yemi
 */
public class ReporteSMATx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ReporteSMATx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {

        JSONObject requestData = (JSONObject) wr.getData();
        String orgNom = requestData.optString("orgNom");
        FileJsonObject miGrafico = new FileJsonObject(requestData.optJSONObject("grafico"));
        
        return crearReporte(miGrafico, orgNom);
    }

    private WebResponse crearReporte(FileJsonObject grafico, String organizacionNom) {
        try{
            String data = crearArchivo(grafico, organizacionNom, new Date());
            return WebResponse.crearWebResponseExito("", new JSONObject().put("reporte",data));
        }catch (Exception e){
            logger.log(Level.SEVERE,"crearReporteEstadistico",e);
            return WebResponse.crearWebResponseError("No se puede crear el reporte");
        }
    }
    private String  crearArchivo(FileJsonObject grafico, String organizacionNom, Date fecha) throws Exception{
        
        Mitext mitext = new Mitext(true);
        
        mitext.agregarParrafo("");
        float[] columnWidthsD = {3, 6, 5};
        Table tabla = new Table(columnWidthsD);
        tabla.setWidthPercent(100);
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph("ORGANIZACION").setFontSize(11).setTextAlignment(TextAlignment.LEFT)));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER).add(new Paragraph(" : " + organizacionNom).setFontSize(11)).setTextAlignment(TextAlignment.LEFT));
        tabla.addCell(new Cell(1,1).setBorder(Border.NO_BORDER));
       
        mitext.agregarTabla(tabla);
        mitext.agregarParrafo("");
        mitext.agregarTitulo(grafico.getName());
        mitext.agregarParrafo("");
        mitext.agregarImagen64(grafico.getData());

        mitext.cerrarDocumento();
        return mitext.encodeToBase64();
    }
}
