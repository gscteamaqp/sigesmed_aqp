/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.scp;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import java.util.Date;
import java.util.List;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.Ambientes;
/**
 *
 * @author Administrador
 */
public interface AmbientesDAO extends GenericDao<Ambientes>{
    
    public List<Ambientes> listarAmbientes(int org_id);
    public void eliminarAmbiente(int amb_id);
    public Ambientes buscarAmbientePorId(int ambId);
    public List<Ambientes> buscarAmbientesDisponibles(int orgId, Date horaInicio, Date horaFin, String diaSem);
}


