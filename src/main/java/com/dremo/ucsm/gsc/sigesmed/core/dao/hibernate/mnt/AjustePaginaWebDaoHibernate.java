/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mnt;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mnt.AjustePaginaWebDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mnt.AjustePaginaWeb;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Administrador
 */
public class AjustePaginaWebDaoHibernate extends GenericDaoHibernate<AjustePaginaWeb> implements AjustePaginaWebDao{

    @Override
    public AjustePaginaWeb obtenerAjustePorIdUsuario(Usuario usuario) {
        Session session=HibernateUtil.getSessionFactory().openSession();
        AjustePaginaWeb ajuste=null;
        try{
            String hql="SELECT a FROM AjustePaginaWeb a WHERE a.usuario=:p1 and a.estReg='A'";
            Query q=session.createQuery(hql);
            q.setParameter("p1", usuario);
            q.setMaxResults(1);
            ajuste=(AjustePaginaWeb)q.uniqueResult();
        }catch(Exception e){
            System.out.println("No se pudo obtener el ajuste "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo obtener el ajuste "+ e.getMessage());
        }finally{
            session.close();
        }
        return ajuste;
    }
    
}
