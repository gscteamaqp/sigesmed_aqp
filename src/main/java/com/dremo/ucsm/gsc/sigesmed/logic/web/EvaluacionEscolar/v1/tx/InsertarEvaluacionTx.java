/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.EvaluacionEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.EvaluacionEscolar;
import java.util.Date;
/**
 *
 * @author abel
 */
public class InsertarEvaluacionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */        
        
        EvaluacionEscolar nuevaEvaluacion = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            String nombre = requestData.getString("nombre");
            String descripcion = requestData.optString("descripcion");
            boolean calificacion = requestData.getBoolean("calificacion");
            boolean automatico = requestData.getBoolean("automatico");
            boolean aleatorio = requestData.getBoolean("aleatorio");
            int intentos = requestData.getInt("intentos");
            
            int planID = requestData.getInt("planID");
            int gradoID = requestData.getInt("gradoID");
            char seccionID = requestData.getString("seccionID").charAt(0);
            int areaID = requestData.getInt("areaID");            
            
            int docenteID = requestData.getInt("docenteID");
            
            nuevaEvaluacion = new EvaluacionEscolar(0,nombre,descripcion,null,null,null,calificacion,automatico,aleatorio,intentos,Evaluacion.ESTADO_NUEVO,planID,gradoID,seccionID,areaID,new Date(),docenteID,'A');
            
                      
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar la evalucion, datos incorrectos", e.getMessage() );
        }
        //Fin       
        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        EvaluacionEscolarDao evalucionDao = (EvaluacionEscolarDao)FactoryDao.buildDao("web.EvaluacionEscolarDao");
        try{
            evalucionDao.insert(nuevaEvaluacion);
            
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar la evalucion", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("evaluacionID",nuevaEvaluacion.getEvaEscId());
        oResponse.put("estado",""+nuevaEvaluacion.getEstado());
        return WebResponse.crearWebResponseExito("El registro de la evalucion se realizo correctamente", oResponse);
        //Fin
    }    
    
    
}
