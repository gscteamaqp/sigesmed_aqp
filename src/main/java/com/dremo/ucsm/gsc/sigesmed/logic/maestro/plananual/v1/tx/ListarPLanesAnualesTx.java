package com.dremo.ucsm.gsc.sigesmed.logic.maestro.plananual.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.maestro.TestMaesto;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.DocenteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.ProgramacionAnualDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.ProgramacionAnual;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by geank on 23/10/16.
 */
public class ListarPLanesAnualesTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(ListarPLanesAnualesTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        //verificacion de usuarios

        int codOrg = data.getInt("org");
        int codUser = data.getInt("usr");

        return listarCurriculas(codOrg,codUser);
    }
    private WebResponse listarCurriculas(int codOrg, int codUser){
        try{
            //verificaremos el tipo de organizacion
            OrganizacionDao orgDao = (OrganizacionDao) FactoryDao.buildDao("OrganizacionDao");
            ProgramacionAnualDao progDao = (ProgramacionAnualDao) FactoryDao.buildDao("maestro.plan.ProgramacionAnualDao");

            Organizacion org = orgDao.buscarConTipoOrganizacionYPadre(codOrg);
            String tipo = org.getTipoOrganizacion().getCod().trim()
                    .toUpperCase();
            JSONObject response = new JSONObject();
            List<ProgramacionAnual> progs;
            switch (tipo){
                case "*":
                    progs = progDao.buscarTodos(ProgramacionAnual.class);
                    response.put("list",crearJSONArray(progs));
                    response.put("edit",false);
                    return WebResponse.crearWebResponseExito("Se listo con exito las programaciones anuales",response);
                case "IE":
                    //verificamos si es docente
                    DocenteDao docDao = (DocenteDao) FactoryDao.buildDao("maestro.DocenteDao");
                    Docente docente= docDao.buscarDocentePorUsuario(codUser);
                    if(docente != null){
                        progs = progDao.buscarProgramacionDocente(docente.getDoc_id());
                        response.put("list",crearJSONArray(progs));
                        response.put("edit",true);
                        return WebResponse.crearWebResponseExito("Se listo con exito las programaciones anuales",response);
                    }else {
                        return WebResponse.crearWebResponseError("El usuario debe ser un docente",WebResponse.BAD_RESPONSE);
                    }
                case "UGEL":
                case "DRE":
                default: return WebResponse.crearWebResponseError("Usuario no permitido",WebResponse.BAD_RESPONSE);

            }
        }catch (Exception e){
            logger.log(Level.SEVERE,"listar curriculas",e);
            return WebResponse.crearWebResponseError("Error al listar datos");
        }
    }
    private JSONArray crearJSONArray(List<ProgramacionAnual> progAnuals){
        JSONArray progsArray = new JSONArray();
        for(ProgramacionAnual prog : progAnuals){
            JSONObject obj = new JSONObject(EntityUtil.objectToJSONString(
                    new String[]{"proAnuId","des","fechProAnu"},
                    new String[]{"cod","nom","fec"},
                    prog
            )).put("are",new JSONObject(EntityUtil.objectToJSONString(
                    new String[]{"areCurId","nom"},
                    new String[]{"id","nom"},
                    prog.getAre()
            ))).put("grad",new JSONObject(EntityUtil.objectToJSONString(
                    new String[]{"graId","nom"},
                    new String[]{"id","nom"},
                    prog.getGra()
            ))).put("org",new JSONObject(EntityUtil.objectToJSONString(
                    new String[]{"orgId","nom"},
                    new String[]{"cod","nom"},
                    prog.getOrg()
            )));
            obj.getJSONObject("grad").put("nivid",prog.getGra().getNivel().getNivId());
            progsArray.put(obj);
        }
        return  progsArray;
    }
}
