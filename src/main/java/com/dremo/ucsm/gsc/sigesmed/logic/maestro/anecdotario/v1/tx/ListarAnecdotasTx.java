package com.dremo.ucsm.gsc.sigesmed.logic.maestro.anecdotario.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.anecdotario.AnecdotarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.anecdotario.Anecdotario;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 22/12/2016.
 */
public class ListarAnecdotasTx implements ITransaction{
    private static Logger logger = Logger.getLogger(ListarAnecdotasTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idEst = data.getInt("est");
        int idDoc = data.getInt("doc");
        return listarAnecdotas(idEst,idDoc);
    }

    private WebResponse listarAnecdotas(int idEst,int idDoc) {
        try{
            AnecdotarioDao anecDao = (AnecdotarioDao) FactoryDao.buildDao("maestro.anecdotario.AnecdotarioDao");
            List<Anecdotario> anecdotas = anecDao.listarAnecdotasEstudiante(idEst,idDoc);
            JSONArray jsonAnecdotas = new JSONArray(EntityUtil.listToJSONString(
                    new String[]{"aneId","fec","des","der","sec"},
                    new String[]{"id","fec","des","der","sec"},
                    anecdotas
            ));
            return WebResponse.crearWebResponseExito("Se listo correctamente las anecdotas",jsonAnecdotas);
        }catch (Exception e){
            logger.log(Level.SEVERE,"listarAnecdotas",e);
            return WebResponse.crearWebResponseError("no se pueden listar las anecdotas");
        }
    }
}
