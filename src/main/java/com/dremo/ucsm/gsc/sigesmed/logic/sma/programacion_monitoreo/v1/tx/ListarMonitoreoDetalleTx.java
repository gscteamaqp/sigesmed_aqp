/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.programacion_monitoreo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.MonitoreoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.TrabajadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.MonitoreoDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ListarMonitoreoDetalleTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ListarMonitoreosPorUgelTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject requestData = (JSONObject)wr.getData();
        Integer monId = requestData.getInt("monId");
                
        List<MonitoreoDetalle> monitoreosDetalle = null;
        MonitoreoDao monitoreoDao = (MonitoreoDao)FactoryDao.buildDao("sma.MonitoreoDao");
        PersonaDao personaDao = (PersonaDao)FactoryDao.buildDao("PersonaDao");
        TrabajadorDao trabajadorDao = (TrabajadorDao)FactoryDao.buildDao("sma.TrabajadorDao");
        
        try{
            monitoreosDetalle = monitoreoDao.listarMonitoreoDetalle(monId);
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar detalle del monitoreo",e);
            System.out.println("No se pudo listar el detalle del monitoreo\n"+e);
            return WebResponse.crearWebResponseError("No se pudo listar el detalle del monitoreo", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        
        JSONArray miArray = new JSONArray();
        for(MonitoreoDetalle m:monitoreosDetalle ){
            JSONObject oResponse = new JSONObject();
            Integer docId = m.getIdDoc();
            Integer traId = 0;
            
            Persona persona =  personaDao.buscarPorID(docId);
            Persona trabajadorPer =  null;
            
            if(m.getIdEsp() != null){
                traId = m.getIdEsp();
                Trabajador trabajador = trabajadorDao.buscarDatosPersonales(traId);
                if (trabajador != null){
                    oResponse.put("espId", traId);
                    oResponse.put("espDetalle", 
                            trabajador.getPersona().getNom() + 
                            " " + 
                            trabajador.getPersona().getApePat() +
                            " " + 
                            trabajador.getPersona().getApeMat()
                    );
                } else{
                    oResponse.put("espId", " ");
                    oResponse.put("espDetalle", " ");
                }
            }else{
                oResponse.put("espId", " ");
                oResponse.put("espDetalle", " ");
            }
            
            
            //oResponse.put("espDetalle", "");
            //oResponse.put("plaId", m.getMndId());
            //oResponse.put("plaDetalle", "");
            oResponse.put("monDetId", m.getMndId());
            oResponse.put("fecVis", m.getFecVis());
            oResponse.put("etapa", m.getEtaMon());
            oResponse.put("etapaDes","");
            oResponse.put("docId", docId);
            oResponse.put("docDetalle", persona.getNom() + " " + persona.getApePat() + " " + persona.getApeMat());
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("El detalle del monitoreo fue listado exitosamente", miArray);
     
        } 
}
