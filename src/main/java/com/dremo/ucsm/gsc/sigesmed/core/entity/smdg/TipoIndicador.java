/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.smdg;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "tipo_indicador")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoIndicador.findAll", query = "SELECT t FROM TipoIndicador t"),
    @NamedQuery(name = "TipoIndicador.findByTinId", query = "SELECT t FROM TipoIndicador t WHERE t.tinId = :tinId"),
    @NamedQuery(name = "TipoIndicador.findByTinDes", query = "SELECT t FROM TipoIndicador t WHERE t.tinDes = :tinDes"),
    @NamedQuery(name = "TipoIndicador.findByUsuMod", query = "SELECT t FROM TipoIndicador t WHERE t.usuMod = :usuMod"),
    @NamedQuery(name = "TipoIndicador.findByFecMod", query = "SELECT t FROM TipoIndicador t WHERE t.fecMod = :fecMod"),
    @NamedQuery(name = "TipoIndicador.findByEstReg", query = "SELECT t FROM TipoIndicador t WHERE t.estReg = :estReg")})
public class TipoIndicador implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "tin_id")
    private Integer tinId;
    @Column(name = "tin_des")
    private String tinDes;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    @Column(name = "est_reg")
    private String estReg;
//    @OneToMany(mappedBy = "tinId")
//    private List<GrupoDetalle> grupoDetalleList;

    public TipoIndicador() {
    }

    public TipoIndicador(Integer tinId) {
        this.tinId = tinId;
    }

    public Integer getTinId() {
        return tinId;
    }

    public void setTinId(Integer tinId) {
        this.tinId = tinId;
    }

    public String getTinDes() {
        return tinDes;
    }

    public void setTinDes(String tinDes) {
        this.tinDes = tinDes;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

//    @XmlTransient
//    public List<GrupoDetalle> getGrupoDetalleList() {
//        return grupoDetalleList;
//    }
//
//    public void setGrupoDetalleList(List<GrupoDetalle> grupoDetalleList) {
//        this.grupoDetalleList = grupoDetalleList;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tinId != null ? tinId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoIndicador)) {
            return false;
        }
        TipoIndicador other = (TipoIndicador) object;
        if ((this.tinId == null && other.tinId != null) || (this.tinId != null && !this.tinId.equals(other.tinId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.smdg.TipoIndicador[ tinId=" + tinId + " ]";
    }
    
}
