/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.registro_trabajador.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.LibroAsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author carlos
 */
public class ListarTrabajadorDetalladoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        Integer organizacoinId;
        Organizacion _user =null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            organizacoinId = requestData.getInt("organizacionID");        
            _user = new Organizacion(organizacoinId);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo verificar la Organizacion", e.getMessage() );
        }

        List<Trabajador> trabajadores = new ArrayList<>();
        LibroAsistenciaDao libroAsistenciaDao = (LibroAsistenciaDao)FactoryDao.buildDao("cpe.LibroAsistenciaDao");
        try{
            trabajadores =libroAsistenciaDao.listarTrabajadores(_user);
            
            System.out.println("Organizacion: " + organizacoinId);
            
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar los Trabajadores de la Institucion ", e.getMessage() );
        }

        JSONArray miArray = new JSONArray();
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        
        for(Trabajador tra:trabajadores ){
            
            JSONObject oResponse = new JSONObject();
            oResponse.put("personaID",tra.getPersona().getPerId());
            oResponse.put("dni",tra.getPersona().getDni());
            oResponse.put("nombreCompleto",tra.getPersona().getNombrePersona());
            oResponse.put("fechaNac", sdf.format(tra.getPersona().getFecNac()));
            oResponse.put("fechaIn", tra.getFecIng());
            
            if (tra.getTraCar() == null) {
                oResponse.put("cargo","");
            } else {
                oResponse.put("cargo",tra.getTraCar().getCrgTraNom());
            }
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}

