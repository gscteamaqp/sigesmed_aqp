/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.rol.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.RolDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.FuncionSistema;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Rol;
import com.dremo.ucsm.gsc.sigesmed.core.entity.RolFuncion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.ArrayList;
import java.util.Date;
import org.json.JSONArray;

/**
 *
 * @author Administrador
 */
public class InsertarRolTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        Rol nuevoRol = null;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            String abreviatura = requestData.getString("abreviatura");
            String nombre = requestData.getString("nombre");
            String descripcion = requestData.getString("descripcion");
            String estado = requestData.getString("estado");
            JSONArray funciones = requestData.getJSONArray("funciones");
            
            nuevoRol = new Rol(0, abreviatura, nombre, descripcion,new Date(), 1, estado.charAt(0), new ArrayList<RolFuncion>());
            
            for(int i = 0; i < funciones.length();i++){
                JSONObject bo = funciones.getJSONObject(i);
                nuevoRol.getRolFunciones().add( new RolFuncion(new FuncionSistema( bo.getInt("funcionID") ) , nuevoRol,i+1, bo.getString("dependencias"),bo.getInt("tipo") ) );
            }
            /*int i =1;            
            for(Object bo: requestData.getElementJSONArray("funciones").getList()){
                nuevoRol.getRolFunciones().add( new RolFuncion(new FuncionSistema( Integer.parseInt( ((JSONObject)bo).getString("funcionID") ) ) , nuevoRol,i++ ) );
            }*/
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        RolDao rolDao = (RolDao)FactoryDao.buildDao("RolDao");
        try{
            rolDao.insert(nuevoRol);
        
        }catch(Exception e){
            System.out.println("No se pudo registrar el Rol\n"+e);
            return WebResponse.crearWebResponseError("No se pudo registrar el Rol", e.getMessage() );
        }
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("rolID",nuevoRol.getRolId());
        oResponse.put("fecha",nuevoRol.getFecMod().toString());
        return WebResponse.crearWebResponseExito("El registro del Rol se realizo correctamente", oResponse);
        //Fin
    }
    
}
