package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.agregar_estudiante.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.DomicilioDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.ParienteEstudianteDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.Domicilio;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.DomicilioId;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.ParientesId;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.ParientesMMI;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.TipoParienteMMI;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

public class GuardarParienteEstudianteTx implements ITransaction {

    private static Logger logger = Logger.getLogger(GuardarParienteEstudianteTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        long perId, parId, parEstId;
        boolean parViv, parVivEst;
        int tipParCod;

        try {
            perId = data.getInt("parPerIdEst");
            parId = data.getInt("parPerIdpar");
            parViv = data.getBoolean("parViv");
            parVivEst = data.getBoolean("parVivEst");
            tipParCod = data.getInt("parTipParCod");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error al cargar datos de nuevo Pariente", e);
            return WebResponse.crearWebResponseError("Nose pudo insertar Pariente");
        }

        ParienteEstudianteDaoHibernate dhPariente = new ParienteEstudianteDaoHibernate();

        try {
            parEstId = dhPariente.llavePariente(perId);
            parEstId = parEstId + 1;

            ParientesMMI miPariente = new ParientesMMI();
            miPariente.setId(new ParientesId(parId, perId));
            miPariente.setParViv(parViv);
            miPariente.setParVivEst(parVivEst);
            miPariente.setTipoPariente(new TipoParienteMMI(tipParCod));
            miPariente.setEstReg("A");
            miPariente.setFecMod(new Date());
            dhPariente.saveOrUpdate(miPariente);

            return WebResponse.crearWebResponseExito("Se guardo el Pariente exitosamente", miPariente);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error Insertar Pariente", e);
            return WebResponse.crearWebResponseError("Nose pudo insertar Pariente");
        }
    }
}
