/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.login.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.UsuarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author harold
 */
public class CheckUserPassword implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        
        String username = "";
        String password = "";        
        
        JSONObject requestData = (JSONObject) wr.getData();
        username = requestData.getString("username");
        password = requestData.getString("password");
        
        /* Parte para la operacion en la Base de Datos */
        Usuario usuario = null;
        try {
            usuario = ((UsuarioDao) FactoryDao.buildDao("UsuarioDao")).buscarPorUsuarioYPassword(username, password);
        } catch (Exception e) {
            System.out.println("No se pudo buscar por Nombre y Password\n" + e);
            return WebResponse.crearWebResponseError("No se pudo buscar por Nombre y Password", e.getMessage());
        }
        
        JSONObject oResponse = new JSONObject();
        
        if (usuario != null) {
            return WebResponse.crearWebResponseExito("La verificación ha sido exitosa", oResponse);
        } else {
            return WebResponse.crearWebResponseError("La contraseña es incorrecta", oResponse);
        }       
        
    }
    
}
