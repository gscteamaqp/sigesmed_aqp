package com.dremo.ucsm.gsc.sigesmed.core.service.base;

import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class FileJsonObject {
    
    private String name;        //nombre del archivo con su extension
    private String type;        //tipo de archivo (ej application, image etc)    
    private String codeType;    //codificacion del contenido (ej base64, base32, etc)
    private String data;        //contenido del archivo   
    private int size;
    
    public FileJsonObject(JSONObject bo){
        parseBaseObject(bo);
    }
    
    public FileJsonObject(JSONObject bo,String name){
        parseBaseObject(bo,name);
    }
    
    private void parseBaseObject(JSONObject bo){
        //leendo un los atributos de un archivo
        try{
            this.name = bo.getString("name");
            this.type = bo.getString("type");
            this.codeType= bo.getString("codeType");
            this.data = bo.getString("data");
            this.size=bo.getInt("size");
        }catch(Exception e){
            System.out.println( "Error leendo formato de archivo : "+e);
            throw new RuntimeException("Error leendo formato de archivo: "+e);
        }
    }
    private void parseBaseObject(JSONObject bo,String name){
        //leendo un los atributos de un archivo
        try{
            String extencion = bo.getString("name");
            this.name = name + extencion.substring(extencion.lastIndexOf("."));
            this.type = bo.getString("type");
            this.codeType= bo.getString("codeType");
            this.data = bo.getString("data");
            this.size=bo.getInt("size");
        }catch(Exception e){
            System.out.println( "Error leendo formato de archivo : "+e);
            throw new RuntimeException("Error leendo formato de archivo: "+e);
        }
    }
    
    public String getName(){
        return name;
    }
    public String getType(){
        return type;
    }
    public String getCodeType(){
        return codeType;
    }
    public String getData(){
        return data;
    }
    public int getSize(){
        return size;
    }
    public String concatName(String nom){
        int pos = name.lastIndexOf(".");
        name =  name.substring(0, pos)+nom+ name.substring(pos);
        return name ;
    }
}
