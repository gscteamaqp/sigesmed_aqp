package com.dremo.ucsm.gsc.sigesmed.logic.maestro.carpeta.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.carpeta.ContenidoSeccionCarpetaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.ArchivosCarpeta;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.layout.element.Paragraph;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by geank on 02/02/17.
 */
public class ReporteCarpetaPedagogicaTx implements ITransaction {
    private Logger logger = Logger.getLogger(ReporteCarpetaPedagogicaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idCar = data.getInt("car");
        int idOrg = data.getInt("org");
        int idDoc = data.getInt("doc");
        return generarReporteCarpeta(idCar,idOrg,idDoc);
    }

    private WebResponse generarReporteCarpeta(int idCar, int idOrg, int idDoc) {
        try{
            OrganizacionDao orgDao = (OrganizacionDao) FactoryDao.buildDao("OrganizacionDao");
            ContenidoSeccionCarpetaDao contDao = (ContenidoSeccionCarpetaDao) FactoryDao.buildDao("maestro.carpeta.ContenidoSeccionCarpetaDao");

            Organizacion organizacion = orgDao.buscarConTipoOrganizacionYPadre(idOrg);
            List<ArchivosCarpeta> archivos = contDao.listarDocumentosCarpeta(idCar,organizacion.getOrgId(),organizacion.getOrganizacionPadre().getOrgId(),idDoc);
            String b64 = generarBase64(archivos);
            return WebResponse.crearWebResponseExito("SE listo correctamente los datos",new JSONObject().put("b64",b64));
        }catch (Exception e){
            logger.log(Level.SEVERE,"generarReporteCarpeta",e);
            return WebResponse.crearWebResponseError("No se puede crear el reporte");
        }
    }
    private String generarBase64(List<ArchivosCarpeta> archivos) throws Exception{
        Map<String,List<ArchivosCarpeta>> seccionesCarpeta = new TreeMap<>();
        for(ArchivosCarpeta archivo : archivos){
            String nomSeccion = archivo.getSecNom();
            if(nomSeccion != null){
                if(seccionesCarpeta.containsKey(nomSeccion)){
                    //si existe
                    seccionesCarpeta.get(nomSeccion).add(archivo);
                }else{
                    // no contiene
                    List<ArchivosCarpeta> archivosSeccion = new ArrayList<>();
                    archivosSeccion.add(archivo);
                    seccionesCarpeta.put(nomSeccion,archivosSeccion);
                }
            }
        }
        //Hora creamos el reporte
        Mitext mitext = new Mitext(false,"SIGESMED");
        mitext.agregarTitulo("Reporte de Carpeta Pedagogica");

        /*JSONObject jsonSub = new JSONObject();
        jsonSub.put("Programacion Curricular",programacion.getDes());
        jsonSub.put("Organizacion",programacion.getOrg().getNom());
        jsonSub.put("Area",programacion.getAre().getNom());
        jsonSub.put("Grado",programacion.getGra().getNom());
        mitext.agregarSubtitulos(jsonSub);*/
        PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
        Paragraph p = new Paragraph("Secciones de Carpeta Pedagogica");
        p.setKeepTogether(true);
        p.setFont(bold).setFontSize(12);
        mitext.getDocument().add(p);
        int i = 1;
        for(Map.Entry<String,List<ArchivosCarpeta>> entry : seccionesCarpeta.entrySet()){
            p = new Paragraph("SECCION " + i +" " + entry.getKey());
            p.setKeepTogether(true);
            p.setFont(bold).setFontSize(12);
            mitext.getDocument().add(p);

            int j = 1;
            GTabla gtabla = new GTabla(new float[]{0.5f,1.5f,2.0f});
            gtabla.build(new String[]{"N°","Nombre Archivo","Estado"});
            for(ArchivosCarpeta arch : entry.getValue()){
                gtabla.processLine(new String[]{String.valueOf(j),arch.getConNom(),arch.getNomFil() == null || arch.getPat() == null ? "PENDIENTE": "SUBIDO"});
                j++;
            }
            i++;
            mitext.agregarTabla(gtabla);
        }
        mitext.cerrarDocumento();
        return mitext.encodeToBase64();
    }

}
