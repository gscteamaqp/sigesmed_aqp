/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.cuadro_horas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.AmbientesDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.Ambientes;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author harold
 */
public class ObtenerAulasPorOrganizacionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        JSONArray responseData = new JSONArray();
        JSONObject requestData = (JSONObject)wr.getData();
        int orgId = requestData.getInt("org_id");        
        int ambId = requestData.getInt("amb_id");
        String diaSem = requestData.getString("dia_semana");
//        String horaInicio = requestData.getString("hora_inicio");
//        String horaFin = requestData.getString("hora_fin");
        
        Date horaInicio = null;
        Date horaFin = null;
        
        try {
            horaInicio = new SimpleDateFormat("H:mm").parse(requestData.getString("hora_inicio"));
            horaFin = new SimpleDateFormat("H:mm").parse(requestData.getString("hora_fin"));
        } catch (ParseException ex) {
            Logger.getLogger(ObtenerAulasPorOrganizacionTx.class.getName()).log(Level.SEVERE, null, ex);
        }
                                
        try{
        
            AmbientesDAO ambDao = (AmbientesDAO)FactoryDao.buildDao("scp.AmbientesDAO");
            
            Ambientes ambiente;
            
            if (ambId != 0) {
                
                ambiente = ambDao.buscarAmbientePorId(ambId);
                
                if (ambiente != null){
                    JSONObject oResponse = new JSONObject();
                    oResponse.put("amb_id", ambiente.getAmb_id());
                    oResponse.put("amb_des", ambiente.getDes());
                    oResponse.put("amb_ubi", ambiente.getUbi());
                    oResponse.put("amb_area", ambiente.getArea());
                    oResponse.put("amb_est", String.valueOf(ambiente.getEst_amb()));
                    responseData.put(oResponse);
                }
            }

            List<Ambientes> ambientes = ambDao.buscarAmbientesDisponibles(orgId, horaInicio, horaFin, diaSem);

            if (ambientes != null) {

                for (Ambientes amb : ambientes) {
                    JSONObject oResponse = new JSONObject();
                    oResponse.put("amb_id", amb.getAmb_id());
                    oResponse.put("amb_des", amb.getDes());
                    oResponse.put("amb_ubi", amb.getUbi());
                    oResponse.put("amb_area", amb.getArea());
                    oResponse.put("amb_est", String.valueOf(amb.getEst_amb()));
                    responseData.put(oResponse);
                }

            } 
        
        } catch(Exception e) {
            System.out.println("No se pudo listar los ambientes\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los Ambientes", e.getMessage() );    
        }
        
        return WebResponse.crearWebResponseExito("Se listo los ambientes correctamente", responseData);
    }
    
}
