/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.web;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.BandejaAlertaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaAlerta;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author abel
 */
public class BandejaAlertaDaoHibernate extends GenericDaoHibernate<BandejaAlerta> implements BandejaAlertaDao{

    @Override
    public void insertarVarios(List<BandejaAlerta> alertas){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{            
            for(BandejaAlerta b: alertas){
                session.persist(b);
            }                
            miTx.commit();        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo insertar varias bandejas de alertas\\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo insertar varias bandejas de alertas\\n"+ e.getMessage());
        }
        finally{
            session.close();
        }
    }
    @Override
    public List<BandejaAlerta> listarNoVistosPorUsuario(int usuarioID){
        List<BandejaAlerta> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT b FROM BandejaAlerta b JOIN FETCH b.alerta WHERE b.usuSesId=:p1 AND b.fecVis IS NULL";
            Query query = session.createQuery(hql);
            query.setParameter("p1", usuarioID);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las alertas no vistas del usuario\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las alertas no vistas del usuario\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public List<BandejaAlerta> listarTodosPorUsuario(int usuarioID){
        List<BandejaAlerta> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT b FROM BandejaAlerta b JOIN FETCH b.alerta WHERE b.usuSesId=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", usuarioID);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar todas las alertas del usuario\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar todas las alertas del usuario\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public void marcarVistoAlerta(int bandejaAlertaID){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{            
            String hql = "UPDATE BandejaAlerta ba SET ba.fecVis=:p2 WHERE ba.banAleId =:p1";            
            Query query = session.createQuery(hql);
            query.setParameter("p1", bandejaAlertaID);
            query.setParameter("p2", new Date());
            query.executeUpdate();
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo ver la alerta\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
}
