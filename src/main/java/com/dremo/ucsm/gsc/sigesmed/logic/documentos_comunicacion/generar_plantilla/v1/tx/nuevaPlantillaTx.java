/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_comunicacion.generar_plantilla.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sdc.PlantillaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.ContenidoPlantilla;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.ImagenPlantilla;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.Plantilla;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.PropiedadLetra;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.TipoDocumento;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildCodigo;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class nuevaPlantillaTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {

        Plantilla nuevaPlantilla = null;
        TipoDocumento tipoDocum = null;
        ContenidoPlantilla contPla = null;
        List<FileJsonObject> listaArchivos = new ArrayList<FileJsonObject>();
        String nombreArchivo="";
        PlantillaDao plantillaDao = (PlantillaDao) FactoryDao.buildDao("sdc.PlantillaDao");
        try {

            JSONObject requestData = (JSONObject) wr.getData();
            String descripcion = requestData.getString("descripcion");

            

            JSONObject tipoDoc = (JSONObject) requestData.get("tipoDoc");
            Integer tipDocId = tipoDoc.getInt("tipoDocumentoID");
            tipoDocum = new TipoDocumento(tipDocId);

            JSONArray contenidos = (JSONArray) requestData.getJSONArray("contenidoPlantilla");
            Integer version = 1;
            String estado = "2";//pendiente
            Integer usuario = requestData.getInt("personaID");
            UsuarioSession user_=new UsuarioSession(usuario);

          

            List<ContenidoPlantilla> ctnPla = new ArrayList<>();
            nuevaPlantilla = new Plantilla(descripcion, version, estado, new Date(), user_, tipoDocum);
            for (int i = 0; i < 3; i++) {

                JSONObject  ctn = (JSONObject) contenidos.get(i);
                String ctn_=ctn.getString("contenido");
                Integer tam=ctn.getInt("size");
                JSONObject  nl=contenidos.getJSONObject(i);
                JSONObject letra=nl.getJSONObject("nombreLetra");
                Integer idLetra=letra.getInt("idLetra");
                JSONObject alineacion=nl.getJSONObject("alineacion");
                Integer idAlineacion=alineacion.getInt("id");
                         
                Boolean isBold=ctn.getBoolean("isBold");
                Boolean isCursiva=ctn.getBoolean("isCursiva");
                Boolean isSubrayado=ctn.getBoolean("isSubrayado");
                String tip = String.valueOf(i + 1);

                contPla = new ContenidoPlantilla(tip, ctn_,nuevaPlantilla, idAlineacion,new PropiedadLetra(idLetra),isBold,isCursiva,isSubrayado, tam);
                ctnPla.add(contPla);

            }
            nuevaPlantilla.setContenidoPlantillas(ctnPla);
           
            int numDoc=0;
            List<ImagenPlantilla> rutas=new ArrayList<>();
            JSONArray imagenes = (JSONArray) requestData.getJSONArray("imagenes");
            for (int i = 0; i < imagenes.length(); i++) {
                JSONObject bo = imagenes.getJSONObject(i);
                String imagenDescripcion = bo.getString("descripcion");
                JSONObject jsonArchivo = bo.optJSONObject("archivo");
                if (jsonArchivo != null && jsonArchivo.length() > 0) {
                    FileJsonObject miF = new FileJsonObject(jsonArchivo,(plantillaDao.buscarUltimoCodigo()+1)+BuildCodigo.cerosIzquierda(++numDoc,2));
                    nombreArchivo = miF.getName();
                    listaArchivos.add(miF);
                    ImagenPlantilla img=new ImagenPlantilla(nombreArchivo,nuevaPlantilla, imagenDescripcion);
                    rutas.add(img);
                }
                
            }
            nuevaPlantilla.setRutasImagenes(rutas);
        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo obtener datos y/o contenido de la Plantilla", e.getMessage());
        }
        //Fin        

        /*
         *  Parte para la operacion en la Base de Datos
         */
        
        try {
            plantillaDao.insert(nuevaPlantilla);

        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se genero la Plantilla ", e.getMessage());
        }

        for(FileJsonObject archivo : listaArchivos){
            BuildFile.buildFromBase64("documentos_comunicacion_imagenes", archivo.getName(), archivo.getData());
        }
        
        return WebResponse.crearWebResponseExito("Se inserto correctamente");
        //Fin
    }

}
