/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.control_libro.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.control_libro.v1.tx.ActualizarControlTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.control_libro.v1.tx.EliminarControlTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.control_libro.v1.tx.InsertarControlTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.control_libro.v1.tx.ListarControlTx;


/**
 *
 * @author Administrador
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_SISTEMA_CONTABLE_INSTITUCIONAL);        
        
        //Registrnado el Nombre del componente
        component.setName("controlLibro");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionPOST("insertarControl", InsertarControlTx.class);
        component.addTransactionGET("listarControl", ListarControlTx.class);        
        component.addTransactionDELETE("eliminarControl", EliminarControlTx.class);
     //   component.addTransactionPUT("actualizarControl", ActualizarControlTx.class);
        
        return component;
    }
}