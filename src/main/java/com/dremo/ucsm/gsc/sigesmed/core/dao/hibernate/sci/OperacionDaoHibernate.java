/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sci;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.OperacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.Operacion;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;


/**
 *
 * @author RE
 */
public class OperacionDaoHibernate  extends GenericDaoHibernate<Operacion> implements OperacionDao {
    
    @Override
    public List<Operacion> buscarConCuentas() {
        List<Operacion> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
      
        try{
            //listar SubModuloSistemas
            String hql = "SELECT DISTINCT o FROM Operacion o LEFT JOIN FETCH o.cuentaOperaciones WHERE o.estReg!='E' ORDER BY o.tipOpe DESC";
            Query query = session.createQuery(hql);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las Operaciones \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las Operaciones \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
      
        return objetos;
    }
    @Override
     public void eliminarCuentas(int operacionID) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            String hql = "DELETE FROM administrativo.cuenta_operacion co WHERE co.ope_id="+operacionID;
            SQLQuery query = session.createSQLQuery(hql);
            query.executeUpdate();
            miTx.commit();
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo Eliminar las Cuentas de la Operacion \n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Eliminar las Cuentas de la Operacion \n "+ e.getMessage());
        }
        finally{
            session.close();
        }
    }
}
