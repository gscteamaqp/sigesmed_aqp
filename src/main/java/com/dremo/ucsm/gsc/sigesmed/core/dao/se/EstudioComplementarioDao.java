/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.EstudioComplementario;
import java.util.List;

/**
 *
 * @author gscadmin
 */
public interface EstudioComplementarioDao extends GenericDao<EstudioComplementario>{
    public List<EstudioComplementario> listarxFichaEscalafonaria(int ficEscId);
    public EstudioComplementario buscarPorId(Integer estComId);
    
}
