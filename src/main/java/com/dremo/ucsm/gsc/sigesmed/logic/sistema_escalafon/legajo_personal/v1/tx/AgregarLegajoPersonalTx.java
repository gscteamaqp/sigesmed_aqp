/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.legajo_personal.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.LegajoPersonalDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.LegajoPersonal;
import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class AgregarLegajoPersonalTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(AgregarLegajoPersonalTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        
        LegajoPersonal legajoPersonal = null;
        FileJsonObject file = null;

        try {
            JSONObject requestData = (JSONObject) wr.getData();

            Integer ficEscId = requestData.getInt("ficEscId");
            Character catLeg = requestData.getString("catLeg").charAt(0);
            Character subCat = requestData.getString("subCat").charAt(0);
            String des = requestData.getString("des");
            Integer codAspOri = requestData.getInt("codAspOri");
            
            //Datos del archivo
            String nomLeg = requestData.optString("nomDoc");
            JSONObject arcJSON = requestData.optJSONObject("archivo");

            if(arcJSON != null && arcJSON.length() > 0){
                file = new FileJsonObject(arcJSON, ficEscId + "_legajo_");
            }else{
                return WebResponse.crearWebResponseError("No se pudo leer el archivo, datos incorrectos");  
            }
            
            legajoPersonal = new LegajoPersonal(new FichaEscalafonaria(ficEscId), nomLeg, new Date(), Legajo.LEGAJO_PATH, des, catLeg, subCat, codAspOri);
            legajoPersonal.setEstReg('A');
            legajoPersonal.setFecMod(new Date());
            legajoPersonal.setUsuMod(wr.getIdUsuario());

        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE,"Datos nuevo legajo personal",e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }
        /*
        *  Parte para la operacion en la Base de Datos
         */
        LegajoPersonalDao legajoPersonalDao = (LegajoPersonalDao) FactoryDao.buildDao("se.LegajoPersonalDao");
        try {
            legajoPersonalDao.insert(legajoPersonal);
            
            legajoPersonal.setNom(file.concatName("" + legajoPersonal.getLegPerId()) );
            BuildFile.buildFromBase64(Legajo.LEGAJO_PATH, legajoPersonal.getNom(), file.getData());
            legajoPersonalDao.update(legajoPersonal);
            
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar nueva exposicion",e);
            System.out.println(e);
        }
        
        DateFormat sdo = new SimpleDateFormat("dd-MM-yyyy");
        String route = ServicioREST.PATH_SIGESMED + File.separator + Sigesmed.UBI_ARCHIVOS + File.separator + Legajo.LEGAJO_PATH + File.separator;
        Path path = Paths.get(route + legajoPersonal.getNom());
        
        JSONObject oResponse = new JSONObject();
        if (Files.exists(path)){
            oResponse.put("existeArchivo", true);
            oResponse.put("mensaje", "El archivo existe");
        } else {
            oResponse.put("existeArchivo", false);
            oResponse.put("mensaje", "El archivo no existe");
        }
        oResponse.put("nomDoc", legajoPersonal.getNom());
        oResponse.put("des", legajoPersonal.getDes());
        oResponse.put("url", Sigesmed.UBI_ARCHIVOS + File.separator  + legajoPersonal.getUrl() + File.separator);
        oResponse.put("fecReg", sdo.format(legajoPersonal.getFecIng()));
        oResponse.put("catLeg", legajoPersonal.getCatLeg());
                
        return WebResponse.crearWebResponseExito("El registro del legajo personal se realizo correctamente", oResponse);
        //Fin
        
    }
    
}
