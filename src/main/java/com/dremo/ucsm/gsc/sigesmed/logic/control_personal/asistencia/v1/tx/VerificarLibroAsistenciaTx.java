/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.asistencia.v1.tx;


import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.LibroAsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.ConfiguracionControl;


import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;


/**
 *
 * @author carlos
 */
public class VerificarLibroAsistenciaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
      
        Integer organizacion;
       
        try{
            JSONObject requestData = (JSONObject)wr.getData(); 
            organizacion=requestData.getInt("organizacionID");

        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo verificar los datos", e.getMessage() );
        }
        
        
        ConfiguracionControl controlActual=null;
        try {

            LibroAsistenciaDao libroAsistenciaDao = (LibroAsistenciaDao) FactoryDao.buildDao("cpe.LibroAsistenciaDao");
            controlActual=libroAsistenciaDao.verificarLibroAsistenciaAbiertoByFecha(organizacion, new Date());
        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo verificar el Libro de Asistencia", e.getMessage());
        }
        
        JSONObject oResponse=new JSONObject();
        if(controlActual!=null)
        {
            oResponse.put("toleranciaHora", controlActual.getConPerTolHora()); 
            oResponse.put("toleranciaMin", controlActual.getConPerTolMin()); 
            oResponse.put("control", true);
            return WebResponse.crearWebResponseExito("Se Verifico correctamente",oResponse);
        }
        else
        {
            oResponse.put("control", false);
            
            return WebResponse.crearWebResponseExito("Fecha o Estado del Libro asistencia no vigente",oResponse);
        }
        
       // return WebResponse.crearWebResponseExito("Se Verifico correctamente",oResponse);
        //Fin
    }

}
