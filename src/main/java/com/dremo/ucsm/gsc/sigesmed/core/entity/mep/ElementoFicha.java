/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.mep;

import java.util.Date;
import java.util.logging.Logger;

/**
 *
 * @author geank
 */
public interface ElementoFicha {
    FichaEvaluacionPersonal getFichaEvaluacionPersonal();
    void setFichaEvaluacionPersonal(FichaEvaluacionPersonal fichaEvaluacionPersonal);
    void setEstReg(Character est);
    void setFecMod(Date date);
    ElementoFicha copyFromOther(ElementoFicha elemento);
}
