/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scp;

/**
 *
 * @author Administrador
 */

import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.CatalogoDireccionDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.CatalogoDireccion;

public class CatalogoDireccionDAOHibernate extends GenericDaoHibernate<CatalogoDireccion> implements CatalogoDireccionDAO{

    @Override
    public List<CatalogoDireccion> listarCatalogoDireccion() {
        
         List<CatalogoDireccion> cat_dire = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{

            String hql = "SELECT cd FROM CatalogoDireccion cd  WHERE cd.est_reg!='E'";

            Query query = session.createQuery(hql); 
            cat_dire = query.list();
            
        }catch(Exception e){
             System.out.println("No se pudo Mostrar el Catalogo de Direcciones \\n "+ e.getMessage());
             throw new UnsupportedOperationException("No se pudo Mostrar el Catalogo de Direcciones \\n "+ e.getMessage());
        }
        finally{
            session.close();
        }
        return cat_dire; 
        
    }
    
    
    
}
