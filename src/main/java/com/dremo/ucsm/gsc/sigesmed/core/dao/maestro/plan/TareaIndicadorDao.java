/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.TareaSesionAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.TareaIndicador;
import java.util.List;


/**
 *
 * @author abel
 */
public interface TareaIndicadorDao extends GenericDao<TareaIndicador>{
    
    List<TareaIndicador> listar_tareas_indicador(int ind_id);
    TareaSesionAprendizaje listar_indicadores_tarea(int id_tarea);
    TareaSesionAprendizaje obtenerTarea(int id_tarea);
    
}
