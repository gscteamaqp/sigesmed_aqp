package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.anecdotario;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Estudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Grado;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrador on 19/12/2016.
 */
@Entity
@Table(name = "anecdotario", schema = "pedagogico")
public class Anecdotario implements java.io.Serializable{
    @Id
    @Column(name = "ane_id", nullable = false,unique = true)
    @SequenceGenerator(name = "anecdotario_ane_id_seq",sequenceName = "pedagogico.anecdotario_ane_id_seq")
    @GeneratedValue(generator = "anecdotario_ane_id_seq")
    private int aneId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec",nullable = false)
    private Date fec;
    @Column(name="des",nullable = false)
    private String des;
    @Column(name = "der", length = 2, nullable = true)
    private String der;
    @Column(name = "sec",length = 3, nullable = true)
    private String sec;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "doc_id",nullable = false)
    private Docente docente;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "est_per_id",nullable = false)
    private Estudiante estudiante;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gra_id",nullable = true)
    private Grado grado;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "org_id",nullable = false)
    private Organizacion org;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "anecdotario",cascade = CascadeType.ALL)
    private List<AccionesCorrectivas> accionesCorrectivas = new ArrayList<>();
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;
    @Column(name = "est_reg", length = 1)
    private Character estReg;

    public Anecdotario() {
    }

    public Anecdotario(Date fec, String des, String der) {
        this.fec = fec;
        this.des = des;
        this.der = der;

        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public int getAneId() {
        return aneId;
    }

    public void setAneId(int aneId) {
        this.aneId = aneId;
    }

    public Date getFec() {
        return fec;
    }

    public void setFec(Date fec) {
        this.fec = fec;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getDer() {
        return der;
    }

    public void setDer(String der) {
        this.der = der;
    }

    public String getSec() {
        return sec;
    }

    public void setSec(String sec) {
        this.sec = sec;
    }

    public Docente getDocente() {
        return docente;
    }

    public void setDocente(Docente docente) {
        this.docente = docente;
    }

    public Estudiante getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(Estudiante estudiante) {
        this.estudiante = estudiante;
    }

    public Grado getGrado() {
        return grado;
    }

    public void setGrado(Grado grado) {
        this.grado = grado;
    }

    public Organizacion getOrg() {
        return org;
    }

    public void setOrg(Organizacion org) {
        this.org = org;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public List<AccionesCorrectivas> getAccionesCorrectivas() {
        return accionesCorrectivas;
    }

    public void setAccionesCorrectivas(List<AccionesCorrectivas> accionesCorrectivas) {
        this.accionesCorrectivas = accionesCorrectivas;
    }
}
