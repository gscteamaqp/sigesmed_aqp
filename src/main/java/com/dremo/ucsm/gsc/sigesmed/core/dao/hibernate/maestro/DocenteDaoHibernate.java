package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.maestro;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.DocenteDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Seccion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Grado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Nivel;
import org.hibernate.*;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;

import java.util.List;

/**
 * Created by Administrador on 13/10/2016.
 */
public class DocenteDaoHibernate extends GenericDaoHibernate<Docente> implements DocenteDao{
    @Override
    public Trabajador buscarTrabajadorPorUsuario(int codUsr) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT t FROM Trabajador t INNER JOIN FETCH t.traCar tc INNER JOIN t.persona p INNER JOIN p.usuario u  WHERE u.usuId =:cod";
            Query query = session.createQuery(hql);
            query.setParameter("cod", codUsr);
            query.setMaxResults(1);
            return (Trabajador) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public Docente buscarDocentePorUsuario(int codUsr) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT d FROM DocenteMaestro d  INNER JOIN d.per p INNER JOIN p.usuario u  WHERE u.usuId =:cod";
            Query query = session.createQuery(hql);
            query.setParameter("cod", codUsr);
            query.setMaxResults(1);
            return (Docente) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public Docente buscarDocentePorId(int idDocente) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(Docente.class)
                    .add(Restrictions.eq("doc_id",idDocente));
            return (Docente) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<Docente> buscarDocentesIE(int idIE) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT d FROM DocenteMaestro d INNER JOIN FETCH d.per p " +
                    "INNER JOIN p.trabajadores t " +
                    "INNER JOIN t.organizacion org WHERE org.orgId =:orgId";
            Query query = session.createQuery(hql);
            query.setInteger("orgId",idIE);
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public Usuario buscarUsuarioPorId(int idUsuario) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(Usuario.class)
                    .add(Restrictions.eq("usuId",idUsuario));
            return (Usuario) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public Nivel buscarNivelPorID(int idNivel) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(Nivel.class)
                    .add(Restrictions.eq("nivId",idNivel));
            return (Nivel) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public Grado buscarGradoPorID(int idGrado) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(Grado.class)
                    .add(Restrictions.eq("graId",idGrado));
            return (Grado) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public Seccion buscarSeccionPorID(char idSeccion) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(Seccion.class)
                    .add(Restrictions.eq("sedId",idSeccion));
            return (Seccion) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public AreaCurricular buscarAreaPorId(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT a FROM AreaCurricularMaestro a WHERE a.areCurId =:arId ";
            Query query = session.createQuery(hql);
            query.setInteger("arId",id);
            return (AreaCurricular) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<AreaCurricular> listarAreasDocente(int idOrg, int idDoc, int idGra, char idSecc ) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
                   String sql2 = "SELECT arcu.* FROM area_curricular arcu" +
                    " INNER JOIN institucional.distribucion_hora_area disare ON disare.are_cur_id = arcu.are_cur_id" +
                    " INNER JOIN institucional.distribucion_hora_grado dis ON dis.dis_hor_gra_id = disare.dis_hor_gra_id" +
                    " INNER JOIN grado gr ON gr.gra_id = dis.gra_id" +
                    " INNER JOIN nivel ni ON ni.niv_id = gr.niv_id" +
                    " INNER JOIN institucional.seccion sec ON sec.sec_id = dis.sec_id" +
                    " INNER JOIN institucional.plaza_magisterial plz ON plz.pla_mag_id = dis.pla_mag_id" +
                    " INNER JOIN institucional.plan_estudios plan ON plan.pla_est_id = dis.pla_est_id" +
                    " WHERE plz.doc_id =:doc AND plan.org_id =:org AND gr.gra_id =:grad AND sec.sec_id =:secc";
                   
                    SQLQuery query = session.createSQLQuery(sql2);
                    query.setInteger("doc",idDoc);
                    query.setInteger("org",idOrg);
                    query.setInteger("grad",idGra);
                    query.setCharacter("secc",idSecc);
                    query.addEntity(AreaCurricular.class);
                    return query.list();

        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<AreaCurricular> listarAreasDocente(int idOrg, int idDoc, int idGra ) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String sql2 = "";

                 sql2 = "SELECT arcu.* FROM area_curricular arcu" +
                    " INNER JOIN institucional.distribucion_hora_area disare ON disare.are_cur_id = arcu.are_cur_id" +
                    " INNER JOIN institucional.distribucion_hora_grado dis ON dis.dis_hor_gra_id = disare.dis_hor_gra_id" +
                    " INNER JOIN grado gr ON gr.gra_id = dis.gra_id" +
                    " INNER JOIN nivel ni ON ni.niv_id = gr.niv_id" +
                    " INNER JOIN institucional.seccion sec ON sec.sec_id = dis.sec_id" +
                    " INNER JOIN institucional.plaza_magisterial plz ON plz.pla_mag_id = dis.pla_mag_id" +
                    " INNER JOIN institucional.plan_estudios plan ON plan.pla_est_id = dis.pla_est_id" +
                    " WHERE plz.doc_id =:doc AND plan.org_id =:org AND gr.gra_id =:grad";
         
            
            SQLQuery query = session.createSQLQuery(sql2);
            query.setInteger("doc",idDoc);
            query.setInteger("org",idOrg);
            query.setInteger("grad",idGra);
            query.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
            query.addEntity(AreaCurricular.class);
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
}
