/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.mech;


import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.DistribucionHoraArea;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.DistribucionHoraGrado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlazaMagisterial;
import java.util.List;

/**
 *
 * @author abel
 */

public interface DistribucionHoraGradoDao extends GenericDao<DistribucionHoraGrado>{
    
    public List<DistribucionHoraGrado> listarDistribucionHorasGradoPorPlanEstudios(int planID);
    public List<DistribucionHoraGrado> listarDistribucionHorasGradoPorPlanEstudiosPlazas(int planID);
    public List<PlazaMagisterial> buscarPlazasPorOrganizacion(int orgID);
    public Docente buscarDocentePorDNI(String dni);
    public Docente buscarDocentePorCodigoModular(String codigo);
    public Docente buscarDocentePorID(int docenteID);
    
    public List<DistribucionHoraGrado> buscarDistribucionHorasGradoPorPlanYDocente(int planID,int docenteID);
    
    public void insertarPlazaMagisterial(PlazaMagisterial plaza);
    public void actualizarPlazaMagisterial(PlazaMagisterial plaza);
    public PlazaMagisterial buscarPlazaPorID(int id);
    
    public void insertarDistribucionGrado(DistribucionHoraGrado disGrado);
    public void insertarDistribucionArea(DistribucionHoraArea disArea);
    
    public void insertarDistribucionGrados(List<DistribucionHoraGrado> disGrados);
    public void insertarDistribucionAreas(List<DistribucionHoraArea> disAreas);
    
    public void actualizarHorasPedagogicasPorPlaza(int plazaID,int hora);
    
    public void eliminarDistribucionPorPlanYPlaza(int planID,int plazaID);
}