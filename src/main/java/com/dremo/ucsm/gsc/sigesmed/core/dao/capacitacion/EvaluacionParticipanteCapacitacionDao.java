package com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.EvaluacionParticipanteCapacitacion;
import java.util.List;

public interface EvaluacionParticipanteCapacitacionDao extends  GenericDao<EvaluacionParticipanteCapacitacion> {
    EvaluacionParticipanteCapacitacion buscarUltima(int codEva, int perId);
    int contarEvaluaciones(int codEva, int perId);
    List<Integer> listarParticipantes(int codEva);    
    List<Double> obtenerParaCertificacion(int codSed, int perId, double notMin);
}
