package com.dremo.ucsm.gsc.sigesmed.logic.mep.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.*;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;

/**
 * Created by geank on 23/08/16.
 */
public abstract class MepGeneralTx {
    protected WebResponse formWebResponse(boolean state){
        WebResponse response = new WebResponse();
        response.setScope("web");
        response.setResponseSta(true);
        if(state){
            response.setResponse(WebResponse.OK_RESPONSE);
            response.setResponseMsg(MEP.SUCC_RESPONSE_MESS_LIST);
        }else{
            response.setResponse(WebResponse.BAD_RESPONSE);
            response.setResponseMsg(MEP.ERR_RESPONSE_MESS_LIST);
        }
        return response;
    }
    protected ElementoFicha builtElementByType(String type, JSONObject data) {
        ElementoFicha elementoFicha = null;
        try {
            switch (type) {
                case MEP.TIPO_ELEMENTO_ESCALA:
                    elementoFicha = new EscalaValoracionFicha(
                            data.getInt("id"),
                            data.getString(MEP.CAMPO_ESCALA_NOMBRE),
                            data.getString(MEP.CAMPO_ESCALA_DESCRIPCION),
                            data.getInt(MEP.CAMPO_ESCALA_VAL));
                    break;
                case MEP.TIPO_ELEMENTO_RANGO:
                    elementoFicha = new RangoPorcentualFicha(
                            data.getInt(MEP.CAMPO_RANGO_MIN),
                            data.getInt(MEP.CAMPO_RANGO_MAX),
                            data.getString(MEP.CAMPO_RANGO_DESCRIPCION));
                    break;
                case MEP.TIPO_ELEMENTO_CONTENIDO:
                    elementoFicha = new ContenidoFichaEvaluacion(
                            data.getString(MEP.CAMPO_CONTENIDO_TIPO).charAt(0),
                            data.getString(MEP.CAMPO_CONTENIDO_NOMBRE));
                    break;
            }
        }catch (Exception e){

        }
        return elementoFicha;
    }
}
