/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.mpf;

import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Grado;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Carlos
 */
@Entity(name="GradoIeEstudianteMpf")
@Table(name = "grado_ie_estudiante",schema = "pedagogico")
public class GradoIeEstudiante implements java.io.Serializable {
    @Id
    @Column(name = "gra_ie_est_id",nullable = false,unique = true)
    @SequenceGenerator(name = "grado_ie_estudiante_gra_ie_est_id_seq",sequenceName = "pedagogico.grado_ie_estudiante_gra_ie_est_id_seq")
    @GeneratedValue(generator = "grado_ie_estudiante_gra_ie_est_id_seq")
    private int graOrgEstId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mat_id")
    private Matricula matriculaEstudiante;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gra_id")
    private Grado grado;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sec_id")
    private Seccion seccion;
    @Column(name = "num_ord")
    private Integer numOrd;
    @Column(name = "act")
    private Boolean act;
    @Column(name = "ord_mer")
    private Integer ordMer;
//    @OneToMany(mappedBy = "gradoEstudiante",fetch = FetchType.LAZY)
//    private List<NotaEvaluacionIndicador> notasIndicador = new ArrayList<>();
    @Column(name = "usu_mod")
    private Integer usuMod;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;

    @Column(name = "est_reg", length = 1)
    private Character estReg;

    public GradoIeEstudiante() {
    }

    public GradoIeEstudiante(Matricula matriculaEstudiante, Grado grado, Seccion seccion) {
        this.matriculaEstudiante = matriculaEstudiante;
        this.grado = grado;
        this.seccion = seccion;

        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public int getGraOrgEstId() {
        return graOrgEstId;
    }

    public Matricula getMatriculaEstudiante() {
        return matriculaEstudiante;
    }

    public void setOrganizacionEstudiante(Matricula matriculaEstudiante) {
        this.matriculaEstudiante = matriculaEstudiante;
    }

    public Grado getGrado() {
        return grado;
    }

    public void setGrado(Grado grado) {
        this.grado = grado;
    }

    public Seccion getSeccion() {
        return seccion;
    }

    public void setSeccion(Seccion seccion) {
        this.seccion = seccion;
    }

//    public List<NotaEvaluacionIndicador> getNotasIndicador() {
//        return notasIndicador;
//    }
//
//    public void setNotasIndicador(List<NotaEvaluacionIndicador> notasIndicador) {
//        this.notasIndicador = notasIndicador;
//    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public Integer getNumOrd() {
        return numOrd;
    }

    public void setNumOrd(Integer numOrd) {
        this.numOrd = numOrd;
    }

    public Boolean getAct() {
        return act;
    }

    public void setAct(Boolean act) {
        this.act = act;
    }

    public Integer getOrdMer() {
        return ordMer;
    }

    public void setOrdMer(Integer ordMer) {
        this.ordMer = ordMer;
    }
    
}