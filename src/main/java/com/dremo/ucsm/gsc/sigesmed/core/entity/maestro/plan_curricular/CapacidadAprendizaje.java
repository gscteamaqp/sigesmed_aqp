package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrador on 13/10/2016.
 */
@Entity
@Table(name = "capacidad", schema = "pedagogico")
public class CapacidadAprendizaje implements java.io.Serializable{
    @Id
    @Column(name = "cap_id", nullable = false, unique = true)
    @SequenceGenerator(name = "capacidad_cap_id_sequence",sequenceName = "pedagogico.capacidad_cap_id_seq")
    @GeneratedValue(generator = "capacidad_cap_id_sequence")
    private int capId;

    @Column(name = "nom", length = 500, nullable = false, unique = true)
    private String nom;

    @Column(name = "des", length = 200)
    private String des;


    @OneToMany(mappedBy = "cap",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    //@Fetch(value = FetchMode.JOIN)
    List<IndicadorAprendizaje> indicadores = new ArrayList<>();

    @Column(name = "usu_mod")
    private Integer usuMod;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;

    @Column(name = "est_reg", length = 1)
    private Character estReg;

    public CapacidadAprendizaje() {
    }

    public CapacidadAprendizaje(String nom) {
        this.nom = nom;
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public CapacidadAprendizaje(String nom, String des) {
        this.nom = nom;
        this.des = des;

        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public int getCapId() {
        return capId;
    }

    public void setCapId(int capId) {
        this.capId = capId;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public List<IndicadorAprendizaje> getIndicadores() {
        return indicadores;
    }

    public void setIndicadores(List<IndicadorAprendizaje> indicadores) {
        this.indicadores = indicadores;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CapacidadAprendizaje)) return false;

        CapacidadAprendizaje that = (CapacidadAprendizaje) o;

        if (capId != that.capId) return false;
        return nom.equals(that.nom);

    }

    @Override
    public int hashCode() {
        int result = capId;
        result = 31 * result + nom.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "CapacidadAprendizaje{" + "capId=" + capId + ", nom=" + nom + ", estReg=" + estReg + '}';
    }
    
    
}
