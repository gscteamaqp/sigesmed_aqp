package com.dremo.ucsm.gsc.sigesmed.logic.maestro.sesion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.SesionAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.UnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.*;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.maestro.UtilMaestroLogic;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 01/12/2016.
 */
public class ListarAprendizajesUnidadTx implements ITransaction {
    private static Logger logger = Logger.getLogger(ListarAprendizajesUnidadTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idUnidad = data.getInt("unid");
        int idSesion = data.getInt("sesid");
        return listarAprendizajesUnidad(idUnidad,idSesion);
    }

    private WebResponse listarAprendizajesUnidad(int idUnidad, int idSesion) {
        try{
            UnidadDidacticaDao unidadDao = (UnidadDidacticaDao) FactoryDao.buildDao("maestro.plan.UnidadDidacticaDao");
            SesionAprendizajeDao sesionDao = (SesionAprendizajeDao) FactoryDao.buildDao("maestro.plan.SesionAprendizajeDao");

            List<CompetenciasUnidadDidactica> competenciasUnidad = unidadDao.listarCompetenciasUnidad(idUnidad);
            Map<CompetenciaAprendizaje,List<CapacidadAprendizaje>> mapCompetencias = new HashMap<>();
            for(CompetenciasUnidadDidactica competenciaUni : competenciasUnidad){
                CompetenciaCapacidad compCap = competenciaUni.getCompetenciaCapacidad();
                Set<IndicadorAprendizaje> indicadores = competenciaUni.getIndicadores();
                List<IndicadorAprendizaje> newIndicadores = new ArrayList<>();
                newIndicadores.addAll(indicadores);
                compCap.getCap().setIndicadores(newIndicadores);
                setSelectedIndicador(compCap.getCap(),sesionDao.listarIndicadoresSeleccionados(idSesion,compCap.getCap().getCapId()));
                if(!mapCompetencias.containsKey(compCap.getCom())){
                    List<CapacidadAprendizaje> listCapacidades = new ArrayList<>();
                    listCapacidades.add(compCap.getCap());
                    mapCompetencias.put(compCap.getCom(),listCapacidades);
                }else{
                    mapCompetencias.get(compCap.getCom()).add(compCap.getCap());
                }
            }
            JSONArray rpta = UtilMaestroLogic.mapCompetenciasToJSON(mapCompetencias);
            return WebResponse.crearWebResponseExito("Se listo correctamente",rpta);
        }catch (Exception e){
            logger.log(Level.SEVERE,"listarAprendizajesUnidad",e);
            return WebResponse.crearWebResponseError("No se pudieron listar los indicadores");
        }
    }
    private void setSelectedIndicador(CapacidadAprendizaje cap,List<IndicadorAprendizaje> indicadoresSeleccionados) throws Exception{
        if(indicadoresSeleccionados == null) return;
        List<IndicadorAprendizaje> indicadores = cap.getIndicadores();
        for(IndicadorAprendizaje ind :  indicadores){
            if(indicadoresSeleccionados.contains(ind))
                ind.setSelectedIndicador(true);
        }
    }
}
