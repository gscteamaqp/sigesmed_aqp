package com.dremo.ucsm.gsc.sigesmed.core.entity.std;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Area;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@IdClass(HistorialExpedienteId.class)
@Entity
@Table(name="historial_expediente" ,schema="administrativo")
public class HistorialExpediente  implements java.io.Serializable {

    @Id 
    @Column(name="his_exp_id", unique=true, nullable=false)
    private int hisExpId;
    @Id
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="exp_id", nullable=false)
    private Expediente expediente;
    
    @Column(name="obs", length=128)
    private String observacion;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_rec", length=29,updatable=false)
    private Date fecRec;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_ate", length=29)
    private Date fecAte;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_env", length=29)
    private Date fecEnv;
    
    @Column(name="est_exp_id" )
    private int estadoId;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="est_exp_id",insertable=false,updatable=false)
    private EstadoExpediente estado;
    
    @Column(name="are_id")
    private int areaId;
    
    @ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="are_id", insertable=false,updatable=false)
    private Area area;
    
    @Column(name="res_id")
    private int resId;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="res_id", insertable=false,updatable=false)
    private Persona responsable;
    
    @OneToMany(mappedBy="historial")
    private List<DocumentoExpediente> documentos;

    public HistorialExpediente() {
    }

    public HistorialExpediente(int hisExpId, int expedienteID) {
        this.hisExpId = hisExpId;
        this.expediente = new Expediente(expedienteID);
    }
	
    public HistorialExpediente(int hisExpId, Expediente expediente) {
        this.hisExpId = hisExpId;
        this.expediente = expediente;
    }
    public HistorialExpediente(int hisExpId, Expediente expediente, String obs, int estadoID, int areaID,int resID) {
       this.hisExpId = hisExpId;
       this.expediente = expediente;
       this.observacion = obs;
       this.estadoId = estadoID;
       this.areaId = areaID;
       this.resId = resID;
    }
    public HistorialExpediente(int hisExpId, Expediente expediente, String obs, int estadoID, int areaID,int resID,Date fecEnv) {
       this.hisExpId = hisExpId;
       this.expediente = expediente;
       this.observacion = obs;
       this.estadoId = estadoID;
       this.areaId = areaID;
       this.resId = resID;
       this.fecEnv = fecEnv;
    }
   
     
    public int getHisExpId() {
        return this.hisExpId;
    }
    public void setHisExpId(int hisExpId) {
        this.hisExpId = hisExpId;
    }

    public Expediente getExpediente() {
        return this.expediente;
    }
    public void setExpediente(Expediente expediente) {
        this.expediente = expediente;
    }

    
    public String getObservacion() {
        return this.observacion;
    }
    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Date getFecRec() {
        return this.fecRec;
    }
    public void setFecRec(Date fecRec) {
        this.fecRec = fecRec;
    }
    public Date getFecAte() {
        return this.fecAte;
    }
    public void setFecAte(Date fecAte) {
        this.fecAte = fecAte;
    }
    public Date getFecEnv() {
        return this.fecEnv;
    }
    public void setFecEnv(Date fecEnv) {
        this.fecEnv = fecEnv;
    }
    
    public int getEstadoId() {
        return this.estadoId;
    }
    public void setEstadoId(int estadoId) {
        this.estadoId = estadoId;
    }
    public EstadoExpediente getEstado() {
        return this.estado;
    }
    public void setEstado(EstadoExpediente estado) {
        this.estado = estado;
    }
    
    public int getAreaId() {
        return this.areaId;
    }
    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }
    
    public Area getArea() {
        return this.area;
    }
    public void setArea(Area area) {
        this.area = area;
    }
    
    public int getResId() {
        return this.resId;
    }
    public void setResId(int resId) {
        this.resId = resId;
    }
    
    public Persona getResponsable() {
        return this.responsable;
    }
    public void setResponsable(Persona responsable) {
        this.responsable = responsable;
    }
    
    public List<DocumentoExpediente> getDocumentos() {
        return this.documentos;
    }
    public void setDocumentos(List<DocumentoExpediente> documentos) {
        this.documentos = documentos;
    }
}


