/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.estudiante.datos_estudiante.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.plan_estudios.v1.tx.*;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.PlanEstudiosDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mpf.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.AreaModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.GradoModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.NivelModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.AreaExonerada;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.Matricula;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
/**
 *
 * @author carlos
 */
public class ListarAreasTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        int orgId = 0;   
        Long matriculaId;
        
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            
            orgId = requestData.getInt("organizacionID");
            matriculaId = requestData.getLong("matriculaID");  
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo encontrar plan de estudios vigente, datos incorrectos", e.getMessage() );
        }
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        EstudianteDao estudianteDao = (EstudianteDao)FactoryDao.buildDao("mpf.EstudianteDao");
        
        
        Matricula estudiante = null;
       
        List<AreaExonerada> exoneraciones=null;
        List<AreaModel> areas = null;
        try{
            estudiante=estudianteDao.getDatosMatriculaAndPlan(matriculaId);
            exoneraciones=estudianteDao.listarCursosExonerados(estudiante.getEstudiante());
            List<Integer> exoIds = new ArrayList<>();
            if(exoneraciones!=null && exoneraciones.size()>0)
            {
                for (AreaExonerada exo : exoneraciones) {
                exoIds.add(exo.getId());
                }
           
                areas = estudianteDao.buscarAreasByPlanEstudios(estudiante.getPlanNivel().getPlaEstId(), estudiante.getGradoId(),estudiante.getSeccionId(), exoIds);
            } 
            else
            {
                areas = estudianteDao.buscarAreasByPlanEstudios(estudiante.getPlanNivel().getPlaEstId(), estudiante.getGradoId(),estudiante.getSeccionId());
            }
                    
           
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo encontrar plan de estudios vigente", e.getMessage() );
        }
        
        JSONArray aAre = new JSONArray();
        
        try{
            
            for(AreaModel a : areas){
                JSONObject oAre = new JSONObject();
                oAre.put("areaID", a.getAreaID() );
                oAre.put("area", a.getArea() );
                aAre.put(oAre);
            }            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listas las Areas", e.getMessage() );
        }    
           

        return WebResponse.crearWebResponseExito("Se listo correctamente",aAre);
        
    }    
    
    
}
