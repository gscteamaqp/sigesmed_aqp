/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.GCell;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author RE
 */
public class ReporteEgresosTx implements ITransaction{
    
    @Override
    public WebResponse execute(WebRequest wr) {        
        /*
        *  Parte para la operacion en la Base de Datos
        */        
      List<String[]> tablaC= new ArrayList<>();
      String[] cabeceraCon = new String[5];
      String[] resumenCon = new String[14];

       try{ 
           
           JSONObject request = (JSONObject)wr.getData();
           

           JSONArray tabla = request.getJSONArray("tablaEgresos");  
          
           if(tabla.length() > 0){
               for(int i = 0; i < tabla.length();i++){                                   
                 
                   Object o =  tabla.get(i);
                   JSONObject objOpe = (JSONObject)o;    
                   String cuenta = objOpe.getString("nombre");
                   
                   double enero = objOpe.optDouble("ene",0);
                   String ene = enero==0? "": String.valueOf(new BigDecimal(enero).setScale(2));
                   double febrero = objOpe.optDouble("feb",0);
                   String feb = febrero==0? "": String.valueOf(new BigDecimal(febrero).setScale(2));
                   double marzo = objOpe.optDouble("mar",0);
                   String mar = marzo==0? "": String.valueOf(new BigDecimal(marzo).setScale(2));
                   double abril = objOpe.optDouble("abr",0);
                   String abr = abril==0? "": String.valueOf(new BigDecimal(abril).setScale(2));
                   double mayo = objOpe.optDouble("may",0);
                   String may = mayo==0? "": String.valueOf(new BigDecimal(mayo).setScale(2));
                   double junio = objOpe.optDouble("jun",0);
                   String jun = junio==0? "": String.valueOf(new BigDecimal(junio).setScale(2));
                   double julio = objOpe.optDouble("jul",0);
                   String jul = julio==0? "": String.valueOf(new BigDecimal(julio).setScale(2));
                   double agosto = objOpe.optDouble("ago",0);
                   String ago = agosto==0? "": String.valueOf(new BigDecimal(agosto).setScale(2));
                   double septiembre = objOpe.optDouble("sep",0);
                   String sep = septiembre==0? "": String.valueOf(new BigDecimal(septiembre).setScale(2));
                   double octubre = objOpe.optDouble("oct",0);
                   String oct = octubre==0? "": String.valueOf(new BigDecimal(octubre).setScale(2));
                   double noviembre = objOpe.optDouble("nov",0);
                   String nov = noviembre==0? "": String.valueOf(new BigDecimal(noviembre).setScale(2));
                   double diciembre = objOpe.optDouble("dic",0);
                   String dic = diciembre==0? "": String.valueOf(new BigDecimal(diciembre).setScale(2));
                  
                   double total = objOpe.optDouble("total",0);
                   String totalCuenta = total==0? "": String.valueOf(new BigDecimal(total).setScale(2));
                   
                                                         
                    String[]row1 ={cuenta,ene,feb,mar,abr,may,jun,jul,ago,sep,oct,nov,dic,"S/ "+totalCuenta}; 
                   
                   
                    tablaC.add(row1);
               }        
           }
           
           JSONObject totales= request.getJSONObject("totales");   
           
           double enero = totales.optDouble("Enero",0);
                   String ene = enero==0? "": String.valueOf(new BigDecimal(enero).setScale(2));
                   double febrero = totales.optDouble("Febrero",0);
                   String feb = febrero==0? "": String.valueOf(new BigDecimal(febrero).setScale(2));
                   double marzo = totales.optDouble("Marzo",0);
                   String mar = marzo==0? "": String.valueOf(new BigDecimal(marzo).setScale(2));
                   double abril = totales.optDouble("Abril",0);
                   String abr = abril==0? "": String.valueOf(new BigDecimal(abril).setScale(2));
                   double mayo = totales.optDouble("Mayo",0);
                   String may = mayo==0? "": String.valueOf(new BigDecimal(mayo).setScale(2));
                   double junio = totales.optDouble("Junio",0);
                   String jun = junio==0? "": String.valueOf(new BigDecimal(junio).setScale(2));
                   double julio = totales.optDouble("Julio",0);
                   String jul = julio==0? "": String.valueOf(new BigDecimal(julio).setScale(2));
                   double agosto = totales.optDouble("Agosto",0);
                   String ago = agosto==0? "": String.valueOf(new BigDecimal(agosto).setScale(2));
                   double septiembre = totales.optDouble("Septiembre",0);
                   String sep = septiembre==0? "": String.valueOf(new BigDecimal(septiembre).setScale(2));
                   double octubre = totales.optDouble("Octubre",0);
                   String oct = octubre==0? "": String.valueOf(new BigDecimal(octubre).setScale(2));
                   double noviembre = totales.optDouble("Noviembre",0);
                   String nov = noviembre==0? "": String.valueOf(new BigDecimal(noviembre).setScale(2));
                   double diciembre = totales.optDouble("Diciembre",0);
                   String dic = diciembre==0? "": String.valueOf(new BigDecimal(diciembre).setScale(2));
                  
                   double total = totales.optDouble("totalEgresos",0);
                   String totalIngresos = total==0? "": String.valueOf(new BigDecimal(total).setScale(2));
                   
                                                         
                    String[]totalMensual ={"EGRESOS S/ ",ene,feb,mar,abr,may,jun,jul,ago,sep,oct,nov,dic,"S/ "+totalIngresos}; 
                    resumenCon= totalMensual;
       }
        catch(Exception e){
            System.out.println("No se pudo Listar el directorio interno \n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar el directorio interno ", e.getMessage() );
        }
      
      
   //   for(int i=0;i<1000;i++){
          
  //                String[]contenido ={"N","Fecha","Medio Pago","Descripción Operación","Razon Social","N° Doc.","Cod. Cta","Descripción Cuenta","Debe","Haber","Cod. Cta","Descripción Cuenta","Debe","Haber"};
   //       tablaC.add(contenido);
   //   }

     
        //Creando el reporte....        
        Mitext m = null;        
        try {
            m = new Mitext(true,"REPORTE DE EGRESOS");
            m.agregarTitulo("REPORTE DE EGRESOS DEL LIBRO CAJA  " );
            
        } catch (Exception ex) {
            System.out.println("No se pudo crear el documento \n"+ex);
            Logger.getLogger(ReporteLibroCajaTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        // m.agregarParrafo("LIBRO CAJA SIGESMED");
        //agregar tabla
       
        
        float[] columnWidthsC={4,1,1,1,1,1,1,1,1,1,1,1,1,2};
        GTabla tCuentas = new GTabla(columnWidthsC);
        tCuentas.addHeaderCell(new Cell(1, 14).setBorder(Border.NO_BORDER).add(new Paragraph("CLASIFICACION ")).setFontSize(12).setBorder(Border.NO_BORDER));
      //  tCuentas.addFooterCell(new Cell(1,14));
        String[]encabezadoC ={"EGRESOS","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Sep.","Oct.","Nov.","Dic.","TOTAL"};
        GCell[] cellC ={tCuentas.createCellCenter(1,1),tCuentas.createCellCenter(1,1),tCuentas.createCellCenter(1,1),tCuentas.createCellCenter(1,1),tCuentas.createCellCenter(1,1),
                        tCuentas.createCellCenter(1,1),tCuentas.createCellCenter(1,1),tCuentas.createCellCenter(1,1),tCuentas.createCellCenter(1,1),tCuentas.createCellCenter(1,1),
                        tCuentas.createCellCenter(1,1),tCuentas.createCellCenter(1,1),tCuentas.createCellCenter(1,1),tCuentas.createCellCenter(1,1)};
        try {
            tCuentas.build(encabezadoC);
        } catch (IOException ex) {
            Logger.getLogger(ReporteIngresosTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        
       //   String[]contenido ={"N°","Fecha","Glosa"};
      for(String [] c:tablaC){
              tCuentas.processLineCell(c,cellC);
      }
        
           
         tCuentas.processLineCell(resumenCon,cellC);
        
        //fin tabla
        
        m.agregarTabla(tCuentas);
        
//        agregar grafico
        
//        MChart chart = new MChart();
//        
//        DefaultPieDataset dataset = new DefaultPieDataset( );
//        dataset.setValue( "IPhone 5s" , new Double( 20 ) );  
//        dataset.setValue( "SamSung Grand" , new Double( 20 ) );   
//        dataset.setValue( "MotoG" , new Double( 40 ) );    
//        dataset.setValue( "Nokia Lumia" , new Double( 10 ) );  
//        
//        try {
//            m.agregarGrafico(chart.createPieChart(dataset, "Grafica de Ejemplo"), 400, 300);
//        } catch (IOException ex) {
//            System.out.println("No se pudo agregar el grafico \n"+ex);
//            Logger.getLogger(ReporteTx.class.getName()).log(Level.SEVERE, null, ex);
//        }
        
        m.cerrarDocumento();  
                                
        JSONArray miArray = new JSONArray();
        JSONObject oResponse = new JSONObject();        
        oResponse.put("datareporte",m.encodeToBase64());
        miArray.put(oResponse);                       
        
        return WebResponse.crearWebResponseExito("Se genero el reporte correctamente",miArray);        
        
    }
    
    
}
