package com.dremo.ucsm.gsc.sigesmed.core.entity.web;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="pedagogico.bandeja_tarea_escolar" )
public class BandejaTarea  implements java.io.Serializable {

    @Id
    @Column(name="ban_tar_esc_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_bandejatareaescolar", sequenceName="pedagogico.bandeja_tarea_escolar_ban_tar_esc_id_seq" )
    @GeneratedValue(generator="secuencia_bandejatareaescolar")
    private int banTarId;
    
    @Column(name="not1")
    private int nota;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_ent", nullable=false, length=29)
    private Date fecEnt;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_vis", nullable=false, length=29)
    private Date fecVis;    
    
    @Column(name="tar_esc_id")
    private int tarEscId;    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="tar_esc_id",insertable = false,updatable = false)
    private TareaEscolar tareaEscolar;
    
    @Column(name="est_tar")
    private char estado;
    
    @Column(name="per_id")
    private Integer alumnoID;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="per_id",insertable = false,updatable = false)
    private Persona alumno;
    
    @OneToMany(mappedBy="bandejaTarea",cascade=CascadeType.ALL)
    private List<TareaDocumento> documentos;
    
    @Column(name="not_lit")
    private String not_lit;
    

    public BandejaTarea() {
    }
    public BandejaTarea(int banTarId) {
        this.banTarId = banTarId;
    }
    public BandejaTarea(int banTarId, Date fecEnt,Date fecVis, int nota,char estado,int tarEscId,int alumnoID) {
       this.banTarId = banTarId;
       this.fecVis = fecVis;
       this.fecEnt = fecEnt;
       this.nota = nota;
       this.estado = estado;
       this.tarEscId = tarEscId;
       this.alumnoID = alumnoID;
    }
   
     
    public int getBanTarId() {
        return this.banTarId;
    }    
    public void setBanTarId(int banTarId) {
        this.banTarId = banTarId;
    }
    
    public Date getFecVis() {
        return this.fecVis;
    }
    public void setFecVis(Date fecVis) {
        this.fecVis = fecVis;
    }
    
    public Date getFecEnt() {
        return this.fecEnt;
    }
    public void setFecEnt(Date fecEnt) {
        this.fecEnt = fecEnt;
    }
    
    public int getNota() {
        return this.nota;
    }
    public void setNota(int nota) {
        this.nota = nota;
    }
    
    public int getTarEscId() {
        return this.tarEscId;
    }    
    public void setTarEscId(int tarEscId) {
        this.tarEscId = tarEscId;
    }
    
    public TareaEscolar getTareaEscolar() {
        return this.tareaEscolar;
    }
    public void setTareaEscolar(TareaEscolar tareaEscolar) {
        this.tareaEscolar = tareaEscolar;
    }
    
    public char getEstado(){
        return this.estado;
    }
    public void setEstado(char estado){
        this.estado = estado;
    }
    
    public Integer getAlumnoId() {
        return this.alumnoID;
    }    
    public void setAlumnoId(Integer alumnoID) {
        this.alumnoID = alumnoID;
    }
    
    public Persona getAlumno() {
        return this.alumno;
    }    
    public void setAlumno(Persona alumno) {
        this.alumno = alumno;
    }
    
    public List<TareaDocumento> getDocumentos(){
        return this.documentos;
    }
    public void setDocumentos(List<TareaDocumento> documentos){
        this.documentos = documentos;
    }

    public void setNot_lit(String not_lit) {
        this.not_lit = not_lit;
    }

    public String getNot_lit() {
        return not_lit;
    }
    
    
}


