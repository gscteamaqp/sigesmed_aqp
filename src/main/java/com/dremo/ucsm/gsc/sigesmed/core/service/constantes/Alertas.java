/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.service.constantes;

/**
 *
 * @author abel
 */
public class Alertas {
    
    /*
    * LISTA DE CODIGOS DE ALERTAS
    *
    *id_alerta = tipo(1 byte) + submodulo(2 byte)+ numero(2 byte)
    */
    private static final int TIPO_INFORMATIVA = 1;
    private static final int TIPO_ADVERTENCIA = 2;
    private static final int TIPO_ERROR = 3;
    
    public static final int WEB_NUEVA_TAREA = codigoAlerta(TIPO_INFORMATIVA,Sigesmed.MODULO_WEB,1);
    public static final int WEB_COMPARTIR_NOTA_TAREA = codigoAlerta(TIPO_INFORMATIVA,Sigesmed.MODULO_WEB,2);
    public static final int WEB_NUEVA_EVALUACION = codigoAlerta(TIPO_INFORMATIVA,Sigesmed.MODULO_WEB,3);
    
    private static int codigoAlerta(int tipo,int modulo,int alerta){
        String nuevoCodigo = ""+tipo;
        //verificando que el submodulo tenga dos byte
        nuevoCodigo += (modulo>9 ? modulo:"0"+modulo);
        //verificando que la alerta tenga dos byte
        nuevoCodigo += (alerta>9 ? alerta:"0"+alerta);
        return Integer.parseInt( nuevoCodigo );
    }
}
