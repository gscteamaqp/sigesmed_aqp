package com.dremo.ucsm.gsc.sigesmed.core.dao.mep;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.TrabajadorCargo;

/**
 * Created by Administrador on 12/10/2016.
 */
public interface TrabajadorCargoDao extends GenericDao<TrabajadorCargo>{
    TrabajadorCargo buscarPorId(int id);
    TrabajadorCargo buscarPorNombre(String nombre);
}
