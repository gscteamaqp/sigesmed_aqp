package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.ComentarioTemaCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.capacitacion.AdjuntoComentarioTemaDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.AdjuntoComentarioTema;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.ComentarioTemaCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class EditarComentarioTemaTx implements ITransaction {
    
    private static final Logger logger = Logger.getLogger(EditarComentarioTemaTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();
            ComentarioTemaCapacitacionDao comentarioTemaCapacitacionDao = (ComentarioTemaCapacitacionDao) FactoryDao.buildDao("capacitacion.ComentarioTemaCapacitacionDao");
            ComentarioTemaCapacitacion comentario = comentarioTemaCapacitacionDao.buscarPorId(data.getInt("cod"));
            
            comentario.setCom(data.getString("com"));            
            comentario.setUsuMod(data.getInt("usuMod"));
            comentario.setFecMod(new Date());
            
            comentarioTemaCapacitacionDao.update(comentario);
            
            JSONArray attachments = data.getJSONArray("del");
            
            if(attachments.length() > 0) {
                AdjuntoComentarioTemaDaoHibernate adjuntoComentarioTemaDao = (AdjuntoComentarioTemaDaoHibernate) FactoryDao.buildDao("capacitacion.AdjuntoComentarioTemaDao");
                String route = ServicioREST.PATH_SIGESMED + File.separator + "archivos" + File.separator + "capacitacion_comentarios_adjuntos" + File.separator;
                
                for(int i = 0;i < attachments.length();i++) {
                    Path path = Paths.get(route + attachments.getJSONObject(i).getString("nom"));
                    boolean success = Files.deleteIfExists(path);
                    System.out.println("Estado de Eliminación: " + attachments.getJSONObject(i).getInt("cod") + " - " + success);
                    
                    AdjuntoComentarioTema adjunto = adjuntoComentarioTemaDao.buscarPorId(attachments.getJSONObject(i).getInt("cod"));
                    adjuntoComentarioTemaDao.deleteAbsolute(adjunto);
                }
            }
            
            attachments = data.getJSONArray("new");
            
            if(attachments.length() > 0) {
                AdjuntoComentarioTemaDaoHibernate adjuntoComentarioTemaDao = (AdjuntoComentarioTemaDaoHibernate) FactoryDao.buildDao("capacitacion.AdjuntoComentarioTemaDao");

                for (int i = 0; i < attachments.length(); i++) {
                    AdjuntoComentarioTema adjunto = new AdjuntoComentarioTema("Nombre_archivo", comentario, comentario.getPerId());
                    adjuntoComentarioTemaDao.insert(adjunto);
                    
                    FileJsonObject file = new FileJsonObject(attachments.getJSONObject(i).optJSONObject("arc"),
                            "tem_" + comentario.getTemCurCapId() + "_com_" + comentario.getComTemCapId() + "_adj_" + adjunto.getAdjComTemId() + "_" + attachments.getJSONObject(i).getString("nom"));
                    
                    BuildFile.buildFromBase64("capacitacion_comentarios_adjuntos", file.getName(), file.getData());
                    adjunto.setNom(file.getName());
                    adjuntoComentarioTemaDao.update(adjunto);
                }
            }             
            
            return WebResponse.crearWebResponseExito("Exito al editar el comentario", WebResponse.OK_RESPONSE);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "editarComentario", e);
            return WebResponse.crearWebResponseError("Error al editar el comentario", WebResponse.BAD_RESPONSE);
        }
    }
}
