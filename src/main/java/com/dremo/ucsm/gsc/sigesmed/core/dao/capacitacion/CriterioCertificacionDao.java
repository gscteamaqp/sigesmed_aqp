package com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.CriterioCertificacion;
import java.util.List;

public interface CriterioCertificacionDao extends GenericDao<CriterioCertificacion> {
    List<CriterioCertificacion> buscarPorCapacitacion(int codCap);
}
