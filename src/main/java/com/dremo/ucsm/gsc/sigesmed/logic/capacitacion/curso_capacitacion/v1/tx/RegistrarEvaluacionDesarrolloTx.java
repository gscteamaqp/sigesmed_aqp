package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.AdjuntoEvaluacionDesarrolloDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.EvaluacionDesarrolloDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.AdjuntoEvaluacionDesarrollo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.EvaluacionDesarrollo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class RegistrarEvaluacionDesarrolloTx implements ITransaction {
    
    private static final Logger logger = Logger.getLogger(RegistrarEvaluacionDesarrolloTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();   
            int des = data.getInt("des");
            int doc = data.getInt("doc");
            
            EvaluacionDesarrolloDao evaluacionDesarrolloDao = (EvaluacionDesarrolloDao) FactoryDao.buildDao("capacitacion.EvaluacionDesarrolloDao");
            EvaluacionDesarrollo evaDes = evaluacionDesarrolloDao.buscarPorId(des, doc);            
            evaDes.setEstReg('E');
            evaDes.setFecEnt(new Date());
            evaluacionDesarrolloDao.update(evaDes);
            
            JSONArray adjArray = data.getJSONArray("adj");
            AdjuntoEvaluacionDesarrolloDao adjuntoTemaCapacitacionDao = (AdjuntoEvaluacionDesarrolloDao) FactoryDao.buildDao("capacitacion.AdjuntoEvaluacionDesarrolloDao");

            for (int i = 0; i < adjArray.length(); i++) {
                AdjuntoEvaluacionDesarrollo adjunto = new AdjuntoEvaluacionDesarrollo("Nombre_archivo", evaDes);
                adjuntoTemaCapacitacionDao.insert(adjunto);

                FileJsonObject file = new FileJsonObject(adjArray.getJSONObject(i).optJSONObject("arc"),
                        "des_" + des + "_doc_" + doc + "_adj_" + adjunto.getAdjEvaDesId() + "_" + adjArray.getJSONObject(i).getString("nom"));
                BuildFile.buildFromBase64(HelpTraining.attachments_Evaluation_Development, file.getName(), file.getData());
                adjunto.setNom(file.getName());
                adjuntoTemaCapacitacionDao.update(adjunto);
            }
            
            return WebResponse.crearWebResponseExito("La evaluación del desarrollo fue creado correctamente", WebResponse.OK_RESPONSE);
        } catch (Exception e){
            logger.log(Level.SEVERE,"registrarEvaluacionDesarrollo",e);
            return WebResponse.crearWebResponseError("No se pudo registrar la evaluación del desarrollo",WebResponse.BAD_RESPONSE);
        }
    }
}
