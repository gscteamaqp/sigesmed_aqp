/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.cantidadModel;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.LibroCajaDao;
import com.dremo.ucsm.gsc.sigesmed.util.GCell;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author RE
 */
public class ListarEgresosTx implements ITransaction{
    
    @Override
    public WebResponse execute(WebRequest wr) {   
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int organizacionID = 0;
        Date desde = null;
        Date hasta = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            organizacionID = requestData.getInt("organizacionID");
            
            if(!requestData.getString("desde").contentEquals(""))
                desde = new SimpleDateFormat("dd/M/yyyy").parse( requestData.getString("desde"));
            if(!requestData.getString("hasta").contentEquals(""))
                hasta = new SimpleDateFormat("dd/M/yyyy HH:mm:ss").parse( requestData.getString("hasta") + " 23:59:59" );
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo realizar la estadistica", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        List<cantidadModel> compras = null;
    
        LibroCajaDao hisDao = (LibroCajaDao)FactoryDao.buildDao("sci.LibroCajaDao");
        try{
            compras = hisDao.cantidadGatosEnAreaPorOrganizacionYFecha(organizacionID, desde, hasta);
          
        }catch(Exception e){
            System.out.println("E"+e);
            return WebResponse.crearWebResponseError("No se pudo realizar la estadistica Consulta", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
         
        JSONArray aAreas = new JSONArray();
        
        
        JSONArray nRegistros = new JSONArray();        
                
      
        
        for(cantidadModel a:compras){
            
            aAreas.put(a.nombre);
            nRegistros.put(a.num1);
            
          
            
        }
        
        for(int j=0;j<aAreas.length();j++){
            System.out.println(aAreas.get(j));    
            System.out.println(nRegistros.get(j)); 
        }
        
        
        
        JSONObject res = new JSONObject();
        res.put("labels", aAreas);
        res.put("registros", nRegistros);        
     
     
        return WebResponse.crearWebResponseExito("Se Listo correctamente",res);        
        //Fin     
        
    }
    
    
}
