/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate;

import com.dremo.ucsm.gsc.sigesmed.core.dao.UsuarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author abel
 */
public class UsuarioDaoHibernate extends GenericDaoHibernate<Usuario> implements UsuarioDao {

    @Override
    public List<Usuario> buscarConRolYOrganizacion() {
        List<Usuario> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar Usuarios
            String hql = "SELECT DISTINCT u FROM Usuario u JOIN FETCH u.persona p LEFT JOIN FETCH u.sessiones s JOIN FETCH s.organizacion JOIN FETCH s.rol r WHERE u.estReg!='E' and s.estReg!='E' ORDER BY u.fecCre";
            Query query = session.createQuery(hql);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los Usuarios \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los Usuarios \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }/*
    @Override
    public List<UsuarioSession> buscarConRolYOrganizacion() {
        List<UsuarioSession> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar Usuarios
            String hql = "SELECT us FROM UsuarioSession us JOIN FETCH us.usuario u JOIN FETCH us.persona p JOIN FETCH us.organizacion JOIN FETCH us.rol r WHERE u.estReg!='E' and us.estReg!='E' ORDER BY u";
            Query query = session.createQuery(hql);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los Usuarios \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los Usuarios \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }*/
    @Override
    public List<Usuario> buscarConRolPorOrganizacion(int orgID) {
        List<Usuario> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar Usuarios
            String hql = "SELECT DISTINCT u FROM Usuario u JOIN FETCH u.persona p LEFT JOIN FETCH u.sessiones s JOIN FETCH s.rol r LEFT JOIN FETCH s.area WHERE u.estReg!='E' and s.organizacion.orgId=:p1 and s.estReg!='E' ORDER BY u.fecCre";
            //String hql = "SELECT us FROM UsuarioSession us JOIN FETCH us.usuario u JOIN FETCH us.rol r LEFT JOIN FETCH us.area WHERE u.estReg!='E' and us.estReg!='E' and us.organizacion.orgId=:p1 ORDER BY us.fecCre" ;
            Query query = session.createQuery(hql);
            query.setParameter("p1", orgID);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los Usuarios por organizacion\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los Usuarios por organizacion\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public List<UsuarioSession> buscarConRolPorArea(int areID) {
        List<UsuarioSession> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar Usuarios
            String hql = "SELECT us FROM UsuarioSession us JOIN FETCH us.persona p JOIN FETCH us.rol r WHERE us.estReg!='E' and us.area.areId=:p1 ORDER BY us.fecCre" ;
            Query query = session.createQuery(hql);
            query.setParameter("p1", areID);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los Usuarios por Area \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los Usuarios por Area \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public Usuario buscarPorUsuarioYPassword(String nombreUsuario, String password) {
        Usuario objeto = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            
            //String hql = "SELECT u FROM Usuario u JOIN FETCH u.organizacion JOIN FETCH u.rol r WHERE u.nom =:nombre AND u.pas =:password and u.estReg='A'";
            String hql = "SELECT DISTINCT u FROM Usuario u LEFT JOIN FETCH u.sessiones us JOIN FETCH us.organizacion JOIN FETCH us.rol LEFT JOIN FETCH us.area r WHERE u.nom =:nombre AND u.pas =:password and u.estReg='A'";
        
            Query query = session.createQuery(hql);
            query.setParameter("nombre", nombreUsuario);
            query.setParameter("password", password);
            query.setMaxResults(1);
            //buscando 
            objeto =  (Usuario)query.uniqueResult(); 
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar al Usuario \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar al Usuario \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objeto;
    }
    @Override
    public Usuario buscarPorId(int idUsuario) {
        Usuario objeto = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            
            //String hql = "SELECT u FROM Usuario u JOIN FETCH u.organizacion JOIN FETCH u.rol r WHERE u.nom =:nombre AND u.pas =:password and u.estReg='A'";
            String hql = "SELECT DISTINCT u FROM Usuario u WHERE u.usuId =:idUser";
        
            Query query = session.createQuery(hql);
            query.setInteger("idUser", idUsuario);
            query.setMaxResults(1);
            //buscando 
            objeto =  (Usuario)query.uniqueResult(); 
        
        }catch(Exception e){
            throw e;   
        }
        finally{
            session.close();
        }
        return objeto;
    }
    
    public List<UsuarioSession> buscarPorUsuario(String nombreUsuario) {
        List<UsuarioSession> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            
            String hql = "SELECT us FROM UsuarioSession us JOIN FETCH us.usuario u JOIN FETCH us.organizacion JOIN FETCH us.rol r WHERE u.nom =:nombre and u.estReg='A' and us.estReg='A' ORDER By us.organizacion.orgId";
        
            Query query = session.createQuery(hql);
            query.setParameter("nombre", nombreUsuario);
            //buscando 
            objetos =  query.list(); 
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar al Usuario \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar al Usuario \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    
    @Override
    public void insertarSession(UsuarioSession usuSession){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            session.persist(usuSession);
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo registrar un usuario session \\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo registrar un usuario session \\n"+ e.getMessage());
        }
        finally{
            session.close();
        }
    }
    
    @Override
    public void eliminarSessiones(int usuarioID,int usuarioModificadorID){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            //eliminando tipo tramite
            String hql = "UPDATE UsuarioSession u SET u.estReg='E', u.usuMod=:p2,u.fecMod=:p3 WHERE u.usuario.usuId =:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", usuarioID);
            query.setParameter("p2", usuarioModificadorID);
            query.setParameter("p3", new Date());
            
            query.executeUpdate();    
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo eliminar las sessiones del usuario\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    public void cambiarPassword(int usuarioID,String password){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            //eliminando tipo tramite
            String hql = "UPDATE Usuario u SET u.pas=:p2 WHERE u.usuId =:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", usuarioID);
            query.setParameter("p2", password);
            
            query.executeUpdate();    
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo cambiar la contraseña\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
}
