/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mnt;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mnt.AyudaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mnt.Ayuda;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mnt.PasosAyuda;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Administrador
 */
public class AyudaDaoHibernate extends GenericDaoHibernate<Ayuda> implements AyudaDao{

    @Override
    public int buscarIdAyudaPorFuncionalidad(int funcionID,char tipo) {
        int id;
        Session session=HibernateUtil.getSessionFactory().openSession();
        Transaction t=session.beginTransaction();
        try{
            //Listar ayudas por funcionalidad
            String hql="SELECT A.ayudaId FROM Ayuda A WHERE A.funcionSistemaId.funSisId=:p1 and A.ayuTip=:p2 and A.estReg='1'";
            Query query=session.createQuery(hql);
            query.setParameter("p1",funcionID);
            query.setParameter("p2",tipo);
            id=((int)query.uniqueResult());
        }catch(Exception e){
            t.rollback();
            System.err.println("No se puede listar las Ayudas \\n "+e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las Ayudas \\n" +e.getMessage());
        }finally{
            session.close();
        }
        return id;
    }

    @Override
    public List<PasosAyuda> buscarPasosPorAyuda(int ayudaID) {
        List<PasosAyuda> pasos=new ArrayList<PasosAyuda>();
        Session session=HibernateUtil.getSessionFactory().openSession();
        Transaction t=session.beginTransaction();
        
        try{
            String hql="SELECT p FROM PasosAyuda p JOIN FETCH p.ayuda WHERE p.ayuda.ayudaId=:p1 and p.estReg='1'";
            Query query=session.createQuery(hql);
            query.setParameter("p1",ayudaID);
            pasos=query.list();         
            t.commit();
        }catch(Exception e){
             t.rollback();
            System.out.println("No se puede listar los pasos \\n "+e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los pasos\\n" +e.getMessage());
        }finally{
              session.close();
        }                
        return pasos;
    }
    
    @Override
    public Ayuda buscarAyuda(int ayudaID){
        Session session=HibernateUtil.getSessionFactory().openSession();
        Transaction t=session.beginTransaction();
        Ayuda output;
         try{
            String hql="FROM Ayuda where ayudaId=:p1 and estReg='1'";
            Query query=session.createQuery(hql);
            query.setParameter("p1",ayudaID);
            query.setMaxResults(1);
            output=(Ayuda)query.uniqueResult();         
            t.commit();
        }catch(Exception e){
             t.rollback();
            System.out.println("No se puede obtener la ayuda \\n "+e.getMessage());
            throw new UnsupportedOperationException("No se pudo obtener la ayuda\\n" +e.getMessage());
        }finally{
              session.close();
        }          
        return output;
    }
    
}
