package com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CapacidadAprendizaje;

/**
 * Created by Administrador on 13/10/2016.
 */
public interface CapacidadAprendizajeDao extends GenericDao<CapacidadAprendizaje> {
    CapacidadAprendizaje buscarPorId(int id);
}
