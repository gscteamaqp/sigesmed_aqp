/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseno_curricular.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.DisenoCurricularDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Grado;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author HernanF
 */
public class EliminarGradoTx implements ITransaction{
    
    @Override
    public WebResponse execute(WebRequest wr){
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        
        int gradoID;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            gradoID = requestData.getInt("gradoID");
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el grado, datos incorrectos", e.getMessage() );
        }
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        DisenoCurricularDao disenoDao = (DisenoCurricularDao)FactoryDao.buildDao("mech.DisenoCurricularDao");
        
        try{
            Grado grado = new Grado(gradoID);
            disenoDao.eliminarGrado(grado);
        }catch(Exception e){
            System.out.println("No se pudo eliminar el grado\n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el  grado", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El grado seleccionada se elimino correctamente");
        //Fin
    }
    
}
