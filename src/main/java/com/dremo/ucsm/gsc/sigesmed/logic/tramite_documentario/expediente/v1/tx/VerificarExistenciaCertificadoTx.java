/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.UsuarioDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author G-16
 */
public class VerificarExistenciaCertificadoTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
          /*
        *   Parte para la lectura, verificacion y validacion de datos
         */


     

        UsuarioDaoHibernate userdao = new UsuarioDaoHibernate();
        Usuario user;
      
        try {
            JSONObject requestData = (JSONObject) wr.getData();

            
            int responsableId = requestData.getInt("UserId");
            user = userdao.buscarPorId(responsableId);
             System.out.println("cernom:"+user.getNomCert());
           //verificando q exista un certificado para el responsable
            if (user.getNomCert()== null){
                return WebResponse.crearWebResponseError("Nose cuenta con un Certificado para el Usuario");
                         
            }
             //verificando ruta del certificado
          
            //leendo los documentos           
        } catch (Exception e) {
            System.out.println(e);
            return WebResponse.crearWebResponseError("Error con la verificacion del certificado", e.getMessage());
        }

       
        /*
        *  Repuesta Correcta
         */
           JSONObject oResponse = new JSONObject();
        oResponse.put("nombreCer",user.getNomCert());
      
        
        return WebResponse.crearWebResponseExito("El Usuario cuenta con Certificado",oResponse);
        //Fin
    }

    
}
