/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.ItemFileDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.ItemFile;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ListarDocumentosGestionTx implements ITransaction {

    private JSONArray listDirs = new JSONArray();
    DateFormat fechaHora = new SimpleDateFormat("yyyy-MM-dd");
    List<ItemFile> tabladocs;
    ItemFile padreTmp;
    int codPadre = 0;
    int idPadre = 0;
    String idPadreCad = "";
    
    public JSONObject getPropiedades(int id){
        JSONObject prop = new JSONObject();
        for (int i = 0; i < listDirs.length(); i++) {
            if (listDirs.getJSONObject(i).getInt("codFile") == id) {
                prop.put("roleId", listDirs.getJSONObject(i).getInt("codFile"));
                prop.put("roleName", listDirs.getJSONObject(i).getString("nomFile"));
                prop.put("rolePad", listDirs.getJSONObject(i).getInt("codPadre"));
                prop.put("roleCat", listDirs.getJSONObject(i).getString("catFile"));
                prop.put("roleTam", listDirs.getJSONObject(i).getDouble("tamFile"));
                prop.put("roleVer", listDirs.getJSONObject(i).getInt("verFile"));
                prop.put("roleFec", listDirs.getJSONObject(i).getLong("fecFile"));
                prop.put("roleUrl", listDirs.getJSONObject(i).getString("urlFile"));
                prop.put("rolePro", listDirs.getJSONObject(i).getString("iteProt"));
                return prop;
            }
        }
        return prop;
    }
    public JSONArray getHijos(int idPadre) {
        JSONArray baTmp = new JSONArray();
        for (int i = 0; i < listDirs.length(); i++) {
            if (listDirs.getJSONObject(i).getInt("codFile") == idPadre) {
                baTmp = listDirs.getJSONObject(i).getJSONArray("arrayHijos");
                return baTmp;
            }
        }
        return baTmp;
    }

    public boolean containsKey(int idPadre) {
        for (int i = 0; i < listDirs.length(); i++) {
            if (listDirs.getJSONObject(i).getInt("codFile") == idPadre) {
                return true;
            }
        }
        return false;
    }

    public void prepararFiles(List<ItemFile> docs) {
        for (ItemFile documento : docs) {

            //Preparamos el hijo y el padre el ItemFile encapsulado en un Object y pueda pasar
            //Para este caso solo pasaremos el id como si fuera todo el ItemFile
            int idHijo = documento.getIteIde();
            
            ItemFile filePadre = null;

            if(documento.getItePadIde()!=null){
                ItemFile padreFile = documento.getItePadIde();
                //buscar el padre con el codigo otorgado
                ItemFileDao itemFileDao = (ItemFileDao)FactoryDao.buildDao("rdg.ItemFileDao");
            
                //Buscamos el itemfile con el codigo del padre
                filePadre = itemFileDao.buscarPorID(padreFile.getIteIde());
            }
            
            if (filePadre != null) {
                idPadre = filePadre.getIteIde();
                idPadreCad = idPadre + "";
            }else{
                codPadre = idHijo;
            }
            
            JSONArray listChildrenFinal = new JSONArray();
            JSONObject objHijo = new JSONObject();
            objHijo.put("codFile", idHijo);
            objHijo.put("nomFile", documento.getIteNom());
            if(filePadre!=null){
                objHijo.put("codPadre",documento.getItePadIde().getIteIde());
            }else{
                objHijo.put("codPadre",0);
            }
            objHijo.put("tamFile",documento.getIteTam()); //Double
            objHijo.put("catFile",documento.getIteCodCat()); //String
            objHijo.put("fecFile",documento.getIteFecCre().getTime()); //String
            objHijo.put("verFile",documento.getIteVer().intValue()); //Int
            objHijo.put("urlFile", documento.getIteUrlDes()); //String
            
            if(documento.getIteProt()!=null){
                String temp=documento.getIteProt();
                
                objHijo.put("iteProt", temp.replace(" ","")); //String
            }else
                objHijo.put("iteProt", ""); //String
//            if(documento.getIteCodPro()!=null){
//                objHijo.put("proFile",documento.getIteCodPro().getPrtCod());
//            }else{
//                objHijo.put("proFile","0");
//            }
            
            objHijo.put("arrayHijos", listChildrenFinal);

            if (filePadre != null) {
                //Preguntamos si existe ya el padre
                if (containsKey(idPadre)) {
                //Si existe retornamos el valor y agregamos una lista
                    //buscar el objTmp en la lista
                    JSONArray hijos = getHijos(idPadre);
                    //agregamos el nuevo hijo (no importa el orden)
                    hijos.put(objHijo);
                } else {
                    //Preparamos su primer hijo
                    JSONArray listChildren = new JSONArray();
                    listChildren.put(objHijo);
                    //el array tiene como id el nombre del padre
                    //listChildren.opt(documento.getItePadIde().getIteIde()); /*revisar el id*/

                    JSONObject padre = new JSONObject();
                    
                    //El padre de documento es el file Padre
                    
                    padre.put("arrayHijos", listChildren);
                    padre.put("codFile", filePadre.getIteIde());
                    padre.put("nomFile", filePadre.getIteNom());
                    if(filePadre.getItePadIde()!=null){
                        padre.put("codPadre", filePadre.getItePadIde().getIteIde());
                    }else{
                        padre.put("codPadre", 0);
                    }
                    
                    //Para este caso damos el codigo de proteccion
                    padre.put("tamFile", filePadre.getIteTam());
                    padre.put("catFile", filePadre.getIteCodCat());
                    padre.put("fecFile", filePadre.getIteFecCre().getTime());
                    padre.put("verFile", filePadre.getIteVer());
                    padre.put("urlFile", filePadre.getIteUrlDes());
                    if(documento.getIteProt()!=null){
                        String temp=documento.getIteProt();
                        objHijo.put("iteProt", temp.replace(" ","")); //String
                    }else
                        padre.put("iteProt", ""); //String
                    
                    //No existe el padre por lo cual agregar al hashMap con un alista vacio de hijos
                    listDirs.put(padre);
                }
            } else {
                listDirs.put(objHijo);
            }
        }
    }

    public void printListDir() {
        for (int i = 0; i < listDirs.length(); i++) {
            int cod = listDirs.getJSONObject(i).getInt("codFile");
            String nom = listDirs.getJSONObject(i).getString("nomFile");

            System.out.println("CodPad: " + cod + " NomPad: " + nom);

            JSONArray hijos = listDirs.getJSONObject(i).getJSONArray("arrayHijos");
            for (int j = 0; j < hijos.length(); j++) {
                int codHijo = hijos.getJSONObject(j).getInt("codFile");
                String nomHijo = hijos.getJSONObject(j).getString("nomFile");
                System.out.println("CodHijo: " + codHijo + " NomHijo: " + nomHijo);
            }
        }
    }

    public void displayDirectoryContents(JSONArray nlistDirs, JSONArray arbolTmp) {

        for (int i = 0; i < nlistDirs.length(); i++) {
            JSONObject hijoAct = nlistDirs.getJSONObject(i);
            int idHijo = hijoAct.getInt("codFile");
            JSONArray children = new JSONArray();
            if (idHijo != 10000) {
                children = getHijos(idHijo);

                if (children.length()!= 0) {
                    //Para agregar al arbol debemos tener encapsularse como baseObjeto
                    JSONObject propDir = new JSONObject();

                    //Nombre de Directorio
                    //Del primer hijo obtenemos las propiedades del padre
                    propDir.put("roleName", hijoAct.getString("nomFile"));
                    propDir.put("roleId", hijoAct.getInt("codFile"));
                    propDir.put("roleTam", hijoAct.getDouble("tamFile"));
                    propDir.put("roleCat", hijoAct.getString("catFile"));
                    propDir.put("roleVer", hijoAct.getInt("verFile"));
                    propDir.put("roleFec", hijoAct.getLong("fecFile"));
                    propDir.put("rolePad", hijoAct.getInt("codPadre"));
                    propDir.put("roleUrl", hijoAct.getString("urlFile"));
                    propDir.put("rolePro", hijoAct.getString("iteProt"));

                    //creamos un nuevo baseArray por ser directorio
                    JSONArray contenedorArchivos = new JSONArray();
                    propDir.put("children", contenedorArchivos);

                    //agregarlo a tu arbol actual
                    arbolTmp.put(propDir);

                    //el nuevo parametro es ahora el arbol contenedor
                    //cuidado con parametros copia (depurar)
                    displayDirectoryContents(children, contenedorArchivos);
                } else {
                    //Al ser un archivo debemos acompañar con uss propiedades de fecha y tamaño y tipo
                    JSONObject propArch = new JSONObject();

                    //Agregar el nombre del archivo
                    propArch.put("roleName", hijoAct.getString("nomFile"));
                    propArch.put("roleId", hijoAct.getInt("codFile"));
                    propArch.put("rolePad", hijoAct.getInt("codPadre"));
                    propArch.put("roleCat", hijoAct.getString("catFile"));
                    propArch.put("roleTam", hijoAct.getDouble("tamFile"));
                    propArch.put("roleVer", hijoAct.getInt("verFile"));
                    propArch.put("roleFec", hijoAct.getLong("fecFile"));
                    propArch.put("roleUrl", hijoAct.getString("urlFile"));
                    propArch.put("rolePro", hijoAct.getString("iteProt"));
                    //un archivo tendra una lista de children vacio
                    JSONArray contenedorArchivos = new JSONArray();
                    propArch.put("children", contenedorArchivos);

                    arbolTmp.put(propArch);
                }
            }

        }

    }

    @Override
    public WebResponse execute(WebRequest wr) {
    
        /*
         *   Parte para la lectura, verificacion y validacion de datos
         */

        try {
            
        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo obtener los parametros para listar las carpetas", e.getMessage());
        }
        //Fin        

        /*
         *  Parte para la operacion en la Base de Datos
         */
        List<ItemFile> docs = null;
        ItemFileDao iteFilDao = (ItemFileDao)FactoryDao.buildDao("rdg.ItemFileDao");
        //docs = iteFilDao.buscarTodos(ItemFile.class);
        docs= iteFilDao.buscarCarpetas();

        JSONArray mapaDir = new JSONArray();
        JSONArray mapaDirHijos = new JSONArray();
        if (docs.size() != 0) {
            prepararFiles(docs);
            printListDir();
            //Para agregar al arbol debemos tener encapsularse como baseObjeto
            JSONObject propDir = new JSONObject();
            propDir = getPropiedades(codPadre);
            JSONArray hijos = getHijos(codPadre);
            displayDirectoryContents(hijos, mapaDirHijos);
            propDir.put("children", mapaDirHijos);

            //agregarlo a tu arbol actual
            mapaDir.put(propDir);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente", mapaDir);
        //Fin
    }

}
