package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.EvaluacionCursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.PreguntaEvaluacionCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.PreguntaEvaluacionCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class ReasignarPreguntaTx implements ITransaction {
    
    private static final Logger logger = Logger.getLogger(ReasignarPreguntaTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();

            EvaluacionCursoCapacitacionDao evaluacionCursoCapacitacionDao = (EvaluacionCursoCapacitacionDao) FactoryDao.buildDao("capacitacion.EvaluacionCursoCapacitacionDao");
            PreguntaEvaluacionCapacitacionDao preguntaEvaluacionCapacitacionDao = (PreguntaEvaluacionCapacitacionDao) FactoryDao.buildDao("capacitacion.PreguntaEvaluacionCapacitacionDao");
            PreguntaEvaluacionCapacitacion pregunta = preguntaEvaluacionCapacitacionDao.buscarPorId(data.getInt("id"));
            
            pregunta.setPreEvaCapId(0);
            pregunta.setFecMod(new Date());
            pregunta.setUsuMod(data.getInt("usuMod"));
            pregunta.setEvaluacionCursoCapacitacion(evaluacionCursoCapacitacionDao.buscarPorId(data.getInt("eva")));
            
            preguntaEvaluacionCapacitacionDao.insert(pregunta);
            
            return WebResponse.crearWebResponseExito("La pregunta de la evaluación fue reasignada correctamente", WebResponse.OK_RESPONSE);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "registrarEvaluacion", e);
            return WebResponse.crearWebResponseError("No se pudo reasignar la pregunta de la evaluación", WebResponse.BAD_RESPONSE);
        }
    }
}
