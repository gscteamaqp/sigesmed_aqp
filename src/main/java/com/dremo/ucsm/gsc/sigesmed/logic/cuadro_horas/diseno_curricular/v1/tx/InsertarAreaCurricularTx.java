/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseno_curricular.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.DisenoCurricularDao;
/**
 *
 * @author abel
 */
public class InsertarAreaCurricularTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
       /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        AreaCurricular nuevo = null;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            int areaID = requestData.optInt("areaID");
            int disenoID = requestData.getInt("diseñoID");
            boolean tipo = requestData.getBoolean("tipo");
            String abreviacion = requestData.getString("abreviacion");
            String nombre = requestData.getString("nombre");
            String descripcion = requestData.getString("descripcion");
            
            nuevo = new AreaCurricular(areaID,abreviacion , nombre, descripcion, tipo, disenoID, new Date(), wr.getIdUsuario(), 'A');
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar el Area Curricular, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        DisenoCurricularDao disenoDao = (DisenoCurricularDao)FactoryDao.buildDao("mech.DisenoCurricularDao");
        try{
            disenoDao.insertarArea(nuevo);
        }catch(Exception e){
            System.out.println("No se pudo registrar el Area Curricular\n"+e);
            return WebResponse.crearWebResponseError("No se pudo registrar el Area Curricular", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("areaID",nuevo.getAreCurId());
        oResponse.put("fecha",nuevo.getFecMod().toString());
        return WebResponse.crearWebResponseExito("El registro del Area Curricular se realizo correctamente", oResponse);
        //Fin
    }    
    
    
}
