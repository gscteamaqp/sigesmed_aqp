/*

 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.std;

import com.dremo.ucsm.gsc.sigesmed.core.dao.*;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.EntidadCantidadModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.DocumentoExpediente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.HistorialExpediente;
import java.util.Date;
import java.util.List;
import java.util.Map;
/**
 *
 * @author abel
 */
public interface HistorialExpedienteDao extends GenericDao<HistorialExpediente>{
    public HistorialExpediente buscarUltimoHistorial(int expedienteID);
    public HistorialExpediente buscarHistorialAnterior(int expedienteID, int historialID);
    public List<HistorialExpediente> buscarPorExpediente(int expedienteID);
    public List<HistorialExpediente> buscarPorExpedienteCodigo(int expedienteID);
    
    public int numeroDocumentos(int expedienteID);
    public void insertarDocumento(DocumentoExpediente documento);
    public void insertarDocumentos(List<DocumentoExpediente> documentos);
    
    
    public List<HistorialExpediente> buscarPorAreaYEstado(int areaID,int estadoID);
    public List<HistorialExpediente> buscarPorAreaYEstado(int areaID,int[] estadosID);
    public List<HistorialExpediente> buscarPorDerivadosPorArea(int areaID);
    
    public List<HistorialExpediente> buscarPorAreaYEstadoEnFinalizados(int areaID,int estadoID);
    public List<HistorialExpediente> buscarPorAreaYEstadoEnFinalizados(int areaID,int[] estadosID);
    
    public void actualizarEstado(HistorialExpediente historial,int estado);
    public void actualizarEstadoVarios(List<HistorialExpediente> historiales,int estado);
    public void insertarEstado(HistorialExpediente historiales);
    public void insertarEstadoVarios(List<HistorialExpediente> historiales);
    
    public List<EntidadCantidadModel> cantidadExpedientesEnAreaPorOrganizacionYFecha(int organizacion,Date desde, Date hasta);
    public List<EntidadCantidadModel> cantidadExpedientesEnAreaPorOrganizacionYEstadoYFecha(int organizacion,int estadoId,Date desde, Date hasta);
    public List<EntidadCantidadModel> cantidadExpedientesEnAreaPorOrganizacionYNoEstadoYFecha(int organizacion,int estadoId,Date desde, Date hasta);
    public List<EntidadCantidadModel> cantidadExpedientesDeTrabajadorPorAreaYFecha(int area,Date desde, Date hasta);
}

