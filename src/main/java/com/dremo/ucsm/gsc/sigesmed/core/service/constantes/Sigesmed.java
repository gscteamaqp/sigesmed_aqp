/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.service.constantes;

/**
 *
 * @author Administrador
 */
public class Sigesmed {
    
    public static final String NOM_PROYECTO = "sigesmed";
    public static final String UBI_SERVICIOS = "service/rest";
    public static final String UBI_LOGICA = "logic";
    
    public static final String COXTEXTO_PROYECTO = "com.dremo.ucsm.gsc.sigesmed";
    public static final String COXTEXTO_PROYECTO_URL = "com/dremo/ucsm/gsc/sigesmed";
    
    
    public static final String UBI_ARCHIVOS = "archivos";
    public static final String FORMATOS_CPE = "archivos/control_personal/horas_efectivas_formatos";
    
    /*
    * LISTA DE MODULOS
    */
    public static final int MODULO_CONFIGURACION = 1;
    public static final int MODULO_TRAMITE_DOCUMENTARIO = 2;
    public static final int MODULO_DIRECTORIO = 3;
    public static final int MODULO_OTROS = 4;
    public static final int MODULO_SISTEMA_CONTABLE_INSTITUCIONAL=5;
    public static final int MODULO_ELABORACION_CUADRO_HORAS = 6;
    public static final int MODULO_DOCUMENTOS_GESTION = 7;
    public static final int MODULO_MONITOREO_DOCUMENTOS_GESTION = 8;
    public static final int SISTEMA_MONITOREO_ACOMPANIAMIENTO = 9;
    public static final int SUBMODULO_MAESTRO = 12;
    public static final int SUBMODULO_ACADEMICO = 13;
    public static final int SUBMODULO_CAPACITACION = 14;
    public static final int MODULO_EVALUACION_PERSONAL = 16;
    public static final int MODULO_WEB = 18;
    public static final int SUBMODULO_COMISIONES = 19;
    public static final int MODULO_ESCALAFON = 20;
    public static final int SISTEMA_ARCHIVO_DIGITAL = 24;
    public static final int MODULO_DOCUMENTOS_COMUNICACION=60;
    public static final int SUBMODULO_MANTENIMIENTO=69;
    public static final int MODULO_CONTROL_PERSONAL=90;
    public static final int MODULO_SISTEMA_CONTROL_PATRIMONIAL = 99;
    public static final int SUBMODULO_MATRICULA_INSTITUCIONAL = 70;
    public static final int MODULO_PADRE_FAMILIA = 98;
    public static final int MODULO_ESTUDIANTE = 77;
    
    /*
    * ROLES PARA ASIGNACIONES
    */
    public static final int ROL_RESPONSABLE_CONTROL_PERSONAL = 14;
    /*
    
    
    /*
    * ESTADOS DEL SISTEMA
    */
    public static final char ESTADO_INACTIVO = 'E';
    
    public static final String TIPO_TRABAJADOR_ADMINISTRATIVO = "Ad";
    public static final String TIPO_TRABAJADOR_DIRECTIVO = "Di";
    public static final String TIPO_TRABAJADOR_DOCENTE = "Do";
    
    /*
    * ORGANIZACIONES
    */
    public static final int ORGANIZACION_SUPER = 1;
    public static final int ORGANIZACION_ID_DREMO = 2;
    public static final String ORGANIZACION_ALI_DREMO = "DREMO";

    public static final int ORGANIZACION_TIPO_GLOBAL = 1;
    public static final int ORGANIZACION_TIPO_DR = 2;
    public static final int ORGANIZACION_TIPO_UGEL = 3;
    public static final int ORGANIZACION_TIPO_IE = 4;
    /*
    
    * Horas Pedagocias CPE-MECH
    */
    public static final int MINUTOS_EQUIVALENTES_1_HORA_PEDAGOGICA = 45;
    public static final int MINUTOS_MINIMO_1_HORA_PEDAGOGICA = 30;
    public static final String ESTADO_DIA_ESPECIAL_NO_LABORABLE = "1";
    public static final String ESTADO_DIA_ESPECIAL_RECUPERABLE = "2";
    public static final int MAXIMO_HORAS_EFECTIVAS_DIARIA=7;
    /*
    public static String contextoAplicacion(){
        String paquete = this..getPackage().getName();
        //buscando el inicio del proyecto global
        int posicionProyecto = paquete.indexOf(".", paquete.indexOf(Sigesmed.NOM_PROYECTO) );
        //substrayendo el paquete principal
        paquete = paquete.substring(0, posicionProyecto);
        
        return paquete;
    }*/
    
}
