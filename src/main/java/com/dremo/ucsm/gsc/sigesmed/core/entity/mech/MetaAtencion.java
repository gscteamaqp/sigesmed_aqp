package com.dremo.ucsm.gsc.sigesmed.core.entity.mech;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="meta_atencion" ,schema="institucional" )
public class MetaAtencion  implements java.io.Serializable {

    @Id
    @Column(name="met_ate_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_meta_atencion", sequenceName="institucional.meta_atencion_met_ate_id_seq" )
    @GeneratedValue(generator="secuencia_meta_atencion")
    private int metAteId;
    @Column(name="num_sec")
    private int numSec;
    @Column(name="num_alu")
    private int numAlu;
    @Column(name="num_hor_cla")
    private int horCla;
    @Column(name="car_doc")
    private double carDoc;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_mod", nullable=false, length=29)
    private Date fecMod;    
    @Column(name="usu_mod")
    private int usuMod;
    @Column(name="est_reg")
    private char estReg;
    
    @Column(name="pla_niv_id")
    private int plaNivId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="pla_niv_id",updatable = false,insertable = false)
    private PlanNivel planNivel;
    
    @Column(name="gra_id")
    private int graId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="gra_id",updatable = false,insertable = false)
    private Grado grado;

    public MetaAtencion() {
    }
    public MetaAtencion(int metAteId) {
        this.metAteId = metAteId;
    }
    public MetaAtencion(int metAteId,int numSec, int numAlu, int horCla, double carDoc,int plaNivId, int graId, Date fecMod, int usuMod, char estReg) {
       this.metAteId = metAteId;
       
       this.numSec = numSec;
       this.numAlu = numAlu;
       this.horCla = horCla;
       this.carDoc = carDoc;
       
       this.plaNivId = plaNivId;
       this.graId = graId;
       
       this.fecMod = fecMod;
       this.usuMod = usuMod;
       this.estReg = estReg;
    }
   
     
    public int getMetAteId() {
        return this.metAteId;
    }    
    public void setMetAteId(int metAteId) {
        this.metAteId = metAteId;
    }
    
    public int getNumSec() {
        return this.numSec;
    }
    public void setNumSec(int numSec) {
        this.numSec = numSec;
    }
    
    public int getNumAlu() {
        return this.numAlu;
    }
    public void setNumAlu(int numAlu) {
        this.numAlu = numAlu;
    }
    public int getHorCla() {
        return this.horCla;
    }
    public void setHorCla(int horCla) {
        this.horCla = horCla;
    }
    public double getCarDoc() {
        return this.carDoc;
    }
    public void setCarDoc(double carDoc) {
        this.carDoc = carDoc;
    }
    
    public Date getFecMod() {
        return this.fecMod;
    }
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }
    
    public int getUsuMod() {
        return this.usuMod;
    }
    public void setUsuMod(int usuMod) {
        this.usuMod = usuMod;
    }
    
    public char getEstReg() {
        return this.estReg;
    }
    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }
    
    public int getPlaNivId() {
        return this.plaNivId;
    }    
    public void setPlaNivId(int plaNivId) {
        this.plaNivId = plaNivId;
    }
    
    public PlanNivel getPlanNivel() {
        return this.planNivel;
    }
    public void setPlanNivel(PlanNivel planNivel) {
        this.planNivel = planNivel;
    }
    
    public int getGraId() {
        return this.graId;
    }    
    public void setGraId(int graId) {
        this.graId = graId;
    }
    
    public Grado getGrado() {
        return this.grado;
    }
    public void setGrado(Grado grado) {
        this.grado = grado;
    }
}


