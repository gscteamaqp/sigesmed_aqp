package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Grado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanEstudios;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrador on 13/10/2016.
 */
@Entity
@Table(name = "programacion_anual",schema = "pedagogico")
public class ProgramacionAnual implements java.io.Serializable {
    @Id
    @Column(name = "pro_anu_id", nullable = false,unique = true)
    @SequenceGenerator(name = "programacion_anual_prog_anu_id_sequence", sequenceName = "pedagogico.programacion_anual_pro_anu_id_seq")
    @GeneratedValue(generator = "programacion_anual_prog_anu_id_sequence")
    private int proAnuId;

    @Column(name = "des", length = 256)
    private String des;
    
    
    @Column(name = "pla_est_id" ,  insertable=false , updatable=false)
    private int pla_est_id;

    public void setPla_est_id(int pla_est_id) {
        this.pla_est_id = pla_est_id;
    }

    public int getPla_est_id() {
        return pla_est_id;
    }

    
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="org_id")
    private Organizacion org;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "doc_id")
    private Docente doc;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "are_cur_id")
    private AreaCurricular are;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pla_est_id")
    private PlanEstudios planEstudios;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gra_id")
    private Grado gra;


    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "objetivos_programacion_anual",schema = "pedagogico",
            joinColumns = @JoinColumn(name = "pro_anu_id"),
            inverseJoinColumns = @JoinColumn(name = "obj_pr_id")
    )
    private List<ObjetivoProgramacion> objetivos = new ArrayList<>();

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "valores_actitudes_programacion_anual",schema = "pedagogico",
            joinColumns = @JoinColumn(name = "pro_anu_id"),
            inverseJoinColumns = @JoinColumn(name = "val_act_id")
    )
    private List<ValoresActitudes> valores = new ArrayList<>();
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fech_pro_anu")
    private Date fechProAnu;
    @Column(name = "fec_mod")
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "est_reg", length = 1)
    private Character estReg;

    public ProgramacionAnual(String des, Date fecProAnu) {
        this.des = des;

        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public ProgramacionAnual(String des, Organizacion org, Docente doc, AreaCurricular are, Grado gra) {
        this.des = des;
        this.org = org;
        this.doc = doc;
        this.are = are;
        this.gra = gra;

        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public ProgramacionAnual() {
    }

    public int getProAnuId() {
        return proAnuId;
    }

    public void setProAnuId(int proAnuId) {
        this.proAnuId = proAnuId;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public Organizacion getOrg() {
        return org;
    }

    public void setOrg(Organizacion org) {
        this.org = org;
    }

    public Docente getDoc() {
        return doc;
    }

    public void setDoc(Docente doc) {
        this.doc = doc;
    }

    public AreaCurricular getAre() {
        return are;
    }

    public void setAre(AreaCurricular are) {
        this.are = are;
    }

    public Grado getGra() {
        return gra;
    }

    public void setGra(Grado gra) {
        this.gra = gra;
    }

    public List<ObjetivoProgramacion> getObjetivos() {
        return objetivos;
    }

    public void setObjetivos(List<ObjetivoProgramacion> objetivos) {
        this.objetivos = objetivos;
    }

    public List<ValoresActitudes> getValores() {
        return valores;
    }

    public void setValores(List<ValoresActitudes> valores) {
        this.valores = valores;
    }

    public PlanEstudios getPlanEstudios() {
        return planEstudios;
    }

    public void setPlanEstudios(PlanEstudios planEstudios) {
        this.planEstudios = planEstudios;
    }
    
     public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Date getFechProAnu() {
        return fechProAnu;
    }

    public void setFechProAnu(Date fechProAnu) {
        this.fechProAnu = fechProAnu;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
}
