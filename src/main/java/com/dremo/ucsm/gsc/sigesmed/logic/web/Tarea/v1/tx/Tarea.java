/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx;

/**
 *
 * @author abel
 */
public class Tarea {
    
    public static final String TAREA_PATH = "/tarea/";    
    public static final String BANDEJA_TAREA_PATH = "/tarea/bandeja/";
    
    public static final char ESTADO_NUEVO = 'N';
    public static final char ESTADO_ENVIADO = 'E';
    public static final char ESTADO_CALIFICADO = 'C';
    public static final char ESTADO_FUERA_TIEMPO = 'F';
    
}
