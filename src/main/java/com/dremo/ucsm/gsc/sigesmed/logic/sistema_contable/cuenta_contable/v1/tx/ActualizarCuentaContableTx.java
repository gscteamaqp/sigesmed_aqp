/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.cuenta_contable.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sci.CuentaContableDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.CuentaContableDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentaContable;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.math.BigDecimal;
import java.util.Date;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class ActualizarCuentaContableTx implements ITransaction{   
    
    @Override    
    public WebResponse execute(WebRequest wr)  {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        CuentaContable nuevaCuentaContable = null;
    
        try{            
             
            JSONObject requestData = (JSONObject)wr.getData();           
            int cuentaContable = requestData.getInt("cuentaContableID");  
            String numero = requestData.getString("numero");
            String tipo =  Integer.toString(requestData.getInt("tipo"));           
            String subClase =  Integer.toString(requestData.getInt("subClase"));                 
            String nombre = requestData.getString("nombre");
            boolean encabezado = requestData.getBoolean("encabezado");
            boolean efectivo = requestData.getBoolean("efectivo");
            boolean iva = requestData.getBoolean("iva");
            
                     
                        
            nuevaCuentaContable = new CuentaContable(cuentaContable,numero, tipo.charAt(0),subClase.charAt(0), nombre,  encabezado, efectivo,  iva,  new Date(), wr.getIdUsuario(), 'A');
                           
        }catch(Exception e){
          
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage() );
        }
         //Fin
        
        /*
        *   Parte de Logica de Negocio    
        *
        */
        
     

        CuentaContableDao cuentaContableDao = new CuentaContableDaoHibernate();
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        try{
            cuentaContableDao.update(nuevaCuentaContable);        
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo registrar actualizar la cuenta contable ", e.getMessage() );
        }
        //Fin                      
       

        return WebResponse.crearWebResponseExito("El registro de la Cuenta Contable se actualizo correctamente");
        //Fin
    }    
    
    
}
