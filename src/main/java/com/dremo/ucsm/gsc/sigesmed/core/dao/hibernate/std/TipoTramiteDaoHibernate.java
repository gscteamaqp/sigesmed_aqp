/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.std;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.std.TipoTramiteDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.RequisitoTramite;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.RutaTramite;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.TipoTramite;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author abel
 */
public class TipoTramiteDaoHibernate extends GenericDaoHibernate<TipoTramite> implements TipoTramiteDao {

    @Override
    public TipoTramite buscarPorID(int clave) {
        TipoTramite tipoTramite = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            tipoTramite = (TipoTramite)session.get(TipoTramite.class, clave);
        
        }catch(Exception e){
            System.out.println("No se pudo buscar por id \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo buscar por id \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return tipoTramite; 
    }
    @Override
    public String buscarUltimoCodigo() {
        String codigo = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar ultimo codigo de tramite
            String hql = "SELECT tt.cod FROM TipoTramite tt  ORDER BY tt.cod DESC ";
            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            codigo = ((String)query.uniqueResult());
            
            //solo cuando no hay ningun registro aun
            if(codigo==null)
                codigo = "0000";
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return codigo;
    }

    @Override
    public List<TipoTramite> buscarPorTipoOrganizacion(int tipoOrganizacionID) {
        List<TipoTramite> tramites = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar Tipo TipoTramites
            String hql = "SELECT DISTINCT tt FROM TipoTramite tt JOIN FETCH tt.tipoOrganizacion LEFT JOIN FETCH tt.requisitoTramites WHERE tt.organizacion.orgId=:p1 and tt.estReg!='E'";
            Query query = session.createQuery(hql);
            query.setParameter("p1", tipoOrganizacionID);
            tramites = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los Tipos TipoTramites \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los Tipos TipoTramites \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return tramites;
    }

    @Override
    //solo los que no han sido elimados
    public List<TipoTramite> buscarConTipoOrganizacion() {
        List<TipoTramite> tramites = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar Tipo TipoTramites que sean tupa
            String hql = "SELECT tt FROM TipoTramite tt JOIN FETCH tt.tipoOrganizacion WHERE tt.estReg!='E' and tt.esTup=true ORDER By tt.tipTraId";                         
            Query query = session.createQuery(hql);
            tramites = query.list();
            
            hql = "SELECT tt FROM TipoTramite tt JOIN FETCH tt.organizacion WHERE tt.estReg!='E' and tt.esTup=false ORDER By tt.tipTraId";
            query = session.createQuery(hql);
            tramites.addAll( query.list() );
            /*
            //listar Tipo TipoTramites que No son tupa
            hql = "SELECT DISTINCT tt FROM TipoTramite tt JOIN FETCH tt.organizacion WHERE tt.estReg!='E' and NOT tt.esTup ORDER By tt.tipTraId";
            query = session.createQuery(hql);
            tramites = query.list();*/
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los Tipos TipoTramites \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los Tipos TipoTramites \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return tramites;
    }
    @Override
    public List<RequisitoTramite> buscarRequisitos(TipoTramite tipoTramite) {
        List<RequisitoTramite> requisitos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar requisitos
            String hql = "SELECT rt FROM RequisitoTramite rt WHERE rt.tipoTramite=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", tipoTramite);
            requisitos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los requistos del tramite \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los requistos del tramite \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return requisitos;
    }
    @Override
    public void eliminarRequisitos(TipoTramite tipoTramite) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            //eliminando tipo tramite
            String hql = "DELETE FROM RequisitoTramite WHERE tipoTramite =:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", tipoTramite);
            query.executeUpdate();    
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo eliminar los requisitos del TipoTramite \n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    @Override
    public List<RutaTramite> buscarRutas(TipoTramite tipoTramite) {
        List<RutaTramite> requisitos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar rutas
            String hql ="";
            if(tipoTramite.getEsTup())
                hql = "SELECT rt FROM RutaTramite rt JOIN FETCH rt.tipoAreaOrigen JOIN FETCH rt.tipoAreaDestino WHERE rt.tipoTramite=:p1 ORDER BY rt";
            else
                hql = "SELECT rt FROM RutaTramite rt JOIN FETCH rt.areaOrigen JOIN FETCH rt.areaDestino WHERE rt.tipoTramite=:p1 ORDER BY rt";
            Query query = session.createQuery(hql);
            query.setParameter("p1", tipoTramite);
            requisitos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las rutas del tramite\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las rutas del tramite\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return requisitos;
    }
    @Override
    public void eliminarRutas(TipoTramite tipoTramite) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            //eliminando tipo tramite
            String hql = "DELETE FROM RutaTramite WHERE tipoTramite =:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", tipoTramite);
            query.executeUpdate();    
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo eliminar las rutas del TipoTramite \n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
}
