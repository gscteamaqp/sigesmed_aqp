/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.directorio.trabajador.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.di.TrabajadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ListarTrabajadoresPorOrganizacionyTipoTx implements ITransaction{
 
    @Override
    public WebResponse execute(WebRequest wr) {        
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        JSONObject requestData = (JSONObject)wr.getData();
        int orgId = requestData.getInt("orgId");
        String traTip = requestData.getString("traTip");
        
        List<Trabajador> trabajadores = null;
        TrabajadorDao trabajadoresDao = (TrabajadorDao)FactoryDao.buildDao("di.TrabajadorDao");
        
        try{
            trabajadores = trabajadoresDao.ListarxOrganizacionxTipo(orgId, traTip);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar el directorio interno \n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar el directorio interno ", e.getMessage() );
        }
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Trabajador trabajador:trabajadores ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("traId",trabajador.getTraId());
            oResponse.put("cargo",trabajador.getTraCar().getCrgTraNom());
            oResponse.put("orgNom",trabajador.getOrganizacion().getNom());
            oResponse.put("perId",trabajador.getPersona().getPerId());
            oResponse.put("perDNI",trabajador.getPersona().getDni());
            oResponse.put("perMat",trabajador.getPersona().getApeMat());
            oResponse.put("perPat",trabajador.getPersona().getApePat());
            oResponse.put("perNom",trabajador.getPersona().getNom());          
            oResponse.put("perDir",trabajador.getPersona().getPerDir());
            oResponse.put("perEma",trabajador.getPersona().getEmail());
            oResponse.put("perFij",trabajador.getPersona().getFij());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
                
    }   
    
}
