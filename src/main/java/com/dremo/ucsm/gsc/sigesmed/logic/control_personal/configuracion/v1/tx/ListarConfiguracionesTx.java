/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.configuracion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.LibroAsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.ConfiguracionControl;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author carlos
 */
public class ListarConfiguracionesTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        Integer organizacoinId;
        Organizacion _user =null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            organizacoinId = requestData.getInt("organizacionID");        
            _user = new Organizacion(organizacoinId);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo verificar la Organizacion", e.getMessage() );
        }

        List<ConfiguracionControl> configuraciones = new ArrayList<>();
        LibroAsistenciaDao libroAsistenciaDao = (LibroAsistenciaDao)FactoryDao.buildDao("cpe.LibroAsistenciaDao");
        try{
            configuraciones =libroAsistenciaDao.listarConfiguracionesControl(_user);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar el libro de Asistencia ", e.getMessage() );
        }

        JSONArray miArray = new JSONArray();
       
        DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        for(ConfiguracionControl config:configuraciones ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("id",config.getConPerId());
            oResponse.put("anho",config.getConPerFecIni().getYear()+1900);
            oResponse.put("documento",config.getConPerDoc());
            
            
            oResponse.put("horaTol",config.getConPerTolHora());
            oResponse.put("minTol",config.getConPerTolMin());
            oResponse.put("descripcion",config.getConPerDes());
            oResponse.put("responsable",config.getTrabajadorId().getPersona().getNombrePersona());
            oResponse.put("idResponsable",config.getTrabajadorId().getTraId());
            oResponse.put("dniResponsable",config.getTrabajadorId().getPersona().getDni());
            String fechaIn=sdf.format(config.getConPerFecIni());
            oResponse.put("fechaInicio",fechaIn);
            String fechaFin=sdf.format(config.getConPerFecFin());
            oResponse.put("fechaFin",fechaFin);
            oResponse.put("estado",config.getEstReg());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}

