/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.maestro.plan;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.TareaIndicadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import org.hibernate.*;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.TareaIndicador;
import java.util.List;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.TareaSesionAprendizaje;

/**
 *
 * @author abel
 */
public class TareaIndicadorDaoHibernate  extends GenericDaoHibernate<TareaIndicador> implements TareaIndicadorDao {

    @Override
    public List<TareaIndicador> listar_tareas_indicador(int ind_id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    @Override
    public TareaSesionAprendizaje listar_indicadores_tarea(int id_tarea){

        TareaSesionAprendizaje  tarea_sesion = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT DISTINCT e FROM TareaSesionAprendizaje e JOIN FETCH e.tarea_indicador WHERE e.tarIdSes=:p1 ";
            Query query = session.createQuery(hql);
            query.setParameter("p1",id_tarea);
            query.setMaxResults(1);
            tarea_sesion = ((TareaSesionAprendizaje)query.uniqueResult());

        }catch(Exception e){
            System.out.println("No se pudo Obtenerer la tarea de la sesion de aprendizaje\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Obtener la tarea de la sesion de aprendizaje\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return tarea_sesion;


    }

    @Override
    public TareaSesionAprendizaje obtenerTarea(int id_tarea) {
        
    
        TareaSesionAprendizaje  tarea_sesion = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT DISTINCT e FROM TareaSesionAprendizaje e  WHERE e.tarIdSes=:p1 ";
            Query query = session.createQuery(hql);
            query.setParameter("p1",id_tarea);
            query.setMaxResults(1);
            tarea_sesion = ((TareaSesionAprendizaje)query.uniqueResult());
        }
        catch(Exception e){
              System.out.println("No se pudo Obtener la tarea de la sesion de aprendizaje\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Obtener la tarea de la sesion de aprendizaje\n "+ e.getMessage());           
        }
         finally{
            session.close();
        }
        return tarea_sesion;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  
    }
}
