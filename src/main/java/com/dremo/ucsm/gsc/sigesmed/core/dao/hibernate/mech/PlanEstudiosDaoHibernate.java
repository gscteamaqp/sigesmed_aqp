/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mech;


import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.PlanEstudiosDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.AreaModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.GradoModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.MetaAtencion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.MetaAtencionModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.NivelModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Periodo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanHoraArea;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanNivel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Turno;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author abel
 */
public class PlanEstudiosDaoHibernate extends GenericDaoHibernate<PlanEstudios> implements PlanEstudiosDao{

    @Override
    public PlanEstudios buscarVigentePorOrganizacion(int orgId){
        
        Date hoy = new Date();        
        hoy.setSeconds(0);
        hoy.setMinutes(0);
        hoy.setHours(0);
        hoy.setMonth(0);
        hoy.setDate(1);
        
        PlanEstudios objeto = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{            
            String hql = "SELECT pe FROM PlanEstudios pe WHERE pe.orgId=:p1 and pe.añoEsc =:p2 ";
        
            Query query = session.createQuery(hql);
            query.setParameter("p1", orgId );
            query.setParameter("p2", hoy );
            query.setMaxResults(1);
            //buscando 
            objeto =  (PlanEstudios)query.uniqueResult(); 
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar el plan de estudios vigente\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar el plan de estudios vigente\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objeto;
    }
    @Override
    public List<PlanEstudios> buscarConOrganizacion(){
        List<PlanEstudios> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT pe FROM PlanEstudios pe JOIN FETCH pe.organizacion WHERE pe.estReg!='E' ORDER BY pe.orgId";
            Query query = session.createQuery(hql);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los planes de estudios con organizaciones\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los planes de estudios con organizaciones\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    
    @Override
    public List<PlanNivel> listarNivelesPorPlanEstudios(int planID){
        List<PlanNivel> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //String hql = "SELECT DISTINCT pn FROM PlanNivel pn LEFT JOIN FETCH pn.metas LEFT JOIN FETCH pn.horasDisponibles WHERE pn.plaEstId=:p1 ORDER BY pn.jorId,pn.turId";
            String hql = "SELECT DISTINCT pn FROM PlanNivel pn WHERE pn.plaEstId=:p1 ORDER BY pn.jorId,pn.turId";
            Query query = session.createQuery(hql);
            query.setParameter("p1", planID);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los niveles del plan de estudios\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los niveles del plan de estudios\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public List<MetaAtencionModel> listarMetasAtencionPorOrganizacion(int orgID){
        List<MetaAtencionModel> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.mech.MetaAtencionModel(m.planNivel.planEstudios.añoEsc,m.planNivel.jornada.nivId,m.graId,SUM(m.numAlu),SUM(m.numSec)) FROM MetaAtencion m WHERE m.planNivel.planEstudios.orgId=:p1 GROUP BY m.planNivel.planEstudios.añoEsc,m.planNivel.jornada.nivId, m.graId ORDER BY m.planNivel.jornada.nivId,m.planNivel.planEstudios.añoEsc ";
            Query query = session.createQuery(hql);
            query.setParameter("p1", orgID);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar todos los niveles por organizacion\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar todos los niveles por organizacion\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    
    @Override
    public List<NivelModel> buscarNiveles(int planEstudiosID){
        List<NivelModel> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.mech.NivelModel(pn.plaNivId,pn.des,pn.jornada.nivId,pn.jornada.jorEscId,pn.jornada.nom,pn.turId,pn.perId,pn.jornada.disCurId) FROM PlanNivel pn WHERE pn.plaEstId=:p1 ORDER BY pn.jornada.nivId";
            Query query = session.createQuery(hql);
            query.setParameter("p1", planEstudiosID);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los niveles model del plan de estudios\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los niveles model del plan de estudios\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    
    @Override
    public List<GradoModel> buscarGrados(int planEstudiosID){
        List<GradoModel> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.mech.GradoModel(m.grado.graId,m.grado.nom,m.grado.nivId,m.numSec) FROM MetaAtencion m WHERE m.planNivel.plaEstId=:p1 ORDER BY m.grado.graId";
            Query query = session.createQuery(hql);
            query.setParameter("p1", planEstudiosID);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los grados model del plan de estudios\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los grados model del plan de estudios\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public List<AreaModel> buscarAreas(int disCurID){
        List<AreaModel> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.mech.AreaModel(a.area.areCurId,a.area.nom,a.grado.graId,a.grado.nom) FROM AreaCurricularHora a WHERE a.area.disCurId=:p1 ORDER BY a.grado.graId";
            Query query = session.createQuery(hql);
            query.setParameter("p1", disCurID);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las areas model del plan de estudios\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las areas model del plan de estudios\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public List<AreaModel> buscarTalleres(int disCurID){
        List<AreaModel> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.mech.AreaModel(a.area.areCurId,a.area.nom,a.grado.graId,a.grado.nom) FROM PlanHoraArea a WHERE a.area.disCurId=:p1 and a.area.esTal=true ORDER BY a.grado.graId";
            Query query = session.createQuery(hql);
            query.setParameter("p1", disCurID);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las areas model del plan de estudios\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las areas model del plan de estudios\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    
    @Override
    public List<AreaModel> buscarAreasByGrado(int disCurID,Integer gradoId){
        List<AreaModel> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.mech.AreaModel(a.area.areCurId,a.area.nom,a.grado.graId,a.grado.nom) FROM AreaCurricularHora a WHERE a.area.disCurId=:p1 AND a.grado.graId=:p2";
            Query query = session.createQuery(hql);
            query.setParameter("p1", disCurID);
            query.setParameter("p2", gradoId);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las areas model del plan de estudios\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las areas model del plan de estudios\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public List<AreaModel> buscarTalleresByGrado(int disCurID,Integer gradoId){
        List<AreaModel> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.mech.AreaModel(a.area.areCurId,a.area.nom,a.grado.graId,a.grado.nom) FROM PlanHoraArea a WHERE a.area.disCurId=:p1 and a.area.esTal=true AND a.grado.graId=:p2";
            Query query = session.createQuery(hql);
            query.setParameter("p1", disCurID);
            query.setParameter("p2", gradoId);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las areas model del plan de estudios\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las areas model del plan de estudios\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    
    @Override
    public void mergeTurno(Turno turno){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            session.merge(turno);
            miTx.commit();        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo realizar la persistencia en turno\\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo realizar la persistencia en turno\\n"+ e.getMessage());
        }
        finally{
            session.close();
        }
    }
    @Override
    public void mergePeriodo(Periodo periodo){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            session.merge(periodo);
            miTx.commit();        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo realizar la persistencia en periodo\\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo realizar la persistencia en periodo\\n"+ e.getMessage());
        }
        finally{
            session.close();
        }
    }
    @Override
    public void mergeNivel(PlanNivel nivel){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            if(nivel.getPlaNivId()==0)
                session.persist(nivel);
            else
                session.update(nivel);
            miTx.commit();        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo realizar la persistencia en plan nivel\\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo realizar la persistencia en plan nivel\\n"+ e.getMessage());
        }
        finally{
            session.close();
        }
    }
    @Override
    public void mergeMetaAtencion(MetaAtencion meta){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            if(meta.getMetAteId()==0)
                session.persist(meta);
            else
                session.update(meta);
            miTx.commit();        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo realizar la persistencia en la meta de atencion\\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo realizar la persistencia en la meta de atencion\\n"+ e.getMessage());
        }
        finally{
            session.close();
        }
    }
    @Override
    public void mergeHoraDisponible(PlanHoraArea horaDisponible){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            if(horaDisponible.getPlaHorAreId()==0)
                session.persist(horaDisponible);
            else
                session.update(horaDisponible);
            miTx.commit();        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo realizar la persistencia de la hora de libre disponibilidad\\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo realizar la persistencia de la hora de libre disponibilidad\\n"+ e.getMessage());
        }
        finally{
            session.close();
        }
    }
    @Override
    public void eliminarTurno(char turnoID){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            String hql = "delete from Turno where turId=:p1";
            
            Query query = session.createQuery(hql);
            query.setParameter("p1", turnoID);
            query.executeUpdate();
            
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo eliminar el turno" + this.getClass().getName() + "\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    
    @Override
    public void eliminarPeriodo(char periodoID){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            String hql = "delete from Periodo where perId=:p1";
            
            Query query = session.createQuery(hql);
            query.setParameter("p1", periodoID);
            query.executeUpdate();
            
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo eliminar el periodo" + this.getClass().getName() + "\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    @Override
    public void eliminarNivel(int nivelID){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            String hql = "delete from PlanNivel where plaNivId=:p1";
            
            Query query = session.createQuery(hql);
            query.setParameter("p1", nivelID);
            query.executeUpdate();
            
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo eliminar el plan nivel" + this.getClass().getName() + "\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    @Override 
    public void eliminarHoraDisponible(PlanHoraArea horaArea){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            session.delete(horaArea);
            miTx.commit();        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo realizar la elimnacion de la hora de libre disponibilidad\\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo realizar la eliminacion de la hora de libre disponibilidad\\n"+ e.getMessage());
        }
        finally{
            session.close();
        }
    }
    
    @Override
    public List<Turno> listarTurnos(){
        List<Turno> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT t FROM Turno t WHERE t.estReg!='E' ORDER BY t.turId";
            Query query = session.createQuery(hql);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los turnos\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los turnos\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public List<Periodo> listarPeriodos(){
        List<Periodo> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT p FROM Periodo p WHERE p.estReg!='E' ORDER BY p.perId";
            Query query = session.createQuery(hql);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los periodos\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los periodos\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public void reiniciarPlazasMagisteriales(int orgId){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{            
            String hql = "UPDATE PlazaMagisterial pm SET pm.jornadaPedagogica=0 WHERE pm.orgId =:p1";            
            Query query = session.createQuery(hql);
            query.setParameter("p1", orgId);
            query.executeUpdate();    
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo reiniciar las plazas magisteriales\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
}
