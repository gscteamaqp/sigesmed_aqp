/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.scp;
import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import java.util.Date;
import java.util.List;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BienesInmuebles;
/**
 *
 * @author Administrador
 */
public interface BienInmuebleDAO extends GenericDao<BienesInmuebles>{
    
    List<BienesInmuebles> listar_bienes_inmuebles(int org_id);
    List<BienesInmuebles> reporteBieneInmuebles(int org_id);
    public BienesInmuebles obtener_bien(int id_bien);
    public void eliminar_bien_inmueble(int id_bie);
}
