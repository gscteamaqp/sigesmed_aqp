/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DemeritoDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class ContarDemeritosPorOrganizacionTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ContarDemeritosPorOrganizacionTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        Integer orgId = data.optInt("orgId");
        
        JSONObject resultado = new JSONObject();
        DemeritoDao demeritosDao = (DemeritoDao)FactoryDao.buildDao("se.DemeritoDao");
        
        try{
            resultado = demeritosDao.contarxOrganizacion(orgId);
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"contarDemeritosPorOrganizacion",e);
            System.out.println("No se pudo contar los demeritos\n"+e);
            return WebResponse.crearWebResponseError("No se pudo contar los demeritos", e.getMessage());
        }
        return WebResponse.crearWebResponseExito("Los demeritos fueron contados exitosamente", resultado);
    }
    
}
