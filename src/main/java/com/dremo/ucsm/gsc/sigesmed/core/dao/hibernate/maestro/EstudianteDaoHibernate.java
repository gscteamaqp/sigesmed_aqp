package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.maestro;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Estudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.GradoIEEstudiante;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * Created by Administrador on 17/01/2017.
 */
public class EstudianteDaoHibernate extends GenericDaoHibernate<Estudiante> implements EstudianteDao{
    @Override
    public GradoIEEstudiante buscarGradoIEEstudiante(int idGradoIE) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT g FROM GradoIEEstudiante g WHERE g.graOrgEstId =:id";
            Query query = session.createQuery(hql);
            query.setInteger("id",idGradoIE);
            return (GradoIEEstudiante) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
}
