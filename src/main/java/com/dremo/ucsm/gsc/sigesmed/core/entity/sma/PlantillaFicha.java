/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.FilterDef;

/**
 *
 * @author gscadmin
 */
@Entity
@FilterDef(name="filtroplantilla")
@Table(name = "plantilla_ficha", schema = "pedagogico")

public class PlantillaFicha implements Serializable {
    @Id 
    @Column(name="plf_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_plantilla_ficha", sequenceName="pedagogico.plantilla_ficha_plf_id_seq" )
    @GeneratedValue(generator="secuencia_plantilla_ficha")
    private Integer plfId;

    @Column(name = "plf_cod")
    private String plfCod;
    
    @Column(name = "plf_nom")
    private String plfNom;
    
    @Column(name = "plf_des")
    private String plfDes;
    
    @Column(name = "plf_ini")
    private Integer plfIni;
    
    @Column(name = "plf_pro")
    private Integer plfPro;
    
    @Column(name = "plf_log")
    private Integer plfLog;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.DATE)
    private Date fecMod;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "est_reg")
    private String estReg;
    
    @OneToMany(mappedBy = "plfId", fetch = FetchType.LAZY)
    private List<CompromisosGestion> compromisosGestionList;
    
    @OneToMany(mappedBy = "plantillaFicha", fetch = FetchType.LAZY)
    private List<FichaEvaluacion> fichaEvaluacionList;

    public PlantillaFicha() {
    }

    public PlantillaFicha(Integer plfId) {
        this.plfId = plfId;
    }
    
    public PlantillaFicha(String plfCod, String plfNom, Date fecMod, String plfDes) {        
        this.plfCod = plfCod;
        this.plfNom = plfNom;
        this.fecMod = fecMod;
        this.plfDes = plfDes;
    }

    public PlantillaFicha(int plaid) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Integer getPlfId() {
        return plfId;
    }

    public void setPlfId(Integer plfId) {
        this.plfId = plfId;
    }

    public String getPlfCod() {
        return plfCod;
    }

    public void setPlfCod(String plfCod) {
        this.plfCod = plfCod;
    }

    public String getPlfNom() {
        return plfNom;
    }

    public void setPlfNom(String plfNom) {
        this.plfNom = plfNom;
    }

    public String getPlfDes() {
        return plfDes;
    }

    public void setPlfDes(String plfDes) {
        this.plfDes = plfDes;
    }

    public Integer getPlfIni() {
        return plfIni;
    }

    public void setPlfIni(Integer plfIni) {
        this.plfIni = plfIni;
    }

    public Integer getPlfPro() {
        return plfPro;
    }

    public void setPlfPro(Integer plfPro) {
        this.plfPro = plfPro;
    }

    public Integer getPlfLog() {
        return plfLog;
    }

    public void setPlfLog(Integer plfLog) {
        this.plfLog = plfLog;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    @XmlTransient
    public List<CompromisosGestion> getCompromisosGestionList() {
        return compromisosGestionList;
    }

    public void setCompromisosGestionList(List<CompromisosGestion> compromisosGestionList) {
        this.compromisosGestionList = compromisosGestionList;
    }

    @XmlTransient
    public List<FichaEvaluacion> getFichaEvaluacionList() {
        return fichaEvaluacionList;
    }

    public void setFichaEvaluacionList(List<FichaEvaluacion> fichaEvaluacionList) {
        this.fichaEvaluacionList = fichaEvaluacionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (plfId != null ? plfId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlantillaFicha)) {
            return false;
        }
        PlantillaFicha other = (PlantillaFicha) object;
        if ((this.plfId == null && other.plfId != null) || (this.plfId != null && !this.plfId.equals(other.plfId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PlantillaFicha{" + "plfId=" + plfId + ", plfCod=" + plfCod + ", plfNom=" + plfNom + ", plfDes=" + plfDes + ", plfIni=" + plfIni + ", plfPro=" + plfPro + ", plfLog=" + plfLog + ", fecMod=" + fecMod + ", usuMod=" + usuMod + ", estReg=" + estReg + ", compromisosGestionList=" + compromisosGestionList + '}';
    }
    
    
}
