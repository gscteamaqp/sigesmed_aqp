/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author angel
 */
public class ReporteAreaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        FileJsonObject miGrafico = null;
        GTabla tablaOrg = null;
        JSONObject cabecera = new JSONObject();
        try{
           
            JSONObject requestData = (JSONObject)wr.getData();
            
            //cabecera.put("Fecha de Fin de Busqueda", requestData.getString("hasta"));
            cabecera.put("Fecha de Incio de Busqueda", requestData.getString("desde"));
       
            
            
            JSONArray tabla = requestData.optJSONArray("resumen");
           // JSONArray fecha = requestData.optJSONArray("resumen");
            
            if(tabla!=null && tabla.length() > 0){
                
                float[] cols = {3f,2f,2f,2f,2f};
                tablaOrg = new GTabla(cols);

                String[] labels = {"Area","N° Recibidos","N° Rechazados","N° Derivados","N° Finalizados"};
                tablaOrg.build(labels);
                for(int i = 0; i < tabla.length();i++){
                    JSONObject bo =tabla.getJSONObject(i);
                    
                    String[] fila = new String [5];
                    fila[0] = bo.getString("areaa");
                    fila[1] = ""+bo.getInt("recibidosa");
                    fila[2] = ""+bo.getInt("devueltosa");
                    fila[3] = ""+bo.getInt("derivadosa");
                    fila[4] = ""+bo.getInt("finalizadosa");
                    
                    tablaOrg.processLine(fila);
                }
                
            }
            else            
                miGrafico = new FileJsonObject( requestData  );
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo realizar el reporte, grafico iicorrecto", e.getMessage() );
        }
        //Fin        
        
        
        /*
        *  Repuesta Correcta
        */
        
        JSONObject response = new JSONObject();
        try {
            Mitext r = new Mitext();
            
            
            if(tablaOrg==null){
                r.agregarTitulo(miGrafico.getName());
                r.newLine(1);
                r.agregarImagen64(miGrafico.getData());
            }
            else{
                r.newLine(4);
                r.agregarTitulo("Cantidad de Tramites Por Area");
                r.newLine(4);
                r.agregarSubtitulos(cabecera);
                r.newLine(3);
                r.agregarTabla(tablaOrg);
            }
            
            r.cerrarDocumento();
            response.append("reporte", r.encodeToBase64() );
        } catch (Exception ex) {
            Logger.getLogger(ReporteTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return WebResponse.crearWebResponseExito("el reporte se realizo",response);
        //Fin
    }
    
}

