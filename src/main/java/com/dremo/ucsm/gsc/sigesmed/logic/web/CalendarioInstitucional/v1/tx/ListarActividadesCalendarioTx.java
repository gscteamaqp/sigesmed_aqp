package com.dremo.ucsm.gsc.sigesmed.logic.web.CalendarioInstitucional.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.ActividadCalendarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.ActividadCalendario;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ListarActividadesCalendarioTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(ListarActividadesCalendarioTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            ActividadCalendarioDao actividadDao = (ActividadCalendarioDao) FactoryDao.buildDao("web.ActividadCalendarioDao");
            List<ActividadCalendario> activities = actividadDao.listarActividades(wr.getIdUsuario());
            JSONArray array = new JSONArray();

            for (ActividadCalendario activity : activities) {
                if (activity.getFecFin().before(new Date())) {
                    activity.setEstReg('F');
                    actividadDao.update(activity);
                }

                JSONObject object = new JSONObject();

                object.put("title", activity.getTit());
                object.put("start", activity.getFecIni().getTime());
                object.put("end", activity.getFecFin().getTime());

                switch (activity.getTipAct()) {
                    case 'P':
                        object.put("color", "#257e4a");
                        break;
                    case 'D':
                        object.put("color", "#25267e");
                        break;
                    case 'U':
                        object.put("color", "#7e2d25");
                        break;
                    case 'I':
                        object.put("color", "#64257e");
                        break;
                }

                object.put("cod", activity.getActCalId());
                object.put("tip", String.valueOf(activity.getTipAct()));
                object.put("des", activity.getDes());
                object.put("est", String.valueOf(activity.getEstReg()));
                array.put(object);
            }

            return WebResponse.crearWebResponseExito("Se listo correctamente", WebResponse.OK_RESPONSE).setData(array);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarActividades", e);
            return WebResponse.crearWebResponseError("Error al listar las actividades", WebResponse.BAD_RESPONSE);
        }
    }
}
