/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.padre_familia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mpf.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.AreaModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.GradoIeEstudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.Matricula;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.SaludControles;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

/**
 *
 * @author HernanF
 */
public class ListarNotasAnualesTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        Long matriculaId;

        try {
            JSONObject requestData = (JSONObject) wr.getData();
            matriculaId = requestData.getLong("matriculaID");

        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo verificar los datos", e.getMessage());
        }

        Matricula estudiante = null;
        List<GradoIeEstudiante> gradoIeEstudiantes = null;
        List<AreaModel>areas = null;
        Map<String, Map<String, String>> notasAnuales = new HashMap();
        Map<String, String> notasCurso = null;
        EstudianteDao estudianteDao = (EstudianteDao) FactoryDao.buildDao("mpf.EstudianteDao");
        try {
            estudiante = estudianteDao.getDatosEstudiante(matriculaId);
            gradoIeEstudiantes = estudiante.getGradoIeEstudiantes();
            for (GradoIeEstudiante gradoIee : gradoIeEstudiantes) {
                areas = estudianteDao.buscarAreasByPlanEstudios(estudiante.getPlanNivel().getPlaEstId(), 
                        gradoIee.getGrado().getGraId(), 
                        gradoIee.getSeccion().getSedId());
                notasCurso = new HashMap();
                for (AreaModel area: areas) {
                    String nota = estudianteDao.getNotaPromAreaFinal(gradoIee.getGraOrgEstId(), area.getAreaID());
                    nota = nota == null ? "0" : nota;
                    notasCurso.put(area.getArea(), nota);
                }
                notasAnuales.put(gradoIee.getGrado().getDes(), notasCurso);
            }
            
        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo  obtener las notas del estudiante", e.getMessage());
        }
        
        JSONObject oResponse = new JSONObject();
        try {
            for (Map.Entry<String, Map<String, String>> gradoEntry : notasAnuales.entrySet()) {
                JSONObject temp = new JSONObject();
                for (Map.Entry<String, String> areaEntry : gradoEntry.getValue().entrySet()) {
                    temp.put(areaEntry.getKey(), areaEntry.getValue());
                }
                oResponse.put(gradoEntry.getKey(), temp);
            }
        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo  procesar las notas del estudiante", e.getMessage());
        }
        return WebResponse.crearWebResponseExito("Se Listo correctamente", oResponse);
    }
    
}
