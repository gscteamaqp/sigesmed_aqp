package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrador on 24/10/2016.
 */
@Entity
@Table(name = "objetivo_programacion", schema = "pedagogico")
public class ObjetivoProgramacion implements java.io.Serializable {

    @Id
    @Column(name = "obj_pr_id")
    @SequenceGenerator(name = "objetivo_programacion_obj_pr_id_sequence",sequenceName = "pedagogico.objetivo_programacion_obj_pr_id_seq")
    @GeneratedValue(generator = "objetivo_programacion_obj_pr_id_sequence")
    private int objPrId;
    @Column(name = "nom", length = 400, nullable = false)
    private String nom;
    @Column(name = "des", length = 400)
    private String des;

    @Column(name = "tip", length = 2, nullable = false)
    private String tip;

    @ManyToMany(mappedBy = "objetivos")
    private List<ProgramacionAnual> programaciones = new ArrayList();

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "est_reg", length = 1)
    private Character estReg;

    public ObjetivoProgramacion() {
    }

    public ObjetivoProgramacion(String nom, String des, String tip) {
        this.nom = nom.toUpperCase();
        this.des = des;
        this.tip = tip;

        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public int getObjPrId() {
        return objPrId;
    }

    public void setObjPrId(int objPrId) {
        this.objPrId = objPrId;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public List<ProgramacionAnual> getProgramaciones() {
        return programaciones;
    }

    public void setProgramaciones(List<ProgramacionAnual> programaciones) {
        this.programaciones = programaciones;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ObjetivoProgramacion)) return false;

        ObjetivoProgramacion that = (ObjetivoProgramacion) o;

        return nom.equals(that.nom);

    }

    @Override
    public int hashCode() {
        return nom.hashCode();
    }
}
