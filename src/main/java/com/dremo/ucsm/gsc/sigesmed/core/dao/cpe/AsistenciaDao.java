/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.cpe;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.AsistenciaAula;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.DiaSemana;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.HorarioDet;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.Inasistencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.RegistroAsistencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.HorarioDetalle;
import java.util.Date;
import java.util.List;

/**
 *
 * @author carlos
 */
public interface AsistenciaDao extends GenericDao<RegistroAsistencia>{
   
//       public Persona buscarPorDNI(Integer dni,Organizacion org);
    public List<HorarioDet> listarHorario(Trabajador trabajador,DiaSemana dia);
    public List<RegistroAsistencia> listarRegistroAsistenciaByFecha(Trabajador trabajador,Date fecha);
    public List<AsistenciaAula> listarAsistenciaAulaByFecha(Docente docenteID, Date fecha);
    public RegistroAsistencia buscarRegistroAsistenciaByFecha(Date fecha,Trabajador trabajador);
    public Boolean registrarHoraSalida(Integer idRegistroAsistencia,Date hora);
    public List<Trabajador> listarAsistenciaTrabajadorByFecha(Date fechaInicio,Date fechaFin,Organizacion org, Integer rolId);
    public Inasistencia buscarInasistencia(Trabajador trabajador,Date fecha);
    public List<Trabajador> listarAsistenciaAllTrabajadorByFecha(Date fechaInicio,Date fechaFin,Date hoy);
    public List<RegistroAsistencia> listarAsistenciasPosiblesToAdicional(Trabajador trabajador,Date fecha,DiaSemana dia);
    public Boolean registrarHoraAdicional(Integer idRa,String descripcion,String nroDoc,String doc,Integer minAdicionales);
    public List<Trabajador> listarConsolidadoAsistenciaTrabajadorByFecha(Date fechaInicio,Date fechaFin,Organizacion org);
    public List<Trabajador> listarAsistenciaByFecha(Date fechaInicio,Date fechaFin,Organizacion org,String dni);
    public  Boolean actualizarAsistenciaToFalta(RegistroAsistencia asistencia);
    public List<Docente> listarHorarioMechDocentes(Integer organizacion,Date fechaInicio,Date fechaFin);
    public List<HorarioDetalle> listarHorarioMech(Integer docente, Character dia);
    public List<Trabajador> listarAsistenciaAllTrabajadorByFechaAndOrganizacion(Integer organizacion,Date fechaInicio,Date fechaFin,Date hoy);
    public List<Inasistencia> listarInasistencias(Persona persona,Organizacion organizacion,Date inicio,Date fin);
    public List<RegistroAsistencia> buscarRegistroAsistenciaJustificadosByFecha(Date fechaInicio,Date fechaFin,Trabajador trabajador);
//
}