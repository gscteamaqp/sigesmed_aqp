/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.LibroCajaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.Asiento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentaCorriente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentasEfectivo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.DetalleCuenta;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.LibroCaja;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.RegistroCompras;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.RegistroVentas;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.ResultadoMensual;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.ResultadosMensualPorCuenta;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ReporteLibroTx implements ITransaction{
    
     @Override
    public WebResponse execute(WebRequest wr){
        //Variables para almacenar informacion que envia frontEnd
        int organizacionID = 0;  
        int personaID =0;
        String fechaD="";
        String fechaH="";
        
        //Variables para almacenar informacion del libro  
        LibroCaja libro=null;
        Organizacion organizacion=null;
        List<ResultadoMensual> hechosLibroCaja=null;
       
        List<List<Object>> objetos=null;  
                
        List<CuentasEfectivo> cuentasEfectivo=null;
        List<CuentaCorriente> cuentasCorrientes=null;
        
        ResultadoMensual saldoMesAnterior= null;
        ResultadoMensual saldoDesde= null;
        List<ResultadoMensual> resultados= new ArrayList<>(); 
        ResultadoMensual saldoHasta=null;
        
        Date desde =null;
        Date hasta= null;
        
        Date desde2 = null;
        Date hasta2 = null;
        
        int desdeD=0; int desdeM=0; int desdeA=0;
        int hastaD=0; int hastaM=0; int hastaA=0;
        
        DateFormat fecha = new SimpleDateFormat("dd-MMMM-yyyy");
        
        List<ResultadosMensualPorCuenta> resultadosCuentaIngreso= new ArrayList<>();
        List<ResultadosMensualPorCuenta> resultadosCuentaEgreso= new ArrayList<>();

        List<ResultadoMensual> resultadoMensualIngresos= new ArrayList<>();
        List<ResultadoMensual> resultadoMensualEgresos= new ArrayList<>();


        
        try{
            JSONObject requestData = (JSONObject)wr.getData();           
            organizacionID = requestData.getInt("organizacionID");
            personaID = requestData.getInt("personaID");
            fechaD = requestData.getString("fechaD");
            fechaH = requestData.getString("fechaH");
            
            
            
           // System.out.println("fechaD ------>"+ fechaD+"   fechaH----->"+ fechaH);
            
            
            
            desde = new Date(fechaD); 
            desdeD=desde.getDate(); 
            desdeM=desde.getMonth(); 
            desdeA=desde.getYear();
            System.out.println("fecha desde  " + desde);
          //   System.out.println("fecha222222 desde  "+ new Date(desdeA,desdeM,desdeD)); 
            hasta = new Date(fechaH); 
            hastaD=hasta.getDate(); 
            hastaM=hasta.getMonth(); 
            hastaA=hasta.getYear();
            System.out.println("fecha hasta  "+ hasta);
          

        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo obtener la información ", e.getMessage() );
        }
        
        LibroCajaDao libroDao = (LibroCajaDao)FactoryDao.buildDao("sci.LibroCajaDao");
        OrganizacionDao organizacionDao =(OrganizacionDao)FactoryDao.buildDao("OrganizacionDao");
              
        try {          
            //consulta BD del estado del libro 
              libro= libroDao.estadoLibroCaja(organizacionID);
              organizacion = organizacionDao.buscarConTipoOrganizacionYPadre(organizacionID);
            if(libro==null){
                
                JSONObject oResponse = new JSONObject();
                oResponse.put("estado","N");                    
                return WebResponse.crearWebResponseExito("El LIBRO DE LA I.E. NO ESTA REGISTRADO" ,oResponse);  
            }
              
            else if(libro.getEstReg().equals('I')){
                
                JSONObject oResponse = new JSONObject();
                oResponse.put("estado","I");                    
                return WebResponse.crearWebResponseExito("El ESTADO DEL LIBRO ES INACTIVO" ,oResponse);  
            }
            
            
           // Información del libro 
            if (libro!=null){                                               
              //  cuentasEfectivo = libroDao.buscarCuentasEfectivo(libro);
              //   cuentasCorrientes = libroDao.buscarCuentasCorrientes(libro);
                
                    if(libro.getFecApe().getMonth()== desdeM && libro.getFecApe().getMonth()== hastaM){

                      saldoMesAnterior = new ResultadoMensual(desdeM, libro.getSalApe(),BigDecimal.ZERO);
                      if(desdeD!=1){                    
                          saldoDesde = libroDao.resultadoAsientosPorFecha(libro, new Date(desdeA,desdeM,1),new Date(desdeA,desdeM,desdeD));
                      }
                          saldoHasta = libroDao.resultadoAsientosPorFecha(libro, new Date(hastaA,hastaM,1),  new Date(hastaA,hastaM,hastaD));
                          saldoHasta.setDebe(saldoHasta.getDebe().add(saldoMesAnterior.getDebe()));
                          saldoHasta.setHaber(saldoHasta.getHaber().add(saldoMesAnterior.getHaber()));

                      resultados.add(saldoHasta);
                    }

                    else {   

                        resultados = libroDao.resultadosHechosLibroCajaPorFechas(libro, desde, hasta);
                        for(ResultadoMensual r:resultados){
                            System.out.println(r.getNombreMes()+"--"+r.getDebe()+"--"+r.getHaber()+"--"+r.getSaldo());
                        }
                        if(desdeD!=1){
                            saldoDesde = libroDao.resultadoAsientosPorFecha(libro, new Date(desdeA,desdeM,1),new Date(desdeA,desdeM,desdeD));
                        }
                        int pos= resultados.size();
                        saldoHasta = libroDao.resultadoAsientosPorFecha(libro,new Date(hastaA,hastaM,1), new Date(hastaA,hastaM,hastaD)); 

                        saldoHasta.setDebe(saldoHasta.getDebe().add(resultados.get(pos-1).getDebe()));
                        saldoHasta.setHaber(saldoHasta.getHaber().add(resultados.get(pos-1).getHaber()));

                        resultados.add(saldoHasta);

                        if(resultados.get(0).getSaldo()==null){
                           resultados.remove(0);                       
                        }
                    }                                             
                   
               }
              //  System.out.println("fecha desde A - desde M - desde D  anio" +desdeA+" mes "+desdeM+" dia "+desdeD+"   HastaA -- hasta M --- hasta D anio" +hastaA+" mes "+hastaM+" dia "+hastaD);
                objetos= libroDao.listarAsientosPorFechas(libro,new Date(desdeA,desdeM,desdeD),new Date(hastaA,hastaM,hastaD));
                //System.out.println("resultado objetos " + objetos );
                resultadosCuentaIngreso = libroDao.resultadosCuentasPorFechaS(libro,fechaD,fechaH, true, false);
               // System.out.println("resultado CuentaIngreso " + resultadosCuentaIngreso );  
                                                                                           //boolean flagIva,boolean flagNat
                resultadoMensualIngresos = libroDao.resultadosMensualesS(libro,fechaD,fechaH, true, false);
                //System.out.println("resultado Ingresos " + resultadoMensualIngresos );
                resultadosCuentaEgreso = libroDao.resultadosCuentasPorFechaS(libro,fechaD,fechaH, true, true);
               // System.out.println("resultado CuentaEgreso " + resultadosCuentaEgreso );
                resultadoMensualEgresos = libroDao.resultadosMensualesS(libro,fechaD,fechaH, true, true);
               // System.out.println("resultado egresos " + resultadoMensualEgresos );
            
        } catch (Exception e) {
            System.out.println("No se pudo encontrar datos del Libro \n"+e);
            return WebResponse.crearWebResponseError("No se pudo encontrar datos del Libro", e.getMessage() );
        }
        /*
        *  Repuesta Correcta
        */ 
        DateFormat fechaHora = new SimpleDateFormat("yyyy/MM/dd");
          
        JSONObject oResponse = new JSONObject();
        
            JSONObject oLibro = new JSONObject();

            oLibro.put("libroID",libro.getLibCajId());
            oLibro.put("nombre",libro.getNom());
            oLibro.put("observacion",libro.getObs());
            oLibro.put("fechaApertura",fechaHora.format(libro.getFecApe())); 
            oLibro.put("fechaCierre",fechaHora.format(libro.getFecCie()));  
            oLibro.put("saldoActual",libro.getSalAct());   
            oLibro.put("saldoApertura",libro.getSalApe());   
            oLibro.put("estado",""+libro.getEstReg());
            oLibro.put("organizacionID",organizacionID);
            oLibro.put("personaID",libro.getPersona().getPerId());
            
        oResponse.put("libro",oLibro);
        oResponse.put("estado",libro.getEstReg());
          
      JSONObject jsaldoDesde = new JSONObject();
      if(saldoDesde!=null){ 
          
                jsaldoDesde.put("mes", saldoDesde.getMes());
                jsaldoDesde.put("nombreMes", saldoDesde.getNombreMes());
                jsaldoDesde.put("debe", saldoDesde.getDebe());
                jsaldoDesde.put("haber", saldoDesde.getHaber());
                jsaldoDesde.put("saldo", saldoDesde.getSaldo());
      }
     
     oResponse.put("saldoDesde",jsaldoDesde);
      
          
      JSONArray jResultados = new JSONArray();    
      for(ResultadoMensual result:resultados){
           JSONObject jresult = new JSONObject();
           if(result!=null){ 
          
                jresult.put("mes", result.getMes());
                jresult.put("nombreMes", result.getNombreMes());
                jresult.put("debe", result.getDebe());
                jresult.put("haber", result.getHaber());
                jresult.put("saldo", result.getSaldo());
             }
           jResultados.put(jresult);
      }
      
     oResponse.put("resultados",jResultados); 
      
      
      
        DateFormat fechaA = new SimpleDateFormat("dd/MM/yyyy");    
        JSONArray miArray = new JSONArray();
        int i=1;
        for(List<Object> list:objetos ){
              JSONObject oAsiento = new JSONObject();
              
            if(list.get(0)!=null){
                Asiento objA=(Asiento)list.get(0);               
                oAsiento.put("i",i++); 
                oAsiento.put("operacionID",objA.getOpeId());
                oAsiento.put("codUniOpeId",objA.getCodUniOpeId());
                oAsiento.put("glosa",objA.getGloOpe());
              //oResponse.put("importe",importe);
                oAsiento.put("numeroD",objA.getNumDoc());
                oAsiento.put("observacion",objA.getObs());
                oAsiento.put("estado",objA.getEstReg());
                oAsiento.put("libro",objA.getCodLibro());                             
                oAsiento.put("fecha",fechaA.format(objA.getFecAsi()));                             
                
                String registro= ""+objA.getCodLibro();
                
                  for(DetalleCuenta det:objA.getDetalleCuentas()){                       
                                             
                       if(det.getNatDetCue()){
                           JSONObject jDebe = new JSONObject();         
                                jDebe.put("cuentaContableID", det.getCuentaContable().getCueConId());
                                jDebe.put("nombre",det.getCuentaContable().getNomCue());  
                                jDebe.put("importe",det.getImpDetCue()); 
                           oAsiento.put("importe",det.getImpDetCue());
                           oAsiento.put("debe",jDebe);
                       }
                       else{
                            JSONObject jHaber = new JSONObject();         
                                jHaber.put("cuentaContableID", det.getCuentaContable().getCueConId());
                                jHaber.put("nombre",det.getCuentaContable().getNomCue());  
                                jHaber.put("importe",det.getImpDetCue()); 
                            oAsiento.put("importe",det.getImpDetCue());
                            oAsiento.put("haber",jHaber);
                                   }
                       
                  }                                                                                                   
                
                 if(list.get(1)!=null && registro.equals("C")){
                     RegistroCompras objC=(RegistroCompras)list.get(1);
                     
                    JSONObject jTipoPago = new JSONObject();         
                        jTipoPago.put("tipoPagoID", objC.getTipoPago().getTipPagId());
                        jTipoPago.put("nombre",objC.getTipoPago().getNom());        
                    oAsiento.put("tipoPago",jTipoPago);  
                    
                     JSONObject jClienteProveedor = new JSONObject();         
                        jClienteProveedor.put("clienteProveedorID", objC.getClienteProveedor().getCliProId());
                        jClienteProveedor.put("datos",objC.getClienteProveedor().getDat());        
                        jClienteProveedor.put("estado",objC.getClienteProveedor().getEstReg());   
                        jClienteProveedor.put("tipoDocumento",objC.getClienteProveedor().getTipoDocumentoIdentidad().getNom());        
                        jClienteProveedor.put("estado",objC.getClienteProveedor().getEstReg()); 
                    oAsiento.put("clienteProveedor",jClienteProveedor); 
                    
                    JSONObject jDoc = new JSONObject(); 
                        jDoc.put("url",Sigesmed.UBI_ARCHIVOS+"/contable/");
                        jDoc.put("nombreArchivo",objC.getNomDocAdj());
                        jDoc.put("edi",true);
                        
                    oAsiento.put("doc",jDoc); 
                    
                     
                 }                 
                 else if(list.get(1)!=null && registro.equals("V")){
                     RegistroVentas objV=(RegistroVentas)list.get(1);
                    JSONObject jTipoPago = new JSONObject();         
                        jTipoPago.put("tipoPagoID", objV.getTipoPago().getTipPagId());
                        jTipoPago.put("nombre",objV.getTipoPago().getNom());        
                    oAsiento.put("tipoPago",jTipoPago);    
                    
                     JSONObject jClienteProveedor = new JSONObject();         
                        jClienteProveedor.put("clienteProveedorID", objV.getClienteProveedor().getCliProId());
                        jClienteProveedor.put("datos",objV.getClienteProveedor().getDat());        
                        jClienteProveedor.put("estado",objV.getClienteProveedor().getEstReg());   
                        jClienteProveedor.put("tipoDocumento",objV.getClienteProveedor().getTipoDocumentoIdentidad().getNom());        
                        jClienteProveedor.put("estado",objV.getClienteProveedor().getEstReg()); 
                    oAsiento.put("clienteProveedor",jClienteProveedor);
                    
                        JSONObject jDoc = new JSONObject(); 
                        jDoc.put("url",Sigesmed.UBI_ARCHIVOS+"/contable/");
                        jDoc.put("nombreArchivo",objV.getNomDocAdj());
                        jDoc.put("edi",true);
                        
                    
                    oAsiento.put("doc",jDoc); 
                                        
                 }
                 
          
            
        }                    
          miArray.put(oAsiento);       
            
        }
        
        oResponse.put("asientos",miArray); 
        
        JSONArray jAcuentasI = new JSONArray();
        JSONObject jCuentaI = null;
        ResultadosMensualPorCuenta re;
        BigDecimal total= null;
        for(int l=0; l<resultadosCuentaIngreso.size();l++){
           
            re=resultadosCuentaIngreso.get(l);
            if(l==0){
               jCuentaI= new JSONObject();
                total= new BigDecimal(BigInteger.ZERO);
                jCuentaI.put("nombre",re.getNombreCuenta());
                jCuentaI.put(registro(re.getMes()),re.getImporte()); 
                total=total.add(re.getImporte()).setScale(2);
                if(l==resultadosCuentaIngreso.size()-1){
                    jCuentaI.put("total", total);
                    jAcuentasI.put(jCuentaI);
               }
            }
            else if(resultadosCuentaIngreso.get(l-1).getNumeroCuenta() == re.getNumeroCuenta()) {
               jCuentaI.put(registro(re.getMes()),re.getImporte()); 
               total=total.add(re.getImporte()).setScale(2);
               if(l==resultadosCuentaIngreso.size()-1){
                    jCuentaI.put("total", total);
                    jAcuentasI.put(jCuentaI);
               }
            }
            
            else{
                jCuentaI.put("total", total);
                jAcuentasI.put(jCuentaI);
                jCuentaI = new JSONObject(); 
                total= new BigDecimal(BigInteger.ZERO);
                jCuentaI.put("nombre",re.getNombreCuenta());
                jCuentaI.put(registro(re.getMes()),re.getImporte()); 
                total=total.add(re.getImporte()).setScale(2);
                if(l==resultadosCuentaIngreso.size()-1){
                    jCuentaI.put("total", total);
                    jAcuentasI.put(jCuentaI);
               }
                
            }
        }
        
        oResponse.put("cuentasIngreso",jAcuentasI);
        
       
        JSONObject jTotalIngreso = new JSONObject();
        BigDecimal totalIngresos= new BigDecimal(BigInteger.ZERO); 
        for(ResultadoMensual res:resultadoMensualIngresos ){                                                                
                jTotalIngreso.put(res.getNombreMes(),res.getSaldo()); 
                totalIngresos =totalIngresos.add(res.getSaldo()).setScale(2);
         }
          jTotalIngreso.put("totalIngresos",totalIngresos);
          oResponse.put("totalIngreso",jTotalIngreso);
        
        JSONArray jAcuentasE = new JSONArray();
        JSONObject jCuentaE = null;
        ResultadosMensualPorCuenta reE;
        BigDecimal totalE= null;
        for(int l=0; l<resultadosCuentaEgreso.size();l++){
           
            reE=resultadosCuentaEgreso.get(l);
            if(l==0){
               jCuentaE= new JSONObject();
                totalE= new BigDecimal(BigInteger.ZERO);
                jCuentaE.put("nombre",reE.getNombreCuenta());
                jCuentaE.put(registro(reE.getMes()),reE.getImporte()); 
                totalE=totalE.add(reE.getImporte()).setScale(2);
                if(l==resultadosCuentaEgreso.size()-1){
                    jCuentaE.put("total", totalE);
                    jAcuentasE.put(jCuentaE);
               }
            }
            else if(resultadosCuentaEgreso.get(l-1).getNumeroCuenta() == reE.getNumeroCuenta()) {
               jCuentaE.put(registro(reE.getMes()),reE.getImporte()); 
               totalE=totalE.add(reE.getImporte()).setScale(2);
               if(l==resultadosCuentaEgreso.size()-1){
                    jCuentaE.put("total", totalE);
                    jAcuentasE.put(jCuentaE);
               }
            }
            
            else{
                jCuentaE.put("total", totalE);
                jAcuentasE.put(jCuentaE);
                jCuentaE = new JSONObject(); 
                totalE= new BigDecimal(BigInteger.ZERO);
                jCuentaE.put("nombre",reE.getNombreCuenta());
                jCuentaE.put(registro(reE.getMes()),reE.getImporte()); 
                totalE=totalE.add(reE.getImporte()).setScale(2);
                if(l==resultadosCuentaEgreso.size()-1){
                    jCuentaE.put("total", totalE);
                    jAcuentasE.put(jCuentaE);
               }
                
            }
        }
        
        oResponse.put("cuentasEgreso",jAcuentasE);
        
       
        JSONObject jTotalEgreso = new JSONObject();
        BigDecimal totalEgresos= new BigDecimal(BigInteger.ZERO); 
        for(ResultadoMensual res:resultadoMensualEgresos ){                                                                
                jTotalEgreso.put(res.getNombreMes(),res.getSaldo()); 
                System.out.println("Mes -------   Saldo"+res.getNombreMes()+res.getSaldo());

                totalEgresos =totalEgresos.add(res.getSaldo()).setScale(2);
         }
          jTotalEgreso.put("totalEgresos",totalEgresos);
          oResponse.put("totalEgreso",jTotalEgreso);
          
          JSONObject datosOrganizacion = new JSONObject();  
         if(organizacion!=null){                     
          
            if(organizacion.getOrganizacionPadre()!=null)
                datosOrganizacion.put("organizacionPadre",organizacion.getOrganizacionPadre().getNom());
            else
                datosOrganizacion.put("organizacionPadre","" );
            datosOrganizacion.put("codigo",organizacion.getCod());
            datosOrganizacion.put("nombre",organizacion.getNom());           
            datosOrganizacion.put("direccion",organizacion.getDir());                      
           }
        oResponse.put("datosOrganizacion",datosOrganizacion);
          
          
        return WebResponse.crearWebResponseExito("Se Listo correctamente",oResponse);        
        //Fin
    }
    
    public String registro(int re){
        String mes="";
         switch (re){
                    case 1: mes ="ene";
                    break;
                    case 2: mes="feb";
                    break;
                    case 3: mes="mar";
                    break;
                    case 4: mes="abr";
                    break;                                      
                    case 5: mes= "may";
                    break;
                    case 6: mes="jun";
                    break;
                    case 7: mes="jul";
                    break;
                    case 8: mes="Ago";
                    break;
                    case 9: mes="sep";
                    break;
                    case 10: mes="oct";
                    break;
                    case 11: mes="nov";
                    break;
                    case 12: mes="dic";
                    break;                    
                }
         return mes;
    }
    
}
