/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.cpe;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TrabajadorCargo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.ConfiguracionControl;
import java.util.List;

/**
 *
 * @author carlos
 */
public interface PersonalDao extends GenericDao<Trabajador>{
   
//       public Persona buscarPorDNI(Integer dni,Organizacion org);
    public List<TrabajadorCargo> listarCargoTrabajadorByTipo(String tipoTrabajador);
    public Persona buscarPorDNI(String dni);
    public List<TrabajadorCargo> listarCargoTrabajador();
    public List<Organizacion> listarOrganizacionesActivasDelTrabajador(String dni,List<Integer> orgId);
    public Trabajador buscarTrabajadorxPersona(Integer per);
    public Persona buscarPersonaxDNI(String dni);
    public Trabajador getTrabajadorByPersonaOrganizacion(Persona persona,Organizacion org);
}