/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.control_libro.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.ControlLibroDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.std.EstadoExpedienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.ControlLibro;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.EstadoExpediente;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class EliminarControlTx implements ITransaction{
    
    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int controlD = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            controlD = requestData.getInt("controlD");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo eliminar, datos incorrectos");
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        ControlLibroDao controlDao = (ControlLibroDao)FactoryDao.buildDao("sci.ControlLibroDao");
        try{
            controlDao.delete(new ControlLibro((short)(controlD)));
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar el Estado Expediente\n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el Estado Expediente", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Estado Expediente se elimino correctamente");
        //Fin
    }
}
