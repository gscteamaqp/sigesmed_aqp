/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author gscadmin
 */
@Entity
@Table(name = "monitoreo", schema = "pedagogico")

public class Monitoreo implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "mni_id")
    private Integer mniId;
    
    @Column(name = "mni_cod")
    private String mniCod;
    
    @Column(name = "mni_yy")
    private String mniYy;
    
    @Column(name = "mni_doc")
    private String mniNom;
    
    @Column(name = "mni_url")
    private String mniUrl;  

    @Column(name = "fec_reg")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fecReg;
    
    @Column(name = "num_esp")
    private Integer numEsp;
    
    @Column(name = "num_doc")
    private Integer numDoc;
    
    @Column(name = "est_mon")
    private String estMon; 
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.DATE)
    private Date fecMod;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "est_reg")
    private String estReg;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "org_id", referencedColumnName = "org_id")    
    private Organizacion organizacion;
    
    @OneToMany(mappedBy = "mniId", fetch = FetchType.LAZY)
    private List<MonitoreoDetalle> monitoreoDetalleList;

    public Monitoreo() {
    }

    public Monitoreo(Integer mniId) {
        this.mniId = mniId;
    }
    
    public Monitoreo(String mniNom, Organizacion organizacion, Integer numEsp, Integer numDoc, Integer usuMod) {
        this.mniNom = mniNom;
        this.organizacion = organizacion;
        this.fecReg = new Date();
        this.mniYy = fecReg.toString().substring(24, 28);
        this.numEsp = numEsp;
        this.numDoc = numDoc;
        this.estMon = "NR";
        this.usuMod = usuMod;
        this.fecMod = new Date();
        this.estReg = "A";
    }

    public Integer getMniId() {
        return mniId;
    }

    public void setMniId(Integer mniId) {
        this.mniId = mniId;
    }

    public String getMniCod() {
        return mniCod;
    }

    public void setMniCod(String mniCod) {
        this.mniCod = mniCod;
    }

    public String getMniYy() {
        return mniYy;
    }

    public void setMniYy(String mniYy) {
        this.mniYy = mniYy;
    }

    public String getMniNom() {
        return mniNom;
    }

    public void setMniNom(String mniNom) {
        this.mniNom= mniNom;
    }

    public String getMniUrl() {
        return mniUrl;
    }

    public void setMniUrl(String mniUrl) {
        this.mniUrl = mniUrl;
    }
    
    public Integer getNumEsp() {
        return numEsp;
    }

    public void setNumEsp(Integer numEsp) {
        this.numEsp = numEsp;
    }
    
    public Integer getNumDoc() {
        return numDoc;
    }

    public void setNumDoc(Integer numDoc) {
        this.numDoc = numDoc;
    }

    public Date getFecReg() {
        return fecReg;
    }

    public void setFecReg(Date fecReg) {
        this.fecReg = fecReg;
    }
    
    public String getEstMon() {
        return estMon;
    }

    public void setEstMon(String estMon) {
        this.estMon = estMon;
    }
        
    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }
    
    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public Organizacion getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(Organizacion organizacion) {
        this.organizacion = organizacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mniId != null ? mniId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Monitoreo)) {
            return false;
        }
        Monitoreo other = (Monitoreo) object;
        if ((this.mniId == null && other.mniId != null) || (this.mniId != null && !this.mniId.equals(other.mniId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Monitoreo{" + "mniId=" + mniId + ", mniCod=" + mniCod + ", mniYy=" + mniYy + ", fecReg=" + fecReg + ", numEsp=" + numEsp + ", numDoc=" + numDoc + ", estMon=" + estMon + ", fecMod=" + fecMod + ", usuMod=" + usuMod + ", estReg=" + estReg + ", organizacion=" + organizacion + '}';
    }
    
    
    
}
