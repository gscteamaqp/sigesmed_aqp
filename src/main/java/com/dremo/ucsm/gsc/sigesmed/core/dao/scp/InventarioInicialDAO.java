/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.scp;
import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import java.util.Date;
import java.util.List;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.InventarioInicial;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.InventarioInicialDetalle;
/**
 *
 * @author Administrador
 */
public interface InventarioInicialDAO extends GenericDao<InventarioInicial>{
    
    public List<InventarioInicial> listarInventarioInicial(int org_id);

    public InventarioInicial obtenerInventarioInicial(int inv_id , int org_id );
    public List<InventarioInicialDetalle> obtenerDetalleInventarioInicial(int inv_id , int org_id);
    public void eliminar_detalle(int inv_ini_id);
    public void eliminar_inventario(int id_inv);
    public List<InventarioInicial> reporteInventario(int org_id);
}
