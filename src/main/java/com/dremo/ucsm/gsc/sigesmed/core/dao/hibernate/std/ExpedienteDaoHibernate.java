/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.std;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.*;
import com.dremo.ucsm.gsc.sigesmed.core.dao.std.ExpedienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.DocumentoExpediente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.EntidadCantidadModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.Expediente;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author abel
 */
public class ExpedienteDaoHibernate extends GenericDaoHibernate<Expediente> implements ExpedienteDao {
    
    @Override
    public String buscarUltimoCodigo() {
        String codigo = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar ultimo id de tramite
            String hql = "SELECT e.expId FROM Expediente e WHERE e.estReg!='E' ORDER BY e.expId DESC ";
            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            //codigo = ((String)query.uniqueResult());
            codigo = query.uniqueResult().toString();
            
            //solo cuando no hay ningun registro aun
            if(codigo==null)
                codigo = "00000000";
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return codigo;
    }
    
    @Override
    public List<Expediente> buscarPorOrganizacion(int organizacionID) {
        List<Expediente> tramites = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar Tipo Expedientes
            String hql = "SELECT e FROM Expediente e JOIN FETCH e.tipoTramite JOIN FETCH e.prioridad WHERE e.orgId=:p1 and e.estReg!='E'";
            Query query = session.createQuery(hql);
            query.setParameter("p1", organizacionID);
            tramites = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los Expedientes por organizacion\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los Expedientes por organizacion\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return tramites;
    }
    
    @Override
    public List<Expediente> buscarPorCodigo(String codigo) {
        List<Expediente> tramites = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT e FROM Expediente e JOIN FETCH e.tipoTramite JOIN FETCH e.prioridad LEFT JOIN FETCH e.persona LEFT JOIN FETCH e.empresa WHERE e.codigo =:p1 and e.fecEnt IS NULL";
            Query query = session.createQuery(hql);
            query.setParameter("p1", codigo);
            
            tramites = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los Expedientes por oFecha\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los Expedientes por Fecha\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return tramites;
    }
    
    @Override
    public List<Expediente> buscarPorNombreApellido(String nombre, String apellidoP, String apellidoM){
        List<Expediente> tramites = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT e FROM Expediente e JOIN FETCH e.tipoTramite JOIN FETCH e.prioridad LEFT JOIN FETCH e.persona LEFT JOIN FETCH e.empresa WHERE e.persona.nom =:p1 and e.persona.apePat =:p2 and e.persona.apeMat =:p3";
            Query query = session.createQuery(hql);
            query.setParameter("p1", nombre);
            query.setParameter("p2", apellidoP);
            query.setParameter("p3", apellidoM);
            
            tramites = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los Expedientes por oFecha\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los Expedientes por Fecha\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return tramites;
    }
    @Override
    public List<Expediente> buscarPorFecha(Date desde, Date hasta) {
        List<Expediente> tramites = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "";
            Query query = null;
            if(desde != null){
                if(hasta != null){
                    hql = "SELECT e FROM Expediente e JOIN FETCH e.tipoTramite JOIN FETCH e.prioridad LEFT JOIN FETCH e.persona LEFT JOIN FETCH e.empresa JOIN FETCH e.organizacion WHERE e.fecIni >:p2 and e.fecIni <:p3 and e.fecEnt IS NULL";
                    query = session.createQuery(hql);
                    query.setTimestamp("p3", hasta);
                }
                else{
                    hql = "SELECT e FROM Expediente e JOIN FETCH e.tipoTramite JOIN FETCH e.prioridad LEFT JOIN FETCH e.persona LEFT JOIN FETCH e.empresa JOIN FETCH e.organizacion WHERE e.fecIni >:p2 and e.fecEnt IS NULL";
                    query = session.createQuery(hql);
                }            
                
                query.setDate("p2", desde);                
            }
            else{
                if(hasta != null){                    
                    hql = "SELECT e FROM Expediente e JOIN FETCH e.tipoTramite JOIN FETCH e.prioridad  JOIN FETCH e.persona LEFT JOIN FETCH e.empresa JOIN FETCH e.organizacion WHERE e.fecIni <:p3 and e.fecEnt IS NULL";
                    query = session.createQuery(hql);
                    query.setTimestamp("p3", hasta);
                }
                else
                    return tramites;
            }
            
            tramites = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los Expedientes por Fecha\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los Expedientes por Fecha\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return tramites;
    }
    
    @Override
    public List<Expediente> buscarPorOrganizacionYFecha(int organizacionID, Date desde, Date hasta) {
        List<Expediente> tramites = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "";
            Query query = null;
            if(desde != null){
                if(hasta != null){
                    hql = "SELECT e FROM Expediente e JOIN FETCH e.tipoTramite JOIN FETCH e.prioridad LEFT JOIN FETCH e.persona LEFT JOIN FETCH e.empresa WHERE e.orgId=:p1 and e.fecIni >:p2 and e.fecIni <:p3 and e.fecEnt IS NULL";
                    query = session.createQuery(hql);
                    query.setTimestamp("p3", hasta);
                }
                else{
                    hql = "SELECT e FROM Expediente e JOIN FETCH e.tipoTramite JOIN FETCH e.prioridad LEFT JOIN FETCH e.persona LEFT JOIN FETCH e.empresa WHERE e.orgId=:p1 and e.fecIni >:p2 and e.fecEnt IS NULL";
                    query = session.createQuery(hql);
                }            
                
                query.setDate("p2", desde);                
            }
            else{
                if(hasta != null){                    
                    hql = "SELECT e FROM Expediente e JOIN FETCH e.tipoTramite JOIN FETCH e.prioridad LEFT JOIN FETCH e.persona LEFT JOIN FETCH e.empresa WHERE e.orgId=:p1 and e.fecIni <:p3 and e.fecEnt IS NULL";
                    query = session.createQuery(hql);
                    query.setTimestamp("p3", hasta);
                }
                else
                    return tramites;
            }            
            query.setParameter("p1", organizacionID);
            
            tramites = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los Expedientes por organizacion y Fecha\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los Expedientes por organizacion y Fecha\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return tramites;
    }    
    
    @Override
    public List<Expediente> buscarPorOrganizacionYFechaYDNI(int organizacionID, Date desde, Date hasta, String dni) {
        List<Expediente> tramites = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "";
            Query query = null;
            if(desde != null){
                if(hasta != null){
                    hql = "SELECT e FROM Expediente e JOIN FETCH e.tipoTramite JOIN FETCH e.prioridad JOIN FETCH e.persona WHERE e.orgId=:p1 and e.persona.dni=:p4 and e.fecIni >:p2 and e.fecIni <:p3 and e.fecEnt IS NULL";
                    query = session.createQuery(hql);
                    query.setTimestamp("p3", hasta);
                }
                else{
                    hql = "SELECT e FROM Expediente e JOIN FETCH e.tipoTramite JOIN FETCH e.prioridad JOIN FETCH e.persona WHERE e.orgId=:p1 and e.persona.dni=:p4 and e.fecIni >:p2 and e.fecEnt IS NULL";
                    query = session.createQuery(hql);
                }            
                
                query.setDate("p2", desde);                
            }
            else{
                if(hasta != null){                    
                    hql = "SELECT e FROM Expediente e JOIN FETCH e.tipoTramite JOIN FETCH e.prioridad JOIN FETCH e.persona WHERE e.orgId=:p1 and e.persona.dni=:p4 and e.fecIni <:p3 and e.fecEnt IS NULL";
                    query = session.createQuery(hql);
                    query.setTimestamp("p3", hasta);
                }
                else
                    return tramites;
            }            
            query.setParameter("p1", organizacionID);
            query.setParameter("p4", dni);
            
            tramites = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los Expedientes por organizacion y Fecha Y DNI\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los Expedientes por organizacion y Fecha Y DNI\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return tramites;
    }
    
    @Override
    public List<Expediente> buscarPorOrganizacionYFechaYRUC(int organizacionID, Date desde, Date hasta, String ruc) {
        List<Expediente> tramites = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "";
            Query query = null;
            if(desde != null){
                if(hasta != null){
                    hql = "SELECT e FROM Expediente e JOIN FETCH e.tipoTramite JOIN FETCH e.prioridad JOIN FETCH e.empresa WHERE e.orgId=:p1 and e.empresa.ruc=:p4 and e.fecIni >:p2 and e.fecIni <:p3 and e.fecEnt IS NULL";
                    query = session.createQuery(hql);
                    query.setTimestamp("p3", hasta);
                }
                else{
                    hql = "SELECT e FROM Expediente e JOIN FETCH e.tipoTramite JOIN FETCH e.prioridad JOIN FETCH e.empresa WHERE e.orgId=:p1 and e.empresa.ruc=:p4 and e.fecIni >:p2 and e.fecEnt IS NULL";
                    query = session.createQuery(hql);
                }            
                
                query.setDate("p2", desde);                
            }
            else{
                if(hasta != null){                    
                    hql = "SELECT e FROM Expediente e JOIN FETCH e.tipoTramite JOIN FETCH e.prioridad JOIN FETCH e.persona WHERE e.orgId=:p1 and e.persona.dni=:p4 and e.fecIni <:p3 and e.fecEnt IS NULL";
                    query = session.createQuery(hql);
                    query.setTimestamp("p3", hasta);
                }
                else
                    return tramites;
            }            
            query.setParameter("p1", organizacionID);
            query.setParameter("p4", ruc);
            
            tramites = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los Expedientes por organizacion y Fecha Y RUC\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los Expedientes por organizacion y Fecha Y RUC\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return tramites;
    }
    
    @Override
    public List<Expediente> buscarEnTramitePorOrganizacionYFecha(int organizacionID, Date desde, Date hasta) {
        List<Expediente> tramites = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "";
            Query query = null;
            if(desde != null){
                if(hasta != null){
                    hql = "SELECT e FROM Expediente e JOIN FETCH e.tipoTramite JOIN FETCH e.prioridad LEFT JOIN FETCH e.persona LEFT JOIN FETCH e.empresa WHERE e.orgId=:p1 and e.fecIni >:p2 and e.fecIni <:p3 and e.fecFin IS NULL";
                    query = session.createQuery(hql);
                    query.setTimestamp("p3", hasta);
                }
                else{
                    hql = "SELECT e FROM Expediente e JOIN FETCH e.tipoTramite JOIN FETCH e.prioridad LEFT JOIN FETCH e.persona LEFT JOIN FETCH e.empresa WHERE e.orgId=:p1 and e.fecIni >:p2 and e.fecFin IS NULL";
                    query = session.createQuery(hql);
                }            
                
                query.setDate("p2", desde);                
            }
            else{
                if(hasta != null){                    
                    hql = "SELECT e FROM Expediente e JOIN FETCH e.tipoTramite JOIN FETCH e.prioridad LEFT JOIN FETCH e.persona LEFT JOIN FETCH e.empresa WHERE e.orgId=:p1 and e.fecIni <:p3 and e.fecFin IS NULL";
                    query = session.createQuery(hql);
                    query.setTimestamp("p3", hasta);
                }
                else
                    return tramites;
            }            
            query.setParameter("p1", organizacionID);
            
            tramites = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los Expedientes Finalizados por organizacion y Fecha\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los Expedientes Finalizados por organizacion y Fecha\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return tramites;
    }
    @Override
    public List<Expediente> buscarFinalizadosPorOrganizacionYFecha(int organizacionID, Date desde, Date hasta) {
        List<Expediente> tramites = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "";
            Query query = null;
            if(desde != null){
                if(hasta != null){
                    hql = "SELECT e FROM Expediente e JOIN FETCH e.tipoTramite JOIN FETCH e.prioridad LEFT JOIN FETCH e.persona LEFT JOIN FETCH e.empresa WHERE e.orgId=:p1 and e.fecFin >:p2 and e.fecFin <:p3 and e.fecEnt IS NULL";
                    query = session.createQuery(hql);
                    query.setTimestamp("p3", hasta);
                }
                else{
                    hql = "SELECT e FROM Expediente e JOIN FETCH e.tipoTramite JOIN FETCH e.prioridad LEFT JOIN FETCH e.persona LEFT JOIN FETCH e.empresa WHERE e.orgId=:p1 and e.fecFin >:p2 and e.fecEnt IS NULL";
                    query = session.createQuery(hql);
                }            
                
                query.setDate("p2", desde);                
            }
            else{
                if(hasta != null){                    
                    hql = "SELECT e FROM Expediente e JOIN FETCH e.tipoTramite JOIN FETCH e.prioridad LEFT JOIN FETCH e.persona LEFT JOIN FETCH e.empresa WHERE e.orgId=:p1 and e.fecFin <:p3 and e.fecEnt IS NULL";
                    query = session.createQuery(hql);
                    query.setTimestamp("p3", hasta);
                }
                else
                    return tramites;
            }            
            query.setParameter("p1", organizacionID);
            
            tramites = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los Expedientes Finalizados por organizacion y Fecha\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los Expedientes Finalizados por organizacion y Fecha\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return tramites;
    }
    @Override
    public List<Expediente> buscarEntregadosPorOrganizacionYFecha(int organizacionID, Date desde, Date hasta) {
        List<Expediente> tramites = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "";
            Query query = null;
            if(desde != null){
                if(hasta != null){
                    hql = "SELECT e FROM Expediente e JOIN FETCH e.tipoTramite JOIN FETCH e.prioridad LEFT JOIN FETCH e.persona LEFT JOIN FETCH e.empresa WHERE e.orgId=:p1 and e.fecEnt >:p2 and e.fecEnt <:p3";
                    query = session.createQuery(hql);
                    query.setTimestamp("p3", hasta);
                }
                else{
                    hql = "SELECT e FROM Expediente e JOIN FETCH e.tipoTramite JOIN FETCH e.prioridad LEFT JOIN FETCH e.persona LEFT JOIN FETCH e.empresa WHERE e.orgId=:p1 and e.fecEnt >:p2";
                    query = session.createQuery(hql);
                }            
                
                query.setDate("p2", desde);                
            }
            else{
                if(hasta != null){                    
                    hql = "SELECT e FROM Expediente e JOIN FETCH e.tipoTramite JOIN FETCH e.prioridad LEFT JOIN FETCH e.persona LEFT JOIN FETCH e.empresa WHERE e.orgId=:p1 and e.fecEnt <:p3";
                    query = session.createQuery(hql);
                    query.setTimestamp("p3", hasta);
                }
                else
                    return tramites;
            }            
            query.setParameter("p1", organizacionID);
            
            tramites = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los Expedientes Finalizados por organizacion y Fecha\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los Expedientes Finalizados por organizacion y Fecha\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return tramites;
    }
    
    @Override
    public List<DocumentoExpediente> buscarDocumentosPorExpediente(int expedienteID) {
        List<DocumentoExpediente> historiales = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar ultimo codigo de tramite
            String hql = "SELECT d FROM DocumentoExpediente d JOIN FETCH d.tipoDocumento WHERE d.expediente.expId =:p1 ORDER BY d.docExpId";
            Query query = session.createQuery(hql);
            query.setParameter("p1", expedienteID);
            historiales = query.list();            
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar los documentos por expediente \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar los documentos por expediente \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return historiales;
    }
    @Override
    public List<String> buscarDocumentosPorExpedienteC(String expedienteCod) {
        List<String> historiales = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{

            //listar ultimo codigo de tramite
            String hql = "SELECT d.arcAdj FROM Expediente e , DocumentoExpediente d WHERE e.codigo =:p1 and e.expId = d.expediente.expId";
            Query query = session.createQuery(hql);
            query.setParameter("p1", expedienteCod);
            historiales = query.list();            
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar los documentos por expediente \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar los documentos por expediente \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return historiales;
    }
    @Override
    public void finalizarExpediente(int expedienteID,int usuarioID){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            String hql = "UPDATE Expediente e SET e.fecFin=:p2 , e.usuMod=:p3 WHERE e.expId =:p1";

            Query query = session.createQuery(hql);
            query.setParameter("p1", expedienteID);
            query.setParameter("p2", new Date());
            query.setParameter("p3", usuarioID);
            query.executeUpdate();
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo finalizar expediente" + this.getClass().getName() + "\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    @Override
    public void finalizarExpedientes(List<Integer> expedientesID,int usuarioID){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            
            for(Integer id: expedientesID){
                String hql = "UPDATE Expediente e SET e.fecFin=:p2 , e.usuMod=:p3 WHERE e.expId =:p1";

                Query query = session.createQuery(hql);
                query.setParameter("p1", id);
                query.setParameter("p2", new Date());
                query.setParameter("p3", usuarioID);
                query.executeUpdate();
            }
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo finalizar expediente varios" + this.getClass().getName() + "\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    @Override
    public void entregarExpediente(int expedienteID,int usuarioID){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            String hql = "UPDATE Expediente e SET e.fecEnt=:p2 , e.usuMod=:p3 WHERE e.expId =:p1";

            Query query = session.createQuery(hql);
            query.setParameter("p1", expedienteID);
            query.setParameter("p2", new Date());
            query.setParameter("p3", usuarioID);
            query.executeUpdate();
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo entregar expediente" + this.getClass().getName() + "\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    
    @Override
    public List<EntidadCantidadModel> cantidadTipoExpedientesPorOrganizacionYFecha(int organizacionID, Date desde, Date hasta) {
        List<EntidadCantidadModel> expedientes = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "";
            Query query = null;
            if(desde != null){
                if(hasta != null){
                    hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.std.EntidadCantidadModel(tt.tipTraId,tt.nom,COUNT(e.expId)) FROM Expediente e join e.tipoTramite as tt"+
                        " WHERE e.orgId=:p1 and e.fecIni >:p2 and h.fecIni <:p3 GROUP BY tt ORDER BY tt.tipTraId";
                    query = session.createQuery(hql);
                    query.setTimestamp("p3", hasta);
                }
                else{
                    hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.std.EntidadCantidadModel(tt.tipTraId,tt.nom,COUNT(e.expId)) FROM Expediente e join e.tipoTramite as tt"+
                        " WHERE e.orgId=:p1 and e.fecIni >:p2 GROUP BY tt ORDER BY tt.tipTraId";
                    query = session.createQuery(hql);
                }            
                
                query.setDate("p2", desde);                
            }
            else{
                if(hasta != null){
                    hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.std.EntidadCantidadModel(tt.tipTraId,tt.nom,COUNT(e.expId)) FROM Expediente e join e.tipoTramite as tt"+
                        " WHERE e.orgId=:p1 and h.fecIni <:p3 GROUP BY tt ORDER BY tt.tipTraId";
                    query = session.createQuery(hql);
                    query.setTimestamp("p3", hasta);
                }
                else
                    return expedientes;
            }            
            query.setParameter("p1", organizacionID);
            
            expedientes = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar la cantidad de tipos de tramite por organizacion\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar la cantidad de tipos de tramite por organizacion\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return expedientes;
    }
    @Override
    public List<EntidadCantidadModel> cantidadTipoExpedientesFinalizadosPorOrganizacionYFecha(int organizacionID, Date desde, Date hasta) {
        List<EntidadCantidadModel> expedientes = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "";
            Query query = null;
            if(desde != null){
                if(hasta != null){
                    hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.std.EntidadCantidadModel(tt.tipTraId,tt.nom,COUNT(e.expId)) FROM Expediente e join e.tipoTramite as tt"+
                        " WHERE e.orgId=:p1 and e.fecFin IS NOT NULL and e.fecIni >:p2 and h.fecIni <:p3 GROUP BY tt ORDER BY tt.tipTraId";
                    query = session.createQuery(hql);
                    query.setTimestamp("p3", hasta);
                }
                else{
                    hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.std.EntidadCantidadModel(tt.tipTraId,tt.nom,COUNT(e.expId)) FROM Expediente e join e.tipoTramite as tt"+
                        " WHERE e.orgId=:p1 and e.fecFin IS NOT NULL and e.fecIni >:p2 GROUP BY tt ORDER BY tt.tipTraId";
                    query = session.createQuery(hql);
                }            
                
                query.setDate("p2", desde);                
            }
            else{
                if(hasta != null){
                    hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.std.EntidadCantidadModel(tt.tipTraId,tt.nom,COUNT(e.expId)) FROM Expediente e join e.tipoTramite as tt"+
                        " WHERE e.orgId=:p1 and e.fecFin IS NOT NULL and h.fecIni <:p3 GROUP BY tt ORDER BY tt.tipTraId";
                    query = session.createQuery(hql);
                    query.setTimestamp("p3", hasta);
                }
                else
                    return expedientes;
            }            
            query.setParameter("p1", organizacionID);
            
            expedientes = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar la cantidad de tipos de tramite por organizacion Finalizados\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar la cantidad de tipos de tramite por organizacion Finalizados\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return expedientes;
    }
    
    @Override
    public List<EntidadCantidadModel> cantidadTipoExpedientesEntregadosPorOrganizacionYFecha(int organizacionID, Date desde, Date hasta) {
        List<EntidadCantidadModel> expedientes = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "";
            Query query = null;
            if(desde != null){
                if(hasta != null){
                    hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.std.EntidadCantidadModel(tt.tipTraId,tt.nom,COUNT(e.expId)) FROM Expediente e join e.tipoTramite as tt"+
                        " WHERE e.orgId=:p1 and e.fecEnt IS NOT NULL and e.fecIni >:p2 and h.fecIni <:p3 GROUP BY tt ORDER BY tt.tipTraId";
                    query = session.createQuery(hql);
                    query.setTimestamp("p3", hasta);
                }
                else{
                    hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.std.EntidadCantidadModel(tt.tipTraId,tt.nom,COUNT(e.expId)) FROM Expediente e join e.tipoTramite as tt"+
                        " WHERE e.orgId=:p1 and e.fecEnt IS NOT NULL and e.fecIni >:p2 GROUP BY tt ORDER BY tt.tipTraId";
                    query = session.createQuery(hql);
                }            
                
                query.setDate("p2", desde);                
            }
            else{
                if(hasta != null){
                    hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.std.EntidadCantidadModel(tt.tipTraId,tt.nom,COUNT(e.expId)) FROM Expediente e join e.tipoTramite as tt"+
                        " WHERE e.orgId=:p1 and e.fecEnt IS NOT NULL and h.fecIni <:p3 GROUP BY tt ORDER BY tt.tipTraId";
                    query = session.createQuery(hql);
                    query.setTimestamp("p3", hasta);
                }
                else
                    return expedientes;
            }            
            query.setParameter("p1", organizacionID);
            
            expedientes = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar la cantidad de tipos de tramite por organizacion Entregados\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar la cantidad de tipos de tramite por organizacion Entregados\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return expedientes;
    }
    
    @Override
    public List<EntidadCantidadModel> cantidadExpedientesPorFecha(Date desde, Date hasta) {
        List<EntidadCantidadModel> expedientes = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "";
            Query query = null;
            if(desde != null){
                if(hasta != null){
                    hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.std.EntidadCantidadModel(o.orgId, o.nom,tt.nom,COUNT(e.expId),COUNT( CASE WHEN e.fecFin IS NOT NULL THEN 1 ELSE null END),COUNT( CASE WHEN e.fecEnt IS NOT NULL THEN 1 ELSE null END) ) FROM Expediente e join e.organizacion as o"+
                        " WHERE e.fecIni >:p2 and h.fecIni <:p3 GROUP BY o ORDER BY o.orgId";
                    query = session.createQuery(hql);
                    query.setTimestamp("p3", hasta);
                }
                else{
                    hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.std.EntidadCantidadModel(o.orgId,o.nom,COUNT(e.expId),COUNT( CASE WHEN e.fecFin IS NOT NULL THEN 1 ELSE null END),COUNT( CASE WHEN e.fecEnt IS NOT NULL THEN 1 ELSE null END)) FROM Expediente e join e.organizacion as o"+
                        " WHERE e.fecIni >:p2 GROUP BY o ORDER BY o.orgId";
                    query = session.createQuery(hql);
                }            
                
                query.setDate("p2", desde);                
            }
            else{
                if(hasta != null){
                    hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.std.EntidadCantidadModel(o.orgId,o.nom,COUNT(e.expId),COUNT( CASE WHEN e.fecFin IS NOT NULL THEN 1 ELSE null END),COUNT( CASE WHEN e.fecEnt IS NOT NULL THEN 1 ELSE null END)) FROM Expediente e join e.organizacion as o"+
                        " WHERE h.fecIni <:p3 GROUP BY o ORDER BY o.orgId";
                    query = session.createQuery(hql);
                    query.setTimestamp("p3", hasta);
                }
                else
                    return expedientes;
            }
            
            expedientes = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar la cantidad expedientes\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar la cantidad de expedientes\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return expedientes;
    }
}
