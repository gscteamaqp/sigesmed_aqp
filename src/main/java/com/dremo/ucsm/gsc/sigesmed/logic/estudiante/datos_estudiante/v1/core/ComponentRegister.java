/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.estudiante.datos_estudiante.v1.core;

import com.dremo.ucsm.gsc.sigesmed.logic.padre_familia.v1.tx.ListarHijosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.mep.v1.core.*;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.estudiante.datos_estudiante.v1.tx.verificarEstudianteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.mep.v1.tx.*;
import com.dremo.ucsm.gsc.sigesmed.logic.estudiante.datos_estudiante.v1.tx.DatosEstudianteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.estudiante.datos_estudiante.v1.tx.DatosMatriculaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.estudiante.datos_estudiante.v1.tx.DatosTareasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.estudiante.datos_estudiante.v1.tx.ListarAreasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.estudiante.datos_estudiante.v1.tx.ListarAsistenciaAreasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.estudiante.datos_estudiante.v1.tx.ListarAsistenciaByAlumnoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.estudiante.datos_estudiante.v1.tx.ListarDetalleNotasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.estudiante.datos_estudiante.v1.tx.ListarNotasAnualesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.estudiante.datos_estudiante.v1.tx.ListarNotasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.estudiante.datos_estudiante.v1.tx.ListarTareasByAlumnoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.estudiante.datos_estudiante.v1.tx.ListarUtilesByAreaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.estudiante.datos_estudiante.v1.tx.ReporteAsistenciaByAreaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.estudiante.datos_estudiante.v1.tx.ReporteBoletaNotasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.estudiante.datos_estudiante.v1.tx.ReporteFichaEstudianteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.estudiante.datos_estudiante.v1.tx.ReporteListaUtilesByAreaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.estudiante.datos_estudiante.v1.tx.ReporteListaUtilesTx;

/**
 *
 * @author Carlos
 */
public class ComponentRegister implements IComponentRegister{

    @Override
    public WebComponent createComponent() {
        WebComponent mepComponent = new WebComponent(Sigesmed.MODULO_ESTUDIANTE);
        mepComponent.setName("datosEstudiante");
        mepComponent.setVersion(1);//verificarEstudiante
        mepComponent.addTransactionGET("verificarEstudiante", verificarEstudianteTx.class);
        mepComponent.addTransactionGET("datosEstudiante", DatosEstudianteTx.class);
        mepComponent.addTransactionGET("datosMatricula", DatosMatriculaTx.class);
        mepComponent.addTransactionGET("datosTareas", DatosTareasTx.class);
        mepComponent.addTransactionGET("listarUtilesByArea", ListarUtilesByAreaTx.class);
        mepComponent.addTransactionGET("listarAreas", ListarAreasTx.class);//listarBandejaTareaByAlumno
        mepComponent.addTransactionGET("listarBandejaTareaByAlumno", ListarTareasByAlumnoTx.class);//listarAsistenciaByAlumno
        mepComponent.addTransactionGET("listarAsistenciaByAlumno", ListarAsistenciaByAlumnoTx.class);
        mepComponent.addTransactionPOST("reporteFichaEstudiante", ReporteFichaEstudianteTx.class);
        mepComponent.addTransactionPOST("reporteListaUtilesByArea", ReporteListaUtilesByAreaTx.class);
        mepComponent.addTransactionPOST("reporteListaUtiles", ReporteListaUtilesTx.class);
        mepComponent.addTransactionPOST("reporteAsistenciaByArea", ReporteAsistenciaByAreaTx.class);
        mepComponent.addTransactionGET("listarNotas", ListarNotasTx.class);
        mepComponent.addTransactionPOST("reporteBoletaNotas", ReporteBoletaNotasTx.class);
        mepComponent.addTransactionGET("listarDetalleNotas", ListarDetalleNotasTx.class);
        mepComponent.addTransactionGET("listarNotasAnuales", ListarNotasAnualesTx.class);
        mepComponent.addTransactionGET("listarAsistenciaAreas", ListarAsistenciaAreasTx.class);

        return mepComponent;
    }
    
}
