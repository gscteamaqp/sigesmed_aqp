/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.organizacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

/**
 *
 * @author Administrador
 */
public class EliminarOrganizacionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int organizacionID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            organizacionID = requestData.getInt("organizacionID");
        
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo eliminar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        OrganizacionDao moduloDao = (OrganizacionDao)FactoryDao.buildDao("OrganizacionDao");
        try{
            moduloDao.delete(new Organizacion(organizacionID));
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar la Organizacion\n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar la Organizacion", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("La Organizacion se elimino correctamente");
        //Fin
    }
}
