/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scp;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.SalidasDAO;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.Salidas;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author harold
 */
public class SalidasDAOHibernate extends GenericDaoHibernate<Salidas> implements SalidasDAO {

    @Override
    public List<Salidas> listarBienesSalidas(int orgId) {

        List<Salidas> salidas = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT sal FROM Salidas sal JOIN FETCH sal.causal_salida JOIN FETCH sal.salDet detalle JOIN FETCH detalle.bm bienes JOIN FETCH bienes.dtm JOIN FETCH bienes.ambiente JOIN FETCH bienes.anexo JOIN FETCH sal.orgDes WHERE sal.estReg!='E' AND sal.org_id_ori=:p1";
            //String hql = "SELECT sal FROM Salidas sal  WHERE sal.estReg!='E' AND sal.org_id_ori=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", orgId);
            salidas = query.list();

        } catch (Exception e) {
            
            System.out.println("No se pudo Mostrar la lista de Bienes de Salida \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Mostrar la lista de Bienes de Salida \\n " + e.getMessage());
        
        } finally {
            
            session.close();
        
        }
        
        return salidas;
    }

    @Override
    public int contarSalidas(int orgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
