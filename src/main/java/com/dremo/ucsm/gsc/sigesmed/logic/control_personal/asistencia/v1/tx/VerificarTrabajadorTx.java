/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.asistencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.AsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.LibroAsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.RegistroAsistencia;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.DateUtil;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author carlos
 */
public class VerificarTrabajadorTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        String dni = "";
        Integer organizacion;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            dni = requestData.getString("dni");      
            organizacion=requestData.getInt("organizacionID");
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("El dni ingresado es incorrecto" );
        }
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        JSONObject oRes = new JSONObject();
        Trabajador trabajador_ = null;
        try {
            
            trabajador_ = ((LibroAsistenciaDao) FactoryDao.buildDao("cpe.LibroAsistenciaDao")).buscarTrabajadorPorDNI(dni, organizacion);
            
            if (trabajador_ == null) {
                Persona persona = null;
                persona = ((PersonaDao) FactoryDao.buildDao("PersonaDao")).buscarPorDNI(dni);
                if (persona != null) {
                    
                    JSONObject oPersona = new JSONObject();

                    oPersona.put("perId", persona.getPerId());
                    oPersona.put("dni", "" + persona.getDni());
                    oPersona.put("materno", persona.getApeMat());
                    oPersona.put("paterno", persona.getApePat());
                    oPersona.put("nombre", persona.getNom());
                    oPersona.put("existe", true);
                    oRes.put("persona", oPersona);

                    return WebResponse.crearWebResponseExito("El DNI no corresponde a un Trabajador de la Organizacion", oRes);
                } else {
                    return WebResponse.crearWebResponseError("el dni no esta registrado en el sistema");
                }
            } else {
                AsistenciaDao asistenciaDao = (AsistenciaDao) FactoryDao.buildDao("cpe.AsistenciaDao");
                List<RegistroAsistencia> registroHoy=null;
                registroHoy=asistenciaDao.listarRegistroAsistenciaByFecha(trabajador_, DateUtil.removeTime(new Date()));
                SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm a");//dd/MM/yyyy
                JSONObject oPersona = new JSONObject();

                oPersona.put("perId", trabajador_.getPersona().getPerId());
                oPersona.put("dni", "" + trabajador_.getPersona().getDni());
                oPersona.put("materno", trabajador_.getPersona().getApeMat());
                oPersona.put("paterno", trabajador_.getPersona().getApePat());
                oPersona.put("nombre", trabajador_.getPersona().getNom());               
                oPersona.put("existe", true);
                oRes.put("persona", oPersona);
           
                //oCargo.put("nombre",trabajador_.getTraCar().getCrgTraNom());
                
                JSONObject oTrabajador = new JSONObject();
                oTrabajador.put("cargo",trabajador_.getTraCar().getCrgTraIde());             
                oTrabajador.put("id", trabajador_.getTraId());
                oTrabajador.put("perId", trabajador_.getPersona().getPerId());                                             
                oTrabajador.put("existe", true);
                
                JSONArray miArray = new JSONArray();
                for(RegistroAsistencia reg:registroHoy)
                {
                     JSONObject oRegistro = new JSONObject();
                     oRegistro.put("id", reg.getRegAsiId());
                     if(reg.getHoraIngreso()==null)
                     {
                         oRegistro.put("ingreso", "");
                     }
                     else
                     {
                        oRegistro.put("ingreso", sdfDate.format(reg.getHoraIngreso()));
                     }
                     
                     if( reg.getHoraSalida()==null)
                     {
                         oRegistro.put("salida", "");
                     }
                     else
                     {
                         oRegistro.put("salida", sdfDate.format(reg.getHoraSalida()));
                     }
                     
                     oRegistro.put("estado", reg.getEstAsi());
                     miArray.put(oRegistro);
                }
                oRes.put("registros", miArray);
                
                oRes.put("trabajador", oTrabajador);
            }

        } catch (Exception e) {
            System.out.println("\n" + e);
            return WebResponse.crearWebResponseError("No se pudo buscar por dni", e.getMessage());
        }
        
        return WebResponse.crearWebResponseExito("Trabajador registrado", oRes);
        //validar el usuario
    
    }
}
