/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.cuadro_horas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.DistribucionHoraGradoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlazaMagisterial;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
/**
 *
 * @author abel
 */
public class InsertarPlazaMagisterialTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        
        PlazaMagisterial plaza = null;
        try{
            
            JSONObject req = (JSONObject)wr.getData();
            
            String modalidad = req.getString("modalidad");
            String nivel = req.getString("nivel");
            String caracteristica = req.optString("caracteristica");            
            char tipo = req.getString("tipo").charAt(0);
            String nexus = req.getString("nexus");
            String cargo = req.getString("cargo");
            int jornadaLaboral = req.getInt("jornadaLaboral");
            int jornadaPedagogica = req.getInt("jornadaPedagogica");
            String especialidad = req.optString("especialidad");
            String motivo = req.optString("motivo");
            char condicionID = req.getString("condicionID").charAt(0);
            char naturalezaID = req.getString("naturalezaID").charAt(0);
            int docenteID = req.optInt("docenteID");
            int organizacionID = req.getInt("organizacionID");
            
            plaza = new PlazaMagisterial(0, modalidad, nivel, caracteristica, tipo, nexus,cargo,jornadaLaboral,jornadaPedagogica,especialidad, motivo,condicionID,naturalezaID, organizacionID, docenteID==0?null:docenteID);
            
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo insertar la plaza magisterial, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        try{
            DistribucionHoraGradoDao disDao = (DistribucionHoraGradoDao)FactoryDao.buildDao("mech.DistribucionHoraGradoDao");
            
            disDao.insertarPlazaMagisterial(plaza);
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo insertar la plaza magisterial", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("plazaID",plaza.getPlaMagId());
        return WebResponse.crearWebResponseExito("Se inserto correctamente la plaza magisterial", oResponse);
        //Fin
    }
    
}
