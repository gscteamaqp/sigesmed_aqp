package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi;

import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.JornadaEscolarMMI;
import org.hibernate.Query;
import org.hibernate.Session;

public class JornadaDaoHibernate extends GenericMMIDaoHibernate<JornadaEscolarMMI> {

    public JornadaEscolarMMI find4JorEscId(int jorEscId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        JornadaEscolarMMI jornada = null;
        String hql;
        Query query;
        try {
            hql = "FROM JornadaEscolarMMI per WHERE per.estReg != 'E' and per.jorEscId= :hqlJorEscId";
            query = session.createQuery(hql);
            query.setInteger("hqlJorEscId", jorEscId);
            query.setMaxResults(1);
            jornada = (JornadaEscolarMMI) query.uniqueResult();
        } catch (Exception ex) {
            throw ex;
        } finally {
            session.close();
        }
        return jornada;
    }

}
