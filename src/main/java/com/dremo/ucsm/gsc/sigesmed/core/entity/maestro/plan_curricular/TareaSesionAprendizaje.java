package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular;

import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.InventarioInicialDetalle;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrador on 15/12/2016.
 */
@Entity
@Table(name = "tarea_sesion_aprendizaje",schema = "pedagogico")
public class TareaSesionAprendizaje implements java.io.Serializable {
    @SequenceGenerator(name = "tarea_sesion_aprendizaje_tar_id_ses_seq",sequenceName = "pedagogico.tarea_sesion_aprendizaje_tar_id_ses_seq")
    @GeneratedValue(generator = "tarea_sesion_aprendizaje_tar_id_ses_seq")
    @Id
    @Column(name = "tar_id_ses", nullable = false, unique = true)
    private int tarIdSes;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ses_apr_id")
    private SesionAprendizaje sesion;
    @Column(name = "nom", length = 256)
    private String nom;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_ent")
    private Date fecEnt;
    @Column(name = "des", length = 400)
    private String des;
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name="sec")
    private String sec;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length=29)
    private Date fecMod;

    @Column(name = "est_reg",length = 1)
    private Character estReg;
    
    @Column(name="tipo")
    private String tipo;

    @OneToMany(mappedBy="tar_ses_apr" , cascade= CascadeType.ALL)
    private List<TareaIndicador> tarea_indicador;
    
    

    
    public TareaSesionAprendizaje() {
    }

    public void setSec(String sec) {
        this.sec = sec;
    }

    public String getSec() {
        return sec;
    }
    

    public void setTarea_indicador(List<TareaIndicador> tarea_indicador) {
        this.tarea_indicador = tarea_indicador;
    }

    public List<TareaIndicador> getTarea_indicador() {
        return tarea_indicador;
    }

    public TareaSesionAprendizaje(String nom, Date fecEnt, String des) {
        this.nom = nom;
        this.fecEnt = fecEnt;
        this.des = des;
    }

    public TareaSesionAprendizaje(String nom) {
        this.nom = nom;
    }

    public int getTarIdSes() {
        return tarIdSes;
    }

    public void setTarIdSes(int tarIdSes) {
        this.tarIdSes = tarIdSes;
    }

    public SesionAprendizaje getSesion() {
        return sesion;
    }

    public void setSesion(SesionAprendizaje sesion) {
        this.sesion = sesion;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getFecEnt() {
        return fecEnt;
    }

    public void setFecEnt(Date fecEnt) {
        this.fecEnt = fecEnt;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }

    
}
