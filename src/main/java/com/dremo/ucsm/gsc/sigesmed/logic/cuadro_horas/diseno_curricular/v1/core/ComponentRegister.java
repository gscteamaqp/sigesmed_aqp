/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseno_curricular.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseno_curricular.v1.tx.ActualizarDisenoCurricularTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseno_curricular.v1.tx.EliminarDisenoCurricularTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseno_curricular.v1.tx.EliminarGradoAreaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseno_curricular.v1.tx.InsertarAreaCurricularTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseno_curricular.v1.tx.InsertarAreaHoraTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseno_curricular.v1.tx.InsertarCicloTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseno_curricular.v1.tx.InsertarDisenoCurricularTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseno_curricular.v1.tx.InsertarGradoAreaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseno_curricular.v1.tx.InsertarGradoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseno_curricular.v1.tx.InsertarJornadaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseno_curricular.v1.tx.InsertarNivelTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseno_curricular.v1.tx.ListarDisenoCurricularTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseno_curricular.v1.tx.ListarJornadaEscolarTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseno_curricular.v1.tx.BuscarVigentePorOrganizacionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseno_curricular.v1.tx.EliminarAreaCurricularTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseno_curricular.v1.tx.EliminarAreaHoraTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseno_curricular.v1.tx.EliminarCicloTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseno_curricular.v1.tx.EliminarGradoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseno_curricular.v1.tx.EliminarNivelTx;

/**
 *
 * @author abel
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_ELABORACION_CUADRO_HORAS);        
        
        //Registrando el Nombre del componente
        component.setName("diseñoCurricular");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionPOST("insertarDiseñoCurricular", InsertarDisenoCurricularTx.class);
        component.addTransactionGET("listarDiseñoCurricular", ListarDisenoCurricularTx.class);
        component.addTransactionPUT("actualizarDiseñoCurricular", ActualizarDisenoCurricularTx.class);
        component.addTransactionDELETE("eliminarDiseñoCurricular", EliminarDisenoCurricularTx.class);
        
        
        component.addTransactionPOST("insertarCiclo", InsertarCicloTx.class);
        component.addTransactionPOST("insertarNivel", InsertarNivelTx.class);
        component.addTransactionPOST("insertarJornada", InsertarJornadaTx.class);
        component.addTransactionPOST("insertarArea", InsertarAreaCurricularTx.class);
        component.addTransactionPOST("insertarGrado", InsertarGradoTx.class);
        component.addTransactionPOST("insertarGradoArea", InsertarGradoAreaTx.class);
        component.addTransactionPOST("insertarAreaHora", InsertarAreaHoraTx.class);
        component.addTransactionDELETE("eliminarAreaHora", EliminarAreaHoraTx.class);
        component.addTransactionDELETE("eliminarArea", EliminarAreaCurricularTx.class);
        component.addTransactionDELETE("eliminarGrado", EliminarGradoTx.class);
        component.addTransactionDELETE("eliminarCiclo", EliminarCicloTx.class);
        component.addTransactionDELETE("eliminarNivel", EliminarNivelTx.class);
        
        component.addTransactionDELETE("eliminarGradoArea", EliminarGradoAreaTx.class);
        component.addTransactionGET("listarJornadaEscolar", ListarJornadaEscolarTx.class);
        
        component.addTransactionGET("buscarVigentePorOrganizacion", BuscarVigentePorOrganizacionTx.class);
        
        return component;
    }
}
