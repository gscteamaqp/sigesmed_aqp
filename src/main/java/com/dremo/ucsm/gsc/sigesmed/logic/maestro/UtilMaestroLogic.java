package com.dremo.ucsm.gsc.sigesmed.logic.maestro;

import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.*;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;

/**
 * Created by Administrador on 01/12/2016.
 */
public class UtilMaestroLogic {
    public static Map<CompetenciaAprendizaje,List<CapacidadAprendizaje>> compenciasSesionToMap(List<IndicadoresSesionAprendizaje> indicadoresSes){
        Map<CompetenciaAprendizaje,List<CapacidadAprendizaje>> mapCompetencias = new HashMap<>();
        for(IndicadoresSesionAprendizaje indSes: indicadoresSes){
            CompetenciaAprendizaje comp = indSes.getCompetencia();
            CapacidadAprendizaje cap = indSes.getCapacidad();
            IndicadorAprendizaje ind = indSes.getIndicador();
            if(!mapCompetencias.containsKey(comp)){
                List<IndicadorAprendizaje> inds = new ArrayList<>();
                inds.add(ind);
                cap.setIndicadores(inds);
                List<CapacidadAprendizaje> listCapacidades = new ArrayList<>();
                listCapacidades.add(cap);
                mapCompetencias.put(comp,listCapacidades);
            }else{
                //revisamos las capacidades que ya estan
                if(!cap.getIndicadores().contains(ind))cap.getIndicadores().add(ind);
                if(!mapCompetencias.get(comp).contains(cap)) mapCompetencias.get(comp).add(cap);
            }
        }
        return mapCompetencias;
    }
    public static Map<CompetenciaAprendizaje,List<CapacidadAprendizaje>> compenciasUnidadToMap(List<CompetenciasUnidadDidactica> competenciasUnidad){
        Map<CompetenciaAprendizaje,List<CapacidadAprendizaje>> mapCompetencias = new HashMap<>();
        for(CompetenciasUnidadDidactica competenciaUni : competenciasUnidad){
            CompetenciaCapacidad compCap = competenciaUni.getCompetenciaCapacidad();
            List<IndicadorAprendizaje> newIndicadores = new ArrayList<>();
            newIndicadores.addAll(competenciaUni.getIndicadores());
            compCap.getCap().setIndicadores(newIndicadores);
            if(!mapCompetencias.containsKey(compCap.getCom())){
                List<CapacidadAprendizaje> listCapacidades = new ArrayList<>();
                listCapacidades.add(compCap.getCap());
                mapCompetencias.put(compCap.getCom(),listCapacidades);
            }else{
                mapCompetencias.get(compCap.getCom()).add(compCap.getCap());
            }
        }
        return mapCompetencias;
    }
    public static JSONArray mapCompetenciasToJSON(Map<CompetenciaAprendizaje,List<CapacidadAprendizaje>> mapCompetencias) throws Exception{
        JSONArray arrResult = new JSONArray();
        for (Map.Entry<CompetenciaAprendizaje,List<CapacidadAprendizaje>> entry : mapCompetencias.entrySet()){
            JSONObject compResult = new JSONObject(EntityUtil.objectToJSONString(
                    new String[]{"comId", "nom", "des"},
                    new String[]{"id", "nom", "des"},
                    entry.getKey()
            ));
            List<CapacidadAprendizaje> capacidadesList =  entry.getValue();
            JSONArray capacidadesJson = new JSONArray();
            int numInd = 0;
            for(CapacidadAprendizaje capObj : capacidadesList){
                JSONObject capacidadJSON = new JSONObject(EntityUtil.objectToJSONString(
                        new String[]{"capId","nom","des"},
                        new String[]{"id","nom","des"},
                        capObj
                ));
                List<IndicadorAprendizaje> indicadoresList = capObj.getIndicadores();
                numInd += indicadoresList.size();
                capacidadJSON.put("indicadores",new JSONArray(EntityUtil.listToJSONString(
                        new String[]{"indAprId","nomInd","selectedIndicador"},
                        new String[]{"id","nom","sele"},
                        indicadoresList
                )));
                capacidadesJson.put(capacidadJSON);
            }
            compResult.put("numInd",numInd);
            compResult.put("capacidades",capacidadesJson);
                /*compResult.put("capacidades",new JSONArray(EntityUtil.listToJSONString(
                        new String[]{"capId","nom","des"},
                        new String[]{"id","nom","des"},
                        entry.getValue()
                )));*/
            arrResult.put(compResult);
        }
        return arrResult;
    }
    public static JSONArray mapCompetenciasSinIndicadoresToJSON(Map<CompetenciaAprendizaje,List<CapacidadAprendizaje>> mapCompetencias) throws Exception{
        JSONArray arrResult = new JSONArray();
        for (Map.Entry<CompetenciaAprendizaje,List<CapacidadAprendizaje>> entry : mapCompetencias.entrySet()){
            JSONObject compResult = new JSONObject(EntityUtil.objectToJSONString(
                    new String[]{"comId", "nom", "des"},
                    new String[]{"id", "nom", "des"},
                    entry.getKey()
            ));
            List<CapacidadAprendizaje> capacidadesList =  entry.getValue();
            JSONArray capacidadesJson = new JSONArray();
            for(CapacidadAprendizaje capObj : capacidadesList){
                JSONObject capacidadJSON = new JSONObject(EntityUtil.objectToJSONString(
                        new String[]{"capId","nom","des"},
                        new String[]{"id","nom","des"},
                        capObj
                ));
                capacidadesJson.put(capacidadJSON);
            }
            compResult.put("capacidades",capacidadesJson);
            arrResult.put(compResult);
        }
        return arrResult;
    }
}
