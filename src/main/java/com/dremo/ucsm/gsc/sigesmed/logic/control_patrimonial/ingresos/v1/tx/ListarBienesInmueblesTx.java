/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import java.util.List;

import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BienesInmuebles;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.BienInmuebleDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BienesMuebles;
import java.text.SimpleDateFormat;

/**
 *
 * @author Administrador
 */
public class ListarBienesInmueblesTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {

        JSONArray miArray = new JSONArray();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        
        try {
            
            List<BienesInmuebles> bie_inmue = null;
            JSONObject requestData = (JSONObject) wr.getData();
            int org_id = requestData.getInt("org_id");
            BienInmuebleDAO bie_inmue_dao = (BienInmuebleDAO) FactoryDao.buildDao("scp.BienInmuebleDAO");
            bie_inmue = bie_inmue_dao.listar_bienes_inmuebles(org_id);

            for (BienesInmuebles bin : bie_inmue) {
                JSONObject oResponse = new JSONObject();
                oResponse.put("cod_bie_inm", bin.getBie_inm_id());
                oResponse.put("desc", bin.getDescripcion());
                oResponse.put("fec_reg", formatter.format(bin.getFec_reg()));
                oResponse.put("ubi", bin.getAmb().getDes());
                miArray.put(oResponse);
            }

        } catch (Exception e) {
            System.out.println("No se pudo mostrar los bienes inmuebles\n" + e);
            return WebResponse.crearWebResponseError("No se pudo Listar los Bienes Inmuebles", e.getMessage());
        }

        return WebResponse.crearWebResponseExito("Se listo los bienes inmubeles correctamente", miArray);

    }

}
