/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.TareaEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaTarea;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Alertas;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.AlertManager;
import java.util.Date;
/**
 *
 * @author abel
 */
public class CalificarTareaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */        
        
        BandejaTarea tarea = new BandejaTarea();
        try{
            JSONObject requestData = (JSONObject)wr.getData();                     
            
            tarea.setBanTarId( requestData.getInt("bandejaTareaID") );
            tarea.setNota( requestData.getInt("nota") );
            tarea.setNot_lit(requestData.getString("nota_lit"));
            
            tarea.setEstado(Tarea.ESTADO_CALIFICADO);//estado de la bandeja a enviado = C
            
        }catch(Exception e){
            System.out.println("No se pudo calificar la tarea, datos incorrectos\n"+e);
            return WebResponse.crearWebResponseError("No se pudo calificar la tarea, datos incorrectos", e.getMessage() );
        }
        //Fin       
        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        TareaEscolarDao tareaDao = (TareaEscolarDao)FactoryDao.buildDao("web.TareaEscolarDao");
        try{
            tarea.setFecVis(new Date());//actualizando la fecha en la que se califico la tarea
            tareaDao.calificarTareaResuelta(tarea);
            AlertManager.send(Alertas.WEB_COMPARTIR_NOTA_TAREA, 2);
        }catch(Exception e){
            System.out.println("No se pudo calificar la tarea\n"+e);
            return WebResponse.crearWebResponseError("No se pudo calificar la tarea", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("estado",""+tarea.getEstado());
        oResponse.put("nota",tarea.getNota());
        oResponse.put("not_lit",tarea.getNot_lit());
        return WebResponse.crearWebResponseExito("Se envio la tarea se califico correctamente",oResponse);
        //Fin
    }    
    
    
}
