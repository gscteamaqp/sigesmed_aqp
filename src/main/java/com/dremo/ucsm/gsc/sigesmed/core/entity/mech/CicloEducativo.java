package com.dremo.ucsm.gsc.sigesmed.core.entity.mech;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="ciclo_educativo")
public class CicloEducativo  implements java.io.Serializable {

    @Id
    @Column(name="cic_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_ciclo_educativo", sequenceName="ciclo_educativo_cic_id_seq" )
    @GeneratedValue(generator="secuencia_ciclo_educativo")
    private int cicEduId;
    @Column(name="abr",length=4)
    private String abr;
    @Column(name="nom",length=64)
    private String nom;
    @Column(name="des", length=256)
    private String des;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_mod", nullable=false, length=29)
    private Date fecMod;    
    @Column(name="usu_mod")
    private int usuMod;
    @Column(name="est_reg")
    private char estReg;
    
    @Column(name="dis_cur_id")
    private int disCurId;    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="dis_cur_id",updatable = false,insertable = false)
    private DisenoCurricularMECH disenoCurricular;

    public CicloEducativo() {
    }
    public CicloEducativo(int cicEduId) {
        this.cicEduId = cicEduId;
    }
    public CicloEducativo(int cicEduId,String abr, String nom, String des,int disCurId, Date fecMod, int usuMod, char estReg) {
       this.cicEduId = cicEduId;
       this.abr = abr;
       this.nom = nom;
       this.des = des;
       this.disCurId = disCurId;
       this.fecMod = fecMod;
       this.usuMod = usuMod;
       this.estReg = estReg;
    }
   
     
    public int getCicEduId() {
        return this.cicEduId;
    }    
    public void setCicEduId(int cicEduId) {
        this.cicEduId = cicEduId;
    }
    
    public String getAbr() {
        return this.abr;
    }
    public void setAbr(String abr) {
        this.abr = abr;
    }
    public String getNom() {
        return this.nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }    
    public String getDes() {
        return this.des;
    }
    public void setDes(String des) {
        this.des = des;
    }
    
    
    public Date getFecMod() {
        return this.fecMod;
    }
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }
    
    public int getUsuMod() {
        return this.usuMod;
    }
    public void setUsuMod(int usuMod) {
        this.usuMod = usuMod;
    }
    
    public char getEstReg() {
        return this.estReg;
    }
    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }
    
    public int getDisCurId() {
        return this.disCurId;
    }    
    public void setDisCurId(int disCurId) {
        this.disCurId = disCurId;
    }
    
    public DisenoCurricularMECH getDisenoCurricular() {
        return this.disenoCurricular;
    }
    public void setDisenoCurricular(DisenoCurricularMECH disenoCurricular) {
        this.disenoCurricular = disenoCurricular;
    }
    
    
}


