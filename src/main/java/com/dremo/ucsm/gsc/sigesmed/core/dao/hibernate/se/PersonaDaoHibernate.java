/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.se.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Persona;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author abel
 */
public class PersonaDaoHibernate extends GenericDaoHibernate<Persona> implements PersonaDao {

    @Override
    public Persona buscarPersonaPorId(Integer perId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Persona p = (Persona)session.get(Persona.class, perId);
        session.close();
        return p;
    }
}
