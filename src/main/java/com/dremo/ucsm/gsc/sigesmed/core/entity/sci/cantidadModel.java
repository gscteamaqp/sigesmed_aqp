/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sci;

import java.math.BigDecimal;

/**
 *
 * @author G-16
 */
public class cantidadModel {
    public int ID;
    public String nombre;
    public BigDecimal num1;
    public int anio;
    public int mes;
    public long num2;
    
    public cantidadModel(int ID, String nombre, BigDecimal num1){
        this.ID = ID;
        this.nombre = nombre;
        this.num1 = num1;
    }
    public cantidadModel(int ID, String nombre, long num2){
        this.ID = ID;
        this.nombre = nombre;
        this.num2 = num2;
    }
    public cantidadModel(int ID, BigDecimal num1){
        this.ID = ID;
        this.num1 = num1;
    }
    public cantidadModel(int anio,int mes, BigDecimal num1){
        this.anio = anio;
        this.mes = mes;
        this.num1 = num1;
    }
}
