/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author gscadmin
 */
@Entity
@Table(name = "publicacion", schema="administrativo")
public class Publicacion implements Serializable {
    
    @Id
    @Column(name = "pub_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_publicacion", sequenceName="administrativo.publicacion_pub_id_seq" )
    @GeneratedValue(generator="secuencia_publicacion")
    private Integer pubId;
    
    @Column(name = "nom_edi")
    private String nomEdi;
    
    @Column(name = "tip_pub")
    private String tipPub;
    
    @Column(name = "tit_pub")
    private String titPub;
    
    @Column(name = "gra_par")
    private String graPar;
    
    @Column(name = "lug")
    private String lug;
    
    @Column(name = "fec_pub")
    @Temporal(TemporalType.DATE)
    private Date fecPub;
    
    @Column(name = "num_reg")
    private String numReg;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "fic_esc_id")
    private FichaEscalafonaria fichaEscalafonaria;

    public Publicacion() {
    }
    
    
    public Publicacion(Integer pubId) {
        this.pubId = pubId;
    }
    
    public Publicacion(FichaEscalafonaria ficEsc, String nomEdi, String tipPub, String titPub, String graPar, String lug, Date fecPub, String numReg, Integer usuMod, Date fecMod, Character estReg) {
        this.fichaEscalafonaria = ficEsc;
        this.nomEdi = nomEdi;
        this.tipPub = tipPub;
        this.tipPub = tipPub;
        this.graPar = graPar;
        this.lug = lug;
        this.fecPub = fecPub;
        this.numReg = numReg;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.nomEdi = nomEdi;
        this.estReg = estReg;
    }

    public Integer getPubId() {
        return pubId;
    }

    public void setPubId(Integer pubId) {
        this.pubId = pubId;
    }

    public String getNomEdi() {
        return nomEdi;
    }

    public void setNomEdi(String nomEdi) {
        this.nomEdi = nomEdi;
    }

    public String getTipPub() {
        return tipPub;
    }

    public void setTipPub(String tipPub) {
        this.tipPub = tipPub;
    }

    public String getTitPub() {
        return titPub;
    }

    public void setTitPub(String titPub) {
        this.titPub = titPub;
    }

    public String getGraPar() {
        return graPar;
    }

    public void setGraPar(String graPar) {
        this.graPar = graPar;
    }

    public String getLug() {
        return lug;
    }

    public void setLug(String lug) {
        this.lug = lug;
    }

    public Date getFecPub() {
        return fecPub;
    }

    public void setFecPub(Date fecPub) {
        this.fecPub = fecPub;
    }

    public String getNumReg() {
        return numReg;
    }

    public void setNumReg(String numReg) {
        this.numReg = numReg;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public FichaEscalafonaria getFichaEscalafonaria() {
        return fichaEscalafonaria;
    }

    public void setFichaEscalafonaria(FichaEscalafonaria fichaEscalafonaria) {
        this.fichaEscalafonaria = fichaEscalafonaria;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pubId != null ? pubId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Publicacion)) {
            return false;
        }
        Publicacion other = (Publicacion) object;
        if ((this.pubId == null && other.pubId != null) || (this.pubId != null && !this.pubId.equals(other.pubId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsc.sigesmed.core.entity.se.Publicaciones[ pubId=" + pubId + " ]";
    }
    
}
