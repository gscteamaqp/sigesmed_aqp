package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.traslado_salida.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.GradoSeccionPlanNivelDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.TrasladoIngresoDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.TrasladoIngreso;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.DateUtil;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class ListarTrasladosIngresosTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        TrasladoIngresoDaoHibernate hb = new TrasladoIngresoDaoHibernate();
        GradoSeccionPlanNivelDaoHibernate orgHb = new GradoSeccionPlanNivelDaoHibernate();

        JSONObject data = (JSONObject) wr.getData();
        Integer orgOriId;
        List<TrasladoIngreso> misTrasladosIngreso;
        String nombre;
        JSONArray response = new JSONArray();
        try {
            orgOriId = data.getInt("orgOriId");
            misTrasladosIngreso = hb.getTrasladosByOrganizacionOrigen(orgOriId);

            for (TrasladoIngreso tra : misTrasladosIngreso) {
                JSONObject temp = new JSONObject();
                temp.put("traIngId", tra.getTraIngId());
                temp.put("orgDesId", tra.getOrgDesId());
                temp.put("orgDesNom", orgHb.getOrganizacionNombre(tra.getOrgDesId()));
                nombre = tra.getEstudiante().getPersona().getNom() + " "
                        + tra.getEstudiante().getPersona().getApePat() + " "
                        + tra.getEstudiante().getPersona().getApeMat();
                temp.put("estId", tra.getEstudiante().getPerId());
                temp.put("estDNI", tra.getEstudiante().getPersona().getDni());
                temp.put("estNom", nombre);
                temp.put("fec", DateUtil.getString2Date(tra.getTraIngFec()));
                temp.put("traTip", tra.getTraIngTip());
                temp.put("traTipNom", getTipoTrasladoNom(tra.getTraIngTip()));
                temp.put("traEst", tra.getTraIngEst());
                temp.put("traEstNom", getEstadoTrasladoNom(tra.getTraIngEst()));
                temp.put("resolucion", tra.getTraIngRes());
                response.put(temp);
            }

        } catch (Exception e) {
            return WebResponse.crearWebResponseError("Error al cargar los traslados de Salida! ", e.getMessage());
        }

        return WebResponse.crearWebResponseExito("Se listaron los traslados de salida", response);
    }

    private String getTipoTrasladoNom(int tipo) {
        switch (tipo) {
            case 1:
                return "Por Cambio de Nivel";
            case 2:
                return "Por Cambio de Año";
            case 3:
                return "En el Mismo año";
            default:
                return "No espesifica";
        }
    }

    private String getEstadoTrasladoNom(char estado) {
        switch (estado) {
            case 'P':
                return "En Proceso";
            case 'A':
                return "Aseptado";
            case 'R':
                return "Rechazado";
            default:
                return "No espesifica";
        }
    }

}
