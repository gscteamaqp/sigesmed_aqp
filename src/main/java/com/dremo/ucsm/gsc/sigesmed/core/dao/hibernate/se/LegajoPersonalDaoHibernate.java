/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.LegajoPersonalDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.LegajoPersonal;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author gscadmin
 */
public class LegajoPersonalDaoHibernate extends GenericDaoHibernate<LegajoPersonal> implements LegajoPersonalDao{

    @Override
    public LegajoPersonal buscarPorId(Integer legPerId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        LegajoPersonal legPer = (LegajoPersonal)session.get(LegajoPersonal.class, legPerId);
        session.close();
        return legPer;
    }

    @Override
    public List<LegajoPersonal> listarxFichaEscalafonaria(int ficEscId) {
        List<LegajoPersonal> legajos = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT leg from LegajoPersonal as leg "
                    + "join fetch leg.fichaEscalafonaria as fe "
                    + "WHERE fe.ficEscId = " + ficEscId + " AND leg.estReg ='A'";
            Query query = session.createQuery(hql);
            legajos = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los legajos \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los legajos \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return legajos;
    }
    
}
