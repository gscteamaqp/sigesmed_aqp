/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.LibroCajaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.cantidadModel;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author G-16
 */
public class ListarEgresosIngresosPorIntTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {

        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        
        Date desde = null;
        Date hasta = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
           
            
            if(!requestData.getString("desde").contentEquals(""))
                desde = new SimpleDateFormat("dd/M/yyyy").parse( requestData.getString("desde"));
            if(!requestData.getString("hasta").contentEquals(""))
                hasta = new SimpleDateFormat("dd/M/yyyy HH:mm:ss").parse( requestData.getString("hasta") + " 23:59:59" );
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo realizar la estadistica", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        List<cantidadModel> compras = null;
        List<cantidadModel> ventas = null;
        LibroCajaDao hisDao = (LibroCajaDao)FactoryDao.buildDao("sci.LibroCajaDao");
        try{
            compras = hisDao.EgresosTotalDeInstitucionesPorFecha( desde, hasta);
            ventas = hisDao.IngresosTotalDeInstitucionesPorFecha( desde, hasta);
        }catch(Exception e){
            System.out.println("E"+e);
            return WebResponse.crearWebResponseError("No se pudo realizar la estadistica Consulta", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
         
        JSONArray aOrgan1 = new JSONArray();
        //JSONArray aOrgan2 = new JSONArray();
        
        JSONArray nRegistros1 = new JSONArray();        
        JSONArray nRegistros2 = new JSONArray();
      
        
        for(cantidadModel a:compras){
            
            aOrgan1.put(a.nombre);
            nRegistros1.put(a.num1);
 
        }
         for(cantidadModel a:ventas){
          
            nRegistros2.put(a.num1);
 
        }
         
         
        for(int j=0;j<aOrgan1.length();j++){
            System.out.println(aOrgan1.get(j));    
            System.out.println(nRegistros1.get(j));
            System.out.println(nRegistros2.get(j)); 
        }
        
        
        
        JSONObject res = new JSONObject();
        res.put("labels", aOrgan1);
        res.put("registros1", nRegistros1);        
        res.put("registros2", nRegistros2);
     
        return WebResponse.crearWebResponseExito("Se Listo correctamente",res);        
        //Fin     
        
    }
    
        
    
}
