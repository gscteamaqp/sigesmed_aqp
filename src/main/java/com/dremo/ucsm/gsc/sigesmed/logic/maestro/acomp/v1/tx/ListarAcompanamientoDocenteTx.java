package com.dremo.ucsm.gsc.sigesmed.logic.maestro.acomp.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.acomp.AcompanamientoDocenteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.acomp.AcompanamientoDocente;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.swing.text.html.parser.Entity;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 06/01/2017.
 */
public class ListarAcompanamientoDocenteTx implements ITransaction {
    private static Logger logger = Logger.getLogger(ListarAcompanamientoDocenteTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data  = (JSONObject) wr.getData();
        int idDoc = data.getInt("doc");
        return listarAcompanamientosDocente(idDoc);
    }

    private WebResponse listarAcompanamientosDocente(int idDoc) {
        try{
            AcompanamientoDocenteDao acompDao = (AcompanamientoDocenteDao)FactoryDao.buildDao("maestro.acomp.AcompanamientoDocenteDao");
            List<AcompanamientoDocente> acomps = acompDao.buscarAcompanamientosDocente(idDoc);
            JSONArray acompsJSON = new JSONArray();
            for(AcompanamientoDocente acomp : acomps){
                JSONObject acompJSON = new JSONObject(EntityUtil.objectToJSONString(
                        new String[]{"acoId","fec","res"},
                        new String[]{"id","fec","res"},
                        acomp
                ));
                acompJSON.put("nivel",new JSONObject(EntityUtil.objectToJSONString(new String[]{"nivId","nom"},new String[]{"id","nom"},acomp.getNivel())));
                acompJSON.put("grado",new JSONObject(EntityUtil.objectToJSONString(new String[]{"graId","nom"},new String[]{"id","nom"},acomp.getGrado())));
                acompJSON.put("seccion",String.valueOf(acomp.getSeccion().getSedId()));
                acompJSON.put("area",new JSONObject(EntityUtil.objectToJSONString(new String[]{"areCurId","nom","abr"},new String[]{"id","nom","abr"},acomp.getArea())));
                acompJSON.put("comp",new JSONArray(EntityUtil.listToJSONString(new String[]{"comAcoId","des","est"},new String[]{"id","des","est"},acomp.getCompromisos())));
                acompsJSON.put(acompJSON);
            }
            return WebResponse.crearWebResponseExito("Se listo correctamente los acompanamietos",acompsJSON);
        }catch (Exception e){
            logger.log(Level.SEVERE,"listarAcompanamientos",e);
            return WebResponse.crearWebResponseError("No se pudo listar");
        }
    }
}
