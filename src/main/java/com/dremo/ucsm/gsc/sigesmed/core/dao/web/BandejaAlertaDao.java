/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.web;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaAlerta;
import java.util.List;

/**
 *
 * @author abel
 */
public interface BandejaAlertaDao extends GenericDao<BandejaAlerta>{
    
    public void insertarVarios(List<BandejaAlerta> varios);
    public List<BandejaAlerta> listarNoVistosPorUsuario(int usuarioID);
    public List<BandejaAlerta> listarTodosPorUsuario(int usuarioID);
    public void marcarVistoAlerta(int bandejaAlertaID);
}
