/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.plan_estudios.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.PlanEstudiosDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Turno;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
/**
 *
 * @author abel
 */
public class PersistenciaTurnoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        Turno nuevo = null;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            char codigo = requestData.getString("turnoID").charAt(0);
            String nombre = requestData.getString("nombre");
            char estado = requestData.getString("estado").charAt(0);
            
            nuevo = new Turno(codigo,nombre,new Date(),wr.getIdUsuario(),estado);
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("datos incorrectos", e.getMessage() );
        }
        try{
            PlanEstudiosDao planDao = (PlanEstudiosDao)FactoryDao.buildDao("mech.PlanEstudiosDao");
            planDao.mergeTurno(nuevo);
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo realizar la accion sobre turno", e.getMessage() );
        }
        //Fin
        
        JSONObject res = new JSONObject();
        res.put("turnoID", ""+nuevo.getTurId());
        
        return WebResponse.crearWebResponseExito("La accion sobre el turno se realizo correctamente",res);
    }    
    
    
}

