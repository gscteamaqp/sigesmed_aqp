/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.salidas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.SalidasDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.Salidas;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author harold
 */
public class ListarSalidasTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        JSONArray responseData = new JSONArray();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        try {
            List<Salidas> salidas = null;
            JSONObject requestData = (JSONObject) wr.getData();
            int org_id = requestData.getInt("org_id");

            SalidasDAO sal_dao = (SalidasDAO) FactoryDao.buildDao("scp.SalidasDAO");
            salidas = sal_dao.listarBienesSalidas(org_id);

            for (Salidas sal : salidas) {
                JSONObject oResponse = new JSONObject();
                oResponse.put("salidas_id", sal.getSalidas_id());
                oResponse.put("cod_pat_bie", sal.getSalDet().get(0).getBm().getCon_pat_id());
                oResponse.put("des_bie", sal.getSalDet().get(0).getBm().getDes_bie());
                oResponse.put("cau_sal", sal.getCausal_salida().getDes());
                oResponse.put("org_des", sal.getOrgDes().getNom());
                oResponse.put("estado", sal.getSalDet().get(0).getBm().getEstado_bie()); 
                oResponse.put("fec_sal", formatter.format(sal.getFec_reg()));
                
                if ("B".equals(sal.getSalDet().get(0).getBm().getEstado_bie())) {
                    oResponse.put("cond", 0);
                    oResponse.put("cond_nombre", "BUENO");
                }
                if ("M".equals(sal.getSalDet().get(0).getBm().getEstado_bie())) {
                    oResponse.put("cond", 1);
                    oResponse.put("cond_nombre", "MALO");
                }
                if ("R".equals(sal.getSalDet().get(0).getBm().getEstado_bie())) {
                    oResponse.put("cond", 2);
                    oResponse.put("cond_nombre", "REGULAR");
                }
                
                responseData.put(oResponse);
            }
        } catch (Exception e) {
            System.out.println("No se pudo listar los bienes de salidas\n" + e);
            return WebResponse.crearWebResponseError("No se pudo listar los bienes de salidas", e.getMessage());
        }
        return WebResponse.crearWebResponseExito("Se listo las salidas de bienes correctamente", responseData);
        
    }
    
}
