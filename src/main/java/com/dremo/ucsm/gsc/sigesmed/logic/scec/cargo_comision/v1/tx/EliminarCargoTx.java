package com.dremo.ucsm.gsc.sigesmed.logic.scec.cargo_comision.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.CargoComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.CargoDeComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.IntegranteComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.CargoComision;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.CargoDeComision;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by geank on 29/09/16.
 */
public class EliminarCargoTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(EliminarCargoTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        String idStr = wr.getMetadataValue("cod");

        return eliminarCargo(Integer.parseInt(idStr));
    }
    public WebResponse eliminarCargo(int idCargo){
        try {
            CargoComisionDao cargoComisionDao = (CargoComisionDao) FactoryDao.buildDao("scec.CargoComisionDao");
            CargoDeComisionDao cdd = (CargoDeComisionDao) FactoryDao.buildDao("scec.CargoDeComisionDao");
            IntegranteComisionDao icd = (IntegranteComisionDao) FactoryDao.buildDao("scec.IntegranteComisionDao");
            CargoComision cargoComision = cargoComisionDao.buscarPorId(idCargo);
            if(cargoComision.getNomCar().toLowerCase().equals("participante")||cargoComision.getNomCar().toLowerCase().equals("presidente")){
                return WebResponse.crearWebResponseError("No se pude eliminar el cargo por defecto",WebResponse.BAD_RESPONSE);
            }
            else
            {
                CargoComision par = cargoComisionDao.buscarCargoPorNombre("participante");
                icd.cambiarCargoComision(par.getCarComId(), idCargo);
                cdd.eliminarCargoComision(idCargo);
                /*List<CargoDeComision> lista = cdd.buscarPorCargo(idCargo);
                for(CargoDeComision obj:lista){
                    obj.setCargoComision(cargoComisionDao.buscarCargoPorNombre("participante"));
                    cdd.update(obj);
                }*/
                cargoComisionDao.deleteAbsolute(cargoComision);
                return WebResponse.crearWebResponseExito("Eliminacion realizada con exito",WebResponse.OK_RESPONSE);
            }
        }catch (Exception e){
            logger.log(Level.SEVERE,"eliminar Cargo",e);
            return WebResponse.crearWebResponseError("No se pudo realizar la transaccion",WebResponse.BAD_RESPONSE);
        }
    }
}
