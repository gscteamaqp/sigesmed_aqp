/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author gscadmin
 */
@Entity
@Table(name = "ficha_evaluacion", schema = "pedagogico")

public class FichaEvaluacion implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "fev_id")
    private Integer fevId;
    
    @Column(name = "fev_cod")
    private String fevCod;
    
    @Column(name = "fev_gra")
    private Short fevGra;
    
    @Column(name = "fev_are")
    private String fevAre;
    
    @Column(name = "fev_fec")
    @Temporal(TemporalType.DATE)
    private Date fevFec;
    
    @Column(name = "fev_pun")
    private Integer fevPun;
    
    @Column(name = "fev_eta")
    private Integer fevEta;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.DATE)
    private Date fecMod;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "est_reg")
    private String estReg;
    
    @JoinColumn(name = "plf_id", referencedColumnName = "plf_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private PlantillaFicha plantillaFicha;
    
    @JoinColumn(name = "ie_id", referencedColumnName = "org_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Organizacion institucionEducativa;
    
    @JoinColumn(name = "esp_id", referencedColumnName = "tra_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Trabajador especialista;
    
    @JoinColumn(name = "doc_id", referencedColumnName = "doc_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Docente docente;
    
    @JoinColumn(name = "mnd_id", referencedColumnName = "mnd_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private MonitoreoDetalle monitoreoDetalle;
    
    @OneToMany(mappedBy = "fevId", fetch = FetchType.LAZY)
    private List<CompromisosFicha> compromisosFichaList;
    
    public FichaEvaluacion() {
    }

    public FichaEvaluacion(Integer fevId) {
        this.fevId = fevId;
    }

    public Integer getFevId() {
        return fevId;
    }

    public void setFevId(Integer fevId) {
        this.fevId = fevId;
    }

    public String getFevCod() {
        return fevCod;
    }

    public void setFevCod(String fevCod) {
        this.fevCod = fevCod;
    }

    public Short getFevGra() {
        return fevGra;
    }

    public void setFevGra(Short fevGra) {
        this.fevGra = fevGra;
    }

    public String getFevAre() {
        return fevAre;
    }

    public void setFevAre(String fevAre) {
        this.fevAre = fevAre;
    }

    public Date getFevFec() {
        return fevFec;
    }

    public void setFevFec(Date fevFec) {
        this.fevFec = fevFec;
    }

    public Integer getFevPun() {
        return fevPun;
    }

    public void setFevPun(Integer fevPun) {
        this.fevPun = fevPun;
    }

    public Integer getFevEta() {
        return fevEta;
    }

    public void setFevEta(Integer fevEta) {
        this.fevEta = fevEta;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public Organizacion getInstitucionEducativa() {
        return institucionEducativa;
    }
    
    public void setInstitucionEducativa(Organizacion institucionEducativa) {
        this.institucionEducativa = institucionEducativa;
    }

    public Trabajador getEspecialista() {
        return especialista;
    }
    
    public void setEspecialista(Trabajador especialista) {
        this.especialista = especialista;
    }

    public Docente getDocente() {
        return docente;
    }

    public void setDocente(Docente docente) {
        this.docente = docente;
    }
    
    public MonitoreoDetalle getMonitoreoDetalle() {
        return monitoreoDetalle;
    }

    public void setMonitoreoDetalle(MonitoreoDetalle monitoreoDetalle) {
        this.monitoreoDetalle = monitoreoDetalle;
    }

    public PlantillaFicha getPlantillaFicha() {
        return plantillaFicha;
    }

    public void setPlantillaFicha(PlantillaFicha plantillaFicha) {
        this.plantillaFicha = plantillaFicha;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fevId != null ? fevId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FichaEvaluacion)) {
            return false;
        }
        FichaEvaluacion other = (FichaEvaluacion) object;
        if ((this.fevId == null && other.fevId != null) || (this.fevId != null && !this.fevId.equals(other.fevId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades_sma.FichaEvaluacion[ fevId=" + fevId + " ]";
    }
    
}
