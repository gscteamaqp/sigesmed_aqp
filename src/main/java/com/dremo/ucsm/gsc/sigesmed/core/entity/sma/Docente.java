/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author gscadmin
 */
@Entity
@Table(name = "docente", schema = "pedagogico")

public class Docente implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "doc_id")
    private Integer docId;
    @Basic(optional = false)
    @Column(name = "cod_mod")
    private String codMod;
    @Column(name = "aut_ess")
    private String autEss;
    @Column(name = "reg_lab")
    private String regLab;
    @Column(name = "reg_pen")
    private String regPen;
    @Column(name = "num_col")
    private String numCol;
    @Column(name = "esp")
    private String esp;
    @Column(name = "niv")
    private Integer niv;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "docente", fetch = FetchType.LAZY)
    private List<DocenteCarpetaPedagogica> docenteCarpetaPedagogicaList;
    @OneToMany(mappedBy = "docente", fetch = FetchType.LAZY)
    private List<FichaEvaluacion> fichaEvaluacionList;

    public Docente() {
    }

    public Docente(Integer docId) {
        this.docId = docId;
    }

    public Docente(Integer docId, String codMod) {
        this.docId = docId;
        this.codMod = codMod;
    }

    public Integer getDocId() {
        return docId;
    }

    public void setDocId(Integer docId) {
        this.docId = docId;
    }

    public String getCodMod() {
        return codMod;
    }

    public void setCodMod(String codMod) {
        this.codMod = codMod;
    }

    public String getAutEss() {
        return autEss;
    }

    public void setAutEss(String autEss) {
        this.autEss = autEss;
    }

    public String getRegLab() {
        return regLab;
    }

    public void setRegLab(String regLab) {
        this.regLab = regLab;
    }

    public String getRegPen() {
        return regPen;
    }

    public void setRegPen(String regPen) {
        this.regPen = regPen;
    }

    public String getNumCol() {
        return numCol;
    }

    public void setNumCol(String numCol) {
        this.numCol = numCol;
    }

    public String getEsp() {
        return esp;
    }

    public void setEsp(String esp) {
        this.esp = esp;
    }

    public Integer getNiv() {
        return niv;
    }

    public void setNiv(Integer niv) {
        this.niv = niv;
    }

    @XmlTransient
    public List<DocenteCarpetaPedagogica> getDocenteCarpetaPedagogicaList() {
        return docenteCarpetaPedagogicaList;
    }

    public void setDocenteCarpetaPedagogicaList(List<DocenteCarpetaPedagogica> docenteCarpetaPedagogicaList) {
        this.docenteCarpetaPedagogicaList = docenteCarpetaPedagogicaList;
    }

    @XmlTransient
    public List<FichaEvaluacion> getFichaEvaluacionList() {
        return fichaEvaluacionList;
    }

    public void setFichaEvaluacionList(List<FichaEvaluacion> fichaEvaluacionList) {
        this.fichaEvaluacionList = fichaEvaluacionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (docId != null ? docId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Docente)) {
            return false;
        }
        Docente other = (Docente) object;
        if ((this.docId == null && other.docId != null) || (this.docId != null && !this.docId.equals(other.docId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades_sma.Docente[ docId=" + docId + " ]";
    }
    
}
