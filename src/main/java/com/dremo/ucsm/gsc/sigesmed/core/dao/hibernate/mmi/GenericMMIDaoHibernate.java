package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import java.util.List;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class GenericMMIDaoHibernate<T> implements GenericDao<T> {

    @Override
    public void insert(T dato) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.save(dato);
            transaction.commit();
        } catch (Exception ex) {
            transaction.rollback();
            System.out.println("No se puede hacer insert " + dato.getClass().getName() + ".\n" + ex.getMessage());
            throw ex;
        } finally {
            session.close();
        }
    }

    public void saveOrUpdate(T dato) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.saveOrUpdate(dato);
            transaction.commit();
        } catch (Exception ex) {
            transaction.rollback();
            System.out.println("No se puede hacer saveOrUdpate " + dato.getClass().getName() + ".\n" + ex.getMessage());
            throw ex;
        } finally {
            session.close();
        }
    }

    @Override
    public void update(T dato) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.update(dato);
            transaction.commit();
        } catch (Exception ex) {
            transaction.rollback();
            System.out.println("No se puede hacer update " + dato.getClass().getName() + ".\n" + ex.getMessage());
            throw ex;
        } finally {
            session.close();
        }
    }

    @Override
    public void delete(T dato) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        String hql;
        Query query;
        try {
            hql = "UPDATE " + dato.getClass().getName() + " ele SET ele.estReg='E' WHERE ele = :p1";
            query = session.createQuery(hql);
            query.setParameter("p1", dato);
            query.executeUpdate();
            transaction.commit();
        } catch (Exception ex) {
            transaction.rollback();
            System.out.println("No se puede hacer delete " + dato.getClass().getName() + ".\n" + ex.getMessage());
            throw ex;
        } finally {
            session.close();
        }
    }

    @Override
    public void deleteAbsolute(T dato) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.delete(dato);
            transaction.commit();
        } catch (Exception ex) {
            transaction.rollback();
            System.out.println("No se puede hacer delete absoluto " + dato.getClass().getName() + ".\n" + ex.getMessage());
            throw ex;
        } finally {
            session.close();
        }
    }

    /**
     * Busca todos los registros no eliminados
     *
     * @param dato
     * @return
     */
    @Override
    public List<T> buscarTodos(Class dato) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<T> estados = null;
        String hql;
        Query query;
        try {
            hql = "FROM " + dato.getName() + " WHERE estReg != 'E'";
            query = session.createQuery(hql);
            estados = query.list();
        } catch (Exception ex) {
            System.out.println("No se puede listar " + dato.getClass().getName() + ".\n" + ex.getMessage());
            throw ex;
        } finally {
            session.close();
        }
        return estados;
    }

    /**
     * Busca todos los registros no eliminados ylos ordena de manera descendente
     *
     * @param dato
     * @param campos
     * @return
     */
    @Override
    public List<T> buscarTodosOrdenados(Class dato, String campos) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<T> estados = null;
        String hql;
        Query query;
        try {
            hql = "FROM " + dato.getName() + " WHERE estReg != 'E' ORDER BY " + campos + " desc";
            query = session.createQuery(hql);
            estados = query.list();
        } catch (Exception ex) {
            System.out.println("No se puede listar " + dato.getClass().getName() + ".\n" + ex.getMessage());
            throw ex;
        } finally {
            session.close();
        }
        return estados;
    }

    /**
     * Listado Generico de todas las Personas
     *
     * @param dato
     * @return
     */
    @Override
    public List<T> listar(Class dato) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<T> estados = null;
        String hql;
        Query query;
        try {
            hql = "FROM " + dato.getName();
            query = session.createQuery(hql);
            estados = query.list();
        } catch (Exception ex) {
            System.out.println("No se puede listar " + dato.getClass().getName() + ".\n" + ex.getMessage());
            throw ex;
        } finally {
            session.close();
        }
        return estados;
    }

    /**
     * Solo para tablas con una unica clave numerica
     *
     * @param c
     * @param key
     * @return
     */
    @Override
    public T load(Class c, int key) {
        long lKey = key;
        Session session = HibernateUtil.getSessionFactory().openSession();
        T estado;
        Query query;
        try {
            estado = (T) session.load(c, lKey);
        } catch (ObjectNotFoundException ex) {
            System.out.println("No se pudo cargar " + c.getName() + ".\n" + ex.getMessage());
            throw ex;
        } finally {
            session.close();
        }
        return estado;
    }

    public T loadLong(Class c, long key) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        T estado;
        try {
            estado = (T) session.load(c, key);
        } catch (ObjectNotFoundException ex) {
            System.out.println("No se pudo cargar " + c.getName() + ".\n" + ex.getMessage());
            throw ex;
        } finally {
            session.close();
        }
        return estado;
    }

    public T loadInt(Class c, int key) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        T estado;
        try {
            estado = (T) session.load(c, key);
        } catch (ObjectNotFoundException ex) {
            throw ex;
        } finally {
            session.close();
        }
        return estado;
    }

    @Override
    public void merge(T dato) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Query query;
        try {
            session.merge(dato);
            transaction.commit();
        } catch (Exception ex) {
            transaction.rollback();
            System.out.println("No se puede hacer merge " + dato.getClass().getName() + ".\n" + ex.getMessage());
            throw ex;
        } finally {
            session.close();
        }
    }

    @Override
    public Object llave(Class dato, String campo) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Object result = null;
        String hql;
        Query query;
        try {
            hql = "SELECT MAX(" + campo + ") FROM " + dato.getName();
//            hql = "SELECT COALESCE(MAX(" + campo + "), 0) FROM " + dato.getName();
            query = session.createQuery(hql);
            query.setMaxResults(1);
            result = query.uniqueResult();
        } catch (Exception ex) {
            System.out.println("No se puede ejecutar llave " + dato.getClass().getName() + ".\n" + ex.getMessage());
            throw ex;
        } finally {
            session.close();
        }
        return result;
    }

}
