package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi;

import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.PersonaMMI;
import org.hibernate.Query;
import org.hibernate.Session;

public class Organizacion4NominaDaoHibernate extends GenericMMIDaoHibernate<Organizacion> {

    public Organizacion find4OrgId(int orgId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Organizacion org = null;
        String hql;
        Query query;
        try {
            hql = "FROM Organizacion per WHERE per.estReg != 'E' and per.orgId= :hdlorgId";
            query = session.createQuery(hql);
            query.setInteger("hdlorgId", orgId);
            query.setMaxResults(1);
            org = (Organizacion) query.uniqueResult();
        } catch (Exception ex) {
            throw ex;
        } finally {
            session.close();
        }
        return org;
    }

}
