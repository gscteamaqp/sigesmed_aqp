/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.exposicion_ponencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ExposicionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Exposicion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class EliminarExposicionTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(EliminarExposicionTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        Integer expId = 0;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            expId = requestData.getInt("expId");          
        
        }catch(Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"eliminarExposicion",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar", e.getMessage() );
        }
        
        ExposicionDao estPosDao = (ExposicionDao)FactoryDao.buildDao("se.ExposicionDao");
        try{
            estPosDao.deleteAbsolute(new Exposicion(expId));
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar la exposicion\n" + e);
            return WebResponse.crearWebResponseError("No se pudo eliminar la exposicion", e.getMessage() );
        }
        return WebResponse.crearWebResponseExito("La exposicion se elimino correctamente");
    }
    
}
