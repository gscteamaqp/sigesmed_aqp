/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.std;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.*;
import com.dremo.ucsm.gsc.sigesmed.core.dao.std.HistorialExpedienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.EntidadCantidadModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.DocumentoExpediente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.HistorialExpediente;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.EstadoExpediente;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author abel
 */
public class HistorialExpedienteDaoHibernate extends GenericDaoHibernate<HistorialExpediente> implements HistorialExpedienteDao {
    
    @Override
    public HistorialExpediente buscarUltimoHistorial(int expedienteID) {
        HistorialExpediente historial = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar ultimo codigo de tramite
            String hql = "SELECT h FROM HistorialExpediente h JOIN FETCH h.area JOIN FETCH h.responsable WHERE h.expediente.expId =:p1 ORDER BY h.hisExpId DESC ";
            Query query = session.createQuery(hql);
            query.setParameter("p1", expedienteID);
            query.setMaxResults(1);
            historial = ((HistorialExpediente)query.uniqueResult());            
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar el ultimo historial del expediente \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar el ultimo historial del expediente \\n "+ e.getMessage());
        }
        finally{
            session.close();
        }
        return historial;
    }
    
    @Override
    public HistorialExpediente buscarHistorialAnterior(int expedienteID,int historialID)  {
        HistorialExpediente historial = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar ultimo codigo de tramite
            String hql = "SELECT h FROM HistorialExpediente h JOIN FETCH h.area JOIN FETCH h.responsable WHERE h.expediente.expId =:p1 and h.hisExpId =:p2";
            Query query = session.createQuery(hql);
            query.setParameter("p1", expedienteID);
            //id del historial anterior
            query.setParameter("p2", historialID - 1);
            query.setMaxResults(1);
            historial = ((HistorialExpediente)query.uniqueResult());            
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar el historial anterior \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar el historial anterior \\n "+ e.getMessage());
        }
        finally{
            session.close();
        }
        return historial;
    }
    
    @Override
    public List<HistorialExpediente> buscarPorExpediente(int expedienteID) {
        List<HistorialExpediente> historiales = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar ultimo codigo de tramite
            String hql = "SELECT h FROM HistorialExpediente h JOIN FETCH h.area JOIN FETCH h.estado JOIN FETCH h.responsable WHERE h.expediente.expId =:p1 ORDER BY h.hisExpId";
            Query query = session.createQuery(hql);
            query.setParameter("p1", expedienteID);
            historiales = query.list();            
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar historial por expediente \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar historial por expediente \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return historiales;
    }
    @Override
    public List<HistorialExpediente> buscarPorExpedienteCodigo(int expedienteID) {
        List<HistorialExpediente> historiales = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar ultimo codigo de tramite
            String hql = "SELECT h FROM HistorialExpediente h JOIN FETCH h.area JOIN FETCH h.estado JOIN FETCH h.responsable JOIN FETCH h.expediente WHERE h.expediente.expId =:p1 ORDER BY h.hisExpId";
            Query query = session.createQuery(hql);
            query.setParameter("p1", expedienteID);
            historiales = query.list();            
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar historial por expediente \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar historial por expediente \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return historiales;
    }
    /*
    @Override
    public List<HistorialExpediente> buscarNuevosPorArea(int areaID) {
        List<HistorialExpediente> historiales = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT h FROM HistorialExpediente h JOIN FETCH h.area JOIN FETCH h.estado WHERE h.areaId =:p1 and h.expediente.fecFin IS NULL and h.fecRec IS NULL ORDER BY h.hisExpId";
            Query query = session.createQuery(hql);
            query.setParameter("p1", areaID);
            historiales = query.list();            
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar historial nuevos por area \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar historial nuevos por area \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return historiales;
    }*/
    @Override
    public List<HistorialExpediente> buscarPorAreaYEstado(int areaID, int estadoID) {
        List<HistorialExpediente> historiales = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT h FROM HistorialExpediente h JOIN FETCH h.estado JOIN FETCH h.expediente e JOIN FETCH e.prioridad WHERE h.areaId =:p1 and h.expediente.fecFin IS NULL and h.estadoId=:p2 ORDER BY h.hisExpId";
            Query query = session.createQuery(hql);
            query.setParameter("p1", areaID);
            query.setParameter("p2", estadoID);
            historiales = query.list();            
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar historial nuevos por area y estado "+ estadoID+"\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar historial nuevos por area y estado "+ estadoID+"\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return historiales;
    }
    @Override
    public List<HistorialExpediente> buscarPorDerivadosPorArea(int areaID) {
        List<HistorialExpediente> historiales = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT h FROM HistorialExpediente h JOIN FETCH h.area JOIN FETCH h.expediente e JOIN FETCH e.prioridad ,HistorialExpediente h2 WHERE h2.areaId =:p1 and h2.estadoId=:p2 and h.expediente=h2.expediente and h.hisExpId=h2.hisExpId+1 and h.estadoId=:p3 ORDER BY h.hisExpId";
            Query query = session.createQuery(hql);
            query.setParameter("p1", areaID);
            query.setParameter("p2", EstadoExpediente.DERIVADO);
            query.setParameter("p3", EstadoExpediente.NUEVO);
            historiales = query.list();            
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar historial nuevos por area y estado "+ EstadoExpediente.DERIVADO+"\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar historial nuevos por area y estado "+ EstadoExpediente.DERIVADO+"\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return historiales;
    }
    @Override
    public List<HistorialExpediente> buscarPorAreaYEstado(int areaID, int[] estadosID) {
        List<HistorialExpediente> historiales = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT h FROM HistorialExpediente h JOIN FETCH h.estado JOIN FETCH h.area JOIN FETCH h.expediente e JOIN FETCH e.prioridad"+
                    " WHERE h.areaId =:p1 and h.expediente.fecFin IS NULL and (";
            for(int i=0; i<estadosID.length;i++){
                if( i>0 )
                    hql+=" or ";
                hql+=" h.estadoId="+ estadosID[i];
            }
            hql += ") ORDER BY h.hisExpId";
            
            Query query = session.createQuery(hql);
            query.setParameter("p1", areaID);
            historiales = query.list();      
           //System.out.println("DETECCION SQL"+historiales.get(0).toString());
           
             
        }catch(Exception e){
            System.out.println("No se pudo encontrar historial nuevos por area y estado "+ estadosID+"\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar historial nuevos por area y estado "+ estadosID+"\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return historiales;
    }
    @Override
    public List<HistorialExpediente> buscarPorAreaYEstadoEnFinalizados(int areaID, int estadoID) {
        List<HistorialExpediente> historiales = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT h FROM HistorialExpediente h JOIN FETCH h.estado JOIN FETCH h.expediente e JOIN FETCH e.prioridad WHERE h.areaId =:p1 and h.expediente.fecEnt IS NULL and h.estadoId=:p2 ORDER BY h.hisExpId";
            Query query = session.createQuery(hql);
            query.setParameter("p1", areaID);
            query.setParameter("p2", estadoID);
            historiales = query.list();            
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar historial nuevos por area y estado "+ estadoID+"\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar historial nuevos por area y estado "+ estadoID+"\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return historiales;
    }
    @Override
    public List<HistorialExpediente> buscarPorAreaYEstadoEnFinalizados(int areaID, int[] estadosID) {
        List<HistorialExpediente> historiales = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT h FROM HistorialExpediente h JOIN FETCH h.estado JOIN FETCH h.expediente e JOIN FETCH e.prioridad"+
                    " WHERE h.areaId =:p1 and h.expediente.fecEnt IS NULL and (";
            for(int i=0; i<estadosID.length;i++){
                if( i>0 )
                    hql+=" or ";
                hql+=" h.estadoId="+ estadosID[i];
            }
            hql += ") ORDER BY h.hisExpId";
            
            Query query = session.createQuery(hql);
            query.setParameter("p1", areaID);
            historiales = query.list();            
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar historial nuevos por area y estado "+ estadosID+"\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar historial nuevos por area y estado "+ estadosID+"\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return historiales;
    }
    
    @Override
    public void actualizarEstado(HistorialExpediente h,int estado){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            String hql = "";
            if(estado <= EstadoExpediente.RECHAZADO){
                hql = "UPDATE HistorialExpediente h SET h.estadoId=:p3,h.fecRec=:p4,h.observacion=:p5 WHERE h.hisExpId =:p1 and h.expediente =:p2";
                             
                Query query = session.createQuery(hql);
                query.setParameter("p1", h.getHisExpId());
                query.setParameter("p2", h.getExpediente());
                query.setParameter("p3", h.getEstadoId());
                query.setParameter("p4", h.getFecRec());
                query.setParameter("p5", h.getObservacion());
                query.executeUpdate();
            }
            else{
                hql = "UPDATE HistorialExpediente h SET h.estadoId=:p3,h.fecAte=:p4,h.observacion=:p5 WHERE h.hisExpId =:p1 and h.expediente =:p2";
                
                Query query = session.createQuery(hql);
                query.setParameter("p1", h.getHisExpId());
                query.setParameter("p2", h.getExpediente());
                query.setParameter("p3", h.getEstadoId());
                query.setParameter("p4", h.getFecAte());
                query.setParameter("p5", h.getObservacion());
                query.executeUpdate();
            } 
            
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo actualizar estado de historial" + this.getClass().getName() + "\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    
    @Override
    public void actualizarEstadoVarios(List<HistorialExpediente> historiales,int estado){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            String hql = "";
            if(estado <= EstadoExpediente.RECHAZADO){
                hql = "UPDATE HistorialExpediente h SET h.estadoId=:p3,h.fecRec=:p4,h.observacion=:p5 WHERE h.hisExpId =:p1 and h.expediente =:p2";
                for(HistorialExpediente h: historiales){                
                    Query query = session.createQuery(hql);
                    query.setParameter("p1", h.getHisExpId());
                    query.setParameter("p2", h.getExpediente());
                    query.setParameter("p3", h.getEstadoId());
                    query.setParameter("p4", h.getFecRec());
                    query.setParameter("p5", h.getObservacion());
                    query.executeUpdate();
                }
            }
            else{
                hql = "UPDATE HistorialExpediente h SET h.estadoId=:p3,h.fecAte=:p4,h.observacion=:p5 WHERE h.hisExpId =:p1 and h.expediente =:p2";
                for(HistorialExpediente h: historiales){
                    Query query = session.createQuery(hql);
                    query.setParameter("p1", h.getHisExpId());
                    query.setParameter("p2", h.getExpediente());
                    query.setParameter("p3", h.getEstadoId());
                    query.setParameter("p4", h.getFecAte());
                    query.setParameter("p5", h.getObservacion());
                    query.executeUpdate();
                }
            } 
            
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo actualizar varios estado de historial" + this.getClass().getName() + "\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    @Override
    public void insertarEstado(HistorialExpediente h){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            String hql = "insert into administrativo.historial_expediente ( his_exp_id, exp_id, are_id, res_id, est_exp_id,fec_env ) VALUES ";
                
            hql+= "('"+ h.getHisExpId()+"','" + h.getExpediente().getExpId()+"','" + h.getAreaId()+"','" + h.getResId()+"','" + h.getEstadoId()+"','" + h.getFecEnv()+"' )";                
            
            Query query = session.createSQLQuery(hql);
            query.executeUpdate();
            
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo insertar estado de historial" + this.getClass().getName() + "\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    @Override
    public void insertarEstadoVarios(List<HistorialExpediente> historiales){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            String hql = "insert into administrativo.historial_expediente ( his_exp_id, exp_id, are_id, res_id, est_exp_id,fec_env ) VALUES ";
            for(int i=0; i<historiales.size();i++){
                HistorialExpediente h = historiales.get(i);
                if( i>0 )
                    hql+=" , ";
                hql+= "('"+ h.getHisExpId()+"','" + h.getExpediente().getExpId()+"','" + h.getAreaId()+"','" + h.getResId()+"','" + h.getEstadoId()+"','" + h.getFecEnv()+"' )";                
            }
            
            Query query = session.createSQLQuery(hql);
            query.executeUpdate();
            
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo insertar varios estado de historial" + this.getClass().getName() + "\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    
    
    @Override
    public int numeroDocumentos(int expedienteID) {
        int numero = 0;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar ultimo codigo de tramite
            String hql = "SELECT COUNT(d.docExpId) FROM DocumentoExpediente d WHERE d.expediente.expId =:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", expedienteID);
            query.setMaxResults(1);
            numero = ((Long)query.uniqueResult()).intValue();
        
        }catch(Exception e){
            System.out.println("No se pudo obtener el numero de documentos\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo obtener el numero de documentos\\n "+ e.getMessage());
        }
        finally{
            session.close();
        }
        return numero;
    }
    
    @Override
    public void insertarDocumento(DocumentoExpediente h){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            String hql = "insert into administrativo.documento_expediente ( doc_exp_id, exp_id, des, arc_adj, tip_doc_id, his_exp_id ) VALUES ";
            
            hql+= "('"+ h.getDocExpId()+"','" + h.getExpediente().getExpId()+"','" + h.getDes()+"','" + h.getArcAdj()+"','" + h.getTipoDocumento().getTipDocId()+"','" + h.getHistorialId()+"' )";                            
            Query query = session.createSQLQuery(hql);
            query.executeUpdate();
            
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo insertar el documento al historial" + this.getClass().getName() + "\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    
    @Override
    public void insertarDocumentos(List<DocumentoExpediente> documentos){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            if(documentos != null){
                String hql = "insert into administrativo.documento_expediente ( doc_exp_id, exp_id, des, arc_adj, tip_doc_id, his_exp_id ) VALUES ";
                for(int i=0; i<documentos.size();i++){
                    DocumentoExpediente h = documentos.get(i);
                    if( i>0 )
                        hql+=" , ";
                    hql+= "('"+ h.getDocExpId()+"','" + h.getExpediente().getExpId()+"','" + h.getDes()+"','" + h.getArcAdj()+"','" + h.getTipoDocumento().getTipDocId()+"','" + h.getHistorialId()+"' )";                
                }

                Query query = session.createSQLQuery(hql);
                query.executeUpdate();

                miTx.commit();
            }
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo insertar varios documentos al historial" + this.getClass().getName() + "\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    
    @Override
    public List<EntidadCantidadModel> cantidadExpedientesEnAreaPorOrganizacionYFecha(int organizacionID,Date desde, Date hasta){
        List<EntidadCantidadModel> areas = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "";
            Query query = null;
            if(desde != null){
                if(hasta != null){
                    hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.std.EntidadCantidadModel(a.areId,a.nom,COUNT(h.hisExpId)) FROM HistorialExpediente h right join h.area as a"+
                        " WHERE a.organizacion.orgId=:p1 and h.fecEnv >:p2 and h.fecEnv <:p3 GROUP BY a ORDER BY a.areId";
                    query = session.createQuery(hql);
                    query.setTimestamp("p3", hasta);
                }
                else{
                    hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.std.EntidadCantidadModel(a.areId,a.nom,COUNT(h.hisExpId)) FROM HistorialExpediente h right join h.area as a"+
                        " WHERE a.organizacion.orgId=:p1 and h.fecEnv >:p2 GROUP BY a ORDER BY a.areId";
                    query = session.createQuery(hql);
                }            
                
                query.setDate("p2", desde);                
            }
            else{
                if(hasta != null){                    
                    hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.std.EntidadCantidadModel(a.areId,a.nom,COUNT(h.hisExpId)) FROM HistorialExpediente h right join h.area as a"+
                        " WHERE a.organizacion.orgId=:p1 and h.fecEnv <:p3 GROUP BY a ORDER BY a.areId";
                    query = session.createQuery(hql);
                    query.setTimestamp("p3", hasta);
                }
                else
                    return areas;
            }            
            query.setParameter("p1", organizacionID);
            
            areas = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo consultar la cantidad de expedientes en area\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo consultar la cantidad de expedientes en area\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return areas;
    }
    
    
    @Override
    public List<EntidadCantidadModel> cantidadExpedientesEnAreaPorOrganizacionYEstadoYFecha(int organizacionID,int estadoID,Date desde, Date hasta){
        List<EntidadCantidadModel> areas = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "";
            Query query = null;
            if(desde != null){
                if(hasta != null){
                    hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.std.EntidadCantidadModel(a.areId,a.nom,COUNT(h.hisExpId)) FROM HistorialExpediente h right join h.area as a"+
                        " WHERE a.organizacion.orgId=:p1 and h.estadoId=:p2 and h.fecEnv >:p3 and h.fecEnv <:p4 GROUP BY a ORDER BY a.areId";
                    query = session.createQuery(hql);
                    query.setTimestamp("p4", hasta);
                }
                else{
                    hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.std.EntidadCantidadModel(a.areId,a.nom,COUNT(h.hisExpId)) FROM HistorialExpediente h right join h.area as a"+
                        " WHERE a.organizacion.orgId=:p1 and h.estadoId=:p2 and h.fecEnv >:p3 GROUP BY a ORDER BY a.areId";
                    query = session.createQuery(hql);
                }            
                
                query.setDate("p3", desde);
            }
            else{
                if(hasta != null){                    
                    hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.std.EntidadCantidadModel(a.areId,a.nom,COUNT(h.hisExpId)) FROM HistorialExpediente h right join h.area as a"+
                        " WHERE a.organizacion.orgId=:p1 and h.estadoId=:p2 and h.fecEnv <:p4 GROUP BY a ORDER BY a.areId";
                    query = session.createQuery(hql);
                    query.setTimestamp("p4", hasta);
                }
                else
                    return areas;
            }
                       
            query.setParameter("p1", organizacionID);
            query.setParameter("p2", estadoID);
            
        areas = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo consultar la cantidad de expedientes en area por estado\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo consultar la cantidad de expedientes en area por estado\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return areas;
    }
    
    @Override
    public List<EntidadCantidadModel> cantidadExpedientesEnAreaPorOrganizacionYNoEstadoYFecha(int organizacionID,int estadoID,Date desde, Date hasta){
        List<EntidadCantidadModel> areas = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.std.EntidadCantidadModel(a.areId,a.nom,COUNT(h.hisExpId)) FROM HistorialExpediente h right join h.area as a"+
                        " WHERE h.organizacion.orgId=:p1 and h.estadoId!=:p2 GROUP BY a ORDER BY a.areId";
            Query query = session.createQuery(hql);
                       
            query.setParameter("p1", organizacionID);
            query.setParameter("p2", estadoID);
            
            areas = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo consultar la cantidad de expedientes en area\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo consultar la cantidad de expedientes en area\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return areas;
    }
    
    @Override
    public List<EntidadCantidadModel> cantidadExpedientesDeTrabajadorPorAreaYFecha(int areaID,Date desde, Date hasta){
        List<EntidadCantidadModel> areas = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "";
            Query query = null;
            if(desde != null){
                if(hasta != null){
                    hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.std.EntidadCantidadModel(r.perId,r.nom,COUNT(h.hisExpId),COUNT( CASE WHEN h.estadoId=:p4 THEN 1 ELSE null END),COUNT( CASE WHEN h.estadoId=:p5 THEN 1 ELSE null END),COUNT( CASE WHEN h.estadoId=:p6 THEN 1 ELSE null END)) FROM HistorialExpediente h right join h.responsable as r"+
                        " WHERE h.areaId=:p1 and h.fecEnv >:p2 and h.fecEnv <:p3 GROUP BY r ORDER BY r.perId";
                    query = session.createQuery(hql);
                    query.setTimestamp("p3", hasta);
                }
                else{
                    hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.std.EntidadCantidadModel(r.perId,r.nom,COUNT(h.hisExpId),COUNT( CASE WHEN h.estadoId=:p4 THEN 1 ELSE null END),COUNT( CASE WHEN h.estadoId=:p5 THEN 1 ELSE null END),COUNT( CASE WHEN h.estadoId=:p6 THEN 1 ELSE null END)) FROM HistorialExpediente h right join h.responsable as r"+
                        " WHERE h.areaId=:p1 and h.fecEnv >:p2 GROUP BY r ORDER BY r.perId";
                    query = session.createQuery(hql);
                }            
                
                query.setDate("p2", desde);
            }
            else{
                if(hasta != null){                    
                    hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.std.EntidadCantidadModel(r.perId,r.nom,COUNT(h.hisExpId),COUNT( CASE WHEN h.estadoId=:p4 THEN 1 ELSE null END),COUNT( CASE WHEN h.estadoId=:p5 THEN 1 ELSE null END),COUNT( CASE WHEN h.estadoId=:p6 THEN 1 ELSE null END)) FROM HistorialExpediente h right join h.responsable as r"+
                        " WHERE h.areaId=:p1 and h.fecEnv <:p3 GROUP BY r ORDER BY r.perId";
                    query = session.createQuery(hql);
                    query.setTimestamp("p3", hasta);
                }
                else
                    return areas;
            }
                       
            query.setParameter("p1", areaID);
            query.setParameter("p4", EstadoExpediente.DERIVADO);
            query.setParameter("p5", EstadoExpediente.DEVUELTO);
            query.setParameter("p6", EstadoExpediente.FINALIZADO);
            
            areas = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo consultar la cantidad de expedientes del trabajador\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo consultar la cantidad de expedientes del trabajador\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return areas;
    }
}

