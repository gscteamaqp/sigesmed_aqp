/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.plantilla_grupo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.PlantillaGrupoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaFichaInstitucional;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaGrupo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.TipoGrupo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ActualizarGrupoTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        PlantillaGrupo grupoAct = null;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            int gruId = requestData.getInt("gruId");
            String gruDes = requestData.optString("gruNom");
            int plaId = requestData.getInt("plaId");
            int gruTip = requestData.getInt("Tipo");
            
            grupoAct = new PlantillaGrupo(gruId, gruDes, new TipoGrupo(gruTip), new PlantillaFichaInstitucional(plaId));
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        PlantillaGrupoDao dexDao = (PlantillaGrupoDao)FactoryDao.buildDao("smdg.PlantillaGrupoDao");
        try{
            dexDao.update(grupoAct);
        
        }catch(Exception e){
            System.out.println("No se pudo actualizar el Grupo\n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar el Grupo", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Grupo se actualizo correctamente");
        //Fin
    }
    
}
