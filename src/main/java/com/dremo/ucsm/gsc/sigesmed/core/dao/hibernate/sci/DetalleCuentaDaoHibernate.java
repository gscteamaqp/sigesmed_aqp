/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sci;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.DetalleCuentaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.DetalleCuenta;

/**
 *
 * @author G-16
 */
public class DetalleCuentaDaoHibernate extends GenericDaoHibernate<DetalleCuenta> implements DetalleCuentaDao {
    
}