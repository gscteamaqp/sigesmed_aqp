/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.registro_trabajador.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.LibroAsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import org.json.JSONObject;

/**
 *
 * @author carlos
 */
public class BuscarPersonaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        String dni = "";
        Integer organizacion;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            dni = requestData.getString("dni");      
            organizacion=requestData.getInt("organizacionID");
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("El dni ingresado es incorrecto" );
        }
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        JSONObject oRes = new JSONObject();
        Trabajador trabajador_ = null;
        try {
            DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            trabajador_ = ((LibroAsistenciaDao) FactoryDao.buildDao("cpe.LibroAsistenciaDao")).buscarTrabajadorPorDNI(dni, organizacion);
            if (trabajador_ == null) {
                Persona persona = null;
                persona = ((PersonaDao) FactoryDao.buildDao("PersonaDao")).buscarPorDNI(dni);
                if (persona != null) {
                    
                    JSONObject oPersona = new JSONObject();

                    oPersona.put("perId", persona.getPerId());
                    oPersona.put("dni", "" + persona.getDni());
                    oPersona.put("materno", persona.getApeMat());
                    oPersona.put("paterno", persona.getApePat());
                    oPersona.put("nombre", persona.getNom());
                    oPersona.put("email", persona.getEmail());
                    oPersona.put("numero1", persona.getNum1());
                    oPersona.put("numero2", persona.getNum2());
                    oPersona.put("direccion", persona.getDireccion());
                    oPersona.put("fijo", persona.getFijo());
                    oPersona.put("nacimiento",fmt.format( persona.getFecNac()));
                    oPersona.put("existe", true);

                    oRes.put("persona", oPersona);

                    return WebResponse.crearWebResponseExito("el dni ya se encuentra registrado", oRes);
                } else {
                    return WebResponse.crearWebResponseError("el dni no esta registrado en el sistema");
                }
            } else {
                JSONObject oPersona = new JSONObject();

                oPersona.put("perId", trabajador_.getPersona().getPerId());
                oPersona.put("dni", "" + trabajador_.getPersona().getDni());
                oPersona.put("materno", trabajador_.getPersona().getApeMat());
                oPersona.put("paterno", trabajador_.getPersona().getApePat());
                oPersona.put("nombre", trabajador_.getPersona().getNom());
                oPersona.put("email", trabajador_.getPersona().getEmail());
                oPersona.put("numero1", trabajador_.getPersona().getNum1());
                oPersona.put("numero2", trabajador_.getPersona().getNum2());
                oPersona.put("direccion", trabajador_.getPersona().getDireccion());
                oPersona.put("fijo", trabajador_.getPersona().getFijo());
                oPersona.put("nacimiento", fmt.format(trabajador_.getPersona().getFecNac()));
                oPersona.put("existe", true);
                oRes.put("persona", oPersona);
                
                
                //JSONObject oCargo = new JSONObject();
                
                
                //oCargo.put("nombre",trabajador_.getTraCar().getCrgTraNom());
                
                JSONObject oTrabajador = new JSONObject();
                oTrabajador.put("cargo",trabajador_.getTraCar().getCrgTraIde());
                oTrabajador.put("fechaIngreso", fmt.format(trabajador_.getFecIng()));
                oTrabajador.put("salario", trabajador_.getSal());
                oTrabajador.put("id", trabajador_.getTraId());
                oTrabajador.put("perId", trabajador_.getPersona().getPerId());               
                oTrabajador.put("tipo", trabajador_.getTraCar().getTraTip());
                
                oTrabajador.put("existe", true);
                oRes.put("trabajador", oTrabajador);
            }

        } catch (Exception e) {
            System.out.println("\n" + e);
            return WebResponse.crearWebResponseError("No se pudo buscar por dni", e.getMessage());
        }
        
        return WebResponse.crearWebResponseExito("El trabajador ya se encuentra Registrado", oRes);
        //validar el usuario
    
    }
}
