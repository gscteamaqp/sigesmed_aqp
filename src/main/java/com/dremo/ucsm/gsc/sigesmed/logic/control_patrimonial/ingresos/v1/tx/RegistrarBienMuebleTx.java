/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BienesMuebles;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.DetalleTecnicoMuebles;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.ValorContable;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.MovimientoIngresos;

import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.BienesMueblesDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.DetalleTecnicoMueblesDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.ValorContableDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.MovimientoIngresosDAO;
import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;

import com.onbarcode.barcode.Code128;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import java.text.SimpleDateFormat;
import java.util.Random;

/**
 *
 * @author Administrador
 */
public class RegistrarBienMuebleTx implements ITransaction {
    
    private static final String CHARACTER_SET = "0123456789";
    private static Random rnd = new Random();
    
    @Override
    public WebResponse execute(WebRequest wr) {

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat formato2 = new SimpleDateFormat("yyyy-MM-dd");
        JSONObject requestData = (JSONObject) wr.getData();
        MovimientoIngresos mov_ing = null;
        BienesMuebles bm = null;
        DetalleTecnicoMuebles dtm = null;
        ValorContable vc = null;
        
        String msgResponse = ""; //Mensaje de respuesta del servicio
        
        try {

            /*Datos del Movimiento*/
            JSONObject mov = requestData.getJSONObject("movimiento");
            int org_id = requestData.getInt("org_id");
            int tipo_mov_ing = mov.getInt("tip_mov_ing");
            int con_pat_id = mov.getInt("con_pat_id");
            String num_res = mov.getString("num_res");
            Date fec_res = formatter.parse(mov.getString("fec_res"));
            Date fec_mov = new Date();
            String obs = mov.getString("obs");

            /*Cabecera del Bien Mueble*/
            int cat_bie_id = requestData.getInt("cat_bie_id");
            int amb_id = requestData.getInt("amb_id");
            int an_id = requestData.getInt("an_id");
            String des_bie = requestData.getString("des_bie");
            Date fec_reg = new Date();
            String cod_int = requestData.getString("cod_int");
            int cantidad = requestData.getInt("cant");
            String estado_bie = requestData.getString("cond");
            //   FileJsonObject doc_bie = new FileJsonObject( requestData.getJSONObject("rut_doc_bie") );
            //     String rut_doc_bie = doc_bie.getName();
            String cod_ba_bie = requestData.getString("cod_ba_bie");
            int usu_mod = requestData.getInt("usu_mod");
            String verificar = requestData.getString("verificar");
            Date fec_mod = new Date();
            //int org_id = requestData.getInt("org_id");
            char est_reg = 'A';

            /*Registramos el Detalle Tecnico */
            String marc = requestData.optString("marc");
            String mod = requestData.optString("mod");
            String cond = requestData.optString("cond");
            int dim = requestData.optInt("dim");
            String ser = requestData.optString("ser");
            String col = requestData.optString("col");
            String tip = requestData.optString("tip");
            String nro_mot = requestData.optString("nro_mot");
            String nro_pla = requestData.optString("nro_pla");
            String nro_cha = requestData.optString("nro_cha");
            String raza = requestData.optString("raza");
            int edad = requestData.optInt("edad");

            JSONObject doc_ref_obj = requestData.optJSONObject("rut_doc_bie");
            JSONObject rut_imag_1_obj = requestData.optJSONObject("rut_imag_1");
            JSONObject rut_imag_2_obj = requestData.optJSONObject("rut_imag_2");
            JSONObject rut_aut_img_obj = requestData.optJSONObject("rut_aut_img");

            //  FileJsonObject doc_ref = new FileJsonObject(requestData.optJSONObject("doc_ref"));
            //   FileJsonObject imag_1 = new FileJsonObject( requestData.optJSONObject("rut_imag_1") );
            //   FileJsonObject imag_2 = new FileJsonObject( requestData.optJSONObject("rut_imag_2") );
            //   FileJsonObject aut_imag = new FileJsonObject( requestData.optJSONObject("rut_aut_img") );
            String rut_imag_1 = "";
            String rut_imag_2 = "";
            String rut_aut_imag = "";
            String rut_doc_ref = "";
            FileJsonObject doc_ref = null;
            FileJsonObject imag_1 = null;
            FileJsonObject imag_2 = null;
            FileJsonObject aut_imag = null;

            SimpleDateFormat fileFormatDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String newFileDate = fileFormatDate.format(new Date());

            if (doc_ref_obj != null) {
                doc_ref = new FileJsonObject(doc_ref_obj);
                rut_doc_ref = newFileDate + "-" + doc_ref.getName();
                
                if (doc_ref != null) {
                    BuildFile.buildFromBase64("control_patrimonial", rut_doc_ref, doc_ref.getData());
                }
                //    BuildFile.buildFromBase64("scp",rut_doc_ref, doc_ref.getData());
            }

            if (rut_imag_1_obj != null) {
                imag_1 = new FileJsonObject(rut_imag_1_obj);
                rut_imag_1 = newFileDate + "-" + imag_1.getName();
                
                if (imag_1 != null) {
                    BuildFile.buildFromBase64("control_patrimonial", rut_imag_1, imag_1.getData());
                }                
            }
            
            if (rut_imag_2_obj != null) {
                imag_2 = new FileJsonObject(rut_imag_2_obj);

                rut_imag_2 = newFileDate + "-" + imag_2.getName();
                
                if (imag_2 != null) {
                    BuildFile.buildFromBase64("control_patrimonial", rut_imag_2, imag_2.getData());
                }
                
            }
            if (rut_aut_img_obj != null) {
                aut_imag = new FileJsonObject(rut_aut_img_obj);
                rut_aut_imag = newFileDate + "-" + aut_imag.getName();
                
                if (aut_imag != null) {
                    BuildFile.buildFromBase64("control_patrimonial", rut_aut_imag, aut_imag.getData());
                }
                
            }

            /*Construimos los Archivos*/
            /*if (doc_ref != null && imag_1 != null && imag_2 != null && aut_imag != null) {
                BuildFile.buildFromBase64("control_patrimonial", rut_doc_ref, doc_ref.getData());
                BuildFile.buildFromBase64("control_patrimonial", rut_imag_1, imag_1.getData());
                BuildFile.buildFromBase64("control_patrimonial", rut_imag_2, imag_2.getData());
                BuildFile.buildFromBase64("control_patrimonial", rut_aut_imag, aut_imag.getData());

            }*/

            BienesMueblesDAO bienes_dao = (BienesMueblesDAO) FactoryDao.buildDao("scp.BienesMueblesDAO");
            bm = new BienesMuebles(0, cat_bie_id, amb_id, an_id, cantidad, des_bie, fec_reg, cod_int, estado_bie, rut_doc_ref, cod_ba_bie, usu_mod, fec_mod, org_id, est_reg);
            bm.setCon_pat_id(con_pat_id);
            bm.setVerificar(verificar);

            int cod_cuenta = requestData.getInt("cod_cuenta");
            
            int val_con = requestData.getInt("valor_cont");
            char act_dep;
            boolean activo = requestData.getBoolean("act_dep");

            if (activo) {
                act_dep = 'A';

            } else {
                act_dep = 'N';
            }           
            
            boolean tipo_registro = requestData.getBoolean("actualizar_bien");
            /*ACTUALIZAR*/ //actualizar=true , insertar=false
            if (tipo_registro == true) {

                /*Actualizamos la ruta de los archivos*/
                bm.setRut_doc_bie(get_nombre_archivo(requestData.optString("doc_referencia")));

                //fec_res = formato2.parse(mov.getString("fec_reg"));
                int mov_ing_id = requestData.getInt("mov_ing_id");
                mov_ing = new MovimientoIngresos(mov_ing_id, fec_mov, num_res, fec_res, usu_mod, est_reg, con_pat_id);
                mov_ing.setTip_mov_ing_id(tipo_mov_ing);
                mov_ing.setObs(obs);
                MovimientoIngresosDAO mov_ing_dao = (MovimientoIngresosDAO) FactoryDao.buildDao("scp.MovimientoIngresosDAO");
                mov_ing_dao.update(mov_ing);

                int bm_id = requestData.getInt("id_bien");
                bm.setCod_bie(bm_id);

                bm.setMov_ing_id(mov_ing.getMov_ing_id());
                bienes_dao.update(bm);
                dtm = new DetalleTecnicoMuebles(bm.getCod_bie(), marc, mod, cond, dim, ser, col, tip, nro_mot, nro_pla, nro_cha, raza, edad, rut_imag_1, rut_imag_2, rut_aut_imag, est_reg);

                dtm.setRut_imag_1(get_nombre_archivo(requestData.optString("imag_1")));
                dtm.setRut_imag_2(get_nombre_archivo(requestData.optString("imag_2")));
                dtm.setRut_aut_img(get_nombre_archivo(requestData.optString("autopartes")));

                DetalleTecnicoMueblesDAO det_tec_dao = (DetalleTecnicoMueblesDAO) FactoryDao.buildDao("scp.DetalleTecnicoMueblesDAO");
                det_tec_dao.update(dtm);
                int val_cont_id = requestData.getInt("val_cont_id");
                vc = new ValorContable(val_cont_id, est_reg, bm.getCod_bie(), act_dep, cod_cuenta, val_con, org_id);
                ValorContableDAO val_cont_dao = (ValorContableDAO) FactoryDao.buildDao("scp.ValorContableDAO");
                val_cont_dao.update(vc);
                
                msgResponse = "El bien se actualizó correctamente";

            } else {
                /*INSERT*/
                fec_res = formatter.parse(mov.getString("fec_res"));
                mov_ing = new MovimientoIngresos(0, fec_mov, num_res, fec_res, usu_mod, est_reg, con_pat_id);
                mov_ing.setTip_mov_ing_id(tipo_mov_ing);
                mov_ing.setObs(obs);
                MovimientoIngresosDAO mov_ing_dao = (MovimientoIngresosDAO) FactoryDao.buildDao("scp.MovimientoIngresosDAO");
                mov_ing_dao.insert(mov_ing);

                bm.setMov_ing_id(mov_ing.getMov_ing_id());
                bienes_dao.insert(bm);
                dtm = new DetalleTecnicoMuebles(bm.getCod_bie(), marc, mod, cond, dim, ser, col, tip, nro_mot, nro_pla, nro_cha, raza, edad, rut_imag_1, rut_imag_2, rut_aut_imag, est_reg);
                
                String barCodeBien = generateBarCodeNumber(bm.getCod_bie(), 10);
                
                createBarCode(bm.getCod_bie(), barCodeBien);
                
                bm.setCod_ba_bie(barCodeBien);
                
                System.out.println("Codigo de barras" + barCodeBien);
                
                bienes_dao.update(bm);
                
                DetalleTecnicoMueblesDAO det_tec_dao = (DetalleTecnicoMueblesDAO) FactoryDao.buildDao("scp.DetalleTecnicoMueblesDAO");
                det_tec_dao.insert(dtm);

                vc = new ValorContable(0, est_reg, bm.getCod_bie(), act_dep, cod_cuenta, val_con, org_id);
                ValorContableDAO val_cont_dao = (ValorContableDAO) FactoryDao.buildDao("scp.ValorContableDAO");
                val_cont_dao.insert(vc);
                
                msgResponse = "El registro del bien y detalle técnico se realizó correctamente";

            }

        } catch (Exception e) {
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar el Bien Mueble , datos incorrectos", e.getMessage());
        }
        JSONObject oResponse = new JSONObject();
        oResponse.put("cod_bie", bm.getCod_bie());
        return WebResponse.crearWebResponseExito(msgResponse, oResponse);

    }
    
    
    public void createBarCode(int bien_id, String barCodeBien) throws Exception{
        
        Code128 barcode = new Code128();       
                
        //barcode.setData("6889265987");
        barcode.setData(barCodeBien);
        barcode.setX(2);
        barcode.setY(60);
        barcode.setBarcodeWidth(150);
        barcode.setBarcodeHeight(80);

        barcode.setLeftMargin(0);
        barcode.setRightMargin(0);
        //barcode.drawBarcode("/home/harold/Downloads/barcode-code128.png");

        String pathFinal = ServicioREST.PATH_SIGESMED + File.separator + "archivos" + File.separator + "control_patrimonial"
                + File.separator + "codigo_barras";

        (new File(pathFinal)).mkdir();

        barcode.drawBarcode(pathFinal + "/"+ barCodeBien +".png");
    }
    
    private String generateBarCodeNumber(int bien_id, int length) {
        
        String strBienId = String.valueOf(bien_id);
        
        int tam = strBienId.length();
        
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < length - tam; i++) {
            builder.append(CHARACTER_SET.charAt(rnd.nextInt(CHARACTER_SET.length())));
        }
        return strBienId + builder.toString();
        
    }

    //Funcion que verifica la extension de la imagen (.PNG , .JPEG)
    public boolean verificar_imagen(String imag) {
        int size = imag.length();
        String jpg = ".jpg";
        String png = ".png";
        String bmp = ".bmp";
        String format = "";
        for (int i = 0; i < size; i++) {
            if (imag.charAt(i) == '.') {
                format = imag.substring(i);
                break;
            }
        }
        if (format.equals(jpg) | format.equals(png) | format.equals(format)) {
            return true;
        } else {
            return false;
        }
    }

    public String get_nombre_archivo(String path) {

        if (!path.equals("")) {
            return path.substring(13);
        } else {
            return "";
        }
    }

}
