package com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.carpeta;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.CarpetaPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.SeccionCarpetaPedagogica;

import java.util.List;

/**
 * Created by Administrador on 10/10/2016.
 */
public interface CarpetaPedagogicaDao extends GenericDao<CarpetaPedagogica>{
    CarpetaPedagogica buscarCarpetaPorEtapa(int eta);
    CarpetaPedagogica buscarCarpetaPorId(int eta);
    SeccionCarpetaPedagogica buscarSeccionPorId(int idSeccion);
    void registrarSeccionCarpeta(SeccionCarpetaPedagogica seccion);
    void eliminarSeccionCarpeta(int idSeccion);
    void editarSeccionCarpeta(SeccionCarpetaPedagogica seccion);
    List<SeccionCarpetaPedagogica> listarSeccionesCarpeta(int idCarpeta);

}
