/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.EvaluacionesUnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.EvaluacionesUnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.UnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.EvaluacionesUnidadDidactica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.UnidadDidactica;
import org.json.JSONObject;
/**
 *
 * @author abel
 */
public class ObtenerUnidadOrganicaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        try{
            int eva_maes_id = 0;
            JSONObject requestData = (JSONObject)wr.getData();
            
            eva_maes_id = requestData.getInt("eva_maes_id");
            EvaluacionesUnidadDidacticaDao eva_dao = (EvaluacionesUnidadDidacticaDao)FactoryDao.buildDao("maestro.plan.EvaluacionesUnidadDidacticaDao");
            EvaluacionesUnidadDidactica evaluacion = eva_dao.buscarEvaluacionId(eva_maes_id);
            JSONObject oResponse = new JSONObject();
            oResponse.put("uni_did_id",evaluacion.getUni_did_id());
            
        
            
             return WebResponse.crearWebResponseExito("Se Obtuvo la Unidad Didactica correctamente",oResponse);        
          
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Obtener la Unidad Didactica ", e.getMessage() ); 

        }
         
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
}
