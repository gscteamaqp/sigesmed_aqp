/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.plan_estudios.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.DistribucionHoraGradoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.PlanEstudiosDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.DistribucionHoraGrado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlazaMagisterial;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONObject;

/**
 *
 * @author HernanF
 */
public class BuscarPlanEstudiosDesdeTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        int orgID, anio;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            orgID = requestData.getInt("organizacionID");
            anio = requestData.getInt("anio");
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo encontrar los planes de estudios, datos incorrectos", e.getMessage() );
        }
        
         /*
        *  Parte para la operacion en la Base de Datos
        */
         
        List<PlanEstudios> planes = null;
        List<DistribucionHoraGrado> distribucionesHoraGrado = new ArrayList<>();
        List<List<PlazaMagisterial>> plazasPorAnio = new ArrayList();
        List<Integer> anios = new ArrayList();
        try{
            PlanEstudiosDao planDao = (PlanEstudiosDao)FactoryDao.buildDao("mech.PlanEstudiosDao");
            DistribucionHoraGradoDao disDao = (DistribucionHoraGradoDao)FactoryDao.buildDao("mech.DistribucionHoraGradoDao");
            planes = planDao.buscarConOrganizacion();
            for(PlanEstudios plan:planes){
                if((plan.getOrgId() == orgID)  && (plan.getAñoEsc().getYear() + 1900 >= anio)){
                    List<PlazaMagisterial> plazas = new ArrayList();
                    distribucionesHoraGrado = disDao.listarDistribucionHorasGradoPorPlanEstudios(plan.getPlaEstId());
                    for(DistribucionHoraGrado distribucion : distribucionesHoraGrado){
                        PlazaMagisterial temp = disDao.buscarPlazaPorID(distribucion.getPlaMagId());
                        plazas.add(temp);
                    }
                    plazasPorAnio.add(plazas);
                    anios.add(plan.getAñoEsc().getYear() + 1900);
                }
            } 
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo encontrar plan de estudios vigente", e.getMessage() );
        }
        JSONObject plazaTemp;
        JSONObject plazasTemp;
        JSONObject output = new JSONObject();
        for(int i = 0; i < plazasPorAnio.size(); i++){
            plazasTemp = new JSONObject();
            for(PlazaMagisterial plaza: plazasPorAnio.get(i)){
                plazaTemp = new JSONObject();
                plazaTemp.put("Nexus", plaza.getCodigoNexus());
                plazaTemp.put("Cargo", plaza.getCargo());
                plazaTemp.put("Condicion", plaza.getCondicion().getNom());
                plazaTemp.put("Modalidad", plaza.getModalidad());
                plazaTemp.put("Caracteristica", plaza.getCaracteristica());
                plazaTemp.put("Motivo", plaza.getMotivo());
                plazaTemp.put("Naturaleza", plaza.getNaturaleza().getNom());
                plazaTemp.put("Nivel", plaza.getNivel());
                plazasTemp.put(plaza.getCodigoNexus(), plazaTemp);
            }
            output.put(Integer.toString(anios.get(i)), plazasTemp);
        }
        
        return WebResponse.crearWebResponseExito("Se encontro el plan de estudios vigente", output);
    }
    
}
