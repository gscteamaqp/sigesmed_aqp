/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.cuadro_horas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.DistribucionHoraGradoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.DistribucionHoraArea;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.DistribucionHoraGrado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlazaMagisterial;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
/**
 *
 * @author abel
 */
public class InsertarDistribucionHoraGradoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */        
        List<DistribucionHoraGrado> grados = new ArrayList<DistribucionHoraGrado>();

        
        PlazaMagisterial plaza = null;
        int planID = 0;
        try{
            
            JSONObject req = (JSONObject)wr.getData();
            
            planID = req.getInt("planID");
            int plazaID = req.getInt("plazaID");
            String modalidad = req.getString("modalidad");
            String nivel = req.getString("nivel");
            String caracteristica = req.getString("caracteristica");            
            char tipo = req.getString("tipo").charAt(0);
            String nexus = req.getString("nexus");
            String cargo = req.getString("cargo");
            int jornadaLaboral = req.getInt("jornadaLaboral");
            int jornadaPedagogica = req.getInt("jornadaPedagogica");
            String especialidad = req.optString("especialidad");
            String motivo = req.optString("motivo");
            char condicionID = req.getString("condicionID").charAt(0);
            char naturalezaID = req.getString("naturalezaID").charAt(0);
            Integer docenteID = req.optInt("docenteID");
            int organizacionID = req.getInt("organizacionID");
            
            plaza = new PlazaMagisterial(plazaID, modalidad, nivel, caracteristica, tipo, nexus,cargo,jornadaLaboral,jornadaPedagogica,especialidad, motivo,condicionID,naturalezaID, organizacionID, docenteID);
            
            JSONArray listaDistribucion = req.getJSONArray("distribucion");
            
            if(listaDistribucion.length() > 0){
                //si es secundaria se contabilizan las areas
                if(nivel.contentEquals("secundaria")){
                    
                    for(int i = 0; i < listaDistribucion.length();i++){
                        JSONObject bo = listaDistribucion.getJSONObject(i);

                        int disGradoID = bo.optInt("disGradoID");
                        int gradoID = bo.getInt("gradoID");
                        String sec = bo.optString("seccionID");
                        char seccionID = 'A';
                        if(!sec.contentEquals(""))                            
                            seccionID= sec.charAt(0);
                        
                        int disAreaID = bo.optInt("disAreaID");
                        int areaID = bo.getInt("areaID");
                        
                        int hora = bo.getInt("hora");
                        
                        DistribucionHoraGrado d = sumarGrados(grados,disGradoID,planID,plazaID, gradoID,seccionID, hora);
                        
                        //si son areas curriculares
                        if(areaID != -1){
                            d.getDistribucionAreas().add(new DistribucionHoraArea(disAreaID,hora,d,areaID));
                        }
                        //si es tutoria
                        else{
                            d.setHorTut(hora);
                        }
                    }
                    
                }
                else{ // Para el caso de Primaria
                    for(int i = 0; i < listaDistribucion.length();i++){
                        JSONObject bo = listaDistribucion.getJSONObject(i);

                        int disGradoID = bo.optInt("disGradoID");
                        int gradoID = bo.getInt("gradoID");
                        
                        //Esta parte es temporal para la secciones
                        String sec = bo.optString("seccionID");
                        char seccionID = 'A';
                        if(!sec.contentEquals(""))                            
                            seccionID= sec.charAt(0);
                        //
                        
                        int hora = bo.getInt("hora");                   
                        DistribucionHoraGrado d = new DistribucionHoraGrado(disGradoID,hora,planID,plazaID,gradoID,seccionID);
                        List<DistribucionHoraArea> horas_area =  new ArrayList<DistribucionHoraArea>();
                        JSONArray areas = req.getJSONArray("areas");
                            for (int j = 0 ; j< areas.length();j++){
                                JSONObject area = areas.getJSONObject(j);
                                 DistribucionHoraArea dis = new DistribucionHoraArea(); 
                                dis.setDistribucionGrado(d);
                                int areaID = area.optInt("areaID");
                                dis.setAreCurId(areaID);
                                horas_area.add(dis);
                            }
                  
                        d.setDistribucionAreas(horas_area);    
                        grados.add(d);
                        
                    }
                    //Al final le asignamos todos los cursos dados por el DiseÃ±o Curricular Actual (OJO : Solo para el caso de Primaria)
                    
                }
            }
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo hacer la distribucion de horas en la plaza, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        try{
            DistribucionHoraGradoDao disDao = (DistribucionHoraGradoDao)FactoryDao.buildDao("mech.DistribucionHoraGradoDao");
            
            disDao.eliminarDistribucionPorPlanYPlaza(planID,plaza.getPlaMagId());
            disDao.insertarDistribucionGrados(grados);
            disDao.actualizarHorasPedagogicasPorPlaza(plaza.getPlaMagId(),plaza.getJornadaPedagogica());
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo hacer la distribucion de horas en la plaza", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        return WebResponse.crearWebResponseExito("La distribucion de horas para la plaza se realizo correctamente");
        //Fin
    }
    
    public DistribucionHoraGrado sumarGrados(List<DistribucionHoraGrado> grados, int disGradoID, int planID,int plazaID, int gradoID,char seccionID ,int hora){
        
        for( DistribucionHoraGrado d: grados ){
            if(d.getGraId() == gradoID){                
                d.setHorAsi( d.getHorAsi() + hora );
                return d;
            }
        }
        
        DistribucionHoraGrado d = new DistribucionHoraGrado(disGradoID,hora,planID,plazaID,gradoID,seccionID);
        d.setDistribucionAreas(new ArrayList<DistribucionHoraArea>());
        grados.add( d );
        return d;        
    }
    
    
}
