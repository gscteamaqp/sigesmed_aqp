/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_comunicacion.generar_plantilla.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sdc.PlantillaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.PropiedadLetra;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class listarPropiedadLetraTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {

        /*
        *  Parte para la operacion en la Base de Datos
        */
       
        List<PropiedadLetra> _nom_letras = new ArrayList<>();
        PlantillaDao plantillaDao = (PlantillaDao)FactoryDao.buildDao("sdc.PlantillaDao");
     
        _nom_letras =plantillaDao.listarNombresLetras();
            
       
        //Fin
        
        /*
         *  Repuesta Correcta
         */
        
        JSONArray miArray = new JSONArray();
        
            for (PropiedadLetra nomLetra : _nom_letras) {
                JSONObject oResponse = new JSONObject();
                oResponse.put("nomLetra", nomLetra.getProLetDes());
                oResponse.put("idLetra", nomLetra.getProLetId());
                miArray.put(oResponse);
           
        }
 
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}

