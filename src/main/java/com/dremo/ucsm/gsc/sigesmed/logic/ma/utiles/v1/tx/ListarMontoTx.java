/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.ma.utiles.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.MontoListaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.MontoLista;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class ListarMontoTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ListarMontoTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        return listarMontos();
    }

    private WebResponse listarMontos() {
       MontoListaDao montoListaDao = (MontoListaDao) FactoryDao.buildDao("ma.MontoListaDao");
       try{
           List<MontoLista> montos = montoListaDao.buscarTodos(MontoLista.class);
           JSONArray array = new JSONArray();
           for(MontoLista monto: montos){
            
            String strjson = EntityUtil.objectToJSONString(new String[]{"monId","anioMonto","valMonto"},new String[]{"id","ani","val"},monto);
            JSONObject jsonMonto = new JSONObject(strjson);
            
            JSONObject nivelJSON = new JSONObject(EntityUtil.objectToJSONString(new String[]{"nivId","nom"},new String[]{"id","nom"},monto.getNivel()));
            jsonMonto.put("niv", nivelJSON);
            array.put(jsonMonto);
           }
           return WebResponse.crearWebResponseExito("Exito al listar los montos",array);
       }catch (Exception e){
           logger.log(Level.SEVERE,"listarComisiones",e);
            return WebResponse.crearWebResponseError("Error al listar los montos",WebResponse.BAD_RESPONSE);
        }      
    }
    
    
}
