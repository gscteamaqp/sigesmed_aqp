/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author ucsm
 */
@Entity
@Table(name="COLUMNS", schema="INFORMATION_SCHEMA")
public class Columns implements java.io.Serializable {
    @Id 
    @Column(name="column_name", unique=true, nullable=false)
    private String column_name;
    @Column(name="table_name", unique=true, nullable=false, length=32)
    private String table_name;
    @Column(name="character_maximum_length", length=16)
    private int character_maximum_length;
    
    public Columns() {
    }
    
    public int getLength(){
        return character_maximum_length;
    }
    
    public String getName(){
        return column_name;
    }
    
    public String getTableName(){
        return table_name;
    }
}