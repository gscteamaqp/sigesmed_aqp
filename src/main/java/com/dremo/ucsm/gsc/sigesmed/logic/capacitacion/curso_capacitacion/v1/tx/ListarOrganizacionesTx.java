package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.CursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.OrganizacionCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class ListarOrganizacionesTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(ListarCursosCapacitacionTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int opt = data.getInt("opt");
        WebResponse response = null;

        switch (opt) {
            case 0:
                response = listarOrganizaciones();
                break;

            case 1:
                response = listarOrganizacionesBanPre();
                break;
                
            case 2:
                response = listarParaCertificacion(data.getInt("codCap"));
                break;
        }

        return response;
    }

    private WebResponse listarOrganizaciones() {
        try {
            CursoCapacitacionDao capacitacionDao = (CursoCapacitacionDao) FactoryDao.buildDao("capacitacion.CursoCapacitacionDao");
            List<Organizacion> organizaciones = capacitacionDao.listarOrganizaciones();

            return WebResponse.crearWebResponseExito("Se listo correctamente", WebResponse.OK_RESPONSE)
                    .setData(new JSONArray(EntityUtil.listToJSONString(
                            new String[]{"orgId", "ali", "nom"},
                            new String[]{"id", "nom", "com"},
                            organizaciones
                    )));
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarOrganizaciones", e);
            return WebResponse.crearWebResponseError("Error al listar las organizaciones", WebResponse.BAD_RESPONSE);
        }
    }

    private WebResponse listarOrganizacionesBanPre() {
        try {
            CursoCapacitacionDao capacitacionDao = (CursoCapacitacionDao) FactoryDao.buildDao("capacitacion.CursoCapacitacionDao");
            List<Organizacion> organizations = capacitacionDao.listarOrganizaciones();

            return WebResponse.crearWebResponseExito("Se listo correctamente", WebResponse.OK_RESPONSE)
                    .setData(new JSONArray(EntityUtil.listToJSONString(
                            new String[]{"orgId", "ali"},
                            new String[]{"cod", "nom"},
                            organizations
                    )));
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarOrganizacionesBanPre", e);
            return WebResponse.crearWebResponseError("Error al listar las organizaciones", WebResponse.BAD_RESPONSE);
        }
    }
    
    private WebResponse listarParaCertificacion(int codCap) {
        try {
            OrganizacionCapacitacionDao organizacionCapacitacionDao = (OrganizacionCapacitacionDao) FactoryDao.buildDao("capacitacion.OrganizacionCapacitacionDao");
            JSONObject object = new JSONObject();
            object.put("capOrg", organizacionCapacitacionDao.buscarCapacitacionOrg(codCap));
            object.put("capAut", organizacionCapacitacionDao.buscarCapacitacionAut(codCap));
            
            return WebResponse.crearWebResponseExito("Se listo correctamente", WebResponse.OK_RESPONSE).setData(object);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarOrganizacionesBanPre", e);
            return WebResponse.crearWebResponseError("Error al listar las organizaciones", WebResponse.BAD_RESPONSE);
        }
    }
}
