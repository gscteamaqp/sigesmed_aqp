/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.catalogo_bienes.v1.core;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;

import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.catalogo_bienes.v1.tx.ImportarCatalogoBienesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.catalogo_bienes.v1.tx.ListarCatalogoBienesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.catalogo_bienes.v1.tx.ListarClaseTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.catalogo_bienes.v1.tx.ListarGrupoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.catalogo_bienes.v1.tx.ListarUnidadMedidaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.catalogo_bienes.v1.tx.RegistrarCatalogoBienesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.catalogo_bienes.v1.tx.RegistrarClaseTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.catalogo_bienes.v1.tx.RegistrarFamiliaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.catalogo_bienes.v1.tx.RegistrarGrupoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.catalogo_bienes.v1.tx.RegistrarUnidadMedidaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.catalogo_bienes.v1.tx.EditarItemCatalogoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.catalogo_bienes.v1.tx.EliminarItemCatalogoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.catalogo_bienes.v1.tx.ListarFamiliaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.catalogo_bienes.v1.tx.ReporteCatalogoTx;

/**
 *
 * @author Administrador
 */




public class ComponentRegister implements IComponentRegister{

    
    @Override
    public WebComponent createComponent() {
        
         // Asignando al modulo al cual Pertenece
         WebComponent component = new WebComponent(Sigesmed.MODULO_SISTEMA_CONTROL_PATRIMONIAL);
         
        //Registrando el Nombre del Componente
        component.setName("catalogo_bienes");
        //version del componente
        component.setVersion(1);
        
        component.addTransactionGET("importar_catalogo_bienes",ImportarCatalogoBienesTx.class);
        component.addTransactionGET("listar_catalogo_bienes",ListarCatalogoBienesTx.class);        
        component.addTransactionGET("listar_clases",ListarClaseTx.class); 
        component.addTransactionGET("listar_grupos",ListarGrupoTx.class);
        component.addTransactionGET("listar_unidades_medida",ListarUnidadMedidaTx.class);
        component.addTransactionPOST("registrar_catalogo_bienes", RegistrarCatalogoBienesTx.class);
        component.addTransactionPOST("registrar_clase", RegistrarClaseTx.class);
        component.addTransactionPOST("registrar_familia", RegistrarFamiliaTx.class);
        component.addTransactionPOST("registrar_unidad_medida", RegistrarUnidadMedidaTx.class);
        component.addTransactionPUT("editar_item_catalogo",EditarItemCatalogoTx.class);        
        component.addTransactionDELETE("eliminar_item_catalogo", EliminarItemCatalogoTx.class);
        component.addTransactionGET("listar_familias",ListarFamiliaTx.class);
        component.addTransactionGET("reporte_catalogo", ReporteCatalogoTx.class);
         
         
        return component;
        
     //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
