/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import java.util.List;

import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BienesMuebles;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.BienesMueblesDAO;
import java.text.SimpleDateFormat;

/**
 *
 * @author Administrador
 */
public class ListarBienesMueblesTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {

        JSONArray miArray = new JSONArray();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        
        try {
            List<BienesMuebles> bie_mue = null;
            JSONObject requestData = (JSONObject) wr.getData();
            int org_id = requestData.getInt("org_id");
            BienesMueblesDAO bie_mue_dao = (BienesMueblesDAO) FactoryDao.buildDao("scp.BienesMueblesDAO");
            bie_mue = bie_mue_dao.listarBienesMuebles(org_id);

            for (BienesMuebles bm : bie_mue) {
                JSONObject oResponse = new JSONObject();
                oResponse.put("cod_bie", bm.getCod_bie());
                oResponse.put("cat_bie_id", bm.getCat_bie_id());
                oResponse.put("mov_ing_id", bm.getMov_ing_id());
                oResponse.put("cod_pat_bie", bm.getCon_pat_id());
                oResponse.put("des_bie", bm.getDes_bie());
                oResponse.put("cant_bie", bm.getCant_bie());
                oResponse.put("fec_reg", formatter.format(bm.getFec_reg()));
                oResponse.put("est_bien", bm.getEstado_bie());
                oResponse.put("ubi_bien", bm.getAmbiente().getDes());
                oResponse.put("an_bien", bm.getAnexo().getAn_des());
                oResponse.put("tip_bie", false);
                oResponse.put("verificar", bm.getVerificar());
                oResponse.put("org_id", bm.getOrg_id());
                miArray.put(oResponse);

            }

        } catch (Exception e) {
            System.out.println("No se pudo Mostrar los Bienes Muebles\n" + e);
            return WebResponse.crearWebResponseError("No se pudo Listar los Bienes Muebles", e.getMessage());
        }
        return WebResponse.crearWebResponseExito("Se Listo los Bienes Muebles Correctamente", miArray);

        //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
