/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.proyectos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.ProyectosDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.ProyectoActividades;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.ProyectoRecursos;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.Proyectos;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ListarProyectoTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        JSONObject requestData = (JSONObject)wr.getData();
        int proid = requestData.getInt("proid");
                
        Proyectos proyecto = null;
        ProyectosDao proyectoDao = (ProyectosDao)FactoryDao.buildDao("smdg.ProyectosDao");        
        
        try{
            proyecto = proyectoDao.load(Proyectos.class, proid);
        }catch(Exception e){            
            return WebResponse.crearWebResponseError("No se encontro el proyecto", e.getMessage());
        }
        //Fin             
        /*
        *  Repuesta Correcta
        */               
        
        JSONArray oproyecto = new JSONArray();        
        Set<ProyectoActividades> actividades = proyecto.getActividades();
        
        for(ProyectoActividades a:actividades){
//            int gruId = a.getPgrId();
            if("A".equals(a.getEstReg())){
                JSONObject oactividad = new JSONObject();
                oactividad.put("pacid",a.getPacId());
                oactividad.put("pacdes",a.getPacDes());
                oactividad.put("pacini",a.getPacIni());
                oactividad.put("pacfin",a.getPacFin());
                oactividad.put("pacres",a.getPacRes());
                oactividad.put("pacava",a.getPacAva());

                JSONArray orecursos = new JSONArray();        
                Set<ProyectoRecursos> recursos = a.getRecursos();
                for(ProyectoRecursos r:recursos){
                    if("A".equals(r.getEstReg())){
                        JSONObject orecurso = new JSONObject();
                        orecurso.put("recid",r.getPruId());
                        orecurso.put("recnom",r.getPruNom());
                        orecurso.put("reccos",r.getPruCos());
        //                orecurso.put("prunom",r.get);
                        orecursos.put(orecurso);
                    }                    
                }
                oactividad.put("recursos",orecursos);            
                oproyecto.put(oactividad);            
            }
            
        }
        
//        proini:fechaini, profin:fechafin, actividades:$scope.tablaActividades.data});
        
//        JSONObject cabecera = new JSONObject();
//        cabecera.put("proid", proyecto.getProId());
//        cabecera.put("pronom", proyecto.getProNom());
//        cabecera.put("protip", proyecto.getProTip());
//        cabecera.put("prores", proyecto.getProRes());
//        cabecera.put("nombreArchivo", proyecto.getDetalle().getPdeDoc());
//        cabecera.put("proini", proyecto.getProIni());
//        cabecera.put("profin", proyecto.getProFin());        
//        
//        oproyecto.put(cabecera);
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",oproyecto);        
                
    }    
}
