/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.asistencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.AsistenciaAulaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.AsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.AsistenciaAula;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.HorarioDetalle;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.DateUtil;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author carlos
 */
public class MarcarAsistenciaAulaTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {

        Docente docente = null;
        Integer dayOfWeek;
        Long idRegistro;
        Date javaDate;
        String horaSalida;
        String horaIngreso;
        Integer idDocente;
        try {
            JSONObject requestData = (JSONObject) wr.getData();
            idDocente = requestData.getInt("id");
            String hora = requestData.getString("hora");
            JSONObject registroLast = requestData.getJSONObject("lastReg");

            javaDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(hora);
            Calendar c = Calendar.getInstance();
            c.setTime(javaDate);
            dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
            docente = new Docente(idDocente);

            idRegistro = registroLast.getLong("id");
            horaSalida = registroLast.getString("salida");
            horaIngreso = registroLast.getString("ingreso");

        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo verificar los datos", e.getMessage());
        }
        AsistenciaDao asistenciaDao = (AsistenciaDao) FactoryDao.buildDao("cpe.AsistenciaDao");
        AsistenciaAulaDao asistenciaAulaDao = (AsistenciaAulaDao) FactoryDao.buildDao("cpe.AsistenciaAulaDao");
        try {

            List<HorarioDetalle> horarioDay = null;
            SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm a");//dd/MM/yyyy
            switch (dayOfWeek) {
                case 1:
                    horarioDay = asistenciaDao.listarHorarioMech(idDocente, new Character('D'));
                    break;
                case 2:
                    horarioDay = asistenciaDao.listarHorarioMech(idDocente, new Character('L'));
                    break;
                case 3:
                    horarioDay = asistenciaDao.listarHorarioMech(idDocente, new Character('M'));
                    break;
                case 4:
                    horarioDay = asistenciaDao.listarHorarioMech(idDocente, new Character('W'));
                    break;
                case 5:
                    horarioDay = asistenciaDao.listarHorarioMech(idDocente, new Character('J'));
                    break;
                case 6:
                    horarioDay = asistenciaDao.listarHorarioMech(idDocente, new Character('V'));
                    break;
                case 7:
                    horarioDay = asistenciaDao.listarHorarioMech(idDocente, new Character('S'));
                    break;

            }

            if (horarioDay.size() != 0) //si tiene horario
            {

                if (idRegistro == -1) //si captura un registro vacio desde la interfaz
                {
                    //caso que sea entrada
                    AsistenciaAula nuevoRegistro = new AsistenciaAula(javaDate, null, "A", docente, wr.getIdUsuario());
                    asistenciaAulaDao.insert(nuevoRegistro);

                    JSONObject oRegistro = new JSONObject();
                    oRegistro.put("id", nuevoRegistro.getAsistenciaAulaId());
                    oRegistro.put("ingreso", sdfDate.format(nuevoRegistro.getHoraIngreso()));
                    oRegistro.put("salida", "");
                    oRegistro.put("hrs", "");
                    oRegistro.put("existe", false);

                    return WebResponse.crearWebResponseExito("Se Registro su Ingreso", oRegistro);

                } else {

                    if (horaSalida.length() > 0) //si el registro tiene una salida
                    {

                        AsistenciaAula nuevoRegistro = new AsistenciaAula(javaDate, null, "A", docente, wr.getIdUsuario());
                        asistenciaAulaDao.insert(nuevoRegistro);

                        JSONObject oRegistro = new JSONObject();
                        oRegistro.put("id", nuevoRegistro.getAsistenciaAulaId());
                        oRegistro.put("ingreso", sdfDate.format(nuevoRegistro.getHoraIngreso()));
                        oRegistro.put("salida", "");
                        oRegistro.put("hrs", "");
                        oRegistro.put("existe", false);
                        return WebResponse.crearWebResponseExito("Se Registro su Ingreso", oRegistro);

                    }
                    
                    Date horaIngresoRegistro=sdfDate.parse(horaIngreso);//hora de ingreso del ultimo registro, la hora de salida es javaDate
                    int horasPedagogicas=0;
                    int minutos=0;
                    String horasEfectivasFromHorarioDetalle="";//contiene el nro de hora efectiva que realizo correctamente
                    int idx=1;//identificador de numero de horario detalle
                    for (HorarioDetalle h : horarioDay) {
                        //si la horas de ingreso es menor o igual a la de la hora pedagogica y la salida es mayor a la hora pedagogica
                        if(DateUtil.compararHora1yHora2(javaDate, h.getHorFin())&&DateUtil.compararHora1yHora2(h.getHorIni(), horaIngresoRegistro))
                        {
                            horasPedagogicas++;
                            horasEfectivasFromHorarioDetalle+=idx+"";
                        } 
                        //si la hora de ingreso es mayor a la hora pedagogica y la hora de salida es mayor a la hora pedagogica
                        else if(DateUtil.compararHora1yHora2(horaIngresoRegistro, h.getHorIni())&&DateUtil.compararHora1yHora2(h.getHorFin(), horaIngresoRegistro)&&DateUtil.compararHora1yHora2(javaDate, h.getHorFin()))
                        {
                            minutos=minutos+DateUtil.diferenciaFechas(horaIngresoRegistro, h.getHorFin(), 4);
                            if(DateUtil.diferenciaFechas(horaIngresoRegistro, h.getHorFin(), 4)%Sigesmed.MINUTOS_EQUIVALENTES_1_HORA_PEDAGOGICA>Sigesmed.MINUTOS_MINIMO_1_HORA_PEDAGOGICA)
                            {
                                horasEfectivasFromHorarioDetalle+=idx+"";
                            }
                        } 
                        //si la hora de ingreso es mayor a la hora pedagogica y la hora de salida menor o igual a la hora pedagogica
                        else if(DateUtil.compararHora1yHora2(horaIngresoRegistro, h.getHorIni())&&DateUtil.compararHora1yHora2(h.getHorFin(),javaDate))
                        {
                            minutos=minutos+DateUtil.diferenciaFechas(horaIngresoRegistro, javaDate, 4);
                            if(DateUtil.diferenciaFechas(horaIngresoRegistro, h.getHorFin(), 4)%Sigesmed.MINUTOS_EQUIVALENTES_1_HORA_PEDAGOGICA>Sigesmed.MINUTOS_MINIMO_1_HORA_PEDAGOGICA)
                            {
                                horasEfectivasFromHorarioDetalle+=idx+"";
                            }
                        }
                        idx++;
                    }
                    /**Redondeo al mayor**/
                    int hoAd=minutos/Sigesmed.MINUTOS_EQUIVALENTES_1_HORA_PEDAGOGICA;
                    if(minutos%Sigesmed.MINUTOS_EQUIVALENTES_1_HORA_PEDAGOGICA>Sigesmed.MINUTOS_MINIMO_1_HORA_PEDAGOGICA)
                    {
                        hoAd++;
                    }
                   
                    asistenciaAulaDao.registrarHoraSalida(idRegistro, javaDate,horasPedagogicas+hoAd,horasEfectivasFromHorarioDetalle);
                    JSONObject oRegistro = new JSONObject();
                    oRegistro.put("id", idRegistro);
                    oRegistro.put("salida", sdfDate.format(javaDate));
                    oRegistro.put("hrs", horasPedagogicas+(minutos/Sigesmed.MINUTOS_EQUIVALENTES_1_HORA_PEDAGOGICA)+"");
                    oRegistro.put("existe", true);
                    return WebResponse.crearWebResponseExito("Se Registro su Salida", oRegistro);

                }
            } else {
                return WebResponse.crearWebResponseError("No existe Horario Asociado de Hoy");
            }

        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo verificar los datos", e.getMessage());
        }

        //Fin
    }

}
