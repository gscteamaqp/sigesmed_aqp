package com.dremo.ucsm.gsc.sigesmed.logic.ma.notas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.NotaEvaluacionIndicadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.SesionAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.UnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.NotaEvaluacionIndicador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.NotaIndicador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Estudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.GradoIEEstudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.IndicadorAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 17/01/2017.
 */
public class ListarNotasIndicadoresAlumnsTx implements ITransaction{
    private static Logger logger = Logger.getLogger(ListarNotasIndicadoresAlumnsTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idOrg = data.getInt("org");
        int idGrad = data.getInt("gra");
        String secc = data.getString("sec");
        int idSession = data.optInt("ses");
        int idUnidad = data.optInt("uni");
        char idPeriodo  = data.getString("per").charAt(0);
        int numPer  = data.getInt("numper");
        return listarNotas(idOrg, idGrad, secc, idSession,idUnidad,idPeriodo,numPer);
    }

    private WebResponse listarNotas(int idOrg, int idGrad, String secc, int idSession,int idUnidad,char idPeriodo,int numPer) {
        try{
            
            
            NotaEvaluacionIndicadorDao notaDao = (NotaEvaluacionIndicadorDao) FactoryDao.buildDao("ma.NotaEvaluacionIndicadorDao");
            SesionAprendizajeDao sesionDao = (SesionAprendizajeDao) FactoryDao.buildDao("maestro.plan.SesionAprendizajeDao");
            UnidadDidacticaDao unidadDao = (UnidadDidacticaDao) FactoryDao.buildDao("maestro.plan.UnidadDidacticaDao");
            List<IndicadorAprendizaje> indicadores = null;
            
            if(idSession != 0){
                
              indicadores  = sesionDao.listarIndicadoresSesion(idSession);
            }
            else{
              indicadores  = unidadDao.listarIndicadoresUnidad(idUnidad);
            }
            
            
            JSONArray indicadoresJSON = new JSONArray(EntityUtil.listToJSONString(
                    new String[]{"indAprId","nomInd","eva"},
                    new String[]{"id","nom","eva"},
                    indicadores
            ));
            List<GradoIEEstudiante> estudiantesGrado = notaDao.listarEstudiantesGradoActual(idOrg, idGrad, secc.charAt(0));
            JSONArray estudiantesJSON = new JSONArray();
            for(GradoIEEstudiante graIe : estudiantesGrado){
                Estudiante estudiante= graIe.getMatriculaEstudiante().getEstudiante();
                JSONObject jsonEstudiante = new JSONObject(EntityUtil.objectToJSONString(
                        new String[]{"perId", "dni", "nom", "apePat", "apeMat", "fecNac"},
                        new String[]{"id", "dni", "nom", "pat", "mat", "nac"},
                        estudiante.getPersona()
                ));
                jsonEstudiante.put("idgra",graIe.getGraOrgEstId());
                jsonEstudiante.put("notas",new JSONArray());
                for(IndicadorAprendizaje indicador : indicadores){
                    NotaEvaluacionIndicador nota;
                    List<NotaEvaluacionIndicador> notas;
                    if(idSession == 0){ // Para el Caso de Notas de Evaluacion OJO: Notas de Evaluacion no dependen de Sesion de Aprendizaje
                          notas = notaDao.buscarNotaIndicadoresEstudiante(indicador.getIndAprId(), graIe.getGraOrgEstId(),idPeriodo,numPer);  
                          for(NotaEvaluacionIndicador not:notas){
                              if(not == null){
                                   jsonEstudiante.getJSONArray("notas").put(new JSONObject().put("ind",indicador.getIndAprId()));
                              }
                              else{
                                jsonEstudiante.getJSONArray("notas").put(new JSONObject()
                                .put("ind",indicador.getIndAprId())
                                .put("id",not.getNotEvaIndId())
                                .put("not",not.getNotaEva())
                                .put("not_lit",not.getNot_lit()));
                              }
                          }
                        
                    }else{
                          nota = notaDao.buscarNotaIndicadorEstudiante(idSession, indicador.getIndAprId(), graIe.getGraOrgEstId(),idPeriodo,numPer);
                           if(nota == null){
                                jsonEstudiante.getJSONArray("notas").put(new JSONObject().put("ind",indicador.getIndAprId()));
                            }else{
                                jsonEstudiante.getJSONArray("notas").put(new JSONObject()
                                    .put("ind",indicador.getIndAprId())
                                    .put("id",nota.getNotEvaIndId())
                                    .put("not",nota.getNotaEva())
                                    .put("not_lit",nota.getNot_lit()));
                            }
                    }
                  
                    
                   
                }
                /*jsonEstudiante.put("notas",new JSONArray());
                List<NotaIndicador> notas = notaDao.listarNotasIndicadorAlumno(idSession,graIe.getGraOrgEstId());
                for(NotaIndicador nota :notas){
                    JSONObject jsonNota = new JSONObject(EntityUtil.objectToJSONString(
                            new String[]{"notevaindid", "indaprid", "nomind", "graieestid", "notaeva", "tipnot"},
                            new String[]{"notid", "indid", "indnom", "graid", "not", "tip"},
                            nota
                    ));
                    jsonEstudiante.getJSONArray("notas").put(jsonNota);
                }*/
                estudiantesJSON.put(jsonEstudiante);
            }
            JSONObject results = new JSONObject().put("indicadores",indicadoresJSON).put("estudiantes",estudiantesJSON);

            return WebResponse.crearWebResponseExito("Se listo las notas correctamente",results);
        }catch (Exception e){
            logger.log(Level.SEVERE,"No se puede listar las notas",e);
            return WebResponse.crearWebResponseError("No se puede listar las notas");
        }
    }
}
