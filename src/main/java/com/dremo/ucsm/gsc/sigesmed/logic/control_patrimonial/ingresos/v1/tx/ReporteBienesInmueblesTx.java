/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.BienInmuebleDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BienesInmuebles;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BienesMuebles;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import java.text.SimpleDateFormat;


import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.dremo.ucsm.gsc.sigesmed.util.GCell;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.border.SolidBorder;
import com.itextpdf.layout.element.Paragraph;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import java.util.ArrayList;


/**
 *
 * @author Administrador
 */
public class ReporteBienesInmueblesTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        try {
            
            JSONObject requestData = (JSONObject) wr.getData();
            int org_id = requestData.getInt("org_id");
                
            List<BienesInmuebles> bim = null;
            BienInmuebleDAO bim_dao = (BienInmuebleDAO) FactoryDao.buildDao("scp.BienInmuebleDAO");
            
            bim = bim_dao.reporteBieneInmuebles(org_id);
            
            System.out.println("Numero de bienes inmbueles " + bim.size());
            
            System.out.println("Ficha regisral " + bim.get(0).getDtm().getNro_fic_reg());
            
            Organizacion org = null;
            OrganizacionDao org_dao = (OrganizacionDao) FactoryDao.buildDao("OrganizacionDao");
            org = org_dao.buscarConTipoOrganizacionYPadre(org_id);
            String nombre_ie = org.getNom();
            String ubic_ie = org.getDir();
            String org_pad = org.getOrganizacionPadre().getDes();
            
            String c1[] = {"Nombre de la institucion", nombre_ie};
            String c2[] = {"Director IE", "", "UGEL", "Mcal Nieto"};
            
            /*Creacion de las Cabeceras (Informacion de la Institucion , Cabeceras de Tablas )*/
            String cab1[] = {"DATOS DEL BIEN INMUEBLE", "DETALLES DEL BIEN INMUEBLE"};
            String cab2[] = {
                "#", "Ubigeo", "Dirección",
                "N°", "Mz", "Lte", 
                "Propiedad", "Tip. Terreno", "Saneado",
                "Area", "UM",
                "Reg SINABIP", "Part. Electrónica", "Fic. Regisral"
            };
            
            Mitext m = null;
            m = new Mitext(true, "REPORTE DE BIENES INMUEBLES DE LA INSTITUCION");
            m.newLine(2);       
            
            m.agregarParrafo("Ubicacion : " +ubic_ie);
            m.newLine(2);
            m.agregarParrafo("UGEL :"+ org_pad);

            // Creamos el Objeto Reporte
            
            float columnWidths_1[] = {9,14};
            float columnWidths_2[] = {1, 2, 3, 1, 1, 1, 2, 2, 1, 2, 1, 2, 2, 2};
            GTabla tabla_1 = new GTabla(columnWidths_1);
            tabla_1.setWidthPercent(100);
            tabla_1.build(cab1);
            GTabla tabla_2 = new GTabla(columnWidths_2);
            tabla_2.setWidthPercent(100);
            tabla_2.build(cab2);
            
            m.agregarTabla(tabla_1);
            
            int data_length = cab2.length;
            String[] archivos_data = new String[data_length];
            for (int i = 0; i < data_length; i++) {
                archivos_data[i] = " ";
            }
            
            GCell[] cell = {tabla_2.createCellCenter(1, 1), tabla_2.createCellCenter(1, 1), tabla_2.createCellCenter(1, 1), tabla_2.createCellCenter(1, 1), tabla_2.createCellCenter(1, 1), tabla_2.createCellCenter(1, 1), tabla_2.createCellCenter(1, 1), tabla_2.createCellCenter(1, 1), tabla_2.createCellCenter(1, 1), tabla_2.createCellCenter(1, 1), tabla_2.createCellCenter(1, 1), tabla_2.createCellCenter(1, 1), tabla_2.createCellCenter(1, 1), tabla_2.createCellCenter(1, 1)};

            /*Llenado de la Data de los Bienes en las Tablas*/
            int size = bim.size();
            
            for (int i = 0; i < size; i++) {
                BienesInmuebles bienInmueble = bim.get(i);

                String index = Integer.toString(i); 
                String ubigeo = bienInmueble.getDepartamento() + bienInmueble.getProvincia() + bienInmueble.getDistrito();
                String direccion = bienInmueble.getTipo_dir() + ", " + bienInmueble.getBie_inm_dir();
                String dirNum = Integer.toString(bienInmueble.getDir_numero());
                String mz = bienInmueble.getDir_mz();
                String lte = Integer.toString(bienInmueble.getDir_lte());
                String propiedad = bienInmueble.getDtm().getDet_tec_prop();
                String tipoTerreno = bienInmueble.getDtm().getTip_terr();
                String saneado = bienInmueble.getDtm().getEst_sanea() + "";
                String area = Integer.toString(bienInmueble.getDtm().getArea());
                String uniArea = bienInmueble.getDtm().getUni_area();
                String regSinabip = bienInmueble.getDtm().getReg_sinabip();
                String partElec = bienInmueble.getDtm().getPart_elec();                
                String ficRegis = bienInmueble.getDtm().getNro_fic_reg();
                
                archivos_data[0] = index;          
                archivos_data[1] = ubigeo;  
                archivos_data[2] = direccion;  
                archivos_data[3] = dirNum; 
                archivos_data[4] = mz; 
                archivos_data[5] = lte; 
                archivos_data[6] = propiedad; 
                archivos_data[7] = tipoTerreno; 
                archivos_data[8] = saneado;
                archivos_data[9] = area;
                archivos_data[10] = uniArea;
                archivos_data[11] = regSinabip;
                archivos_data[12] = partElec;
                archivos_data[13] = ficRegis;

                tabla_2.processLineCell(archivos_data, cell);
            }
            
            m.agregarTabla(tabla_2);
            
            JSONArray miArray = new JSONArray();

            m.cerrarDocumento();
            JSONObject oResponse = new JSONObject();
            oResponse.put("datareporte", m.encodeToBase64());
            miArray.put(oResponse);

            return WebResponse.crearWebResponseExito("Se genero el Reporte con exito ...", miArray);
            
        } catch (Exception e) {
            
            System.out.println(e);
            return WebResponse.crearWebResponseError("ERROR AL GENERAR REPORTE ", e.getMessage());
        }
        
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

        
    
    
}
