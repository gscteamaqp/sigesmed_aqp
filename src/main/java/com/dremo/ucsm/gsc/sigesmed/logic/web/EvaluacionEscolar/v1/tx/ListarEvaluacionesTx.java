/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx.*;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.TareaEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.TareaEscolar;
import java.util.List;

/**
 *
 * @author abel
 */
public class ListarEvaluacionesTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int planID = 0;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            
            planID = requestData.getInt("planID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listar las tareas por plan de estudios", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<TareaEscolar> tareas = null;
        TareaEscolarDao tareaDao = (TareaEscolarDao)FactoryDao.buildDao("web.TareaEscolarDao");
        try{
            tareas = tareaDao.buscarPorPlanEstudios(planID);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las tareas por plan de estudios \n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar las tareas por plan de estudios", e.getMessage() );
        }
        //Fin        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        int i = 0;
        for(TareaEscolar tarea:tareas ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("tareaID",tarea.getTarEscId() );
            oResponse.put("nombre",tarea.getNom());
            oResponse.put("descripcion",tarea.getDes());
            oResponse.put("numeroDoc",tarea.getNumDoc());
            oResponse.put("adjunto",tarea.getDocAdj());
            
            oResponse.put("planID",tarea.getPlaEstId());
            oResponse.put("gradoID",tarea.getGraId());
            oResponse.put("seccionID",""+tarea.getSecId());
            oResponse.put("areaID",tarea.getAreCurId());
            
            oResponse.put("estado",""+tarea.getEstado());
            
            oResponse.put("fechaEnvio",tarea.getFecEnv());
            oResponse.put("fechaEntrega",tarea.getFecEnt());
            
            oResponse.put("i",i++);
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}

