/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.configuracion_patrimonial.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.ControlPatrimonialDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.ControlPatrimonial;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONObject;

/**
 *
 * @author harold
 */
public class EliminarControlPatrimonialTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {

        JSONObject requestData = (JSONObject) wr.getData();
        ControlPatrimonial cp = null;
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {

            int cp_id = requestData.getInt("cp_id");
            // cp = new ControlPatrimonial();

            ControlPatrimonialDAO cp_dao = (ControlPatrimonialDAO) FactoryDao.buildDao("scp.ControlPatrimonialDAO");

            cp_dao.eliminarControlPatrimonial(cp_id);

            System.out.print("Control Patrimonial Eliminado : " + cp_id);

        } catch (Exception e) {
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Actualizar el Control Patrimonial , datos incorrectos", e.getMessage());
        }

        return WebResponse.crearWebResponseExito("La actualizacion  del Control Patrimonial , se realizo correctamente");

        //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
