package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.agregar_estudiante.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.GenericMMIDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.Pais;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class ListarPaisesTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        GenericMMIDaoHibernate<Pais> hb = new GenericMMIDaoHibernate<>();
        JSONArray paises = new JSONArray();

        List paisesSQL;

        try {
            paisesSQL = hb.buscarTodos(Pais.class);

            for (Object obj : paisesSQL) {
                Pais pais = (Pais) obj;
                JSONObject temp = new JSONObject();
                temp.put("paiId", pais.getPaiId());
                temp.put("paiNom", pais.getPaiNom());
                paises.put(temp);
            }

        } catch (Exception e) {
            return WebResponse.crearWebResponseError("Error al cargar los paises! ", e.getMessage());
        }

        return WebResponse.crearWebResponseExito("Listaron los paises", paises);
    }

}
