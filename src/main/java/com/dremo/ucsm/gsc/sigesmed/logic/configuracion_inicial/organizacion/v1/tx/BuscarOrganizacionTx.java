/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.organizacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;

/**
 *
 * @author Administrador
 */
public class BuscarOrganizacionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        int organizacionID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            organizacionID = requestData.getInt("organizacionID");
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("NO se puedo Buscar datos de entrada incorrectos"); //, e.getMessage() );
        }
        /*
        *  Parte para la operacion en la Base de Datos
        */
        Organizacion organizacion = null;
        OrganizacionDao moduloDao = (OrganizacionDao)FactoryDao.buildDao("OrganizacionDao");
        try{
            organizacion = moduloDao.buscarConTipoOrganizacionYPadre(organizacionID);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las Organizaciones\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar las Organizaciones", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        if(organizacion!=null){
            JSONObject oResponse = new JSONObject();
            oResponse.put("organizacionID",organizacion.getOrgId() );
            oResponse.put("tipoOrganizacionID",organizacion.getTipoOrganizacion().getTipOrgId() );
            oResponse.put("tipoOrganizacion",organizacion.getTipoOrganizacion().getNom() );
            if(organizacion.getOrganizacionPadre()!=null)
                oResponse.put("organizacionPadreID",organizacion.getOrganizacionPadre().getOrgId() );
            else
                oResponse.put("organizacionPadreID",0 );
            oResponse.put("codigo",organizacion.getCod());
            oResponse.put("nombre",organizacion.getNom());
            oResponse.put("alias",organizacion.getAli());
            oResponse.put("descripcion",organizacion.getDes());
            oResponse.put("direccion",organizacion.getDir());
            oResponse.put("fecha",organizacion.getFecMod().toString());
            oResponse.put("estado",""+organizacion.getEstReg());
            oResponse.put("organizacionImgInst", "" + organizacion.getOrgImgInst());

            return WebResponse.crearWebResponseExito("Se Listo correctamente",oResponse);
        }
        return WebResponse.crearWebResponseError("No se encontro la organizacion");
        //Fin
    }
    
}

