/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.demeritos.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.demeritos.v1.tx.*;

/**
 *
 * @author Yemi
 */
public class ComponentRegister implements IComponentRegister{

    @Override
    public WebComponent createComponent() {
        WebComponent seComponent = new WebComponent(Sigesmed.MODULO_ESCALAFON);

        seComponent.setName("demeritos");
        seComponent.setVersion(1);
        
        seComponent.addTransactionGET("listarDemeritos", ListarDemeritosTx.class);
	seComponent.addTransactionPOST("agregarDemerito", AgregarDemeritoTx.class);
        seComponent.addTransactionPOST("actualizarDemerito", ActualizarDemeritoTx.class);
        
        return seComponent;
    }
    
}
