/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.ma.notas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.ConfiguracionNotaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.NotaEvaluacionIndicadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.RegistroAuxiliarDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.DocenteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.IndicadorAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ConfiguracionNota;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.RegistroAuxiliar;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import static com.dremo.ucsm.gsc.sigesmed.logic.ma.notas.v1.tx.MA_Constantes.*;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class RegistrarPromedioIndicadoresTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(RegistrarPromedioIndicadoresTx.class.getName());
    List<ConfiguracionNota> config_docente = null;
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idSession = data.getInt("ses");
        char idPeriodo  = data.getString("per").charAt(0);
        int numPer  = data.getInt("numper");
        int idArea = data.getInt("are");
        int org = data.getInt("org");
        int idUsr = data.getInt("usr");
        JSONArray array = data.getJSONArray("estudiantes");
        listarConfiguracionDocente(org,idUsr);
        return registrarNotas(idUsr,idSession,idPeriodo,numPer, idArea,array);
    }
    
    private WebResponse registrarNotas(int idUsr, int idSession, char idPeriodo, int numPer, int idArea, JSONArray array) {
        try{
            RegistroAuxiliarDao regisDao = (RegistroAuxiliarDao) FactoryDao.buildDao("ma.RegistroAuxiliarDao");
            NotaEvaluacionIndicadorDao notaDao = (NotaEvaluacionIndicadorDao) FactoryDao.buildDao("ma.NotaEvaluacionIndicadorDao");
            IndicadorAprendizajeDao indicadorDao = (IndicadorAprendizajeDao) FactoryDao.buildDao("maestro.plan.IndicadorAprendizajeDao");
            DocenteDao docDao = (DocenteDao) FactoryDao.buildDao("maestro.DocenteDao");
            EstudianteDao estDao = (EstudianteDao) FactoryDao.buildDao("maestro.EstudianteDao");
            
            String mensaje = "";
            for(int i = 0; i < array.length(); i++){
                JSONObject estudiante = array.getJSONObject(i);
                int idGraOrEst = estudiante.getInt("idgra");
                Docente docente = docDao.buscarDocentePorUsuario(idUsr);
                JSONArray indicadoresdeSesion = notaDao.buscarIndicadoresxSesionxEstudiante(idSession, idGraOrEst, idPeriodo, numPer);
        
                for(int j = 0; j < indicadoresdeSesion.length(); j++){
                    int idInd = indicadoresdeSesion.getJSONObject(j).getInt("indId");
                    JSONArray notas = notaDao.buscarNotasxIndicadorxEstudiante(idInd, idGraOrEst , idPeriodo, numPer);
                    Double promedio = 0.0;
                    
                    for(int k = 0; k < notas.length(); k++){
                            promedio += Double.parseDouble(notas.getJSONObject(k).getString("nota"));
                    }
                    promedio = promedio/notas.length();
                    promedio = (Math.round(promedio * 10.0) / 10.0);
                    String nota = String.valueOf(promedio);
                    String nota_lit = obtenerNotaLiteral(promedio);

                    if(!regisDao.buscarNotaFinalIndicador(idInd, docente.getDoc_id(), numPer, idArea, idGraOrEst)){
                        RegistroAuxiliar regAux = new RegistroAuxiliar(nota);
                        regAux.setNotIndDoc(nota);
                        regAux.setNot_ind_lit(nota_lit);
                        regAux.setNot_ind_lit_doc(nota_lit);
                        regAux.setIndicador(indicadorDao.buscarPorId(idInd));
                        regAux.setPeriodo(regisDao.buscarPeriodoPorId(numPer));
                        regAux.setDocente(docente);
                        regAux.setArea(docDao.buscarAreaPorId(idArea));
                        regAux.setGradoEstudiante(estDao.buscarGradoIEEstudiante(idGraOrEst));
                        regisDao.insert(regAux);
                        mensaje = "Se registro el promedio correctamente";
                        
                    }else {
                        RegistroAuxiliar regAux = regisDao.buscarNotaEstudiante(idInd, docente.getDoc_id(), numPer, idArea, idGraOrEst);
                        regAux.setNotInd(nota);
                        regAux.setNotIndDoc(nota);
                        regAux.setNot_ind_lit(nota_lit);
                        regAux.setNot_ind_lit_doc(nota_lit);
                        regAux.setFecMod(new Date());
                        regisDao.update(regAux);
                        mensaje = "Se actualizo el promedio correctamente";
                    }
                }   
            }
            return WebResponse.crearWebResponseExito(mensaje);
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarPromedioIndicadores",e);
            return WebResponse.crearWebResponseError("No se pudo registrar el promedio de los indicadores");
        }
    }
    public String obtenerNotaLiteral(Double not){
        
        for(ConfiguracionNota conf : config_docente){
            if( (int)Math.round(not) >= conf.getConf_not_min() && (int)Math.round(not) <= conf.getConf_not_max()){
                return conf.getConf_cod();
            }
        }
        return "";

    }
    
    public void listarConfiguracionDocente(int org_id , int idUser){
        ConfiguracionNotaDao configNot = (ConfiguracionNotaDao) FactoryDao.buildDao("ma.ConfiguracionNotaDao");
        config_docente = configNot.listarConfiguracionDocente(idUser, org_id);

    }

   
    
    
}
