/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.sub_modulo_sistema.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.SubModuloSistemaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ModuloSistema;
import com.dremo.ucsm.gsc.sigesmed.core.entity.SubModuloSistema;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;

/**
 *
 * @author Administrador
 */
public class InsertarSubModuloSistemaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        SubModuloSistema nuevoSubModulo = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            int moduloID = requestData.getInt("moduloID");
            String codigo = requestData.getString("codigo");
            String nombre = requestData.getString("nombre");
            String descripcion = requestData.getString("descripcion");
            String icono = requestData.getString("icono");
            String estado = requestData.getString("estado");
            nuevoSubModulo = new SubModuloSistema(0,new ModuloSistema(moduloID), codigo, nombre, descripcion,icono, new Date(), 1, estado.charAt(0), null);
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        SubModuloSistemaDao subModuloDao = (SubModuloSistemaDao)FactoryDao.buildDao("SubModuloSistemaDao");
        try{
            subModuloDao.insert(nuevoSubModulo);
        
        }catch(Exception e){
            System.out.println("No se pudo registrar el Sub Modulo\n"+e);
            return WebResponse.crearWebResponseError("No se pudo registrar el Sub Modulo ", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("subModuloID",nuevoSubModulo.getSubModSisId());
        oResponse.put("fecha",nuevoSubModulo.getFecMod().toString());
        return WebResponse.crearWebResponseExito("El registro Sub Modulo se realizo correctamente", oResponse);
        //Fin
    }
    
}
