/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.exposicion_ponencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ExposicionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Exposicion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class AgregarExposicionTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(AgregarExposicionTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
         /*
        *   Parte para la lectura, verificacion y validacion de datos
         */
        Exposicion expPon = null;
        
        Integer ficEscId = 0;

        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat sdo = new SimpleDateFormat("dd/MM/yyyy");

        try {
            JSONObject requestData = (JSONObject) wr.getData();

            ficEscId = requestData.getInt("ficEscId");
            String des = requestData.getString("des");
            String insOrg = requestData.getString("insOrg");
            String tipPar = requestData.getString("tipPar");
            Date fecIni = sdi.parse(requestData.getString("fecIni").substring(0, 10));
            Date fecTer = sdi.parse(requestData.getString("fecTer").substring(0, 10));
            Integer horLec = requestData.getInt("horLec");
            
            expPon = new Exposicion(new FichaEscalafonaria(ficEscId), des, insOrg, tipPar, fecIni, fecTer, horLec, wr.getIdUsuario(), new Date(), 'A');

        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE,"Datos nueva exposicion",e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }
        //Fin

        /*
        *  Parte para la operacion en la Base de Datos
         */
        //si el pariente no existe en la tabla persona
        ExposicionDao expPonDao = (ExposicionDao) FactoryDao.buildDao("se.ExposicionDao");
        try {
            expPonDao.insert(expPon);
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar nueva exposicion",e);
            System.out.println(e);
        }
        
        //Fin       

        /*
        *  Repuesta Correcta
         */
        JSONObject oResponse = new JSONObject();
        oResponse.put("expId", expPon.getExpId());
        oResponse.put("des", expPon.getDes());
        oResponse.put("insOrg", expPon.getInsOrg());
        oResponse.put("tipPar", expPon.getTipPar());
        oResponse.put("fecIni", sdo.format(expPon.getFecIni()));
        oResponse.put("fecTer", sdo.format(expPon.getFecTer()));
        oResponse.put("horLec", expPon.getHorLec());
                
        return WebResponse.crearWebResponseExito("El registro de la exposcion se realizo correctamente", oResponse);
        //Fin
    }
    
}
