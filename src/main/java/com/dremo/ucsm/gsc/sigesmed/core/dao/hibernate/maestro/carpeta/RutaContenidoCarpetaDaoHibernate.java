package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.maestro.carpeta;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.carpeta.RutaContenidoCarpetaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.RutaContenidoCarpeta;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 * Created by Administrador on 30/12/2016.
 */
public class RutaContenidoCarpetaDaoHibernate extends GenericDaoHibernate<RutaContenidoCarpeta> implements RutaContenidoCarpetaDao {
    @Override
    public RutaContenidoCarpeta buscarRutaPorId(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(RutaContenidoCarpeta.class)
                    .add(Restrictions.eq("rutConCarId",id));
            return (RutaContenidoCarpeta) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
}
