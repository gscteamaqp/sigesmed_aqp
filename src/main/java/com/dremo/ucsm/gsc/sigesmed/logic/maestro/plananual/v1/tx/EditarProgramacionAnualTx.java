package com.dremo.ucsm.gsc.sigesmed.logic.maestro.plananual.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.ProgramacionAnualDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.UnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.MaterialRecursoProgramacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.ProgramacionAnual;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import org.json.JSONObject;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 16/12/2016.
 */
public class EditarProgramacionAnualTx implements ITransaction {
    private static Logger logger = Logger.getLogger(EditarProgramacionAnualTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idPlan = data.getInt("cod");
        return editarProgramacionAnual(idPlan, data);
    }

    private WebResponse editarProgramacionAnual(int idPlan, JSONObject data) {
        try{
            ProgramacionAnualDao progDao = (ProgramacionAnualDao) FactoryDao.buildDao("maestro.plan.ProgramacionAnualDao");
            ProgramacionAnual prog = progDao.buscarProgramacionConObjetivos(idPlan);
            if(prog != null){
                int idArea = data.getJSONObject("are").getInt("id");
                int idGrado = data.getJSONObject("grad").getInt("id");
                String strFec = data.getString("fec");
                SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
                Date fec = sf.parse(strFec);

                //prog.setFecProAnu(new Date(data.optLong("fec",prog.getFecProAnu().getTime())));
                prog.setDes(data.optString("nom",prog.getDes()));
                prog.setAre(progDao.buscarArea(idArea));
                prog.setGra(progDao.buscarGrado(idGrado));
                prog.setFechProAnu(fec);

                progDao.update(prog);
                return WebResponse.crearWebResponseExito("Se edito correctamente la programacion anual");
            }else{
                return WebResponse.crearWebResponseError("no se puede encontrar programacion anual");
            }
        }catch (Exception e){
            logger.log(Level.SEVERE,"editarProgramacionAnual",e);
            return WebResponse.crearWebResponseError("error al editar programacion anual");
        }
    }
}
