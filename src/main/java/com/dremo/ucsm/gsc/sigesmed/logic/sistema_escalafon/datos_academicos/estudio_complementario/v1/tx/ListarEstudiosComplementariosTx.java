/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.estudio_complementario.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.EstudioComplementarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.EstudioComplementario;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class ListarEstudiosComplementariosTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ListarEstudiosComplementariosTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        JSONObject requestData = (JSONObject)wr.getData();
        Integer ficEscId = requestData.getInt("ficEscId");
                
        List<EstudioComplementario> estCom = null;
        EstudioComplementarioDao estComDao = (EstudioComplementarioDao)FactoryDao.buildDao("se.EstudioComplementarioDao");
        
        try{
            estCom = estComDao.listarxFichaEscalafonaria(ficEscId);
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar estudios complementarios",e);
            System.out.println("No se pudo listar los estudios complementarios\n"+e);
            return WebResponse.crearWebResponseError("No se pudo listar los estudios complementarios", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(EstudioComplementario ec:estCom ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("estComId", ec.getEstComId());
            oResponse.put("tip", ec.getTip());
            oResponse.put("des", ec.getDes());
            oResponse.put("niv", ec.getNiv());
            oResponse.put("insCer", ec.getInsCer());
            oResponse.put("tipPar", ec.getTipPar());
            oResponse.put("fecIni", ec.getFecIni());
            oResponse.put("fecTer", ec.getFecTer());
            oResponse.put("horLec", ec.getHorLec());
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Los estudios complementarios fueron listados exitosamente", miArray);
    
    }
    
}
