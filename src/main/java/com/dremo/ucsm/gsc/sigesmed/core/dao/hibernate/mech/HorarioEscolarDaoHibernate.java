/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mech;


import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.HorarioEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.DistribucionHoraGrado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.HorarioDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.HorarioDetalleModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.HorarioEscolar;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author abel
 */
public class HorarioEscolarDaoHibernate extends GenericDaoHibernate<HorarioEscolar> implements HorarioEscolarDao{

   @Override
    public HorarioEscolar buscarHorario(int planNivelID,int gradoID,char seccionID){
        
        HorarioEscolar objeto = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{            
            String hql = "SELECT DISTINCT h FROM HorarioEscolar h LEFT JOIN FETCH h.detalles WHERE h.plaNivId=:p1 and h.graId=:p2 and h.secId=:p3 ";
        
            Query query = session.createQuery(hql);
            query.setParameter("p1", planNivelID );
            query.setParameter("p2", gradoID );
            query.setParameter("p3", seccionID );
            query.setMaxResults(1);
            //buscando 
            objeto =  (HorarioEscolar)query.uniqueResult(); 
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar el horario para esta seccion\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar el horario para esta seccion\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objeto;
    }
    @Override
    public List<Integer> buscarAulasDisponibles(int horarioID,char diaID,Date horaInicio, Date horaFin){
        List<Integer> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{            
            String hql = "SELECT hd.aulId FROM HorarioDetalle hd WHERE hd.horarioId=:p1 and hd.diaId=:p2 and (hd.horIni >=:p4 or hd.horFin <=:p3 )";
        
            Query query = session.createQuery(hql);
            query.setParameter("p1", horarioID );
            query.setParameter("p2", diaID );
            query.setParameter("p3", horaInicio );
            query.setParameter("p4", horaFin );
            query.setMaxResults(1);
            //buscando 
            objetos =  query.list(); 
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar las aulas Disponibles  para el horario\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar las aulas Disponibles  para el horario\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public List<DistribucionHoraGrado> buscarDocentesPorIntervaloHora(int planID,char diaID,Date horaInicio, Date horaFin){
        
        List<DistribucionHoraGrado> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{            
            String hql = "SELECT DISTINCT dg FROM DistribucionHoraGrado dg LEFT JOIN FETCH dg.horarios AS hd WHERE dg.plaEstId=:p1 and hd.diaId=:p3 and hd.horIni >=:p4 and hd.horFin <=:p5";
            
            Query query = session.createQuery(hql);
            query.setParameter("p1", planID );
            query.setParameter("p3", diaID );
            query.setParameter("p4", horaInicio );
            query.setParameter("p5", horaFin );
            query.setMaxResults(1);
            //buscando 
            objetos =  query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar las plazas magisteriales en el intervalo de hora\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar las plazas magisteriales en el intervalo de hora\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public List<HorarioDetalleModel> buscarDistribucionPlazasEnHorario(int planId){
        List<HorarioDetalleModel> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.mech.HorarioDetalleModel(d.horario.graId,d.horario.secId,d.horDetId,d.horIni,d.horFin,d.horario.horarioId,d.plaMagId,d.areCurId,d.aulId,d.diaId) FROM HorarioDetalle d JOIN d.horario WHERE d.horario.planNivel.plaEstId =:p1 ORDER BY d.diaId";
            Query query = session.createQuery(hql);
            query.setParameter("p1", planId );
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar la distribucion de plazas en el horario\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar la distribucion de plazas en el horario\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public List<HorarioDetalleModel> buscarDistribucionPorDocente(int docenteId){
        List<HorarioDetalleModel> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.mech.HorarioDetalleModel(d.horario.graId,d.horario.secId,d.horDetId,d.horIni,d.horFin,d.horario.horarioId,d.plaMagId,d.areCurId,d.aulId,d.diaId) FROM HorarioDetalle d JOIN d.horario WHERE d.plazaMagisterial.docId =:p1 ORDER BY d.diaId";
            Query query = session.createQuery(hql);
            query.setParameter("p1", docenteId );
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar la distribucion horaria por docente\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar la distribucion horaria por docente\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public List<DistribucionHoraGrado> buscarDocentesConHorario(int planId){
        List<DistribucionHoraGrado> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT DISTINCT p FROM DistribucionHoraGrado dg, PlazaMagisterial p LEFT JOIN FETCH p.horarios WHERE dg.plaEstId=:p1 ORDER BY dg.graId";
            Query query = session.createQuery(hql);
            query.setParameter("p1", planId );
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar la distribucion del horario por plan de estudios\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar la distribucion del horario por plan de estudios\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    
    @Override
    public void mergeHorarioDetalle(HorarioDetalle detalle){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            if(detalle.getHorDetId()==0)
                session.persist(detalle);
            else
                session.update(detalle);
            miTx.commit();        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo realizar la persistencia en Horario Detalle\\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo realizar la persistencia en Horario Detalle\\n"+ e.getMessage());
        }
        finally{
            session.close();
        }
    }
    @Override
    public void eliminarHorarioDetalle(int horarioDetalleID){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            //eliminando tipo tramite
            String hql = "DELETE FROM HorarioDetalle WHERE horDetId =:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", horarioDetalleID);
            query.executeUpdate();    
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo eliminar el horario detalle\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
}
