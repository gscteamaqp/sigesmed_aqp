package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.ma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.RegistroAuxiliarDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.HistoricoNotasEstudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.RegistroAuxiliar;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.RegistroAuxiliarCompetencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.PeriodosPlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CompetenciaAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.IndicadorAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.IndicadoresSesionAprendizaje;
import org.hibernate.*;

import java.util.List;
import org.hibernate.criterion.CriteriaSpecification;

/**
 * Created by Administrador on 25/01/2017.
 */
public class RegistroAuxiliarDaoHibernate extends GenericDaoHibernate<RegistroAuxiliar> implements RegistroAuxiliarDao {
    @Override
    public List<CompetenciaAprendizaje> listarCompetenciasPeriodo(int idPeriodo, int idArea, int idOrg, int idDoc, int idGrado, int idPlan) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT DISTINCT comp FROM CompetenciaAprendizaje comp" +
                    " INNER JOIN comp.capacidades cap INNER JOIN cap.competeciasUnidad compUni" +
                    " INNER JOIN compUni.unidad u INNER JOIN u.programacionAnual pa" +
                    " WHERE pa.planEstudios.plaEstId =:idPlan AND pa.are.areCurId =:idAre" +
                    " AND pa.org =:idOrg AND pa.doc.doc_id =:idDoc AND pa.gra.graId =:idGra AND u.periodo.perPlaEstId =:idPer";
            Query query = session.createQuery(hql);
            query.setInteger("idPlan",idPlan);
            query.setInteger("idAre",idArea);
            query.setInteger("idOrg",idOrg);
            query.setInteger("idDoc",idDoc);
            query.setInteger("idGra",idGrado);
            query.setInteger("idPer",idPeriodo);
            query.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<IndicadorAprendizaje> listarIndicadoresPeriodo(int idPer, int idorg, int idDoc, int idPLan, int idGra, int idAre, int idComp) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT DISTINCT i.* FROM pedagogico.indicador_aprendizaje i" +
                    "  INNER JOIN pedagogico.indicadores_unidad_aprendizaje iu ON i.ind_apr_id = iu.ind_apr_id" +
                    "  INNER JOIN pedagogico.unidad_didactica u ON iu.uni_did_id = u.uni_did_id" +
                    "  INNER JOIN pedagogico.programacion_anual pa ON u.pro_anu_id = pa.pro_anu_id" +
                    "  WHERE u.per_pla_est_id =:idPer AND pa.pla_est_id =:plaEst AND pa.doc_id =:idDoc" +
                    "  AND pa.org_id =:idOrg AND pa.gra_id =:idGra AND pa.are_cur_id =:idArea AND iu.com_id =:idComp";
            SQLQuery query = session.createSQLQuery(hql);
            query.addEntity(IndicadorAprendizaje.class);
            //query.addFetch("c","i","cap");
            query.setInteger("idPer",idPer);
            query.setInteger("plaEst",idPLan);
            query.setInteger("idDoc",idDoc);
            query.setInteger("idOrg",idorg);
            query.setInteger("idGra",idGra);
            query.setInteger("idArea",idAre);
            query.setInteger("idComp",idComp);
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public RegistroAuxiliar buscarNotaEstudiante(int idIndicador, int idDoc, int idPer, int idArea, int idMatGra) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT r FROM RegistroAuxiliar r" +
                    " WHERE r.indicador.indAprId =:idInd AND r.docente.doc_id =:docId" +
                    " AND r.periodo.perPlaEstId =:idPer AND r.area.areCurId =:areId AND r.gradoEstudiante.graOrgEstId =:idGraMa ";
            Query query = session.createQuery(hql);
            query.setInteger("idInd",idIndicador);
            query.setInteger("docId",idDoc);
            query.setInteger("idPer",idPer);
            query.setInteger("areId",idArea);
            query.setInteger("idGraMa",idMatGra);
            query.setMaxResults(1);
            return (RegistroAuxiliar) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public PeriodosPlanEstudios buscarPeriodoPorId(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT p FROM PeriodosPlanEstudios p WHERE p.perPlaEstId =:id ";
            Query query = session.createQuery(hql);
            query.setInteger("id",id);
            return (PeriodosPlanEstudios) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public RegistroAuxiliarCompetencia buscarNotaCompetenciaEsp(int idComp, int idArea,int idPer, int idAlum) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT r FROM RegistroAuxiliarCompetencia r" +
                    " WHERE r.comp.comId =:idComp AND r.areaCurricular.areCurId =:idAre AND r.gradoEst.graOrgEstId =:idEstMat AND r.periodoPlanEstudios =:idPer ";
            Query query = session.createQuery(hql);
            query.setInteger("idComp",idComp);
            query.setInteger("idAre",idArea);
            query.setInteger("idEstMat",idAlum);
            query.setInteger("idPer",idPer);
            query.setMaxResults(1);
            return (RegistroAuxiliarCompetencia) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public HistoricoNotasEstudiante buscaHistoricoNotasEstudiante(int idArea, int idPer, int idAlum) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT h FROM HistoricoNotasEstudiante h" +
                    " WHERE h.areaCurricular.areCurId =:idAre AND h.gradoEst.graOrgEstId =:idEstMat AND h.periodos.perPlaEstId =:idPer ";
            Query query = session.createQuery(hql);
            query.setInteger("idAre",idArea);
            query.setInteger("idEstMat",idAlum);
            query.setInteger("idPer",idPer);
            query.setMaxResults(1);
            return (HistoricoNotasEstudiante) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public RegistroAuxiliarCompetencia buscarNotaCompetencia(int idNot) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT rac FROM RegistroAuxiliarCompetencia  rac WHERE rac.regAuxComId=:id ";
            Query query = session.createQuery(hql);
            query.setInteger("id",idNot);
            return (RegistroAuxiliarCompetencia) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public void registrarNotaCompetencia(RegistroAuxiliarCompetencia reg) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
           session.saveOrUpdate(reg);
            tx.commit();
        }catch (Exception e){
            tx.rollback();
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public void guardarActualizarNotaArea(HistoricoNotasEstudiante historico) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            session.saveOrUpdate(historico);
            tx.commit();
        }catch (Exception e){
            tx.rollback();
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public Boolean buscarNotaFinalIndicador(int idIndicador, int idDoc, int idPer, int idArea, int idMatGra) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT r FROM RegistroAuxiliar r" +
                    " WHERE r.indicador.indAprId =:idInd AND r.docente.doc_id =:docId" +
                    " AND r.periodo.perPlaEstId =:idPer AND r.area.areCurId =:areId AND r.gradoEstudiante.graOrgEstId =:idGraMa ";
            Query query = session.createQuery(hql);
            query.setInteger("idInd",idIndicador);
            query.setInteger("docId",idDoc);
            query.setInteger("idPer",idPer);
            query.setInteger("areId",idArea);
            query.setInteger("idGraMa",idMatGra);
            query.setMaxResults(1);
            
            Boolean resultado = false;
            if (query.uniqueResult() != null)
                resultado = true;
            return resultado;
            
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public Boolean buscarNotaFinalCompetencia(int idComp, int idArea, int idPer, int idAlum) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT r FROM RegistroAuxiliarCompetencia r" +
                    " WHERE r.comp.comId =:idComp AND r.areaCurricular.areCurId =:idAre AND r.gradoEst.graOrgEstId =:idEstMat AND r.periodoPlanEstudios =:idPer ";
            Query query = session.createQuery(hql);
            query.setInteger("idComp",idComp);
            query.setInteger("idAre",idArea);
            query.setInteger("idEstMat",idAlum);
            query.setInteger("idPer",idPer);
            query.setMaxResults(1);
            
            Boolean resultado = false;
            if (query.uniqueResult() != null)
                resultado = true;
            return resultado;
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
    
    @Override
    public Boolean buscarNotaAreaEnPeriodo(int idArea, int idPer, int idAlum) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT h FROM HistoricoNotasEstudiante h" +
                    " WHERE h.areaCurricular.areCurId =:idAre AND h.gradoEst.graOrgEstId =:idEstMat AND h.periodos.perPlaEstId =:idPer ";
            Query query = session.createQuery(hql);
            query.setInteger("idAre",idArea);
            query.setInteger("idEstMat",idAlum);
            query.setInteger("idPer",idPer);
            query.setMaxResults(1);
            
            Boolean resultado = false;
            if (query.uniqueResult() != null)
                resultado = true;
            return resultado;
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<IndicadoresSesionAprendizaje> obtenerCompetenciasDeSesion(int sesId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String sql = "SELECT indSesApr FROM IndicadoresSesionAprendizaje as indSesApr" +
                    " join fetch indSesApr.sesion as ses" +
                    " join fetch indSesApr.competencia as com" + 
                    " WHERE ses.sesAprId=:sesId AND ses.estReg = 'A'";
            Query query = session.createQuery(sql);
            query.setInteger("sesId",sesId);
            
            List<IndicadoresSesionAprendizaje> indicadorSesApr = query.list();
            return indicadorSesApr;
            
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
    
    @Override
    public List<RegistroAuxiliarCompetencia> buscarNotaCompetenciasEstudiante(int grad_ie_est, int area, int periodo) {
        
         Session session = HibernateUtil.getSessionFactory().openSession();
         try{
             String hql = "SELECT rac FROM RegistroAuxiliarCompetencia rac WHERE rac.gra_ie_est_id=:p1  AND  rac.are_cur_id=:p2  AND  rac.per_eva_id=:p3";
             Query query = session.createQuery(hql);
             query.setParameter("p1",grad_ie_est);
             query.setParameter("p2",area);
             query.setParameter("p3",periodo);
             List<RegistroAuxiliarCompetencia> reg_aux = query.list();
             return reg_aux;
         }
         catch(Exception e){
              throw e;
         }finally {
            session.close();
        }
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
