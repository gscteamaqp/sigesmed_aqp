/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.LibroCajaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentaCorriente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentasEfectivo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.LibroCaja;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.Tesorero;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author RE
 */
public class EstadoLibroCajaTx implements ITransaction{
    
     @Override
    public WebResponse execute(WebRequest wr){
        //Parte de la operacion con la Base de Datos
        
        LibroCaja libro=null;
        int organizacionID = 0;  
      
        
        List<CuentasEfectivo> cuentasEfectivo=null;
        List<CuentaCorriente> cuentasCorrientes=null;
        List<Tesorero> tesoreros=null;
       
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();           
            organizacionID = requestData.getInt("organizacionID");
           
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo obtener la organizacion a la que pertenece", e.getMessage() );
        }
        
        LibroCajaDao libroDao = (LibroCajaDao)FactoryDao.buildDao("sci.LibroCajaDao");
              
        try {                        
            libro= libroDao.estadoLibroCaja(organizacionID);
            
            if (libro!=null){                                               
                cuentasEfectivo = libroDao.buscarCuentasEfectivo(libro);
                cuentasCorrientes = libroDao.buscarCuentasCorrientes(libro);
                if(!libro.getEstReg().equals('C')){
                    tesoreros = libroDao.buscarTesoreros(libro);
                }
                    
            }
            
        } catch (Exception e) {
            System.out.println("No se pudo encontrar datos del Libro \n"+e);
            return WebResponse.crearWebResponseError("No se pudo encontrar datos del Libro", e.getMessage() );
        }
        /*
        *  Repuesta Correcta
        */ 
        DateFormat fechaHora = new SimpleDateFormat("yyyy/MM/dd");
          
        JSONObject oResponse = new JSONObject();
             
          
            
        if(libro!=null && !libro.getEstReg().equals('C')){
            
            oResponse.put("libroID",libro.getLibCajId());
            oResponse.put("nombre",libro.getNom());
            oResponse.put("observacion",libro.getObs());
            oResponse.put("fechaA",fechaHora.format(libro.getFecApe())); 
            oResponse.put("fechaC",fechaHora.format(libro.getFecCie()));  
            oResponse.put("saldoActual",libro.getSalAct());   
            oResponse.put("saldoApertura",libro.getSalApe());   
            oResponse.put("estado",""+libro.getEstReg());
            oResponse.put("organizacionID",organizacionID);
            
                JSONArray cuentasE = new JSONArray();                              
                    for(CuentasEfectivo cE:cuentasEfectivo){
                         JSONObject cuenta = new JSONObject();
                        cuenta.put("nombre",cE.getCuenta().getNomCue());
                        cuenta.put("numero",cE.getCuenta().getNumCue());
                        cuenta.put("importe",cE.getSalAct());
                        for(CuentaCorriente cC:cuentasCorrientes){
                            if(cC.getCuenta().getCueConId()==cE.getCuenta().getCueConId()){
                                cuenta.put("subClase",cC.getCuenta().getSubCla());
                                cuenta.put("banco",cC.getBanco().getEmpId());
                                cuenta.put("num",cC.getNum()); 
                            }
                        }
                         cuentasE.put(cuenta);
                    }                                                
            
            oResponse.put("cuentasEfectivo",cuentasE);  
            
            
                JSONArray tesorerosA = new JSONArray();                              
                    for(Tesorero t:tesoreros){
                         JSONObject tes = new JSONObject();
                        tes.put("personaID", t.getPersona().getPerId());
                        tes.put("datos",t.getPersona().getNombrePersona());
                        tes.put("fechaInicio",t.getFecIni());
                        tes.put("fechaCierre",t.getFecFin());
                        tes.put("nomDocAdj", t.getNomDocAdj());
                        tes.put("rd", t.getNumRes());
                        tes.put("url",Sigesmed.UBI_ARCHIVOS+"/contable/");
                        
                   
                        tesorerosA.put(tes);
                    }

            oResponse.put("tesoreros",tesorerosA);  

            }
        
        else if(libro!=null) {
            JSONArray cuentasE = new JSONArray();
            for(CuentasEfectivo cE:cuentasEfectivo){
              JSONObject cuenta = new JSONObject();
                cuenta.put("nombre",cE.getCuenta().getNomCue());
                cuenta.put("numero",cE.getCuenta().getNumCue());
                cuenta.put("importe",cE.getSalAct());
                for(CuentaCorriente cC:cuentasCorrientes){
                    if(cC.getCuenta().getCueConId()==cE.getCuenta().getCueConId()){
                        cuenta.put("flag",true);  
                        cuenta.put("banco",cC.getBanco().getRazSoc());  
                        cuenta.put("numeroCuenta",cC.getNum()); 
                    }
                    else{
                        cuenta.put("flag",false); 
                    }
                }
                 cuentasE.put(cuenta);
            }
            
             oResponse.put("saldo",libro.getSalAct()); 
             oResponse.put("cuentasEfectivo",cuentasE); 
             oResponse.put("estado","C");
        }
        
        else
            oResponse.put("estado","N");
            
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",oResponse);        
        //Fin
    }
    
}
