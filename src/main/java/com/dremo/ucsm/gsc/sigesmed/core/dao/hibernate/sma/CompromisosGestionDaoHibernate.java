/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.*;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.CompromisosGestion;

import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.Items;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Administrador
 */
public class CompromisosGestionDaoHibernate extends GenericDaoHibernate<CompromisosGestion> implements CompromisosGestionDao{
    @Override
    public String buscarUltimoCodigo() {
        String codigo = null;
        return codigo;
    }
    @Override
    public CompromisosGestion obtenerPlantillaCompromisos(int gruId){
        Session session = HibernateUtil.getSessionFactory().openSession();
        CompromisosGestion compromisos= null;
        Transaction t = session.beginTransaction();
        try{                                        
            String hql = "SELECT g FROM CompromisosGestion g "
                    + "join fetch g.items i "                    
                    + "WHERE g.cgeId = " + gruId;
            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            compromisos = (CompromisosGestion)query.uniqueResult();            
            t.commit();
                      
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo obtener el compromiso \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo obtener el compromiso \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return compromisos;
    }
    
    public List<Items> obtenerItems(int gruId){
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Items> items= new ArrayList<Items>();
        Transaction t = session.beginTransaction();
        try{                                        
            String hql = "SELECT i FROM Items i "                    
                    + "WHERE i.compromiso = " + gruId;
            Query query = session.createQuery(hql);            
            items = query.list();            
            t.commit();
                      
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo obtener el item \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo obtener el item \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return items;
    }    
}
