package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular;

import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.NotaEvaluacionIndicador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.RegistroAuxiliar;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 13/10/2016.
 */
@Entity
@Table(name = "indicador_aprendizaje", schema = "pedagogico")
public class IndicadorAprendizaje implements java.io.Serializable {
    @Id
    @Column(name = "ind_apr_id", nullable = false, unique = true)
    @SequenceGenerator(name = "indicador_aprendizaje_ind_id_sequence",sequenceName = "pedagogico.indicador_aprendizaje_ind_apr_id_seq")
    @GeneratedValue(generator = "indicador_aprendizaje_ind_id_sequence")
    private int indAprId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cap_id",nullable = false)
    private CapacidadAprendizaje cap;

    
    @OneToMany(mappedBy = "indicador",fetch = FetchType.LAZY)
    List<IndicadoresSesionAprendizaje> indicadoresSesion = new ArrayList<>();
    
    @OneToMany(mappedBy = "indicador",fetch = FetchType.LAZY)
    List<IndicadoresUnidadAprendizaje> indicadoresUnidad = new ArrayList<>();
    
    @OneToMany(mappedBy = "indicador",fetch = FetchType.LAZY)
    List<NotaEvaluacionIndicador> notasEvaluacion = new ArrayList<>();

    @Column(name = "nom_ind", length = 500, unique = true)
    private String nomInd;

    @Column(name = "eva", nullable = false)
    private Boolean eva;

    @Column(name = "cre")
    private Integer cre;

    @Column(name = "usu_mod")
    private Integer usuMod;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;

    @Column(name = "est_reg", length = 1)
    private Character estReg;

    @Transient
    private Boolean selectedIndicador;

    @OneToMany(mappedBy = "indicador",fetch = FetchType.LAZY)
    List<RegistroAuxiliar> notas = new ArrayList<>();

    public IndicadorAprendizaje(String nomInd, Boolean eva) {
        this.nomInd = nomInd;
        this.eva = eva;

        this.fecMod = new Date();
        this.estReg = 'A';
    }
    public IndicadorAprendizaje(String nomInd, CapacidadAprendizaje cap, Boolean eva) {
        this.nomInd = nomInd;
        this.cap = cap;
        this.eva = eva;

        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public IndicadorAprendizaje(String nomInd, Boolean eva, Integer cre) {
        this.nomInd = nomInd;
        this.eva = eva;
        this.cre = cre;

        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public void setIndicadoresUnidad(List<IndicadoresUnidadAprendizaje> indicadoresUnidad) {
        this.indicadoresUnidad = indicadoresUnidad;
    }

    public List<IndicadoresUnidadAprendizaje> getIndicadoresUnidad() {
        return indicadoresUnidad;
    }

    
    public IndicadorAprendizaje(String nomInd, CapacidadAprendizaje cap, Boolean eva, Integer cre) {
        this.nomInd = nomInd;
        this.cap = cap;
        this.eva = eva;
        this.cre = cre;

        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public IndicadorAprendizaje() {
    }

    public int getIndAprId() {
        return indAprId;
    }

    public void setIndAprId(int indAprId) {
        this.indAprId = indAprId;
    }

    public CapacidadAprendizaje getCap() {
        return cap;
    }

    public void setCap(CapacidadAprendizaje cap) {
        this.cap = cap;
    }

    public String getNomInd() {
        return nomInd;
    }

    public void setNomInd(String nomInd) {
        this.nomInd = nomInd;
    }

    public Boolean getEva() {
        return eva;
    }

    public void setEva(Boolean eva) {
        this.eva = eva;
    }

    public Integer getCre() {
        return cre;
    }

    public void setCre(Integer cre) {
        this.cre = cre;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public Boolean getSelectedIndicador() {
        return selectedIndicador;
    }

    public void setSelectedIndicador(Boolean selectedIndicador) {
        this.selectedIndicador = selectedIndicador;
    }

    public List<IndicadoresSesionAprendizaje> getIndicadoresSesion() {
        return indicadoresSesion;
    }

   
    public void setIndicadoresSesion(List<IndicadoresSesionAprendizaje> indicadoresSesion) {
        this.indicadoresSesion = indicadoresSesion;
    }

   
    
    
    public List<NotaEvaluacionIndicador> getNotasEvaluacion() {
        return notasEvaluacion;
    }

    public void setNotasEvaluacion(List<NotaEvaluacionIndicador> notasEvaluacion) {
        this.notasEvaluacion = notasEvaluacion;
    }

    public List<RegistroAuxiliar> getNotas() {
        return notas;
    }

    public void setNotas(List<RegistroAuxiliar> notas) {
        this.notas = notas;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof IndicadorAprendizaje))return false;
        IndicadorAprendizaje that = (IndicadorAprendizaje) o;

        return indAprId == that.getIndAprId();
    }

    @Override
    public int hashCode() {
        return indAprId;
    }

    @Override
    public String toString() {
        return "{" +
                "indAprId:" + indAprId +
                ", nomInd:\"" + nomInd + '\"' +
                '}';
    }
}
