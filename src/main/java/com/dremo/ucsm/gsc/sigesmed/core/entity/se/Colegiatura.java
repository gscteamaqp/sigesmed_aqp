/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author gscadmin
 */
@Entity
@Table(name = "colegiatura", schema="administrativo")

public class Colegiatura implements Serializable {
 
    @Id
    @Column(name = "col_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_colegiatura", sequenceName="administrativo.colegiatura_col_id_seq" )
    @GeneratedValue(generator = "secuencia_colegiatura")
    private Integer colId;
    
    @Column(name = "nom_col_pro")
    private String nomColPro;
    
    @Column(name = "num_reg_col")
    private String numRegCol;
    
    @Column(name = "con_reg")
    private Boolean conReg;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "fic_esc_id")
    private FichaEscalafonaria fichaEscalafonaria;

    public Colegiatura() {
    }

    public Colegiatura(Integer colId) {
        this.colId = colId;
    }
    
    public Colegiatura(FichaEscalafonaria fichaEscalafonaria, String nomColPro, String numRegCol, Boolean conReg, Integer usuMod, Date fecMod, Character estReg ) {
        this.fichaEscalafonaria = fichaEscalafonaria;
        this.nomColPro = nomColPro;
        this.numRegCol = numRegCol;
        this.conReg = conReg;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
    }

    public Integer getColId() {
        return colId;
    }

    public void setColId(Integer colId) {
        this.colId = colId;
    }

    public String getNomColPro() {
        return nomColPro;
    }

    public void setNomColPro(String nomColPro) {
        this.nomColPro = nomColPro;
    }

    public String getNumRegCol() {
        return numRegCol;
    }

    public void setNumRegCol(String numRegCol) {
        this.numRegCol = numRegCol;
    }

    public Boolean getConReg() {
        return conReg;
    }

    public void setConReg(Boolean conReg) {
        this.conReg = conReg;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
    
    public FichaEscalafonaria getFichaEscalafonaria() {
        return fichaEscalafonaria;
    }

    public void setFichaEscalafonaria(FichaEscalafonaria fichaEscalafonaria) {
        this.fichaEscalafonaria = fichaEscalafonaria;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (colId != null ? colId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Colegiatura)) {
            return false;
        }
        Colegiatura other = (Colegiatura) object;
        if ((this.colId == null && other.colId != null) || (this.colId != null && !this.colId.equals(other.colId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsc.sigesmed.core.entity.se.Colegiatura[ colId=" + colId + " ]";
    }
    
}
