/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BienesMuebles;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import static com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx.Reporte_Bienes_BibliografiaTx.IMG;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import java.text.SimpleDateFormat;


import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.dremo.ucsm.gsc.sigesmed.util.GCell;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.io.source.ByteArrayOutputStream;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.border.SolidBorder;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import java.util.ArrayList;
import org.apache.commons.codec.binary.Base64;
/**
 *
 * @author Administrador
 */
public class Reporte_Bienes_MaquinariasTx {
    
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    
    public static final String DEST = ServicioREST.PATH_SIGESMED + "/ReportesTemp/";
    public static final String IMG = ServicioREST.PATH_SIGESMED + "/recursos/img/minedu.png";
    
    public class HeaderTable extends PdfPageEventHelper {
        protected PdfPTable header;
        protected float tableHeight;
        public HeaderTable() throws DocumentException, IOException {
            header = new PdfPTable(1);
            header.setWidths(new int[]{2});
            header.setTotalWidth(391);
            header.setLockedWidth(true);
            header.getDefaultCell().setFixedHeight(40);
            header.getDefaultCell().setBorder(Rectangle.NO_BORDER);
            
            // add image
            Image logo = Image.getInstance(IMG);
            header.addCell(logo);
            tableHeight = header.getTotalHeight();
        }
 
        public float getTableHeight() {
            return tableHeight;
        }
 
        public void onEndPage(PdfWriter writer, Document document) {
            header.writeSelectedRows(0, -1,
                    document.left(),
                    document.top() + ((document.topMargin() + tableHeight) / 2),
                    writer.getDirectContent());
        }
    }
    
    Reporte_Bienes_MaquinariasTx(){
        
    }
    
    public String generar_reporte_maquinarias(List<BienesMuebles> bm , Organizacion org) throws DocumentException, IOException{
         
        Reporte_Bienes_MaquinariasTx.HeaderTable event = new Reporte_Bienes_MaquinariasTx.HeaderTable();
        
        Document document = new Document(PageSize.A4.rotate(), 36, 36, 20 + event.getTableHeight(), 36);
        
        ByteArrayOutputStream baos = new ByteArrayOutputStream();       
        
        try {
        
            //PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(DEST));
            PdfWriter writer = PdfWriter.getInstance(document, baos);
            writer.setPageEvent(event);
        
        } catch (DocumentException ex) {
            Logger.getLogger(Reporte_Bienes_BibliografiaTx.class.getName()).log(Level.SEVERE, null, ex);
        }
                        
        /* DATOS DE LA ORGANIZACION*/
        String nombreInst = org.getNom();
        //    String ugel = org.getOrganizacionPadre().getNom();
        String denominacion = org.getAli();
        String direccion = org.getDir();
            
        document.open();
        
        Font regular = new Font(Font.FontFamily.HELVETICA, 11);
        Font bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
        
        PdfPTable table1 = new PdfPTable(2);
        table1.setWidths(new int[]{42,18});
        table1.getDefaultCell().setPadding(0);
        table1.setTotalWidth(PageSize.A4.rotate().getWidth() - 50);
        table1.setLockedWidth(true);            
                
        PdfPTable innerTable1 = new PdfPTable(16);
        innerTable1.setWidths(new int[]{1,1,1,1,1,1,1,1,3,3,3,3,3,1,2,2});

        PdfPCell cellT;
        cellT = new PdfPCell(new Phrase("Nombre de la Institucion", bold));
        cellT.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cellT.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellT.setColspan(8);
        innerTable1.addCell(cellT);
        cellT = new PdfPCell(new Phrase(nombreInst, regular));
        cellT.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellT.setColspan(4);
        innerTable1.addCell(cellT);
        cellT = new PdfPCell(new Phrase("DRE", bold));
        cellT.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cellT.setHorizontalAlignment(Element.ALIGN_CENTER);
        innerTable1.addCell(cellT);
        cellT = new PdfPCell(new Phrase("", regular));
        cellT.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellT.setColspan(3);
        innerTable1.addCell(cellT);
        
        cellT = new PdfPCell(new Phrase("Director de la IE", bold));
        cellT.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cellT.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellT.setColspan(8);
        innerTable1.addCell(cellT);
        cellT = new PdfPCell(new Phrase("", regular));
        cellT.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellT.setColspan(4);
        innerTable1.addCell(cellT);
        cellT = new PdfPCell(new Phrase("UGEL", bold));
        cellT.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cellT.setHorizontalAlignment(Element.ALIGN_CENTER);
        innerTable1.addCell(cellT);
        cellT = new PdfPCell(new Phrase("", regular));
        cellT.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellT.setColspan(3);
        innerTable1.addCell(cellT);
        
        cellT = new PdfPCell(new Phrase("Codigo Modular", bold));
        cellT.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cellT.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellT.setColspan(7);
        innerTable1.addCell(cellT);
        cellT = new PdfPCell(new Phrase("Denominacion", bold));
        cellT.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cellT.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellT.setColspan(2);
        innerTable1.addCell(cellT);
        cellT = new PdfPCell(new Phrase("Gestión", bold));
        cellT.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cellT.setHorizontalAlignment(Element.ALIGN_CENTER);
        innerTable1.addCell(cellT);
        cellT = new PdfPCell(new Phrase("Nivel", bold));
        cellT.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cellT.setHorizontalAlignment(Element.ALIGN_CENTER);
        innerTable1.addCell(cellT);
        cellT = new PdfPCell(new Phrase("Dirección", bold));
        cellT.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cellT.setHorizontalAlignment(Element.ALIGN_CENTER);
        innerTable1.addCell(cellT);
        cellT = new PdfPCell(new Phrase(direccion, regular));
        cellT.setColspan(4);
        cellT.setHorizontalAlignment(Element.ALIGN_CENTER);
        innerTable1.addCell(cellT);
        
        cellT = new PdfPCell(new Phrase(""));
        innerTable1.addCell(cellT);
        cellT = new PdfPCell(new Phrase(""));
        innerTable1.addCell(cellT);
        cellT = new PdfPCell(new Phrase(""));
        innerTable1.addCell(cellT);
        cellT = new PdfPCell(new Phrase(""));
        innerTable1.addCell(cellT);
        cellT = new PdfPCell(new Phrase(""));
        innerTable1.addCell(cellT);
        cellT = new PdfPCell(new Phrase(""));
        innerTable1.addCell(cellT);
        cellT = new PdfPCell(new Phrase(""));
        innerTable1.addCell(cellT);
        
        cellT = new PdfPCell(new Phrase("", regular));
        cellT.setColspan(2);
        cellT.setHorizontalAlignment(Element.ALIGN_CENTER);
        innerTable1.addCell(cellT);
        
        cellT = new PdfPCell(new Phrase("", regular));
        cellT.setHorizontalAlignment(Element.ALIGN_CENTER);
        innerTable1.addCell(cellT);
        
        cellT = new PdfPCell(new Phrase("", regular));
        cellT.setHorizontalAlignment(Element.ALIGN_CENTER);
        innerTable1.addCell(cellT);
        
        cellT = new PdfPCell(new Phrase("Provincia", bold));   
        cellT.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cellT.setHorizontalAlignment(Element.ALIGN_CENTER);
        innerTable1.addCell(cellT);
        
        cellT = new PdfPCell(new Phrase("", regular));
        cellT.setColspan(2);
        cellT.setHorizontalAlignment(Element.ALIGN_CENTER);
        innerTable1.addCell(cellT);
        
        cellT = new PdfPCell(new Phrase("Distrito", bold));
        cellT.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cellT.setHorizontalAlignment(Element.ALIGN_CENTER);
        innerTable1.addCell(cellT);
        
        cellT = new PdfPCell(new Phrase("", regular));
        cellT.setHorizontalAlignment(Element.ALIGN_CENTER);
        innerTable1.addCell(cellT);
        
        cellT = new PdfPCell(innerTable1);
        table1.addCell(cellT);
        
        PdfPCell cell2 = new PdfPCell(new Phrase("INVENTARIO FISICO GENERAL DE BIENES\n PATRIMONIALES AÑO 201_\n MAQUINAS Y EQUIPOS"));
        cell2.setRowspan(3);       
        cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);              
        
        table1.addCell(cell2);        
        
        document.add(table1);    
               
        /*CABECERA DE TABLA*/
        String [] cab = {"N°", "Codigo Patrimonial","Descripcion del Bien","Marca","Modelo","Serie","Dimensiones","Color","Valor en Libros","Fecha de Ingreso","Dcoumentos Ingreso","Estado del Bien","Ubicacion","Procedencia","Usuario a Cargo Bien","Tiempo de Duracion"};
        
        /*CONSTRUIMOS EL OBJETO REPORTE*/
        float columnWidths_1[] = {1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 2, 1, 2, 1, 1, 1};
        PdfPTable tablaContent = new PdfPTable(columnWidths_1);
        
        tablaContent.setWidthPercentage(100);        

        tablaContent.getDefaultCell().setBackgroundColor(new GrayColor(0.75f));
        tablaContent.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        tablaContent.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
                
        for (int i = 0; i < cab.length; i++) {
            tablaContent.addCell(new Phrase(cab[i], bold));
        }
        
        document.add(new Phrase("\n"));
        
        int size = bm.size();

        for (int i = 0; i < size; i++) {
            BienesMuebles bien_mueble = bm.get(i);
            String index = (i + 1) + "";
            String cod_pat = Integer.toString(bien_mueble.getCon_pat_id());
            String des_bie = bien_mueble.getDes_bie();
            String marca = bien_mueble.getDtm().getMarc();
            String modelo = bien_mueble.getDtm().getMod();
            String serie = bien_mueble.getDtm().getSer();
            String dim = Integer.toString(bien_mueble.getDtm().getDim());
            String color = bien_mueble.getDtm().getCol();
            String val_lib = "";
            String fec_ing = formatter.format(bien_mueble.getFec_reg());
            String doc_ing = bien_mueble.getRut_doc_bie();
            String est_bie = bien_mueble.getEstado_bie();
            String ubicac = bien_mueble.getAmbiente().getDes();
            String proc = "";
            String user = Integer.toString(bien_mueble.getUsu_mod());
            String tiem_dur = "";            
            
            tablaContent.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
            tablaContent.addCell(index);
            tablaContent.addCell(cod_pat);
            tablaContent.addCell(des_bie);
            tablaContent.addCell(marca);
            tablaContent.addCell(modelo);
            tablaContent.addCell(serie);
            tablaContent.addCell(dim);
            tablaContent.addCell(color);
            tablaContent.addCell(val_lib);
            tablaContent.addCell(fec_ing);
            tablaContent.addCell(doc_ing);
            tablaContent.addCell(est_bie);
            tablaContent.addCell(ubicac);
            tablaContent.addCell(proc);
            tablaContent.addCell(user);
            tablaContent.addCell(tiem_dur);
            
        }
        
        document.add(tablaContent);
        
        document.close();
        
        String responseStringPdf = "data:application/pdf;base64," + Base64.encodeBase64String(baos.toByteArray());
        baos = null;
        
        return responseStringPdf;        
         
    }
    
}
