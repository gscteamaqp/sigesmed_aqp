package com.dremo.ucsm.gsc.sigesmed.core.entity.mech;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="horario_escolar" ,schema="institucional" )
public class HorarioEscolar  implements java.io.Serializable {

    @Id
    @Column(name="hor_esc_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_horarioescolar", sequenceName="institucional.horario_escolar_hor_esc_id_seq" )
    @GeneratedValue(generator="secuencia_horarioescolar")
    private int horarioId;
    
    @Column(name="hor_ini")
    @Temporal(TemporalType.TIME)
    private Date horIni;
    @Temporal(TemporalType.TIME)
    @Column(name="hor_fin")
    private Date horFin;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_mod", nullable=false, length=29)
    private Date fecMod;    
    @Column(name="usu_mod")
    private int usuMod;
    @Column(name="est_reg")
    private char estReg;
    
    @Column(name="sec_id")
    private char secId;
    
    @Column(name="pla_niv_id")
    private int plaNivId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="pla_niv_id",updatable = false,insertable = false)
    private PlanNivel planNivel;
    
    @Column(name="gra_id")
    private int graId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="gra_id",updatable = false,insertable = false)
    private Grado grado;
    
    @OneToMany(mappedBy="horario",cascade=CascadeType.PERSIST)
    private List<HorarioDetalle> detalles;

    public HorarioEscolar() {
    }
    public HorarioEscolar(int horarioId) {
        this.horarioId = horarioId;
    }
    public HorarioEscolar(int horarioId,Date horIni,Date horFin ,int plaNivId, int graId,char secId,Date fecMod,int usuMod,char estReg) {
       this.horarioId = horarioId;
       
       this.horIni = horIni;
       this.horFin = horFin;
       
       this.plaNivId = plaNivId;
       this.graId = graId;
       this.secId = secId;
       
       this.fecMod = fecMod;
       this.usuMod = usuMod;
       this.estReg = estReg;
    }
   
     
    public int getHorarioId() {
        return this.horarioId;
    }    
    public void setHorarioId(int horarioId) {
        this.horarioId = horarioId;
    }
    
    public Date getHorIni() {
        return this.horIni;
    }
    public void setHorIni(Date horIni) {
        this.horIni = horIni;
    }
    
    public Date getHorFin() {
        return this.horFin;
    }
    public void setHorFin(Date horFin) {
        this.horFin = horFin;
    }
    
    public Date getFecMod() {
        return this.fecMod;
    }
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }
    
    public int getUsuMod() {
        return this.usuMod;
    }
    public void setUsuMod(int usuMod) {
        this.usuMod = usuMod;
    }
    
    public char getEstReg() {
        return this.estReg;
    }
    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }
    
    public int getPlaNivId() {
        return this.plaNivId;
    }    
    public void setPlaNivId(int plaNivId) {
        this.plaNivId = plaNivId;
    }
    
    public PlanNivel getPlanNivel() {
        return this.planNivel;
    }
    public void setPlanNivel(PlanNivel planNivel) {
        this.planNivel = planNivel;
    }
    
    public char getSecId() {
        return this.secId;
    }    
    public void setSecId(char secId) {
        this.secId = secId;
    }
    
    public int getGraId() {
        return this.graId;
    }    
    public void setGraId(int graId) {
        this.graId = graId;
    }
    public Grado getGrado() {
        return this.grado;
    }
    public void setGrado(Grado grado) {
        this.grado = grado;
    }
    
    public List<HorarioDetalle> getDetalles() {
        return this.detalles;
    }
    public void setDetalles(List<HorarioDetalle> detalles) {
        this.detalles = detalles;
    }
}


