/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.ma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.ConfiguracionNotaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.RegistroAuxiliar;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ConfiguracionNota;
import java.util.List;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;
/**
 *
 * @author abel
 */
public class ConfiguracionNotaDaoHibernate extends GenericDaoHibernate<ConfiguracionNota> implements ConfiguracionNotaDao{

    @Override
    public List<ConfiguracionNota> listar_configuracion() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ConfiguracionNota> listarConfiguracionDocente(int docID, int orgID) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT cn FROM ConfiguracionNota cn WHERE cn.doc_id=:p1 AND cn.org_id=:p2 AND cn.estReg='A'";
           
            Query query = session.createQuery(hql);
            query.setInteger("p1", docID);
            query.setInteger("p2", orgID);
            
            return query.list();
        }catch(Exception e){
             throw e;
        }finally {
            session.close();
        }
        
        
        
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   
    
}
