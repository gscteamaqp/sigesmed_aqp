package com.dremo.ucsm.gsc.sigesmed.logic.maestro.curriculabanco.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.CompetenciaAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CompetenciaAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 13/10/2016.
 */
public class ListarBancoCompetenciasTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ListarBancoCompetenciasTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            CompetenciaAprendizajeDao competenciaDao = (CompetenciaAprendizajeDao) FactoryDao.buildDao("maestro.plan.CompetenciaAprendizajeDao");
            List<CompetenciaAprendizaje> competencias = competenciaDao.buscarCompetenciasConCapacidades();
            JSONArray jsonCom = new JSONArray();
            for(CompetenciaAprendizaje competencia : competencias){
                JSONObject jsonObj = new JSONObject(EntityUtil.objectToJSONString(
                        new String[]{"comId","nom"},
                        new String[]{"cod","nom"},
                        competencia
                ));
                jsonObj.put("cur",competencia.getDcn().getNombre());
                jsonObj.put("des",competencia.getDes());
                jsonObj.put("cap",competencia.getCapacidades().size());
                if(competencia.getArea() != null){
                    jsonObj.put("are",new JSONObject()
                            .put("cod",competencia.getArea().getAreCurId())
                            .put("nom",competencia.getArea().getNom()));
                }

                jsonCom.put(jsonObj);

            }
            return WebResponse.crearWebResponseExito("Se listaron correctamente los datos",WebResponse.OK_RESPONSE).setData(jsonCom);
        }catch (Exception e){
            logger.log(Level.SEVERE, "listarBancoCompetencias",e);
            return WebResponse.crearWebResponseError("No se pudo listar la data",WebResponse.BAD_RESPONSE);
        }
    }
}
