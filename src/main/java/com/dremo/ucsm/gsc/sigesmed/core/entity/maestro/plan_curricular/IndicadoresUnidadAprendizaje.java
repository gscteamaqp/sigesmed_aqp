/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular;
import javax.persistence.*;
import java.util.Date;
import java.util.logging.Logger;

/**
 *
 * @author abel
 */
@Entity
@Table(name = "indicadores_unidad_aprendizaje", schema = "pedagogico")
public class IndicadoresUnidadAprendizaje implements java.io.Serializable{
    
    @Embeddable
    public static class Id implements java.io.Serializable{
        
        @Column(name = "uni_did_id", nullable = false)
        protected int uni_did_id;
        @Column(name = "com_id", nullable = false)
        protected int com_id;
        @Column(name = "cap_id", nullable = false)
        protected int cap_id;
        @Column(name = "ind_apr_id", nullable = false)
        protected int ind_apr_id;
        
        public Id() {
        }
         
        public Id(int uni_did_id, int com_id, int cap_id, int ind_apr_id) {
            this.uni_did_id = uni_did_id;
            this.com_id = com_id;
            this.cap_id = cap_id;
            this.ind_apr_id = ind_apr_id;
            
        }
  
    }
    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "uni_did_id", column = @Column(name = "uni_did_id", nullable = false)),
            @AttributeOverride(name = "com_id", column = @Column(name = "com_id", nullable = false)),
            @AttributeOverride(name = "cap_id", column = @Column(name = "cap_id", nullable = false)),
            @AttributeOverride(name = "ind_apr_id", column = @Column(name = "ind_apr_id", nullable = false)),
    })
     private Id id = new Id();   
        
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "uni_did_id", insertable = false, updatable = false)
    private UnidadDidactica unidad;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "com_id", insertable = false, updatable = false)
    private CompetenciaAprendizaje competencia;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cap_id", insertable = false, updatable = false)
    private CapacidadAprendizaje capacidad;
    
    
    
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ind_apr_id", insertable = false, updatable = false)
    private IndicadorAprendizaje indicador;

    public IndicadoresUnidadAprendizaje() {
    }

    public IndicadoresUnidadAprendizaje(UnidadDidactica unidad, CompetenciaAprendizaje competencia, CapacidadAprendizaje capacidad, IndicadorAprendizaje indicador) {
        this.unidad = unidad;
        this.competencia = competencia;
        this.capacidad = capacidad;
        this.indicador = indicador;
    }

    public void setId(Id id) {
        this.id = id;
    }

    public Id getId() {
        return id;
    }
    
    
    

    public void setUnidad(UnidadDidactica unidad) {
        this.unidad = unidad;
    }

    public void setCompetencia(CompetenciaAprendizaje competencia) {
        this.competencia = competencia;
    }

    public void setCapacidad(CapacidadAprendizaje capacidad) {
        this.capacidad = capacidad;
    }

    public void setIndicador(IndicadorAprendizaje indicador) {
        this.indicador = indicador;
    }

    public UnidadDidactica getUnidad() {
        return unidad;
    }

    public CompetenciaAprendizaje getCompetencia() {
        return competencia;
    }

    public CapacidadAprendizaje getCapacidad() {
        return capacidad;
    }

    public IndicadorAprendizaje getIndicador() {
        return indicador;
    }
       
      
       

    
}
    

