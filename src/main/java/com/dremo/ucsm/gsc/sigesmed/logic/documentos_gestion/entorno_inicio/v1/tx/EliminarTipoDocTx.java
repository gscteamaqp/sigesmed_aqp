/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.ItemFileDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.TipoEspecializadoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.TipoEspecializado;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class EliminarTipoDocTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        Integer tipoDocID = 0;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            tipoDocID = requestData.getInt("tipoDocID");
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo eliminar, datos incorrectos", e.getMessage() );
        }
        //Fin
        /*
        *  Parte para la operacion en la Base de Datos
        */
        TipoEspecializadoDao tipoEspDao = (TipoEspecializadoDao)FactoryDao.buildDao("rdg.TipoEspecializadoDao");
        try{
            tipoEspDao.delete(new TipoEspecializado(tipoDocID.shortValue()));
                    ItemFileDao itemDao=(ItemFileDao)FactoryDao.buildDao("rdg.ItemFileDao");
        itemDao.desactivarDocPorTipoDocumento(new TipoEspecializado(tipoDocID.shortValue()));
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo eliminar el tipo de tramite ", e.getMessage() );
        }
        //Fin
        
        
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Tipo TipoTramite se elimino correctamente");
        //Fin
    }
}
