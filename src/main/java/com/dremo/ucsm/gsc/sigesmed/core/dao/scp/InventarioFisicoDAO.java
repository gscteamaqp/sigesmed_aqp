/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.scp;
import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import java.util.Date;
import java.util.List;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.InventarioFisico;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.InventarioInicial;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.InventarioFisicoDetalle;
/**
 *
 * @author Administrador
 */
public interface InventarioFisicoDAO extends GenericDao<InventarioFisico>{
    
    public List<InventarioFisico> listarInventarioFisico(int org_id);
    public InventarioFisico obtenerInventarioFisico(int inv_id , int org_id );
    public List<InventarioFisicoDetalle> obtenerDetalleInventarioFisico(int inv_id , int org_id);
    public void eliminar_detalle(int inv_ini_id);
    public void eliminar_inventario_fisico(int inv_fis_id);
    public List<InventarioFisico> reporteInventario(int org_id);
}
