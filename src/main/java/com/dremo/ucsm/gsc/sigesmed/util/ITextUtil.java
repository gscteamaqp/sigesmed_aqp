package com.dremo.ucsm.gsc.sigesmed.util;

import com.itextpdf.io.font.FontConstants;
import com.itextpdf.io.font.PdfEncodings;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.Style;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.HorizontalAlignment;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.VerticalAlignment;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

/**
 * Orden Logico -> ITextUtil -> tablas, imagenes, graficos y parrafos ->
 * closeDocument()
 *
 * @author CGS_H
 */
public class ITextUtil {

    protected PdfDocument pdfDoc;
    protected Document doc;
    protected File file;
    protected final double VALUE_CM = 28.34;

    public static final int ALIGN_LEFT = 1;
    public static final int ALIGN_RIGHT = 2;
    public static final int ALIGN_CENTER = 3;
    public static final int ALIGN_JUSTIFIED = 4;
    public static final int ALIGN_JUSTIFIED_ALL = 5;

    /**
     * Funcion para Crear un Archivo IText en formato A4 Ej. path:
     * "src/main/resources/fonts/lekton/", name: "myPdf.pdf"
     *
     * @param path Ubicacion del Archivo pdf
     * @param name Nombre del Archivo pdf
     */
    public ITextUtil(String path, String name) {
        try {
            file = new File(path + name);
            pdfDoc = new PdfDocument(new PdfWriter(file.getAbsolutePath()));
            doc = new Document(pdfDoc, PageSize.A4);

        } catch (FileNotFoundException ex) {
        }
    }

    /**
     * Funcion para Crear un Archivo IText en formato A4 Ej. path:
     * "src/main/resources/fonts/lekton/", name: "myPdf.pdf"
     *
     * @param path Ubicacion del Archivo pdf
     * @param name Nombre del Archivo pdf
     * @param pageRotate Indica la Rotacion de la Hoja
     */
    public ITextUtil(String path, String name, boolean pageRotate) {
        try {
            file = new File(path + name);
            pdfDoc = new PdfDocument(new PdfWriter(file.getAbsolutePath()));
            if (pageRotate) {
                doc = new Document(pdfDoc, PageSize.A4.rotate());
            } else {
                doc = new Document(pdfDoc, PageSize.A4);
            }

        } catch (FileNotFoundException ex) {
        }
    }

    /**
     * Funcion para Crear un Archivo IText en formato A4, 1 cm -> 28.34 pixeles
     *
     * @param path Ubicacion del Archivo pdf
     * @param name Nombre del Archivo pdf
     * @param pageRotate Indica la Rotacion de la Hoja
     * @param leftMargin Margen Izquierdo (pixeles)
     * @param rightMargin Margen Derecho (pixeles)
     * @param topMargin Margen Superior (pixeles)
     * @param bottonMargin Margen Inferior (pixeles)
     */
    public ITextUtil(String path, String name, boolean pageRotate, float leftMargin, float rightMargin,
            float topMargin, float bottonMargin) {
        try {
            file = new File(path + name);
            pdfDoc = new PdfDocument(new PdfWriter(file.getAbsolutePath()));
            if (pageRotate) {
                doc = new Document(pdfDoc, PageSize.A4.rotate());
            } else {
                doc = new Document(pdfDoc, PageSize.A4);
            }
            doc.setMargins(leftMargin, rightMargin, topMargin, bottonMargin);
        } catch (FileNotFoundException ex) {
        }
    }
    

    /**
     * 
     * @param path Ubicacion del Archivo pdf
     * @param name Nombre del Archivo pdf
     * @param pageRotate Indica la Rotacion de la Hoja
     * @param leftMargin Margen Izquierdo (cm)
     * @param rightMargin Margen Derecho (cm)
     * @param topMargin Margen Superior (cm)
     * @param bottonMargin Margen Inferior (cm) 
     */
    public ITextUtil(String path, String name, boolean pageRotate, double leftMargin,
            double rightMargin, double topMargin, double bottonMargin) {
        
        float myLeftMargin, myRightMargin, myTopMargin, myBottonMargin;
        try {
            myLeftMargin = (float) (leftMargin * VALUE_CM);
            myRightMargin = (float) (rightMargin * VALUE_CM);
            myTopMargin = (float) (topMargin * VALUE_CM);
            myBottonMargin = (float) (bottonMargin * VALUE_CM);
            file = new File(path + name);
            pdfDoc = new PdfDocument(new PdfWriter(file.getAbsolutePath()));
            if (pageRotate) {
                doc = new Document(pdfDoc, PageSize.A4.rotate());
            } else {
                doc = new Document(pdfDoc, PageSize.A4);
            }
            doc.setMargins(myLeftMargin, myRightMargin, myTopMargin, myBottonMargin);
        } catch (FileNotFoundException ex) {
        }
    }

    /**
     * Funcion para Crear un Archivo IText, 1 cm -> 28.34 pixeles
     *
     * @param path Ubicacion del Archivo pdf
     * @param name Nombre del Archivo pdf
     * @param width Ancho del pdf (pixeles)
     * @param height Alto del pdf (pixeles)
     * @param leftMargin Margen Izquierdo (pixeles)
     * @param rightMargin Margen Derecho (pixeles)
     * @param topMargin Margen Superior (pixeles)
     * @param bottonMargin Margen Inferior (pixeles)
     */
    public ITextUtil(String path, String name, float width, float height, float leftMargin,
            float rightMargin, float topMargin, float bottonMargin) {
        try {
            file = new File(path + name);
            pdfDoc = new PdfDocument(new PdfWriter(file.getAbsolutePath()));
            doc = new Document(pdfDoc, new PageSize(new Rectangle(width, height)));
            doc.setMargins(leftMargin, rightMargin, topMargin, bottonMargin);
        } catch (FileNotFoundException ex) {
        }
    }

    /**
     * Funcion para Crear un Archivo IText, 1 cm -> 28.34 pixeles pixeles
     *
     * @param path Ubicacion del Archivo pdf
     * @param name Nombre del Archivo pdf
     * @param width_cm Ancho del pdf (cm)
     * @param height_cm Alto del pdf (cm)
     * @param leftMargin Margen Izquierdo (cm)
     * @param rightMargin Margen Derecho (cm)
     * @param topMargin Margen Superior (cm)
     * @param bottonMargin Margen Inferior (cm)
     */
    public ITextUtil(String path, String name, double width_cm, double height_cm, double leftMargin,
            double rightMargin, double topMargin, double bottonMargin) {

        float myWidth, myHeight, myLeftMargin, myRightMargin, myTopMargin, myBottonMargin;
        try {
            myWidth = (float) (width_cm * VALUE_CM);
            myHeight = (float) (height_cm * VALUE_CM);
            myLeftMargin = (float) (leftMargin * VALUE_CM);
            myRightMargin = (float) (rightMargin * VALUE_CM);
            myTopMargin = (float) (topMargin * VALUE_CM);
            myBottonMargin = (float) (bottonMargin * VALUE_CM);
            file = new File(path + name);
            pdfDoc = new PdfDocument(new PdfWriter(file.getAbsolutePath()));
            doc = new Document(pdfDoc, new PageSize(new Rectangle(myWidth, myHeight)));
            doc.setMargins(myLeftMargin, myRightMargin, myTopMargin, myBottonMargin);
        } catch (FileNotFoundException ex) {
        }
    }

    /**
     * Cierra la escritura del documento y genera el Documento Pdf
     */
    public void closeDocument() {
        doc.close();
    }

    /**
     * Agrega un Parrafo al documento pdf
     *
     * @param leters Contenido del parrafo
     * @param font Fuente del parrafo
     */
    public void addParagraph(String leters, Style font) {
        Paragraph paragraph = new Paragraph(leters);
        paragraph.addStyle(font);
        doc.add(paragraph);
    }

    /**
     * Agrega un Parrafo al documento pdf
     *
     * @param leters Contenido del parrafo
     * @param font Fuente del parrafo
     * @param alignment Alineacion del parrafo LEFT:1 RIGHT:2 CENTER:3
     * JUSTIFIED:4 JUSTIFIED ALL:5
     */
    public void addParagraph(String leters, Style font, int alignment) {
        Paragraph paragraph = new Paragraph(leters);
        paragraph.addStyle(font);
        switch (alignment) {
            case ALIGN_LEFT: {
                paragraph.setTextAlignment(TextAlignment.LEFT);
                break;
            }
            case ALIGN_RIGHT: {
                paragraph.setTextAlignment(TextAlignment.RIGHT);
                break;
            }
            case ALIGN_CENTER: {
                paragraph.setTextAlignment(TextAlignment.CENTER);
                break;
            }
            case ALIGN_JUSTIFIED: {
                paragraph.setTextAlignment(TextAlignment.JUSTIFIED);
                break;
            }
            case ALIGN_JUSTIFIED_ALL: {
                paragraph.setTextAlignment(TextAlignment.JUSTIFIED_ALL);
                break;
            }
        }
        doc.add(paragraph);
    }

    /**
     * Agrega un Parrafo al documento pdf
     *
     * @param leters Contenido del parrafo
     */
    public void addParagraph(String leters) {
        Paragraph paragraph = new Paragraph(leters);
        doc.add(paragraph);
    }

    /**
     * Agrega un salto de linea
     */
    public void addLineJump() {
        doc.add(new Paragraph(""));
    }

    /**
     * Agrega un imagen
     *
     * @param urlImg url de la Imagen
     * @param with Ancho de la imagen (Pixeles)
     * @param height Alto de la imagen (Pixeles)
     * @param absolute_x posicion Eje X (Pixeles)
     * @param absolute_y posicion Eje Y (Pixeles)
     */
    public void addImg(String urlImg, float with, float height, float absolute_x, float absolute_y) {
        Image img;
        try {
            img = new Image(ImageDataFactory.create(urlImg), absolute_x, absolute_y);
            img.setWidth(with);
            img.setHeight(height);
            doc.add(img);
        } catch (MalformedURLException ex) {
        }

    }

    /**
     * Agrega un imagen
     *
     * @param urlImg url de la Imagen
     * @param with_cm Ancho de la imagen (cm)
     * @param height_cm Alto de la imagen (cm)
     * @param absolute_x_cm posicion Eje X (cm)
     * @param absolute_y_cm posicion Eje Y (cm)
     */
    public void addImg(String urlImg, double with_cm, double height_cm,
            double absolute_x_cm, double absolute_y_cm) {

        float myAbsolute_X, myAbsolute_Y, myWith, myHeight;
        Image img;
        try {
            myAbsolute_X = (float) (absolute_x_cm * VALUE_CM);
            myAbsolute_Y = (float) (absolute_y_cm * VALUE_CM);
            myWith = (float) (with_cm * VALUE_CM);
            myHeight = (float) (height_cm * VALUE_CM);
            img = new Image(ImageDataFactory.create(urlImg), myAbsolute_X, myAbsolute_Y);
            img.setWidth(myWith);
            img.setHeight(myHeight);
            doc.add(img);
        } catch (MalformedURLException ex) {
        }
    }

    /**
     * Agrega un imagen
     *
     * @param urlImg url de la Imagen
     * @param with_cm Ancho de la imagen (cm)
     * @param height_cm Alto de la imagen (cm)
     * @param alignment Alineacion del parrafo LEFT:1 RIGHT:2 CENTER:3
     * JUSTIFIED:4 JUSTIFIED ALL:5
     */
    public void addImg(String urlImg, double with_cm, double height_cm, int alignment) {

        float myWith, myHeight;
        Image img;
        try {
            myWith = (float) (with_cm * VALUE_CM);
            myHeight = (float) (height_cm * VALUE_CM);
            img = new Image(ImageDataFactory.create(urlImg));
            img.setWidth(myWith);
            img.setHeight(myHeight);
            switch (alignment) {
                case ALIGN_LEFT: {
                    img.setTextAlignment(TextAlignment.LEFT);
                    break;
                }
                case ALIGN_RIGHT: {
                    img.setTextAlignment(TextAlignment.RIGHT);
                    break;
                }
                case ALIGN_CENTER: {
                    img.setTextAlignment(TextAlignment.CENTER);
                    break;
                }
                case ALIGN_JUSTIFIED: {
                    img.setTextAlignment(TextAlignment.JUSTIFIED);
                    break;
                }
                case ALIGN_JUSTIFIED_ALL: {
                    img.setTextAlignment(TextAlignment.JUSTIFIED_ALL);
                    break;
                }
            }
            doc.add(img);
        } catch (MalformedURLException ex) {
        }
    }

    /**
     * Inserta una Tabla en el documento
     *
     * @param mainHeader Titulo Principal
     * @param fontMainHeader Fuente Titulo Principal
     * @param header Cabeceras de las columnas
     * @param fontHeader Fuente Cabecera de Columnas
     * @param footer Pie de Tabla (Inidicador de continuacion de tabla)
     * @param fontFooter Fuente Pie de Pagina
     * @param data Datos de la tabla en ArrayList
     * @param fontData Fuente de Datos
     */
    public void addTable(
            String mainHeader, Style fontMainHeader,
            ArrayList<String> header, Style fontHeader,
            String footer, Style fontFooter,
            ArrayList<ArrayList<String>> data, Style fontData) {
        int columns = header.size();
        Table table = new Table(columns);
        Cell cell;
        cell = new Cell(1, columns);
        cell.add(new Paragraph(mainHeader).addStyle(fontMainHeader));
        cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
        cell.setHorizontalAlignment(HorizontalAlignment.CENTER);
        table.addCell(cell);
        for (String itm : header) {
            cell = new Cell(1, 1);
            cell.add(new Paragraph(itm).addStyle(fontHeader));
            cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
            cell.setHorizontalAlignment(HorizontalAlignment.CENTER);
            table.addCell(cell);
            table.addHeaderCell(cell);
        }
        cell = new Cell(1, columns);
        cell.add(new Paragraph(footer).addStyle(fontFooter));
        table.addFooterCell(cell);
        table.setSkipFirstHeader(true);
        table.setSkipLastFooter(true);
        for (ArrayList<String> row : data) {
            for (String dataCell : row) {
                cell = new Cell(1, 1);
                cell.add(new Paragraph(dataCell).addStyle(fontData));
                table.addCell(cell);
            }
        }
        doc.add(table);

    }

    /**
     * Devuelve un Font descargado ubicado en la URL
     *
     * @param urlFont Direccion del archivo .ttf
     * @param size Tamaño del font
     * @param color Color de la Fuente
     * @param isBold Indica si la Fuente es negrita
     * @param isItalic Indica si la Fuente es Italica
     * @param isUnderLine Indica si la Fuente es Subrayada
     * @return Style seleccionado para su uso en la app
     */
    public static Style getFont(String urlFont, float size, Color color,
            boolean isBold, boolean isItalic, boolean isUnderLine) {
        PdfFont myFont;
        Style myStyle = null;
        try {
            myFont = PdfFontFactory.createFont(urlFont, PdfEncodings.CP1250, true);
            myStyle = new Style();
            myStyle.setFont(myFont);
            myStyle.setFontSize(size);
            myStyle.setFontColor(color);
            if (isBold) {
                myStyle.setBold();
            }
            if (isItalic) {
                myStyle.setItalic();
            }
            if (isUnderLine) {
                myStyle.setUnderline();
            }
        } catch (IOException ex) {
        }
        return myStyle;
    }

    /**
     * Devuelve un Font descargado ubicado en la URL
     *
     * @param urlFont Direccion del archivo .ttf
     * @param size Tamaño del font
     * @return Style seleccionado para su uso en la app
     */
    public static Style getFont(String urlFont, float size) {
        PdfFont myFont;
        Style myStyle = null;
        try {
            myFont = PdfFontFactory.createFont(urlFont, PdfEncodings.CP1250, true);
            myStyle = new Style();
            myStyle.setFont(myFont);
            myStyle.setFontSize(size);
            myStyle.setFontColor(Color.BLACK);
        } catch (IOException ex) {
        }
        return myStyle;
    }
    
    public static Style getFontTimesRoman(float size) {
        PdfFont myFont;
        Style myStyle = null;
        try {
            myFont = PdfFontFactory.createFont(FontConstants.TIMES_ROMAN);
            myStyle = new Style();
            myStyle.setFont(myFont);
            myStyle.setFontSize(size);
            myStyle.setFontColor(Color.BLACK);
        } catch (IOException ex) {
        }
        return myStyle;
    }
    
    public static Style getFontConstants(String font, float size) {
        PdfFont myFont;
        Style myStyle = null;
        try {
            myFont = PdfFontFactory.createFont(font);
            myStyle = new Style();
            myStyle.setFont(myFont);
            myStyle.setFontSize(size);
            myStyle.setFontColor(Color.BLACK);
        } catch (IOException ex) {
        }
        return myStyle;
    }
    
    public static Style getFontConstants(String font, float size, 
            boolean isBold, boolean isItalic, boolean isUnderLine) {
        PdfFont myFont;
        Style myStyle = null;
        try {
            myFont = PdfFontFactory.createFont(font);
            myStyle = new Style();
            myStyle.setFont(myFont);
            myStyle.setFontSize(size);
            myStyle.setFontColor(Color.BLACK);
            if (isBold) {
                myStyle.setBold();
            }
            if (isItalic) {
                myStyle.setItalic();
            }
            if (isUnderLine) {
                myStyle.setUnderline();
            }
        } catch (IOException ex) {
        }
        return myStyle;
    }
    
    

    public String getAbsolutePath() {
        return file.getAbsolutePath();
    }

    /**
     * Cabecera para documentos TIGER
     *
     * @param urlImgMinEdu Ubicacion de la Imagen Ministerio de Educacion
     * @param urlImgTiger Ubicacion de la Imagen Tiger
     */
    public void addDocumentHeader(String urlImgMinEdu, String urlImgTiger) {
        Table table = new Table(2);
        Cell cell;
        Image imgMinEdu, imgTiger;

        float sizeMinEdu = (float) (8 * VALUE_CM);
        float sizeTiger = (float) (9 * VALUE_CM);
        try {
            imgMinEdu = new Image(ImageDataFactory.create(urlImgMinEdu));
            imgMinEdu.setWidth(sizeMinEdu);
            //imgMinEdu.setHeight(sizeMinEdu);
            imgMinEdu.setTextAlignment(TextAlignment.LEFT);

            imgTiger = new Image(ImageDataFactory.create(urlImgTiger));
            imgTiger.setWidth(sizeTiger);
            //imgTiger.setHeight(sizeTiger);
            imgTiger.setTextAlignment(TextAlignment.RIGHT);

            cell = new Cell(1, 1);
            cell.add(imgMinEdu);
            cell.setBorder(Border.NO_BORDER);
            cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
            cell.setTextAlignment(TextAlignment.LEFT);
            table.addCell(cell);

            cell = new Cell(1, 1);
            cell.add(imgTiger);
            cell.setBorder(Border.NO_BORDER);
            cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
            cell.setTextAlignment(TextAlignment.RIGHT);
            table.addCell(cell);

            doc.add(table);
        } catch (MalformedURLException ex) {
        }

    }

    /**
     * Cabecera para documentos TIGER
     *
     * @param urlImgMinEdu Ubicacion de la Imagen Ministerio de Educacion
     * @param msj
     * @param msjFont
     */
    public void addDocumentHeader(String urlImgMinEdu, String msj, Style msjFont) {
        Table table = new Table(2);
        Cell cell;
        Image imgMinEdu;

        float sizeMinEdu = (float) (8 * VALUE_CM);
        try {
            imgMinEdu = new Image(ImageDataFactory.create(urlImgMinEdu));
            imgMinEdu.setWidth(sizeMinEdu);
            //imgMinEdu.setHeight(sizeMinEdu);
            imgMinEdu.setTextAlignment(TextAlignment.CENTER);

            cell = new Cell(1, 1);
            cell.add(imgMinEdu);
            cell.setBorder(Border.NO_BORDER);
            cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
            cell.setTextAlignment(TextAlignment.LEFT);
            table.addCell(cell);

            cell = new Cell(1, 1);
            cell.add(msj).addStyle(msjFont);
            cell.setBorder(Border.NO_BORDER);
            cell.setVerticalAlignment(VerticalAlignment.MIDDLE);
            cell.setTextAlignment(TextAlignment.RIGHT);
            table.addCell(cell);
            
            doc.add(table);
        } catch (MalformedURLException ex) {
        }

    }

}
