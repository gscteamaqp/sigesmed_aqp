/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.std.HistorialExpedienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.HistorialExpediente;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author abel
 */
public class ListarHistorialTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int areaID = 0;
        String tipo = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            areaID = requestData.getInt("areaID");
            tipo = requestData.optString("estado");
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar el historial de los expedientes por area, datos incorrectos", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<HistorialExpediente> historial = null;
        //List<RutaTramite> rutas = null;
        HistorialExpedienteDao historialDao = (HistorialExpedienteDao)FactoryDao.buildDao("std.HistorialExpedienteDao");
        try{
            if(tipo.contentEquals("rechazados"))
                historial =historialDao.buscarPorAreaYEstado(areaID,EstadoExpediente.RECHAZADO);
            else if(tipo.contentEquals("recibidos"))
                historial =historialDao.buscarPorAreaYEstado(areaID,EstadoExpediente.RECIBIDO);
            else if(tipo.contentEquals("espera"))
                historial =historialDao.buscarPorAreaYEstado(areaID,EstadoExpediente.ESPERA);
            else if(tipo.contentEquals("finalizados"))
                historial =historialDao.buscarPorAreaYEstadoEnFinalizados(areaID,EstadoExpediente.FINALIZADO);
            else if(tipo.contentEquals("derivados"))
                historial =historialDao.buscarPorDerivadosPorArea(areaID);
            /*else if(tipo.contentEquals("devueltos"))
                historial =historialDao.buscarPorAreaYEstado(areaID,EstadoExpediente.DEVUELTO);*/
            else{
                int[] estados = {EstadoExpediente.NUEVO,EstadoExpediente.RECHAZADO};
                historial =historialDao.buscarPorAreaYEstado(areaID,estados);
            }
            
                
            //rutas =historialDao.buscarRutas(expediente);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar el historial de los expedientes por area", e.getMessage() );
        }
        //Fin
        
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        Date hoy = new Date();        
        hoy.setSeconds(0);
        hoy.setMinutes(0);
        hoy.setHours(0);
        
        long hoyL = hoy.getTime();
        
        long milisegundosPorDia = 1000*60*60*24;
        
        JSONArray aHistoriales = new JSONArray();
        try {
        for(HistorialExpediente h: historial){
            JSONObject oHistorial = new JSONObject();
            oHistorial.put("historialID",h.getHisExpId());
            oHistorial.put("observacion",h.getObservacion() );
            oHistorial.put("areaID",h.getAreaId());
            //oHistorial.put("area",h.getArea().getNom());
            
            oHistorial.put("expedienteID",h.getExpediente().getExpId());
            oHistorial.put("codigo",h.getExpediente().getCodigo());
            long fL = sf.parse( h.getExpediente().getFecIni().toString() ).getTime();
            oHistorial.put("tiempo", (hoyL - fL)/milisegundosPorDia );
            oHistorial.put("folios", h.getExpediente().getFolios() );
            oHistorial.put("modo", h.getExpediente().getTipo() );
            
            oHistorial.put("prioridadID",h.getExpediente().getPrioridad().getPriExpId());
            oHistorial.put("nombrePrioridad",h.getExpediente().getPrioridad().getNom());
            oHistorial.put("area",h.getArea().getNom());
            
            if( h.getAreaId()==areaID ){
                oHistorial.put("estadoID",h.getEstado().getEstExpId());
                oHistorial.put("estado",h.getEstado().getNom());      
                

            }
            else{
                oHistorial.put("area",h.getArea().getNom());
            }
            
            
            
            oHistorial.put("tipoTramiteID",h.getExpediente().getTipoTramiteId());
            
            oHistorial.put("fechaEnvio",h.getFecEnv());
            oHistorial.put("fechaRecepcion",h.getFecRec());
            oHistorial.put("fechaAtencion",h.getFecAte());
            
            
            oHistorial.put("responsableID",h.getResId());
            aHistoriales.put(oHistorial);
        }
            
        } catch (ParseException ex) {
            Logger.getLogger(ListarHistorialTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        /*
        JSONArray aRutas = new JSONArray();
        //si es tupa llamamos a los tipos de area
        if(expediente.getEsTup())
            for(RutaTramite rt:rutas){
                JSONObject oRuta = new JSONObject();
                oRuta.put("rutaID",rt.getRutTraId() );
                oRuta.put("descripcion",rt.getDes() );

                oRuta.put("areaOriID",rt.getTipoAreaOrigen().getTipAreId());
                JSONObject areaOri = new JSONObject();                    
                areaOri.put("nombre",rt.getTipoAreaOrigen().getNom());                    
                oRuta.put("areaDesID",rt.getTipoAreaDestino().getTipAreId());
                JSONObject areaDes = new JSONObject();                    
                areaDes.put("nombre",rt.getTipoAreaDestino().getNom());

                oRuta.put("areaOri",areaOri );
                oRuta.put("areaDes",areaDes );

                aRutas.put(oRuta);
            }
        //si no lo es llamamos a las areas de la orgnizacion
        else
            for(RutaTramite rt:rutas){
                JSONObject oRuta = new JSONObject();
                oRuta.put("rutaID",rt.getRutTraId() );
                oRuta.put("descripcion",rt.getDes() );

                JSONObject areaOri = new JSONObject();
                areaOri.put("areaOriID",rt.getAreaOrigen().getAreId());
                areaOri.put("nombre",rt.getAreaOrigen().getNom());                    
                JSONObject areaDes = new JSONObject();
                areaDes.put("areaDesID",rt.getAreaDestino().getAreId());
                areaDes.put("nombre",rt.getAreaDestino().getNom());

                oRuta.put("areaOri",areaOri );
                oRuta.put("areaDes",areaDes );

                aRutas.put(oRuta);
            }
        
        
        oResponse.put("rutas",aRutas);*/
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",aHistoriales);
    }
    
}

