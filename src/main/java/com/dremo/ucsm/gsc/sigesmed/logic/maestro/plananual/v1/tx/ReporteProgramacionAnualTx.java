package com.dremo.ucsm.gsc.sigesmed.logic.maestro.plananual.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.ProgramacionAnualDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.SesionAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.UnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.*;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.maestro.UtilMaestroLogic;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.layout.border.SolidBorder;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 31/01/2017.
 */
public class ReporteProgramacionAnualTx implements ITransaction {
    private static Logger logger = Logger.getLogger(ReporteProgramacionAnualTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idProg = data.getInt("prog");
        return generarReporteProgramacionAnual(idProg);
    }

    private WebResponse generarReporteProgramacionAnual(int idProg) {
        try{
            ProgramacionAnualDao progDao = (ProgramacionAnualDao) FactoryDao.buildDao("maestro.plan.ProgramacionAnualDao");
            ProgramacionAnual prog = progDao.buscarProgramacionConDatos(idProg);
            String reporteStr = crearReporteMemoria(prog);
            return WebResponse.crearWebResponseExito("Se creo el reporte satisfactoriamente",
                    new JSONObject().put("b64", reporteStr)
            );
        }catch (Exception e){
            logger.log(Level.SEVERE,"generarReporteProgramacionAnual",e);
            return WebResponse.crearWebResponseError("No se puede crear el reporte");
        }
    }
    String crearReporteMemoria(ProgramacionAnual programacion) throws Exception{
        Mitext mitext = new Mitext(false,"SIGESMED");
        mitext.agregarTitulo("Reporte de Reunion");

        JSONObject jsonSub = new JSONObject();
        jsonSub.put("Programacion Curricular",programacion.getDes());
        jsonSub.put("Organizacion",programacion.getOrg().getNom());
        jsonSub.put("Area",programacion.getAre().getNom());
        jsonSub.put("Grado",programacion.getGra().getNom());
        mitext.agregarSubtitulos(jsonSub);

        PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
        Paragraph p = new Paragraph("Objetivos");
        p.setKeepTogether(true);
        p.setFont(bold).setFontSize(12);
        mitext.getDocument().add(p);
        int num = 1;
        GTabla gtabla = new GTabla(new float[]{1.0f,1.5f});
        gtabla.build(new String[]{"N°","Nombre Objetivos"});
        for(ObjetivoProgramacion obj : programacion.getObjetivos()){
            gtabla.processLine(new String[]{String.valueOf(num),obj.getNom()});
            num++;
        }
        mitext.agregarTabla(gtabla);
        //valores
        num = 1;
        p = new Paragraph("Valores");
        p.setKeepTogether(true);
        p.setFont(bold).setFontSize(12);
        mitext.getDocument().add(p);

        gtabla = new GTabla(new float[]{0.5f,1.5f,2.0f});
        gtabla.build(new String[]{"N°","Nombre Valor","situacion significativa"});
        for(ValoresActitudes val : programacion.getValores()){
            gtabla.processLine(new String[]{String.valueOf(num),val.getVal(),val.getSitSig()});
            num++;
        }
        mitext.agregarTabla(gtabla);

        ///unidades didacticas
        UnidadDidacticaDao unidadDao = (UnidadDidacticaDao) FactoryDao.buildDao("maestro.plan.UnidadDidacticaDao");
        List<UnidadDidactica> unidades = unidadDao.buscarUnidadesPorPlanConDatos(programacion.getProAnuId());
        num = 1;
        p = new Paragraph("Unidades de Didacticas");
        p.setKeepTogether(true);
        p.setFont(bold).setFontSize(12);
        mitext.getDocument().add(p);
        for(UnidadDidactica unidad : unidades){
            p = new Paragraph("UNIDAD "+num + ": " + unidad.getTit());
            p.setKeepTogether(true);
            p.setFont(bold).setFontSize(11);
            mitext.getDocument().add(p);
            mitext.agregarParrafo("SITUACION SIGNIFICATIVA: "+ unidad.getSitSig());
            mitext.agregarParrafo("FECHA DE INICIO: " + new SimpleDateFormat("dd-MM-yyyy").format(unidad.getFecIni()));
            mitext.agregarParrafo("FECHA DE FIN: " + new SimpleDateFormat("dd-MM-yyyy").format(unidad.getFecFin()));
            //productos
            mitext.agregarParrafo("Productos: ");
            int j = 1;
            gtabla = new GTabla(new float[]{0.5f,1.5f});
            gtabla.build(new String[]{"N°","Nombre Producto",});
            for(ProductosUnidadAprendizaje prodUni : unidad.getProductos()){
                gtabla.processLine(new String[]{String.valueOf(j),prodUni.getNom()});
                j++;
            }
            mitext.agregarTabla(gtabla);
            //materiales
            mitext.agregarParrafo("Materiales: ");
            j = 1;
            gtabla = new GTabla(new float[]{0.5f,1.5f});
            gtabla.build(new String[]{"N°","Nombre Material",});
            for(MaterialRecursoProgramacion mat: unidad.getMateriales()){
                gtabla.processLine(new String[]{String.valueOf(j),mat.getNom()});
                j++;
            }
            mitext.agregarTabla(gtabla);

            //Evaluaciones
            mitext.agregarParrafo("Evaluaciones: ");
            j = 1;
            gtabla = new GTabla(new float[]{0.5f,1.5f,2.0f});
            gtabla.build(new String[]{"N°","Fecha Evaluacion","Descripcion"});
            for(EvaluacionesUnidadDidactica eva: unidad.getEvaluaciones()){
                gtabla.processLine(new String[]{String.valueOf(j),new SimpleDateFormat("dd-MM-yyyy").format(eva.getFecEva()),eva.getDes()});
                j++;
            }
            mitext.agregarTabla(gtabla);
            //aprendizajes esperados
            mitext.agregarParrafo("Aprendizajes Esperados: ");
            gtabla = new GTabla(new float[]{2.5f,2.5f,2.5f});
            gtabla.build(new String[]{"Competencias","Capacidades","Indicadores"});


            Map<CompetenciaAprendizaje,List<CapacidadAprendizaje>> competenciasUnidad = UtilMaestroLogic.compenciasUnidadToMap(unidad.getCompetenciasUnidad());
            insertarTablaReporte(mitext,gtabla,competenciasUnidad);
            //mitext.agregarTabla(gtabla);
            //Sesiones
            SesionAprendizajeDao sesDao = (SesionAprendizajeDao) FactoryDao.buildDao("maestro.plan.SesionAprendizajeDao");
            mitext.agregarParrafo("Sesiones: ");
            j = 1;
            for(SesionAprendizaje sesion : sesDao.listarSesionesConDatos(unidad.getUniDidId())){
                mitext.agregarParrafo("SESION: " + num + "." + j + " " + sesion.getTit());
                mitext.agregarParrafo("FECHA DE INICIO: " + new SimpleDateFormat("dd-MM-yyyy").format(sesion.getFecIni()));
                mitext.agregarParrafo("FECHA DE FIN: " + new SimpleDateFormat("dd-MM-yyyy").format(sesion.getFecFin()));
                //Aprendizajes Esperados de Sesion
                gtabla = new GTabla(new float[]{2.5f,2.5f,2.5f});
                gtabla.build(new String[]{"Competencias","Capacidades","Indicadores"});
                Map<CompetenciaAprendizaje,List<CapacidadAprendizaje>> competenciasSesion = UtilMaestroLogic.compenciasSesionToMap(sesion.getIndicadoresSesion());
                insertarTablaReporte(mitext,gtabla,competenciasSesion);
                //mitext.agregarTabla(gtabla);
                j++;

            }
            num++;
        }
        mitext.cerrarDocumento();

        return mitext.encodeToBase64();
    }
    private  void insertarTablaReporte(Mitext mitext,GTabla gtabla,Map<CompetenciaAprendizaje,List<CapacidadAprendizaje>> competencias){
        for(Map.Entry<CompetenciaAprendizaje,List<CapacidadAprendizaje>> entry : competencias.entrySet()){
            int sizeComp = 0;
            for(CapacidadAprendizaje capacidad : entry.getValue()){
                sizeComp += capacidad.getIndicadores().size();
            }
            if(sizeComp > 0){
                Cell cellCompetencia = new Cell(sizeComp,1).add(new Paragraph(entry.getKey().getNom()));
                gtabla.addCell(cellCompetencia);
                for(CapacidadAprendizaje capacidad : entry.getValue()){
                    Cell cellCapacidad = new Cell(capacidad.getIndicadores().size(),1).add(new Paragraph(capacidad.getNom()));
                    gtabla.addCell(cellCapacidad);
                    for(IndicadorAprendizaje indicadorAprendizaje : capacidad.getIndicadores()){
                        gtabla.addCell(new Cell().add(new Paragraph(indicadorAprendizaje.getNomInd())));
                    }
                }
            }
        }
        mitext.agregarTabla(gtabla);
    }
}
