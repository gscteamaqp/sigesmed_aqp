package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrador on 14/12/2016.
 */
@Entity
@Table(name = "productos_unidad_aprendizaje", schema = "pedagogico")
public class ProductosUnidadAprendizaje implements java.io.Serializable {

    @Id
    @Column(name = "pro_uni_apr_id", nullable = false, unique = true)
    @SequenceGenerator(name = "productos_unidad_aprendizaje_pro_uni_apr_id_seq",sequenceName = "pedagogico.productos_unidad_aprendizaje_pro_uni_apr_id_seq")
    @GeneratedValue(generator = "productos_unidad_aprendizaje_pro_uni_apr_id_seq")
    private int proUniAprId;
    @Column(name = "nom",length = 256,nullable = false)
    private String nom;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "uni_did_id", nullable = false)
    private UnidadDidactica unidad;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "est_reg", length = 1)
    private Character estReg;

    @Transient
    private Boolean editable = new Boolean(false);


    public ProductosUnidadAprendizaje() {
    }

    public ProductosUnidadAprendizaje(String nom) {
        this.nom = nom;
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public int getProUniAprId() {
        return proUniAprId;
    }

    public void setProUniAprId(int proUniAprId) {
        this.proUniAprId = proUniAprId;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public UnidadDidactica getUnidad() {
        return unidad;
    }

    public void setUnidad(UnidadDidactica unidad) {
        this.unidad = unidad;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public Boolean getEditable() {
        return editable;
    }

    public void setEditable(Boolean editable) {
        this.editable = editable;
    }
}
