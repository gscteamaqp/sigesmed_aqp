/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.tipo_organizacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.TipoOrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Rol;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TipoOrganizacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.ArrayList;
import java.util.Date;
import org.json.JSONArray;

/**
 *
 * @author Administrador
 */
public class InsertarTipoOrganizacionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        TipoOrganizacion nuevoTipoOrg = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            String codigo = requestData.getString("codigo");
            String nombre = requestData.getString("nombre");
            String descripcion = requestData.getString("descripcion");
            String estado = requestData.getString("estado");
            JSONArray roles = requestData.getJSONArray("roles");
            nuevoTipoOrg = new TipoOrganizacion((int)0, codigo, nombre, descripcion, new Date(), 1, estado.charAt(0), null);
            
            nuevoTipoOrg.setRoles(new ArrayList<Rol>());
            for(int i = 0; i < roles.length();i++){
                JSONObject bo = roles.getJSONObject(i);
                nuevoTipoOrg.getRoles().add( new Rol( bo.getInt("rolID") ) );
            }
        
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        TipoOrganizacionDao tipoOrgDao = (TipoOrganizacionDao)FactoryDao.buildDao("TipoOrganizacionDao");
        try{
            tipoOrgDao.insert(nuevoTipoOrg);
        
        }catch(Exception e){
            System.out.println("No se pudo registrar el Tipo de Organizacion\n"+e);
            return WebResponse.crearWebResponseError("No se pudo registrar el Tipo de Organizacion", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("tipoOrganizacionID",nuevoTipoOrg.getTipOrgId());
        oResponse.put("fecha",nuevoTipoOrg.getFecMod().toString());
        return WebResponse.crearWebResponseExito("El registro del Tipo de Organizacion se realizo correctamente", oResponse);
        //Fin
    }
    
}

