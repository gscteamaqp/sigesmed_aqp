/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.cuenta_contable.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.CuentaContableDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentaContable;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class EliminarCuentaContableTx   implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int cuentaContableID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            cuentaContableID = requestData.getInt("cuentaContableID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo eliminar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        CuentaContableDao cuentaContableDao = (CuentaContableDao)FactoryDao.buildDao("sci.CuentaContableDao");
        try{
            cuentaContableDao.delete(new CuentaContable(cuentaContableID));
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar la cuenta contable del Sistema\n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar la Cuenta Contable", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("La Cuenta Contable se elimino correctamente");
        //Fin
    }
}

    

