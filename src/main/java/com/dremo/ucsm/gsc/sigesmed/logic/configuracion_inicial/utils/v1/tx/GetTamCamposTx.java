/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.utils.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import java.util.List;

/**
 *
 * @author Razor
 */
public class GetTamCamposTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */
        boolean colExists=false;
        Session session = HibernateUtil.getSessionFactory().openSession();
        
        JSONArray miArray = new JSONArray();
        JSONObject oResponse = new JSONObject();
        try{
            // Nombre de la tabla y columnas (para obtener su tamaño)
            JSONObject requestData = (JSONObject)wr.getData();
            
            
            JSONObject columns = requestData.getJSONObject("nomcol");
            String nomTabla = requestData.getString("target");
            colExists = columns.length()<0;
            if(colExists){
                System.out.println("cols");
                List<Integer> tamCamposVarChar = null;
                List<Integer> tamCamposNumeric = null;
                //HQL para obtener los tamaños de los campos de tipo varchar
                String hql = "SELECT character_maximum_length FROM Columns where table_name = '"+ nomTabla +"' and data_type='character varying' ";

                //Cuando se tiene columnas específicas
                colExists=true;
                hql+= " and column_name in (";
                for (int j=1;j<=columns.length(); j++){
                    hql+="'"+columns.getString("col"+j)+"'";
                    hql+=(j!=columns.length()) ? " ,":"";
                }
                hql+=")" ;

                Query query = session.createQuery(hql);
                tamCamposVarChar = query.list();
                int i=1;
                for (int tcv:tamCamposVarChar){
                    oResponse.put(columns.getString("col"+i++), tcv);
                }
                
                //HQL para obtener los tamaños de los campos de tipo numeric
                hql = "SELECT numeric_precision FROM Columns where table_name = '"+ nomTabla +"' and data_type = 'numeric'";
                //Cuando se tiene columnas específicas
                hql+= " and column_name in (";
                for (int j=1;j<=columns.length(); j++){
                    hql+="'"+columns.getString("col"+j)+"'";
                    hql+=(j!=columns.length()) ? " ,":"";
                }
                hql+=")" ;
                query = session.createQuery(hql);
                tamCamposNumeric = query.list();
                for (int tcn:tamCamposNumeric){
                    oResponse.put(columns.getString("col"+i++), tcn);
                }
            }
            else{
                System.out.println("nocols");
                List<String> tamCamposVarChar = null;
                List<String> tamCamposNumeric = null;
                //HQL para obtener los tamaños de los campos de tipo varchar
                String hql = "SELECT column_name||','||character_maximum_length FROM Columns where table_name = '"+ nomTabla +"' and  character_maximum_length IS NOT NULL and data_type in ('character varying', 'character')";

                Query query = session.createQuery(hql);
                tamCamposVarChar = query.list();
                int i=1;
                for (String tcv:tamCamposVarChar){
                    System.out.println(tcv);
                    oResponse.put(tcv.substring(0,tcv.indexOf(',')), tcv.substring(tcv.indexOf(',')+1));
                }
                
                //HQL para obtener los tamaños de los campos de tipo numeric
                hql = "SELECT column_name||','||numeric_precision FROM Columns where table_name = '"+ nomTabla +"' and numeric_precision IS NOT NULL and data_type = 'numeric'";
                
                query = session.createQuery(hql);
                tamCamposNumeric = query.list();
                for (String tcn:tamCamposNumeric){
                    System.out.println(tcn);
                    oResponse.put(tcn.substring(0,tcn.indexOf(',')), tcn.substring(tcn.indexOf(',')+1));
                }
            }        
        }catch(Exception e){
            System.out.println("No se pudo obtener los tamaños de los campos \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo obtener los tamaños de los campos \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        //Fin
        
        miArray.put(oResponse);
        
        //return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);   
    }
    
}