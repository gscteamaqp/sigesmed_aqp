package com.dremo.ucsm.gsc.sigesmed.core.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="usuario_session" )
public class UsuarioSession  implements java.io.Serializable {

    @Id 
    @Column(name="usu_ses_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_usuario_session", sequenceName="usuario_session_usu_ses_id_seq" )
    @GeneratedValue(generator="secuencia_usuario_session")
    private int usuSesId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="org_id", nullable=false)
    private Organizacion organizacion;    
    @Column(name="rol_id",updatable = false,insertable = false)
    private int rolId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="rol_id", nullable=false)
    private Rol rol;
    
    @Column(name="are_id")
    private Integer areId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="are_id",updatable = false,insertable = false)
    private Area area;
    @Column(name="usu_id",updatable = false,insertable = false)
    private int usuId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="usu_id", nullable=false)
    private Usuario usuario;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="usu_id", nullable=false,updatable = false,insertable = false)
    private Persona persona;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_cre", nullable=false, length=29,updatable = false)
    private Date fecCre;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_mod", length=29)
    private Date fecMod;
    @Column(name="usu_mod", nullable=false)
    private int usuMod;
    @Column(name="est_reg", nullable=false, length=1)
    private char estReg;

    public UsuarioSession() {
    }
    public UsuarioSession(int usuSesId) {
        this.usuSesId = usuSesId;
    }

	
    public UsuarioSession(int usuSesId, Organizacion organizacion, Date fecCre, int usuMod, char estReg) {
        this.usuSesId = usuSesId;
        this.organizacion = organizacion;
        this.fecCre = fecCre;
        this.usuMod = usuMod;
        this.estReg = estReg;
    }
    public UsuarioSession(int usuSesId, Organizacion organizacion, Rol rol, Usuario usuario, Date fecCre, Date fecMod, int usuMod, char estReg) {
       this.usuSesId = usuSesId;
       this.organizacion = organizacion;
       this.rol = rol;
       this.usuario = usuario;
       this.fecCre = fecCre;
       this.fecMod = fecMod;
       this.usuMod = usuMod;
       this.estReg = estReg;
    }
   
     
    public int getUsuSesId() {
        return this.usuSesId;
    }
    
    public void setUsuSesId(int usuSesId) {
        this.usuSesId = usuSesId;
    }

    public Organizacion getOrganizacion() {
        return this.organizacion;
    }
    
    public void setOrganizacion(Organizacion organizacion) {
        this.organizacion = organizacion;
    }

    public int getRolId() {
        return this.rolId;
    }    
    public void setRolId(int rolId) {
        this.rolId = rolId;
    }
    public Rol getRol() {
        return this.rol;
    }
    
    public void setRol(Rol rol) {
        this.rol = rol;
    }


    public Date getFecCre() {
        return this.fecCre;
    }
    
    public void setFecCre(Date fecCre) {
        this.fecCre = fecCre;
    }

    public Date getFecMod() {
        return this.fecMod;
    }
    
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    
    public int getUsuMod() {
        return this.usuMod;
    }
    
    public void setUsuMod(int usuMod) {
        this.usuMod = usuMod;
    }

    
    public char getEstReg() {
        return this.estReg;
    }
    
    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }

    public int getUsuId() {
        return this.usuId;
    }    
    public void setUsuId(int usuId) {
        this.usuId = usuId;
    }
    public Usuario getUsuario() {
        return this.usuario;
    }
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
    public Persona getPersona() {
        return this.persona;
    }
    public void setPersona(Persona persona) {
        this.persona = persona;
    }
    
    public Integer getAreId() {
        return this.areId;
    }
    public void setAreId(Integer areId) {
        this.areId = areId;
    }
    public Area getArea() {
        return this.area;
    }
    public void setArea(Area area) {
        this.area = area;
    }



}


