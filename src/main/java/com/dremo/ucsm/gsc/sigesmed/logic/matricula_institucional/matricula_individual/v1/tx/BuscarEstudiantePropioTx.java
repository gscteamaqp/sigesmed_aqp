package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.matricula_individual.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.EstudianteMMIDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.EstudianteMMI;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BuscarEstudiantePropioTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        EstudianteMMIDaoHibernate dh = new EstudianteMMIDaoHibernate();
        EstudianteMMI estudianteMmi;
        JSONObject requestData = (JSONObject) wr.getData();
        JSONArray estudiantes = new JSONArray();

        try {
            long perId = requestData.getLong("perId");
            estudianteMmi = dh.find4CodPer(perId);

            if (estudianteMmi == null) {
                return WebResponse.crearWebResponseError("No se encontraron estudiantes! ", "Usuario null");
            }

            if (estudianteMmi != null) {
                JSONObject temp = new JSONObject();

                if ((Long) estudianteMmi.getPersona().getPerId() != null) {
                    temp.put("estId", estudianteMmi.getPersona().getPerId());
                } else {
                    temp.put("estId", -1);
                }
                if (estudianteMmi.getPersona().getNom() != null) {
                    temp.put("estNom", estudianteMmi.getPersona().getNom());
                } else {
                    temp.put("estNom", "-");
                }
                if (estudianteMmi.getPersona().getNom() != null) {
                    temp.put("estApePat", estudianteMmi.getPersona().getApePat());
                } else {
                    temp.put("estApePat", "-");
                }
                if (estudianteMmi.getPersona().getNom() != null) {
                    temp.put("estApeMat", estudianteMmi.getPersona().getApeMat());
                } else {
                    temp.put("estApeMat", "-");
                }
                if (estudianteMmi.getPersona().getNom() != null) {
                    temp.put("estDni", estudianteMmi.getPersona().getDni());
                } else {
                    temp.put("estDni", "desconocido");
                }
                if (estudianteMmi.getPersona().getNom() != null) {
                    temp.put("estCodEst", estudianteMmi.getCodEst());
                } else {
                    temp.put("estCodEst", "desconocido");
                }

                if (estudianteMmi.getEstMat() != null) {
                    temp.put("estMat", estudianteMmi.getEstMat());
                    if (estudianteMmi.getEstMat()) {
                        temp.put("estMatNom", "Activa");
                    } else {
                        temp.put("estMatNom", "Inactiva");
                    }
                } else {
                    temp.put("estMat", false);
                    temp.put("estMatNom", "Inactiva");
                }

                if (estudianteMmi.getUltGraCul() != null) {
                    temp.put("ultGraCul", estudianteMmi.getUltGraCul());
                    temp.put("ultGraCulNom", selectLastGrade(estudianteMmi.getUltGraCul()));
                } else {
                    temp.put("ultGraCul", 0);
                    temp.put("ultGraCulNom", "Sin Antecedentes");
                }

                String lastSec = "";
                if (estudianteMmi.getUltSec() != null) {
                    lastSec = lastSec + estudianteMmi.getUltSec().toString();
                    temp.put("ultSec", lastSec);
                } else {
                    lastSec = "-";
                    temp.put("ultSec", lastSec);
                }

                if (estudianteMmi.getOrgId() != null) {
                    temp.put("estOrgOri", estudianteMmi.getOrgId());
                } else {
                    temp.put("estOrgOri", -1);
                }

                estudiantes.put(temp);

            } else {
                return WebResponse.crearWebResponseError("No se encontro estudiantes! ", "exepcion");
            }

        } catch (NumberFormatException | JSONException e) {
            return WebResponse.crearWebResponseError("No se encontro estudiantes! ", e.getMessage());
        }

        return WebResponse.crearWebResponseExito("Se encontro los estudiantes", estudiantes);
    }

    private String selectLastGrade(String tipo) {
        String returnKey;
        try {
            switch (Integer.parseInt(tipo)) {
                case 0: {
                    returnKey = "Sin Antecedentes";
                    break;
                }
                case 1: {
                    returnKey = "Inicial 0 a 2 años";
                    break;
                }
                case 2: {
                    returnKey = "Inicial 3 a 5 años";
                    break;
                }
                case 3: {
                    returnKey = "1ro de Primaria";
                    break;
                }
                case 4: {
                    returnKey = "2do de Primaria";
                    break;
                }
                case 5: {
                    returnKey = "3ro de Primaria";
                    break;
                }
                case 6: {
                    returnKey = "4to de Primaria";
                    break;
                }
                case 7: {
                    returnKey = "5to de Primaria";
                    break;
                }
                case 8: {
                    returnKey = "6to de Primaria";
                    break;
                }
                case 9: {
                    returnKey = "1ro de Secundaria";
                    break;
                }
                case 10: {
                    returnKey = "2do se Secundaria";
                    break;
                }
                case 11: {
                    returnKey = "3ro de Secundaria";
                    break;
                }
                case 12: {
                    returnKey = "4to de Secundaria";
                    break;
                }
                case 13: {
                    returnKey = "5to de Secundaria";
                    break;
                }
                default: {
                    returnKey = "Sin Antecedentes";
                    break;
                }

            }
        } catch (Exception e) {
            returnKey = "Sin Antecedentes";
        }
        return returnKey;
    }

}
