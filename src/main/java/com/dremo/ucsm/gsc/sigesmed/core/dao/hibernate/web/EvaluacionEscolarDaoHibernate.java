/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.web;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.EvaluacionEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaEvaluacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaEvaluacionModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.RespuestaEvaluacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.EvaluacionEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.MensajeCalificacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.PreguntaEvaluacion;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author abel
 */
public class EvaluacionEscolarDaoHibernate extends GenericDaoHibernate<EvaluacionEscolar> implements EvaluacionEscolarDao{
    
    @Override
    public void insertarPreguntas(List<PreguntaEvaluacion> preguntas){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{            
            //insertando varias preguntas
            for(PreguntaEvaluacion p : preguntas){
                
                String hql = "insert into pedagogico.pregunta_evaluacion "
                        + "( eva_esc_id,pre_eva_id,tit,con_pre,alt_pre,ord_pre,tip_pre,res_pre,tie_est,pun ) "
                        + "VALUES (:p1,:p2,:p3,:p4,:p5,:p6,:p7,:p8,:p9,:p10)";
                Query query = session.createSQLQuery(hql);
                query.setParameter("p1", p.getEvaluacion().getEvaEscId());
                query.setParameter("p2", p.getPreEvaId());
                query.setParameter("p3", p.getTitulo());
                query.setParameter("p4", p.getPregunta());
                query.setParameter("p5", p.getAlternativa());
                query.setParameter("p6", p.getOrden());
                query.setParameter("p7", p.getTipo());
                query.setParameter("p8", p.getRespuesta());
                query.setParameter("p9", p.getTiempo());
                query.setParameter("p10", p.getPuntos());

                query.executeUpdate();
            }
            miTx.commit();        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo registrar las preguntas\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo registrar las preguntas\n"+ e.getMessage());
        }
        finally{
            session.close();
        }
    }
    @Override
    public void eliminarPreguntas(int evaluacionID) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            //eliminando las preguntas de la evaluacion
            String hql = "DELETE FROM PreguntaEvaluacion WHERE evaluacion =:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", new EvaluacionEscolar(evaluacionID));
            query.executeUpdate();    
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo eliminar las preguntas de la evaluacion \n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    @Override
    public void insertarMensajes(List<MensajeCalificacion> mensajes){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            
            //insertando varias mensajes
            for(MensajeCalificacion m : mensajes){
                
                String hql = "insert into pedagogico.mensaje_calificacion "
                        + "( eva_esc_id,men_cal_id,men,pun ) "
                        + "VALUES (:p1,:p2,:p3,:p4)";
                Query query = session.createSQLQuery(hql);
                query.setParameter("p1", m.getEvaluacion().getEvaEscId());
                query.setParameter("p2", m.getMenCalId());
                query.setParameter("p3", m.getMensaje());
                query.setParameter("p4", m.getPuntos());

                query.executeUpdate();
            }
            miTx.commit();        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo registrar los mensajes\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo registrar los mensajes\n"+ e.getMessage());
        }
        finally{
            session.close();
        }
    }
    @Override
    public void eliminarMensajes(int evaluacionID) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            //eliminando los mensajes de la evaluacion
            String hql = "DELETE FROM MensajeCalificacion WHERE evaluacion =:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", new EvaluacionEscolar(evaluacionID));
            query.executeUpdate();    
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo eliminar los mensajes de la evaluacion \n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    @Override
    public void enviarEvaluacion(EvaluacionEscolar evaluacion,BandejaEvaluacion bandeja){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            String hql = "UPDATE EvaluacionEscolar e SET e.fecEnv=:p2,e.fecIni=:p3,e.fecFin=:p4,e.estado=:p5 WHERE e.evaEscId =:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", evaluacion.getEvaEscId());
            query.setParameter("p2", evaluacion.getFecEnv());
            query.setParameter("p3", evaluacion.getFecIni());
            query.setParameter("p4", evaluacion.getFecFin());
            query.setParameter("p5", evaluacion.getEstado());
            query.executeUpdate();
            
            //actualizando la bandeja de evaluacion
            session.persist(bandeja);
            miTx.commit();
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo enviar la evaluacion a un alumno\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo enviar la evaluacion a un alumno\n"+ e.getMessage());
        }
        finally{
            session.close();
        }
    }
    @Override
    public void enviarEvaluacion(EvaluacionEscolar evaluacion,List<BandejaEvaluacion> bandejas){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            String hql = "UPDATE EvaluacionEscolar e SET e.fecEnv=:p2,e.fecIni=:p3,e.estado=:p4 WHERE e.evaEscId =:p1";            
            Query query = session.createQuery(hql);
            query.setParameter("p1", evaluacion.getEvaEscId());
            query.setParameter("p2", evaluacion.getFecEnv());
            query.setParameter("p3", evaluacion.getFecIni());
            query.setParameter("p4", evaluacion.getEstado());
            query.executeUpdate();
            
            //actualizando la bandeja de evaluacions
            for(BandejaEvaluacion bt: bandejas){
                session.persist(bt);
            }                
            miTx.commit();        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo enviar la evaluacion a los alumnos\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo enviar la evaluacion a los alumnos\n"+ e.getMessage());
        }
        finally{
            session.close();
        }
    }

    @Override
    public List<EvaluacionEscolar> buscarPorPlanEstudios(int planID){
        List<EvaluacionEscolar> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT e FROM EvaluacionEscolar e WHERE e.plaEstId=:p1 and e.estReg='A'";
            Query query = session.createQuery(hql);
            query.setParameter("p1", planID);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las evaluaciones por plan de estudios\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las evaluaciones por plan de estudios\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public List<EvaluacionEscolar> buscarPorDocente(int planID,int docenteID){
        List<EvaluacionEscolar> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT DISTINCT e FROM EvaluacionEscolar e LEFT JOIN FETCH e.preguntas WHERE e.plaEstId=:p1 and e.usuMod=:p2 and e.estReg='A'";
            Query query = session.createQuery(hql);
            query.setParameter("p1", planID);
            query.setParameter("p2", docenteID);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las evaluaciones por plan de estudios y docente\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las evaluaciones por plan de estudios y docente\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public List<BandejaEvaluacionModel> buscarAlumnosQueCumplieronEvaluacion(int evaluacionID){
        List<BandejaEvaluacionModel> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaEvaluacionModel(b.banEvaId,b.fecEnt,b.fecVis,b.nota,b.not_lit,b.estado,b.alumno.nom,b.alumno.apePat,b.alumno.apeMat,b.evaEscId,b.alumnoID) FROM BandejaEvaluacion b WHERE b.evaEscId=:p1 and (b.estado='E' or b.estado='C')";
            
            Query query = session.createQuery(hql);
            query.setParameter("p1", evaluacionID);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los alumnos que cumplieron con evaluacion\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los alumnos que cumplieron con evaluacion\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public List<RespuestaEvaluacion> verRespuestasEvaluacion(int bandejaEvaluacionID){
        List<RespuestaEvaluacion> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT re FROM RespuestaEvaluacion re WHERE re.bandejaEvaluacion.banEvaId=:p1";// and b.estado='E'";
            
            Query query = session.createQuery(hql);
            query.setParameter("p1", bandejaEvaluacionID);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las respuestas de la evaluacion resuelta\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las respuestas de la evaluacion resuelta\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    
    @Override
    public List<BandejaEvaluacion> buscarEvaluacionesPorAlumno(int planID,int alumnoID){
        List<BandejaEvaluacion> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT DISTINCT be FROM BandejaEvaluacion be JOIN FETCH be.evaluacionEscolar LEFT JOIN FETCH be.documentos WHERE be.evaluacionEscolar.plaEstId=:p1 and be.alumnoID=:p2";
            Query query = session.createQuery(hql);
            query.setParameter("p1", planID);
            query.setParameter("p2", alumnoID);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las evaluacions por plan de estudios y alumno\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las evaluacions por plan de estudios y alumno\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    
    @Override
    public void enviarEvaluacionResuelta(BandejaEvaluacion bandeja ){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            //insertando todos los documentos adjuntos de la evaluacion
            for(RespuestaEvaluacion td : bandeja.getRespuestas()){
                
                String hql = "insert into pedagogico.evaluacion_documento_adjunto ( ban_eva_esc_id, res_eva_id, res ) VALUES (:p1,:p2,:p3)";
                Query query = session.createSQLQuery(hql);
                query.setParameter("p1", bandeja.getBanEvaId());
                query.setParameter("p2", td.getResEvaId());
                query.setParameter("p3", td.getRespuesta());

                query.executeUpdate();
            }
            //actualizando el estado y la fecha de entrega de la evaluacion
            String hql = "UPDATE BandejaEvaluacion bt SET bt.fecEnt=:p2,bt.estado=:p3 WHERE bt.banEvaId=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", bandeja.getBanEvaId());
            query.setParameter("p2", bandeja.getFecEnt());
            query.setParameter("p3", bandeja.getEstado());
            
            query.executeUpdate();
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo enviar la evaluacion resuelta\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    
    @Override
    public void calificarEvaluacionResuelta(BandejaEvaluacion bandeja ){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{            
            String hql = "UPDATE BandejaEvaluacion bt SET bt.estado=:p2, bt.nota=:p3, bt.fecVis=:p4 WHERE bt.banEvaId =:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", bandeja.getBanEvaId());
            query.setParameter("p2", bandeja.getEstado());
            query.setParameter("p3", bandeja.getNota());
            query.setParameter("p4", bandeja.getFecVis());
            query.executeUpdate();
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo calificar la evaluacion\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    
    @Override
    public void finalizarEvaluacion(EvaluacionEscolar evaluacion){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{            
            String hql = "UPDATE EvaluacionEscolar t SET t.estado=:p2,t.fecMod=:p3 WHERE t.evaEscId =:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", evaluacion.getEvaEscId() );
            query.setParameter("p2", evaluacion.getEstado());
            query.setParameter("p3", evaluacion.getFecMod());
            query.executeUpdate();
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo dar por finalizado la evaluacion\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    @Override
    public void evaluacionesFueraDeTiempo(List<Integer> evaluaciones){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            for(Integer evaluacionID:evaluaciones){
                String hql = "UPDATE BandejaEvaluacion bt SET bt.estado=:p2 WHERE bt.banEvaId=:p1";
                Query query = session.createQuery(hql);
                query.setParameter("p1", evaluacionID);
                query.setParameter("p2", 'F');
                query.executeUpdate();
            }            
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo actualizar el estado a finalizado de las evaluacions\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    @Override
    public List<MensajeCalificacion> buscarMensajesPorEvaluacion(int evaluacionID){
        List<MensajeCalificacion> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT mc FROM MensajeCalificacion mc WHERE mc.evaluacion.evaEscId=:p1";// and b.estado='E'";
            
            Query query = session.createQuery(hql);
            query.setParameter("p1", evaluacionID);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los mensajes de la evaluacion\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los mensajes de la evaluacion\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public List<PreguntaEvaluacion> buscarPreguntasPorEvaluacion(int evaluacionID){
        List<PreguntaEvaluacion> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT pe FROM PreguntaEvaluacion pe WHERE pe.evaluacion.evaEscId=:p1";// and b.estado='E'";
            
            Query query = session.createQuery(hql);
            query.setParameter("p1", evaluacionID);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las preguntas de la evaluacion\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las preguntas de la evaluacion\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }

    @Override
    public EvaluacionEscolar obtenerEvaluacion(int evaluacionID) {
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        EvaluacionEscolar eva_escolar;
        try{
            String hql = "SELECT ev FROM EvaluacionEscolar ev JOIN FETCH ev.bandeja_evaluacion WHERE ev.eva_maes_id=:p1";// and b.estado='E'";
            
            Query query = session.createQuery(hql);
            query.setParameter("p1",evaluacionID);
            eva_escolar = ((EvaluacionEscolar)query.uniqueResult());
           
           if(eva_escolar == null){
               hql = "SELECT ev FROM EvaluacionEscolar ev  WHERE ev.eva_maes_id=:p1";
                query = session.createQuery(hql);
                query.setParameter("p1",evaluacionID);
                eva_escolar = ((EvaluacionEscolar)query.uniqueResult());
           }
           
        }
        catch(Exception e){
               System.out.println("No se pudo Obtener la evaluacion\n "+ e.getMessage());
               throw new UnsupportedOperationException("No se pudo Obtener la evaluacion\n "+ e.getMessage());            
        }
         finally{
            session.close();
        }
        return eva_escolar;
    }

    @Override
    public void insertarRespuestasAlumno(List<RespuestaEvaluacion> respuestas) {
        
       Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{            
            //insertando las respuestas del alumno
            for(RespuestaEvaluacion r : respuestas){
                
                String hql = "insert into pedagogico.respuesta_evaluacion "
                        + "( ban_eva_esc_id,res_eva_id,res,pun,tie_res) "
                        + "VALUES (:p1,:p2,:p3,:p4,:p5)";
                Query query = session.createSQLQuery(hql);
                query.setParameter("p1", r.getBan_eva_esc_id());
                query.setParameter("p2", r.getResEvaId());
                query.setParameter("p3", r.getRespuesta());
                query.setParameter("p4", r.getPuntos());
                query.setParameter("p5", r.getTie_res());
               

                query.executeUpdate();
            }
            miTx.commit();        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo registrar las Respuestas del Alumno\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo registrar las Respuestas del Alumno\n"+ e.getMessage());
        }
        finally{
            session.close();
        } 
        
        
        
     //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BandejaEvaluacion obtenerEvaluacionPreguntas(int bandejaID) {
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        BandejaEvaluacion eva_escolar;
         try{
            String hql = "SELECT ba FROM BandejaEvaluacion ba JOIN FETCH ba.evaluacionEscolar eva JOIN FETCH eva.preguntas  WHERE ba.banEvaId=:p1";// and b.estado='E'";
            
            Query query = session.createQuery(hql);
            query.setParameter("p1",bandejaID);
            eva_escolar = ((BandejaEvaluacion)query.uniqueResult());
           
           if(eva_escolar == null){
              return null;
                
           }
           return eva_escolar;
           
        }
         catch(Exception e){
              System.out.println("No se pudo Obtener la Evaluacion del Alumno solicitado\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo Obtener la Evaluacion del Alumno solicitado \n"+ e.getMessage());
         }
        
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

