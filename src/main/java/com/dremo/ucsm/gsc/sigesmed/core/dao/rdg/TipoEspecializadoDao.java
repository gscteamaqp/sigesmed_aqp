/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.rdg;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.TipoEspecializado;
import java.util.List;

/**
 *
 * @author ucsm
 */
public interface TipoEspecializadoDao extends GenericDao<TipoEspecializado>{
    TipoEspecializado tipoEspById(int id);
    List<TipoEspecializado> getAllTiposEsp();
    List<TipoEspecializado> getAllTiposEspActivos();
    public TipoEspecializado getByAlias(String alias);
}
