/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.mantenimiento.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mnt.MensajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mnt.Mensaje;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class EliminarMensajeTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {        
        int menID=0;
        boolean flag=false;
        try{
            JSONObject requestData=(JSONObject)wr.getData();
            menID=requestData.getInt("menID");
            flag=requestData.getBoolean("flag");
        }catch(Exception e){
             System.out.println("No se pudo obtener el mensaje: " +e.getMessage());
            return WebResponse.crearWebResponseError("No se pudo obtener el mensaje",e.getMessage());
        }
        
        
        
        
        MensajeDao menDao=(MensajeDao)FactoryDao.buildDao("mnt.MensajeDao");
        if(flag)
            menDao.eliminarMensajeAutor(menID);
        else
            menDao.eliminarMensajeDestinatario(menID);
        
        
        
        return WebResponse.crearWebResponseExito("Se elimino con éxito el mensaje");
    }
    
}

