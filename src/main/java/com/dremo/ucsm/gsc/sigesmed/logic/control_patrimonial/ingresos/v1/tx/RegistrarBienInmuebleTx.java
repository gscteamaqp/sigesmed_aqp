/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BienesInmuebles;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.DetalleTecnicoInmuebles;

import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.AmbientesDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.DetalleTecnicoMueblesDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.DetalleTecnicoInmueblesDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.ValorContableDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.BienInmuebleDAO;

import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class RegistrarBienInmuebleTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {

        JSONObject requestData = (JSONObject) wr.getData();
        BienesInmuebles bi = null;
        DetalleTecnicoInmuebles dti = null;
        String msgResponse = "";

        try {
            
            boolean actualizarBien = requestData.getBoolean("actualizar_bien");
            
            String tip_dir = requestData.getString("tip_dire");
            String descripcion = requestData.getString("descripcion");
            String direccion = requestData.getString("direccion");
            int nro = requestData.getInt("nro");
            String mz = requestData.getString("mz");
            int lt = requestData.getInt("lt");
            String departamento = requestData.getString("departamento");
            String provincia = requestData.getString("provincia");
            String distrito = requestData.getString("distrito");
            int user = requestData.getInt("user");
            int org_id = requestData.getInt("orgId");
            Date fec_reg = new Date();
            char est_reg = 'A';

            bi = new BienesInmuebles(0, descripcion, direccion, nro, mz, lt, departamento, provincia, distrito, fec_reg, user, fec_reg, est_reg);
            bi.setTipo_dir(tip_dir);
            int amb_id = requestData.getInt("amb_id");
            bi.setAmb_id(amb_id);
            bi.setOrg_id(org_id);

            String propiedad = requestData.getString("propiedad");
            String tipo_terreno = requestData.getString("tipo_terreno");

            boolean est_sane = requestData.getBoolean("est_sanea");
            char est_sanea;
            if (est_sane == true) {
                est_sanea = 'S';
            } else {
                est_sanea = 'N';
            }

            int area = requestData.getInt("area");
            String um = requestData.getString("um");
            String part_elec = requestData.getString("part_elec");
            String reg_sinabip = requestData.getString("reg_sinabip");
            String nro_fic_reg = requestData.getString("nro_fic_reg");

            //     dti = new DetalleTecnicoInmuebles(bi.getBie_inm_id(),propiedad,tipo_terreno,est_sanea,area,um,part_elec,reg_sinabip,nro_fic_reg);
            /*   
            bi.setDtm(new DetalleTecnicoInmuebles(bi.getBie_inm_id(),propiedad,tipo_terreno,est_sanea,area,um,part_elec,reg_sinabip,nro_fic_reg)); 
            bi.setDtm(dti);
             */
            
            BienInmuebleDAO bien_inm_dao = (BienInmuebleDAO) FactoryDao.buildDao("scp.BienInmuebleDAO");
            DetalleTecnicoInmueblesDAO det_tec_inm_dao = (DetalleTecnicoInmueblesDAO) FactoryDao.buildDao("scp.DetalleTecnicoInmueblesDAO");
            
            
            
            if (actualizarBien) {
                
                int bienId = requestData.getInt("bien_id");                
                bi.setBie_inm_id(bienId);
                bien_inm_dao.update(bi);
                dti = new DetalleTecnicoInmuebles(bi.getBie_inm_id(), propiedad, tipo_terreno, est_sanea, area, um, part_elec, reg_sinabip, nro_fic_reg);
                det_tec_inm_dao.update(dti);
                msgResponse = "El bien se actualizó correctamente";
                
            } else {               
                
                bien_inm_dao.insert(bi);   
                dti = new DetalleTecnicoInmuebles(bi.getBie_inm_id(), propiedad, tipo_terreno, est_sanea, area, um, part_elec, reg_sinabip, nro_fic_reg);
                det_tec_inm_dao.insert(dti); 
                msgResponse = "El registro del bien inmueble y detalle técnico se realizó correctamente";
            }            

        } catch (Exception e) {
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar el bien inmueble , datos incorrectos", e.getMessage());
        }

        JSONObject oResponse = new JSONObject();
        oResponse.put("cod_bie", bi.getBie_inm_id());
        return WebResponse.crearWebResponseExito(msgResponse, oResponse);

    }

}
