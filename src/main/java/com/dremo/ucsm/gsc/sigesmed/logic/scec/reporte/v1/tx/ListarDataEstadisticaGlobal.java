package com.dremo.ucsm.gsc.sigesmed.logic.scec.reporte.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.ActasReunionComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.ComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.ReunionComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.ActasReunionComision;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.AcuerdosActaComision;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.AsistenciaReunionComision;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.ReunionComision;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by geank on 16/10/16.
 */
public class ListarDataEstadisticaGlobal implements ITransaction{
    private static final Logger logger = Logger.getLogger(ListarDataEstadisticaGlobal.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        return obtenerData(data.getInt("com"),data.getString("tip"));
    }

    private WebResponse obtenerData(int com, String tip) {
        try{
            switch (tip.toUpperCase()){
                case "ACTA":
                    return WebResponse.crearWebResponseExito("Exito al listar los datos",listarActas(com));
                case "ASISTENCIA":
                    return WebResponse.crearWebResponseExito("Exito al listar los datos",listarAsistencia(com));
            }
        }catch (Exception e){
            logger.log(Level.SEVERE,"obtener data",e);
            return WebResponse.crearWebResponseError("No se pudo obtener la data",WebResponse.BAD_RESPONSE);
        }
        return null;
    }
    private JSONObject listarActas(int com) throws Exception{
        ReunionComisionDao reunionDao = (ReunionComisionDao) FactoryDao.buildDao("scec.ReunionComisionDao");
        ActasReunionComisionDao actasDao = (ActasReunionComisionDao) FactoryDao.buildDao("scec.ActasReunionComisionDao");
        List<ReunionComision> reuniones = reunionDao.buscarReunionesPorComision(com);   
        JSONArray labels = new JSONArray();
        JSONArray cumplidos = new JSONArray();
        JSONArray no_cumplidos = new JSONArray();
        for(ReunionComision reunion : reuniones){
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            labels.put(sdf.format(reunion.getFecReu()));
            
            int numCum = 0, numNoCum = 0;
            
            List<ActasReunionComision> actas = actasDao.buscarPorReunionConAcuerdos(reunion.getReuId());
            for(ActasReunionComision acta : actas){
                List<AcuerdosActaComision> acuerdos = acta.getAcuerdos();
                logger.log(Level.SEVERE,"numero de acuerdos: {0}",acuerdos.size());
                for(AcuerdosActaComision acuerdo : acuerdos){
                    
                    if(acuerdo.getEstCumAcu()){
                        numCum++;
                     
                    }else numNoCum++;
                }
             
            }
            cumplidos.put(numCum);
            no_cumplidos.put(numNoCum);
        }
        JSONObject obj = new JSONObject();
        
        JSONArray data = new JSONArray().put(cumplidos).put(no_cumplidos);
        obj.put("labels",labels);
        obj.put("data",data);
        return obj;
    }
    
    private JSONObject listarAsistencia(int com) throws Exception{
        ReunionComisionDao reunionDao = (ReunionComisionDao) FactoryDao.buildDao("scec.ReunionComisionDao");
        List<ReunionComision> reuniones = reunionDao.buscarReunionesConAsistencias(com);
        
        JSONArray labels = new JSONArray();
        JSONArray asistio = new JSONArray();
        JSONArray noAsistio = new JSONArray();
        for(ReunionComision reunion : reuniones){
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            labels.put(sdf.format(reunion.getFecReu()));
            int numAsis = 0, numNoAsi = 0;
            List<AsistenciaReunionComision> asistencias = reunion.getAsistenciasReunion();
            for(AsistenciaReunionComision asistencia: asistencias){
                
                if(asistencia.getEst()){
                    numAsis++;
                }else numNoAsi++;
            }
            asistio.put(numAsis);
            noAsistio.put(numNoAsi);
        }
        
        JSONObject obj = new JSONObject();
        JSONArray data = new JSONArray().put(asistio).put(noAsistio);
        obj.put("labels",labels);
        obj.put("data",data);
        return obj;
    }
}
