/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.horario_escolar.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.HorarioEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

/**
 *
 * @author abel
 */
public class EliminarDetalleHorarioTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int detalleHorarioID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            detalleHorarioID = requestData.getInt("detalleHorarioID");
        
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo eliminar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        try{
            HorarioEscolarDao horarioDao = (HorarioEscolarDao)FactoryDao.buildDao("mech.HorarioEscolarDao");
            horarioDao.eliminarHorarioDetalle(detalleHorarioID);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo eliminar el detalle de horario", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("Se elimino la asignacion");
        //Fin
    }
}
