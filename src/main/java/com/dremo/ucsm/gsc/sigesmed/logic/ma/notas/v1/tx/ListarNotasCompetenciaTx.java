package com.dremo.ucsm.gsc.sigesmed.logic.ma.notas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.ConfiguracionNotaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.NotaEvaluacionIndicadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.RegistroAuxiliarDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.HistoricoNotasEstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.DocenteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.PlanEstudiosDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ConfiguracionNota;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.HistoricoNotasEstudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.RegistroAuxiliarCompetencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Estudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.GradoIEEstudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CompetenciaAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 27/01/2017.
 */
public class ListarNotasCompetenciaTx implements ITransaction {
    private Logger logger = Logger.getLogger(ListarNotasCompetenciaTx.class.getName());
    private List<ConfiguracionNota> config_docente = null;
    
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject)wr.getData();
        int idPeriodo = data.getInt("per");
        int idArea = data.getInt("are");
        int idOrg = data.getInt("org");
        int idUser = data.getInt("usr");
        int idGrado = data.getInt("gra");
        String secc = data.getString("secc");
        int idPlan = data.optInt("pla", -1);
        listarConfiguracionDocente(idOrg, idUser);
        return listarNotasCompetencia(idPeriodo, idArea, idOrg, idUser, idGrado, secc, idPlan);
    }

    private WebResponse listarNotasCompetencia(int idPeriodo, int idArea, int idOrg, int idUser, int idGrado, String secc, int idPlan) {
        try{
            PlanEstudiosDao planDao = (PlanEstudiosDao) FactoryDao.buildDao("mech.PlanEstudiosDao");
            RegistroAuxiliarDao regisDao = (RegistroAuxiliarDao) FactoryDao.buildDao("ma.RegistroAuxiliarDao");
            NotaEvaluacionIndicadorDao notaDao = (NotaEvaluacionIndicadorDao) FactoryDao.buildDao("ma.NotaEvaluacionIndicadorDao");
            DocenteDao docDao =(DocenteDao) FactoryDao.buildDao("maestro.DocenteDao");
            List<CompetenciaAprendizaje> competencias = regisDao.listarCompetenciasPeriodo(idPeriodo,idArea,idOrg,
                    docDao.buscarDocentePorUsuario(idUser).getDoc_id(),idGrado,
                    idPlan == -1 ? planDao.buscarVigentePorOrganizacion(idOrg).getPlaEstId() : idPlan);
            JSONArray competenciasJSON = new JSONArray(EntityUtil.listToJSONString(
                    new String[]{"comId", "nom"},
                    new String[]{"id", "nom"},
                    competencias
            ));
            List<GradoIEEstudiante> estudiantesGrado = notaDao.listarEstudiantesGradoActual(idOrg, idGrado, secc.charAt(0));
            JSONArray estudiantesJSON = new JSONArray();
            for(GradoIEEstudiante gramat : estudiantesGrado){
                Estudiante estudiante= gramat.getMatriculaEstudiante().getEstudiante();
                JSONObject jsonEstudiante = new JSONObject(EntityUtil.objectToJSONString(
                        new String[]{"perId", "dni", "nom", "apePat", "apeMat", "fecNac"},
                        new String[]{"id", "dni", "nom", "pat", "mat", "nac"},
                        estudiante.getPersona()
                ));
                jsonEstudiante.put("idgra",gramat.getGraOrgEstId());
                jsonEstudiante.put("notas",new JSONArray());
                for(CompetenciaAprendizaje competencia : competencias){
                    RegistroAuxiliarCompetencia notComp = regisDao.buscarNotaCompetenciaEsp(competencia.getComId(),idArea,idPeriodo,gramat.getGraOrgEstId());
                    if(notComp == null){
                        jsonEstudiante.getJSONArray("notas").put(new JSONObject().put("comp",competencia.getComId()));
                    }else {
                        jsonEstudiante.getJSONArray("notas").put(new JSONObject()
                                .put("comp", competencia.getComId())
                                .put("id", notComp.getRegAuxComId())
                                .put("not", notComp.getNota())
                                .put("not_lit",notComp.getNot_com_lit()));
                        
                    }
                }
                
                
                //Actualizamos los Promedios Finales
                int not_final = actualizar_historico_nota(gramat.getGraOrgEstId(),idArea,idPeriodo);
                String not_lit_final = obtenerNotaLiteral(not_final);
                
                
                // Aqui la nota final
                HistoricoNotasEstudiante historico = regisDao.buscaHistoricoNotasEstudiante(idArea,idPeriodo,gramat.getGraOrgEstId());
                historico.setNota(Integer.toString(not_final));
                historico.setNot_are_lit(not_lit_final);
                
                //Actualizamos la Tabla Historico Notas
                HistoricoNotasEstudianteDao hist_dao = (HistoricoNotasEstudianteDao) FactoryDao.buildDao("ma.HistoricoNotasEstudianteDao");
                hist_dao.update(historico);
                
                
                if(historico != null){
                    jsonEstudiante.put("notAre",historico.getNota());
                    jsonEstudiante.put("notArelit",historico.getNot_are_lit());
                }else{
                     jsonEstudiante.put("notAre","");
                     jsonEstudiante.put("notArelit","");
                } 
                estudiantesJSON.put(jsonEstudiante);    
            }
            JSONObject results = new JSONObject().put("indicadores",competenciasJSON).put("estudiantes",estudiantesJSON);
            return WebResponse.crearWebResponseExito("Se listo las notas correctamente",results);
        }catch (Exception e){
            logger.log(Level.SEVERE,"listarNotas",e);
            return WebResponse.crearWebResponseError("No se puede listar los datos");
        }
    }
    public int actualizar_historico_nota(int grad_ie_estudiante,int id_area,int id_period){
        
        RegistroAuxiliarDao regisDao = (RegistroAuxiliarDao) FactoryDao.buildDao("ma.RegistroAuxiliarDao");
        List<RegistroAuxiliarCompetencia> notas_comp = regisDao.buscarNotaCompetenciasEstudiante(grad_ie_estudiante, id_area, id_period);
        float suma_comp = 0;
        int num_comp = notas_comp.size();
        //Promediamos las notas por Competencia para finalmente actualizar el Historico
        for(RegistroAuxiliarCompetencia reg: notas_comp){
            suma_comp += Float.parseFloat(reg.getNota());
        }
          return (int)(suma_comp/num_comp);
    }
    
    public String obtenerNotaLiteral(int not){
        
        for(ConfiguracionNota conf : config_docente){
            if(not >= conf.getConf_not_min() && not <= conf.getConf_not_max()){
                return conf.getConf_cod();
            }
        }
        return "";

    }
    
    public void listarConfiguracionDocente(int org_id , int idUser){
        ConfiguracionNotaDao configNot = (ConfiguracionNotaDao) FactoryDao.buildDao("ma.ConfiguracionNotaDao");
        config_docente = configNot.listarConfiguracionDocente(idUser, org_id);

    }
    
    

}
