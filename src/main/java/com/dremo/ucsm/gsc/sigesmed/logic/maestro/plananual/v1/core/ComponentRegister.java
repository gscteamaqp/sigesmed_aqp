package com.dremo.ucsm.gsc.sigesmed.logic.maestro.plananual.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.maestro.plananual.v1.tx.*;

/**
 * Created by Administrador on 10/10/2016.
 */
public class ComponentRegister implements IComponentRegister {
    @Override
    public WebComponent createComponent() {
        WebComponent component = new WebComponent(Sigesmed.SUBMODULO_MAESTRO);
        component.setName("plan_anual");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        //Competencias
        component.addTransactionGET("listarPlanEstudiosActual",ListarPlanEstudiosActualTx.class);
        component.addTransactionGET("reporteProgramacionAnual",ReporteProgramacionAnualTx.class);
        component.addTransactionGET("listarPlanesAnuales", ListarPLanesAnualesTx.class);
        component.addTransactionGET("listarComponentesPlan", ListarComponentesPlanTx.class);
        component.addTransactionPOST("registrarPlanAnual", RegistrarPlanAnualTx.class);
        component.addTransactionPOST("detallePlanAnual", DetalleProgramacionAnualTx.class);
        component.addTransactionPUT("editarPlanAnual", EditarProgramacionAnualTx.class);
        component.addTransactionDELETE("eliminarPlanAnual", EliminarProgramacionAnualTx.class);

        return component;
    }
}
