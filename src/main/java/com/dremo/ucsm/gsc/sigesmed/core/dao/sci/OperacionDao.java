/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.sci;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.Operacion;
import java.util.List;


/**
 *
 * @author RE
 */
public interface OperacionDao extends GenericDao<Operacion>{
    public List<Operacion> buscarConCuentas();
    public void eliminarCuentas(int operacionID);
}
