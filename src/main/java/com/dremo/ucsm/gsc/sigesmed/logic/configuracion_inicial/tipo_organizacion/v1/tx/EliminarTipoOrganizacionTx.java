/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.tipo_organizacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.TipoOrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TipoOrganizacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

/**
 *
 * @author Administrador
 */
public class EliminarTipoOrganizacionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int tipoOrganizacionID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            tipoOrganizacionID = requestData.getInt("tipoOrganizacionID");
        
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo eliminar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        TipoOrganizacionDao tipoOrgDao = (TipoOrganizacionDao)FactoryDao.buildDao("TipoOrganizacionDao");
        try{
            tipoOrgDao.delete(new TipoOrganizacion(tipoOrganizacionID));
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar el Tipo de Organizacion\n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el Tipo de Organizacion ", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Modulo del Sistema se elimino correctamente");
        //Fin
    }
}
