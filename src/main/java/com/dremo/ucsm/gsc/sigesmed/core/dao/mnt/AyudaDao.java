/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.mnt;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mnt.Ayuda;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mnt.PasosAyuda;
import java.util.List;

/**
 *
 * @author Administrador
 */
public interface AyudaDao  extends GenericDao<Ayuda>{
    public int buscarIdAyudaPorFuncionalidad(int funcionID,char tipo);
    public List<PasosAyuda> buscarPasosPorAyuda(int ayudaID);
    public Ayuda buscarAyuda(int ayudaID);

}
