package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.maestro.plan;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.EvaluacionesUnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.EvaluacionesUnidadDidactica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.IndicadorAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.IndicadoresEvaluacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.IndicadoresSesionAprendizaje;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import java.util.List;
import org.hibernate.criterion.Projections;

/**
 * Created by Administrador on 03/01/2017.
 */
public class EvaluacionesUnidadDidacticaDaoHibernate extends GenericDaoHibernate<EvaluacionesUnidadDidactica> implements EvaluacionesUnidadDidacticaDao{
    @Override
    public EvaluacionesUnidadDidactica buscarEvaluacionId(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(EvaluacionesUnidadDidactica.class)
                    .add(Restrictions.eq("evaUniDidId",id))
                    .add(Restrictions.eq("estReg",'A'));
            return (EvaluacionesUnidadDidactica) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<EvaluacionesUnidadDidactica> listarEvaluacionesPorUnidad(int idUnidad) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(EvaluacionesUnidadDidactica.class)
                    .add(Restrictions.eq("estReg",'A'))
                    .createCriteria("unidad", JoinType.INNER_JOIN).add(Restrictions.eq("uniDidId",idUnidad));
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

   
}
