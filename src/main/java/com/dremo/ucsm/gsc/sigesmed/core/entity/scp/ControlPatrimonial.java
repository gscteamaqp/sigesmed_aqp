/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.scp;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;

/**
 *
 * @author Jeferson
 */
@Entity
@Table(name="control_patrimonial", schema="administrativo")
public class ControlPatrimonial implements java.io.Serializable {
    
    @Id
    @Column(name="con_pat_id", unique= true , nullable=false)
    @SequenceGenerator(name="secuencia_det_inv_tra",sequenceName="administrativo.control_patrimonial_con_pat_id_seq")
    @GeneratedValue(generator="secuencia_det_inv_tra")
    private int con_pat_id;
    
    
    @Id
    @Column(name="org_id")
    private int org_id;
    
    
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="org_id" , insertable=false , updatable=false)
    private Organizacion org;
   
    
    @Column(name="per_id")
    private int per_id;
 
    
    @Column(name="per_res")
    private String per_res;
    
    @Column(name="fec_ini")
    private Date fec_ini;
    
    @Column(name="fec_cie")
    private Date fec_cie;
    
    @Column(name="obs")
    private String obs;
    
    @Column(name="fec_mod")
    private Date fec_mod;
    
    @Column(name="usu_mod")
    private int usu_mod;
    
    @Column(name="est_reg")
    private char est_reg;
        
    @Column(name="file_name")
    private String file_name;
    
    public void setOrg(Organizacion org) {
        this.org = org;
    }

    public void setOrg_id(int org_id) {
        this.org_id = org_id;
    }

    public Organizacion getOrg() {
        return org;
    }

    public int getPer_id() {
        return per_id;
    }
    
    
    

    public void setCar_tec_id(int con_pat_id) {
        this.con_pat_id = con_pat_id;
    }
    
    

    public void setPer_id(String per_res) {
        this.per_res = per_res;
    }

    public void setFec_ini(Date fec_ini) {
        this.fec_ini = fec_ini;
    }

    public void setFec_cie(Date fec_cie) {
        this.fec_cie = fec_cie;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public void setFec_mod(Date fec_mod) {
        this.fec_mod = fec_mod;
    }

    public void setUsu_mod(int usu_mod) {
        this.usu_mod = usu_mod;
    }

    public void setEst_reg(char est_reg) {
        this.est_reg = est_reg;
    }

    public int getCon_pat_id() {
        return con_pat_id;
    }

   
   

    public Date getFec_ini() {
        return fec_ini;
    }

    public Date getFec_cie() {
        return fec_cie;
    }

    public String getObs() {
        return obs;
    }

    public Date getFec_mod() {
        return fec_mod;
    }

    public int getUsu_mod() {
        return usu_mod;
    }

    public char getEst_reg() {
        return est_reg;
    }

    public void setCon_pat_id(int con_pat_id) {
        this.con_pat_id = con_pat_id;
    }

    public void setPer_res(String per_res) {
        this.per_res = per_res;
    }

    public String getPer_res() {
        return per_res;
    }

    public int getOrg_id() {
        return org_id;
    }

    public void setPer_id(int per_id) {
        this.per_id = per_id;
    }
    
    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public ControlPatrimonial() {
    }

    public ControlPatrimonial(int con_pat_id, int org_id, String per_res, Date fec_ini, Date fec_cie, String obs, Date fec_mod, int usu_mod, char est_reg) {
        this.con_pat_id = con_pat_id;
        this.org_id = org_id;
        this.per_res = per_res;
        this.fec_ini = fec_ini;
        this.fec_cie = fec_cie;
        this.obs = obs;
        this.fec_mod = fec_mod;
        this.usu_mod = usu_mod;
        this.est_reg = est_reg;
    }

    public ControlPatrimonial(int con_pat_id, int org_id, int per_id, String per_res, Date fec_ini, Date fec_cie, String obs, Date fec_mod, int usu_mod, char est_reg) {
        this.con_pat_id = con_pat_id;
        this.org_id = org_id;
        this.per_id = per_id;
        this.per_res = per_res;
        this.fec_ini = fec_ini;
        this.fec_cie = fec_cie;
        this.obs = obs;
        this.fec_mod = fec_mod;
        this.usu_mod = usu_mod;
        this.est_reg = est_reg;
    }
    
    
    
    
}
