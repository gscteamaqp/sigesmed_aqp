/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.scp;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import java.util.List;
/**
 *
 * @author Jeferson
 */
@Entity
@Table(name="altas", schema="administrativo")
public class Altas {
    
    @Id
    @Column(name="altas_id" , unique=true, nullable=false)
    @SequenceGenerator(name="secuencia_inv_tra",sequenceName="administrativo.altas_altas_id_seq")
    @GeneratedValue(generator="secuencia_inv_tra")
    private int altas_id;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="mov_ing_id" , insertable=false , updatable=false)
    private MovimientoIngresos movimiento_ingresos;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cau_alt_id" , insertable=false , updatable=false)
    private CausalAlta causal_alta;
    
    
    @Column(name="cau_alt_id")
    private int cau_alt_id;
    
    @OneToMany(fetch=FetchType.LAZY)
    @JoinColumn(name="altas_id" , insertable=false , updatable=false)
    private List<AltasDetalle> aldet ;
    
    @Column(name="org_id")
    private int org_id;
    
    @Column(name="usu_mod")
    private int usu_mod;
    
    
    @Column(name="mov_ing_id")
    private int mov_ing_id;
    
    @Column(name="est_reg")
    private char est_reg;

    public void setOrg_id(int org_id) {
        this.org_id = org_id;
    }

    public void setUsu_mod(int usu_mod) {
        this.usu_mod = usu_mod;
    }

    public int getOrg_id() {
        return org_id;
    }

    public int getUsu_mod() {
        return usu_mod;
    }

    public void setMov_ing_id(int mov_ing_id) {
        this.mov_ing_id = mov_ing_id;
    }

    public int getMov_ing_id() {
        return mov_ing_id;
    }

    public void setAldet(List<AltasDetalle> aldet) {
        this.aldet = aldet;
    }

    public List<AltasDetalle> getAldet() {
        return aldet;
    }

    public void setCau_alt_id(int cau_alt_id) {
        this.cau_alt_id = cau_alt_id;
    }

    public int getCau_alt_id() {
        return cau_alt_id;
    }
    

    public void setAltas_id(int altas_id) {
        this.altas_id = altas_id;
    }

    public void setMovimiento_ingresos(MovimientoIngresos movimiento_ingresos) {
        this.movimiento_ingresos = movimiento_ingresos;
    }

    public void setCausal_alta(CausalAlta causal_alta) {
        this.causal_alta = causal_alta;
    }

    public void setEst_reg(char est_reg) {
        this.est_reg = est_reg;
    }

    public int getAltas_id() {
        return altas_id;
    }

    public MovimientoIngresos getMovimiento_ingresos() {
        return movimiento_ingresos;
    }

    public CausalAlta getCausal_alta() {
        return causal_alta;
    }

    public char getEst_reg() {
        return est_reg;
    }
    /*
    public void setDetalle_altas(List<AltasDetalle> detalle_altas) {
        this.detalle_altas = detalle_altas;
    }

    public List<AltasDetalle> getDetalle_altas() {
        return detalle_altas;
    }
    */

    public Altas() {
    }

    public Altas(int altas_id, MovimientoIngresos movimiento_ingresos, int cau_alt_id, char est_reg) {
        this.altas_id = altas_id;
        this.movimiento_ingresos = movimiento_ingresos;
        this.cau_alt_id = cau_alt_id;        
        this.est_reg = est_reg;
    }
    
//    public Altas(int altas_id, MovimientoIngresos movimiento_ingresos, int cau_alt_id, AltasDetalle aldet, char est_reg) {
//        this.altas_id = altas_id;
//        this.movimiento_ingresos = movimiento_ingresos;
//        this.cau_alt_id = cau_alt_id;
//        this.aldet = aldet;
//        this.est_reg = est_reg;
//    }

    public Altas(int altas_id, int cau_alt_id, int org_id, int usu_mod, int mov_ing_id, char est_reg) {
        this.altas_id = altas_id;
        this.cau_alt_id = cau_alt_id;
        this.org_id = org_id;
        this.usu_mod = usu_mod;
        this.mov_ing_id = mov_ing_id;
        this.est_reg = est_reg;
    }

    
    
    
}
