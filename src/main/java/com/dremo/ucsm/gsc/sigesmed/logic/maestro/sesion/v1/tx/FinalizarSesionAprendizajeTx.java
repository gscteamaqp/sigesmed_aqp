package com.dremo.ucsm.gsc.sigesmed.logic.maestro.sesion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.SesionAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.SesionAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 19/01/2017.
 */
public class FinalizarSesionAprendizajeTx implements ITransaction{
    private static Logger logger = Logger.getLogger(FinalizarSesionAprendizajeTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject)wr.getData();
        int idSesion = data.getInt("id");
        return finalizarSesion(idSesion);
    }

    private WebResponse finalizarSesion(int idSesion) {
        try{
            SesionAprendizajeDao sesDao = (SesionAprendizajeDao) FactoryDao.buildDao("maestro.plan.SesionAprendizajeDao");
            SesionAprendizaje sesion = sesDao.buscarSesionById(idSesion);
            sesion.setNivAva("F");
            sesDao.update(sesion);
            SesionAprendizaje sig = sesDao.buscarSesionSiguiente(sesion.getSesAprId(),sesion.getUnidadDidactica().getUniDidId());
            if(sig != null){
                sig.setNivAva("A");
                sesDao.update(sig);
                return WebResponse.crearWebResponseExito("Se registro exitosamente",new JSONObject().put("sig",sig.getSesAprId()));
            }

            return WebResponse.crearWebResponseExito("Se registro exitosamente");
        }catch (Exception e){
            logger.log(Level.SEVERE,"finalizarSesion",e);
            return WebResponse.crearWebResponseError("No se puede listar los datos");
        }
    }
}
