package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.ComentarioTemaCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.ComentarioTemaCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class EliminarComentarioTemaTx implements ITransaction {
    
    private static final Logger logger = Logger.getLogger(EliminarComentarioTemaTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();
            ComentarioTemaCapacitacionDao comentarioTemaCapacitacionDao = (ComentarioTemaCapacitacionDao) FactoryDao.buildDao("capacitacion.ComentarioTemaCapacitacionDao");
            ComentarioTemaCapacitacion comentario = comentarioTemaCapacitacionDao.buscarPorId(data.getInt("cod"));
            
            comentario.setEstReg('E');
            comentario.setUsuMod(data.getInt("usuMod"));
            comentario.setFecMod(new Date());
            
            comentarioTemaCapacitacionDao.update(comentario);

            return WebResponse.crearWebResponseExito("Exito al eliminar el comentario", WebResponse.OK_RESPONSE);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "eliminarComentario", e);
            return WebResponse.crearWebResponseError("Error al eliminar el comentario", WebResponse.BAD_RESPONSE);
        }
    }
}
