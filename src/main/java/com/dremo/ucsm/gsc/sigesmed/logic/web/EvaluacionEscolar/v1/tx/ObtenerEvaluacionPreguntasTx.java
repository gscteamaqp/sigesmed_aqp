/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx.*;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.BandejaEvaluacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.EvaluacionEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.EvaluacionEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.PreguntaEvaluacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaEvaluacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.RespuestaEvaluacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.TareaDocumento;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
/**
 *
 * @author abel
 */
public class ObtenerEvaluacionPreguntasTx implements ITransaction {
    
    List<RespuestaEvaluacion> respuestas;

    @Override
    public WebResponse execute(WebRequest wr) {
        
        
        int  ban_eva_id = 0;
        BandejaEvaluacion evaluacion = null;
        
        try{
             JSONObject requestData = (JSONObject)wr.getData();
             
             ban_eva_id = requestData.getInt("ban_eva_id");
             
             EvaluacionEscolarDao eva_esc_dao = (EvaluacionEscolarDao) FactoryDao.buildDao("web.EvaluacionEscolarDao");
             BandejaEvaluacionDao res_eva_dao = (BandejaEvaluacionDao) FactoryDao.buildDao("web.BandejaEvaluacionDao");
             evaluacion = eva_esc_dao.obtenerEvaluacionPreguntas(ban_eva_id);
             
             
             if(evaluacion == null){
                 return WebResponse.crearWebResponseError("No se Encontro la Evaluacion del Alumno Solicitada");
             }
             
            // Construimos la evaluacion
             //Cabecera del Examen
            JSONObject oExamen = new JSONObject();
            oExamen.put("evaluacionID",evaluacion.getEvaEscId());
            oExamen.put("nom",evaluacion.getEvaluacionEscolar().getNom());
            oExamen.put("des",evaluacion.getEvaluacionEscolar().getDes());
            oExamen.put("fecEnvio",evaluacion.getEvaluacionEscolar().getFecEnv());
            
            // Preguntas del Examen
            JSONArray JSONRespuestas = new JSONArray();
            List<PreguntaEvaluacion> preguntas = evaluacion.getEvaluacionEscolar().getPreguntas();
            respuestas = res_eva_dao.buscar_respuestas_alumno(ban_eva_id);
            
            
            for(PreguntaEvaluacion pregunta : preguntas){
                
                JSONObject oPregunta = new JSONObject();
                oPregunta.put("pregId",pregunta.getPreEvaId());
                oPregunta.put("pregTit",pregunta.getTitulo());
                oPregunta.put("pregDes",pregunta.getPregunta());
                oPregunta.put("pregAlt",pregunta.getAlternativa());
                oPregunta.put("pregRes",pregunta.getRespuesta());
                oPregunta.put("pregTie",pregunta.getTiempo());
                for(RespuestaEvaluacion respuesta : respuestas){ /*Buscamos las Respuestas del Alumno*/
                    if(respuesta.getResEvaId()== pregunta.getPreEvaId()){ 
                        oPregunta.put("resAlu",respuesta.getRespuesta());
                        oPregunta.put("puntAlu",respuesta.getPuntos());
                        break;
                    }
                 }
               
                JSONRespuestas.put(oPregunta);
            }
            oExamen.put("preguntas",JSONRespuestas);
            
            
            
         
        
        return WebResponse.crearWebResponseExito("Se listo la Evaluacion del Alumno Correctamente",oExamen);        
        //Fin
             
            
        }catch(Exception e){
             System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Obtener la Evaluacion del Alumno Solicitada", e.getMessage() );
        }
        
        
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
   
    
    
    
    
}
