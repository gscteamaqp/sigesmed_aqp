/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.InventarioFisicoDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.InventarioInicialDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.InventarioFisico;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.InventarioInicial;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.GCell;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author harold
 */
public class ReporteInventariosTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        
        List<InventarioInicial> invIni = null;
        List<InventarioFisico> invFis = null;
        
        try {
            
            JSONObject requestData = (JSONObject) wr.getData();
            int org_id = requestData.getInt("org_id");
            
            /*Listamos los Inventarios Iniciales*/
            InventarioInicialDAO inv_ini_dao = (InventarioInicialDAO)FactoryDao.buildDao("scp.InventarioInicialDAO");
            invIni = inv_ini_dao.reporteInventario(org_id);                                  
            
            /*Listamos los Inventarios Fisicos*/
            InventarioFisicoDAO inv_fis_dao = (InventarioFisicoDAO)FactoryDao.buildDao("scp.InventarioFisicoDAO");
            invFis = inv_fis_dao.reporteInventario(org_id);
            
            Organizacion org = null;
            OrganizacionDao org_dao = (OrganizacionDao) FactoryDao.buildDao("OrganizacionDao");
            org = org_dao.buscarConTipoOrganizacionYPadre(org_id);
            String nombre_ie = org.getNom();
            String ubic_ie = org.getDir();
            String org_pad = org.getOrganizacionPadre().getDes();
                        
            /*Creacion de las Cabeceras (Informacion de la Institucion , Cabeceras de Tablas )*/
            
            String cab1[] = {
                "N°", "Cod. Patrimonial", "Tip. Movimiento",
                "N° Resolución", "Fec. Resol", "Tip. Inventario"                
            };
            
            Mitext m = null;
            m = new Mitext(true, "REPORTE DE BIENES INMUEBLES DE LA INSTITUCION");
            m.newLine(2);       
            
            m.agregarParrafo("Ubicacion : " +ubic_ie);
            m.newLine(1);
            m.agregarParrafo("UGEL :"+ org_pad);

            // Creamos el Objeto Reporte
                        
            float columnWidths_1[] = {1, 3, 3, 3, 2, 2};
            GTabla tabla_1 = new GTabla(columnWidths_1);
            tabla_1.setWidthPercent(100);
            tabla_1.build(cab1);
                        
            int data_length = cab1.length;
            String[] archivos_data = new String[data_length];
            for (int i = 0; i < data_length; i++) {
                archivos_data[i] = " ";
            }
            
            GCell[] cell = {tabla_1.createCellCenter(1, 1), tabla_1.createCellCenter(1, 1), tabla_1.createCellCenter(1, 1), tabla_1.createCellCenter(1, 1), tabla_1.createCellCenter(1, 1), tabla_1.createCellCenter(1, 1), tabla_1.createCellCenter(1, 1)};
            
            int size = invIni.size();
            int j = 0;  // Para continuar la numeracion
            
            for (int i = 0; i < size; i++) {
                InventarioInicial inventario = invIni.get(i);
                
                String index = Integer.toString(i); 
                
                archivos_data[0] = index;
                archivos_data[2] = inventario.getMov_ingresos().getTipo_movimiento().getDes();
                archivos_data[3] = inventario.getMov_ingresos().getNum_res();
                archivos_data[4] = inventario.getMov_ingresos().getFec_res().toString();
                archivos_data[5] = "Inv Inicial";
                
                tabla_1.processLineCell(archivos_data, cell);
                
                j = i;
            }
            
            size = invFis.size();
            
            for (int i = 0; i < size; i++) {
                InventarioFisico inventarioF = invFis.get(i);
                
                String index = Integer.toString(j++); 
                
                archivos_data[0] = index;
                archivos_data[2] = inventarioF.getMov_ingresos().getTipo_movimiento().getDes();
                archivos_data[3] = inventarioF.getMov_ingresos().getNum_res();
                archivos_data[4] = inventarioF.getMov_ingresos().getFec_res().toString();
                archivos_data[5] = "Inv Fisico";
                
                tabla_1.processLineCell(archivos_data, cell);
            }
            
            m.agregarTabla(tabla_1);
            
            JSONArray responseData = new JSONArray();

            m.cerrarDocumento();
            JSONObject oResponse = new JSONObject();
            oResponse.put("datareporte", m.encodeToBase64());
            responseData.put(oResponse);            
            
            return WebResponse.crearWebResponseExito("Se genero el Reporte con exito ...", responseData);
            
        } catch (Exception e) {
            
            System.out.println(e);
            return WebResponse.crearWebResponseError("ERROR AL GENERAR REPORTE ", e.getMessage());
            
        }
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
