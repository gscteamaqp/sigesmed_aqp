/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.plan_estudios.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.PlanEstudiosDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanHoraArea;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
/**
 *
 * @author abel
 */
public class PersistenciaHoraDisponibilidadTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        PlanHoraArea nuevo = null;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            int horaDisponibleID = requestData.optInt("horaDisponibleID");
            int planNivelID = requestData.getInt("planNivelID");
            int areaID = requestData.getInt("areaID");
            int gradoID = requestData.getInt("gradoID");
            int horaL = requestData.getInt("horaL");
            
            nuevo = new PlanHoraArea(horaDisponibleID,horaL,planNivelID,areaID,gradoID);
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo asignar la hora de libre disponibilidad, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        PlanEstudiosDao planDao = (PlanEstudiosDao)FactoryDao.buildDao("mech.PlanEstudiosDao");
        try{
            if(nuevo.getHorDis() == 0){
                planDao.eliminarHoraDisponible(nuevo);
            }else{
                planDao.mergeHoraDisponible(nuevo);
            }
        }catch(Exception e){
            System.out.println("No se pudo asignar la hora de libre disponibilidad\n"+e);
            return WebResponse.crearWebResponseError("No se pudo asignar la hora de libre disponibilidad", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("horaDisponibleID",nuevo.getPlaHorAreId());
        return WebResponse.crearWebResponseExito("Se asigno la hora de libre disponibilidad correctamente", oResponse);
        //Fin
    }    
    
    
}

