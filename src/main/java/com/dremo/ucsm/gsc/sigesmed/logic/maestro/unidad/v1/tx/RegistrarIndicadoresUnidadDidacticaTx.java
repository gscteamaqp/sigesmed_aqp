package com.dremo.ucsm.gsc.sigesmed.logic.maestro.unidad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.IndicadorAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.SesionAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.UnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CompetenciasUnidadDidactica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.IndicadorAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 23/11/2016.
 */
public class RegistrarIndicadoresUnidadDidacticaTx implements ITransaction {
    private static Logger logger = Logger.getLogger(RegistrarIndicadoresUnidadDidacticaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idUnidad = data.getInt("idUnidad");
        JSONArray indNuevos = data.getJSONArray("nuevos");
        JSONArray indEliminados = data.getJSONArray("eliminados");
        return registrarIndicadoresUnidad(idUnidad,indNuevos,indEliminados);
    }

    private WebResponse registrarIndicadoresUnidad(int idUnidad, JSONArray indNuevos, JSONArray indEliminados) {
        try{
            boolean indicadorEnSesion = false;
            UnidadDidacticaDao uniDao = (UnidadDidacticaDao) FactoryDao.buildDao("maestro.plan.UnidadDidacticaDao");
            SesionAprendizajeDao sesionDao = (SesionAprendizajeDao) FactoryDao.buildDao("maestro.plan.SesionAprendizajeDao");
            IndicadorAprendizajeDao indicadorDao = (IndicadorAprendizajeDao) FactoryDao.buildDao("maestro.plan.IndicadorAprendizajeDao");
            for(int i = 0; i < indEliminados.length(); i++){
                JSONObject objData = indEliminados.getJSONObject(i);
                int idComp = objData.getInt("com");
                int idCap = objData.getInt("cap");
                int idInd = objData.getInt("ind");
                CompetenciasUnidadDidactica compUni = uniDao.buscarCompetenciaUnidad(idUnidad, idComp, idCap);
                //List<IndicadorAprendizaje> indicadores = compUni.getIndicadores();
                Set<IndicadorAprendizaje> indicadores = compUni.getIndicadores();
                //Set<IndicadorAprendizaje> indicador = (Set<IndicadorAprendizaje>)compUni.getIndicadores();
                indicadores.remove(indicadorDao.buscarPorId(idInd));
                uniDao.updateCompetenciaUnidadDidactica(compUni);
                /*if(sesionDao.buscarIndicadoresUnidadAprendizaje(idUnidad,idComp,idCap,idInd) == null){
                    indicadores.remove(indicadorDao.buscarPorId(idInd));
                    uniDao.updateCompetenciaUnidadDidactica(compUni);
                }else{
                    indicadorEnSesion = true;
                }*/
                /*indicadores.remove(indicadorDao.buscarPorId(idInd));
                uniDao.updateCompetenciaUnidadDidactica(compUni);*/
            }
            for(int i = 0; i < indNuevos.length(); i++){
                JSONObject objData = indNuevos.getJSONObject(i);
                int idComp = objData.getInt("com");
                int idCap = objData.getInt("cap");
                int idInd = objData.getInt("ind");
                CompetenciasUnidadDidactica compUni = uniDao.buscarCompetenciaUnidad(idUnidad, idComp, idCap);
                compUni.getIndicadores().add(indicadorDao.buscarPorId(idInd));
                uniDao.updateCompetenciaUnidadDidactica(compUni);
            }
            //eliminamos los indicadores nuevos

            String mensaje = "Se guardaron los indicadores correctamente";
            //if(indicadorEnSesion) mensaje = "Existen elementos que esta asociados a en las sesiones, quitelos de las sesiones y vuelva a intentarlo";
            return WebResponse.crearWebResponseExito(mensaje);
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarIndicadorUnidad",e);
            return WebResponse.crearWebResponseError("Existen elementos que esta asociados a en las sesiones, quitelos de las sesiones y vuelva a intentarlo");
        }
    }
}
