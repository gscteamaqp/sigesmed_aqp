/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.scp;

/**
 *
 * @author Jeferson
 */

import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentaContable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;

@Entity
@Table(name="valor_contable", schema="administrativo")

public class ValorContable {
    
    @Id
    @Column(name="val_cont_id", unique= true , nullable=false)
    @SequenceGenerator(name="secuencia_det_inv_tra",sequenceName="administrativo.valor_contable_val_cont_id_seq")
    @GeneratedValue(generator="secuencia_det_inv_tra")
    private int val_cont_id;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cod_bie" , insertable=false , updatable=false)
    private BienesMuebles bien_mueble; 
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cod_cue_id" , insertable=false , updatable=false)
    private CuentaContable cue_con;

    public CuentaContable getCue_con() {
        return cue_con;
    }

    public void setCue_con(CuentaContable cue_con) {
        this.cue_con = cue_con;
    }

    public int getCod_cue_id() {
        return cod_cue_id;
    }

    public void setCod_cue_id(int cod_cue_id) {
        this.cod_cue_id = cod_cue_id;
    }
    
    @Column(name="est_reg")
    private char est_reg;
    
    @Column(name="cod_bie")
    private int cod_bie;
    
    @Column(name="act_dep")
    private char act_dep;
    
    @Column(name="cod_cue_id")
    private int cod_cue_id;   
        
    @Column(name="val_cont")
    private int val_cont;
    
    @Column(name="org_id")
    private int org_id;

    public int getOrg_id() {
        return org_id;
    }

    public void setOrg_id(int org_id) {
        this.org_id = org_id;
    }

    public void setVal_cont_id(int val_cont_id) {
        this.val_cont_id = val_cont_id;
    }

    public void setBien_mueble(BienesMuebles bien_mueble) {
        this.bien_mueble = bien_mueble;
    }

    public void setEst_reg(char est_reg) {
        this.est_reg = est_reg;
    }

    public void setCod_bie(int cod_bie) {
        this.cod_bie = cod_bie;
    }

    public int getVal_cont_id() {
        return val_cont_id;
    }

    public BienesMuebles getBien_mueble() {
        return bien_mueble;
    }

    public char getEst_reg() {
        return est_reg;
    }

    public int getCod_bie() {
        return cod_bie;
    }

    public void setAct_dep(char act_dep) {
        this.act_dep = act_dep;
    }

    public char getAct_dep() {
        return act_dep;
    }    

    public void setVal_cont(int val_cont) {
        this.val_cont = val_cont;
    }

    public int getVal_cont() {
        return val_cont;
    }
    

    public ValorContable() {
    }

    public ValorContable(int val_cont_id, char est_reg, int cod_bie, char act_dep, int cod_cue_id,int val_cont, int org_id) {
        this.val_cont_id = val_cont_id;
        this.est_reg = est_reg;
        this.cod_bie = cod_bie;
        this.act_dep = act_dep;   
        this.cod_cue_id = cod_cue_id;
        this.val_cont = val_cont;
        this.org_id = org_id;
    }
    
    
}
