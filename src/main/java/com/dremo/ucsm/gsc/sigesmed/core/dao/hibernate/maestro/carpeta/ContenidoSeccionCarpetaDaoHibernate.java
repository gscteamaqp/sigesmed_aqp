package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.maestro.carpeta;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.carpeta.ContenidoSeccionCarpetaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Rol;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.ArchivosCarpeta;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.ContenidoSeccionCarpeta;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.ContenidoFichaEvaluacion;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;

import java.util.List;

/**
 * Created by Administrador on 28/12/2016.
 */
public class ContenidoSeccionCarpetaDaoHibernate extends GenericDaoHibernate<ContenidoSeccionCarpeta> implements ContenidoSeccionCarpetaDao{
    @Override
    public Usuario buscarUsuarioporId(int idUser) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(Usuario.class)
                    .add(Restrictions.eq("usuId",idUser));
            return (Usuario) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<Rol> buscarRolesUsuario(int idUsuario) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT r FROM Rol r INNER JOIN r.usuarios us INNER JOIN us.usuario u WHERE u.usuId =:idUsu";
            Query query = session.createQuery(hql);
            query.setInteger("idUsu",idUsuario);
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public ContenidoSeccionCarpeta buscarContenidoPorId(int idCont) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(ContenidoSeccionCarpeta.class)
                    .add(Restrictions.eq("conSecCarPedId",idCont));
            return (ContenidoSeccionCarpeta) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<ArchivosCarpeta> listarDocumentosCarpeta(int idCar, int idIE,int idUgel, int idDoc) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria criteria1 = session.createCriteria(ContenidoSeccionCarpeta.class, "con")
                    .createCriteria("con.seccion", "sec", JoinType.INNER_JOIN)
                    .createCriteria("sec.carpeta", "car", JoinType.INNER_JOIN)
                    .createCriteria("con.rutas", "ru", JoinType.LEFT_OUTER_JOIN)
                    .add(Restrictions.eq("car.carDigId", idCar))
                    .add(Restrictions.eq("ru.usuario.usuId",idDoc))
                    .add(Restrictions.eq("ru.organizacion.orgId", idIE))
                    .setProjection(Projections.projectionList()
                            .add(Projections.property("car.carDigId"),"carDigId")
                            .add(Projections.property("sec.secCarPedId"),"secCarPedId")
                            .add(Projections.property("sec.ord"),"ord")
                            .add(Projections.property("sec.nom"), "secNom")
                            .add(Projections.property("con.conSecCarPedId"),"conSecCarPedId")
                            .add(Projections.property("con.nom"), "conNom")
                            .add(Projections.property("ru.rutConCarId"),"rutConCarId")
                            .add(Projections.property("con.tipUsu"),"tipUsu")
                            .add(Projections.property("ru.pat"),"pat")
                            .add(Projections.property("ru.nomFil"),"nomFil"))
                    .setResultTransformer(Transformers.aliasToBean(ArchivosCarpeta.class));
            Criteria criteria2 = session.createCriteria(ContenidoSeccionCarpeta.class, "con")
                    .createCriteria("con.seccion", "sec", JoinType.INNER_JOIN)
                    .createCriteria("sec.carpeta", "car", JoinType.INNER_JOIN)
                    .createCriteria("con.rutas", "ru", JoinType.LEFT_OUTER_JOIN)
                    .add(Restrictions.eq("car.carDigId", idCar))
                    .add(Restrictions.eq("con.estReg", 'A'))
                    .add(Restrictions.eq("sec.estReg", 'A'))
                    //.add(Restrictions.ne("con.tipUsu", 3))
                    .add(Restrictions.or(Restrictions.ne("ru.tipUsu", 3), Restrictions.isNull("ru.tipUsu")))
                    .add(Restrictions.or(new Criterion[]{Restrictions.eq("ru.organizacion.orgId", idIE), Restrictions.eq("ru.organizacion.orgId", idUgel), Restrictions.isNull("ru.organizacion.orgId")}))
                    .setProjection(Projections.projectionList()
                            .add(Projections.property("car.carDigId"),"carDigId")
                            .add(Projections.property("sec.secCarPedId"),"secCarPedId")
                            .add(Projections.property("sec.ord"),"ord")
                            .add(Projections.property("sec.nom"), "secNom")
                            .add(Projections.property("con.conSecCarPedId"),"conSecCarPedId")
                            .add(Projections.property("con.nom"), "conNom")
                            .add(Projections.property("ru.rutConCarId"),"rutConCarId")
                            .add(Projections.property("con.tipUsu"),"tipUsu")
                            .add(Projections.property("ru.pat"),"pat")
                            .add(Projections.property("ru.nomFil"),"nomFil"))
                    .setResultTransformer(Transformers.aliasToBean(ArchivosCarpeta.class));
            List<ArchivosCarpeta> archivos = criteria1.list();
            archivos.addAll(criteria2.list());
            return archivos;
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<ArchivosCarpeta> listarDocumentosCarpetaAdmin(int idCar, int idOrg,int idUser, int tipUsuario) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria criteria = session.createCriteria(ContenidoSeccionCarpeta.class, "con")
                    .createCriteria("con.seccion", "sec", JoinType.INNER_JOIN)
                    .createCriteria("sec.carpeta", "car", JoinType.INNER_JOIN)
                    .createCriteria("con.rutas", "ru", JoinType.LEFT_OUTER_JOIN)
                    .add(Restrictions.eq("car.carDigId", idCar))
                    .add(Restrictions.eq("con.estReg", 'A'))
                    .add(Restrictions.eq("sec.estReg", 'A'))
                    .add(Restrictions.eq("con.tipUsu", tipUsuario))
                    .add(Restrictions.or(Restrictions.eq("ru.organizacion.orgId", idOrg), Restrictions.isNull("ru.organizacion.orgId")))
                    .add(Restrictions.or(Restrictions.eq("ru.usuario.usuId", idUser), Restrictions.isNull("ru.usuario.usuId")))
                    .setProjection(Projections.projectionList()
                            .add(Projections.property("car.carDigId"), "carDigId")
                            .add(Projections.property("sec.secCarPedId"), "secCarPedId")
                            .add(Projections.property("sec.ord"), "ord")
                            .add(Projections.property("sec.nom"), "secNom")
                            .add(Projections.property("con.conSecCarPedId"), "conSecCarPedId")
                            .add(Projections.property("con.nom"), "conNom")
                            .add(Projections.property("ru.rutConCarId"), "rutConCarId")
                            .add(Projections.property("con.tipUsu"), "tipUsu")
                            .add(Projections.property("ru.pat"), "pat")
                            .add(Projections.property("ru.nomFil"), "nomFil"))
                    .setResultTransformer(Transformers.aliasToBean(ArchivosCarpeta.class));
            return criteria.list();

        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

}
