package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.CriterioCertificacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.CursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.MotivoIncumplimientoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.CriterioCertificacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.CursoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.MotivoIncumplimiento;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class RegistrarCriterioTx implements ITransaction {
    
    private static final Logger logger = Logger.getLogger(RegistrarCriterioTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            JSONObject data = (JSONObject) wr.getData();        
            JSONArray array = data.getJSONArray("criterios");

            CursoCapacitacionDao capacitacionDao = (CursoCapacitacionDao) FactoryDao.buildDao("capacitacion.CursoCapacitacionDao");
            CriterioCertificacionDao criterioCertificacionDao = (CriterioCertificacionDao) FactoryDao.buildDao("capacitacion.CriterioCertificacionDao");
            MotivoIncumplimientoDao motivoIncumplimientoDao = (MotivoIncumplimientoDao) FactoryDao.buildDao("capacitacion.MotivoIncumplimientoDao");

            CursoCapacitacion capacitacion = capacitacionDao.buscarPorId(data.getInt("cod"));
            
            for(int i = 0;i < array.length();i++) {
                JSONObject object = array.getJSONObject(i);
                CriterioCertificacion criterio = new CriterioCertificacion(object.getString("nom"), 
                        object.getString("est").charAt(0), object.getString("id").charAt(0), capacitacion
                );
                
                criterioCertificacionDao.insert(criterio);
                
                if(criterio.getEstReg() == 'A') {
                    criterio.setTipEva(object.getString("evaCod").charAt(0));
                    criterio.setVal(object.getDouble("val"));
                    
                    switch(criterio.getTipCri()) {
                        case 'C':
                            criterio.setNotMin(object.getDouble("notMin"));
                            break;

                        case 'H':
                            criterio.setNotMin(object.getDouble("notMin"));
                            break;
                            
                        case 'F':
                            JSONArray motivos = object.getJSONArray("mot");
                            for(int j = 0;j < motivos.length();j++) {
                                JSONObject motivo = motivos.getJSONObject(j);
                                MotivoIncumplimiento motive = new MotivoIncumplimiento(motivo.getString("mot"),
                                    motivo.getString("jus"), criterio
                                );
                                
                                criterio.getCriterios().add(motive);
                                motivoIncumplimientoDao.insert(motive);
                            }
                            break;
                    }
                    
                    criterioCertificacionDao.update(criterio);
                }
            }
            
            return WebResponse.crearWebResponseExito("Los criterios de certificación del curso de capacitación fueron creados correctamente", WebResponse.OK_RESPONSE);
        } catch (Exception e){
            logger.log(Level.SEVERE,"registrarCriterio",e);
            return WebResponse.crearWebResponseError("No se pudo registrar los criterios de certificación del curso de capacitación",WebResponse.BAD_RESPONSE);
        }
    }
}
