/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sci;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.RegistroComprasDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.RegistroCompras;

/**
 *
 * @author G-16
 */
public class RegistroComprasDaoHibernate extends GenericDaoHibernate<RegistroCompras> implements RegistroComprasDao {
    
}