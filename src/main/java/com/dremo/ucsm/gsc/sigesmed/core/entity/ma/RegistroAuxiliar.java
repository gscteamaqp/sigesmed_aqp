package com.dremo.ucsm.gsc.sigesmed.core.entity.ma;

import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.GradoIEEstudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.PeriodosPlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.IndicadorAprendizaje;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

/**
 * Created by Administrador on 24/01/2017.
 */
@Entity
@Table(name = "registro_auxiliar", schema = "pedagogico")
public class RegistroAuxiliar implements java.io.Serializable {
    @Id
    @Column(name = "reg_aux_id",nullable = false, unique = true)
    @SequenceGenerator(name = "registro_auxiliar_reg_aux_id_seq",sequenceName = "pedagogico.registro_auxiliar_reg_aux_id_seq")
    @GeneratedValue(generator = "registro_auxiliar_reg_aux_id_seq")
    private int regAuxId;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ind_apr_id")
    private IndicadorAprendizaje indicador;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "doc_id")
    private Docente docente;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "per_pla_est_id")
    private PeriodosPlanEstudios periodo;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "are_cur_id")
    private AreaCurricular area;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gra_ie_est_id")
    private GradoIEEstudiante gradoEstudiante;
    
    @Column(name = "not_ind", length = 4,nullable = false)
    private String notInd;
    
    @Column(name = "not_ind_doc", length = 4)
    private String notIndDoc;
    
    @Column(name="not_ind_lit" , length = 2)
    private String not_ind_lit;
    
    
    @Column(name="not_ind_lit_doc" , length=2)
    private String not_ind_lit_doc;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length=29)
    private Date fecMod;
    
    @Column(name = "est_reg",length = 1)
    private Character estReg;

    
    
    
    
    public RegistroAuxiliar() {
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public RegistroAuxiliar(String notInd) {
        this.notInd = notInd;
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public int getRegAuxId() {
        return regAuxId;
    }

    public IndicadorAprendizaje getIndicador() {
        return indicador;
    }

    public void setIndicador(IndicadorAprendizaje indicador) {
        this.indicador = indicador;
    }

    public Docente getDocente() {
        return docente;
    }

    public void setDocente(Docente docente) {
        this.docente = docente;
    }

    public PeriodosPlanEstudios getPeriodo() {
        return periodo;
    }

    public void setPeriodo(PeriodosPlanEstudios periodo) {
        this.periodo = periodo;
    }

    public AreaCurricular getArea() {
        return area;
    }

    public void setArea(AreaCurricular area) {
        this.area = area;
    }

    public GradoIEEstudiante getGradoEstudiante() {
        return gradoEstudiante;
    }

    public void setGradoEstudiante(GradoIEEstudiante gradoEstudiante) {
        this.gradoEstudiante = gradoEstudiante;
    }

    public String getNotInd() {
        return notInd;
    }

    public void setNotInd(String notInd) {
        this.notInd = notInd;
    }

    public String getNotIndDoc() {
        return notIndDoc;
    }

    public void setNotIndDoc(String notIndDoc) {
        this.notIndDoc = notIndDoc;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    @Override
    public String toString() {
        return "RegistroAuxiliar{" + "notInd=" + notInd + ", notIndDoc=" + notIndDoc + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RegistroAuxiliar other = (RegistroAuxiliar) obj;
        if (this.regAuxId != other.regAuxId) {
            return false;
        }
        if (!Objects.equals(this.indicador, other.indicador)) {
            return false;
        }
        if (!Objects.equals(this.docente, other.docente)) {
            return false;
        }
        if (!Objects.equals(this.periodo, other.periodo)) {
            return false;
        }
        if (!Objects.equals(this.area, other.area)) {
            return false;
        }
        if (!Objects.equals(this.gradoEstudiante, other.gradoEstudiante)) {
            return false;
        }
        if (!Objects.equals(this.notInd, other.notInd)) {
            return false;
        }
        if (!Objects.equals(this.notIndDoc, other.notIndDoc)) {
            return false;
        }
        if (!Objects.equals(this.usuMod, other.usuMod)) {
            return false;
        }
        if (!Objects.equals(this.fecMod, other.fecMod)) {
            return false;
        }
        if (!Objects.equals(this.estReg, other.estReg)) {
            return false;
        }
        return true;
    }

    public void setNot_ind_lit(String not_ind_lit) {
        this.not_ind_lit = not_ind_lit;
    }

    public String getNot_ind_lit() {
        return not_ind_lit;
    }

    public String getNot_ind_lit_doc() {
        return not_ind_lit_doc;
    }

    public void setNot_ind_lit_doc(String not_ind_lit_doc) {
        this.not_ind_lit_doc = not_ind_lit_doc;
    }
    
    
    
    
}
