package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.maestro.plan;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.CompetenciaCapacidadDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CompetenciaCapacidad;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.IndicadorAprendizaje;
import org.hibernate.*;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.DistinctResultTransformer;

import java.util.List;

/**
 * Created by Administrador on 13/10/2016.
 */
public class CompetenciaCapacidadDaoHibernate extends GenericDaoHibernate<CompetenciaCapacidad> implements CompetenciaCapacidadDao{
    @Override
    public List<CompetenciaCapacidad> buscarCapacidadesDeCompetencia(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria criteria = session.createCriteria(CompetenciaCapacidad.class)
                    .add(Restrictions.eq("estReg", 'A'))
                    .createCriteria("com",JoinType.INNER_JOIN).add(Restrictions.eq("comId", id))
                    .setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
            return criteria.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public CompetenciaCapacidad buscarCapacidadDeCompetencia(int idCap, int idCom) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria criteria = session.createCriteria(CompetenciaCapacidad.class)
                    .add(Restrictions.eq("id",new CompetenciaCapacidad.Id(idCom,idCap)));
            return (CompetenciaCapacidad) criteria.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
}
