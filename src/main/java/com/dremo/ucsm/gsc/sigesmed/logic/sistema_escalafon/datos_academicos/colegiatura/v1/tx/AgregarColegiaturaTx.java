/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.colegiatura.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ColegiaturaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Colegiatura;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class AgregarColegiaturaTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(AgregarColegiaturaTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
         /*
        *   Parte para la lectura, verificacion y validacion de datos
         */
        Colegiatura colegiatura = null;
        
        Integer ficEscId = 0;

        try {
            JSONObject requestData = (JSONObject) wr.getData();

            ficEscId = requestData.getInt("ficEscId");
            String nomColPro = requestData.getString("nomColPro");
            String numRegCol = requestData.getString("numRegCol");
            Boolean conReg = requestData.getBoolean("conReg");
            
            colegiatura = new Colegiatura(new FichaEscalafonaria(ficEscId), nomColPro, numRegCol, conReg ,wr.getIdUsuario(), new Date(), 'A');

        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE,"Datos nueva colegiatura",e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }
        //Fin

        /*
        *  Parte para la operacion en la Base de Datos
         */
        //si el pariente no existe en la tabla persona
        ColegiaturaDao colegiaturaDao = (ColegiaturaDao) FactoryDao.buildDao("se.ColegiaturaDao");
        try {
            colegiaturaDao.insert(colegiatura);
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar nueva colegiatura",e);
            System.out.println(e);
        }
        
        //Fin       

        /*
        *  Repuesta Correcta
         */
        JSONObject oResponse = new JSONObject();
        oResponse.put("colId", colegiatura.getColId());
        oResponse.put("nomColPro", colegiatura.getNomColPro());
        oResponse.put("numRegCol", colegiatura.getNumRegCol());
        oResponse.put("conReg", colegiatura.getConReg());
                
        return WebResponse.crearWebResponseExito("El registro de la colegiatura se realizo correctamente", oResponse);
        //Fin
    }
    
}
