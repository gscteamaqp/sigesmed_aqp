/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.scp;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import java.util.List;

/**
 *
 * @author Jeferson
 */
@Entity
@Table(name="bajas", schema="administrativo")
public class Bajas {
    
    @Id
    @Column(name="bajas_id" , unique=true, nullable=false)
    @SequenceGenerator(name="secuencia_inv_tra",sequenceName="administrativo.bajas_bajas_id_seq")
    @GeneratedValue(generator="secuencia_inv_tra")
    private int bajas_id;
    
    @Column(name="mov_ing_id")
    private int mov_ing_id;
            
    @Column(name="cau_ba_id")
    private int cau_ba_id;
    
    @Column(name="usu_mod")
    private int usu_mod;
    
    @Column(name="org_id")
    private int org_id;
    
    @Column(name="est_reg")
    private char est_reg;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cau_ba_id" , insertable=false , updatable=false)
    private CausalBaja causal_baja;

    public CausalBaja getCausal_baja() {
        return causal_baja;
    }

    public void setCausal_baja(CausalBaja causal_baja) {
        this.causal_baja = causal_baja;
    }
    
    @OneToMany(fetch=FetchType.LAZY)
    @JoinColumn(name="bajas_id" , insertable=false , updatable=false)
    private List<BajasDetalle> bd ;

    public void setBd(List<BajasDetalle> bd) {
        this.bd = bd;
    }

    public List<BajasDetalle> getBd() {
        return bd;
    }

    public void setBajas_id(int bajas_id) {
        this.bajas_id = bajas_id;
    }

    public void setMov_ing_id(int mov_ing_id) {
        this.mov_ing_id = mov_ing_id;
    }

    public void setCau_ba_id(int cau_ba_id) {
        this.cau_ba_id = cau_ba_id;
    }

    public void setUsu_mod(int usu_mod) {
        this.usu_mod = usu_mod;
    }

    public void setOrg_id(int org_id) {
        this.org_id = org_id;
    }

    public void setEst_reg(char est_reg) {
        this.est_reg = est_reg;
    }

    public int getBajas_id() {
        return bajas_id;
    }

    public int getMov_ing_id() {
        return mov_ing_id;
    }

    public int getCau_ba_id() {
        return cau_ba_id;
    }

    public int getUsu_mod() {
        return usu_mod;
    }

    public int getOrg_id() {
        return org_id;
    }

    public char getEst_reg() {
        return est_reg;
    }

    public Bajas() {
    }

    public Bajas(int bajas_id, int mov_ing_id, int cau_ba_id, int usu_mod, int org_id, char est_reg) {
        this.bajas_id = bajas_id;
        this.mov_ing_id = mov_ing_id;
        this.cau_ba_id = cau_ba_id;
        this.usu_mod = usu_mod;
        this.org_id = org_id;
        this.est_reg = est_reg;
    }
    
    
    
    
    
    
    
    
}
