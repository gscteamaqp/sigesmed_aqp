/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.UsuarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.TrabajadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class BuscarPersonaTx implements ITransaction {

    //Datos requeridos
    String nombreUser = "";
    int orgId=0;
    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            nombreUser = requestData.getString("nomUser");
            orgId=requestData.getInt("organizacionID");
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo leer los datos para buscar a la persona");
        }
        
        //Lectura con base
//        OrganizacionDao orgDao= (OrganizacionDao)FactoryDao.buildDao("OrganizacionDao");
//        orgDao.buscarTrabajadorEnOrganizacion(orgId,nombreUser);
        List<UsuarioSession> usuarios = null;
        TrabajadorDao trabDao = (TrabajadorDao)FactoryDao.buildDao("rdg.TrabajadorDao");
        try{
            usuarios = trabDao.buscarConRolPorOrganizacionYNombre(orgId,nombreUser);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los usuarios del Sistema\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los usuarios del Sistema ", e.getMessage() );
        }
        
        
        JSONArray miArray = new JSONArray();
        for(UsuarioSession session:usuarios ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("usuarioSesionID",session.getUsuSesId());
            oResponse.put("usuarioID",session.getUsuario().getUsuId());
            oResponse.put("nombre",session.getPersona().getNom());
            oResponse.put("apellidoPat",session.getPersona().getApePat());
            oResponse.put("apellidoMat",session.getPersona().getApeMat());
            oResponse.put("nombreCompleto", session.getPersona().getNom() +" "+ session.getPersona().getApePat() +" "+ session.getPersona().getApeMat());
            oResponse.put("nombreCompletoRol", session.getPersona().getNom() +" "+ session.getPersona().getApePat() +" "+ session.getPersona().getApeMat()+ " (" + session.getRol().getNom() + ")");
            oResponse.put("rolID",session.getRol().getRolId() );
            oResponse.put("rol",session.getRol().getNom() );
            oResponse.put("sessionID",session.getUsuSesId() );
            oResponse.put("organizacion",session.getOrganizacion().getNom());
            oResponse.put("organizacionID",session.getOrganizacion().getOrgId());
                        
            oResponse.put("estado",""+session.getEstReg());
            miArray.put(oResponse);
        }                               
        return WebResponse.crearWebResponseExito("Exito", miArray);
    }
    
}
