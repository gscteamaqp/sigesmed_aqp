/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.std.HistorialExpedienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.EntidadCantidadModel;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
/**
 *
 * @author angel
 */
public class ReporteEstadisticaExpedienteTrabajadorPorAreaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int areaID = 0;
        Date desde = null;
        Date hasta = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            areaID = requestData.getInt("areaID");
            
            if(!requestData.getString("desde").contentEquals(""))
                desde = new SimpleDateFormat("dd/M/yyyy").parse( requestData.getString("desde"));
            if(!requestData.getString("hasta").contentEquals(""))
                hasta = new SimpleDateFormat("dd/M/yyyy HH:mm:ss").parse( requestData.getString("hasta") + " 23:59:59" );
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo realizar la estadistica", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        List<EntidadCantidadModel> expedientes = null;
        
        HistorialExpedienteDao hisDao = (HistorialExpedienteDao)FactoryDao.buildDao("std.HistorialExpedienteDao");
        try{
            expedientes = hisDao.cantidadExpedientesDeTrabajadorPorAreaYFecha(areaID,desde, hasta);
            
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo realizar la estadistica", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        
        
        JSONArray aResponsables = new JSONArray();
        
        JSONArray aExpedientes = new JSONArray();        
        JSONArray aDerivados = new JSONArray();
        JSONArray aDevueltos = new JSONArray();
        JSONArray aFinalizados = new JSONArray();
        
        for(EntidadCantidadModel a:expedientes){
            
            aResponsables.put( a.nombre );
            aExpedientes.put( a.num1 );
            aDerivados.put(a.num2 );
            aDevueltos.put( a.num3 );
            aFinalizados.put(a.num4 );  
        }
       
        JSONArray aOrg = new JSONArray();

        for(int i = 0 ; i < aResponsables.length() ;i++){
            
            JSONObject o = new JSONObject();
             
           // String respons = aResponsables.get(i).toString();
            o.put("Responsaletr",aResponsables.get(i).toString());
            o.put("expedientestr",aExpedientes.get(i));
            o.put("derivadostr",aDerivados.get(i));
            o.put("devueltostr",aDevueltos.get(i));
            o.put("finalizadostr",aFinalizados.get(i));
            
            aOrg.put(o);
        }
    
       System.out.println(aOrg);
        return WebResponse.crearWebResponseExito("Se Listo correctamente",aOrg);      
        //Fin
    }
    
}

