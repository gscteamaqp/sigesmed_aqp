package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scec;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.CargoComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.CargoComision;
import org.hibernate.*;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by geank on 28/09/16.
 */
public class CargoComisionDaoHibernate extends GenericDaoHibernate<CargoComision> implements CargoComisionDao{
    private static final Logger logger = Logger.getLogger(CargoComisionDaoHibernate.class.getName());
    @Override
    public CargoComision buscarPorId(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            CargoComision cargoComision = (CargoComision)session.get(CargoComision.class,id);
            return cargoComision;
        }catch (Exception e){
            logger.log(Level.SEVERE,"buscarPorId",e);
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public CargoComision buscarCargoDefecto() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String sql = "SELECT c FROM CargoComision c WHERE lower(c.nomCar) LIKE lower('participante')";
            //SQLQuery query = session.createSQLQuery(sql);
            Query query = session.createQuery(sql);
            return (CargoComision) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public CargoComision buscarCargoPorNombre(String nombre) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String sql = "SELECT c FROM CargoComision c WHERE c.nomCar LIKE upper(:nombre)";
            //SQLQuery query = session.createSQLQuery(sql);
  
            Query query = session.createQuery(sql);
            query.setString("nombre", nombre);
            query.setMaxResults(1);
            return (CargoComision) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
}
