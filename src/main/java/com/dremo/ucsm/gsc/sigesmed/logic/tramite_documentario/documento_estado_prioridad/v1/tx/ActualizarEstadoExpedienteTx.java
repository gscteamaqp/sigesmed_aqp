/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.documento_estado_prioridad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.std.EstadoExpedienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.EstadoExpediente;
import java.util.Date;

/**
 *
 * @author abel
 */
public class ActualizarEstadoExpedienteTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        EstadoExpediente estadoExpediente = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            int estadoExpedienteID = requestData.getInt("estadoExpedienteID");
            String nombre = requestData.getString("nombre");
            String descripcion = requestData.getString("descripcion");
            String estado = requestData.getString("estado");
            
            estadoExpediente = new EstadoExpediente(estadoExpedienteID, nombre, descripcion, new Date(), wr.getIdUsuario(), estado.charAt(0));
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos");
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        EstadoExpedienteDao areaDao = (EstadoExpedienteDao)FactoryDao.buildDao("std.EstadoExpedienteDao");
        try{
            areaDao.update(estadoExpediente);
        
        }catch(Exception e){
            System.out.println("No se pudo actualizar el Estado Expediente\n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar el Tipo Area", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Estado Expediente se actualizo correctamente");
        //Fin
    }
    
}
