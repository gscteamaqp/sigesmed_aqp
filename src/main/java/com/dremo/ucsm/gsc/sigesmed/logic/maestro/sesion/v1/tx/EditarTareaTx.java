/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.maestro.sesion.v1.tx;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.SesionAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.TareaIndicadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.TareaSesionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.TareaEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.SesionAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.TareaIndicador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.TareaSesionAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaTarea;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.TareaEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.hibernate.Hibernate.isInitialized;
import org.json.JSONArray;
/**
 *
 * @author abel
 */
public class EditarTareaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            JSONObject data = (JSONObject)wr.getData();
            TareaIndicadorDao tar_ind_dao = (TareaIndicadorDao) FactoryDao.buildDao("maestro.plan.TareaIndicadorDao");
            
            TareaSesionAprendizaje tarea = tar_ind_dao.obtenerTarea(data.getInt("id"));
            tarea.setNom(data.getString("nom"));
            tarea.setDes(data.getString("des"));
        //    tarea.setFecEnt(new Date(data.getString("fec")));
            Date fec_nueva = formatter.parse(data.getString("fec"));
            Date fec_ant = formatter.parse(data.getString("fec_ant"));
            tarea.setFecEnt(fec_nueva);  
            
            TareaSesionDao tar_ses_dao = (TareaSesionDao) FactoryDao.buildDao("maestro.plan.TareaSesionDao");
            tar_ses_dao.update(tarea);
             
            TareaEscolarDao tar_esc_dao = (TareaEscolarDao) FactoryDao.buildDao("web.TareaEscolarDao");
            
            TareaEscolar tar_es = tar_esc_dao.obtenertareaWeb(tarea.getTarIdSes());
          
            
            tar_es.setNom(tarea.getNom());
            tar_es.setDes(tarea.getDes());
            tar_es.setFecEnt(tarea.getFecEnt());
            
            if(fec_nueva.compareTo(fec_ant) > 0){
                tar_es.setEstado('N');
            }
            if(fec_nueva.compareTo(new Date()) < 0){
                tar_es.setEstado('F');
            }
            
            //Actualizamos la Bandeja de Tarea de los Alumnos
            List<BandejaTarea> bandejas=null;
            bandejas = tar_es.getBandeja();
              
            
            if(isInitialized(bandejas)!=false)
            {
                
                if(fec_nueva.compareTo(fec_ant) > 0 && fec_nueva.compareTo(new Date()) > 0){
                    for(BandejaTarea bandeja : bandejas){
                        bandeja.setEstado('N');
                    }
                }
                else if(fec_nueva.compareTo(new Date()) < 0){
                    for(BandejaTarea bandeja : bandejas){
                        if(bandeja.getEstado() == 'N'){
                            bandeja.setEstado('F');
                        }   
                    }       
                }
                
            }
            tar_esc_dao.update(tar_es);
           
 
        }
        catch(Exception e){
              System.out.println(e);
             return WebResponse.crearWebResponseError("No se pudo actualizar la Tarea , Datos Incorrectos", e.getMessage() );
        }
       
        return  WebResponse.crearWebResponseExito("La Tarea se Actualizo Correctamente");
        
     //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
