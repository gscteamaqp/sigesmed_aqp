package com.dremo.ucsm.gsc.sigesmed.core.entity.web;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@IdClass(MensajeDocumentoId.class)
@Entity
@Table(name="mensaje_documento_adjunto" ,schema="pedagogico")
public class MensajeDocumento  implements java.io.Serializable {

    @Id 
    @Column(name="men_doc_adj_id", unique=true, nullable=false)
    private int menDocId;
    @Id
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="men_ele_id", nullable=false)
    private MensajeElectronico mensaje;
    
    @Column(name="nom_doc", length=64)
    private String nomDoc;

    public MensajeDocumento() {
    }

	
    public MensajeDocumento(int menDocId, MensajeElectronico mensaje) {
        this.menDocId = menDocId;
        this.mensaje = mensaje;
    }
    public MensajeDocumento(int menDocId, MensajeElectronico mensaje, String nomDoc) {
       this.menDocId = menDocId;
       this.mensaje = mensaje;
       this.nomDoc = nomDoc;
    }
   
     
    public int getMenDocId() {
        return this.menDocId;
    }
    public void setMenDocId(int menDocId) {
        this.menDocId = menDocId;
    }

    public MensajeElectronico getMensaje() {
        return this.mensaje;
    }
    
    public void setMensaje(MensajeElectronico mensaje) {
        this.mensaje = mensaje;
    }

    
    public String getNomDoc() {
        return this.nomDoc;
    }
    
    public void setNomDoc(String nomDoc) {
        this.nomDoc = nomDoc;
    }

}


