package com.dremo.ucsm.gsc.sigesmed.logic.maestro.carpeta.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.carpeta.CarpetaPedagogicaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.SeccionCarpetaPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 29/12/2016.
 */
public class RegistrarSeccionCarpetaTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(RegistrarSeccionCarpetaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idCarpeta = data.getInt("car");
        String nom = data.getString("nom").toUpperCase();
        int ord = data.getInt("ord");
        return registrarSeccion(idCarpeta,nom,ord);
    }

    private WebResponse registrarSeccion(int idCarpeta,String nom,int ord) {
        try{
            CarpetaPedagogicaDao carpetaDao = (CarpetaPedagogicaDao) FactoryDao.buildDao("maestro.carpeta.CarpetaPedagogicaDao");
            SeccionCarpetaPedagogica seccion = new SeccionCarpetaPedagogica(nom,ord);
            seccion.setCarpeta(carpetaDao.buscarCarpetaPorId(idCarpeta));
            carpetaDao.registrarSeccionCarpeta(seccion);
            return WebResponse.crearWebResponseExito("Se registro correctamente",new JSONObject().put("id",seccion.getSecCarPedId()));
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarSeccion",e);
            return WebResponse.crearWebResponseError("No se puede registrar la seccion");
        }
    }
}
