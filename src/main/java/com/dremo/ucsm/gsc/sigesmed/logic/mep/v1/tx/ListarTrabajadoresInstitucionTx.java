/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.mep.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mep.EvaluacionPersonalDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.EvaluacionesPersonal;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author geank
 */
public class ListarTrabajadoresInstitucionTx implements ITransaction{
    private static Logger  logger = Logger.getLogger(ListarTrabajadoresInstitucionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        String idInst = wr.getMetadataValue("institucion");
        try{
            return listarTrabajadores(Integer.parseInt(idInst));
        }catch(NumberFormatException e){
            logger.log(Level.SEVERE,logger.getName() + ": execute()",e);
        }
        return null;
    }
    private WebResponse listarTrabajadores(int idInst){
        WebResponse response = new WebResponse();
        EvaluacionPersonalDaoHibernate evdh = new EvaluacionPersonalDaoHibernate();
        try{
            List<EvaluacionesPersonal> trabajadores = evdh.getCantidadEvaluacionesTrabajador(idInst);
            JSONArray aData = new JSONArray();
            for(EvaluacionesPersonal trabajador : trabajadores){
                JSONObject jsonTrab = new JSONObject(trabajador.toString());
                aData.put(jsonTrab);
            }
            response.setResponse(MEP.SUCC_RESPONSE_COD);
            response.setResponseMsg(MEP.SUCC_RESPONSE_MESS_LIST);
            response.setData(aData);
        }catch(Exception e ){
            logger.log(Level.SEVERE,logger.getName() + ":listarTrabajadores()",e);
            response.setResponse(MEP.ERR_RESPONSE_COD);
            response.setResponseMsg(MEP.ERR_RESPONSE_MESS_LIST);
        }
        return response;
    }
    
}
