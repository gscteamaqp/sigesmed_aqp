package com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.MetodologiaCursoCapacitacion;

public interface MetodologiaCursoCapacitacionDao extends GenericDao<MetodologiaCursoCapacitacion> {
    
}
