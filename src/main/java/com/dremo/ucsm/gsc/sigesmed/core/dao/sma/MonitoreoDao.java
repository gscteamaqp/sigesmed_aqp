/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.sma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.Monitoreo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.MonitoreoDetalle;
import java.util.List;

/**
 *
 * @author gscadmin
 */
public interface MonitoreoDao extends GenericDao<Monitoreo>{
    public List<Monitoreo> listarMonitoreosxUgel (int ugelId);
    public List<MonitoreoDetalle> listarMonitoreoDetalle (int monId);
    public Monitoreo buscarPorId(Integer monId);
}
