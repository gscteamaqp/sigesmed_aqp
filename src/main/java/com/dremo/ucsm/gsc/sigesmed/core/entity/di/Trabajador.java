/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.di;

import java.util.List;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TrabajadorCargo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Persona;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.criteria.Fetch;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrador
 */
@Entity(name = "com.dremo.ucsm.gsc.sigesmed.core.entity.di.Trabajador")
@Table(name = "trabajador",schema="public")
public class Trabajador implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "tra_id")
    private Integer traId;
    @Column(name = "tie_serv")
    private Short tieServ;
    @Column(name = "fec_ing")
    @Temporal(TemporalType.DATE)
    private Date fecIng;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "sal")
    private BigDecimal sal;    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "est_reg")
    private String estReg;  
    @Column(name = "tra_tip")
    private String traTip;
    @JoinColumn(name = "per_id", referencedColumnName = "per_id")
    @ManyToOne(fetch=FetchType.LAZY)
    private Persona persona;    
    @JoinColumn(name = "org_id", referencedColumnName = "org_id")
    @ManyToOne(fetch=FetchType.LAZY)
    private Organizacion organizacion;
    @JoinColumn(name = "tra_car", referencedColumnName = "crg_tra_ide")
    @ManyToOne(fetch=FetchType.LAZY)
    private TrabajadorCargo traCar;
    
    public Trabajador() {
    }

    public Trabajador(Integer traId) {
        this.traId = traId;
    }

    public Trabajador(Integer traId, BigDecimal sal) {
        this.traId = traId;
        this.sal = sal;        
    }
    public Trabajador(BigDecimal sal, Persona p) {
        this.persona = p;
        this.sal = sal;        
    }

    public Trabajador(Integer traId, BigDecimal sal, Persona persona) {
        this.traId = traId;
//        this.tieServ = tieServ;
//        this.fecIng = fecIng;
        this.sal = sal;        
//        this.fecMod = fecMod;
//        this.usuMod = usuMod;
//        this.estReg = estReg;
        this.persona = persona;
//        this.traTip = traTip;
//        this.organizacion = organizacion;
//        this.parientes = parientes;
    }

    public Trabajador(Integer traId, Short tieServ, Date fecIng, BigDecimal sal, Date fecMod, Integer usuMod, String estReg, Persona persona, String traTip, Organizacion organizacion) {
        this.traId = traId;
        this.tieServ = tieServ;
        this.fecIng = fecIng;
        this.sal = sal;        
        this.fecMod = fecMod;
        this.usuMod = usuMod;
        this.estReg = estReg;
        this.persona = persona;
        this.traTip = traTip;
        this.organizacion = organizacion;
        
    }
        
    public Trabajador(Integer traId, Persona persona, Organizacion organizacion) {
        this.traId = traId;
//        this.tieServ = tieServ;
//        this.fecIng = fecIng;
//        this.sal = sal;
//        this.traCar = traCar;
//        this.fecMod = fecMod;
//        this.usuMod = usuMod;
//        this.estReg = estReg;
        this.persona = persona;
//        this.traTip = traTip;
        this.organizacion = organizacion;
        
    }

    public Integer getTraId() {
        return traId;
    }

    public void setTraId(Integer traId) {
        this.traId = traId;
    }

    public Short getTieServ() {
        return tieServ;
    }

    public void setTieServ(Short tieServ) {
        this.tieServ = tieServ;
    }

    public Date getFecIng() {
        return fecIng;
    }

    public void setFecIng(Date fecIng) {
        this.fecIng = fecIng;
    }

    public BigDecimal getSal() {
        return sal;
    }

    public void setSal(BigDecimal sal) {
        this.sal = sal;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public String getTraTip() {
        return traTip;
    }

    public void setTraTip(String traTip) {
        this.traTip = traTip;
    }

    public Organizacion getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(Organizacion organizacion) {
        this.organizacion = organizacion;
    }

    public TrabajadorCargo getTraCar() {
        return traCar;
    }

    public void setTraCar(TrabajadorCargo traCar) {
        this.traCar = traCar;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (traId != null ? traId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Trabajador)) {
            return false;
        }
        Trabajador other = (Trabajador) object;
        if ((this.traId == null && other.traId != null) || (this.traId != null && !this.traId.equals(other.traId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Trabajador[ traId=" + traId + " ]";
    }
    
}
