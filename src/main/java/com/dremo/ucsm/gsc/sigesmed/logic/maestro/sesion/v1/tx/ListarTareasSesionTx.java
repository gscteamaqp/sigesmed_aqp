package com.dremo.ucsm.gsc.sigesmed.logic.maestro.sesion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.SesionAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.TareaSesionAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 15/12/2016.
 */
public class ListarTareasSesionTx implements ITransaction{
    private static Logger logger = Logger.getLogger(ListarTareasSesionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idSesion = data.getInt("sesid");
        return listarTareasSesion(idSesion);
    }

    private WebResponse listarTareasSesion(int idSesion) {
        try{
            SesionAprendizajeDao sesionDao = (SesionAprendizajeDao) FactoryDao.buildDao("maestro.plan.SesionAprendizajeDao");
            List<TareaSesionAprendizaje> tareas = sesionDao.listarTareasSesion(idSesion);
            JSONObject jsonResult = new JSONObject();
            jsonResult.put("tareas",new JSONArray(EntityUtil.listToJSONString(
                    new String[]{"tarIdSes", "nom","des","fecEnt","sec"},
                    new String[]{"id", "nom", "des", "fec","sec"},
                    tareas
            )));

            return WebResponse.crearWebResponseExito("Se listo correctamente la lista de materiales",jsonResult);
        }catch (Exception e){
            logger.log(Level.SEVERE,"listarmaterialesUnidad",e);
            return WebResponse.crearWebResponseError("no se pudo listar la lista de materiales");
        }
    }
}
