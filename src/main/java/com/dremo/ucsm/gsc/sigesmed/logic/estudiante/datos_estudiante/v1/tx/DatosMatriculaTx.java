/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.estudiante.datos_estudiante.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.PlanEstudiosDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mpf.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.AreaModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.NivelModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanHoraArea;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.AreaExonerada;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.Matricula;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author carlos
 */
public class DatosMatriculaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
       Long matriculaId;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            matriculaId = requestData.getLong("matriculaID");  
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo verificar los datos", e.getMessage() );
        }

        Matricula estudiante = null;
        EstudianteDao estudianteDao = (EstudianteDao)FactoryDao.buildDao("mpf.EstudianteDao");
        List tutores=null;
        List<AreaExonerada> exoneraciones=null;
        List<AreaModel> areas = null;
        try{
            
            estudiante=estudianteDao.getDatosMatricula(matriculaId);
            tutores=estudianteDao.ListarTutores(estudiante.getGrado().getGraId(), estudiante.getSeccion().getSedId(),estudiante.getPlanNivel().getPlaEstId());
            exoneraciones=estudianteDao.listarCursosExonerados(estudiante.getEstudiante());
            
            if(exoneraciones.size()>0)
            {
                List<Integer> exoIds=new ArrayList<>();
                for(AreaExonerada exo:exoneraciones)
                {
                    exoIds.add(exo.getId());
                }
                areas = estudianteDao.buscarAreasByPlanEstudios(estudiante.getPlanNivel().getPlaEstId(),estudiante.getGrado().getGraId(),estudiante.getSeccionId(),exoIds);
            }
            else
            {
                    areas = estudianteDao.buscarAreasByPlanEstudios(estudiante.getPlanNivel().getPlaEstId(),estudiante.getGrado().getGraId(),estudiante.getSeccionId());
                
            }
            
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo  obtener datos de la Matricula", e.getMessage() );
        }
        
            JSONArray oArray=new JSONArray();
            JSONObject oRes = new JSONObject();
           
            if(tutores.size()>0)
            {
                for(Object object:tutores)
                {
                    JSONObject oResponse = new JSONObject();
                    Map row = (Map)object;
                    oResponse.put("nivel", estudiante.getPlanNivel().getDes());
                    oResponse.put("grado", estudiante.getGrado().getNom());
                    oResponse.put("gradoId", estudiante.getGrado().getGraId());
                    oResponse.put("seccion", estudiante.getSeccion().getNom());
                    oResponse.put("seccionId", estudiante.getSeccion().getSedId());
                    oResponse.put("turno", estudiante.getPlanNivel().getTurno().getNom());
                    oResponse.put("tutor", row.get("paterno")+" "+row.get("materno")+" "+row.get("nombre"));
                    oArray.put(oResponse);
                }
            }
            else
            {
                JSONObject oResponse = new JSONObject();
                oResponse.put("nivel", estudiante.getPlanNivel().getDes());
                oResponse.put("grado", estudiante.getGrado().getNom());
                oResponse.put("gradoId", estudiante.getGrado().getGraId());
                oResponse.put("seccion", estudiante.getSeccion().getNom());
                oResponse.put("seccionId", estudiante.getSeccion().getSedId());
                oResponse.put("turno", estudiante.getPlanNivel().getTurno().getNom());
                oResponse.put("tutor", "");
                oArray.put(oResponse);
                
            }
            JSONArray oArrayAreas=new JSONArray();
            if(areas!=null && areas.size()>0)
            {
                for(AreaModel obj:areas)
                {
                    JSONObject oResponse = new JSONObject();
                    oResponse.put("area", obj.getArea());
                    oResponse.put("areaId", obj.getAreaID());
                    oResponse.put("docente", obj.getNombresPM());
                    oArrayAreas.put(oResponse);
                }
                
            }
             
            oRes.put("matricula", oArray);
            oRes.put("areas", oArrayAreas);
            oRes.put("obs", estudiante.getObs());

            
        
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",oRes);     
        //Fin
    }
    
}

