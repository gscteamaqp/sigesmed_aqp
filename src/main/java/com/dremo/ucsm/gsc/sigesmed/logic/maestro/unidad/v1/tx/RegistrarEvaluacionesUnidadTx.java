package com.dremo.ucsm.gsc.sigesmed.logic.maestro.unidad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.EvaluacionesUnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.UnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.me.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.EvaluacionEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.EvaluacionesUnidadDidactica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.UnidadDidactica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaEvaluacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.EvaluacionEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Alertas;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.AlertManager;
import java.math.BigInteger;
import java.util.ArrayList;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 03/01/2017.
 */
public class RegistrarEvaluacionesUnidadTx implements ITransaction{
    private static Logger logger = Logger.getLogger(RegistrarEvaluacionesUnidadTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idUnidad = data.getInt("uni");
        String tipUnidad = data.getString("tipUni");
        int tipEvaluacion = data.getInt("tip");
        char sec = data.optString("sec").charAt(0);
        String nombre_ev = data.getString("nom");
        String des = data.optString("des","DESCRIPCION DE LA EVALUACION");
        int planID = data.getInt("planID");
        int gradoID = data.getInt("gradoID");
        int areaID = data.getInt("areaID");
        int usuMod = data.getInt("usuMod");
        
        int org = data.getInt("org");
        long fec = data.optLong("fec",new Date().getTime());
        

        return registrarEvaluacion(idUnidad,tipUnidad, tipEvaluacion, des, fec,sec,nombre_ev,planID,gradoID,areaID,usuMod,org);
    }

    private WebResponse registrarEvaluacion(int idUnidad,String tipUnidad, int tipEvaluacion, String des, long fec, char seccion,String nombre,int planID, int gradoID , int areaID,int usuMod,int org) {
        try{
            EvaluacionesUnidadDidacticaDao evaDao = (EvaluacionesUnidadDidacticaDao) FactoryDao.buildDao("maestro.plan.EvaluacionesUnidadDidacticaDao");
            EvaluacionEscolarDao evaesDao = (EvaluacionEscolarDao) FactoryDao.buildDao("web.EvaluacionEscolarDao");
            
            UnidadDidacticaDao uniDao = (UnidadDidacticaDao) FactoryDao.buildDao("maestro.plan.UnidadDidacticaDao");

            //INSERTAMOS EN EL MODULO MAESTRO
            EvaluacionesUnidadDidactica evaluacion = new EvaluacionesUnidadDidactica(des,new Date(fec),tipEvaluacion);
          //  evaluacion.setUnidad(uniDao.buscarUnidadDidactica(idUnidad,tipUnidad));
            UnidadDidactica unidad = uniDao.buscarUnidadDidactica(idUnidad,tipUnidad);
            evaluacion.setSec(seccion);
            evaluacion.setUni_did_id(unidad.getUniDidId());
            evaluacion.setNom_eva(nombre);
            
            evaDao.insert(evaluacion);
            
            //INSERTAMOS LA EVALUACION EN EL MODULO WEB
            EvaluacionEscolar evEsc = new EvaluacionEscolar();
            evEsc.setNom(nombre);
            evEsc.setDes(des);
            evEsc.setFecEnv(new Date());
            evEsc.setFecFin(new Date(fec));
            evEsc.setFecIni(new Date(fec));
            evEsc.setPlaEstId(planID);
            evEsc.setGraId(gradoID);
            evEsc.setAreCurId(areaID);
            evEsc.setSecId(seccion);
            evEsc.setEva_maes_id(evaluacion.getEvaUniDidId()); //Por motivos de Busqueda
            evEsc.setEstReg('A');
            evEsc.setEstado('N');
            evEsc.setNumInt(2);
            evEsc.setUsuMod(usuMod);
            evEsc.setFecMod(new Date());
            
            if(tipEvaluacion==2 || tipEvaluacion==3 ){
                return enviar_evaluacion_alumnos(evEsc ,org);
            }

            evaesDao.insert(evEsc);
            
            return WebResponse.crearWebResponseExito("Se registro la evaluacion", new JSONObject()
                    .put("id",evaluacion.getEvaUniDidId()));
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarEvaluacion",e);
            return WebResponse.crearWebResponseError("no se puede regitrar la evaluacion");
        }
    }
    
    private WebResponse enviar_evaluacion_alumnos(EvaluacionEscolar evaluacion ,int organizacionID){
        EvaluacionEscolarDao eva_dao = (EvaluacionEscolarDao)FactoryDao.buildDao("web.EvaluacionEscolarDao");
        EstudianteDao estDao = (EstudianteDao)FactoryDao.buildDao("me.EstudianteDao");        
        try{
            
            List<Object[]> alumnos = estDao.buscarEstudiantesIdsPorGradoYSeccion(organizacionID, evaluacion.getGraId(), evaluacion.getSecId());
            
            //si hay alumnos se envia la tarea
            if(alumnos==null || alumnos.size()==0){
                System.out.println("No hay alumnos para enviar tarea");
                return WebResponse.crearWebResponseError("No hay alumnos para enviar tarea" );
            }
            evaluacion.setEstado('E');
            eva_dao.insert(evaluacion);
            
            List<BandejaEvaluacion> bandejas = new ArrayList<BandejaEvaluacion>();
            List<Integer> ids = new ArrayList<Integer>();
            for(Object[] alumno : alumnos){
                BandejaEvaluacion nueva_bandeja = new BandejaEvaluacion(0,new Date(),new Date(),0,"",'E',evaluacion.getEvaEscId(),(Integer)alumno[0]);
                bandejas.add(nueva_bandeja);  
                ids.add((Integer)alumno[1]); 
            }

            eva_dao.enviarEvaluacion(evaluacion, bandejas);
            AlertManager.sendAll(Alertas.WEB_NUEVA_EVALUACION, ids);             
        }
        catch(Exception e){
            System.out.println("No se pudo enviar la Evaluacion\n"+e);
            return WebResponse.crearWebResponseError("No se pudo enviar la Evaluacion", e.getMessage() );
        }
        
        return WebResponse.crearWebResponseExito("Se registro la evaluacion", new JSONObject()
                    .put("id",evaluacion.getEva_maes_id()));
          
    }
}

