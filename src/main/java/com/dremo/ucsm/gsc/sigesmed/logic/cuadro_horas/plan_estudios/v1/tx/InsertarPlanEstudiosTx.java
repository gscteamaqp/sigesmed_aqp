/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.plan_estudios.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.PlanEstudiosDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanNivel;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.ArrayList;
import java.util.Date;
import org.json.JSONArray;
/**
 *
 * @author abel
 */
public class InsertarPlanEstudiosTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        PlanEstudios nuevo = null;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();            
            int orgId = requestData.getInt("organizacionID");
            String nombre = requestData.getString("nombre");
            JSONArray niveles = requestData.getJSONArray("niveles");
            
            Date año = new Date();        
            año.setSeconds(0);
            año.setMinutes(0);
            año.setHours(0);
            año.setMonth(0);
            año.setDate(1);
            
            nuevo = new PlanEstudios(0,nombre,orgId,año,new Date(),wr.getIdUsuario(),'A');
            
            if(niveles.length() > 0){
                nuevo.setNiveles(new ArrayList<PlanNivel>());
                for(int i = 0; i < niveles.length();i++){
                    JSONObject bo =niveles.getJSONObject(i);
                    
                    nuevo.getNiveles().add( new PlanNivel(bo.getString("descripcion"),nuevo, bo.getString("periodoID").charAt(0),bo.getString("turnoID").charAt(0),bo.getInt("jornadaID"),new Date(),wr.getIdUsuario(),'A') );
                }
            }
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar el el plan de estudios, datos incorrectos", e.getMessage() );
        }
        try{
            PlanEstudiosDao planDao = (PlanEstudiosDao)FactoryDao.buildDao("mech.PlanEstudiosDao");
            planDao.insert(nuevo);
            planDao.reiniciarPlazasMagisteriales(nuevo.getOrgId());
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar el el plan de estudios", e.getMessage() );
        }
        //Fin
        
        JSONObject res = new JSONObject();
        res.put("planID", nuevo.getPlaEstId());
        
        JSONArray aPlanNiveles = new JSONArray();
        for(PlanNivel pn : nuevo.getNiveles() )
            aPlanNiveles.put(pn.getPlaNivId());
        
        res.put("IDs", aPlanNiveles);
        
        return WebResponse.crearWebResponseExito("El registro el Plan de Estudios correctamente",res);
    }    
    
    
}

