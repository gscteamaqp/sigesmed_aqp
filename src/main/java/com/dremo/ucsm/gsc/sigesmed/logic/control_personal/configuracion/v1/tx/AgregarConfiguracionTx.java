/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.configuracion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.UsuarioSessionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.LibroAsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Rol;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.ConfiguracionControl;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author carlos
 */
public class AgregarConfiguracionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        Integer organizacoinId;
        Organizacion _user =null;
        LibroAsistenciaDao libroAsistenciaDao = (LibroAsistenciaDao)FactoryDao.buildDao("cpe.LibroAsistenciaDao");
        UsuarioSession session=null;
        ConfiguracionControl config=null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            organizacoinId = requestData.getInt("org");  
            
            String descripcion=requestData.getString("descripcion");
            String responsableDNI=requestData.getString("responsable");
            Trabajador worker=libroAsistenciaDao.buscarTrabajadorPorDNI(responsableDNI, organizacoinId);
            
            String fechaInicio = requestData.getString("fechaInicio");
            String fechaFin = requestData.getString("fechaFin");
            
            DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            
            Date fecIn = fmt.parse(fechaInicio.substring(0,10));
            Date fecFin = fmt.parse(fechaFin.substring(0,10));
            
            JSONObject estado=(JSONObject)requestData.get("estado");
            String estado_=estado.getInt("id")+"";
            
            Integer hora=requestData.getInt("horaTol");
            Integer min=requestData.getInt("minTol");
            
           
            String nroDoc=requestData.getString("documento");
            
            _user = new Organizacion(organizacoinId);
            config=new ConfiguracionControl(hora,min, nroDoc, descripcion, estado_,organizacoinId , fecIn, fecFin, worker);
            session = new UsuarioSession(0, _user, new Rol(Sigesmed.ROL_RESPONSABLE_CONTROL_PERSONAL), new Usuario(worker.getPersona().getPerId()), new Date(), new Date(), wr.getIdUsuario(), 'A');
            
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo verificar los datos de Configuracion", e.getMessage() );
        }
        
        UsuarioSessionDao usuarioSessionDao = (UsuarioSessionDao)FactoryDao.buildDao("UsuarioSessionDao");
        try{
            //asignar rol de respCPE
            usuarioSessionDao.insert(session);
            libroAsistenciaDao.insert(config);
           
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Registrar la Configuracion del Libro de Caja", e.getMessage() );
        }
    
        return WebResponse.crearWebResponseExito("El Registro de Configuracion de Libro Caja se Realizo Correctamente");        
        //Fin
    }
    
}

