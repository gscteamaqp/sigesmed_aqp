/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.LibroCaja;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Administrador
 * @param <T>
 */
public abstract class GenericDaoHibernate<T> implements GenericDao<T> {
    @Override
    public void insert(T dato){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            session.persist(dato);
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo registrar : " + dato.getClass().getName() + "\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    @Override
    public void update(T dato){        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            session.update(dato);
            miTx.commit();        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo actualizar : " + dato.getClass().getName() + "\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    
    //metodo que cambia el estado de registro a eliminado (E)
    //precaucion: solo utilizar este metodo siempre y cuando el dato tenga estado de registro (estReg)
    @Override
    public void delete(T dato){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            //eliminando tipo tramite
            String hql = "UPDATE "+dato.getClass().getName()+" o SET o.estReg='E' WHERE o =:p1";            
            Query query = session.createQuery(hql);
            query.setParameter("p1", dato);
            query.executeUpdate();    
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo eliminar : " + dato.getClass().getName() + "\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    //metodo que elimina los datos de la Base de Datos
    //precaucion: solo utilizar este metodo en caso de se requiera quitar definitibamente los datos de BD (analizar su uso)
    @Override
    public void deleteAbsolute(T dato){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            session.delete(dato);
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo eliminar definitivamente : " + dato.getClass().getName() + "\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    //precaucion: solo utilizar este metodo siempre y cuando el dato tenga estado de registro (estReg)
    @Override
    public List<T> buscarTodos(Class dato ){
        List<T> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar
            //System.out.println("alertaaaaaaaaaaaaaaaaaaaaaaaaaa---->>>>>>"+dato.getName());
            String hql = "SELECT o FROM "+ dato.getName()+" o WHERE o.estReg!='E'";
            Query query = session.createQuery(hql);
            objetos = query.list();
           // System.out.println("cuentaaaass::"+objetos);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar : " + dato.getClass().getName() + "\n"+e.getMessage());
            throw e;            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    //precaucion: solo utilizar este metodo siempre y cuando el dato tenga estado de registro (estReg)
    @Override
    public List<T> buscarTodosOrdenados(Class dato ,String campos){
        List<T> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar
            String hql = "SELECT o FROM "+ dato.getName()+" o WHERE o.estReg!='E' "+campos;
            Query query = session.createQuery(hql);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar : " + dato.getClass().getName() + "\n"+e.getMessage());
            throw e;            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    
    //Lista de forma general
    @Override
    public List<T> listar(Class dato){
        List<T> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar
            String hql = "SELECT o FROM "+ dato.getName()+" o ";
            Query query = session.createQuery(hql);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar : " + dato.getClass().getName() + "\n"+e.getMessage());
            throw e;            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    
    @Override
    public void merge(T dato){        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            session.merge(dato);
            miTx.commit();        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo actualizar : " + dato.getClass().getName() + "\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }       
    @Override
    public T load(Class c,int key){        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        
        T dato = null;
        
        try{
            dato = (T)session.get(c, key);
            miTx.commit();        
        }catch(Exception e){ 
            miTx.rollback();
            System.out.println("No se pudo obtener : " + dato.getClass().getName() + "\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
        return dato;
    }
    
    @Override 
    public Object llave(Class dato,String campo){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Object obj= null;
        try{
//            String hql = "SELECT (MAX(o."+campo+"),0) FROM "+ dato.getName()+" o ";
            String hql = "SELECT COALESCE(MAX("+campo+"),0) FROM "+ dato.getName();
            Query query = session.createQuery(hql);                    
            query.setMaxResults(1);
            obj = query.uniqueResult(); 
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar : " + dato.getClass().getName() + "\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
        return obj;
    }
    
}
