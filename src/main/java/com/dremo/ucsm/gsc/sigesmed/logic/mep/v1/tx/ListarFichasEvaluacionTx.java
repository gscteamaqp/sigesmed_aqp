/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.mep.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mep.FichaEvaPerslDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.FichaEvaluacionPersonal;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author geank
 */
public class ListarFichasEvaluacionTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ListarFichasEvaluacionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        String orden  = wr.getMetadataValue("order");
        if(orden.equals("fecha")){
            return listarFichasEvaluacion();
        }
        
        return null;
    }
    private WebResponse listarFichasEvaluacion(){
        WebResponse response = new WebResponse();
        response.setScope("web");
        response.setResponseSta(true);
        try{
            FichaEvaPerslDaoHibernate fevd = new FichaEvaPerslDaoHibernate();
            List<FichaEvaluacionPersonal> fichas = fevd.getFichas();
            JSONArray aData = new JSONArray();
            for(FichaEvaluacionPersonal fep : fichas){
                JSONObject ficha = new JSONObject(EntityUtil.objectToJSONString(
                        new String[]{"ficEvaPerId", "nom", "des", "fecCre", "vigencia"},
                        new String[]{"id", "nom", "des", "fcre", "vig"},
                        fep
                ));
                ficha.put("tip",fep.getTipTra().getCarNom()).put("tipcod",fep.getTipTra().getTraCarId());
                aData.put(ficha);
            }
            response.setResponse(MEP.SUCC_RESPONSE_COD);
            response.setResponseMsg(MEP.SUCC_RESPONSE_MESS_LIST);
            response.setData(aData);
        }catch(Exception e){
            logger.log(Level.SEVERE,logger.getName()+":listarFichasEvaluacion()",e);
            response.setResponse(MEP.ERR_RESPONSE_COD);
            response.setResponseMsg(MEP.ERR_RESPONSE_MESS_LIST);
            
        }
        return response;
    }
}
