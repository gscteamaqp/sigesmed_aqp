/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.TareaEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaTareaModel;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 *
 * @author abel
 */
public class ListarAlumnosQueCumplieronTareaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int tareaID = 0;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            
            tareaID = requestData.getInt("tareaID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listar los alumnos que cumplieron tarea", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<BandejaTareaModel> tareas = null;
        TareaEscolarDao tareaDao = (TareaEscolarDao)FactoryDao.buildDao("web.TareaEscolarDao");
        try{
            tareas = tareaDao.buscarAlumnosQueCumplieronTarea(tareaID);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los alumnos que cumplieron tarea\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los alumnos que cumplieron tarea", e.getMessage() );
        }
        //Fin        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        int i = 0;
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        for(BandejaTareaModel tarea:tareas ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("bandejaTareaID",tarea.banTarId );
            oResponse.put("fechaEntrega",sf.format(tarea.fecEnt));
            oResponse.put("fechaVisto",tarea.fecVis);
            
            oResponse.put("estado",""+tarea.estado);
            oResponse.put("nota",tarea.nota);
            oResponse.put("not_lit",tarea.not_lit);
            oResponse.put("alu_id",tarea.per_id);
            oResponse.put("nombres",tarea.nombres);
            oResponse.put("apellidos",tarea.apellido1+ " "+tarea.apellido2);
            oResponse.put("tip_per",tarea.tip_per);
            oResponse.put("id_per",tarea.id_per);
            
            
            
            oResponse.put("i",i++);
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se listaron los alumnos que cumplieron con tarea correctamente",miArray);        
        //Fin
    }
    
}

