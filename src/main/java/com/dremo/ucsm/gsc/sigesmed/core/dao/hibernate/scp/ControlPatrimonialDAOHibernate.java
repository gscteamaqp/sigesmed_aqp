/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scp;


import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.ClaseGenericaDAO;
import java.util.List;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.FamiliaGenericaDAO;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.ControlPatrimonial;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.ControlPatrimonialDAO;


/**
 *
 * @author Administrador
 */
public class ControlPatrimonialDAOHibernate extends GenericDaoHibernate<ControlPatrimonial> implements ControlPatrimonialDAO {

    @Override
    public List<ControlPatrimonial> listarPatrimonioUGEL(int orgID) {
        
        
         List<ControlPatrimonial> control_patrimonial = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            
            String hql = "SELECT DISTINCT cp FROM ControlPatrimonial cp JOIN FETCH cp.org WHERE cp.org.organizacionPadre.orgId=:p1 and cp.est_reg!='E'";
            Query query = session.createQuery(hql);
            query.setParameter("p1",orgID );
            control_patrimonial = query.list();
            
        }catch(Exception e){
             System.out.println("No se pudo Listar las Clases Genericas \\n "+ e.getMessage());
             throw new UnsupportedOperationException("No se pudo Listar las Clases Genericas \\n "+ e.getMessage());
        }
        finally{
            session.close();
        }
        return control_patrimonial;  

    //    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ControlPatrimonial> listarPatrimonio(int orgId) {
        
        List<ControlPatrimonial> control_patrimonial = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            
            String hql = "SELECT DISTINCT cp FROM ControlPatrimonial cp  WHERE cp.org_id=:p1 and cp.est_reg!='E'";
            Query query = session.createQuery(hql);
            query.setParameter("p1",orgId );
            control_patrimonial = query.list();
            
        }catch(Exception e){
             System.out.println("No se pudo Listar el Control Patrimonial \\n "+ e.getMessage());
             throw new UnsupportedOperationException("No se pudo Listar el Control Patrimonial \\n "+ e.getMessage());
        }
        finally{
            session.close();
        }
        return control_patrimonial;  

    }

    @Override
    public void eliminarControlPatrimonial(int con_pat_id) {
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            //eliminando tipo tramite
            String hql = "UPDATE  ControlPatrimonial cp SET cp.est_reg='E' WHERE cp.con_pat_id =:p1";            
            Query query = session.createQuery(hql);
            query.setParameter("p1", con_pat_id);
            query.executeUpdate();    
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo eliminar : Control Patrimonial " + "\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    
    
}
