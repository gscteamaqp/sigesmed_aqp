/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.catalogo_bienes.v1.tx;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.SerieDocumentalDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.CatalogoBienesDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.SerieDocumental;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.CatalogoBienes;


import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 *
 * @author Administrador
 */
public class EditarItemCatalogoTx  implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        try{
            CatalogoBienes cb = null;
            JSONObject requestData = (JSONObject)wr.getData();
            
            
            int cat_bie_id = requestData.getInt("cat_bie_id");
            int gru_gen_id = requestData.getInt("gru_gen_id");
            int cla_gen_id = requestData.getInt("cla_gen_id");
            int fam_gen_id = requestData.getInt("fam_gen_id");
            short uni_med_id =(short) requestData.getInt("uni_med_id");
            String cod  = requestData.getString("cod");
            
            String den_bie = requestData.getString("den_bie");
            Date fec_cre = new Date();
            String num_res = requestData.getString("num_res");
            boolean fla_sbn = false;
            Date fec_mod = new Date();
            int usu_mod = requestData.getInt("usu_mod");
            char est_reg = 'A';
            cb = new CatalogoBienes(cat_bie_id,cod,den_bie,fec_cre,num_res,uni_med_id,fla_sbn,fec_mod,usu_mod,est_reg,gru_gen_id,cla_gen_id,fam_gen_id);

            CatalogoBienesDAO cat_bie_dao = (CatalogoBienesDAO)FactoryDao.buildDao("scp.CatalogoBienesDAO");
            cat_bie_dao.update(cb);

            return WebResponse.crearWebResponseExito("Se actualizo correctamente el item del catálogo");
            
        }catch(Exception e){
            
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo editar el Item del Catalogo", e.getMessage() );             
        }
        
    }
    
}
