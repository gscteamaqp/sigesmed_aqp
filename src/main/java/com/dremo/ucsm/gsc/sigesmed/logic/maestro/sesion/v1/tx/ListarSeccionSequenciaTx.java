package com.dremo.ucsm.gsc.sigesmed.logic.maestro.sesion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.SesionAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.SeccionSequenciaDidactica;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;

import java.util.List;

/**
 * Created by Administrador on 05/12/2016.
 */
public class ListarSeccionSequenciaTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            SesionAprendizajeDao sesionDao = (SesionAprendizajeDao) FactoryDao.buildDao("maestro.plan.SesionAprendizajeDao");
            List<SeccionSequenciaDidactica> secciones = sesionDao.listarSeccionesSequenciaDidactica();
            return WebResponse.crearWebResponseExito("Se listo correctamente la data",new JSONArray(EntityUtil.listToJSONString(
                    new String[]{"secSeqDid", "nomSec"},
                    new String[]{"id","nom"},
                    secciones
            )));
        }catch (Exception e){
            return WebResponse.crearWebResponseError("Error al listar datos");
        }

    }
}
