package com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.EvaluacionesUnidadDidactica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.IndicadorAprendizaje;

import java.util.List;

/**
 * Created by Administrador on 03/01/2017.
 */
public interface EvaluacionesUnidadDidacticaDao extends GenericDao<EvaluacionesUnidadDidactica> {
    EvaluacionesUnidadDidactica buscarEvaluacionId(int id);
    List<EvaluacionesUnidadDidactica> listarEvaluacionesPorUnidad(int idUnidad);
    
}
