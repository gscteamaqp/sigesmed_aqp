/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Area;
import java.util.Date;
import java.util.List;

/**
 *
 * @author abel
 */
public interface AreaDao extends GenericDao<Area>{
    
    public String buscarUltimoCodigo();
    public List<Area> buscarConOrganizacionYAreaPadre();
    public List<Area> buscarPorOrganizacion(int organizacionID);
    public void cambiarResponsable(Integer resID, Date fecha, int usuID, int areID);
}
