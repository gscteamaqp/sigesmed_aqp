/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import java.util.List;

import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.Altas;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.AltasDetalle;

import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.AltasDAO;

/**
 *
 * @author Administrador
 */
public class ListarAltasTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {

        JSONArray miArray = new JSONArray();

        try {
            List<Altas> altas = null;
            JSONObject requestData = (JSONObject) wr.getData();
            int org_id = requestData.getInt("org_id");

            AltasDAO alt_dao = (AltasDAO) FactoryDao.buildDao("scp.AltasDAO");
            altas = alt_dao.listar_bienes_altas(org_id);

            for (Altas alt : altas) {
                JSONObject oResponse = new JSONObject();
                oResponse.put("cod_pat_bie", alt.getAldet().get(0).getBm().getCon_pat_id());
                oResponse.put("des_bie", alt.getAldet().get(0).getBm().getDes_bie());
                oResponse.put("cau_alt", alt.getCausal_alta().getDes());
                oResponse.put("estado", alt.getAldet().get(0).getBm().getEstado_bie());
                if ("B".equals(alt.getAldet().get(0).getBm().getEstado_bie())) {
                    oResponse.put("cond", 0);
                    oResponse.put("cond_nombre", "BUENO");
                }
                if ("M".equals(alt.getAldet().get(0).getBm().getEstado_bie())) {
                    oResponse.put("cond", 1);
                    oResponse.put("cond_nombre", "MALO");
                }
                if ("R".equals(alt.getAldet().get(0).getBm().getEstado_bie())) {
                    oResponse.put("cond", 2);
                    oResponse.put("cond_nombre", "REGULAR");
                }
                miArray.put(oResponse);
            }
        } catch (Exception e) {
            System.out.println("No se pudo Listar los Bienes de Altas\n" + e);
            return WebResponse.crearWebResponseError("No se pudo Listar los Bienes de Altas", e.getMessage());
        }
        return WebResponse.crearWebResponseExito("Se Listo Las Altas de Bienes Correctamente", miArray);

        //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
