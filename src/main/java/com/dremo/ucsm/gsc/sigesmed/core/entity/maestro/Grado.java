package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrador on 21/12/2016.
 */

public class Grado implements java.io.Serializable {

    private int niv_id;

    private String nivnom;

    private int gra_id;

    private String abr;

    private String nom;

    private String des;

    private Character sec_id;

    public Grado() {
    }

    public int getNivId() {
        return niv_id;
    }

    public void setNivId(int niv_id) {
        this.niv_id = niv_id;
    }


    public String getNivnom() {
        return nivnom;
    }

    public void setNivnom(String nivnom) {
        this.nivnom = nivnom;
    }

    public int getGraId() {
        return gra_id;
    }

    public void setGraId(int graId) {
        this.gra_id = graId;
    }

    public String getAbr() {
        return abr;
    }

    public void setAbr(String abr) {
        this.abr = abr;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public Character getSec() {
        return sec_id;
    }

    public void setSec(Character sec) {
        this.sec_id = sec_id;
    }
}
