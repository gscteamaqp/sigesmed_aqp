/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.rdg;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.TipoEspecializadoDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.TipoEspecializado;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author ucsm
 */
public class TipoEspecializadoDaoHibernate extends GenericDaoHibernate<TipoEspecializado> implements TipoEspecializadoDao{
    @Override
    public TipoEspecializado tipoEspById(int idTipEsp) {
        TipoEspecializado te = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Query query = session.getNamedQuery("TipoEspecializado.findByTesIde")
            .setInteger("tesIde", idTipEsp);
            query.setMaxResults(1);
            te = ((TipoEspecializado)query.uniqueResult());
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar el tipoEsepcializado \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar por nombre el tipoEspe \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return te;
    }

    @Override
    public List<TipoEspecializado> getAllTiposEsp() {
        List <TipoEspecializado> listTiposEsp;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
       
        try{
            Query query = session.getNamedQuery("TipoEspecializado.findAll");
            listTiposEsp = query.list();
            t.commit();
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar el tipoEspecializado \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar tiposEspecializados \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return listTiposEsp;
    }
    
    @Override
    public List<TipoEspecializado> getAllTiposEspActivos() {
        List <TipoEspecializado> listTiposEsp;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql="SELECT esp FROM TipoEspecializado esp WHERE esp.estReg='A'";
            Query query = session.createQuery(hql);            
            listTiposEsp = query.list();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar el tipoEspecializado \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar tiposEspecializados \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return listTiposEsp;
    }

    @Override
    public TipoEspecializado getByAlias(String alias) {
        TipoEspecializado te = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql="SELECT tip FROM TipoEspecializado tip WHERE tip.tesAli=:p1";
            Query q=session.createQuery(hql);
            q.setParameter("p1", alias);
            q.setMaxResults(1);
            te = ((TipoEspecializado)q.uniqueResult());
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar el tipoEsepcializado \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar por nombre el tipoEspe \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return te;
    }
    

}
