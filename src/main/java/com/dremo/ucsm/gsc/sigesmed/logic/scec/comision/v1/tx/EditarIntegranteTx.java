/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.scec.comision.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.CargoComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.IntegranteComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.IntegranteComision;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ucsm
 */
public class EditarIntegranteTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(EditarIntegranteTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            JSONObject js = (JSONObject)wr.getData();
            PersonaDao pd = (PersonaDao)FactoryDao.buildDao("PersonaDao");
            Persona persona = pd.buscarPorDNI(js.getString("dni"));
            persona.setEmail(js.optString("ema",persona.getEmail()));
            persona.setNum1(js.optString("fij",persona.getNum1()));
            persona.setNum2(js.optString("cel",persona.getNum2()));
            pd.update(persona);

            IntegranteComisionDao icd = (IntegranteComisionDao)FactoryDao.buildDao("scec.IntegranteComisionDao");
            CargoComisionDao ccd = (CargoComisionDao)FactoryDao.buildDao("scec.CargoComisionDao");
            IntegranteComision integrante = icd.buscarPorComision(persona.getPerId(),js.getInt("com"));
            integrante.setCargoComision(ccd.buscarPorId(js.getInt("car")));
            icd.update(integrante);
            return WebResponse.crearWebResponseExito("Registro correcto",WebResponse.OK_RESPONSE);
        }
        catch(Exception e){
            logger.log(Level.SEVERE,"editarIntegrante",e);
            return WebResponse.crearWebResponseError("Error de registro",WebResponse.BAD_RESPONSE);
        }
    }
    
}
