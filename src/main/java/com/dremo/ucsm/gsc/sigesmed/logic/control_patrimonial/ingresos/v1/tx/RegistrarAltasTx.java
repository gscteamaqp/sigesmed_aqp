/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.Altas;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.AltasDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.MovimientoIngresos;

import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.AltasDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.BajasDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.DetalleAltasDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.DetalleBajasDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.BienesMueblesDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.Altas;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.Bajas;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BajasDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BienesMuebles;

import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.text.SimpleDateFormat;

/**
 *
 * @author Administrador
 */
public class RegistrarAltasTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {

        JSONObject requestData = (JSONObject) wr.getData();

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Altas altas = null;
        Bajas baja = null;
        AltasDetalle alt_det = null;
        MovimientoIngresos mov_ing = null;
        BienesMuebles bm = null;
        try {

            String verificar = requestData.getString("verificar");

            int id_bien = requestData.getInt("id_bien");
            
            System.out.println("Ib bien: " + id_bien);
            int mov_ing_id = requestData.getInt("mov_ing_id");
            int cau_alt_id = requestData.getInt("causal_id");
            int usu_mod = requestData.getInt("usu_mod");
            int org_id = requestData.getInt("org_id");
            char est_reg = 'A';
            AltasDAO alt_dao = (AltasDAO) FactoryDao.buildDao("scp.AltasDAO");
            altas = new Altas(0, cau_alt_id, org_id, usu_mod, mov_ing_id, est_reg);
            BienesMueblesDAO bie_mue_dao = (BienesMueblesDAO) FactoryDao.buildDao("scp.BienesMueblesDAO");
            bm = new BienesMuebles();
            bm.setCod_bie(id_bien);
            bm.setVerificar("A");
            bie_mue_dao.update_verificar_bien(id_bien, "A");
            //  bie_mue_dao.update(bm);

            switch (verificar) {
                case "A":
                    return WebResponse.crearWebResponseExito("ERROR: El Bien Mueble ya ha sido dado de Alta");

                case "B":
                    BajasDetalle baj_det = null;
                    Bajas baj = null;
                    /*Eliminamos la Baja (Cabecera+Detalle)*/
                    baj_det = alt_dao.baja_alta(id_bien);
                    int id_baja = baj_det.getBajas_id();
                    baj = new Bajas();
                    baj.setBajas_id(id_baja);
                    DetalleBajasDAO det_baj_dao = (DetalleBajasDAO) FactoryDao.buildDao("scp.DetalleBajasDAO");
                    det_baj_dao.deleteAbsolute(baj_det);
                    BajasDAO baj_dao = (BajasDAO) FactoryDao.buildDao("scp.BajasDAO");
                    baj_dao.deleteAbsolute(baj);
            }
            alt_dao.insert(altas);
            alt_det = new AltasDetalle(0, id_bien, altas.getAltas_id());

            DetalleAltasDAO det_alt_dao = (DetalleAltasDAO) FactoryDao.buildDao("scp.DetalleAltasDAO");
            det_alt_dao.insert(alt_det);

        } catch (Exception e) {
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar la Alta del Bien, datos incorrectos", e.getMessage());
        }
        JSONObject oResponse = new JSONObject();
        oResponse.put("alta_id", altas.getAltas_id());
        return WebResponse.crearWebResponseExito("El registro de la Alta del Bien  se realizo correctamente", oResponse);

    }
}
