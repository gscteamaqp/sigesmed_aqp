/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseno_curricular.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.DisenoCurricularMECH;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.DisenoCurricularDao;

/**
 *
 * @author abel
 */
public class EliminarDisenoCurricularTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int disenoID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            disenoID = requestData.getInt("diseñoID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo eliminar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        DisenoCurricularDao disenoDao = (DisenoCurricularDao)FactoryDao.buildDao("mech.DisenoCurricularDao");
        try{
            disenoDao.delete(new DisenoCurricularMECH(disenoID));
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar el Diseno curricular\n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el Diseno curricular", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Diseno curricular se elimino correctamente");
        //Fin
    }
}
