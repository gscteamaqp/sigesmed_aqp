package com.dremo.ucsm.gsc.sigesmed.core.entity.cpe;

import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.*;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
@Entity(name="cpe.PlazaMagisterial")
@Table(name="plaza_magisterial" ,schema="institucional" )
public class PlazaMagisterial  implements java.io.Serializable {

    @Id
    @Column(name="pla_mag_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_plaza_magisterial", sequenceName="institucional.plaza_magisterial_pla_mag_id_seq" )
    @GeneratedValue(generator="secuencia_plaza_magisterial")
    private int plaMagId;
    
    @Column(name="jor_ped")
    private int jornadaPedagogica;
    @Column(name="jor_lab")
    private int jornadaLaboral;
    @Column(name="mod",length=32)
    private String modalidad;
    @Column(name="niv",length=32)
    private String nivel;
    @Column(name="car",length=64)
    private String caracteristica;
    @Column(name="tip")
    private char tipo;
    @Column(name="cod_nex",length=16)
    private String codigoNexus;
    @Column(name="tip_car",length=64)
    private String cargo;
    @Column(name="are_esp",length=64)
    private String especialidad;
    @Column(name="mot_vac",length=64)
    private String motivo;
    
    @Column(name="nat_pla_id")
    private char naturalezaId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="nat_pla_id",updatable = false,insertable = false)
    private NaturalezaPlaza naturaleza;
    
    @Column(name="con_pla_id")
    private char condicionId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="con_pla_id",updatable = false,insertable = false)
    private CondicionPlaza condicion;
    
    @Column(name="org_id")
    private int orgId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="org_id",updatable = false,insertable = false)
    private Organizacion organizacion;
    
    @Column(name="doc_id")
    private Integer docenteId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="doc_id",updatable = false,insertable = false)
    private Docente docente;
    
    @OneToMany(mappedBy="plazaMagisterial",cascade=CascadeType.PERSIST,fetch=FetchType.LAZY)
    private List<DistribucionHoraGrado> distribucionGrados;
    
    @OneToMany(mappedBy="plazaMagisterial",fetch=FetchType.LAZY)
    private List<HorarioDetalle> horarios;

    public PlazaMagisterial() {
    }
    public PlazaMagisterial(int plaMagId) {
        this.plaMagId = plaMagId;
    }
    public PlazaMagisterial(int plaMagId, String modalidad,String nivel, String caracteristica, char tipo, String codigoNexus,String cargo,int jornadaLaboral,int jornadaPedagogica, String especialidad, String motivo,char condicionId, char naturalezaId,int orgId, Integer docenteId) {
       this.plaMagId = plaMagId;
       this.modalidad = modalidad;
       this.nivel = nivel;
       this.caracteristica = caracteristica;
       this.tipo = tipo;
       this.codigoNexus = codigoNexus;
       this.cargo = cargo;
       this.jornadaPedagogica = jornadaPedagogica;
       this.jornadaLaboral = jornadaLaboral;
       this.especialidad = especialidad;
       this.motivo = motivo;
       this.condicionId = condicionId;
       this.naturalezaId = naturalezaId;
       this.orgId = orgId;
       this.docenteId = docenteId;
    }
   
     
    public int getPlaMagId() {
        return this.plaMagId;
    }    
    public void setPlaMagId(int plaMagId) {
        this.plaMagId = plaMagId;
    }
    
    public String getModalidad() {
        return this.modalidad;
    }
    public void setModalidad(String modalidad) {
        this.modalidad = modalidad;
    }
    public String getNivel() {
        return this.nivel;
    }
    public void setNivel(String nivel) {
        this.nivel = nivel;
    }
    public String getCaracteristica() {
        return this.caracteristica;
    }
    public void setCaracteristica(String caracteristica) {
        this.caracteristica = caracteristica;
    }
    public char getTipo() {
        return this.tipo;
    }
    public void setTipo(char tipo) {
        this.tipo = tipo;
    }
    public String getCodigoNexus() {
        return this.codigoNexus;
    }
    public void setCodigoNexus(String codigoNexus) {
        this.codigoNexus = codigoNexus;
    }
    public String getCargo() {
        return this.cargo;
    }
    public void setCargo(String cargo) {
        this.cargo = cargo;
    }
    public int getJornadaPedagogica() {
        return this.jornadaPedagogica;
    }
    public void setJornadaPedagogica(int jornadaPedagogica) {
        this.jornadaPedagogica = jornadaPedagogica;
    }
    public int getJornadaLaboral() {
        return this.jornadaLaboral;
    }
    public void setJornadaLaboral(int jornadaLaboral) {
        this.jornadaLaboral = jornadaLaboral;
    }
    public String getEspecialidad() {
        return this.especialidad;
    }
    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }
    public String getMotivo() {
        return this.motivo;
    }
    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }
    
    public char getCondicionId() {
        return this.condicionId;
    }
    public void setCondicionId(char condicionId) {
        this.condicionId = condicionId;
    }
    public CondicionPlaza getCondicion() {
        return this.condicion;
    }
    public void setCondicion(CondicionPlaza condicion) {
        this.condicion = condicion;
    }
    
    public char getNaturalezaId() {
        return this.naturalezaId;
    }
    public void setNaturalezaId(char naturalezaId) {
        this.naturalezaId = naturalezaId;
    }
    public NaturalezaPlaza getNaturaleza() {
        return this.naturaleza;
    }
    public void setNaturaleza(NaturalezaPlaza naturaleza) {
        this.naturaleza = naturaleza;
    }
    
    public int getOrgId() {
        return this.orgId;
    }    
    public void setOrgId(int orgId) {
        this.orgId = orgId;
    }
    
    public Organizacion getOrganizacion() {
        return this.organizacion;
    }
    public void setOrganizacion(Organizacion organizacion) {
        this.organizacion = organizacion;
    }
    
    public Integer getDocenteId() {
        return this.docenteId;
    }
    public void setDocenteId(Integer docenteId) {
        this.docenteId = docenteId;
    }
    
    public Docente getDocente() {
        return this.docente;
    }
    public void setDocente(Docente docente) {
        this.docente = docente;
    }
    
    public List<DistribucionHoraGrado> getDistribucionGrados() {
        return this.distribucionGrados;
    }
    public void setNiveles(List<DistribucionHoraGrado> distribucionGrados) {
        this.distribucionGrados = distribucionGrados;
    }
    
    public List<HorarioDetalle> getHorarios() {
        return this.horarios;
    }
    public void setHorarios(List<HorarioDetalle> horarios) {
        this.horarios = horarios;
    }
}


