/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.scp;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.AnexoBienes;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BienesMuebles;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrador
 */
public interface BienesMueblesDAO extends GenericDao<BienesMuebles> {
    
    public List<BienesMuebles> listarBienesMuebles(int org_id);
    public List<BienesMuebles> listarPorFecha(Date fec_ini , Date fec_fin, int an_id,int org_id);
    public List<BienesMuebles> listarPorAtributos(Date fec_ini , Date fec_fin , int an_id , int org_id , int amb_id , int cod_pat , String est_bie);
    public BienesMuebles obtener_bien(int id_bien);
    public BienesMuebles obtenerBienCodigoBarras(String cod_ba_bie);
    public void update_verificar_bien(int id_bien , String ver);
    public void eliminar_bien(int bie_mue_id);
    public Long obtener_correlativo(int org_id);
    public void updateBienPorSalida(int bienId, int orgId, String ver);
}
