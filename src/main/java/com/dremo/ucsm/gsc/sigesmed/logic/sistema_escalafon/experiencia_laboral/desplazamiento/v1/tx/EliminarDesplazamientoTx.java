/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.desplazamiento.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DesplazamientoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Desplazamiento;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class EliminarDesplazamientoTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(EliminarDesplazamientoTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        Integer desId = 0;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            desId = requestData.getInt("desId");          
        
        }catch(Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"eliminarDesplazamiento",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar", e.getMessage() );
        }
        
        DesplazamientoDao desplazamientoDao = (DesplazamientoDao)FactoryDao.buildDao("se.DesplazamientoDao");
        try{
            desplazamientoDao.deleteAbsolute(new Desplazamiento(desId));
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar el desplazamiento\n" + e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el desplazamiento", e.getMessage() );
        }
        return WebResponse.crearWebResponseExito("El desplazamienti se elimino correctamente");
    }
    
}
