/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.sci;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.Asiento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.ClienteProveedor;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentaContable;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentaCorriente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentasEfectivo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.Empresa;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.HechosLibro;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.LibroCaja;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.RegistroCompras;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.RegistroVentas;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.ResultadoMensual;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.ResultadosMensualPorCuenta;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.Tesorero;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.TipoPago;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.cantidadModel;

import java.util.Date;
import java.util.List;

/**
 *
 * @author ucsm
 */
public interface LibroCajaDao extends GenericDao<LibroCaja>{   
    public LibroCaja estadoLibroCaja(int organizacionID);
    public List<CuentasEfectivo> buscarCuentasEfectivo(LibroCaja libro);
    public List<CuentaCorriente> buscarCuentasCorrientes(LibroCaja libro);
    public List<HechosLibro> buscarHechosLibroCaja(LibroCaja libro);
    public List<ResultadoMensual> resultadosHechosLibroCajaPorFechas(LibroCaja libro,Date desde, Date hasta);
    public List<Tesorero> buscarTesoreros(LibroCaja libro);
    public boolean verificarTesorero(LibroCaja libro, Persona persona);
    public LibroCaja estadoLibroAnterior(int organizacionID);
    public void saveUpdate(LibroCaja dato);
    public List<Trabajador> listarTrabajadoresPorOrganizacion(int organizacionID);
    public void insertarTesorero (Tesorero dato);
    public List<LibroCaja> listarLibroCajaConTesoreros(int organizacionID);
    public List<LibroCaja> listarLibrosCajaPorUGEL(int organizacionID, int fecha);
    public List<ClienteProveedor> listarClienteProveedor();
    public List<TipoPago> listarTipoPago();
    public List<Empresa> listarBancos();
    public List<CuentaContable> listarCuentasEfectivo();

    public void insertarCompra(RegistroCompras compra);
    public void insertarVenta(RegistroVentas venta);
    public void insertarAsiento(Asiento asiento);
    public void insertarHechos(HechosLibro hecho);
    
    public void actualizarCompra(int idCompra,RegistroCompras compra);
    public void actualizarVenta(int idVenta,RegistroVentas venta);
    public void actualizarAsientoC(int idAsiento,Asiento asiento);
    public void actualizarAsientoV(int idAsiento,Asiento asiento);

    public List<List<Object>> listarAsientosConCompraVenta(Date d,LibroCaja libro);
    public List<List<Object>> listarAsientosPorFechas(LibroCaja libro,Date desde,Date hasta);
    public ResultadoMensual resultadoAsientosPorFecha(LibroCaja libro,Date desde, Date hasta);
    public List<ResultadosMensualPorCuenta> resultadosCuentasPorFecha(LibroCaja libro,Date desde, Date hasta,boolean flagIva,boolean flagNat);
    public List<ResultadosMensualPorCuenta> resultadosCuentasPorFechaS(LibroCaja libro,String desde, String hasta,boolean flagIva,boolean flagNat);
    public List<ResultadoMensual> resultadosMensuales(LibroCaja libro,Date desde, Date hasta,boolean flagIva,boolean flagNat);
    public List<ResultadoMensual> resultadosMensualesS(LibroCaja libro,String desde, String hasta,boolean flagIva,boolean flagNat);

    //reportes
    public List<cantidadModel> cantidadGatosEnAreaPorOrganizacionYFecha(int organizacion,Date desde, Date hasta);
    public List<cantidadModel> importeTotalCuentaContablePorOrganizacionYFecha(int organizacion,Date desde, Date hasta);
    public List<cantidadModel> EgresosTotalDeInstitucionesPorFecha(Date desde, Date hasta);
    public List<cantidadModel> IngresosTotalDeInstitucionesPorFecha(Date desde, Date hasta);
    public List<cantidadModel> IngresosTotalDeInstitucionesPorMes(int organizacion,Date desde, Date hasta);
    public List<cantidadModel> EgresosTotalDeInstitucionesPorMes(int organizacion,Date desde, Date hasta);
    public List<cantidadModel> ControlesCajaPorFechaCierre(Date desde, Date hasta);
    public List<HechosLibro> HistorialCajaDeInstitucionesPorFecha(int organizacion,Date desde, Date hasta);
    public void EliminarDetalleCuenta(int idCuentaDetalle);
}

