package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.maestro.plan;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.SesionAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.*;
import org.hibernate.*;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 29/11/2016.
 */
public class SesionAprendizajeDaoHibernate extends GenericDaoHibernate<SesionAprendizaje> implements SesionAprendizajeDao {

    @Override
    public List<SesionAprendizaje> listarSesionesUnidad(int idUnidad) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(SesionAprendizaje.class)
                    .add(Restrictions.eq("estReg", 'A'))
                    .addOrder(Order.asc("numSes"))
                    .createCriteria("unidadDidactica").add(Restrictions.eq("uniDidId", idUnidad));
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<SesionAprendizaje> listarSesionesConDatos(int idUnidad) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(SesionAprendizaje.class)
                    .add(Restrictions.eq("estReg", 'A'))
                    .addOrder(Order.asc("numSes"))
                    .createCriteria("unidadDidactica").add(Restrictions.eq("uniDidId", idUnidad));
            List<SesionAprendizaje> sesiones = query.list();
            for(SesionAprendizaje sesion : sesiones){
                sesion.getTareas().size();
                sesion.getSequenciasDidacticas().size();
                List<IndicadoresSesionAprendizaje> indicadoresSesion =sesion.getIndicadoresSesion();
                for(IndicadoresSesionAprendizaje indSes : indicadoresSesion){
                    indSes.getCapacidad().getCapId();
                    indSes.getCompetencia().getComId();
                }
            }
            return sesiones;
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public SesionAprendizaje buscarSesionById(int idSesion) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria criteria = session.createCriteria(SesionAprendizaje.class)
                    .createAlias("unidadDidactica","unidad",JoinType.INNER_JOIN)
                    .add(Restrictions.eq("sesAprId",idSesion));

            return (SesionAprendizaje)criteria.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public IndicadoresSesionAprendizaje registrarIndicadorSesion(IndicadoresSesionAprendizaje indicador) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            session.persist(indicador);
            tx.commit();
            return indicador;
        }catch (Exception e){
            tx.rollback();
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<IndicadorAprendizaje> listarIndicadoresSesion(int idSession) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(IndicadorAprendizaje.class)
                    .createCriteria("indicadoresSesion",JoinType.INNER_JOIN)
                    .createCriteria("sesion",JoinType.INNER_JOIN)
                    .add(Restrictions.eq("sesAprId",idSession));
            return  query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<IndicadorAprendizaje> listarIndicadoresSeleccionados(int sesId, int capId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(IndicadoresSesionAprendizaje.class)
                    .add(Restrictions.eq("estReg", 'A'));
            query.createCriteria("sesion").add(Restrictions.eq("sesAprId",sesId));
            query.createCriteria("capacidad").add(Restrictions.eq("capId",capId));
            query.setProjection(Projections.projectionList()
                    .add(Projections.property("indicador"),"indicador"));
            //query.setResultTransformer(Transformers.aliasToBean(IndicadorAprendizaje.class));
            return  query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<SeccionSequenciaDidactica> listarSeccionesSequenciaDidactica() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(SeccionSequenciaDidactica.class)
                    .add(Restrictions.eq("estReg", 'A'))
                    .addOrder(Order.asc("secSeqDid"));
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<SesionSequenciaDidactica> listarSecuenciasDidacticas(int idSesion) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(SesionSequenciaDidactica.class)
                    .add(Restrictions.eq("estReg",'A'))
                    .createCriteria("sesion").add(Restrictions.eq("sesAprId",idSesion))
                    .setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public SeccionSequenciaDidacticaSesion getSeccioneSequencia(int idSecuencia,int idSeccion) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(SeccionSequenciaDidacticaSesion.class);
            query.createCriteria("sequenciaDidactica").add(Restrictions.eq("sesSeqDidId", idSecuencia));
            query.createCriteria("seccionSequencia").add(Restrictions.eq("secSeqDid", idSeccion));
            query.setMaxResults(1);

            return (SeccionSequenciaDidacticaSesion)query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public SeccionSequenciaDidactica getSeccionSequenciaDidactica(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(SeccionSequenciaDidactica.class)
                    .add(Restrictions.eq("secSeqDid",id));
            query.setMaxResults(1);
            return (SeccionSequenciaDidactica) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public SesionSequenciaDidactica registrarSequenciaDidactica(SesionSequenciaDidactica seq) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            session.persist(seq);
            tx.commit();
            return seq;
        }catch (Exception e){
            tx.rollback();
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public SeccionSequenciaDidacticaSesion registrarSeccionDeSequencia(SeccionSequenciaDidacticaSesion sesSeq) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try {
            session.persist(sesSeq);
            tx.commit();
            return sesSeq;
        }catch (Exception e){
            tx.rollback();
            throw  e;
        }finally {
            session.close();
        }
    }

    @Override
    public SesionSequenciaDidactica buscarSequenciaDidactica(int idSec) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            SesionSequenciaDidactica secuencia = (SesionSequenciaDidactica)session.get(SesionSequenciaDidactica.class,idSec);
            return secuencia;
        }catch (Exception e){
            throw  e;
        }finally {
            session.close();
        }
    }

    @Override
    public void registrarActividadSequencia(ActividadSeccionSequenciaDidactica actividad) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            session.persist(actividad);
            tx.commit();
        }catch (Exception e){
            tx.rollback();
            throw  e;
        }finally {
            session.close();
        }
    }

    @Override
    public void eliminarIndicadoresSesionTodos(int idSes) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            String hql = "DELETE FROM IndicadoresSesionAprendizaje WHERE sesion.sesAprId =:idSes";
            Query query = session.createQuery(hql);
            query.setInteger("idSes",idSes);
            query.executeUpdate();
            tx.commit();
        }catch (Exception e){
            tx.rollback();
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public void eliminarSecuenciaDidactica(SesionSequenciaDidactica secuencia) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            session.delete(secuencia);
            tx.commit();
        }catch (Exception e){
            tx.rollback();
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<IndicadoresSesionAprendizaje> buscarIndicadoresUnidadAprendizaje(int idUnidad, int idComp, int idCap, int idInd) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT ind FROM IndicadoresSesionAprendizaje  ind WHERE ind.id.unididId =:uni AND ind.id.comId =:com AND ind.id.capId =:cap AND ind.id.indAprId =:ind";
            Query query = session.createQuery(hql);
            query.setInteger("uni",idUnidad);
            query.setInteger("com",idComp);
            query.setInteger("cap",idCap);
            query.setInteger("ind",idInd);
            return query.list();
        }catch (Exception e){
            throw e;
        }
    }
    @Override
    public List<TareaSesionAprendizaje> listarTareasSesion(int idSesion) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria criteria = session.createCriteria(TareaSesionAprendizaje.class)
                    .add(Restrictions.eq("estReg",'A'))
                    .createCriteria("sesion").add(Restrictions.eq("sesAprId",idSesion));
            return criteria.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public TareaSesionAprendizaje buscarTareaSesion(int idTarea) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(TareaSesionAprendizaje.class)
                    .add(Restrictions.eq("tarIdSes", idTarea))
                    .setMaxResults(1);
            return (TareaSesionAprendizaje) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public void registrarTareaSesion(TareaSesionAprendizaje tarea) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            session.persist(tarea);
            tx.commit();
        }catch (Exception e){
            tx.rollback();
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public void eliminarTareaSesion(int idTarea) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            String hql = "DELETE FROM TareaSesionAprendizaje WHERE id =:idTarea";
            Query query = session.createQuery(hql);
            query.setInteger("idTarea",idTarea);
            query.executeUpdate();
            tx.commit();
        }catch (Exception e){
            tx.rollback();
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public void editarTareaSesion(TareaSesionAprendizaje tarea) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            session.update(tarea);
            tx.commit();
        }catch (Exception e){
            tx.rollback();
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public SesionAprendizaje buscarSesionSiguiente(int idSesion, int idUnidad) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria criteria = session.createCriteria(SesionAprendizaje.class)
                    .add(Restrictions.eq("estReg", 'A'))
                    .addOrder(Order.asc("numSes"))
                    .createCriteria("unidadDidactica", "unidad", JoinType.INNER_JOIN)
                    .add(Restrictions.eq("uniDidId",idUnidad));

            List<SesionAprendizaje> sesiones = criteria.list();
            for(int i = 0; i < sesiones.size(); i++){
                if(i < sesiones.size()-1){
                    if(sesiones.get(i).getSesAprId() == idSesion){
                        return sesiones.get(i+1);
                    }
                }

            }
            return null;
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
}