/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mech;


import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.DistribucionHoraGradoDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.DistribucionHoraArea;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.DistribucionHoraGrado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlazaMagisterial;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author abel
 */
public class DistribucionHoraGradoDaoHibernate extends GenericDaoHibernate<DistribucionHoraGrado> implements DistribucionHoraGradoDao{

    @Override
    public void insertarPlazaMagisterial(PlazaMagisterial plaza){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            session.persist(plaza);
            miTx.commit();        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo insertar la plaza magisterial\\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo insertar la plaza magisterial\\n"+ e.getMessage());
        }
        finally{
            session.close();
        }
    }
    @Override
    public void actualizarPlazaMagisterial(PlazaMagisterial plaza){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            session.update(plaza);
            miTx.commit();        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo actulizar la plaza magisterial\\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo actualizar la plaza magisterial\\n"+ e.getMessage());
        }
        finally{
            session.close();
        }
    }
    
    @Override
    public List<PlazaMagisterial> buscarPlazasPorOrganizacion(int orgId){
        List<PlazaMagisterial> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT pm FROM PlazaMagisterial pm WHERE pm.orgId=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", orgId );
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las plazas magisteriales por organizacion\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las plazas magisteriales por organizacion\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public List<DistribucionHoraGrado> listarDistribucionHorasGradoPorPlanEstudios(int planId){
        List<DistribucionHoraGrado> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT DISTINCT dg FROM DistribucionHoraGrado dg LEFT JOIN FETCH dg.distribucionAreas WHERE dg.plaEstId=:p1 AND pm.orgId=:p2 ORDER BY dg.graId";
            Query query = session.createQuery(hql);
            query.setParameter("p1", planId );
            objetos = query.list();
            
        }catch(Exception e){
            System.out.println("No se pudo Listar la distribucion de las horas por plan de estudios\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar la distribucion de las horas por plan de estudios\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    
    @Override
    public List<DistribucionHoraGrado> buscarDistribucionHorasGradoPorPlanYDocente(int planId,int docenteId){
        List<DistribucionHoraGrado> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT DISTINCT dg FROM DistribucionHoraGrado dg LEFT JOIN FETCH dg.distribucionAreas WHERE dg.plaEstId=:p1 and dg.plazaMagisterial.docenteId=:p2 ORDER BY dg.graId";
            Query query = session.createQuery(hql);
            query.setParameter("p1", planId );
            query.setParameter("p2", docenteId );
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar la distribucion de las horas por plan y docente\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar la distribucion de las horas por plan docente\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public void insertarDistribucionGrados(List<DistribucionHoraGrado> disGrados){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            for(DistribucionHoraGrado dg: disGrados)
                session.persist(dg);
            miTx.commit();        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo realizar la persistencia en distribucion hora grado\\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo realizar la persistencia en distribucion hora grado\\n"+ e.getMessage());
        }
        finally{
            session.close();
        }
    }
    @Override
    public void insertarDistribucionAreas(List<DistribucionHoraArea> disAreas){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            for(DistribucionHoraArea da: disAreas)
                session.persist(da);
            miTx.commit();        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo realizar la persistencia en distribucion hora area\\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo realizar la persistencia en distribucion hora area\\n"+ e.getMessage());
        }
        finally{
            session.close();
        }
    }
    
    @Override
    public void insertarDistribucionGrado(DistribucionHoraGrado disGrado){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            if(disGrado.getDisHorGraId()==0)
                session.persist(disGrado);
            else
                session.update(disGrado);
            miTx.commit();        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo realizar la persistencia en distribucion hora grado\\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo realizar la persistencia en distribucion hora grado\\n"+ e.getMessage());
        }
        finally{
            session.close();
        }
    }
    @Override
    public void insertarDistribucionArea(DistribucionHoraArea disGrado){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            if(disGrado.getDisHorGraId()==0)
                session.persist(disGrado);
            else
                session.update(disGrado);
            miTx.commit();        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo realizar la persistencia en distribucion hora area\\n"+e.getMessage());
            throw new UnsupportedOperationException("No se pudo realizar la persistencia en distribucion hora area\\n"+ e.getMessage());
        }
        finally{
            session.close();
        }
    }
    
    @Override
    public void eliminarDistribucionPorPlanYPlaza(int planID,int plazaID) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            
            //eliminando la distribucion por areas
            String hql = "DELETE FROM DistribucionHoraArea WHERE disHorGraId IN (SELECT dg.disHorGraId FROM DistribucionHoraGrado dg WHERE dg.plaEstId =:p1 and dg.plaMagId =:p2)";
            Query query = session.createQuery(hql);
            query.setParameter("p1", planID);
            query.setParameter("p2", plazaID);
            query.executeUpdate();
            
            //eliminando la distribucion por grado
            hql = "DELETE FROM DistribucionHoraGrado WHERE plaEstId =:p1 and plaMagId =:p2";
            query = session.createQuery(hql);
            query.setParameter("p1", planID);
            query.setParameter("p2", plazaID);
            query.executeUpdate();
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo eliminar la distribucion de horas de la plaza \n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    
    @Override
    public void actualizarHorasPedagogicasPorPlaza(int plazaID,int hora){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try{
            String hql = "UPDATE PlazaMagisterial pm SET pm.jornadaPedagogica=:p2 WHERE pm.plaMagId =:p1";

            Query query = session.createQuery(hql);
            query.setParameter("p1", plazaID);
            query.setParameter("p2", hora);
            query.executeUpdate();
            miTx.commit();
        
        }catch(Exception e){
            miTx.rollback();
            System.out.println("No se pudo actualizar las horas pedagogicas de la plaza" + this.getClass().getName() + "\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
    }
    
    @Override
    public Docente buscarDocentePorDNI(String dni) {
        Docente objeto = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{            
            String hql = "SELECT d FROM mech.Docente d JOIN FETCH d.persona WHERE d.persona.dni =:dni ";
        
            Query query = session.createQuery(hql);
            query.setParameter("dni", dni);
            query.setMaxResults(1);
            //buscando 
            objeto =  (Docente)query.uniqueResult(); 
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar el Docente por DNI\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar el Docente por DNI\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objeto;
    }
    @Override
    public Docente buscarDocentePorCodigoModular(String codigo) {
        Docente objeto = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{            
            String hql = "SELECT d FROM mech.Docente d JOIN FETCH d.persona WHERE d.codMod =:codigo ";
        
            Query query = session.createQuery(hql);
            query.setParameter("codigo", codigo);
            query.setMaxResults(1);
            //buscando 
            objeto =  (Docente)query.uniqueResult(); 
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar el Docente por codigo modular\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar el Docente por codigo modular\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objeto;
    }
    @Override
    public Docente buscarDocentePorID(int docenteID) {
        Docente objeto = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{            
            String hql = "SELECT d FROM mech.Docente d JOIN FETCH d.persona WHERE d.docId =:docenteID ";
        
            Query query = session.createQuery(hql);
            query.setParameter("docenteID", docenteID);
            query.setMaxResults(1);
            //buscando 
            objeto =  (Docente)query.uniqueResult(); 
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar el Docente por docente id\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar el Docente por docente id\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objeto;
    }

    @Override
    public PlazaMagisterial buscarPlazaPorID(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        PlazaMagisterial plaza = null;
        try{
            String hql = "SELECT pm FROM PlazaMagisterial pm JOIN FETCH pm.condicion JOIN FETCH pm.naturaleza WHERE pm.plaMagId=:plazaID";
            Query query = session.createQuery(hql);
            query.setParameter("plazaID", id);
            plaza = (PlazaMagisterial)query.uniqueResult();
        
        }catch(Exception e){
            System.out.println("No se pudo obtener la plaza magisterial por id\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo obtener la plaza magisterial por id\\n "+ e.getMessage());            
        } finally{
            session.close();
        }
        return plaza;
    }

    @Override
    public List<DistribucionHoraGrado> listarDistribucionHorasGradoPorPlanEstudiosPlazas(int planID) {
        
        List<DistribucionHoraGrado> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT DISTINCT dg FROM DistribucionHoraGrado dg JOIN FETCH dg.plazaMagisterial pm JOIN FETCH pm.docente do JOIN FETCH do.persona   LEFT JOIN FETCH dg.distribucionAreas WHERE dg.plaEstId=:p1 ORDER BY dg.graId";
            Query query = session.createQuery(hql);
            query.setParameter("p1", planID );
            objetos = query.list();
            
        }catch(Exception e){
            System.out.println("No se pudo Listar la distribucion de las horas por plan de estudios\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar la distribucion de las horas por plan de estudios\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
        
        
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
