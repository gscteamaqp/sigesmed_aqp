package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrador on 12/12/2016.
 */
@Entity
@Table(name = "material_recurso_programacion", schema = "pedagogico")
public class MaterialRecursoProgramacion implements java.io.Serializable {
    @Id
    @Column(name = "mat_id")
    @SequenceGenerator(name = "material_recurso_programacion_mat_id_seq", sequenceName = "pedagogico.material_recurso_programacion_mat_id_seq")
    @GeneratedValue(generator = "material_recurso_programacion_mat_id_seq")
    private int matId;
    @Column(name = "nom")
    private String nom;
    @Column(name = "tip")
    private Character tip;
    @Column(name = "des")
    private String des;
    @Column(name = "can")
    private Integer can;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "uni_did_id")
    private UnidadDidactica unidadDidactica;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "est_reg",length = 1)
    private Character estReg;


    public MaterialRecursoProgramacion() {
    }

    public MaterialRecursoProgramacion(String nom, Character tip, String des, Integer can) {
        this.nom = nom;
        this.tip = tip;
        this.des = des;
        this.can = can;

        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public int getMatId() {
        return matId;
    }

    public void setMatId(int matId) {
        this.matId = matId;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Character getTip() {
        return tip;
    }

    public void setTip(Character tip) {
        this.tip = tip;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public Integer getCan() {
        return can;
    }

    public void setCan(Integer can) {
        this.can = can;
    }

    public UnidadDidactica getUnidadDidactica() {
        return unidadDidactica;
    }

    public void setUnidadDidactica(UnidadDidactica unidadDidactica) {
        this.unidadDidactica = unidadDidactica;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
}
