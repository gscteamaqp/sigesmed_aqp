/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.InventarioInicialDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.InventarioInicial;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.InventarioInicialDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.dremo.ucsm.gsc.sigesmed.util.GCell;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.border.SolidBorder;
import com.itextpdf.layout.element.Paragraph;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.text.SimpleDateFormat;
import java.text.SimpleDateFormat;

/**
 *
 * @author Administrador
 */
public class ReporteInventarioInicialTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        
        InventarioInicial invIni = null;
        List<InventarioInicialDetalle> invIniDet = null;
        
        try {
            
            JSONObject requestData = (JSONObject) wr.getData();
            int org_id = requestData.getInt("org_id");
            int inv_ini_id = requestData.getInt("inv_ini_id");
            
            //System.out.println("Organizacion " + org_id + " " + inv_ini_id);
            
            InventarioInicialDAO inv_ini_dao = (InventarioInicialDAO)FactoryDao.buildDao("scp.InventarioInicialDAO");
            invIni = inv_ini_dao.obtenerInventarioInicial(inv_ini_id, org_id);
            
            invIniDet = inv_ini_dao.obtenerDetalleInventarioInicial(inv_ini_id, org_id);
                        
            Organizacion org = null;
            OrganizacionDao org_dao = (OrganizacionDao) FactoryDao.buildDao("OrganizacionDao");
            org = org_dao.buscarConTipoOrganizacionYPadre(org_id);
            String nombre_ie = org.getNom();
            String ubic_ie = org.getDir();
            String org_pad = org.getOrganizacionPadre().getDes();
            
            String cab1[] = {
                "N°", "Cod. Patrimonial", "Descripción",
                "Modelo", "Dimensiones", "Color",
                "Fec Ingreso", "Doc Ingreso", "Estado",
                "Ubicación"
            };
            
            Mitext m = null;
            m = new Mitext(true, "REPORTE DE BIENES INMUEBLES DE LA INSTITUCION");
            m.newLine(2);       
            
            m.agregarParrafo("Ubicacion : " +ubic_ie);
            m.newLine(1);
            m.agregarParrafo("UGEL :"+ org_pad);
            m.newLine(1);
            
            m.agregarParrafo("INVENTARIO INICIAL: " + invIni.getFec_mod());
            m.newLine(1);
            // Creamos el Objeto Reporte
                        
            float columnWidths_1[] = {1, 2, 3, 2, 2, 2, 2, 3, 2, 3};
            GTabla tabla_1 = new GTabla(columnWidths_1);
            tabla_1.setWidthPercent(100);
            tabla_1.build(cab1);
                        
            int data_length = cab1.length;
            String[] archivos_data = new String[data_length];
            for (int i = 0; i < data_length; i++) {
                archivos_data[i] = " ";
            }
            
            GCell[] cell = {tabla_1.createCellCenter(1, 1), tabla_1.createCellCenter(1, 1), tabla_1.createCellCenter(1, 1), tabla_1.createCellCenter(1, 1), tabla_1.createCellCenter(1, 1), tabla_1.createCellCenter(1, 1), tabla_1.createCellCenter(1, 1), tabla_1.createCellCenter(1, 1), tabla_1.createCellCenter(1, 1), tabla_1.createCellCenter(1, 1)};
            
            int size = invIniDet.size();           
            
            for (int i = 0; i < size; i++) {
                InventarioInicialDetalle inventarioDet = invIniDet.get(i);
                
                String index = Integer.toString(i); 
                
                String estado = inventarioDet.getBien_inmueble().getEstado_bie();
                String estDes = "";
                
                if (estado == "B") {
                    estDes = "Bueno";
                }
                if (estado == "M") {
                    estDes = "Malo";
                }
                if (estado == "R") {
                    estDes = "Regular";
                }

                archivos_data[0] = index;
                archivos_data[1] = Integer.toString(invIni.getCon_pat_id());
                archivos_data[2] = inventarioDet.getBien_inmueble().getDes_bie();
                archivos_data[3] = inventarioDet.getBien_inmueble().getDtm().getMod();
                archivos_data[4] = Integer.toString(inventarioDet.getBien_inmueble().getDtm().getDim());
                archivos_data[5] = inventarioDet.getBien_inmueble().getDtm().getCol();
                archivos_data[6] = formatter.format(inventarioDet.getBien_inmueble().getFec_reg());
                archivos_data[7] = inventarioDet.getBien_inmueble().getRut_doc_bie();
                archivos_data[8] = estado;
                archivos_data[9] = inventarioDet.getBien_inmueble().getAmbiente().getDes();;
                
                tabla_1.processLineCell(archivos_data, cell);                
            }
            
            m.agregarTabla(tabla_1);
            
            JSONArray responseData = new JSONArray();

            m.cerrarDocumento();
            JSONObject oResponse = new JSONObject();
            oResponse.put("datareporte", m.encodeToBase64());
            responseData.put(oResponse);
            
            return WebResponse.crearWebResponseExito("Se genero el Reporte con exito ...", responseData);
            
        } catch (Exception e) {
            
            System.out.println(e);
            return WebResponse.crearWebResponseError("ERROR AL GENERAR REPORTE ", e.getMessage());
        }

    }

}
