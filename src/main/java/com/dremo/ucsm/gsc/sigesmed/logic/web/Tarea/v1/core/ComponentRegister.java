/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx.ActualizarTareaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx.CalificarTareaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx.EliminarTareaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx.EnviarTareaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx.FinalizarTareaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx.InsertarTareaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx.ListarAlumnosQueCumplieronTareaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx.ListarDocumentosDeTareaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx.ListarTareasPorAlumnoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx.ListarTareasPorDocenteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx.ListarTareasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx.ResolverTareaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx.ObtenerIndicadoresTx;
/**
 *
 * @author abel
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_WEB);        
        
        //Registrnado el Nombre del componente
        component.setName("tarea");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionPOST("insertarTarea", InsertarTareaTx.class);
        component.addTransactionPOST("enviarTarea", EnviarTareaTx.class);
        component.addTransactionPUT("actualizarTarea", ActualizarTareaTx.class);
        component.addTransactionDELETE("eliminarTarea", EliminarTareaTx.class);
        
        component.addTransactionPOST("resolverTarea", ResolverTareaTx.class);
        component.addTransactionPUT("calificarTarea", CalificarTareaTx.class);
        component.addTransactionPUT("finalizarTarea", FinalizarTareaTx.class);
        
        component.addTransactionGET("listarTareasPorDocente", ListarTareasPorDocenteTx.class);
        component.addTransactionGET("listarTareas", ListarTareasTx.class);
        component.addTransactionGET("listarAlumnosCumplieronTarea", ListarAlumnosQueCumplieronTareaTx.class);
        component.addTransactionGET("verDocumentosDeTarea", ListarDocumentosDeTareaTx.class);
        component.addTransactionGET("listarTareasPorAlumno", ListarTareasPorAlumnoTx.class);
        component.addTransactionGET("obtener_indicadores_tarea", ObtenerIndicadoresTx.class);
        return component;
    }
}
