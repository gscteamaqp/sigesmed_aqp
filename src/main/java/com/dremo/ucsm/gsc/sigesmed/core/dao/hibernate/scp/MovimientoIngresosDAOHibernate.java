/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scp;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import java.util.List;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.MovimientoIngresosDAO;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.MovimientoIngresos;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.TipoMovimientoIngresos;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.MovimientoIngresos;
/**
 *
 * @author Administrador
 */
public class MovimientoIngresosDAOHibernate extends GenericDaoHibernate<MovimientoIngresos> implements MovimientoIngresosDAO {

    @Override
    public List<MovimientoIngresos> listarMovimientos(int orgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<TipoMovimientoIngresos> listarTipoMovimiento() {
        
        
        List<TipoMovimientoIngresos> tmi = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT tmi FROM TipoMovimientoIngresos tmi  WHERE tmi.est_reg!='E'";
            
            Query query = session.createQuery(hql); 
            tmi = query.list();
            
        }
        catch(Exception e){
         System.out.println("No se pudo Listar los Tipo de Movimiento de Ingreso \\n "+ e.getMessage());
             throw new UnsupportedOperationException("No se pudo Listar los tipo de Movimiento de Ingreso \\n "+ e.getMessage());
        }
        finally{
            session.close();
        }
        return tmi;  
        
        
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public MovimientoIngresos obtener_movimiento(int mov_id) {
        
        MovimientoIngresos mi = null;
  
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            
            String hql = "SELECT mi FROM MovimientoIngresos mi JOIN FETCH mi.tipo_movimiento WHERE mi.mov_ing_id=:p1 AND mi.est_reg!='E'";
            Query query = session.createQuery(hql); 
            query.setParameter("p1",mov_id);
            mi = (MovimientoIngresos)(query.uniqueResult());
 
       }
        catch(Exception e){
            System.out.println("No se pudo Obtener el Movimiento de Ingresos \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Obtener el Movimiento de Ingresos \\n "+ e.getMessage());

        }
        finally{
            session.close();
        }
        return mi;  
        
        
        
        
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
