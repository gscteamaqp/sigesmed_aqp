package com.dremo.ucsm.gsc.sigesmed.logic.ma.notas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.RegistroAuxiliarDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.DocenteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.CompetenciaAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.IndicadorAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.RegistroAuxiliar;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.RegistroAuxiliarCompetencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 26/01/2017.
 */
public class RegistrarNotasRegistroAuxiliarTx implements ITransaction {
    private static Logger logger = Logger.getLogger(RegistrarNotasRegistroAuxiliarTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        System.out.println("registro auxiliar: " +  data);
        int idPeriodo = data.getInt("per");
        int idComp = data.getInt("comp");
        char tipNot = data.optString("tip", "N").charAt(0);
        int idUsr = data.getInt("usr");
        int idArea = data.getInt("are");
        JSONArray array = data.optJSONArray("estudiantes");
        if(array == null) return WebResponse.crearWebResponseError("Se tiene que seleccionar a los alumnos");
        return registrarNotas(idPeriodo,idComp,idUsr,idArea,tipNot,array);
    }

    private WebResponse registrarNotas(int idPeriodo,int idComp, int idUsr, int idArea ,char tipNot, JSONArray array) {
        try{
            RegistroAuxiliarDao regisDao = (RegistroAuxiliarDao) FactoryDao.buildDao("ma.RegistroAuxiliarDao");
            IndicadorAprendizajeDao indicadorDao = (IndicadorAprendizajeDao) FactoryDao.buildDao("maestro.plan.IndicadorAprendizajeDao");
            CompetenciaAprendizajeDao compDao = (CompetenciaAprendizajeDao) FactoryDao.buildDao("maestro.plan.CompetenciaAprendizajeDao");
            DocenteDao docDao = (DocenteDao) FactoryDao.buildDao("maestro.DocenteDao");
            EstudianteDao estDao = (EstudianteDao) FactoryDao.buildDao("maestro.EstudianteDao");
            
            String mensaje = "";
            for(int i = 0; i < array.length(); i++){
                JSONObject alumno = array.getJSONObject(i);
                JSONArray notas = alumno.getJSONArray("notas");
                int idMatGra = alumno.getInt("idgra");
                for(int j=0; j < notas.length(); j++){
                    JSONObject nota = notas.getJSONObject(j);
                    int idInd = nota.optInt("ind");
                    Docente docente = docDao.buscarDocentePorUsuario(idUsr);

                    String not = nota.optString("not", tipNot == 'L' ? "C":"0");
                    String not_ind_doc = nota.optString("notIndDoc", tipNot == 'L' ? "C":"0");
                    if(!regisDao.buscarNotaFinalIndicador(idInd, docente.getDoc_id(), idPeriodo, idArea, idMatGra)){
                        RegistroAuxiliar regAux = new RegistroAuxiliar(not);
                        regAux.setNotInd(not);
                        regAux.setNotIndDoc(not_ind_doc);
                        regAux.setIndicador(indicadorDao.buscarPorId(idInd));
                        regAux.setPeriodo(regisDao.buscarPeriodoPorId(idPeriodo));
                        regAux.setDocente(docente);
                        regAux.setArea(docDao.buscarAreaPorId(idArea));
                        regAux.setGradoEstudiante(estDao.buscarGradoIEEstudiante(idMatGra));
                        regisDao.insert(regAux);
                        mensaje = "Se registro correctamente";
                    }else {
                        RegistroAuxiliar regAux = regisDao.buscarNotaEstudiante(idInd, docente.getDoc_id(), idPeriodo, idArea, idMatGra);
                        regAux.setNotIndDoc(not_ind_doc);
                        regAux.setNotInd(not);
                        regAux.setFecMod(new Date());
                        regisDao.update(regAux);
                        mensaje = "Se actualizo correctamente";
                    }
                }
                if(!alumno.optString("notComp").isEmpty()){
                    RegistroAuxiliarCompetencia notComp = regisDao.buscarNotaCompetenciaEsp(idComp,idArea,idPeriodo,idMatGra);
                    if(notComp == null){
                        notComp = new RegistroAuxiliarCompetencia();
                        notComp.setAreaCurricular(docDao.buscarAreaPorId(idArea));
                        notComp.setComp(compDao.buscarCompetenciPorCodigo(idComp));
                        notComp.setGradoEst(estDao.buscarGradoIEEstudiante(idMatGra));
                        notComp.setPeriodoPlanEstudios(regisDao.buscarPeriodoPorId(idPeriodo));
                    }
                    notComp.setNota(alumno.getString("notComp"));
                    regisDao.registrarNotaCompetencia(notComp);
                }


            }
            
            
            return WebResponse.crearWebResponseExito(mensaje);
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrar nota",e);
            return WebResponse.crearWebResponseError("No se pudo registrar los datos");
        }
    }
    
   
    
    
    
    
}
