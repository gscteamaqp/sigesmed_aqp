/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.ItemFileDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.ItemFileDetalleDao;
//import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.PermisoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.TrabajadorCargoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.TrabajadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.ItemFile;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.ItemFileDetalle;
//import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.Permiso;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TrabajadorCargo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class RegistrarComparticionTx implements ITransaction {
    ItemFileDetalle iteFilDet = null;
    //Datos requeridos
//    Integer idePerm = 0;
    Integer ideDoc = 0;
    Integer ideUsu = 0;
    Integer ideUsuSes = 0;
    Integer ideCrg = 0;

    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject requestData = (JSONObject) wr.getData();
//            idePerm = requestData.getInt("idePerm");
            ideDoc = requestData.getInt("ideDoc");
            ideUsu = requestData.getInt("ideUsu");
            ideUsuSes = requestData.getInt("ideUsuSes");
            //ideCrg = requestData.getInt("ideCrg");
        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo leer datos para registrar comparticion");
        }
        try {
//            //Encontrar trabajador cargo
//            TrabajadorCargoDao traCrgDao = (TrabajadorCargoDao) FactoryDao.buildDao("rdg.TrabajadorCargoDao");
//            TrabajadorCargo cargo = traCrgDao.buscarByCod(ideCrg);
//
//            //Ahora encontramos al trabajador con estos dos objetos
//            
//            TrabajadorDao trabDao = (TrabajadorDao) FactoryDao.buildDao("rdg.rdg.TrabajadorDao");
//            Trabajador trab = null;
//            try{
//                trab = trabDao.buscarPorPersonayCargo(ideUsu.longValue(), cargo);
//            }catch(Exception e){
//                return WebResponse.crearWebResponseError("El usuario actual no existe como trabajador debera registrarlo para aplicar la comparticion");
//            }

            //Obtenemos el documento
            ItemFileDao filDao = (ItemFileDao) FactoryDao.buildDao("rdg.ItemFileDao");
            ItemFile iteFil = filDao.buscarPorID(ideDoc);

//            //Obtenemos el permiso
//            PermisoDao permDao = (PermisoDao) FactoryDao.buildDao("rdg.PermisoDao");
//            Permiso perm = permDao.buscarByCod(idePerm);

           

            ItemFileDetalleDao iteFilDao = (ItemFileDetalleDao) FactoryDao.buildDao("rdg.ItemFileDetalleDao");
            if(iteFilDao.verificarExisteComparticion(ideUsu,iteFil)){
                 iteFilDet = new ItemFileDetalle();
                iteFilDet.setEstReg("A");
//                iteFilDet.setIfdPrmIde(perm);
                iteFilDet.setIfdUsuSes(ideUsuSes);
                iteFilDet.setIfdUsu(ideUsu);
                
    //            if(trab.getTraId()!= null){
    //                iteFilDet.setIfdUsu(trab.getTraId());
    //            }else{
    //                //Debemos crear un nuevo trabajador para esto enviando a su correo
    //                //la notificacion respectiva
    //                return WebResponse.crearWebResponseError("No existe el trabajador con dicho cargo");
    //            }
                iteFilDet.setIfdIteIde(iteFil);
                iteFilDao.insert(iteFilDet);
            }else{
                return WebResponse.crearWebResponseError("El documento ya esta compartido con esa persona");
            }
            
            
            
            
        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo insertar la comparticion");
        }

        return WebResponse.crearWebResponseExito("Se registro correctamente la comparticion",iteFilDet.getIfdIde());
    }

}
