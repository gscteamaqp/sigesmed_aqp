/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.ma;
import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ArticuloEscolar;
import java.util.List;

/**
 *
 * @author ucsm
 */
public interface ArticuloEscolarDao extends GenericDao<ArticuloEscolar>{
    ArticuloEscolar buscarPorId(int id);
    ArticuloEscolar buscarArticuloPorNombre(String nombre);
    public List<ArticuloEscolar> buscarArtiPorNombre(String nombre);
}
