/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.cpe;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.ConfiguracionControl;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.Docente;
import java.util.Date;
import java.util.List;

/**
 *
 * @author ucsm
 */
public interface LibroAsistenciaDao extends GenericDao<ConfiguracionControl>{
   
   
    public List<ConfiguracionControl> listarConfiguracionesControl(Organizacion organizacion);
    public List<Trabajador> listarTrabajadores(Organizacion organizacion);
    public List<Trabajador> listarTrabajadoresUsuario(Organizacion organizacion);
    public Trabajador buscarTrabajadorPorDNI(String dni,Integer organizacion);
    public Trabajador buscarDocentePorDNI(String dni,Integer organizacion);
    public ConfiguracionControl verificarLibroAsistenciaAbiertoByFecha(Integer Organizacion,Date fecha);
    public UsuarioSession getSessionByConfiguracionId(Integer rol,Integer id);
    public List<ConfiguracionControl> listarConfiguraciones(Organizacion organizacion);
    public Date getMinFechaConfiguraciones(Organizacion org);
//    public List<ContenidoDocumento> listarContenidosDocumento(Documento documentoId);
   
}