/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.plan_estudios.v1.tx;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;

/**
 *
 * @author abel
 */
public class ReporteTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        FileJsonObject miGrafico = null;
        GTabla tablaOrg = null;
        String organizacion = "";
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            JSONArray tabla = requestData.optJSONArray("resumen");
            organizacion = requestData.optString("organizacion");
            
            if(tabla!=null && tabla.length() > 0){
                
                float[] cols = {0.5f,1f,1f,1f,1f,0.5f,1f,1f,0.5f,0.5f,1f,0.5f};
                tablaOrg = new GTabla(cols);

                String[] labels = {"N°","Nexus","Modalidad","Nivel","Cargo","Tipo","Especialidad","Caracteristica","Naturaleza","Condicion","Motivo","Jornada"};
                tablaOrg.build(labels);
                for(int i = 0; i < tabla.length();i++){
                    JSONObject bo =tabla.getJSONObject(i);
                    
                    String[] fila = new String [12];
                    fila[0] = i+1+"";
                    fila[1] = bo.getString("nexus");
                    fila[2] = bo.getString("modalidad");
                    fila[3] = bo.getString("nivel");
                    fila[4] = bo.getString("cargo");
                    fila[5] = bo.getString("tipo");
                    fila[6] = bo.getString("especialidad");
                    fila[7] = bo.getString("caracteristica");
                    fila[8] = bo.getString("naturalezaID");
                    fila[9] = bo.getString("condicionID");
                    fila[10] = bo.getString("motivo");
                    fila[11] = ""+bo.getInt("jornadaLaboral");
                    
                    tablaOrg.processLine(fila);
                }
                
            }
            else            
                miGrafico = new FileJsonObject( requestData  );
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo realizar el reporte, grafico iicorrecto", e.getMessage() );
        }
        //Fin        
        
        
        /*
        *  Repuesta Correcta
        */
        
        JSONObject response = new JSONObject();
        try {
            Mitext r = new Mitext(true);
            
            
            if(tablaOrg==null){
                r.agregarTitulo(miGrafico.getName());
                r.newLine(1);
                r.agregarImagen64(miGrafico.getData());
            }
            else{
                r.agregarTitulo("Plazas Magisteriales de IE : "+organizacion);
                r.newLine(1);
                r.agregarTabla(tablaOrg);
            }
            
            r.cerrarDocumento();
            response.append("reporte", r.encodeToBase64() );
        } catch (Exception ex) {
            Logger.getLogger(ReporteTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return WebResponse.crearWebResponseExito("el reporte se realizo",response);
        //Fin
    }
    
}

