/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_personales.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DireccionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FichaEscalafonariaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TrabajadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Direccion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Yemi
 */
public class ActualizarDatosPersonalesTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ActualizarDatosPersonalesTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            JSONObject data = (JSONObject)wr.getData();  
            System.out.println(data);
            return actualizarDatosPersonales(data);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar datos personales",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    
    private WebResponse actualizarDatosPersonales(JSONObject data) {
        try{
            JSONObject rPersona = data.getJSONObject("persona");
            JSONObject rTrabajador = data.getJSONObject("trabajador");
            JSONObject rFicha = data.getJSONObject("fichaEscalafonaria");
            JSONArray rDirecciones = data.getJSONArray("direcciones");
            
            DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
            PersonaDao personaDao = (PersonaDao) FactoryDao.buildDao("se.PersonaDao");
            TrabajadorDao trabajadorDao = (TrabajadorDao) FactoryDao.buildDao("se.TrabajadorDao");
            FichaEscalafonariaDao fichaDao = (FichaEscalafonariaDao)FactoryDao.buildDao("se.FichaEscalafonariaDao"); 
            DireccionDao direccionDao = (DireccionDao) FactoryDao.buildDao("se.DireccionDao");
            
            Persona persona = personaDao.buscarPersonaPorId(rPersona.getInt("perId"));
            persona.setApePat(rPersona.getString("apePat"));
            persona.setApeMat(rPersona.getString("apeMat"));
            persona.setNom(rPersona.getString("nom"));
            persona.setDni(rPersona.getString("dni"));
            persona.setFecNac(sdi.parse(rPersona.getString("fecNac").substring(0,10)));
            persona.setNum1(rPersona.getString("num1"));
            persona.setNum2(rPersona.getString("num2"));
            persona.setFij(rPersona.getString("fij"));
            persona.setEmail(rPersona.getString("email"));
            persona.setSex(rPersona.getString("sex").charAt(0));
            persona.setEstCiv(rPersona.getString("estCiv").charAt(0));
            persona.setDepNac(rPersona.getString("depNac"));
            persona.setProNac(rPersona.getString("proNac"));
            persona.setDisNac(rPersona.getString("disNac"));
            personaDao.update(persona);
            
            Trabajador trabajador = trabajadorDao.buscarPorId(rTrabajador.getInt("traId"));
            trabajador.setTraCon(rTrabajador.getString("traCon").charAt(0));
            trabajadorDao.update(trabajador);
            
            FichaEscalafonaria ficha = fichaDao.buscarPorId(rFicha.getInt("ficEscId"));
            ficha.setAutEss(rFicha.getString("autEss"));
            ficha.setSisPen(rFicha.getString("sisPen"));
            ficha.setNomAfp(rFicha.getString("nomAfp"));
            ficha.setFecIngAfp(sdi.parse(rFicha.getString("fecIngAfp").substring(0,10)));
            ficha.setPerDis(rFicha.getBoolean("perDis"));
            ficha.setRegCon(rFicha.getString("regCon"));
            ficha.setGruOcu(rFicha.getString("gruOcu").charAt(0));
            fichaDao.update(ficha);
            
            List<Direccion> direcciones = new ArrayList<Direccion>();
            for(int i = 0;i < rDirecciones.length();i++) {
                Direccion direccion = direccionDao.buscarPorId(rDirecciones.getJSONObject(i).getInt("dirId"));
                direccion.setTip(rDirecciones.getJSONObject(i).getString("tipDir").charAt(0));
                direccion.setNom(rDirecciones.getJSONObject(i).getString("nomDir"));
                direccion.setDep(rDirecciones.getJSONObject(i).getString("depDir"));
                direccion.setPro(rDirecciones.getJSONObject(i).getString("proDir"));
                direccion.setDis(rDirecciones.getJSONObject(i).getString("disDir"));
                direccion.setNomZon(rDirecciones.getJSONObject(i).getString("nomZon"));
                direccion.setDesRef(rDirecciones.getJSONObject(i).getString("desRef"));
                direccionDao.update(direccion);
                direcciones.add(direccion);
            }
            
            JSONObject oResponse = new JSONObject();  
            JSONObject personaRes = new JSONObject();
            JSONObject trabajadorRes = new JSONObject();
            JSONObject fichaRes  = new JSONObject();
            JSONArray direccionesRes  = new JSONArray();
            DateFormat sdo = new SimpleDateFormat("yyyy-MM-dd");
            
            personaRes.put("perId", persona.getPerId());
            personaRes.put("apePat", persona.getApePat());
            personaRes.put("apeMat", persona.getApeMat());
            personaRes.put("nom", persona.getNom());
            personaRes.put("dni", persona.getDni());
            personaRes.put("fecNac", sdo.format(persona.getFecNac()));
            personaRes.put("num1", persona.getNum1());
            personaRes.put("num2", persona.getNum2());
            personaRes.put("fij", persona.getFij());
            personaRes.put("email", persona.getEmail());
            personaRes.put("sex", persona.getSex());
            personaRes.put("estCiv", persona.getEstCiv());
            personaRes.put("depNac", persona.getDepNac());
            personaRes.put("proNac", persona.getProNac());
            personaRes.put("disNac", persona.getDisNac());

            trabajadorRes.put("traId", trabajador.getTraId());
            trabajadorRes.put("traCon", trabajador.getTraCon());

            fichaRes.put("ficEscId", ficha.getFicEscId());
            fichaRes.put("autEss", ficha.getAutEss());
            fichaRes.put("sisPen", ficha.getSisPen());
            fichaRes.put("nomAfp", ficha.getNomAfp());
            fichaRes.put("codCuspp", ficha.getCodCuspp());
            fichaRes.put("fecIngAfp", sdo.format(ficha.getFecIngAfp()));
            fichaRes.put("perDis", ficha.getPerDis());
            fichaRes.put("regCon", ficha.getRegCon());
            fichaRes.put("gruOcu", ficha.getGruOcu());

            oResponse.put("persona", personaRes);
            oResponse.put("trabajador", trabajadorRes);
            oResponse.put("ficha", fichaRes);
            
            for(Direccion dir:direcciones ){
                JSONObject aux = new JSONObject();
                aux.put("dirId", dir.getDirId());
                aux.put("tipDir", dir.getTip());
                aux.put("nomDir", dir.getNom());
                aux.put("depDir", dir.getDep());
                aux.put("proDir", dir.getPro());
                aux.put("disDir", dir.getDis());
                aux.put("nomZon", dir.getNomZon());
                aux.put("desRef", dir.getDesRef());
                direccionesRes.put(aux);
            }
            oResponse.put("direcciones", direccionesRes);
            System.out.println(oResponse);
            
            return WebResponse.crearWebResponseExito("Datos personales actualizados exitosamente", oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizarDatosPersonales",e);
            return WebResponse.crearWebResponseError("Error, los datos personales no fueron actualizados");
        }
    }
    
}
