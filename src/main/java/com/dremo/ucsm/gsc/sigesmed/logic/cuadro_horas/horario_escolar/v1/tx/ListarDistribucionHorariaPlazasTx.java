/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.horario_escolar.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.HorarioEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.DistribucionHoraGrado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.HorarioDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.HorarioDetalleModel;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;

/**
 *
 * @author abel
 */
public class ListarDistribucionHorariaPlazasTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int planID = 0;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            planID = requestData.getInt("planID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listar la distribucion horaria de plazas, datos incorrectos", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<HorarioDetalleModel> distribucion = null;
        
        try{
            HorarioEscolarDao horarioDao = (HorarioEscolarDao)FactoryDao.buildDao("mech.HorarioEscolarDao");
            
            //distribucion = horarioDao.buscarDocentesConHorario(planID);
            
            distribucion = horarioDao.buscarDistribucionPlazasEnHorario(planID);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar Listar la distribucion hoararia de plazas\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar la distribucion horaria de plazas", e.getMessage() );
        }
        //Fin        
        /*
        *  Repuesta Correcta
        */
        /*
        JSONArray miArray = new JSONArray();
        
        for(HorarioDetalleModel d:distribucion ){
            JSONObject oResponse = new JSONObject();
            
            oResponse.put("disGradoID",d.getDisHorGraId());
            oResponse.put("hora",d.getHorAsi());
            oResponse.put("gradoID",d.getGraId());
            oResponse.put("plazaID",d.getPlaMagId());
            
            oResponse.put("horasAsignadas",d.getHorarios().size());
            */
            JSONArray aDet = new JSONArray();
            for(HorarioDetalleModel dh : distribucion){
                /*JSONObject oDet = new JSONObject();
                oDet.put("horarioDetalleID", dh.getHorDetId() );
                oDet.put("horaInicio", dh.getHorIni());
                oDet.put("horaI", dh.getHorIni().getHours());
                oDet.put("minutoI", dh.getHorIni().getMinutes());
                oDet.put("horaFin", dh.getHorFin());
                oDet.put("horaF", dh.getHorFin().getHours());
                oDet.put("minutoF", dh.getHorFin().getMinutes());
                oDet.put("aulaID", dh.getAulId() );
                oDet.put("areaID", dh.getAreCurId() );
                oDet.put("diaID", ""+dh.getDisId() );
                oDet.put("plazaID", dh.getPlaMagId() );
                oDet.put("gradoID", dh.grado);
                oDet.put("seccionID", ""+dh.seccion);
                
                aDet.put(oDet);
                */
                buscarPorGradoYPlaza(aDet,dh);
            }
            //oResponse.put("detalle", aDet);
            
            //miArray.put(oResponse);
        //}
        
        return WebResponse.crearWebResponseExito("La distribucion horaria de las plazas se listo correctamente",aDet);        
        //Fin
    }
    
    public void buscarPorGradoYPlaza(JSONArray lista, HorarioDetalleModel hd){
        
        for( int i=0; i< lista.length();i++){
            JSONObject o = lista.getJSONObject(i);
            if(o.getInt("plazaID") == hd.plaMagId && o.getInt("gradoID") == hd.grado ){
                JSONArray a = o.getJSONArray("dias");
                o.put("horasAsignadas",o.getInt("horasAsignadas")+1);
                
                buscarPorDia(a,hd);
                return;
            }
        }
        JSONObject nuevo = new JSONObject();
        nuevo.put("plazaID", hd.plaMagId);
        nuevo.put("gradoID", hd.grado);
        nuevo.put("horasAsignadas", 1);

        JSONArray a = new JSONArray();        
        
        buscarPorDia(a,hd);
        nuevo.put("dias", a);
        
        lista.put(nuevo);
    }
    
    public void buscarPorDia(JSONArray lista, HorarioDetalleModel hd){
        
        for( int i=0; i< lista.length();i++){
            JSONObject o = lista.getJSONObject(i);
            if( o.getInt("diaID") == hd.getDisId() ){
                JSONArray a = o.getJSONArray("intervalos");
                
                JSONObject oDet = new JSONObject();
                oDet.put("horarioDetalleID", hd.getHorDetId() );
                oDet.put("horaInicio", hd.getHorIni());
                /*oDet.put("horaI", hd.getHorIni().getHours());
                oDet.put("minutoI", hd.getHorIni().getMinutes());*/
                oDet.put("horaFin", hd.getHorFin());
                /*oDet.put("horaF", hd.getHorFin().getHours());
                oDet.put("minutoF", hd.getHorFin().getMinutes());*/
                oDet.put("aulaID", hd.getAulId() );
                oDet.put("areaID", hd.getAreCurId() );
                oDet.put("diaID", ""+hd.getDisId() );
                oDet.put("plazaID", hd.getPlaMagId() );
                oDet.put("gradoID", hd.grado);
                oDet.put("seccionID", ""+hd.seccion);                
                a.put(oDet);
                
                return;
            }
        }
        
        JSONObject nuevo = new JSONObject();
        nuevo.put("diaID", hd.getDisId());
        nuevo.put("dia", ""+hd.getDisId());

        JSONArray a = new JSONArray();

        JSONObject oDet = new JSONObject();
        oDet.put("horarioDetalleID", hd.getHorDetId() );
        oDet.put("horaInicio", hd.getHorIni());
        /*oDet.put("horaI", hd.getHorIni().getHours());
        oDet.put("minutoI", hd.getHorIni().getMinutes());*/
        oDet.put("horaFin", hd.getHorFin());
        /*oDet.put("horaF", hd.getHorFin().getHours());
        oDet.put("minutoF", hd.getHorFin().getMinutes());*/
        oDet.put("aulaID", hd.getAulId() );
        oDet.put("areaID", hd.getAreCurId() );
        oDet.put("diaID", ""+hd.getDisId() );
        oDet.put("plazaID", hd.getPlaMagId() );
        oDet.put("gradoID", hd.grado);
        oDet.put("seccionID", ""+hd.seccion);

        a.put(oDet);

        nuevo.put("intervalos", a);
        
        lista.put(nuevo);
    }
    
    
    
}

