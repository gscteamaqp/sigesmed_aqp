   /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.mantenimiento.v1.tx;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.di.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;
/**
 *
 * @author Administrador
 */
public class BuscarPersonaPorIdTx implements ITransaction{
        @Override
    public WebResponse execute(WebRequest wr) {        
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        JSONObject requestData = (JSONObject)wr.getData();
        Long perId = requestData.getLong("perID");
        String lala="";
        
        Persona pe = null;
        PersonaDao peDao = (PersonaDao)FactoryDao.buildDao("di.PersonaDao");
        
        try{
            pe = peDao.buscarPersonaxId(perId);
        
        }catch(Exception e){
            System.out.println("No se encontro a la persona con el DNI dado \n"+e);
            return WebResponse.crearWebResponseError("No se encontro a la persona con el DNI dado ", e.getMessage() );
        }
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        //JSONArray miArray = new JSONArray();
        
        JSONObject oResponse = new JSONObject();
                
        if (pe != null){
            oResponse.put("parId",pe.getPerId());
            oResponse.put("parDni",pe.getDni());
            oResponse.put("parMat",pe.getApeMat());
            oResponse.put("parPat",pe.getApePat());
            oResponse.put("parNom",pe.getNom());
            oResponse.put("parDir",pe.getPerDir());
            oResponse.put("parTel",pe.getFij());
            return WebResponse.crearWebResponseExito("La persona si existe ",oResponse);        
        }
                
        //oResponse.put("traId",pariente.getTrabajador().getTraId());            
        //miArray.put(oResponse);
        return WebResponse.crearWebResponseExito("La persona no existe",oResponse);        
                
        
                
    }   


}
