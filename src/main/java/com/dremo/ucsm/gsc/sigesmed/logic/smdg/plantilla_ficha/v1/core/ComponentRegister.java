/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.plantilla_ficha.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.plantilla_grupo.v1.tx.ActualizarGrupoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.plantilla_indicadores.v1.tx.ActualizarIndicadorTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.plantilla_ficha.v1.tx.ActualizarPlantillaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.plantilla_grupo.v1.tx.EliminarGrupoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.plantilla_indicadores.v1.tx.EliminarIndicadorTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.plantilla_ficha.v1.tx.EliminarPlantillaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.plantilla_ficha.v1.tx.ImprimirPlantillaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.plantilla_grupo.v1.tx.ListarGruposTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.plantilla_ficha.v1.tx.ListarPlantillaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.plantilla_ficha.v1.tx.ListarPlantillasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.plantilla_ficha.v1.tx.ListarTipoPlantillasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.plantilla_ficha.v1.tx.RegistrarPlantillaTx;

/**
 *
 * @author Administrador
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_MONITOREO_DOCUMENTOS_GESTION);        
        
        //Registrando el Nombre del componente
        component.setName("plantilla_ficha");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionGET("listarPlantillas", ListarPlantillasTx.class);
        component.addTransactionGET("listarPlantilla", ListarPlantillaTx.class);
        
        component.addTransactionGET("listarTipoPlantillas", ListarTipoPlantillasTx.class);
        component.addTransactionPUT("actualizarPlantilla", ActualizarPlantillaTx.class);
        component.addTransactionPOST("registrarPlantilla", RegistrarPlantillaTx.class);
        component.addTransactionPUT("eliminarPlantilla", EliminarPlantillaTx.class);        
        component.addTransactionPUT("actualizarIndicador", ActualizarIndicadorTx.class);        
        component.addTransactionPUT("eliminarIndicador", EliminarIndicadorTx.class);
        
        component.addTransactionGET("imprimirPlantilla", ImprimirPlantillaTx.class);
                
        return component;
    }
}

