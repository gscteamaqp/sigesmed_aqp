package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.PerfilCapacitadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.CursoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.PerfilCapacitador;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;

public class PerfilCapacitadorDaoHibernate extends GenericDaoHibernate<PerfilCapacitador> implements PerfilCapacitadorDao {
    private static final Logger logger = Logger.getLogger(PerfilCapacitadorDaoHibernate.class.getName());
    
    @Override
    public List<PerfilCapacitador> listarPerfilesCursoCapacitacion(int curCapId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            CursoCapacitacion capacitacion = (CursoCapacitacion)session.get(CursoCapacitacion.class,curCapId);

            return new ArrayList<>(capacitacion.getPerfilesCapacitacion());
        } catch (Exception e){
            logger.log(Level.SEVERE,"listarPerfilesCursoCapacitacion",e);
            throw e;
        } finally {
            session.close();
        }
    }    

    @Override
    public PerfilCapacitador buscarPorId(int idPer) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            PerfilCapacitador perfil = (PerfilCapacitador)session.get(PerfilCapacitador.class,idPer);

            return perfil;
        } catch (Exception e){
            logger.log(Level.SEVERE,"buscarPorId",e);
            throw e;
        } finally {
            session.close();
        }
    }
}
