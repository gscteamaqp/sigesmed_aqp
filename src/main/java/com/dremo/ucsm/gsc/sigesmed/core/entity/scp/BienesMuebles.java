/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.scp;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import java.util.List;
/**
 *
 * @author Jeferson
 */
@Entity
@Table(name="bienes_muebles", schema="administrativo")
public class BienesMuebles implements java.io.Serializable {
    
    @Id
    @Column(name="cod_bie" , unique=true, nullable=false)
    @SequenceGenerator(name="secuencia_inv_tra",sequenceName="administrativo.bienes_muebles_cod_bie_seq")
    @GeneratedValue(generator="secuencia_inv_tra")
    private int cod_bie;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="amb_id" , insertable=false , updatable=false)
    private Ambientes ambiente;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="an_id" , insertable=false , updatable=false)
    private AnexoBienes anexo;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cod_bie" , insertable=false , updatable=false)
    private DetalleTecnicoMuebles dtm ;
    
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cod_bie" , insertable=false , updatable=false)
    private ValorContable val_cont;       
    
    @Column(name="cat_bie_id")
    private int cat_bie_id;
    
    @Column(name="amb_id")
    private int amb_id;
    
    @Column(name="an_id")
    private int an_id;
    
    @Column(name="cant_bie")
    private int cant_bie;
    
    @Column(name="des_bie")
    private String des_bie;
    
    @Column(name="fec_reg")
    private Date fec_reg;
    
    @Column(name="cod_int")
    private String cod_int;
    
    @Column(name="estado_bie")
    private String estado_bie;
    
    @Column(name="rut_doc_bie")
    private String rut_doc_bie;
    
    @Column(name="cod_ba_bie")
    private String cod_ba_bie;
    
    @Column(name="usu_mod")
    private int usu_mod;
    
    @Column(name="fec_mod")
    private Date fec_mod;

    
    @Column(name="est_reg")
    private char est_reg;
    
    @Column(name="org_id")
    private int org_id;
    
    @Column(name="cont_pat_id")
    private int con_pat_id;
    
//    @OneToOne(fetch=FetchType.LAZY)
//    @JoinColumns({
//        @JoinColumn(name="con_pat_id",referencedColumnName="con_pat_id",insertable=false,updatable=false),
//        @JoinColumn(name="org_id",referencedColumnName="org_id",insertable=false,updatable=false)
//    })
//    private ControlPatrimonial cp;
    
    @Column(name="mov_ing_id")
    private int mov_ing_id;
    
    @Column(name="verificar")
    private String verificar;

    public void setVerificar(String verificar) {
        this.verificar = verificar;
    }

    public String getVerificar() {
        return verificar;
    }
    
    

    public void setMov_ing_id(int mov_ing_id) {
        this.mov_ing_id = mov_ing_id;
    }

    public int getMov_ing_id() {
        return mov_ing_id;
    }
    
    public void setCon_pat_id(int cod_pat_id) {
        this.con_pat_id = cod_pat_id;
    }

    public int getCon_pat_id() {
        return con_pat_id;
    }
        
//    public ControlPatrimonial getCp() {
//        return cp;
//    }
//
//    public void setCp(ControlPatrimonial cp) {
//        this.cp = cp;
//    }

    public void setOrg_id(int org_id) {
        this.org_id = org_id;
    }

    public int getOrg_id() {
        return org_id;
    }
    

    public void setDtm(DetalleTecnicoMuebles dtm) {
        this.dtm = dtm;
    }

    public DetalleTecnicoMuebles getDtm() {
        return dtm;
    }
 
    public void setCod_bie(int cod_bie) {
        this.cod_bie = cod_bie;
    }

    public BienesMuebles() {
    }

    public void setAmbiente(Ambientes ambiente) {
        this.ambiente = ambiente;
    }

    public void setDes_bie(String des_bie) {
        this.des_bie = des_bie;
    }

    public void setFec_reg(Date fec_reg) {
        this.fec_reg = fec_reg;
    }

    public void setCod_int(String cod_int) {
        this.cod_int = cod_int;
    }

    public void setEstado_bie(String estado_bie) {
        this.estado_bie = estado_bie;
    }

    public void setRut_doc_bie(String rut_doc_bie) {
        this.rut_doc_bie = rut_doc_bie;
    }

    public void setCod_ba_bie(String cod_ba_bie) {
        this.cod_ba_bie = cod_ba_bie;
    }

    public void setEst_reg(char est_reg) {
        this.est_reg = est_reg;
    }

    public int getCod_bie() {
        return cod_bie;
    }

    public Ambientes getAmbiente() {
        return ambiente;
    }

    public void setAnexo(AnexoBienes anexo) {
        this.anexo = anexo;
    }

    public AnexoBienes getAnexo() {
        return anexo;
    }

    public void setVal_cont(ValorContable val_cont) {
        this.val_cont = val_cont;
    }

    public ValorContable getVal_cont() {
        return val_cont;
    }
    
    

    public String getDes_bie() {
        return des_bie;
    }

    public Date getFec_reg() {
        return fec_reg;
    }

    public String getCod_int() {
        return cod_int;
    }

    public String getEstado_bie() {
        return estado_bie;
    }

    public String getRut_doc_bie() {
        return rut_doc_bie;
    }

    public String getCod_ba_bie() {
        return cod_ba_bie;
    }

    public void setCant_bie(int cant_bie) {
        this.cant_bie = cant_bie;
    }

    public int getCant_bie() {
        return cant_bie;
    }

    
    public char getEst_reg() {
        return est_reg;
    }

    public void setUsu_mod(int usu_mod) {
        this.usu_mod = usu_mod;
    }

    public void setFec_mod(Date fec_mod) {
        this.fec_mod = fec_mod;
    }

    public int getUsu_mod() {
        return usu_mod;
    }

    public Date getFec_mod() {
        return fec_mod;
    }

    public void setCat_bie_id(int cat_bie_id) {
        this.cat_bie_id = cat_bie_id;
    }

    public void setAmb_id(int amb_id) {
        this.amb_id = amb_id;
    }

    public void setAn_id(int an_id) {
        this.an_id = an_id;
    }

    public int getCat_bie_id() {
        return cat_bie_id;
    }

    public int getAmb_id() {
        return amb_id;
    }

    public int getAn_id() {
        return an_id;
    }

  

    public BienesMuebles(int cod_bie, String des_bie, Date fec_reg, String cod_int, String estado_bie, String rut_doc_bie, String cod_ba_bie, char est_reg) {
        this.cod_bie = cod_bie;
        this.des_bie = des_bie;
        this.fec_reg = fec_reg;
        this.cod_int = cod_int;
        this.estado_bie = estado_bie;
        this.rut_doc_bie = rut_doc_bie;
        this.cod_ba_bie = cod_ba_bie;
        this.est_reg = est_reg;
    }

    public BienesMuebles(int cod_bie, String des_bie, Date fec_reg, String cod_int,int cant, String estado_bie, String rut_doc_bie, String cod_ba_bie, int usu_mod, Date fec_mod, char est_reg) {
        this.cod_bie = cod_bie;
        this.des_bie = des_bie;
        this.fec_reg = fec_reg;
        this.cod_int = cod_int;
        this.cant_bie = cant;
        this.estado_bie = estado_bie;
        this.rut_doc_bie = rut_doc_bie;
        this.cod_ba_bie = cod_ba_bie;
        this.usu_mod = usu_mod;
        this.fec_mod = fec_mod;
        this.est_reg = est_reg;
    }

    public BienesMuebles(int cod_bie, int cat_bie_id, int amb_id, int an_id, int cant_bie, String des_bie, Date fec_reg, String cod_int, String estado_bie, String rut_doc_bie, String cod_ba_bie, int usu_mod, Date fec_mod,int org_id, char est_reg) {
        this.cod_bie = cod_bie;
        this.cat_bie_id = cat_bie_id;
        this.amb_id = amb_id;
        this.an_id = an_id;
        this.cant_bie = cant_bie;
        this.des_bie = des_bie;
        this.fec_reg = fec_reg;
        this.cod_int = cod_int;
        this.estado_bie = estado_bie;
        this.rut_doc_bie = rut_doc_bie;
        this.cod_ba_bie = cod_ba_bie;
        this.usu_mod = usu_mod;
        this.fec_mod = fec_mod;
        this.est_reg = est_reg;
        this.org_id = org_id;
    }

    
    
    
    
}

