package com.dremo.ucsm.gsc.sigesmed.logic.ma.notas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.NotaEvaluacionIndicadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.IndicadorAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.SesionAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.NotaEvaluacionIndicador;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 17/01/2017.
 */
public class RegistrarNotasIndicadorTx implements ITransaction{
    private static Logger logger = Logger.getLogger(RegistrarNotasIndicadorTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idSession = data.optInt("ses");
        int idUnidad = data.optInt("uni");
        char idPeriodo  = data.getString("per").charAt(0);
        int numPer  = data.getInt("numper");
        
        JSONArray array = data.getJSONArray("estudiantes");
        return registrarNotas(idSession,idUnidad,idPeriodo,numPer,array);
    }

    private WebResponse registrarNotas(int idSession,int idUnidad, char idPeriodo, int numPer,  JSONArray array) {
        try{
            SesionAprendizajeDao sesionDao = (SesionAprendizajeDao) FactoryDao.buildDao("maestro.plan.SesionAprendizajeDao");
            NotaEvaluacionIndicadorDao notaDao = (NotaEvaluacionIndicadorDao) FactoryDao.buildDao("ma.NotaEvaluacionIndicadorDao");
            IndicadorAprendizajeDao indicadorDao = (IndicadorAprendizajeDao) FactoryDao.buildDao("maestro.plan.IndicadorAprendizajeDao");
            EstudianteDao estDao = (EstudianteDao) FactoryDao.buildDao("maestro.EstudianteDao");
            String mensaje = "";
            for(int i = 0; i < array.length(); i++){
                JSONObject est = array.getJSONObject(i);
                JSONArray notas = est.getJSONArray("notas");
                for(int j=0; j < notas.length(); j++){
                    JSONObject nota = notas.getJSONObject(j);
                    int idInd = nota.optInt("ind");
                    int idGraOrgEst = est.getInt("idgra");
                  
                    String not = nota.optString("not","0");
                    String not_lit = nota.optString("not_lit");
                    if(idSession == 0){
                        List<NotaEvaluacionIndicador> nots = notaDao.buscarNotaIndicadoresEstudiante(idInd, idGraOrgEst, idPeriodo, numPer);
                        for(NotaEvaluacionIndicador n: nots){
                            n.setNot_lit(not_lit);
                            n.setNotaEva(not);
                            n.setFecMod(new Date());
                            notaDao.update(n);
                        }
                        mensaje = "Se actualizo correctamente el Registro de Evaluacion-Indicador";
                        break;
                         
                        
                    }else{
                        if(!notaDao.buscarNota(idSession, idInd, idGraOrgEst, idPeriodo, numPer)){
                        //nuevo registro
                        NotaEvaluacionIndicador nei = new NotaEvaluacionIndicador(numPer,not_lit,not);
                        nei.setSesion(sesionDao.buscarSesionById(idSession));
                        nei.setPeriodo(notaDao.buscarPeriodoId(idPeriodo));
                        nei.setIndicador(indicadorDao.buscarPorId(nota.getInt("ind")));
                        nei.setGradoEstudiante(estDao.buscarGradoIEEstudiante(est.getInt("idgra")));
                        notaDao.insert(nei);
                        
                        mensaje = "Se registro correctamente";
                    }else{
                        NotaEvaluacionIndicador nei = notaDao.buscarNotaIndicadorEstudiante(idSession, idInd, idGraOrgEst, idPeriodo, numPer);
                        nei.setNot_lit(not_lit);
                        nei.setNotaEva(not);
                        nei.setFecMod(new Date());
                        notaDao.update(nei);
                        
                        mensaje = "Se actualizo correctamente";
                    }
                    }

                }
            }
            return WebResponse.crearWebResponseExito(mensaje);
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarNotas",e);
            return WebResponse.crearWebResponseError("No se pudo registrar las notas");
        }
    }
}
