/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.util;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfPKCS7;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfSignatureAppearance;
import com.lowagie.text.pdf.PdfStamper;
import com.lowagie.text.pdf.PdfString;
import com.lowagie.text.pdf.AcroFields;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.cert.Certificate;
import java.util.List;
/**
 *
 * @author G-16
 */
public class FirmaDigital {
   public static boolean limpiar(String pathPDFM, String pathPDFO){
    File fichero = new File(pathPDFO); 
    fichero.delete();
    //reenombrar(pathPDFM,pathPDFO);
    return true;
}
public static boolean reenombrar(String rutaPDFM,String rutaPDFO){
            File fichero = new File(rutaPDFM);
            File fichero2 = new File(rutaPDFO);
            boolean success = fichero.renameTo(fichero2);
            System.out.println(success);
            if (!success) {
                System.out.println("Error intentando cambiar el nombre de fichero");
                
                return false;
            }
            else {
               System.out.println("Correcto cambio de nombre de fichero");
                return true;
            }
}
public static boolean verificar(String ruta){
    File af = new File(ruta);
    return af.exists();
}
public static boolean verifacarPassCert(){
    return true;
}
public static String sign(String pathpdf, String pathPKCS12, String passwordPKCS12)
{  
        File fichero = new File(pathpdf);
        String fileKey = pathPKCS12;
        String fileKeyPassword = passwordPKCS12;
        try {
            //Crea un KeyStore
            KeyStore ks = KeyStore.getInstance("pkcs12");
            //Carga Certificado
            ks.load(new FileInputStream(fileKey), fileKeyPassword.toCharArray());
            String alias = (String)ks.aliases().nextElement();
            //Recupera Clave Privada
            PrivateKey key = (PrivateKey)ks.getKey(alias, fileKeyPassword.toCharArray());
            //Recupera Cadena de Certificacion si existe
            Certificate[] chain = ks.getCertificateChain(alias);
            //Lee Documento PDF y crea archivo de salida con otro nombre para no pisar el original
            PdfReader pdfReader = new PdfReader((new File(pathpdf)).getAbsolutePath());
            System.out.println(pathpdf);
            
           // FileOutputStream fout = new FileOutputStream(pathpdf+".pdf");
          
            // Crea la firma en el objeto PdfStamper de la librería iText
            
            // obtenemos la lista de firmas del doc si es que ubiera
            AcroFields acroFields = pdfReader.getAcroFields();
            List<String> signatureNames = acroFields.getSignatureNames();    
            PdfStamper pdfStamper;
            int numFirmas = signatureNames.size();
            
            if(numFirmas > 50){
                 throw new Exception("El Documento Excedio el limite de Firmas permitidas"); 
             }
            
             FileOutputStream fout = new FileOutputStream(pathpdf+".pdf");
             
            // verificando si se encuentra mas firmas en el documento  
            if(signatureNames.isEmpty()){
                pdfStamper = PdfStamper.createSignature(pdfReader, fout, '\0');
            } else{
                pdfStamper = PdfStamper.createSignature(pdfReader, fout, '\0', null,
                        true);
            }
            //pdfStamper = PdfStamper.createSignature(pdfReader, null, '\0', outputFile);
           // pdfStamper = PdfStamper.createSignature(pdfReader,fout, '\0');
           
            PdfSignatureAppearance sap = pdfStamper.getSignatureAppearance();
            sap.setCrypto(key, chain, null, PdfSignatureAppearance.SELF_SIGNED);
            sap.setReason("Test2 GGS");
            sap.setLocation("Peru");
            // Posiciona la Firma
            String sName = "sign_" + (signatureNames.size());
            //------------------------------------------------------------------------------
             Rectangle cropBox = pdfReader.getCropBox(1);
             float width = 108;
             float height = 32;
             Rectangle rectangle = null;

             //encaso de ser la primera firma , esta se ubicara en la parte superior izquierda del documento
             if(numFirmas >= 0&& numFirmas <=25){
                rectangle = new Rectangle(cropBox.getLeft(), cropBox.getTop(height)-(32*numFirmas),
                cropBox.getLeft(width), cropBox.getTop()-(32*numFirmas));
             }
             //en caso de superar las 25 firmas de lado izquierdo se procede al lado derecho permitiendo un total de 50 firmas 
             else if(numFirmas > 25 && numFirmas <= 50 ){
                
                rectangle = new Rectangle(cropBox.getRight(width), cropBox.getTop(height)-(32*(numFirmas-26)),
                              cropBox.getRight(), cropBox.getTop()-(32*(numFirmas-26)));
             }
             
            //sap.setVisibleSignature(new Rectangle(10, 10, 50, 30), 2, "sign_g4gs");
            //sap.setVisibleSignature(new Rectangle(10, 10, 50, 30),1, sName);
            sap.setVisibleSignature(rectangle,1, sName);
            //tamaño del key
            sap.setExternalDigest(new byte[512],null,"RSA");
            sap.preClose();
            
            PdfPKCS7 sig= sap.getSigStandard().getSigner();
            Signature sign = Signature.getInstance("SHA1withRSA");
            sign.initSign(key);
            byte buf[]= new byte[8192];
            int n;
            InputStream inp = sap.getRangeStream();
            while ((n = inp.read(buf)) > 0){
               sign.update(buf,0,n);
            }
            sig.setExternalDigest(sign.sign(),null,"RSA");
            PdfDictionary dic = new PdfDictionary();
            dic.put(PdfName.CONTENTS,new PdfString(sig.getEncodedPKCS1()).setHexWriting(true));
            System.out.println(pathpdf+".pdf");
            sap.close(dic);
          

           // pdfStamper.setFormFlattening(true);
           // pdfStamper.close();
        }
        catch (Exception key) {
            key.printStackTrace();
            return "ERROR";
        }
      return (pathpdf+".pdf");
 }

}
