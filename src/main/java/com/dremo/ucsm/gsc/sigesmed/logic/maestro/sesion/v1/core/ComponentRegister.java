package com.dremo.ucsm.gsc.sigesmed.logic.maestro.sesion.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.maestro.sesion.v1.tx.*;

/**
 * Created by Administrador on 31/10/2016.
 */
public class ComponentRegister implements IComponentRegister {
    @Override
    public WebComponent createComponent() {
        WebComponent component = new WebComponent(Sigesmed.SUBMODULO_MAESTRO);
        component.setName("sesiones");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        //Competencias
        component.addTransactionGET("listarSesiones", ListarSesionesAprendizajeTx.class);
        component.addTransactionGET("listarSecuenciasSesion", ListarSecuenciasSesionTx.class);
        component.addTransactionGET("listarAprendizajesUnidad", ListarAprendizajesUnidadTx.class);
        component.addTransactionGET("listarSeccionesSequencia", ListarSeccionSequenciaTx.class);
        component.addTransactionGET("listarTareasSesion", ListarTareasSesionTx.class);
        component.addTransactionPOST("registrarSesion", RegistrarSesionAprendizajeTx.class);
        component.addTransactionPOST("finalizarSesion", FinalizarSesionAprendizajeTx.class);
        component.addTransactionPOST("registrarIndicadoresSesion", RegistrarIndicadoresSesionTx.class);
        component.addTransactionPOST("registrarSecuenciaDidactica", RegistrarSecuenciaDidacticaTx.class);
        component.addTransactionPOST("registrarTareaSesion", RegistrarTareaSesionTx.class);
        component.addTransactionDELETE("eliminarSesionAprendizaje", EliminarSesionAprendizajeTx.class);
        component.addTransactionDELETE("eliminarSecuenciaDidactica", EliminarSecuenciaDidacticaTx.class);
        component.addTransactionDELETE("eliminarTareaSesion", EliminarTareaSesionTx.class);
        component.addTransactionPUT("editarSesionAprendizaje", EditarSesionAprendizajeTx.class);
        component.addTransactionPUT("editarTarea",EditarTareaTx.class);
        return component;
    }
}
