package com.dremo.ucsm.gsc.sigesmed.logic.maestro.unidad.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.maestro.unidad.v1.tx.*;

/**
 * Created by Administrador on 31/10/2016.
 */
public class ComponentRegister implements IComponentRegister {
    @Override
    public WebComponent createComponent() {
        WebComponent component = new WebComponent(Sigesmed.SUBMODULO_MAESTRO);
        component.setName("unidades");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        //Competencias
        component.addTransactionGET("listarUnidadesDidacticas", ListarUnidadesTx.class);
        component.addTransactionGET("listarPeriodosPlanEstudios", ListarPeriodosPlanTx.class);
        component.addTransactionGET("listarCompetenciasArea", ListarCompetenciasAreaTx.class);
        component.addTransactionGET("listarCompetenciasUnidad", ListarCompetenciasUnidadTx.class);
        component.addTransactionGET("listarMaterialUnidad", ListarMaterialUnidadTx.class);
        component.addTransactionGET("listarEvaluacionesUnidad", ListarEvaluacionesUnidadTx.class);
        component.addTransactionPOST("registrarCapacidadesUnidad", RegistrarCapacidadUnidadTx.class);
        component.addTransactionPOST("registrarIndicadoresUnidad", RegistrarIndicadoresUnidadDidacticaTx.class);
        component.addTransactionPOST("registrarMaterialesUnidad", RegistrarMaterialUnidadTx.class);
        component.addTransactionPOST("registrarProductosUnidad", RegistrarProductosUnidadTx.class);
        component.addTransactionPOST("registrarEvaluacionesUnidad", RegistrarEvaluacionesUnidadTx.class);
        component.addTransactionDELETE("eliminarUnidadDidactica", EliminarUnidadDidacticaTx.class);
        component.addTransactionDELETE("eliminarMaterialesUnidad", EliminarMaterialUnidadTx.class);
        component.addTransactionDELETE("eliminarCompetenciaUnidad", EliminarCompetenciaUnidadTx.class);
        component.addTransactionDELETE("eliminarProductosUnidad", EliminarProductosUnidadTx.class);
        component.addTransactionDELETE("eliminarEvaluacionesUnidad", EliminarEvaluacionesUnidadTx.class);
        component.addTransactionPUT("editarMaterialesUnidad", EditarMaterialUnidadTx.class);
        component.addTransactionPUT("editarProductosUnidad", EditarProductosUnidadTx.class);
        component.addTransactionPUT("editarProductosUnidad", EditarUnidadDidacticaTx.class);
        component.addTransactionPUT("editarEvaluacionesUnidad", EditarEvaluacionesUnidadTx.class);
        component.addTransactionGET("listarAprendizajesEvaluacion", ListarAprendizajesEvaluacionTx.class);
        return component;
    }
}
