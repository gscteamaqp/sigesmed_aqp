package com.dremo.ucsm.gsc.sigesmed.core.entity.mpf;

import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.GradoIEEstudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.PeriodosPlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CompetenciaAprendizaje;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Carlos
 */

public class RegistroAuxiliarCompetenciaModel implements java.io.Serializable {
    private String competencia;
    private String nota[];
    private String area;
    private int competenciaId;
    private int areaId;
    public RegistroAuxiliarCompetenciaModel() {
    }

    public RegistroAuxiliarCompetenciaModel(String competencia, String nota[], String area,int competenciaId,int areaId) {
        this.competencia = competencia;
        this.nota = nota;
        this.area = area;
        this.competenciaId=competenciaId;
        this.areaId=areaId;
    }

    public String getCompetencia() {
        return competencia;
    }

    public void setCompetencia(String competencia) {
        this.competencia = competencia;
    }

    public String[] getNota() {
        return nota;
    }

    public void setNota(String nota[]) {
        this.nota = nota;
    }
    
     public String getNota(int i) {
        return nota[i];
    }

    public void setNota(String nota,int index) {
        this.nota[index] = nota;
    }


    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public int getCompetenciaId() {
        return competenciaId;
    }

    public void setCompetenciaId(int competenciaId) {
        this.competenciaId = competenciaId;
    }

    public int getAreaId() {
        return areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }
    
    
}
