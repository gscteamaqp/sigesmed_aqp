/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author gscadmin
 */
@Entity
@Table(name = "formacion_educativa", schema="administrativo")

public class FormacionEducativa implements Serializable {

    @Id
    @Column(name = "for_edu_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_formacion_educativa", sequenceName="administrativo.formacion_educativa_for_edu_id_seq" )
    @GeneratedValue(generator="secuencia_formacion_educativa")
    private Integer forEduId;
    
    @Column(name = "tip_for")
    private Character tipFor;
    
    @Column(name = "niv_aca")
    private String nivAca;
    
    @Column(name = "num_tit")
    private String numTit;
    
    @Column(name = "esp_aca")
    private String espAca;
    
    @Column(name = "est_con")
    private Boolean estCon;
    
    @Column(name = "fec_exp")
    @Temporal(TemporalType.DATE)
    private Date fecExp;
    
    @Column(name = "cen_est")
    private String cenEst;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "est_reg")
    private Character estReg;
     
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fic_esc_id")
    private FichaEscalafonaria fichaEscalafonaria;

    public FormacionEducativa() {
    }
    
    public FormacionEducativa(FichaEscalafonaria fichaEscalafonaria, Character tipFor, String nivAca, String numTit, String espAca, Boolean estCon, Date fecExp, String cenEst, Integer usuMod, Date fecMod, Character estReg) {
        this.fichaEscalafonaria = fichaEscalafonaria;
        this.tipFor = tipFor;
        this.nivAca = nivAca;
        this.numTit = numTit;
        this.espAca = espAca;
        this.estCon = estCon;
        this.fecExp = fecExp;
        this.cenEst = cenEst;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
    }

    public FormacionEducativa(Integer forEduId) {
        this.forEduId = forEduId;
    }

    public Integer getForEduId() {
        return forEduId;
    }

    public void setForEduId(Integer forEduId) {
        this.forEduId = forEduId;
    }

    public Character getTipFor() {
        return tipFor;
    }

    public void setTipFor(Character tipFor) {
        this.tipFor = tipFor;
    }

    public String getNivAca() {
        return nivAca;
    }

    public void setNivAca(String nivAca) {
        this.nivAca = nivAca;
    }

    public String getNumTit() {
        return numTit;
    }

    public void setNumTit(String numTit) {
        this.numTit = numTit;
    }

    public String getEspAca() {
        return espAca;
    }

    public void setEspAca(String espAca) {
        this.espAca = espAca;
    }

    public Boolean getEstCon() {
        return estCon;
    }

    public void setEstCon(Boolean estCon) {
        this.estCon = estCon;
    }

    public Date getFecExp() {
        return fecExp;
    }

    public void setFecExp(Date fecExp) {
        this.fecExp = fecExp;
    }

    public String getCenEst() {
        return cenEst;
    }

    public void setCenEst(String cenEst) {
        this.cenEst = cenEst;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public FichaEscalafonaria getFichaEscalafonaria() {
        return fichaEscalafonaria;
    }

    public void setFichaEscalafonaria(FichaEscalafonaria fichaEscalafonaria) {
        this.fichaEscalafonaria = fichaEscalafonaria;
    }

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsc.sigesmed.core.entity.se.FormacionEducativa[ forEduId=" + forEduId + " ]";
    }
    
}
