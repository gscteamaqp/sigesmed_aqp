/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.scp;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;

/**
 *
 * @author Jeferson
 */
@Entity
@Table(name="bienes_inmuebles", schema="administrativo")
public class BienesInmuebles {
    
    @Id
    @Column(name="bie_inm_id" , unique=true, nullable=false)
    @SequenceGenerator(name="secuencia_inv_tra",sequenceName="administrativo.bienes_inmuebles_bie_inm_id_seq")
    @GeneratedValue(generator="secuencia_inv_tra")
    private int bie_inm_id;
    
    @Column(name="descripcion")
    private String descripcion;
    
    
    @Column(name="bie_inm_dir")
    private String bie_inm_dir;
    
    @Column(name="dir_numero")
    private int dir_numero;
    
    @Column(name="dir_mz")
    private String dir_mz;
    
    @Column(name="dir_lte")
    private int dir_lte;
    
    @Column(name="depart")
    private String departamento;
    
    @Column(name="prov")
    private String provincia;
    
    @Column(name="dist")
    private String distrito;
    
    @Column(name="fec_reg")
    private Date fec_reg;
    
    @Column(name="user_mod")
    private int user_mod;
    
    @Column(name="fec_mod")
    private Date fec_mod;
    
    @Column(name="est_reg")
    private char est_reg;
    
    @Column(name="amb_id")
    private int amb_id;
    
    @Column(name="org_id")
    private int org_id;
    
    @Column(name="tipo_dir")
    private String tipo_dir;
    
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="bie_inm_id" , insertable=true , updatable=false)
    private DetalleTecnicoInmuebles dtm;
        
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="amb_id", insertable=false , updatable=false )
    private Ambientes amb;
    

    public void setOrg_id(int org_id) {
        this.org_id = org_id;
    }

    public int getOrg_id() {
        return org_id;
    }

    public void setDtm(DetalleTecnicoInmuebles dtm) {
        this.dtm = dtm;
    }

    public DetalleTecnicoInmuebles getDtm() {
        return dtm;
    }
    
    public void setAmb_id(int amb_id) {
        this.amb_id = amb_id;
    }

    public int getAmb_id() {
        return amb_id;
    }
    
    
    
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public void setFec_reg(Date fec_reg) {
        this.fec_reg = fec_reg;
    }

    public void setUser_mod(int user_mod) {
        this.user_mod = user_mod;
    }

    public void setFec_mod(Date fec_mod) {
        this.fec_mod = fec_mod;
    }

    public String getDepartamento() {
        return departamento;
    }

    public String getProvincia() {
        return provincia;
    }

    public String getDistrito() {
        return distrito;
    }

    public Date getFec_reg() {
        return fec_reg;
    }

    public int getUser_mod() {
        return user_mod;
    }

    public Date getFec_mod() {
        return fec_mod;
    }
 
    public void setBie_inm_id(int bie_inm_id) {
        this.bie_inm_id = bie_inm_id;
    }

    public void setBie_inm_dir(String bie_inm_dir) {
        this.bie_inm_dir = bie_inm_dir;
    }

    public void setDir_numero(int dir_numero) {
        this.dir_numero = dir_numero;
    }

    public void setDir_mz(String dir_mz) {
        this.dir_mz = dir_mz;
    }

    public void setDir_lte(int dir_lte) {
        this.dir_lte = dir_lte;
    }

    public void setEst_reg(char est_reg) {
        this.est_reg = est_reg;
    }

    public int getBie_inm_id() {
        return bie_inm_id;
    }

    public String getBie_inm_dir() {
        return bie_inm_dir;
    }

    public int getDir_numero() {
        return dir_numero;
    }

    public String getDir_mz() {
        return dir_mz;
    }

    public int getDir_lte() {
        return dir_lte;
    }

    public char getEst_reg() {
        return est_reg;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setAmb(Ambientes amb) {
        this.amb = amb;
    }

    public Ambientes getAmb() {
        return amb;
    }

    public void setTipo_dir(String tipo_dir) {
        this.tipo_dir = tipo_dir;
    }

    public String getTipo_dir() {
        return tipo_dir;
    }
    
    

    public BienesInmuebles() {
    }

    public BienesInmuebles(int bie_inm_id,String des , String bie_inm_dir, int dir_numero, String dir_mz, int dir_lte, String departamento, String provincia, String distrito, Date fec_reg, int user_mod, Date fec_mod, char est_reg) {
        
        this.descripcion = des;
        this.bie_inm_id = bie_inm_id;
        this.bie_inm_dir = bie_inm_dir;
        this.dir_numero = dir_numero;
        this.dir_mz = dir_mz;
        this.dir_lte = dir_lte;
        this.departamento = departamento;
        this.provincia = provincia;
        this.distrito = distrito;
        this.fec_reg = fec_reg;
        this.user_mod = user_mod;
        this.fec_mod = fec_mod;
        this.est_reg = est_reg;
    }

    
    
    
    
}
