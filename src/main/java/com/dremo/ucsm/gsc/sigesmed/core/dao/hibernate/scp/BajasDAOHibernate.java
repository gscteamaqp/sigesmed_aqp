/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scp;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.Bajas;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.BajasDAO;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.AltasDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BajasDetalle;

/**
 *
 * @author Administrador
 */
public class BajasDAOHibernate extends GenericDaoHibernate<Bajas> implements BajasDAO{

    @Override
    public List<Bajas> listar_bajas_bienes(int org_id) {
        
         List<Bajas> bajas = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
           // String hql = "SELECT ba FROM Bajas ba JOIN FETCH ba.causal_baja JOIN FETCH ba.bd bajas_detalle JOIN FETCH bajas_detalle.bm bienes JOIN FETCH bienes.dtm  WHERE ba.est_reg!='E' and ba.org_id=:p1";
            //String hql ="SELECT ba FROM Bajas ba JOIN FETCH ba.causal_baja JOIN FETCH ba.bd bajas_detalle JOIN FETCH bajas_detalle.bm bienes JOIN FETCH bienes.dtm WHERE ba.org_id=:p1 ";
            String hql ="SELECT ba FROM Bajas ba JOIN FETCH ba.causal_baja JOIN FETCH ba.bd bajas_detalle JOIN FETCH bajas_detalle.bm bienes WHERE ba.est_reg!='E' and ba.org_id=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1",org_id);
            bajas = query.list();
            
        }
        catch(Exception e){
         System.out.println("No se pudo Mostrar la lista de Bienes de Baja \\n "+ e.getMessage());
             throw new UnsupportedOperationException("No se pudo Mostrar la lista de Bienes de Baja \\n "+ e.getMessage());
        }
        finally{
            session.close();
        }
        return bajas;  
        
        
        
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Bajas obtenerBaja(int cod_bie) {
        
        
        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AltasDetalle alta_baja(int cod_bie) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        AltasDetalle alt_det = null;
        try{
            String hql = "SELECT alt_det FROM AltasDetalle alt_det WHERE alt_det.cod_bie=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1",cod_bie);
            alt_det = (AltasDetalle)(query.uniqueResult());
            
            
        }catch(Exception e){
            System.out.println("No se pudo Obtener el Detalle del Bien de Alta\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Obtener el Detalle del Bien de Alta\\n "+ e.getMessage());

        }finally{
            session.close();
        }
        return alt_det;  
        
        
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int contar_bajas(int org_id) {
        
        
        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
