/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.asistencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.AsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.HorarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.InasistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.DiaSemana;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.DiasEspeciales;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.HorarioCab;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.HorarioDet;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.RegistroAsistencia;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.DateUtil;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author carlos
 */
public class MarcarAsistenciaTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {

        Trabajador trab = null;
        Integer dayOfWeek;
        Integer idRegistro;
        Integer tolHora;
        Integer tolMin;
        Date javaDate;
        String estadoReg;
        String horaSalida;
        Integer organizacion;
        try {
            JSONObject requestData = (JSONObject) wr.getData();
            Integer idTrab = requestData.getInt("id");
            String hora = requestData.getString("hora");
            tolHora = requestData.getInt("toleranciaHora");
            tolMin = requestData.getInt("toleranciaMin");
            organizacion=requestData.getInt("orgID");
            //estadoReg = requestData.getString("estadoReg");
            JSONObject registroLast = requestData.getJSONObject("lastReg");

            javaDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(hora);
            Calendar c = Calendar.getInstance();
            c.setTime(javaDate);
            dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
            trab = new Trabajador(idTrab);

            idRegistro = registroLast.getInt("id");
            horaSalida = registroLast.getString("salida");

        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo verificar los datos", e.getMessage());
        }
        AsistenciaDao asistenciaDao = (AsistenciaDao) FactoryDao.buildDao("cpe.AsistenciaDao");
        InasistenciaDao inasistenciaDao = (InasistenciaDao) FactoryDao.buildDao("cpe.InasistenciaDao");
        
        
        try {
            List<DiasEspeciales> diasEspecialesNoLaborables=inasistenciaDao.getDiasEspeciales(new Organizacion(organizacion), javaDate, javaDate, Sigesmed.ESTADO_DIA_ESPECIAL_NO_LABORABLE);
            if(diasEspecialesNoLaborables.size()>0)
            {
                return WebResponse.crearWebResponseError("Dia No Laborable en la Institucion");
            }
                 
            List<HorarioDet> horarioDay = null;
            SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm a");//dd/MM/yyyy
            switch (dayOfWeek) {
                case 1:
                    horarioDay = asistenciaDao.listarHorario(trab, new DiaSemana('D'));
                    break;
                case 2:
                    horarioDay = asistenciaDao.listarHorario(trab, new DiaSemana('L'));
                    break;
                case 3:
                    horarioDay = asistenciaDao.listarHorario(trab, new DiaSemana('M'));
                    break;
                case 4:
                    horarioDay = asistenciaDao.listarHorario(trab, new DiaSemana('W'));
                    break;
                case 5:
                    horarioDay = asistenciaDao.listarHorario(trab, new DiaSemana('J'));
                    break;
                case 6:
                    horarioDay = asistenciaDao.listarHorario(trab, new DiaSemana('V'));
                    break;
                case 7:
                    horarioDay = asistenciaDao.listarHorario(trab, new DiaSemana('S'));
                    break;

            }
            if (horarioDay.size() != 0) //si tiene horario
            {
                HorarioCab horarioCabecera = horarioDay.get(0).getHorCabId();
                if (idRegistro == -1) //si captura un registro vacio desde la interfaz
                {
                    //buscamos turno
                    HorarioDet turnoActual = new HorarioDet();
                    boolean tm = false;
                    for (HorarioDet h : horarioDay) {
                        if (DateUtil.compararHora1yHora2(h.getFecSalida(), javaDate)) {
                            turnoActual = h;
                            tm = true;
                            break;
                        }
                    }
                    if (tm)//si horario es reconocido
                    {
                        //caso que sea entrada
                        if (DateUtil.compararHora1yHora2(turnoActual.getFecIngreso(), javaDate)) //si llega temprano
                        {
                            RegistroAsistencia nuevoRegistro = new RegistroAsistencia(javaDate, null, "A", "1", trab, horarioCabecera);
                            asistenciaDao.insert(nuevoRegistro);

                            JSONObject oRegistro = new JSONObject();
                            oRegistro.put("id", nuevoRegistro.getRegAsiId());
                            oRegistro.put("ingreso", sdfDate.format(nuevoRegistro.getHoraIngreso()));
                            oRegistro.put("salida", "");
                            oRegistro.put("estado", nuevoRegistro.getEstAsi());
                            oRegistro.put("existe", false);

                            return WebResponse.crearWebResponseExito("Se Registro su Ingreso", oRegistro);
                        }
                        Date horaConTolerancia = turnoActual.getFecIngreso();
                        if (tolHora != 0) {
                            horaConTolerancia = DateUtil.sumarRestarHorasFecha(horaConTolerancia, tolHora);
                        }
                        if (tolMin != 0) {
                            horaConTolerancia = DateUtil.sumarRestarMinutosFecha(horaConTolerancia, tolMin);
                        }

                        if (DateUtil.compararHora1yHora2(horaConTolerancia, javaDate)) //si llega tarde
                        {
                            RegistroAsistencia nuevoRegistro = new RegistroAsistencia(javaDate, null, "A", "2", trab, horarioCabecera, DateUtil.getDiferenciaFechas(javaDate, horaConTolerancia, 2));
                            asistenciaDao.insert(nuevoRegistro);

                            JSONObject oRegistro = new JSONObject();
                            oRegistro.put("id", nuevoRegistro.getRegAsiId());
                            oRegistro.put("ingreso", sdfDate.format(nuevoRegistro.getHoraIngreso()));
                            oRegistro.put("salida", "");
                            oRegistro.put("estado", nuevoRegistro.getEstAsi());
                            oRegistro.put("existe", false);
                            return WebResponse.crearWebResponseExito("Se Registro su Ingreso", oRegistro);
                        } else //si llega demasiado tarde
                        {
                            RegistroAsistencia nuevoRegistro = new RegistroAsistencia(javaDate, null, "A", "3", trab, horarioCabecera);
                            asistenciaDao.insert(nuevoRegistro);

                            JSONObject oRegistro = new JSONObject();
                            oRegistro.put("id", nuevoRegistro.getRegAsiId());
                            oRegistro.put("ingreso", sdfDate.format(nuevoRegistro.getHoraIngreso()));
                            oRegistro.put("salida", "");
                            oRegistro.put("estado", nuevoRegistro.getEstAsi());
                            oRegistro.put("existe", false);
                            return WebResponse.crearWebResponseExito("Se Registro su Ingreso", oRegistro);
                        }
                    } else {
                        RegistroAsistencia nuevoRegistro = new RegistroAsistencia(javaDate, null, "A", "3", trab, horarioCabecera);
                        asistenciaDao.insert(nuevoRegistro);
                        JSONObject oRegistro = new JSONObject();
                        oRegistro.put("id", nuevoRegistro.getRegAsiId());
                        oRegistro.put("ingreso", sdfDate.format(nuevoRegistro.getHoraIngreso()));
                        oRegistro.put("salida", "");
                        oRegistro.put("estado", nuevoRegistro.getEstAsi());
                        oRegistro.put("existe", false);

                        return WebResponse.crearWebResponseExito("Se Registro su Ingreso", oRegistro);
                    }
                } else {

                    if (horaSalida.length() > 0) //si el registro tiene una salida
                    {

                        //buscamos turno
                        HorarioDet turnoActual = new HorarioDet();
                        boolean tm = false;
                        for (HorarioDet h : horarioDay) {
                            if (DateUtil.compararHora1yHora2(h.getFecSalida(), javaDate)) {
                                turnoActual = h;
                                tm = true;
                                break;
                            }
                        }
                        if (tm)//si la hora pertenece a un turno
                        {
                            if (DateUtil.compararHora1yHora2(turnoActual.getFecIngreso(), javaDate))//si es temprano
                            {
                                RegistroAsistencia nuevoRegistro = new RegistroAsistencia(javaDate, null, "A", "1", trab, horarioCabecera);
                                asistenciaDao.insert(nuevoRegistro);

                                JSONObject oRegistro = new JSONObject();
                                oRegistro.put("id", nuevoRegistro.getRegAsiId());
                                oRegistro.put("ingreso", sdfDate.format(nuevoRegistro.getHoraIngreso()));
                                oRegistro.put("salida", "");
                                oRegistro.put("estado", nuevoRegistro.getEstAsi());
                                oRegistro.put("existe", false);

                                return WebResponse.crearWebResponseExito("Se Registro su Ingreso", oRegistro);
                            }
                            Date horaConTolerancia = turnoActual.getFecIngreso();
                            if (tolHora != 0) {
                                horaConTolerancia = DateUtil.sumarRestarHorasFecha(horaConTolerancia, tolHora);
                            }
                            if (tolMin != 0) {
                                horaConTolerancia = DateUtil.sumarRestarMinutosFecha(horaConTolerancia, tolMin);
                            }

                            if (DateUtil.compararHora1yHora2(horaConTolerancia, javaDate)) //si es tarde
                            {
                                RegistroAsistencia nuevoRegistro = new RegistroAsistencia(javaDate, null, "A", "2", trab, horarioCabecera, DateUtil.getDiferenciaFechas(javaDate, horaConTolerancia, 2));
                                asistenciaDao.insert(nuevoRegistro);

                                JSONObject oRegistro = new JSONObject();
                                oRegistro.put("id", nuevoRegistro.getRegAsiId());
                                oRegistro.put("ingreso", sdfDate.format(nuevoRegistro.getHoraIngreso()));
                                oRegistro.put("salida", "");
                                oRegistro.put("estado", nuevoRegistro.getEstAsi());
                                oRegistro.put("existe", false);
                                return WebResponse.crearWebResponseExito("Se Registro su Ingreso", oRegistro);
                            } else {//si es demasiado tarde
                                RegistroAsistencia nuevoRegistro = new RegistroAsistencia(javaDate, null, "A", "3", trab, horarioCabecera);
                                asistenciaDao.insert(nuevoRegistro);

                                JSONObject oRegistro = new JSONObject();
                                oRegistro.put("id", nuevoRegistro.getRegAsiId());
                                oRegistro.put("ingreso", sdfDate.format(nuevoRegistro.getHoraIngreso()));
                                oRegistro.put("salida", "");
                                oRegistro.put("estado", nuevoRegistro.getEstAsi());
                                oRegistro.put("existe", false);
                                return WebResponse.crearWebResponseExito("Se Registro su Ingreso", oRegistro);
                            }
                        } else {
                            RegistroAsistencia nuevoRegistro = new RegistroAsistencia(javaDate, null, "A", "3", trab, horarioCabecera);
                            asistenciaDao.insert(nuevoRegistro);
                            JSONObject oRegistro = new JSONObject();
                            oRegistro.put("id", nuevoRegistro.getRegAsiId());
                            oRegistro.put("ingreso", sdfDate.format(nuevoRegistro.getHoraIngreso()));
                            oRegistro.put("salida", "");
                            oRegistro.put("estado", nuevoRegistro.getEstAsi());
                            oRegistro.put("existe", false);

                            return WebResponse.crearWebResponseExito("Se Registro su Ingreso", oRegistro);
                        }

                    }
                    asistenciaDao.registrarHoraSalida(idRegistro, javaDate);
                    JSONObject oRegistro = new JSONObject();
                    oRegistro.put("id", idRegistro);
                    oRegistro.put("salida", sdfDate.format(javaDate));
                    oRegistro.put("existe", true);
                    return WebResponse.crearWebResponseExito("Se Registro su Salida", oRegistro);

                }
            } else {
                return WebResponse.crearWebResponseError("No existe Horario Asociado Hoy");
            }

        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo verificar los datos", e.getMessage());
        }

        //Fin
    }

}
