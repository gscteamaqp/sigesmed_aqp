/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.BandejaAlerta.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.web.BandejaAlerta.v1.tx.ActualizarBandejaAlertaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.BandejaAlerta.v1.tx.ListarBandejaAlertaPendientesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.BandejaAlerta.v1.tx.ListarBandejaAlertaTx;

/**
 *
 * @author abel
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_WEB);        
        
        //Registrnado el Nombre del componente
        component.setName("bandejaAlerta");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionGET("listarBandejaAlerta", ListarBandejaAlertaTx.class);
        component.addTransactionGET("listarBandejaAlertaPendientes", ListarBandejaAlertaPendientesTx.class);
        component.addTransactionPUT("marcarComoVisto", ActualizarBandejaAlertaTx.class);
        
        return component;
    }
}
