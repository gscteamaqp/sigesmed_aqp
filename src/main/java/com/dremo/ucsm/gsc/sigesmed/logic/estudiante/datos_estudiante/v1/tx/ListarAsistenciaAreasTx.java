/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.estudiante.datos_estudiante.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mpf.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.EstudianteAsistenciaModel;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author HernanF
 */
public class ListarAsistenciaAreasTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        Long matriculaID;
        JSONArray areas;
        Date desde;
        Date hasta;
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sfa = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        try {
            
            JSONObject requestData = (JSONObject)wr.getData();
            
            areas = requestData.getJSONArray("area");
            matriculaID = requestData.getLong("matriculaID");
            String fecDesde = requestData.getString("desde");
            String fecHasta = requestData.getString("hasta");
            
            desde = sf.parse(fecDesde);
            hasta = sf.parse(fecHasta);
           
        } catch (Exception e) {
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listar las assitencias, datos incorrectos", e.getMessage() );
        }
        
        EstudianteDao estudianteDao = (EstudianteDao)FactoryDao.buildDao("mpf.EstudianteDao");
        List<EstudianteAsistenciaModel> asistencias = null;
        JSONObject oResponse = new JSONObject();
        try {
            int l = areas.length();
            for (int i = 0; i < l; i++) {
                JSONArray aAre = new JSONArray();
                JSONObject aAsistencia = new JSONObject();
                int j=0;
                int nAsis=0;
                int nTjus=0;
                int nTinj=0;
                int nFjus=0;
                int nFinj=0;
                int areaId = areas.getInt(i);
                
                asistencias = estudianteDao.buscarAsistenciaByAlumno(matriculaID, areaId, desde, hasta);
                
                for (EstudianteAsistenciaModel a : asistencias) {
                    JSONObject oAre = new JSONObject();
                    oAre.put("i", i );
                    oAre.put("fecha", sfa.format(a.getFecAsi()));

                    Integer dayOfWeek;
                    Calendar c = Calendar.getInstance();
                    c.setTime(a.getFecAsi());
                    dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

                    switch (dayOfWeek) {
                        case 1:
                            oAre.put("dia", "Domingo" );
                            break;
                        case 2:
                            oAre.put("dia", "Lunes" );
                            break;
                        case 3:
                            oAre.put("dia", "Martes" );
                            break;
                        case 4:
                            oAre.put("dia", "Miercoles" );
                            break;
                        case 5:
                            oAre.put("dia", "Jueves" );
                            break;
                        case 6:
                            oAre.put("dia", "Viernes" );
                            break;
                        case 7:
                            oAre.put("dia", "Sabado" );
                            break;

                    }

                    switch (a.getEstAsi()) {
                        case 0:
                            oAre.put("estado","A" );
                            nAsis++;
                            break;
                        case 1:
                            oAre.put("estado", "F");
                            if (a.getJusId() != null) {
                                nFinj++;
                            } else {
                                nFjus++;
                            }

                            break;
                        case 2:  
                            oAre.put("estado","T" );
                            if (a.getJusId() != null) {
                                nTinj++;
                            } else {
                                nTjus++;
                            }
                            break;
                        default: 
                            oAre.put("estado","" );
                            break;
                    }
                    oAre.put("jus",a.getDesJus() );

                    aAre.put(oAre);
                }
                aAsistencia.put("asistencias", aAre);
                JSONObject oResumen = new JSONObject();
                oResumen.put("nAsis", nAsis);
                oResumen.put("nTjus", nTjus);
                oResumen.put("Tinj", nTinj);
                oResumen.put("nFjus", nFjus);
                oResumen.put("Finj", nFinj);
                aAsistencia.put("resumen", oResumen);
                
                oResponse.put(areaId+"", aAsistencia);
            }
            
        } catch (Exception e) {
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listas las Areas", e.getMessage() );
        }
        return WebResponse.crearWebResponseExito("Se listo correctamente", oResponse);
    }
    
}
