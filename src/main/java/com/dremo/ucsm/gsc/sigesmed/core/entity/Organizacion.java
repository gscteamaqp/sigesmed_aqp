package com.dremo.ucsm.gsc.sigesmed.core.entity;

import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.DiasEspeciales;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="organizacion")
public class Organizacion  implements java.io.Serializable {
    @Id
    @Column(name="org_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_organizacion", sequenceName="organizacion_org_id_seq" )
    @GeneratedValue(generator="secuencia_organizacion")
    private int orgId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="tip_org_id", nullable=false)
    private TipoOrganizacion tipoOrganizacion;
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="org_pad_id")
    private Organizacion organizacionPadre;
    @Column(name="cod", nullable=false, length=16)
    private String cod;
    @Column(name="nom", nullable=false, length=64)
    private String nom;
    @Column(name="ali", nullable=false, length=64)
    private String ali;
    @Column(name="des", length=256)
    private String des;
    @Column(name="dir", length=128)
    private String dir;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_mod", length=29)
    private Date fecMod;
    @Column(name="usu_mod")
    private Integer usuMod;
    @Column(name="est_reg", length=1)
    private Character estReg;
    @OneToMany(fetch=FetchType.LAZY, mappedBy="organizacion")
    private List<UsuarioSession> usuarios;
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy="organizacionPadre")
    private List<Organizacion> organizaciones;
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy="organizacionId")
    private List<DiasEspeciales> diasEspeciales;
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy="organizacion")
    private List<Trabajador> trabajadores;
    
    //SMDG
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "niv_id", referencedColumnName = "niv_id")    
    private Nivel nivel;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "org_ubi_ide", referencedColumnName = "ubi_ide")    
    private Ubigeo ubigeo;  
    
    @Column(name="ubi_cod", length=6)
    private String ubiCod;
    @Column(name="org_ges", length=2)
    private String orgGes;
    @Column(name="org_car", length=2)
    private String orgCar;
    @Column(name="org_pro", length=3)
    private String orgPro;
    @Column(name="org_for", length=5)
    private String orgFor;
    @Column(name="org_var", length=2)
    private String orgVar;
    @Column(name="org_mod", length=3)
    private String orgMod;
    
    @Column(name="org_img_inst")
    private String orgImgInst;
    //fin

    public Organizacion(int orgId, TipoOrganizacion tipoOrganizacion, Organizacion organizacionPadre, String cod, String nom, String ali, String des, String dir, Date fecMod, Integer usuMod, Character estReg, List<UsuarioSession> usuarios, List<Organizacion> organizaciones, List<DiasEspeciales> diasEspeciales, List<Trabajador> trabajadores, Nivel nivel, Ubigeo ubigeo, String ubiCod, String orgGes, String orgCar, String orgPro, String orgFor, String orgVar, String orgMod) {
        this.orgId = orgId;
        this.tipoOrganizacion = tipoOrganizacion;
        this.organizacionPadre = organizacionPadre;
        this.cod = cod;
        this.nom = nom;
        this.ali = ali;
        this.des = des;
        this.dir = dir;
        this.fecMod = fecMod;
        this.usuMod = usuMod;
        this.estReg = estReg;
        this.usuarios = usuarios;
        this.organizaciones = organizaciones;
        this.diasEspeciales = diasEspeciales;
        this.trabajadores = trabajadores;
        this.nivel = nivel;
        this.ubigeo = ubigeo;
        this.ubiCod = ubiCod;
        this.orgGes = orgGes;
        this.orgCar = orgCar;
        this.orgPro = orgPro;
        this.orgFor = orgFor;
        this.orgVar = orgVar;
        this.orgMod = orgMod;
    }
    
    public Organizacion() {
    }
    
    public Organizacion(int orgId) {
        this.orgId = orgId;
    }
	
    public Organizacion(int orgId, TipoOrganizacion tipoOrganizacion, String cod, String nom) {
        this.orgId = orgId;
        this.tipoOrganizacion = tipoOrganizacion;
        this.cod = cod;
        this.nom = nom;
    }
    public Organizacion(int orgId, TipoOrganizacion tipoOrganizacion,String cod, String nom, String ali, String des, String dir, Date fecMod, Integer usuMod, Character estReg) {
       this.orgId = orgId;
       this.tipoOrganizacion = tipoOrganizacion;
       this.cod = cod;
       this.nom = nom;
       this.ali = ali;
       this.des = des;
       this.dir = dir;
       this.fecMod = fecMod;
       this.usuMod = usuMod;
       this.estReg = estReg;
    }
    public Organizacion(int orgId, TipoOrganizacion tipoOrganizacion,Organizacion organizacionPadre, String cod, String nom, String ali, String des, String dir, Date fecMod, Integer usuMod, Character estReg, List<UsuarioSession> usuarios) {
       this.orgId = orgId;
       this.tipoOrganizacion = tipoOrganizacion;
       this.organizacionPadre = organizacionPadre;
       this.cod = cod;
       this.nom = nom;
       this.ali = ali;
       this.des = des;
       this.dir = dir;
       this.fecMod = fecMod;
       this.usuMod = usuMod;
       this.estReg = estReg;
       this.usuarios = usuarios;
    }
   
    public int getOrgId() {
        return this.orgId;
    }
    
    public void setOrgId(int orgId) {
        this.orgId = orgId;
    }

    public TipoOrganizacion getTipoOrganizacion() {
        return this.tipoOrganizacion;
    }    
    public void setTipoOrganizacion(TipoOrganizacion tipoOrganizacion) {
        this.tipoOrganizacion = tipoOrganizacion;
    }

    public Organizacion getOrganizacionPadre() {
        return this.organizacionPadre;
    }    
    public void setOrganizacionPadre(Organizacion organizacionPadre) {
        this.organizacionPadre = organizacionPadre;
    }
    
    public String getCod() {
        return this.cod;
    }
    
    public void setCod(String cod) {
        this.cod = cod;
    }

    
    public String getNom() {
        return this.nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    public String getAli() {
        return this.ali;
    }
    public void setAli(String ali) {
        this.ali = ali;
    }
    
    public String getDes() {
        return this.des;
    }
    public void setDes(String des) {
        this.des = des;
    }
    
    public String getDir() {
        return this.dir;
    }
    public void setDir(String dir) {
        this.dir = dir;
    }
    
    public Date getFecMod() {
        return this.fecMod;
    }
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    
    public Integer getUsuMod() {
        return this.usuMod;
    }
    
    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    
    public Character getEstReg() {
        return this.estReg;
    }
    
    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public List<UsuarioSession> getUsuarioSessions() {
        return this.usuarios;
    }
    public void setUsuarioSessions(List<UsuarioSession> usuarios) {
        this.usuarios = usuarios;
    }
    
    public List<Organizacion> getOrganizaciones() {
        return this.organizaciones;
    }
    public void setOrganizaciones(List<Organizacion> organizaciones) {
        this.organizaciones = organizaciones;
    }

    public List<Trabajador> getTrabajadores() {
        return this.trabajadores;
    }
    public void setTrabajadores(List<Trabajador> trabajadores) {
        this.trabajadores = trabajadores;
    }

    public Nivel getNivId() {
        return nivel;
    }

    public void setNivId(Nivel nivel) {
        this.nivel = nivel;
    }

    public Ubigeo getUbigeo() {
        return ubigeo;
    }

    public void setUbigeo(Ubigeo ubigeo) {
        this.ubigeo = ubigeo;
    }
    
    @Override
    public String toString(){
        return EntityUtil.objectToJSONString(new String[]{"orgId","cod","nom","ali","des","dir"},null, this);
    }

    public List<DiasEspeciales> getDiasEspeciales() {
        return diasEspeciales;
    }

    public void setDiasEspeciales(List<DiasEspeciales> diasEspeciales) {
        this.diasEspeciales = diasEspeciales;
    }

    public List<UsuarioSession> getUsuarios() {
        return usuarios;
    }

    public Nivel getNivel() {
        return nivel;
    }

    public String getUbiCod() {
        return ubiCod;
    }

    public String getOrgGes() {
        return orgGes;
    }

    public String getOrgCar() {
        return orgCar;
    }

    public String getOrgPro() {
        return orgPro;
    }

    public String getOrgFor() {
        return orgFor;
    }

    public String getOrgVar() {
        return orgVar;
    }

    public String getOrgMod() {
        return orgMod;
    }

    public void setUsuarios(List<UsuarioSession> usuarios) {
        this.usuarios = usuarios;
    }

    public void setNivel(Nivel nivel) {
        this.nivel = nivel;
    }

    public void setUbiCod(String ubiCod) {
        this.ubiCod = ubiCod;
    }

    public void setOrgGes(String orgGes) {
        this.orgGes = orgGes;
    }

    public void setOrgCar(String orgCar) {
        this.orgCar = orgCar;
    }

    public void setOrgPro(String orgPro) {
        this.orgPro = orgPro;
    }

    public void setOrgFor(String orgFor) {
        this.orgFor = orgFor;
    }

    public void setOrgVar(String orgVar) {
        this.orgVar = orgVar;
    }

    public void setOrgMod(String orgMod) {
        this.orgMod = orgMod;
    }

    public String getOrgImgInst() {
        return orgImgInst;
    }

    public void setOrgImgInst(String orgImgInst) {
        this.orgImgInst = orgImgInst;
    }
    
}
