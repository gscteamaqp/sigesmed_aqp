/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.ma;
import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ConfiguracionNota;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.RegistroAuxiliar;
import java.util.List;
/**
 *
 * @author abel
 */
public interface ConfiguracionNotaDao extends GenericDao<ConfiguracionNota>{
    
    public List<ConfiguracionNota>listar_configuracion();
    public List<ConfiguracionNota>listarConfiguracionDocente(int docID,int orgID);
}
