/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.LegajoPersonal;
import java.util.List;

/**
 *
 * @author gscadmin
 */
public interface LegajoPersonalDao extends GenericDao<LegajoPersonal>{
    public LegajoPersonal buscarPorId(Integer legPerId);
    public List<LegajoPersonal> listarxFichaEscalafonaria(int ficEscId);
}
