package com.dremo.ucsm.gsc.sigesmed.core.entity.std;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="tipo_documento" ,schema="administrativo")
public class TipoDocumento  implements java.io.Serializable  {

    @Id
    @Column(name="tip_doc_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_tipodocumento", sequenceName="administrativo.tipo_documento_tip_doc_id_seq" )
    @GeneratedValue(generator="secuencia_tipodocumento")
    private int tipDocId;
    @Column(name="nom", nullable=false, length=64)
    private String nom;
    @Column(name="des", length=256)
    private String des;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_mod", length=29)
    private Date fecMod;
    @Column(name="usu_mod")
    private Integer usuMod;
    @Column(name="est_reg", length=1)
    private char estReg;

    public TipoDocumento() {
    }
    public TipoDocumento(int tipDocId) {
        this.tipDocId = tipDocId;
    }
    public TipoDocumento(int tipDocId, String nom, String des, Date fecMod, Integer usuMod, char estReg) {
       this.tipDocId = tipDocId;
       this.nom = nom;
       this.des = des;
       this.fecMod = fecMod;
       this.usuMod = usuMod;
       this.estReg = estReg;
    }
   
    public int getTipDocId() {
        return this.tipDocId;
    }
    public void setTipDocId(int tipDocId) {
        this.tipDocId = tipDocId;
    }
    
    public String getNom() {
        return this.nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    public String getDes() {
        return this.des;
    }
    public void setDes(String des) {
        this.des = des;
    }

    public Date getFecMod() {
        return this.fecMod;
    }
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }
    
    public Integer getUsuMod() {
        return this.usuMod;
    }
    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }
    
    public char getEstReg() {
        return this.estReg;
    }
    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }
}


