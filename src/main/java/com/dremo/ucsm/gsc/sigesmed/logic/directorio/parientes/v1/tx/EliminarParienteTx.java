/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.directorio.parientes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.di.ParientesDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Parientes;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class EliminarParienteTx implements ITransaction{
    
    @Override
    public WebResponse execute(WebRequest wr) {        
        /*
        *  Parte para la operacion en la Base de Datos
        */        
                
        int parId = 0,
            perId = 0;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            parId = requestData.getInt("parId");
            perId = requestData.getInt("perId");            
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo eliminar", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        ParientesDao parientesDao = (ParientesDao)FactoryDao.buildDao("di.ParientesDao");
        try{
            parientesDao.delete(new Parientes(parId, perId));
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar el Area\n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el Area", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Area se elimino correctamente");
        //Fin
                
    }
    
}
