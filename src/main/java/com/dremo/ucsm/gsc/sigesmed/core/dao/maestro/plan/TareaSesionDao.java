/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan;
import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.TareaSesionAprendizaje;
import java.util.List;
/**
 *
 * @author abel
 */
public interface TareaSesionDao extends GenericDao<TareaSesionAprendizaje> {
    
    List<TareaSesionAprendizaje> obtenerTarea(int id_tarea);
}
