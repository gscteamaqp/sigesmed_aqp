/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.LibroCajaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.cantidadModel;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author G-16
 */
public class ListarControlFechaCierreCajaTx implements ITransaction{
    
    @Override
    public WebResponse execute(WebRequest wr) {

        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        
        Date desde = null;
        Date hasta = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
           
            
            if(!requestData.getString("desde").contentEquals(""))
                desde = new SimpleDateFormat("dd/M/yyyy").parse( requestData.getString("desde"));
            if(!requestData.getString("hasta").contentEquals(""))
                hasta = new SimpleDateFormat("dd/M/yyyy HH:mm:ss").parse( requestData.getString("hasta") + " 23:59:59" );
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo realizar la estadistica", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        List<cantidadModel> registros = null;
        
        LibroCajaDao hisDao = (LibroCajaDao)FactoryDao.buildDao("sci.LibroCajaDao");
        try{
            registros = hisDao.ControlesCajaPorFechaCierre(desde, hasta);
            
        }catch(Exception e){
            System.out.println("E"+e);
            return WebResponse.crearWebResponseError("No se pudo realizar la estadistica Consulta", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
      
       
        
        
        
        JSONArray nombre = new JSONArray();
        JSONArray cantidad = new JSONArray();
        
        for(cantidadModel a:registros){
          
           nombre.put(a.nombre);
           
           cantidad.put(a.num2);
        }
       
        for(int j=0;j<nombre.length();j++){
           System.out.println(nombre.get(j));    
           System.out.println(cantidad.get(j)); 
           

        }
        
        
        
        JSONObject res = new JSONObject();
              
        res.put("nombre", nombre);
        
        res.put("cantidad", cantidad);
        
       
        return WebResponse.crearWebResponseExito("Se Listo correctamente",res);        
        //Fin     
        
    }
}
