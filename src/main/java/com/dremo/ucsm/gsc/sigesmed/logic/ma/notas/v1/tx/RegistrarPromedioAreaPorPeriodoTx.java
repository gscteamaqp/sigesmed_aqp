/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.ma.notas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.ConfiguracionNotaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.HistoricoNotasEstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.RegistroAuxiliarDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.DocenteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ConfiguracionNota;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.HistoricoNotasEstudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.RegistroAuxiliarCompetencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.IndicadoresSesionAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import static com.dremo.ucsm.gsc.sigesmed.logic.ma.notas.v1.tx.MA_Constantes.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class RegistrarPromedioAreaPorPeriodoTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(RegistrarPromedioAreaPorPeriodoTx.class.getName());
    private List<ConfiguracionNota> config_docente = null;
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idSession = data.getInt("ses");
        char idPeriodo  = data.getString("per").charAt(0);
        int numPer  = data.getInt("numper");
        char tipNot = data.optString("tip", "N").charAt(0);
        int idUsr = data.getInt("usr");
        int idArea = data.getInt("are");
        int idOrg = data.getInt("org");
        JSONArray array = data.optJSONArray("estudiantes");
        if(array == null) return WebResponse.crearWebResponseError("Se tiene que seleccionar a los alumnos");
        listarConfiguracionDocente(idOrg , idUsr);
        return registrarNotas(idSession, idPeriodo, numPer, idUsr, idArea, tipNot, idOrg, array);
    }

    private WebResponse registrarNotas(int idSession, char idPeriodo, int numPer, int idUsr, int idArea ,char tipNot, int idOrg, JSONArray array) {
        try{
            RegistroAuxiliarDao regisDao = (RegistroAuxiliarDao) FactoryDao.buildDao("ma.RegistroAuxiliarDao");
            DocenteDao docDao = (DocenteDao) FactoryDao.buildDao("maestro.DocenteDao");
            EstudianteDao estDao = (EstudianteDao) FactoryDao.buildDao("maestro.EstudianteDao");
            HistoricoNotasEstudianteDao hisNotEst = (HistoricoNotasEstudianteDao) FactoryDao.buildDao("ma.HistoricoNotasEstudianteDao");
            
            String mensaje = "Se registro el promedio del area con exito";
            for(int i = 0; i < array.length(); i++){
                JSONObject alumno = array.getJSONObject(i);
                int idGraOrEst = alumno.getInt("idgra");
                
                List<IndicadoresSesionAprendizaje> competencias = regisDao.obtenerCompetenciasDeSesion(idSession);
                Double promedio = 0.0;
                for(IndicadoresSesionAprendizaje indApr: competencias){
                    RegistroAuxiliarCompetencia regAuxCom = regisDao.buscarNotaCompetenciaEsp(indApr.getCompetencia().getComId(), idArea, numPer, idGraOrEst);
                    String nota = "0.0";
                    if (regAuxCom != null){
                        nota = regAuxCom.getNota();
                    }
                        promedio += Double.parseDouble(nota);

                }
                promedio = promedio/competencias.size();
                String nota = String.valueOf(Math.round(promedio));
                String nota_lit = obtenerNotaLiteral(promedio);
                 
                
                HistoricoNotasEstudiante historico = new HistoricoNotasEstudiante();
                if(!regisDao.buscarNotaAreaEnPeriodo(idArea, numPer, idGraOrEst)){
                    historico.setNota(nota);
                    historico.setNot_are_lit(nota_lit);
                    historico.setAreaCurricular(docDao.buscarAreaPorId(idArea));
                    historico.setGradoEst(estDao.buscarGradoIEEstudiante(idGraOrEst));
                    historico.setPeriodos(regisDao.buscarPeriodoPorId(numPer));
                    hisNotEst.insert(historico);
                } else{
                    historico = regisDao.buscaHistoricoNotasEstudiante(idArea, numPer, idGraOrEst);
                    historico.setNota(nota);
                    historico.setNot_are_lit(nota_lit);
                    hisNotEst.update(historico);
                }
            }
            return WebResponse.crearWebResponseExito(mensaje);
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarPromedioArea",e);
            return WebResponse.crearWebResponseError("No se pudo registrar el promedio del area en el periodo actual");
        }
    }
    
     public String obtenerNotaLiteral(Double not){
        
        for(ConfiguracionNota conf : config_docente){
            if(not >= conf.getConf_not_min() && not <= conf.getConf_not_max()){
                return conf.getConf_cod();
            }
        }
        return "";

    }
    
    public void listarConfiguracionDocente(int org_id , int idUser){
        ConfiguracionNotaDao configNot = (ConfiguracionNotaDao) FactoryDao.buildDao("ma.ConfiguracionNotaDao");
        config_docente = configNot.listarConfiguracionDocente(idUser, org_id);

    }
}
