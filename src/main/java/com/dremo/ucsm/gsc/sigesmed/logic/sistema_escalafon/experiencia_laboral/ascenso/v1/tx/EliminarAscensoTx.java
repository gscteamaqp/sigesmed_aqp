/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.ascenso.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.AscensoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Ascenso;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class EliminarAscensoTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(EliminarAscensoTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        Integer ascId = 0;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            ascId = requestData.getInt("ascId");          
        
        }catch(Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"eliminarAscenso",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar", e.getMessage() );
        }
        
        AscensoDao ascensoDao = (AscensoDao)FactoryDao.buildDao("se.AscensoDao");
        try{
            ascensoDao.delete(new Ascenso(ascId));
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar el ascenso\n" + e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el ascenso", e.getMessage() );
        }
        return WebResponse.crearWebResponseExito("El ascenso se elimino correctamente");
    }
    
}
