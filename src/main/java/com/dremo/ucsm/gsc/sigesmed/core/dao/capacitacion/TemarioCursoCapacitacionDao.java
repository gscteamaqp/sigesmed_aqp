package com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.TemarioCursoCapacitacion;
import java.util.List;
import java.util.Map;

public interface TemarioCursoCapacitacionDao extends GenericDao<TemarioCursoCapacitacion> {
    List<TemarioCursoCapacitacion> buscarPorSede(int codSed);
    TemarioCursoCapacitacion buscarPorId(int idTem);
    Map<Integer, String> obtenerTemas(int sedCod);
    boolean verificarComentarios(int temCod);
    TemarioCursoCapacitacion buscarEliminar(int temCod);
}
