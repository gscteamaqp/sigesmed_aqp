/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.std.HistorialExpedienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.EntidadCantidadModel;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author angel
 */
public class ReporteEstadisticaExpedienteEnAreaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int organizacionID = 0;
        Date desde = null;
        Date hasta = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            organizacionID = requestData.getInt("organizacionID");
            
            if(!requestData.getString("desde").contentEquals(""))
                desde = new SimpleDateFormat("dd/M/yyyy").parse( requestData.getString("desde"));
            if(!requestData.getString("hasta").contentEquals(""))
                hasta = new SimpleDateFormat("dd/M/yyyy HH:mm:ss").parse( requestData.getString("hasta") + " 23:59:59" );
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo realizar la estadistica", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        List<EntidadCantidadModel> expedientes = null;
        List<EntidadCantidadModel> expedientesDerivados = null;
        List<EntidadCantidadModel> expedientesDevueltos = null;
        List<EntidadCantidadModel> expedientesFinalizados = null;
        
        HistorialExpedienteDao hisDao = (HistorialExpedienteDao)FactoryDao.buildDao("std.HistorialExpedienteDao");
        try{
            expedientes = hisDao.cantidadExpedientesEnAreaPorOrganizacionYFecha(organizacionID, desde, hasta);
            expedientesDerivados = hisDao.cantidadExpedientesEnAreaPorOrganizacionYEstadoYFecha(organizacionID,EstadoExpediente.DERIVADO, desde, hasta);
            expedientesDevueltos = hisDao.cantidadExpedientesEnAreaPorOrganizacionYEstadoYFecha(organizacionID,EstadoExpediente.DEVUELTO, desde, hasta);
            expedientesFinalizados = hisDao.cantidadExpedientesEnAreaPorOrganizacionYEstadoYFecha(organizacionID,EstadoExpediente.FINALIZADO, desde, hasta);
            
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo realizar la estadistica", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
         
        JSONArray aAreas = new JSONArray();
        
        
        JSONArray aExpedientes = new JSONArray();        
        JSONArray aDerivados = new JSONArray();
        JSONArray aDevueltos = new JSONArray();
        JSONArray aFinalizados = new JSONArray();
        
        boolean encontro = false;
        for(EntidadCantidadModel a:expedientes){
            
            aAreas.put(a.nombre);
            aExpedientes.put(a.num1);
            
            for(EntidadCantidadModel e :expedientesDerivados)
                if( e.ID == a.ID ){
                    aDerivados.put(e.num1);
                    encontro = true;
                    break;
                }
            if(!encontro)
                aDerivados.put(0);
            encontro = false;
            
            for(EntidadCantidadModel e :expedientesDevueltos)
                if( e.ID == a.ID ){
                    aDevueltos.put(e.num1);
                    encontro = true;
                    break;
                }
            if(!encontro)
                aDevueltos.put(0);
            encontro = false;
            
            for(EntidadCantidadModel e :expedientesFinalizados)
                if( e.ID == a.ID ){
                    aFinalizados.put(e.num1);
                    encontro = true;
                    break;
                }
            if(!encontro)
                aFinalizados.put(0);
            encontro = false;
            
        }
        for(int j=0;j<aAreas.length();j++){
            System.out.println(aAreas.get(j));            
        }
        
      
           
        
        JSONArray aOrg = new JSONArray();

        for(int i = 0 ; i < aAreas.length() ;i++){
            
            JSONObject o = new JSONObject();
             
            String area = aAreas.get(i).toString();
            o.put("areaa",area);
            o.put("recibidosa",aExpedientes.get(i));
            o.put("devueltosa",aDevueltos.get(i));
            o.put("derivadosa",aDerivados.get(i));
            o.put("finalizadosa",aFinalizados.get(i));
            
            aOrg.put(o);
        }
    
      // System.out.println(aOrg);
        return WebResponse.crearWebResponseExito("Se Listo correctamente",aOrg);        
        //Fin
    }
    
}

